##############################################################################
#	Makefile for Imapacs server.
##############################################################################

CC=c++

CCFLAGS=-c -O3 -march=x86-64 -mtune=i686 -std=c++11 -Wno-write-strings
CCSWITCH=-lpthread -std=c++11 -Wno-write-strings

# The value of these variables may need to be changed depending on your dcmtk
# and oci sdks installation.
#
OCIINCLUDE=$(ORAINCLUDE)/rdbms/public
DCMTK_ROOT_INCLUDE=/usr/local/include

LIBRARIES=-ldcmnet -ldcmdata -ldcmjpeg -ldcmimgle -ldcmimage -ldcmpstat -lijg12 -lijg16 -lijg8 -loflog -lofstd -ldl -lz 

EXE=Imapacs
OBJS=OCIDatabase.o SMTPClient.o Mailer.o log.o StringBuffer.o CDCMClient.o DICOMUtil.o os.o PatchModule.o DBGTool.o CFindService.o CGetService.o CMoveService.o CStoreService.o NActionService.o NCreateService.o NSetService.o C_ECHOTransaction.o C_FINDTransaction.o C_GETTransaction.o C_MOVETransaction.o C_STORETransaction.o N_ACTIONTransaction.o N_CREATETransaction.o N_SETTransaction.o DICOMCompressor.o PacsConfig.o DICOMSession.o ServerMain.o main.o

build:
	$(CC) $(CCFLAGS) -I $(OCIINCLUDE) ./Utils/OCIDatabase.cpp
	$(CC) $(CCFLAGS) ./Utils/SMTPClient.cpp
	$(CC) $(CCFLAGS) ./Utils/Mailer.cpp	
	$(CC) $(CCFLAGS) ./Utils/log.cpp
	$(CC) $(CCFLAGS) ./Utils/DBGTool.cpp
	$(CC) $(CCFLAGS) ./Utils/StringBuffer.cpp
	$(CC) $(CCFLAGS) -I $(DCMTK_ROOT_INCLUDE) -I $(OCIINCLUDE) ./Utils/CDCMClient.cpp
	$(CC) $(CCFLAGS) -I $(DCMTK_ROOT_INCLUDE) ./Utils/DICOMUtil.cpp
	$(CC) $(CCFLAGS) -I $(DCMTK_ROOT_INCLUDE) ./Utils/os.cpp
	$(CC) $(CCFLAGS) -I $(DCMTK_ROOT_INCLUDE) -I $(OCIINCLUDE) ./Utils/PatchModule.cpp
	$(CC) $(CCFLAGS) -I $(DCMTK_ROOT_INCLUDE) -I $(OCIINCLUDE) ./DatabaseServices/CFindService.cpp
	$(CC) $(CCFLAGS) -I $(DCMTK_ROOT_INCLUDE) -I $(OCIINCLUDE) ./DatabaseServices/CGetService.cpp
	$(CC) $(CCFLAGS) -I $(DCMTK_ROOT_INCLUDE) -I $(OCIINCLUDE) ./DatabaseServices/CMoveService.cpp
	$(CC) $(CCFLAGS) -I $(DCMTK_ROOT_INCLUDE) -I $(OCIINCLUDE) ./DatabaseServices/CStoreService.cpp
	$(CC) $(CCFLAGS) -I $(DCMTK_ROOT_INCLUDE) -I $(OCIINCLUDE) ./DatabaseServices/NActionService.cpp
	$(CC) $(CCFLAGS) -I $(DCMTK_ROOT_INCLUDE) -I $(OCIINCLUDE) ./DatabaseServices/NCreateService.cpp
	$(CC) $(CCFLAGS) -I $(DCMTK_ROOT_INCLUDE) -I $(OCIINCLUDE) ./DatabaseServices/NSetService.cpp
	$(CC) $(CCFLAGS) -I $(DCMTK_ROOT_INCLUDE) -I $(OCIINCLUDE) ./Transactions/C_ECHOTransaction.cpp
	$(CC) $(CCFLAGS) -I $(DCMTK_ROOT_INCLUDE) -I $(OCIINCLUDE) ./Transactions/C_FINDTransaction.cpp
	$(CC) $(CCFLAGS) -I $(DCMTK_ROOT_INCLUDE) -I $(OCIINCLUDE) ./Transactions/C_GETTransaction.cpp
	$(CC) $(CCFLAGS) -I $(DCMTK_ROOT_INCLUDE) -I $(OCIINCLUDE) ./Transactions/C_MOVETransaction.cpp
	$(CC) $(CCFLAGS) -I $(DCMTK_ROOT_INCLUDE) -I $(OCIINCLUDE) ./Transactions/C_STORETransaction.cpp
	$(CC) $(CCFLAGS) -I $(DCMTK_ROOT_INCLUDE) -I $(OCIINCLUDE) ./Transactions/N_ACTIONTransaction.cpp
	$(CC) $(CCFLAGS) -I $(DCMTK_ROOT_INCLUDE) -I $(OCIINCLUDE) ./Transactions/N_CREATETransaction.cpp
	$(CC) $(CCFLAGS) -I $(DCMTK_ROOT_INCLUDE) -I $(OCIINCLUDE) ./Transactions/N_SETTransaction.cpp
	$(CC) $(CCFLAGS) -I $(DCMTK_ROOT_INCLUDE) DICOMCompressor.cpp
	$(CC) $(CCFLAGS) -I $(DCMTK_ROOT_INCLUDE) -I $(OCIINCLUDE) PacsConfig.cpp
	$(CC) $(CCFLAGS) -I $(DCMTK_ROOT_INCLUDE) -I $(OCIINCLUDE) DICOMSession.cpp
	$(CC) $(CCFLAGS) -I $(DCMTK_ROOT_INCLUDE) -I $(OCIINCLUDE) ServerMain.cpp
	$(CC) $(CCFLAGS) -I $(DCMTK_ROOT_INCLUDE) -I $(OCIINCLUDE) main.cpp
	
	$(CXX) -o $(EXE) $(OBJS) $(CCSWITCH) $(LIBRARIES) -L $(DCMTK_ROOT_LIB) -L $(OCILIBSO)

 installer:
	mkdir ./installer
	cp Imapacs ./installer
	cp -R Oracle ./installer
	cp install.sh ./installer
	cp uninstall.sh ./installer
	cp imapacs.service ./installer
	cp imapacs_transient.service ./installer
	cp dicom.dic ./installer
	cp stop.sh ./installer
	chmod -R 755 ./installer/*
	
	dos2unix ./installer/install.sh
	dos2unix ./installer/uninstall.sh
	dos2unix ./installer/imapacs.service
	dos2unix ./installer/imapacs_transient.service
	dos2unix ./installer/stop.sh

	zip -r pacs_3.6.0.2_installer.zip ./installer
	
	rm -f $(EXE)
	rm -f *.o

clean:
	rm -f $(EXE)
	rm -f *.o
	rm -Rf ./installer
	rm -f *.zip
