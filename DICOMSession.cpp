///////////////////////////////////////////////////////////////////////////////////////////////////
//	DICOMSession.cpp
///////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef WIN32
#include "stdafx.h"
#endif

#include "DICOMCompressor.h"
#include "./Transactions/ITransaction.h"
#include "./Transactions/TransactionFactory.h"
#include "./Utils/DICOMUtil.h"
#include "DICOMSession.h"

#ifdef WIN32
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////
//
namespace imapacs
{
	///////////////////////////////////////////////////////////////////////////////////////////////
	//	This is where all initializations take place.
	bool DICOMSession::Init()
	{
		bool bRetVal     = false;
		OFCondition cond = EC_Normal;

		//	For convenience.
		m_pServerLogger   = m_pCfg->GetServerLogger();
		m_pModalityLogger = m_pCfg->GetModalityLogger();
		m_pMailer         = m_pCfg->GetMailer();
		m_pDb             = m_pCfg->GetDb();

		DICOMCompressor::Init();

		DUL_markProcessAsForkedChild();
		dcmExternalSocketHandle.set(m_iFd);	//	Set the accepted descriptor to be
											//	used by dcmtk.

		cond = ASC_initializeNetwork(NET_ACCEPTORREQUESTOR,
                                     1024,		//	The value here doesn't have any importance since
												//	we have already accepted the connection.
								     -1,		//	Timeout (-1 means no timeout).
								     &m_pNetwork);
		if(cond.good())
		{
			bRetVal = true;
		}
		else
		{
			m_pServerLogger->LogMsg("ERROR : %s", cond.text());
			m_pServerLogger->LogMsg("         Failed to initialize network.");

			DICOMCompressor::Shutdown();
		}
		return bRetVal;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////
	//
	bool DICOMSession::Run()
	{
		char sLogPrefix[129]               = {0};
		char sCallingAeTitle[17]           = {0};
		char sCalledAeTitle[17]            = {0};
		ITransaction *pTransaction         = nullptr;
		OFCondition cond                   = EC_Normal;
		T_ASC_PresentationContextID PresID = 0;
		T_DIMSE_Message msg;

		//	Receive association parameters like PDU size as well as the list
		//	of presentation contexts. Also creates association object with
		//	data from the client (like AE titles, etc).
		//
		cond = ASC_receiveAssociation(m_pNetwork, &m_pAssociation, ASC_DEFAULTMAXPDU, nullptr, nullptr, OFFalse, DUL_BLOCK, 1);
		if((cond == DUL_NOASSOCIATIONREQUEST) || cond.bad())
		{
			DICOMUtil::ClearAssociation(&m_pAssociation);
			DICOMUtil::ClearNetwork(&m_pNetwork);

			m_pServerLogger->LogMsg("ERROR : %s", cond.text());
			m_pServerLogger->LogMsg("         Failed to receive association");
			m_pMailer->AddLine("ERROR : %s", cond.text());
			m_pMailer->AddLine("         Failed to receive association");

			return false;
		}

		//	Get the AE title of the client and the AE title called by
		//	the client.
		//
		cond = ASC_getAPTitles(m_pAssociation->params, sCallingAeTitle, sCalledAeTitle, nullptr);
		if(cond.bad())
		{
			DICOMUtil::ClearAssociation(&m_pAssociation);
			DICOMUtil::ClearNetwork(&m_pNetwork);

			m_pServerLogger->LogMsg("ERROR : %s", cond.text());
			m_pServerLogger->LogMsg("         Failed to get AETitle.");
			m_pMailer->AddLine("ERROR : %s", cond.text());
			m_pMailer->AddLine("         Failed to get AETitle.");

			return false;
		}

		m_pMailer->AddLine("Calling AE Title:  [%s]", sCallingAeTitle);

		//	Setup modality logger.
		//
		sprintf(sLogPrefix, "%s_", sCallingAeTitle);
		m_pModalityLogger->SetPrefix(sLogPrefix);
		m_pModalityLogger->LogMsg(BEGIN_LOG_LINE);

		///////////////////////////////////////////////////////////////////////////////////////////////
		//	Check if the client is authorized.
		//
		if(!IsKnownAETitle(sCallingAeTitle))
		{
			m_pServerLogger->LogMsg("[%s] - Rejecting association : unauthorized application.", sCallingAeTitle);
			m_pServerLogger->LogMsg("[%s] - Unknown AETitle", sCallingAeTitle);

			m_pModalityLogger->LogMsg("Rejecting association : unauthorized application.");

			m_pMailer->AddLine("Rejecting association : unauthorized application.");
			m_pMailer->AddLine("Unknown AETitle:  [%s].", sCallingAeTitle);

			RejectAssociation(ARR_BADAEPEER);

			DICOMUtil::ClearAssociation(&m_pAssociation);
			DICOMUtil::ClearNetwork(&m_pNetwork);

			m_pModalityLogger->LogMsg(END_LOG_LINE);

			return false;
		}

		//	Loads the per-modality configurations.
		//
		m_pCfg->LoadModalityConfigurations(sCallingAeTitle);

		//	Check what prentation context we accept.
		//
		if(!NegociateAssociation())
		{
			m_pServerLogger->LogMsg("[%s] - Rejecting association : negociation failed.", sCallingAeTitle);
			m_pModalityLogger->LogMsg("[%s] - Rejecting association : negociation failed.", sCallingAeTitle);
			m_pMailer->AddLine("[%s] - Rejecting association : negociation failed.", sCallingAeTitle);

			RejectAssociation(ARR_BADAPPCONTEXT);

			DICOMUtil::ClearAssociation(&m_pAssociation);
			DICOMUtil::ClearNetwork(&m_pNetwork);

			m_pModalityLogger->LogMsg(END_LOG_LINE);

			return false;
		}

		//	Acknoledge the association.
		//
		if(!AcknoledgeAssociation())
		{
			m_pServerLogger->LogMsg("[%s] - Could not acknoledge association.", sCallingAeTitle);
			m_pModalityLogger->LogMsg("Could not acknoledge association.");
			m_pMailer->AddLine("Could not acknoledge association for [%s]", sCallingAeTitle);

			RejectAssociation(ARR_NOSPECIFICREASON);

			DICOMUtil::ClearAssociation(&m_pAssociation);
			DICOMUtil::ClearNetwork(&m_pNetwork);

			m_pModalityLogger->LogMsg(END_LOG_LINE);

			return false;
		}

		//	Process incomming DICOM commands.
		//
		while(cond.good())
		{
			cond = DIMSE_receiveCommand(m_pAssociation, DIMSE_BLOCKING, 0, &PresID, &msg, nullptr);
			if(cond.good())
			{
				pTransaction = TransactionFactory::CreateTransaction(m_pAssociation,
					                                                 &msg,
					                                                 PresID,
																	 m_pCfg);
				if(pTransaction)
				{
					if(pTransaction->ProcessTransaction())
					{
						m_pServerLogger->LogMsg("[%s] - Successful %s.", m_pAssociation->params->DULparams.callingAPTitle, pTransaction->GetTransactionType());
						m_pModalityLogger->LogMsg("Successful %s.", pTransaction->GetTransactionType());
						m_pMailer->ClearMail();
					}
					else
					{
						m_pServerLogger->LogMsg("[%s] - %s RQ with error.", m_pAssociation->params->DULparams.callingAPTitle, pTransaction->GetTransactionType());
						m_pModalityLogger->LogMsg("%s RQ with error.", pTransaction->GetTransactionType());

						if(m_pCfg->SendMail())
						{
							if(!m_pMailer->SendMail())
							{
								m_pServerLogger->LogMsg("Could not send mail");
								m_pServerLogger->LogMsg("Error:  %s", m_pMailer->GetLastError());
							}
						}
					}
					pTransaction->Shutdown();

					delete pTransaction;
					pTransaction = nullptr;

					m_pModalityLogger->LogMsg(END_LOG_LINE);
				}
				else
				{
					m_pModalityLogger->LogMsg("ERROR : Failed to create Transaction object.");
				}
			}
			else
			{
				if(cond == DUL_PEERREQUESTEDRELEASE)
				{
					ASC_acknowledgeRelease(m_pAssociation);
				}
				else if(cond == DUL_PEERABORTEDASSOCIATION)
				{
					//	Peer just dropped the connection, not
					//	much else we can do.
					m_pServerLogger->LogMsg("Connection was lost or closed by the client.");
				}
			}
		}
		return true;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////
	//
	void DICOMSession::Shutdown()
	{
		DICOMCompressor::Shutdown();

		DICOMUtil::ClearNetwork(&m_pNetwork);
		DICOMUtil::ClearAssociation(&m_pAssociation);
	}

	void DICOMSession::AbortAssociation()
	{
		if(m_pAssociation != nullptr)
			ASC_abortAssociation(m_pAssociation);
		else
			close(m_iFd);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////
	//
	void DICOMSession::CloseAssociation()
	{
		if(m_pAssociation)
		{
			ASC_releaseAssociation(m_pAssociation);
			ASC_destroyAssociation(&m_pAssociation);
			ASC_dropNetwork(&m_pNetwork);

			m_pAssociation = nullptr;
			m_pNetwork     = nullptr;
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////////
	//	Initializes the codecs for compression/decompression for the
	//	application. This method MUST be called only ONCE during
	//	application's runtime.
	void DICOMSession::InitializeCodecs()
	{
		// Initialize codec for decoding
		//
		DJDecoderRegistration::registerCodecs(EDC_photometricInterpretation,	//	Color Space
											  EUC_default,						//	UID Creation
											  EPC_default,						//	Planar config
											  OFFalse);							//	Verbose

		// Initialize codec for encoding
		//
		DJEncoderRegistration::registerCodecs();
	}

	///////////////////////////////////////////////////////////////////////////////////////////////
	//	Cleanup the codecs for compression/decompression for the application.
	//	This method MUST be called only ONCE during application's runtime.
	void DICOMSession::ClearCodecs()
	{
		DJDecoderRegistration::cleanup();
		DJEncoderRegistration::cleanup();
	}

	///////////////////////////////////////////////////////////////////////////////////////////////
	//
	bool DICOMSession::IsKnownAETitle(const char *pAETitle)
	{
		StringBuffer sql;
		bool bRetVal    = false;
		char sResult[5] = {0};
		HANDLE hCursor  = 0;

		hCursor = m_pDb->GetCursor();
		if(hCursor)
		{
			sql.Format("SELECT %s.VALIDATE_AE_TITLE('%s') FROM DUAL", m_pCfg->GetPkgName(), pAETitle);

			if(m_pDb->SQLExec(hCursor, sql.GetBuffer(), "%s", sResult))
			{
				if(sResult[0] == '1')
					bRetVal = true;
			}
			else
			{
				m_pModalityLogger->LogMsg("%s", m_pDb->GetLastOciErrorText(hCursor));
				m_pModalityLogger->LogMsg("SQL:  %s", sql.GetBuffer());

				m_pMailer->AddLine("%s", m_pDb->GetLastOciErrorText(hCursor));
				m_pMailer->AddLine("SQL:  %s", sql.GetBuffer());
			}
			m_pDb->DeleteCursor(hCursor);
			hCursor = 0;
		}
		else
		{
			m_pModalityLogger->LogMsg("ERROR : Failed to get cursor in  DataInterface::IsKnownAE [%s line %d]", __FILE__, __LINE__);
			m_pMailer->AddLine("Failed to get cursor in  DataInterface::IsKnownAE");
		}
		return bRetVal;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////
	//
	bool DICOMSession::NegociateAssociation()
	{
		int i                        = 0;
		OFCondition cond             = EC_Normal;
		const char **pTrnsynList     = nullptr;
		int iTrnsynCount             = 0;
		const char **pNonStorageSOP  = nullptr;
		int iNonStorageSOPCount      = 0;
		T_ASC_PresentationContext pc = {0};
		T_ASC_SC_ROLE role           = ASC_SC_ROLE_DEFAULT;

		pNonStorageSOP = m_pCfg->GetSOPList(&iNonStorageSOPCount, false);
		if(!pNonStorageSOP)
		{
			m_pModalityLogger->LogMsg("       Aborting association.");
			m_pMailer->AddLine("Aborting association.");

			return false;
		}

		pTrnsynList = m_pCfg->GetTransferList(pNonStorageSOP[0], &iTrnsynCount);
		if(!pTrnsynList)
		{
			PacsConfig::ClearStringList(pNonStorageSOP, iNonStorageSOPCount);

			m_pModalityLogger->LogMsg("       No transfer syntax found for %s", pNonStorageSOP[0]);
			m_pMailer->AddLine("Aborting association.");

			return false;
		}

		//  Accept any of the non-storage syntaxes.
		//
		cond = ASC_acceptContextsWithPreferredTransferSyntaxes(m_pAssociation->params,
															   (const char**)pNonStorageSOP, 
															   iNonStorageSOPCount,
															   (const char**)pTrnsynList, 
															   iTrnsynCount);

		PacsConfig::ClearStringList(pNonStorageSOP,iNonStorageSOPCount);
		PacsConfig::ClearStringList(pTrnsynList,iTrnsynCount);

		if(cond.good())
		{
			int npc = ASC_countPresentationContexts(m_pAssociation->params);
			for(i = 0; i < npc; ++i)
			{
				ASC_getPresentationContext(m_pAssociation->params, i, &pc);
				if(dcmIsaStorageSOPClassUID(pc.abstractSyntax))
				{
					//	We are prepared to accept whatever role the SCU
					//	proposes. Normally we can be the SCP of the Storage
					//	Service Class. When processing the C-GET operation
					//	we can be the SCU of the Storage Service Class.
					//
					role = pc.proposedRole;

					//	Accept in the order "least wanted" to "most wanted"
					//	transfer syntax. Accepting a transfer syntax will
					//	override previously accepted transfer syntaxes.
					//
					pTrnsynList = m_pCfg->GetTransferList(pc.abstractSyntax, &iTrnsynCount);

					for(int k = (iTrnsynCount - 1); k >= 0; --k)
					{
						if(pTrnsynList && iTrnsynCount)
						{
							for(int j=0; j < (int)pc.transferSyntaxCount; ++j)
							{
								if(strcmp(pc.proposedTransferSyntaxes[j], pTrnsynList[k]) == 0)
								{
									cond = ASC_acceptPresentationContext(m_pAssociation->params, 
																		 pc.presentationContextID, 
																		 pTrnsynList[k], 
																		 role);
									if(m_pCfg->GetModalityDebug())
									{
										if(cond.good())
										{
											m_pModalityLogger->LogMsg("Accepted presentation context:");
											m_pModalityLogger->LogMsg("Proposed transfert syntax:  %s", pc.proposedTransferSyntaxes[j]);
											m_pModalityLogger->LogMsg("Abstract syntax:            %s", pc.abstractSyntax);
										}
										else
										{
											m_pModalityLogger->LogMsg("Refused presentation context:");
											m_pModalityLogger->LogMsg("Proposed transfert syntax:  %s", pc.proposedTransferSyntaxes[j]);
											m_pModalityLogger->LogMsg("Abstract syntax:            %s", pc.abstractSyntax);
										}
									}
									if(cond.bad()) 
									{
										m_pModalityLogger->LogMsg("Error:  %s", cond.text());

										PacsConfig::ClearStringList(pTrnsynList, iTrnsynCount);
										pTrnsynList  = nullptr;
										iTrnsynCount = 0;
										return false;
									}
									break;
								}
							}
						}
						else
						{
							m_pModalityLogger->LogMsg("ERROR : No transfer syntax available for SOP : %s", pc.abstractSyntax);
							m_pMailer->AddLine("ERROR : No transfer syntax available for SOP : %s", pc.abstractSyntax);
						}
					}
					PacsConfig::ClearStringList(pTrnsynList, iTrnsynCount);
					pTrnsynList  = nullptr;
					iTrnsynCount = 0;
				}
			}
			m_pModalityLogger->LogMsg(" ");
		}
		else
		{
			m_pServerLogger->LogMsg("[%s] - Cannot accept presentation contexts: %s", m_pAssociation->params->DULparams.callingAPTitle, cond.text());
			m_pMailer->AddLine("Cannot accept presentation contexts: %s", cond.text());
			return false;
		}
		return true;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////
	//
	bool DICOMSession::RejectAssociation(ASC_REFUSE_REASON Reason)
	{
		bool bRetVal     = false;
		char sReason[33] = {0};
		T_ASC_RejectParameters rej;

		if(m_pAssociation)
		{
			switch(Reason)
			{
				case ARR_TOOMANYASSOCIATIONS:
				{
					strcpy(sReason, "Cannot Fork");

					rej.result = ASC_RESULT_REJECTEDPERMANENT;
					rej.source = ASC_SOURCE_SERVICEPROVIDER_PRESENTATION_RELATED;
					rej.reason = ASC_REASON_SP_PRES_TEMPORARYCONGESTION;
				}
				break;

				case ARR_BADAPPCONTEXT:
				{
					strcpy(sReason, "Bad App Context");

					rej.result = ASC_RESULT_REJECTEDTRANSIENT;
					rej.source = ASC_SOURCE_SERVICEUSER;
					rej.reason = ASC_REASON_SU_APPCONTEXTNAMENOTSUPPORTED;
				}
				break;

				case ARR_BADAEPEER:
				{
					strcpy(sReason, "BadAEPeer");

					rej.result = ASC_RESULT_REJECTEDPERMANENT;
					rej.source = ASC_SOURCE_SERVICEUSER;
					rej.reason = ASC_REASON_SU_CALLINGAETITLENOTRECOGNIZED;
				}
				break;

				case ARR_BADAESERVICE:
				{
					strcpy(sReason, "Bad AE Service");

					rej.result = ASC_RESULT_REJECTEDPERMANENT;
					rej.source = ASC_SOURCE_SERVICEUSER;
					rej.reason = ASC_REASON_SU_CALLEDAETITLENOTRECOGNIZED;
				}
				break;

				case ARR_NOSPECIFICREASON:
				{
					strcpy(sReason, "No Reason");

					rej.result = ASC_RESULT_REJECTEDPERMANENT;
					rej.source = ASC_SOURCE_SERVICEUSER;
					rej.reason = ASC_REASON_SU_NOREASON;
				}
				break;

				default:
				{
					strcpy(sReason, "???");

					rej.result = ASC_RESULT_REJECTEDPERMANENT;
					rej.source = ASC_SOURCE_SERVICEUSER;
					rej.reason = ASC_REASON_SU_NOREASON;
				}
				break;
			}

			OFCondition cond = ASC_rejectAssociation(m_pAssociation, &rej);
			if(cond.good())
			{
				m_pServerLogger->LogMsg("[%s] - Refusing association : %s", m_pAssociation->params->DULparams.callingAPTitle, sReason);
				m_pMailer->AddLine("Refusing association : %s", sReason);

				bRetVal = true;
			}
			else
			{
				m_pServerLogger->LogMsg("[%s] - ERROR sending reject association : %s", m_pAssociation->params->DULparams.callingAPTitle, cond.text());
				m_pMailer->AddLine("Could not send reject association : %s", cond.text());
			}
		}
		return bRetVal;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////
	//
	bool DICOMSession::AcknoledgeAssociation()
	{
		bool bRetVal     = false;
		OFCondition cond = EC_Normal;

		cond = ASC_acknowledgeAssociation(m_pAssociation);
		if(cond.good())
		{
			bRetVal = true;
		}
		else
		{
			m_pServerLogger->LogMsg("[%s] - ERROR : %s", m_pAssociation->params->DULparams.callingAPTitle, cond.text());
			m_pServerLogger->LogMsg("Fail to acknowledge association");
			m_pMailer->AddLine("%s", cond.text());
		}
		return bRetVal;
	}
/*
	/////////////////////////////////////////////////////////////////////////
	//
	int DICOMSession::MakeTmpFile(char *pTemplate)
	{
		char tempfile[81] = {0};
		strncpy(tempfile, "/tmp/dcmqrscp_XXXXXX", 80);
#ifdef HAVE_MKSTEMP
		return mkstemp(tempfile);
#else // ! HAVE_MKSTEMP
		mktemp(tempfile);
		return open(tempfile, O_WRONLY|O_CREAT|O_APPEND, 0644);
#endif
	}
*/
	///////////////////////////////////////////////////////////////////////////////////////////////
	//
#ifdef WIN32
	void DICOMSession::SetDescriptor(SOCKET iFd)
	{
		m_iFd = iFd;
	}
#else
	///////////////////////////////////////////////////////////////////////////////////////////////
	//
	void DICOMSession::SetDescriptor(int iFd)
	{
		m_iFd = iFd;

		// int fd  = 0;

		// m_iFd = dup(0);
		// if(iFd < 0)
		// {
		// 	m_pServerLogger->LogMsg("ERROR : Failed to duplicate descriptor.");
		// 	return;
		// }
		
		// close(0); // Close stdin.
		// close(1); // Close stdout.
		// close(2); // Close stderr.

		// fd = open("/dev/null", O_RDONLY);
		// if(fd != 0)
		// {
		// 	m_pServerLogger->LogMsg("ERROR : Failed to open stdin.");
		// 	return;
		// }

		// fd = open("/dev/null", O_WRONLY|O_CREAT|O_APPEND, 0644);
		// if(fd != 1)
		// {
		// 	ClearTmpFile();

		// 	m_pServerLogger->LogMsg("ERROR : Failed to redirect stdout to /dev/null.");	
		// 	return;
		// }
		// m_iTmpFileDescriptorStdOut = fd;

		// fd = open("/dev/null", O_WRONLY|O_CREAT|O_APPEND, 0644);
		// if(fd != 2)
		// {
		// 	ClearTmpFile();

		// 	m_pServerLogger->LogMsg("ERROR : Failed to redirect stderr to /dev/null.");	
		// 	return;
		// }
		// m_iTmpFileDescriptorStdErr = fd;
/*
		strncpy(m_sTmpStdOutFileName, "/tmp/dcmqrscp_XXXXXX", 80);

		fd = MakeTempFile(m_sTmpStdOutFileName);
		if(fd != 1)
		{
			m_pServerLogger->LogMsg("ERROR : Failed to open stdout.");	
			return;
		}
		m_iTmpFileDescriptorStdOut = fd;
		
		strncpy(m_sTmpStdErrFileName, "/tmp/dcmqrscp_XXXXXX", 80);
		fd = MakeTempFile(m_sTmpStdErrFileName);
		if(fd != 2)
		{
			m_pServerLogger->LogMsg("ERROR : Failed to open stderr.");
			return;		
		}
		m_iTmpFileDescriptorStdErr = fd;*/
	}

	///////////////////////////////////////////////////////////////////////////////////////////////
	//
	int DICOMSession::MakeTempFile(char *pTemplate)
	{
#ifdef HAVE_MKSTEMP
		return mkstemp(pTemplate);
#else /* ! HAVE_MKSTEMP */
		mktemp(pTemplate);
		return open(pTemplate, O_WRONLY|O_CREAT|O_APPEND, 0644);
#endif
	}

	///////////////////////////////////////////////////////////////////////////////////////////////
	//
	void DICOMSession::ClearTmpFile()
	{
		if(m_iTmpFileDescriptorStdOut == 1)
		{
			close(m_iTmpFileDescriptorStdOut);
			m_iTmpFileDescriptorStdOut = -1;
		}

		if(m_iTmpFileDescriptorStdErr == 2)
		{
			close(m_iTmpFileDescriptorStdErr);
			m_iTmpFileDescriptorStdErr = -1;
		}

/*		if(m_iTmpFileDescriptorStdOut == 1)
		{
			close(m_iTmpFileDescriptorStdOut);
			m_iTmpFileDescriptorStdOut = -1;
		}

		if(m_iTmpFileDescriptorStdErr == 2)
		{
			close(m_iTmpFileDescriptorStdErr);
			m_iTmpFileDescriptorStdErr = -1;
		}
		remove(m_sTmpStdOutFileName);
		remove(m_sTmpStdErrFileName);*/
	}
#endif
}
