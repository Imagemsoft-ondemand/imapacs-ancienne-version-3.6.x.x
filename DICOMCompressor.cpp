/////////////////////////////////////////////////////////////////////////////
//	DICOMCompressor.cpp
/////////////////////////////////////////////////////////////////////////////

#ifdef WIN32
#include "stdafx.h"
#endif

#include "DICOMCompressor.h"
#include "./Utils/DICOMUtil.h"

#ifdef WIN32
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
#endif

/////////////////////////////////////////////////////////////////////////////
//
DICOMCompressor::DICOMCompressor()
{
	m_pLastErrorText = NULL;
}

/////////////////////////////////////////////////////////////////////////////
//
DICOMCompressor::~DICOMCompressor()
{
	if(m_pLastErrorText)
	{
		delete [] m_pLastErrorText;
		m_pLastErrorText = NULL;
	}
}

/////////////////////////////////////////////////////////////////////////////
//	One time compressor initialization.
bool DICOMCompressor::Init()
{
	return true;
}

/////////////////////////////////////////////////////////////////////////////
//	Compressor cleanup.
void DICOMCompressor::Shutdown()
{
}

/////////////////////////////////////////////////////////////////////////////
//	Only compress with JPEG lossless process 14 predictor lvl 1.
bool DICOMCompressor::CompressFromDatasetToFile(const char *pFileName, DcmDataset *pDataset)
{
	E_TransferSyntax Xfer = EXS_Unknown;

	//	Set compression mode and transefr syntax.
	DcmXfer opt_oxferSyn(EXS_JPEGProcess14SV1);
	DJ_RPLossless rp_lossless(6,0);

	const DcmRepresentationParameter *rp = &rp_lossless;

	pDataset->chooseRepresentation(EXS_JPEGProcess14SV1, rp);
/*
	if(!pDataset->canWriteXfer(EXS_JPEGProcess14SV1TransferSyntax))
	{
		SetLastErrorText("Can not compress this dicom object.");
		return false;
	}
*/
	DcmFileFormat DICOMFile(pDataset);

	OFCondition cond = DICOMFile.saveFile(pFileName,
		                                  EXS_JPEGProcess14SV1,
										  EET_ExplicitLength,
										  EGL_recalcGL,
										  EPD_noChange,
										  0,
										  0);
	if(cond.bad())
	{
		DICOMFile.clear();

		SetLastErrorText(cond.text());

		return false;
	}

	DICOMFile.clear();

	return true;
}

/////////////////////////////////////////////////////////////////////////////
//
bool DICOMCompressor::UncompressToFile(DcmDataset *pSrcDataset, const char *pDestFile, E_TransferSyntax WantedTrnSyn)
{
	bool bRet            = true;
	OFCondition cond     = EC_Normal;
	DcmDataset *pDataset = NULL;

	if(!pSrcDataset || !pDestFile)
	{
		SetLastErrorText("NULL argument passed to DICOMCompressor::UncompressToFile");
		return false;
	}

	cond = pSrcDataset->chooseRepresentation(WantedTrnSyn, NULL);
	if(cond.bad())
	{
		SetLastErrorText(cond.text());
		bRet = false;
	}
	else if(pSrcDataset->canWriteXfer(WantedTrnSyn))
	{
		DcmFileFormat DICOMFile(pSrcDataset);

		cond = DICOMFile.saveFile(pDestFile,
				                  WantedTrnSyn,
								  EET_ExplicitLength,
								  EGL_recalcGL,
								  EPD_noChange,
								  (Uint32) 0,
								  (Uint32) 0);

		if(cond.bad())
		{
			SetLastErrorText(cond.text());
			bRet = false;
		}
	}
	else
	{
		SetLastErrorText("Unable to compress.");
		bRet = false;
	}
	return bRet;
}

/////////////////////////////////////////////////////////////////////////////
//
bool DICOMCompressor::UncompressToFile(const char *pSrcFile, const char *pDestFile, E_TransferSyntax WantedTrnSyn)
{
	bool bRet            = true;
	OFCondition cond     = EC_Normal;
	DcmDataset *pDataset = NULL;
	DcmFileFormat DICOMFile;

	if(!pSrcFile || !pDestFile)
	{
		SetLastErrorText("NULL argument passed to DICOMCompressor::UncompressToFile");
		return false;
	}

	cond = DICOMFile.loadFile(pSrcFile);
	if(cond.bad())
	{
		SetLastErrorText(cond.text());
		return false;
	}

	DcmXfer original_xfer(DICOMFile.getDataset()->getOriginalXfer());

	pDataset = DICOMFile.getDataset();

	DcmXfer opt_oxferSyn(WantedTrnSyn);

	cond = pDataset->chooseRepresentation(WantedTrnSyn, NULL);
	if(cond.bad())
	{
		SetLastErrorText(cond.text());
		bRet = false;
	}
	else if(pDataset->canWriteXfer(WantedTrnSyn))
	{
		cond = DICOMFile.saveFile(pDestFile,
				                  WantedTrnSyn,
								  EET_ExplicitLength,
								  EGL_recalcGL,
								  EPD_noChange,
								  (Uint32) 0,
								  (Uint32) 0);

		if(cond.bad())
		{
			SetLastErrorText(cond.text());
			bRet = false;
		}
	}
	else
	{
		SetLastErrorText("Unable to compress.");
		bRet = false;
	}
	return bRet;
}

/////////////////////////////////////////////////////////////////////////////
//
bool DICOMCompressor::FixCompressedImage(const char *pFile)
{
	DcmFileFormat dfile;
	unsigned char missing[8] = {0};

	OFCondition cond = dfile.loadFile(pFile);
    if(!cond.good())
	{
		if(strstr(cond.text(),"I/O suspension")!=NULL)
		{
			FILE *fd = fopen(pFile,"ab");
			if(fd)
			{
				missing[0]=254;
				missing[1]=255;
				missing[2]=221;
				missing[3]=224;
				missing[4]=0;
				missing[5]=0;
				missing[6]=0;
				missing[7]=7;

				fwrite(missing,1,8,fd);
				fclose(fd);

				return true;
			}
		}
		else
		{
			return false;
		}
	}
	return false;
}

/////////////////////////////////////////////////////////////////////////////
//
void DICOMCompressor::SetLastErrorText(const char *pText)
{
	if(m_pLastErrorText)
	{
		delete [] m_pLastErrorText;
		m_pLastErrorText = NULL;
	}

	m_pLastErrorText = new char[strlen(pText) + 1];
	if(m_pLastErrorText)
	{
		strcpy(m_pLastErrorText, pText);
	}
}
