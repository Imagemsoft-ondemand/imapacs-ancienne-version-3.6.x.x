///////////////////////////////////////////////////////////////////////////////////////////////////
//	PacsConfig.cpp
///////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef WIN32
#include "stdafx.h"
#endif

#include <iostream>
#include <fstream>
#include <algorithm>

#include "PacsConfig.h"
#include "./Utils/StringBuffer.h"

#ifdef WIN32
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////
//
namespace imapacs
{
	///////////////////////////////////////////////////////////////////////////////////////////////
	//
	bool PacsConfig::ConnectDb()
	{
		bool bRetVal = false;

		m_hCursor = m_Db.Open(m_sDbHost, m_sDbPort, m_sDbSource, m_sDbUser, m_sDbPassword, "./Oracle/");

		if(m_hCursor)
			bRetVal = true;

		m_LastError = m_Db.GetLastError();
		
		return bRetVal;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////
	//
	bool PacsConfig::ReadConfigFromFile(const char* pCfgFile)
	{
		std::string line;
		char* pTmp   = nullptr;
		bool bRetVal = false;

		if(pCfgFile)
		{
			std::ifstream cFile(pCfgFile);

			if(cFile.is_open())
			{			
				while(std::getline(cFile, line))
				{
					line.erase(std::remove_if(line.begin(), line.end(), isspace), line.end());

					if(line[0] == '#' || line.empty())	//	Skip commented and empty lines.
						continue;

					auto delimiterPos = line.find("=");
					auto name         = line.substr(0, delimiterPos);
					auto value        = line.substr(delimiterPos + 1);

					if(strcmp(name.c_str(), "dbhost") == 0)
					{
						strcpy(m_sDbHost, value.c_str());
					}
					else if(strcmp(name.c_str(), "dbport") == 0)
					{
						strcpy(m_sDbPort, value.c_str());
					}
					else if(strcmp(name.c_str(), "dbsource") == 0)
					{
						strcpy(m_sDbSource, value.c_str());
					}
					else if(strcmp(name.c_str(), "dbuser") == 0)
					{
						strcpy(m_sDbUser, value.c_str());
					}
					else if(strcmp(name.c_str(), "dbpassword") == 0)
					{
						strcpy(m_sDbPassword, value.c_str());
					}
					else if(strcmp(name.c_str(), "serveraetitle") == 0)
					{
						strcpy(m_sServerAeTitle, value.c_str());
					}
					else if(strcmp(name.c_str(), "dbpackagename") == 0)
					{
						strcpy(m_sPkgName, value.c_str());
					}
					else if(strcmp(name.c_str(), "logpath") == 0)
					{
						strcpy(m_sLogPath, value.c_str());
					}
					else if(strcmp(name.c_str(), "tmppath") == 0)
					{
						strcpy(m_sTmpPath, value.c_str());
					}
					else if(strcmp(name.c_str(), "port") == 0)
					{
						m_Port = atoi(value.c_str());
					}
					else if(strcmp(name.c_str(), "porttransient") == 0)
					{
						m_TransientPort = atoi(value.c_str());
					}
				}

				bRetVal = true;
			}
			else
				m_ServerLogger.LogMsg("Configuration file not found or unaccessible.");
		}
		return bRetVal;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////
	//
	bool PacsConfig::ReadConfigFromDb()
	{
		bool bRetVal             = false;
		HANDLE hCursor           = NULL;
		PresentationContext *pPc = NULL;
		char sItem[26]           = {0};
		char sValue[129]         = {0};
		char sSql[256]           = {0};

		hCursor = m_Db.GetCursor();
		if(hCursor)
		{
			sprintf(sSql, "SELECT PC_ITEM_CONF, PC_VALUE FROM PACS_CONFIG WHERE PC_AE_TITLE = '%s'", m_sServerAeTitle);

			if(m_Db.SQLExec(hCursor, sSql, "%s%s", sItem, sValue))
			{
				do
				{
					if(strcmp(sItem, "WORKLIST_STATUS") == 0)
					{
						strcpy(m_sWorklistStatus, sValue);
					}
					else if(strcmp(sItem, "IMPLEMENTATION_UID") == 0)
					{
						strcpy(m_sImplementationId, sValue);
					}
					else if(strcmp(sItem, "MAIL_NAME") == 0)
					{
						strcpy(m_sMailSubject, sValue);
						m_Mailer.SetSubject(m_sMailSubject);
					}
					else if(strcmp(sItem, "MAIL_RECIPIENT") == 0)
					{
						m_Mailer.SetMailTo(sValue);
					}			
					else if(strcmp(sItem, "MAIL_SERVER_NAME") == 0)
					{
						m_Mailer.SetMailServer(sValue);
					}
					else if(strcmp(sItem, "MAIL_SERVER_PORT") == 0)
					{
						m_Mailer.SetMailPort(atoi(sValue));
					}
					else if(strcmp(sItem, "SEND_MAIL_ON_ERRORS") == 0)
					{
						if(sValue[0] == '0')
							m_bSendMail = false;
					}
					else if(strcmp(sItem, "CHARACTER_SET") == 0)
					{
						strncpy(m_sCharacterSet, sValue, 64);
						m_sCharacterSet[64] = 0x00;
					}
					else if(strcmp(sItem, "PROFILING") == 0)
					{
						if((sValue[0] == '1') || (sValue[0] == 'y') || (sValue[0] == 'Y'))
							m_bProfiling = true;
						else
							m_bProfiling = false;
					}
				}while(m_Db.NextRecord(hCursor));

				//	This is always the same value.
				m_Mailer.SetMailFrom("imapacs@imagemsoft.com");

				bRetVal = true;
			}
			else
			{
				if(m_Db.GetLastOciErrorCode(hCursor) == 0)
				{
					m_ServerLogger.LogMsg("ERROR : No configuration found in database for %s", m_sServerAeTitle);
				}
				else
				{
					m_ServerLogger.LogMsg("ERROR %s", m_Db.GetLastOciErrorText(hCursor));
				}
			}
			m_Db.DeleteCursor(hCursor);
			hCursor = NULL;
		}
		else
		{
			m_ServerLogger.LogMsg("ERROR : Failed to get cursor in DataInterface::LoadConfigFromDb");
		}
		return bRetVal;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////
	//
	bool PacsConfig::LoadModalityConfigurations(const char *pAeTitle)
	{
		bool bRetVal     = true;
		char sValue[129] = {0};
		HANDLE hCursor   = NULL;
		StringBuffer sql;

		hCursor = m_Db.GetCursor();
		if(hCursor)
		{
			//	Get the debug flag for the specified AE title.
			sql.Format("SELECT AT_DEBUG FROM AE_TITLE WHERE AT_AE_TITLE = '%s'", pAeTitle);
			if(m_Db.SQLExec(hCursor, (char *)sql, "%s", sValue))
			{
				if((sValue[0] == 'y') || (sValue[0] == 'Y') || (sValue[0] == '1'))
				{
					m_bModalityDebug = true;
				}
				else
				{
					m_bModalityDebug = false;
				}
			}
			else
			{
				if(m_Db.GetLastOciErrorCode(hCursor) != 0)
				{
					m_ModalityLogger.LogMsg("ERROR : %s", m_Db.GetLastOciErrorText(hCursor));
					m_ModalityLogger.LogMsg("SQL   : %s", (char *)sql);

					m_Mailer.AddLine("ERROR : %s", m_Db.GetLastOciErrorText(hCursor));
					m_Mailer.AddLine("SQL   : %s", (char *)sql);

					bRetVal = false;
				}
			}

			//	Get the configuration for accents removal for
			//	the specified AE title.
			sValue[0] = 0x00;
			sql.Format("SELECT AT_DISABLE_ACCENTS FROM AE_TITLE WHERE AT_AE_TITLE = '%s'", pAeTitle);
			if(m_Db.SQLExec(hCursor, (char *)sql, "%s", sValue))
			{
				if((sValue[0] == 'y') || (sValue[0] == 'Y') || (sValue[0] == '1'))
				{
					m_bRemoveAccents = true;
				}
				else
				{
					m_bRemoveAccents = false;
				}
			}
			else
			{
				if(m_Db.GetLastOciErrorCode(hCursor) != 0)
				{
					m_ModalityLogger.LogMsg("ERROR : %s", m_Db.GetLastOciErrorText(hCursor));
					m_ModalityLogger.LogMsg("SQL   : %s", (char *)sql);

					m_Mailer.AddLine("ERROR : %s", m_Db.GetLastOciErrorText(hCursor));
					m_Mailer.AddLine("SQL   : %s", (char *)sql);

					bRetVal = false;
				}
			}
			m_Db.DeleteCursor(hCursor);
			hCursor = NULL;
		}
		else
		{
			m_ModalityLogger.LogMsg("ERROR : Failed to get cursor in PacsConfig::LoadModalityConfigurations");
			m_Mailer.AddLine("ERROR : Failed to get cursor in PacsConfig::LoadModalityConfigurations");
		}
		return bRetVal;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////
	//
	void PacsConfig::CloseDb()
	{
		if(m_hCursor)
		{
			m_Db.DeleteCursor(m_hCursor);
			m_hCursor = NULL;
		}
		m_Db.Close();
	}

	///////////////////////////////////////////////////////////////////////////////////////////////
	//
	bool PacsConfig::GetMFS(const char *pStudyInstanceUID, char *pMfs)
	{
		bool bRetVal   = false;
		HANDLE hCursor = NULL;
		StringBuffer sql;

		hCursor = m_Db.GetCursor();
		if(hCursor)
		{
			sql.Format("SELECT %s.GET_MOUNT_POINT('%s') FROM DUAL", m_sPkgName,
																    pStudyInstanceUID);

			if(m_Db.SQLExec(hCursor, sql.GetBuffer(), "%s", pMfs))
			{
				bRetVal = true;
			}
			else
			{
				m_ModalityLogger.LogMsg("%s", m_Db.GetLastOciErrorText(hCursor));
				m_ModalityLogger.LogMsg("SQL :  %s", sql.GetBuffer());

				m_Mailer.AddLine("%s", m_Db.GetLastOciErrorText(hCursor));
				m_Mailer.AddLine("SQL:  %s", sql.GetBuffer());
			}
			m_Db.DeleteCursor(hCursor);
			hCursor = NULL;
		}
		else
		{
			m_ModalityLogger.LogMsg("ERROR : Failed to get cursor in PacsConfig::GetMFS");
			m_Mailer.AddLine("Failed to get cursor in PacsConfig::GetMFS");
		}
		return bRetVal;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////
	//
	bool PacsConfig::GetQaRoutingInfos(const char *pAeTitle,
		                               double *pScaleFactor,
									   char *pIp,
									   unsigned short *pPort,
									   char *pDestinationAeTitle)
	{
		bool bRetVal          = false;
		bool bSqlRet          = false;
		HANDLE hCursor        = NULL;
		char sPort[17]        = {0};
		char sScaleFactor[33] = {0};
		StringBuffer sql;

		hCursor = m_Db.GetCursor();
		if(hCursor)
		{
			//////////////////////////////////////////////////////////////////////////
			//	NOTE : DD_AR_DESTINATION is the calling AE title we receive the
			//	       transaction from.
			//////////////////////////////////////////////////////////////////////////
			sql.Format("SELECT DD_SCALE_FACTOR, DD_REMOTE_HOST, DD_REMOTE_PORT, DD_REMOTE_AE_TITLE	 FROM DESTINATION_DETAILS" \
				               " WHERE (DD_AR_DESTINATION = '%s') AND (DD_UPLOAD = 'F')", pAeTitle);

			bSqlRet = m_Db.SQLExec(hCursor, sql.GetBuffer(), "%s%s%s%s", sScaleFactor,
				                                                         pIp,
																		 sPort,
																		 pDestinationAeTitle);
			if(bSqlRet)
			{
				(*pScaleFactor) = atof(sScaleFactor);
				(*pPort)        = atoi(sPort);

				bRetVal = true;
			}
			m_Db.DeleteCursor(hCursor);
			hCursor = NULL;
		}
		return bRetVal;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////
	//
	void PacsConfig::ParseArgs(int iArgc, char **pArgv)
	{
		for(int i = 1; i < iArgc; ++i)
		{
			//	Verify  if we are in transient mode.
			if(strcmp(pArgv[i],"-t")==0)
			{
				m_bTransient = true;
			}
			else if((strcmp(pArgv[i], "-version") == 0) || (strcmp(pArgv[i], "--v") == 0))
			{
				m_bDisplayAbout = true;
			}
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////////
	//
	bool PacsConfig::InsertEvent(const char *pEventType,
		                         const char *pStudyInstanceUID,
								 const char *pApplication,
								 const char *pSource,
								 const char *pOperator,
								 const char *pPatId,
								 const char *pCOIType,
								 const char *pCOIInstanceUID,
								 const char *pCOISopClassDesc,
								 const char *pArgv1,
								 const char *pArgv2,
								 const char *pArgv3,
								 const char *pArgv4,
								 const char *pArgv5)
	{
		bool bRetVal   = false;
		HANDLE hCursor = NULL;
		StringBuffer sql;

		char sEventType[33]         = {0};
		char sStudyInstanceUID[67]	= {0};
		char sApplication[19]		= {0};
		char sSource[19]			= {0};
		char sOperator[67]			= {0};
		char sPatID[67]				= {0};
		char sCOIType[23]			= {0};
		char sCOIInstanceUID[67]	= {0};
		char sCOISopClassDesc[67]	= {0};
		char sArgv1[67]				= {0};
		char sArgv2[131]			= {0};
		char sArgv3[67]				= {0};
		char sArgv4[67]				= {0};
		char sArgv5[67]				= {0};

		if(pEventType)
			sprintf(sEventType, "'%s'", pEventType);
		else
			strcat(sEventType, "NULL");

		if(pStudyInstanceUID)
			sprintf(sStudyInstanceUID, "'%s'", pStudyInstanceUID);
		else
			strcat(sStudyInstanceUID, "NULL");

		if(pApplication)
			sprintf(sApplication, "'%s'", pApplication);
		else
			strcat(sApplication, "NULL");

		if(pSource)
			sprintf(sSource, "'%s'", pSource);
		else
			strcat(sSource, "NULL");

		if(pOperator)
			sprintf(sOperator, "'%s'", pOperator);
		else
			strcat(sOperator, "NULL");

		if(pPatId)
			sprintf(sPatID, "'%s'", pPatId);
		else
			strcat(sPatID, "NULL");

		if(pCOIType)
			sprintf(sCOIType, "'%s'", pCOIType);
		else
			strcat(sCOIType, "NULL");

		if(pCOIInstanceUID)
			sprintf(sCOIInstanceUID, "'%s'", pCOIInstanceUID);
		else
			strcat(sCOIInstanceUID, "NULL");

		if(pCOISopClassDesc)
			sprintf(sCOISopClassDesc, "'%s'", pCOISopClassDesc);
		else
			strcat(sCOISopClassDesc, "NULL");

		if(pArgv1)
			sprintf(sArgv1, "'%s'", pArgv1);
		else
			strcat(sArgv1, "NULL");

		if(pArgv2)
			sprintf(sArgv2, "'%s'", pArgv2);
		else
			strcat(sArgv2, "NULL");

		if(pArgv3)
			sprintf(sArgv3, "'%s'", pArgv3);
		else
			strcat(sArgv3, "NULL");

		if(pArgv4)
			sprintf(sArgv4, "'%s'", pArgv4);
		else
			strcat(sArgv4, "NULL");

		if(pArgv5)
			sprintf(sArgv5, "'%s'", pArgv5);
		else
			strcat(sArgv5, "NULL");

		hCursor = m_Db.GetCursor();
		if(hCursor)
		{
			sql.Format("CALL %s.INSERT_EVENT(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", 
					   m_sPkgName,
					   sEventType,
					   sStudyInstanceUID, 
					   sApplication,
					   sSource,
					   sOperator,
					   sPatID,
					   sCOIType,
					   sCOIInstanceUID,
					   sCOISopClassDesc,
					   sArgv1,
					   sArgv2,
					   sArgv3,
					   sArgv4,
					   sArgv5);

			if(m_Db.SQLExec(hCursor, sql.GetBuffer()))
			{
				bRetVal = true;
			}
			else
			{
				m_ModalityLogger.LogMsg("ERROR : %s", m_Db.GetLastOciErrorText(hCursor));
				m_ModalityLogger.LogMsg("SQL :  %s", sql.GetBuffer());

				m_Mailer.AddLine("%s", m_Db.GetLastOciErrorText(hCursor));
				m_Mailer.AddLine("SQL :  %s", sql.GetBuffer());
			}
			m_Db.DeleteCursor(hCursor);
			hCursor = NULL;
		}
		else
		{
			m_ModalityLogger.LogMsg("ERROR : Failed to get cursor in DataInterface::InsertEvent.");
			m_Mailer.AddLine("ERROR : Failed to get cursor in DataInterface::InsertEvent.");
		}
		return bRetVal;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////
	//
	int PacsConfig::SaveCompressed(const char *pAeTitle)
	{
		int iRetVal     = 0;
		char sResult[5] = {0};
		HANDLE hCursor  = NULL;
		StringBuffer sql;

		hCursor = m_Db.GetCursor();
		if(hCursor)
		{
			sql.Format("SELECT %s.MUST_COMPRESS_OBJECT('%s') FROM DUAL", m_sPkgName, pAeTitle);
			if(m_Db.SQLExec(hCursor, sql.GetBuffer(), "%s", sResult))
			{
				iRetVal = atoi(sResult);
			}
			else
			{
				m_ModalityLogger.LogMsg("%s", m_Db.GetLastOciErrorText(hCursor));
				m_ModalityLogger.LogMsg("SQL :  %s", sql.GetBuffer());

				m_Mailer.AddLine("%s", m_Db.GetLastOciErrorText(hCursor));
				m_Mailer.AddLine("SQL :  %s", sql.GetBuffer());
			}
			m_Db.DeleteCursor(hCursor);
			hCursor = NULL;
		}
		else
		{
			m_ModalityLogger.LogMsg("ERROR : Failed to get cursor in DataInterface::SaveCompressed.");
			m_Mailer.AddLine("Failed to get cursor in DataInterface::SaveCompressed.");
		}
		return iRetVal;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////
	//
	bool PacsConfig::GetMoveDestinationAddress(const char *pMoveToAeTitle,
									           char *pMoveToIP,
									           unsigned short *pMoveToPort)
	{
		bool bRetVal       = false;
		HANDLE hCursor     = NULL;
		char sPort[15]     = {0};
		unsigned int iTime = 0;
		StringBuffer sql;
		DBGTool dbg;

		hCursor = m_Db.GetCursor();
		if(hCursor)
		{
			if(m_bProfiling)
			{
				m_ServerLogger.LogMsg(BEGIN_TRANSACTION_LINE);
				m_ServerLogger.LogMsg("Executing sql to get destination parameters.");

				dbg.StartTimer();
			}

			sql.Format("SELECT AT_ADDR_IP, AT_PORT FROM AE_TITLE WHERE AT_AE_TITLE = '%s'", pMoveToAeTitle);
			if(m_Db.SQLExec(hCursor, sql.GetBuffer(), "%s%s", pMoveToIP, sPort))
			{
				(*pMoveToPort) = atoi(sPort);
				bRetVal = true;
			}
			else
			{
				m_ModalityLogger.LogMsg("ERROR : %s", m_Db.GetLastOciErrorText(hCursor));
				m_ModalityLogger.LogMsg("SQL :  %s", sql.GetBuffer());

				m_Mailer.AddLine("%s", m_Db.GetLastOciErrorText(hCursor));
				m_Mailer.AddLine("SQL :  %s", sql.GetBuffer());
			}
			m_Db.DeleteCursor(hCursor);
			hCursor = NULL;

			if(m_bProfiling)
			{
				dbg.StopTimer();

				iTime = dbg.GetIntervalInMili();
				if(iTime > 999)
				{
					m_ServerLogger.LogMsg("Time   :   %f seconds", ((float)iTime/1000.0f));
				}
				else
				{
					m_ServerLogger.LogMsg("Time   :   %d miliseconds", iTime);
				}
				m_ServerLogger.LogMsg("Sql Executed.");
				m_ServerLogger.LogMsg(END_TRANSACTION_LINE);
			}
		}
		else
		{
			m_ModalityLogger.LogMsg("ERROR : Failed to get cursor in PacsConfig::GetMoveDestinationAddress.");
			m_Mailer.AddLine("NULL move port passed to PacsConfig::GetMoveDestinationAddress");
		}
		return bRetVal;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////
	//
	bool PacsConfig::GetCommitAddressAndPort(const char *pCommitAeTitle,
								             char *pCommitIP,
							                 unsigned short *pCommitPort)
	{
		bool bRetVal = false;
		HANDLE hCursor     = NULL;
		char sPort[15]     = {0};
		unsigned int iTime = 0;
		StringBuffer sql;
		DBGTool dbg;

		hCursor = m_Db.GetCursor();
		if(hCursor)
		{
			if(m_bProfiling)
			{
				m_ServerLogger.LogMsg(BEGIN_TRANSACTION_LINE);
				m_ServerLogger.LogMsg("Executing sql to get destination parameters.");

				dbg.StartTimer();
			}

			sql.Format("SELECT AT_ADDR_IP, AT_PORT_COMMIT FROM AE_TITLE WHERE AT_AE_TITLE = '%s'", pCommitAeTitle);
			if(m_Db.SQLExec(hCursor, sql.GetBuffer(), "%s%s", pCommitIP, sPort))
			{
				if(sPort[0])
				{
					(*pCommitPort) = atoi(sPort);
					bRetVal = true;
				}
			}
			else
			{
				m_ModalityLogger.LogMsg("ERROR : %s", m_Db.GetLastOciErrorText(hCursor));
				m_ModalityLogger.LogMsg("SQL :  %s", sql.GetBuffer());

				m_Mailer.AddLine("%s", m_Db.GetLastOciErrorText(hCursor));
				m_Mailer.AddLine("SQL :  %s", sql.GetBuffer());
			}
			m_Db.DeleteCursor(hCursor);
			hCursor = NULL;

			if(m_bProfiling)
			{
				dbg.StopTimer();

				iTime = dbg.GetIntervalInMili();
				if(iTime > 999)
				{
					m_ServerLogger.LogMsg("Time   :   %f seconds", ((float)iTime/1000.0f));
				}
				else
				{
					m_ServerLogger.LogMsg("Time   :   %d miliseconds", iTime);
				}
				m_ServerLogger.LogMsg("Sql Executed.");
				m_ServerLogger.LogMsg(END_TRANSACTION_LINE);
			}
			m_Db.DeleteCursor(hCursor);
			hCursor = NULL;
		}
		else
		{
			m_ModalityLogger.LogMsg("ERROR : Failed to get cursor in PacsConfig::GetMoveDestinationAddress.");
			m_Mailer.AddLine("NULL move port passed to PacsConfig::GetMoveDestinationAddress");
		}
		return bRetVal;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////
	//
	bool PacsConfig::FindObjectLocation(const char *pSOPInstanceUID, char *pObjectLocation)
	{
		bool bRetVal          = false;
		HANDLE hCursor        = NULL;
		char sMountPoint[129] = {0};
		unsigned int iTime    = 0;
		StringBuffer sql;
		DBGTool dbg;

		hCursor = m_Db.GetCursor();
		if(hCursor)
		{
			if(m_bProfiling)
			{
				m_ServerLogger.LogMsg(BEGIN_TRANSACTION_LINE);
				m_ServerLogger.LogMsg("Executing sql : GET_OBJECT_PATH");

				dbg.StartTimer();
			}

			sql.Format("SELECT %s.GET_OBJECT_PATH('%s') FROM DUAL", m_sPkgName, pSOPInstanceUID);
			if(m_Db.SQLExec(hCursor, sql.GetBuffer(), "%s", pObjectLocation))
			{
				bRetVal = true;
			}
			else
			{
				if(m_Db.GetLastOciErrorCode(hCursor) != 0)
				{
					m_ModalityLogger.LogMsg("ERROR : %s", m_Db.GetLastOciErrorText(hCursor));
					m_Mailer.AddLine("SQL :  %s", sql.GetBuffer());
				}
			}
			m_Db.DeleteCursor(hCursor);
			hCursor = NULL;

			if(m_bProfiling)
			{
				dbg.StopTimer();

				iTime = dbg.GetIntervalInMili();
				if(iTime > 999)
				{
					m_ServerLogger.LogMsg("Time   :   %f seconds", ((float)iTime/1000.0f));
				}
				else
				{
					m_ServerLogger.LogMsg("Time   :   %d miliseconds", iTime);
				}
				m_ServerLogger.LogMsg("Sql Executed.");
				m_ServerLogger.LogMsg(END_TRANSACTION_LINE);
			}
		}
		else
		{
			m_ModalityLogger.LogMsg("ERROR : Failed to get cursor in DataInterface::FindObjectLocation.");
			m_Mailer.AddLine("Failed to get cursor in DataInterface::FindObjectLocation");
		}
		return bRetVal;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////
	//
	const char **PacsConfig::GetTransferList(const char *pSOPClassUID, int *iCount, bool bAccepted)
	{
		bool bRetVal          = false;
		int i                 = 0;
		char sSql[513]        = {0};
		char sTrnSyn[65]      = {0};
		char *pValue          = NULL;
		const char **pRetList = NULL;
		HANDLE hCursor        = NULL;
		Queue <char *> aTrnSynList;

		hCursor = m_Db.GetCursor();
		if(hCursor)
		{
			if(bAccepted)
			{
				sprintf(sSql, "SELECT ATS_TRANSFERT_SYNTAX " \
							  "FROM ACCEPTED_TRANSFERT_SYNTAX " \
							  "WHERE ATS_SOP_CLASS_UID = '%s' AND ATS_ORDER_ACCEPTED != '0' ORDER BY ATS_ORDER_ACCEPTED",
							  pSOPClassUID);
			}
			else
			{
				sprintf(sSql, "SELECT ATS_TRANSFERT_SYNTAX " \
							  "FROM ACCEPTED_TRANSFERT_SYNTAX " \
							  "WHERE ATS_SOP_CLASS_UID = '%s' AND ATS_ORDER_PROPOSED != '0'  ORDER BY ATS_ORDER_PROPOSED	",
							  pSOPClassUID);
			}

			if(m_Db.SQLExec(hCursor, sSql, "%s", sTrnSyn))
			{
				do
				{
					pValue = new char[65];
					if(pValue)
					{
						strcpy(pValue, sTrnSyn);
						aTrnSynList.Add(pValue);
					}
				}while(m_Db.NextRecord(hCursor));

				(*iCount) = (int)aTrnSynList.GetSize();
				pRetList = new const char*[(*iCount)];
				if(pRetList)
				{
					aTrnSynList.SetAtFirst();
					while(aTrnSynList.GetCurrent(pValue))
					{
						pRetList[i] = pValue;
						aTrnSynList.Next();
						++i;
					}
					aTrnSynList.Clear();

					bRetVal = true;
				}
				else
				{
					while(aTrnSynList.Pop(pValue))
					{
						delete [] pValue;
					}
					pValue = NULL;
				}
			}
			m_Db.DeleteCursor(hCursor);
			hCursor = NULL;
		}
		else
		{
			m_ServerLogger.LogMsg("ERROR : Failed to get cursor in PacsConfig::GetTransferList");
			m_Mailer.AddLine("ERROR : Failed to get cursor in PacsConfig::GetTransferList");
		}
		return pRetList;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////
	///
	const char **PacsConfig::GetSOPList(int *iSOPCount, bool bStorage)
	{
		char sSql[513]        = {0};
		char sStorage[2]      = {0};
		char sValue[65]       = {0};
		int i                 = 0;
		char *pValue          = NULL;
		HANDLE hCursor        = NULL;
		const char **pRetList = NULL;
		Queue <char *> aList;

		hCursor = m_Db.GetCursor();
		if(hCursor)
		{
			if(bStorage)
			{
				sStorage[0] = '1';
			}
			else
			{
				sStorage[0] = '0';
			}

			sprintf(sSql, "SELECT ASC_SOP_CLASS " \
				          "FROM ACCEPTED_SOP_CLASS " \
						  "WHERE ASC_IS_STORAGE = '%s'",
						  sStorage);

			if(m_Db.SQLExec(hCursor, sSql, "%s", sValue))
			{
				do
				{
					pValue = new char[65];
					if(pValue)
					{
						strcpy(pValue, sValue);
						aList.Add(pValue);
					}
				}while(m_Db.NextRecord(hCursor));

				(*iSOPCount) = (int)aList.GetSize();
				pRetList = new const char*[(*iSOPCount)];
				if(pRetList)
				{
					aList.SetAtFirst();
					while(aList.GetCurrent(pValue))
					{
						pRetList[i] = pValue;
						aList.Next();
						++i;
					}
					aList.Clear();
				}
				else
				{
					while(aList.Pop(pValue))
					{
						delete [] pValue;
					}
					pValue = NULL;
				}
			}
			else
			{
				if(m_Db.GetLastOciErrorCode(hCursor) == 0)
				{
					m_ServerLogger.LogMsg("ERROR : No non-storage SOP class was found.");
					m_Mailer.AddLine("ERROR : No non-storage SOP class was found.");
				}
				else
				{
					m_ServerLogger.LogMsg("ERROR : %s", m_Db.GetLastOciErrorText(hCursor));
					m_ServerLogger.LogMsg("SQL   : %s", sSql);

					m_Mailer.AddLine("ERROR : %s", m_Db.GetLastOciErrorText(hCursor));
					m_Mailer.AddLine("SQL   : %s", sSql);
				}
			}
			m_Db.DeleteCursor(hCursor);
			hCursor = NULL;
		}
		else
		{
			m_ServerLogger.LogMsg("ERROR : Failed to get cursor in PacsConfig::GetStorageSOP");
			m_Mailer.AddLine("ERROR : Failed to get cursor in PacsConfig::GetStorageSOP");
		}
		return pRetList;
	}

	///
	///
	void PacsConfig::LogConfiguration(Log* logger)
	{
		logger->LogMsg("-------------------------------------------------");
		logger->LogMsg("                 Configuration");
		logger->LogMsg("-------------------------------------------------");

		logger->LogMsg("dbhost        : %s", m_sDbHost);
		logger->LogMsg("dbport        : %s", m_sDbPort);
		logger->LogMsg("dbsource      : %s", m_sDbSource);
		logger->LogMsg("dbuser        : %s", m_sDbUser);
		logger->LogMsg("dbpassword    : %s", m_sDbPassword);
		logger->LogMsg("serveraetitle : %s", m_sServerAeTitle);
		logger->LogMsg("logpath       : %s", m_sLogPath);
		logger->LogMsg("tmppath       : %s", m_sTmpPath);
		logger->LogMsg("port          : %d", m_Port);
		logger->LogMsg("porttransient : %d", m_TransientPort);

		logger->LogMsg("-------------------------------------------------");
	}
}
