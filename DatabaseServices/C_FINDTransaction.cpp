/////////////////////////////////////////////////////////////////////////////
//	C_FINDTransaction.cpp
/////////////////////////////////////////////////////////////////////////////

#include "C_FINDTransaction.h"

/////////////////////////////////////////////////////////////////////////////
//
namespace imapacs
{
	/////////////////////////////////////////////////////////////////////////
	//
	bool C_FINDTransaction::Init(T_ASC_Association *pAssociation, 
		                               T_DIMSE_Message *pMsg, 
									   T_ASC_PresentationContextID PresID, 
									   PacsConfig *pCfg)
	{
		bool bRetVal = false;

		memcpy(&m_CurrentReq, pMsg, sizeof(T_DIMSE_Message));
		memset(&m_CurrentRsp, 0, sizeof(T_DIMSE_Message));

		m_CurrentRsp.CommandField                           = DIMSE_C_GET_RSP;
		m_CurrentRsp.msg.CFindRSP.DataSetType               = DIMSE_DATASET_NULL;	//	Assuming
		m_CurrentRsp.msg.CFindRSP.DimseStatus               = STATUS_Success;		//	Assuming.
		m_CurrentRsp.msg.CFindRSP.MessageIDBeingRespondedTo = m_CurrentReq.msg.CFindRQ.MessageID;

		strcpy(m_CurrentRsp.msg.CFindRSP.AffectedSOPClassUID, m_CurrentReq.msg.CFindRQ.AffectedSOPClassUID);

		m_pAssociation  = pAssociation;
		m_CurrentPresID = PresID;

		m_pCfg = pCfg;

		//	For convenience.
		m_pServerLogger   = pCfg->GetServerLogger();
		m_pModalityLogger = pCfg->GetModalityLogger();
		m_pMailer         = pCfg->GetMailer();

		m_pServerLogger->LogMsg("[%s] - C-FIND request.", pAssociation->params->DULparams.callingAPTitle);
		m_pModalityLogger->LogMsg("C-FIND request.");
		m_pMailer->AddLine("C-FIND request.");

		m_DataService.SetConfig(pCfg);

		bRetVal = true;

		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	bool C_FINDTransaction::ProcessTransaction()
	{
		OFCondition cond     = EC_Normal;
		bool bRetVal         = true;
		DcmDataset *pStatus  = NULL;
		DcmDataset *pRequest = NULL;

		/////////////////////////////////////////////////////////////////////
		//	Read request dataset.
		cond = DIMSE_receiveDataSetInMemory(m_pAssociation,
											DIMSE_BLOCKING,
											0,
											&m_CurrentPresID,
											&pRequest,
											NULL,
											NULL);
		if(cond.good())
		{
			/////////////////////////////////////////////////////////////////////
			//	What kind of C-FIND are we dealing with ?
			if(strcmp(m_CurrentReq.msg.CFindRQ.AffectedSOPClassUID, UID_FINDPatientRootQueryRetrieveInformationModel) == 0)
			{
				m_pModalityLogger->LogMsg("[%s] - C-FIND Patient Root.", m_pAssociation->params->DULparams.callingAPTitle);
				m_pModalityLogger->LogMsg("C-FIND Patient Root.");
				m_pMailer->AddLine("C-FIND Patient Root.");

				m_pCfg->InsertEvent("C-FIND (PATIENT)", 
									NULL, 
									"IMAPACS",
									m_pAssociation->params->DULparams.callingAPTitle, 
									NULL,
									NULL,
									NULL,
									NULL, 
									NULL, 
									NULL, 
									NULL, 
									NULL, 
									NULL, 
									NULL);

				bRetVal = ProcessFindQuery(pRequest, PATIENT_ROOT);
			}
			else if(strcmp(m_CurrentReq.msg.CFindRQ.AffectedSOPClassUID, UID_FINDStudyRootQueryRetrieveInformationModel) == 0)
			{
				m_pModalityLogger->LogMsg("[%s] - C-FIND Study Root.", m_pAssociation->params->DULparams.callingAPTitle);
				m_pModalityLogger->LogMsg("C-FIND Study Root.");
				m_pMailer->AddLine("C-FIND Study Root.");

				m_pCfg->InsertEvent("C-FIND (STUDY)", 
									NULL,
									"IMAPACS",
									m_pAssociation->params->DULparams.callingAPTitle,
									NULL,
									NULL,
									NULL,
									NULL,
									NULL,
									NULL,
									NULL,
									NULL,
									NULL,
									NULL);

				bRetVal = ProcessFindQuery(pRequest, STUDY_ROOT);
			}
			else if(strcmp(m_CurrentReq.msg.CFindRQ.AffectedSOPClassUID, UID_FINDPatientStudyOnlyQueryRetrieveInformationModel) == 0)
			{
				m_pModalityLogger->LogMsg("[%s] - C-FIND Patient-Study Root.", m_pAssociation->params->DULparams.callingAPTitle);
				m_pModalityLogger->LogMsg("C-FIND Patient-Study Root.");
				m_pMailer->AddLine("C-FIND Patient-Study Root.");

				m_pCfg->InsertEvent("C-FIND (PATIENT_STUDY)", 
									NULL,
									"IMAPACS",
									m_pAssociation->params->DULparams.callingAPTitle,
									NULL,
									NULL,
									NULL,
									NULL,
									NULL,
									NULL,
									NULL,
									NULL,
									NULL,
									NULL);

				bRetVal = ProcessFindQuery(pRequest, PATIENT_STUDY_ROOT);
			}
			else if(strcmp(m_CurrentReq.msg.CFindRQ.AffectedSOPClassUID, UID_FINDModalityWorklistInformationModel) == 0)
			{
				m_pServerLogger->LogMsg("[%s] - C-FIND Worklist.", m_pAssociation->params->DULparams.callingAPTitle);
				m_pModalityLogger->LogMsg("C-FIND Worklist.");

				m_pMailer->AddLine("C-FIND Worklist.");

				m_pCfg->InsertEvent("C-FIND WORKLIST", 
									NULL, 
									"IMAPACS",
									m_pAssociation->params->DULparams.callingAPTitle, 
									NULL, 
									NULL, 
									NULL, 
									NULL, 
									NULL, 
									NULL, 
									NULL, 
									NULL, 
									NULL, 
									NULL);

				bRetVal = ProcessWorklist(pRequest);
			}
			else if(strcmp(m_CurrentReq.msg.CFindRQ.AffectedSOPClassUID, UID_FINDGeneralPurposeWorklistInformationModel) == 0)
			{
				m_pServerLogger->LogMsg("[%s] - C-FIND General purpose worklist.", m_pAssociation->params->DULparams.callingAPTitle);
				m_pModalityLogger->LogMsg("C-FIND General purpose worklist.");

				m_pMailer->AddLine("C-FIND General purpose worklist.");

				m_pCfg->InsertEvent("C-FIND GPWL", 
									NULL, 
									"IMAPACS",
									m_pAssociation->params->DULparams.callingAPTitle, 
									NULL, 
									NULL, 
									NULL, 
									NULL, 
									NULL, 
									NULL, 
									NULL, 
									NULL, 
									NULL, 
									NULL);

				bRetVal = ProcessGPWorklist(pRequest);
			}
			else
			{
				m_CurrentRsp.CommandField                           = DIMSE_C_FIND_RSP;
				m_CurrentRsp.msg.CFindRSP.MessageIDBeingRespondedTo = m_CurrentReq.msg.CFindRQ.MessageID;
				m_CurrentRsp.msg.CFindRSP.DimseStatus               = STATUS_FIND_Refused_SOPClassNotSupported;
				m_CurrentRsp.msg.CFindRSP.DataSetType               = DIMSE_DATASET_NULL;

				strcpy(m_CurrentRsp.msg.CFindRSP.AffectedSOPClassUID, m_CurrentReq.msg.CFindRQ.AffectedSOPClassUID);

				m_pServerLogger->LogMsg("[%s] - Unsupported or unknown Root query uid : %s", 
					                    m_pAssociation->params->DULparams.callingAPTitle, 
										m_CurrentReq.msg.CFindRQ.AffectedSOPClassUID);

				m_pModalityLogger->LogMsg("Unsupported or unknown Root query uid : %s", m_CurrentReq.msg.CFindRQ.AffectedSOPClassUID);
				m_pMailer->AddLine("Unsupported or unknown Root query uid : [%s]", m_CurrentReq.msg.CFindRQ.AffectedSOPClassUID);

				cond = DIMSE_sendMessageUsingMemoryData(m_pAssociation, m_CurrentPresID, &m_CurrentRsp, pStatus, NULL, NULL, NULL);
				if(cond.bad())
				{
					m_pModalityLogger->LogMsg("ERROR : %s", cond.text());
					m_pModalityLogger->LogMsg("        Could not send C-FIND responce.");

					m_pMailer->AddLine("%s", cond.text());
					m_pMailer->AddLine("Could not send C-FIND responce.");
				}
			}
		}
		else
		{
			m_pModalityLogger->LogMsg("ERROR : %s", cond.text());
			m_pModalityLogger->LogMsg("Could not receive C-FIND dataset.");

			m_pMailer->AddLine("ERROR : %s", cond.text());
			m_pMailer->AddLine("Could not receive C-FIND dataset.");
		}

		DICOMUtil::ClearDataset(&pRequest);
		DICOMUtil::ClearDataset(&pStatus);

		return true;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	void C_FINDTransaction::Shutdown()
	{
	}

	/////////////////////////////////////////////////////////////////////////
	//	Process a C-FIND query based on the root level.
	bool C_FINDTransaction::ProcessFindQuery(DcmDataset *pRequest, int iRlevel)
	{
		bool bRetVal          = true;
		bool bCancelled       = false;
		int iResultCount      = 0;
		QUERY_STATUS QueryRet = QS_OK_NO_RESULT;

		/////////////////////////////////////////////////////////////////////////
		//	dcmtk variables.
		OFCondition cond      = EC_Normal;
		DcmDataset *pResult   = NULL;
		DcmDataset *pStatus   = NULL;

		if(m_pCfg->IsVerbose())
		{
			m_pModalityLogger->LogMsg("--ProcessFindQuery()");
		}

		/////////////////////////////////////////////////////////////////////////
		//	Query variables.
		FindData FindRequestData;

		m_CurrentRsp.CommandField                           = DIMSE_C_FIND_RSP;
		m_CurrentRsp.msg.CFindRSP.MessageIDBeingRespondedTo = m_CurrentReq.msg.CFindRQ.MessageID;
		m_CurrentRsp.msg.CFindRSP.DimseStatus               = STATUS_Pending;
		m_CurrentRsp.msg.CFindRSP.opts						= O_FIND_AFFECTEDSOPCLASSUID;

		strcpy(m_CurrentRsp.msg.CFindRSP.AffectedSOPClassUID, m_CurrentReq.msg.CFindRQ.AffectedSOPClassUID);

		if(ExtractFindData(pRequest, &FindRequestData))
		{
			QueryRet = m_DataService.StartFind(&FindRequestData, iRlevel);
			if(QueryRet == QS_OK_RESULT)
			{
				/////////////////////////////////////////////////////////////////
				//	We have results, send them.
				do
				{
					++iResultCount;

					/////////////////////////////////////////////////////////////
					//	Did we receive a cancel request ?
					cond = DIMSE_checkForCancelRQ(m_pAssociation, m_CurrentPresID, m_CurrentReq.msg.CMoveRQ.MessageID);
					if(cond == EC_Normal)
					{
						bCancelled = true;

						m_CurrentReq.msg.CFindRSP.DimseStatus = STATUS_FIND_Cancel_MatchingTerminatedDueToCancelRequest;
						m_CurrentReq.msg.CFindRSP.DataSetType = DIMSE_DATASET_NULL;

						m_pServerLogger->LogMsg("[%s] - Received cancel request.", m_pAssociation->params->DULparams.callingAPTitle);
						m_pModalityLogger->LogMsg("Received cancel request.");
						m_pMailer->AddLine("Received cancel request.");

						break;
					}
					else if(cond == DIMSE_NODATAAVAILABLE)
					{
						//	Normal timeout, just continue
						//	since nothing has been received.
					}
					else
					{
						m_pModalityLogger->LogMsg("ERROR : %s", cond.text());
						m_pMailer->AddLine("%s", cond.text());

						m_CurrentRsp.msg.CFindRSP.DimseStatus = STATUS_FIND_Failed_UnableToProcess;
						m_CurrentRsp.msg.CFindRSP.DataSetType = DIMSE_DATASET_NULL;

						break;
					}

					/////////////////////////////////////////////////////////////
					//	Create responce dataset.
					pResult = CreateFindResultDataset(&FindRequestData, pRequest, iRlevel);
					if(pResult)
					{
						m_CurrentRsp.msg.CFindRSP.DimseStatus = STATUS_Pending;	//	There will be another result after this one.
						m_CurrentRsp.msg.CFindRSP.DataSetType = DIMSE_DATASET_PRESENT;

						/////////////////////////////////////////////////////////
						//	Send responce result.
						cond = DIMSE_sendMessageUsingMemoryData(m_pAssociation, m_CurrentPresID, &m_CurrentRsp, pStatus, pResult, NULL, NULL, NULL);
						if(cond.bad())
						{
							m_pModalityLogger->LogMsg("ERROR : %s", cond.text());
							m_pModalityLogger->LogMsg("        Unable to send C-FIND responce.");

							m_pMailer->AddLine("%s", cond.text());
							m_pMailer->AddLine("Unable to send C-FIND responce.");

							DICOMUtil::ClearDataset(&pResult);
							DICOMUtil::ClearDataset(&pStatus);

							m_CurrentRsp.msg.CFindRSP.DimseStatus = STATUS_FIND_Failed_UnableToProcess;
							m_CurrentRsp.msg.CFindRSP.DataSetType = DIMSE_DATASET_NULL;

							bRetVal = false;

							DICOMUtil::ClearDataset(&pResult);
							DICOMUtil::ClearDataset(&pStatus);

							break;
						}
						DICOMUtil::ClearDataset(&pResult);
						DICOMUtil::ClearDataset(&pStatus);
					}
					else
					{
						m_CurrentRsp.msg.CFindRSP.DimseStatus = STATUS_MOVE_Refused_OutOfResourcesSubOperations;
						m_CurrentRsp.msg.CFindRSP.DataSetType = DIMSE_DATASET_NULL;

						m_pModalityLogger->LogMsg("ERROR : Unable to allocate responce dataset.");
						m_pMailer->AddLine("Unable to allocate responce dataset.");

						bRetVal = false;

						break;
					}
				}while(m_DataService.NextFindResult());

				if(!bCancelled)
				{
					if(bRetVal)
					{
						m_CurrentRsp.msg.CFindRSP.DimseStatus = STATUS_Success;
						m_CurrentRsp.msg.CFindRSP.DataSetType = DIMSE_DATASET_NULL;
					}
					else
					{
						m_CurrentRsp.msg.CFindRSP.DimseStatus = STATUS_FIND_Failed_UnableToProcess;
						m_CurrentRsp.msg.CFindRSP.DataSetType = DIMSE_DATASET_NULL;
					}
				}
			}
			else if(QueryRet == QS_ERROR)
			{
				/////////////////////////////////////////////////////////////////
				//	A database related error occured.
				m_CurrentRsp.msg.CFindRSP.DimseStatus = STATUS_FIND_Failed_UnableToProcess;
				m_CurrentRsp.msg.CFindRSP.DataSetType = DIMSE_DATASET_NULL;

				m_pModalityLogger->LogMsg("        Unable to find result for the query.");
				m_pMailer->AddLine("Unable to find result for the query.");

				bRetVal  = false;
			}
			else
			{
				/////////////////////////////////////////////////////////////////
				//	The request to the database was successful but there was no
				//	result.
				m_CurrentRsp.msg.CFindRSP.DimseStatus = STATUS_Success;
				m_CurrentRsp.msg.CFindRSP.DataSetType = DIMSE_DATASET_NULL;

				m_pModalityLogger->LogMsg("No result was found.");
				m_pMailer->AddLine("No result was found.");
			}
		}
		else
		{
			m_CurrentRsp.msg.CFindRSP.DimseStatus = STATUS_FIND_Failed_UnableToProcess;
			m_CurrentRsp.msg.CFindRSP.DataSetType = DIMSE_DATASET_NULL;

			m_pModalityLogger->LogMsg("Unable to extract data from find request.");
			m_pMailer->AddLine("Unable to extract data from find request.");

			bRetVal  = false;
		}

		/////////////////////////////////////////////////////////////////////////
		//	Send last responce.
		cond = DIMSE_sendMessageUsingMemoryData(m_pAssociation, m_CurrentPresID, &m_CurrentRsp, pStatus, NULL, NULL, NULL);
		if(cond.bad())
		{
			m_pModalityLogger->LogMsg("ERROR : %s", cond.text());
			m_pModalityLogger->LogMsg("        Could not send responce.");

			m_pMailer->AddLine("%s", cond.text());
			m_pMailer->AddLine("Could not send responce.");

			bRetVal = false;
		}

		if(bCancelled)
		{
			m_pServerLogger->LogMsg("[%s] - C-FIND cancelled.", m_pAssociation->params->DULparams.callingAPTitle);
			m_pModalityLogger->LogMsg("C-FIND cancelled.");
			m_pMailer->AddLine("C-FIND cancelled.");
		}
		else
		{
			m_pModalityLogger->LogMsg("Number of result found : %d", iResultCount);
			m_pMailer->AddLine("Number of result found : %d", iResultCount);
		}

		DICOMUtil::ClearDataset(&pResult);
		DICOMUtil::ClearDataset(&pStatus);

		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	bool C_FINDTransaction::ExtractFindData(DcmDataset *pDataset, FindData *pFindData)
	{
		bool bRetVal     = false;
		OFCondition cond = EC_Normal;

		DICOMUtil::GetTagFromDataset(pDataset, DCM_QueryRetrieveLevel, pFindData->sQLevel);

		/////////////////////////////////////////////////////////////////////////
		//	Extract patient infos.
		DICOMUtil::GetTagFromDataset(pDataset, DCM_PatientID, &pFindData->PatId);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_PatientsName, &pFindData->PatName);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_PatientsBirthDate, &pFindData->PatBirthDate);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_PatientsBirthTime, &pFindData->PatBirthTime);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_OtherPatientIDs, &pFindData->PatOtherId);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_PatientsSex, &pFindData->PatSex);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_OtherPatientNames, &pFindData->PatOtherName);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_PatientsAge, &pFindData->PatAge);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_PatientsSize, &pFindData->PatSize);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_PatientsWeight, &pFindData->PatWeight);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_PatientsMothersBirthName, &pFindData->PatMotherName);

		/////////////////////////////////////////////////////////////////////////
		//	Extract study infos.
		DICOMUtil::GetTagFromDataset(pDataset, DCM_StudyInstanceUID, &pFindData->StudyInstanceUID);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_StudyDate, &pFindData->StudyDate);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_StudyTime, &pFindData->StudyTime);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_StudyDescription, &pFindData->StudyDesc);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_StudyID, &pFindData->StudyId);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_AccessionNumber, &pFindData->StudyAccessionNumber);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_AdmittingDiagnosesDescription, &pFindData->StudyAdmDiag);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_ReferringPhysiciansName, &pFindData->StudyRefPhysicianName);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_NameOfPhysiciansReadingStudy, &pFindData->StudyReadPhysicianName);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_ModalitiesInStudy, &pFindData->StudyModalitiesInStudy);

		/////////////////////////////////////////////////////////////////////////
		//	Extract series infos.
		DICOMUtil::GetTagFromDataset(pDataset, DCM_SeriesInstanceUID, &pFindData->SeriesInstanceUID);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_SeriesDate, &pFindData->SeriesDate);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_SeriesTime, &pFindData->SeriesTime);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_SeriesDescription, &pFindData->SeriesDesc);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_Laterality, &pFindData->SeriesLaterality);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_Modality, &pFindData->SeriesModality);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_SeriesNumber, &pFindData->SeriesNumber);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_ViewPosition, &pFindData->SeriesViewPosition);

		/////////////////////////////////////////////////////////////////////////
		//	Extract object infos.
		DICOMUtil::GetTagFromDataset(pDataset, DCM_SOPClassUID, &pFindData->ObjectSopClasUID);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_SOPInstanceUID, &pFindData->ObjectInstanceUID);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_InstanceNumber, &pFindData->ObjectInstanceNumber);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_AcquisitionDate, &pFindData->ObjectAcquisitionDate);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_AcquisitionTime, &pFindData->ObjectAcquisitionTime);

		if(pFindData->sQLevel[0])
		{
			bRetVal = true;
		}
		else
		{
			m_pModalityLogger->LogMsg("Unable to get query level from request dataset in C_FINDTransaction::ExtractFindData");
			m_pMailer->AddLine("Unable to get query level from request dataset in C_FINDTransaction::ExtractFindData");
		}
		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	DcmDataset *C_FINDTransaction::CreateFindResultDataset(FindData *pResponceData, 
		                                                   DcmDataset *pRequestDataset, 
								                           int iQLevel)
	{
		DcmDataset *pDataset = NULL;
		OFCondition cond     = EC_Normal;

		/////////////////////////////////////////////////////////////////////
		//	NOTE : This typecast is safe. The overridden method in DcmDataset
		//         creates a copy wich is a DcmDataset but it is casted as a
		//         DcmObject to have the same prototype as the base class.
		/////////////////////////////////////////////////////////////////////
		pDataset = (DcmDataset *)pRequestDataset->clone();
		if(pDataset)
		{
			/////////////////////////////////////////////////////////////////
			//	Update patient infos.
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_PatientID, &pResponceData->PatId);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_PatientsName, &pResponceData->PatName);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_PatientsBirthDate, &pResponceData->PatBirthDate);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_PatientsBirthTime, &pResponceData->PatBirthTime);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_OtherPatientIDs, &pResponceData->PatOtherId);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_PatientsSex, &pResponceData->PatSex);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_OtherPatientNames, &pResponceData->PatOtherName);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_PatientsAge, &pResponceData->PatAge);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_PatientsSize, &pResponceData->PatSize);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_PatientsWeight, &pResponceData->PatWeight);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_PatientsMothersBirthName, &pResponceData->PatMotherName);

			/////////////////////////////////////////////////////////////////
			//	Update study infos.
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_StudyInstanceUID, &pResponceData->StudyInstanceUID);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_StudyDate, &pResponceData->StudyDate);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_StudyTime, &pResponceData->StudyTime);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_StudyDescription, &pResponceData->StudyDesc);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_StudyID, &pResponceData->StudyId);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_AccessionNumber, &pResponceData->StudyAccessionNumber);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_AdmittingDiagnosesDescription, &pResponceData->StudyAdmDiag);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_ReferringPhysiciansName, &pResponceData->StudyRefPhysicianName);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_NameOfPhysiciansReadingStudy, &pResponceData->StudyReadPhysicianName);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_ModalitiesInStudy, &pResponceData->StudyModalitiesInStudy);

			/////////////////////////////////////////////////////////////////
			//	Update series infos.
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_SeriesInstanceUID, &pResponceData->SeriesInstanceUID);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_SeriesDate, &pResponceData->SeriesDate);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_SeriesTime, &pResponceData->SeriesTime);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_SeriesDescription, &pResponceData->SeriesDesc);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_Laterality, &pResponceData->SeriesLaterality);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_Modality, &pResponceData->SeriesModality);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_SeriesNumber, &pResponceData->SeriesNumber);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_ViewPosition, &pResponceData->SeriesViewPosition);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_BodyPartExamined, &pResponceData->SeriesBodyPart);

			/////////////////////////////////////////////////////////////////
			//	Update object infos.
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_SOPClassUID, &pResponceData->ObjectSopClasUID);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_SOPInstanceUID, &pResponceData->ObjectInstanceUID);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_InstanceNumber, &pResponceData->ObjectInstanceNumber);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_AcquisitionDate, &pResponceData->ObjectAcquisitionDate);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_AcquisitionTime, &pResponceData->ObjectAcquisitionTime);
		}
		else
		{
			m_pModalityLogger->LogMsg("ERROR : Unable to clone dataset in C_FINDTransaction::CreateFindResultDataset.");
			m_pMailer->AddLine("Unable to clone dataset in C_FINDTransaction::CreateFindResultDataset");
		}
		return pDataset;
	}

	/////////////////////////////////////////////////////////////////////////
	//	Process a modality worklist query.
	bool C_FINDTransaction::ProcessWorklist(DcmDataset *pRequest)
	{
		bool bRetVal						= true;
		bool bCancelled					= false;
		int iResultCount				= 0;
		bool nextResult					= false;
		bool mustSendResponse			= true;
		char oldRequestedProcedure[65]	= {0};
		QUERY_STATUS QueryRet			= QS_OK_NO_RESULT;
		WLData Query					= {0};
		WLData Result					= {0};

		if(m_pCfg->IsVerbose())
		{
			m_pModalityLogger->LogMsg("--ProcessWorklist()");
		}

		/////////////////////////////////////////////////////////////////////////
		//	dcmtk variables.
		OFCondition cond             = EC_Normal;
		DcmDataset *pResultDataset   = NULL;
		DcmDataset *pStatus          = NULL;

		m_CurrentRsp.CommandField                           = DIMSE_C_FIND_RSP;
		m_CurrentRsp.msg.CFindRSP.MessageIDBeingRespondedTo = m_CurrentReq.msg.CFindRQ.MessageID;
		m_CurrentRsp.msg.CFindRSP.DimseStatus               = STATUS_Pending;	//	Assuming.
		m_CurrentRsp.msg.CFindRSP.opts                      = O_FIND_AFFECTEDSOPCLASSUID;

		strcpy(m_CurrentRsp.msg.CFindRSP.AffectedSOPClassUID, m_CurrentReq.msg.CFindRQ.AffectedSOPClassUID);

		if(ExtractWorklistQry(pRequest, &Query))
		{
			QueryRet = m_DataService.StartFindWorklist(m_pAssociation->params->DULparams.callingAPTitle, &Query, &Result);
			if(QueryRet == QS_OK_RESULT)
			{
				//////////////////////////////////
				//	We have results, send them.
				//////////////////////////////////
				nextResult = true;
				while (nextResult)
				{
					mustSendResponse = true;
					/////////////////////////////////////////////
					//	Check for cancel request.
					cond = DIMSE_checkForCancelRQ(m_pAssociation, m_CurrentPresID, m_CurrentReq.msg.CFindRQ.MessageID);
					if(cond == EC_Normal)
					{
						//	We received a cancel request.
						bCancelled = true;
						m_CurrentRsp.msg.CFindRSP.DimseStatus = STATUS_FIND_Cancel_MatchingTerminatedDueToCancelRequest;
						m_CurrentRsp.msg.CFindRSP.DataSetType = DIMSE_DATASET_NULL;

						m_pServerLogger->LogMsg("[%s] - Received cancel request.", m_pAssociation->params->DULparams.callingAPTitle);
						m_pModalityLogger->LogMsg("Received cancel request.");
						m_pMailer->AddLine("Received cancel request.");

						break;
					}
					else if(cond == DIMSE_NODATAAVAILABLE)
					{
						//	Normal timeout, just continue
						//	since nothing has been received.
					}
					else
					{
						m_pModalityLogger->LogMsg("ERROR : %s", cond.text());
						m_pMailer->AddLine("%s", cond.text());

						m_CurrentRsp.msg.CFindRSP.DimseStatus = STATUS_FIND_Failed_UnableToProcess;
						m_CurrentRsp.msg.CFindRSP.DataSetType = DIMSE_DATASET_NULL;

						break;
					}

					pResultDataset = CreateFindWorklistResultDataset(&Result, pResultDataset);

					//	Check if we have another scheduled procedure
					//	step before sending the result.
					strcpy(oldRequestedProcedure, Result.RequestedProcedureId);

					nextResult = m_DataService.NextFindResult();
					if(nextResult)
					{
						if(strcmp(Result.RequestedProcedureId, oldRequestedProcedure)==0)
						{
							mustSendResponse = false;
						}
					}
					
					if(mustSendResponse)
					{
						++iResultCount;
						if(pResultDataset)
						{
							m_CurrentRsp.msg.CFindRSP.DimseStatus = STATUS_Pending;
							m_CurrentRsp.msg.CFindRSP.DataSetType = DIMSE_DATASET_PRESENT;

							//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
							// Il va falloir integrer un module de patch
							// Patch pour:  Manufacturer:	Swissray 
							//				Model:			ddR Multi System
							// Probl�me:	Nous affiche le refering physician mais 
							//				pas le code d'examen ni la description

							if(strstr(m_pAssociation->params->DULparams.callingAPTitle,"SWISSRAY")!=0)
							{
								m_pModalityLogger->LogMsg("It's a Swissray ddR Multi System modality");
								m_pModalityLogger->LogMsg("Must apply patch");

								m_pMailer->AddLine("It's a Swissray ddR Multi System modality");
								m_pMailer->AddLine("Must apply patch");

								const char *pStudyDesc  = NULL;
								cond = pResultDataset->findAndGetString(DCM_RequestedProcedureDescription, pStudyDesc);
								if(cond.good() && pStudyDesc)
								{
									pResultDataset->putAndInsertString(DCM_ReferringPhysiciansName, pStudyDesc);
									m_pModalityLogger->LogMsg("We put the description in the refering physician tag for swissray modality.");
									m_pMailer->AddLine("We put the description in the refering physician tag for swissray modality.");
								}				
							}
							// Patch pour:  Manufacturer:	Swissray 
							//				Model:			ddR Multi System
							//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

							/////////////////////////////////////////////////
							//	Send responce result.
							cond = DIMSE_sendMessageUsingMemoryData(m_pAssociation, m_CurrentPresID, &m_CurrentRsp, pStatus, pResultDataset, NULL, NULL);
							if(cond.bad())
							{
								m_pModalityLogger->LogMsg("ERROR : %s", cond.text());
								m_pModalityLogger->LogMsg("        Unable to send C-FIND responce.");

								m_pMailer->AddLine("%s", cond.text());
								m_pMailer->AddLine("Unable to send C-FIND responce.");

								DICOMUtil::ClearDataset(&pResultDataset);
								DICOMUtil::ClearDataset(&pStatus);

								bRetVal = false;

								break;
							}
							DICOMUtil::ClearDataset(&pResultDataset);
							DICOMUtil::ClearDataset(&pStatus);
						}
						else
						{
							break;
						}
					}
				}

				if(!bCancelled)
				{
					if(bRetVal)
					{
						m_CurrentRsp.msg.CFindRSP.DimseStatus = STATUS_Success;
						m_CurrentRsp.msg.CFindRSP.DataSetType = DIMSE_DATASET_NULL;
					}
					else
					{
						m_CurrentRsp.msg.CFindRSP.DimseStatus = STATUS_FIND_Failed_UnableToProcess;
						m_CurrentRsp.msg.CFindRSP.DataSetType = DIMSE_DATASET_NULL;
					}
				}
			}
			else if(QueryRet == QS_ERROR)
			{
				m_CurrentRsp.msg.CFindRSP.DimseStatus = STATUS_FIND_Failed_UnableToProcess;
				m_CurrentRsp.msg.CFindRSP.DataSetType = DIMSE_DATASET_NULL;

				bRetVal  = false;
			}
			else
			{
				//	The request to the database was successful but there was no result.
				m_CurrentRsp.msg.CFindRSP.DimseStatus = STATUS_Success;
				m_CurrentRsp.msg.CFindRSP.DataSetType = DIMSE_DATASET_NULL;

				m_pModalityLogger->LogMsg("No result was found.");

				m_pMailer->AddLine("No result was found.");
			}
		}
		else
		{
			m_CurrentRsp.msg.CFindRSP.DimseStatus = STATUS_FIND_Failed_UnableToProcess;
			m_CurrentRsp.msg.CFindRSP.DataSetType = DIMSE_DATASET_NULL;

			m_pModalityLogger->LogMsg("Unable to extract data from find worklist request.");

			m_pMailer->AddLine("Unable to extract data from find worklist request.");

			bRetVal  = false;
		}

		cond = DIMSE_sendMessageUsingMemoryData(m_pAssociation, m_CurrentPresID, &m_CurrentRsp, pStatus, NULL, NULL, NULL);
		if(cond.bad())
		{
			m_pModalityLogger->LogMsg("ERROR : %s", cond.text());
			m_pModalityLogger->LogMsg("        Could not send responce.");

			m_pMailer->AddLine("%s", cond.text());
			m_pMailer->AddLine("Could not send responce.");

			bRetVal = false;
		}

		if(bCancelled)
		{
			m_pServerLogger->LogMsg("[%s] - C-FIND worklist cancelled.", m_pAssociation->params->DULparams.callingAPTitle);
			m_pModalityLogger->LogMsg("C-FIND worklist cancelled.");

			m_pMailer->AddLine("C-FIND worklist cancelled.");
		}
		else
		{
			m_pModalityLogger->LogMsg("Number of result found : %d", iResultCount);
			m_pMailer->AddLine("Number of result found : %d", iResultCount);
		}
		DICOMUtil::ClearDataset(&pResultDataset);
		DICOMUtil::ClearDataset(&pStatus);

		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////////
	//
	bool C_FINDTransaction::ExtractWorklistQry(DcmDataset *pDataset, WLData *pInfo)
	{
		bool bRetVal          = false;
		OFCondition cond      = EC_Normal;
		char *pValue          = NULL;
		DcmElement *pElement  = NULL;
		DcmElement *pSequence = NULL;
		DcmItem    *pDi       = NULL;
		int iElemCount        = 0;

		if(m_pCfg->IsVerbose())
		{
			m_pModalityLogger->LogMsg("--ExtractWorklistQry()");
		}

		if(pDataset)
		{
			cond = pDataset->findAndGetElement(DCM_ScheduledProcedureStepSequence, pSequence);
			if(cond.good())
			{
				DICOMUtil::GetTagFromDataset(pDataset, DCM_PatientsName, pInfo->PatientName);
				DICOMUtil::GetTagFromDataset(pDataset, DCM_PatientID, pInfo->PatientId);

				iElemCount = (int)((DcmSequenceOfItems*)pSequence)->card();
				if(iElemCount > 0)
				{
					//	If the sequence attribute is existent and non-empty,
					//	we need to check every item in the sequence for completeness.
					//	iElemCount should always be one or zero.
					for(int i = 0; i < iElemCount; ++i)
					{
						pDi = ((DcmSequenceOfItems*)pSequence)->getItem(i);
						if(pDi)
						{
							DICOMUtil::GetTagFromDataset(pDi, DCM_ScheduledStationAETitle, pInfo->ScheduledStationAeTitle);
							DICOMUtil::GetTagFromDataset(pDi, DCM_ScheduledProcedureStepStartDate,pInfo->ScheduledProcedureStepStartDate);
							DICOMUtil::GetTagFromDataset(pDi, DCM_ScheduledProcedureStepStartTime, pInfo->ScheduledProcedureStepStartTime);
							DICOMUtil::GetTagFromDataset(pDi, DCM_Modality, pInfo->Modality);
							DICOMUtil::GetTagFromDataset(pDi, DCM_ScheduledPerformingPhysiciansName, pInfo->ScheduledPerformingPhysicianName);

							bRetVal = true;
						}
						else
						{
							m_pModalityLogger->LogMsg("ERROR : NULL element in scheduled procedure step sequence.");
							m_pMailer->AddLine("NULL element in scheduled procedure step sequence.");
						}
					}
				}
				else
				{
					//	We have no query criteria, send everything.
					//	We will put nothing in the where clause of
					//	our select. Let's hope it never happens.
				}
			}
			else
			{
				//	If the sequence attribute is absent, we want to return true.
				bRetVal = true;
			}
		}
		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	DcmDataset *C_FINDTransaction::CreateFindWorklistResultDataset(WLData *pResult, DcmDataset *pDataset)
	{
		bool bRetVal                      = false;
		OFCondition cond                  = EC_Normal;
		DcmItem *pItem                    = NULL;
		DcmSequenceOfItems *pPatientSeq   = NULL;
		char modality[65]                 = {0};
		char CodeValue[129]               = {0};
		char ReqProcDesc[129]             = {0};
		char oldRequestedProcedureID[65]  = {0};
		bool mustCreateRequestedProcedure = false;

		if(!pDataset)
		{
			pDataset = new DcmDataset;
			mustCreateRequestedProcedure=true;
		}

		/////////////////////////////////////////////////////////////////////
		//	Remove the matricule in the physician's name if any.
		for(int i = strlen(pResult->ReferringPhysicianName); i > 0; --i)
		{
			if(pResult->ReferringPhysicianName[i] == '#')
			{
				pResult->ReferringPhysicianName[i] = 0x00;
				break;
			}
		}
		//
		/////////////////////////////////////////////////////////////////////

		/////////////////////////////////////////////////////////////////////
		//	If there was no forced modality and no requested modality, we
		//	use the first returned by the select.
		if(!pResult->Modality[0] && pResult->sSpsModality[0])
		{
			int iLen = strlen(pResult->sSpsModality);
			for(int i = 0; i < iLen; ++i)
			{
				if(pResult->sSpsModality[i] == ';')
				{
					pResult->sSpsModality[i] = 0x00;
					break;
				}
			}
			strcpy(pResult->Modality, pResult->sSpsModality);
		}
		//
		/////////////////////////////////////////////////////////////////////

		cond = pDataset->findOrCreateSequenceItem(DCM_ScheduledProcedureStepSequence,		//	0x0040, 0x0100
												  pItem,
												  -2);
		if(cond.good())
		{
			DICOMUtil::UpdateTagInDataset(pItem, DCM_ScheduledStationAETitle, m_pAssociation->params->DULparams.callingAPTitle);
			DICOMUtil::UpdateTagInDataset(pItem, DCM_ScheduledProcedureStepStartDate, pResult->ScheduledProcedureStepStartDate);
			DICOMUtil::UpdateTagInDataset(pItem, DCM_ScheduledProcedureStepStartTime, pResult->ScheduledProcedureStepStartTime);
			DICOMUtil::UpdateTagInDataset(pItem, DCM_Modality, pResult->Modality);
			DICOMUtil::UpdateTagInDataset(pItem, DCM_ScheduledPerformingPhysiciansName, pResult->ScheduledPerformingPhysicianName);
			DICOMUtil::UpdateTagInDataset(pItem, DCM_ScheduledProcedureStepDescription, pResult->ScheduledProcedureStepDescription);
			DICOMUtil::UpdateTagInDataset(pItem, DCM_ScheduledProcedureStepLocation, pResult->ScheduledProcedureStepLocation);
			DICOMUtil::UpdateTagInDataset(pItem, DCM_PreMedication, pResult->PreMedication);
			DICOMUtil::UpdateTagInDataset(pItem, DCM_ScheduledProcedureStepID, pResult->ScheduledProcedureStepId);
			DICOMUtil::UpdateTagInDataset(pItem, DCM_RequestedContrastAgent, pResult->RequestedContrastAgent);
			DICOMUtil::UpdateTagInDataset(pItem, DCM_ScheduledProcedureStepStatus, pResult->ScheduledProcedureStepStatus);
		}

		if(mustCreateRequestedProcedure)
		{
			cond = pDataset->findOrCreateSequenceItem(DCM_RequestedProcedureCodeSequence,	//	0x0032, 0x1064
												      pItem,
												      -2);
			if(cond.good())
			{
				if(m_DataService.UseInternalCode(m_pAssociation->params->DULparams.callingAPTitle))
				{
					strcpy(CodeValue, pResult->RequestedProcedureInternalCode);
					strcat(CodeValue, pResult->RequestedProcedureLaterality);
				}
				else
				{
					strcpy(CodeValue, pResult->RequestedProcedureCode);
				}

				if(strlen(pResult->RequestedProcedureLaterality) > 0)
				{
					strcpy(ReqProcDesc, pResult->RequestedProcedureLaterality);
					strcat(ReqProcDesc, " ");
				}
				strcat(ReqProcDesc, pResult->RequestedProcedureDescription);
				
				pItem->putAndInsertString(DCM_CodeValue, CodeValue);
				pItem->putAndInsertString(DCM_CodingSchemeDesignator, "IMAGEM");
				pItem->putAndInsertString(DCM_CodeMeaning, ReqProcDesc);
			}

			pDataset->putAndInsertOFStringArray(DCM_RequestedProcedureID, pResult->RequestedProcedureId);
			memset(ReqProcDesc,0,sizeof(ReqProcDesc));
			if(strlen(pResult->RequestedProcedureLaterality) > 0)
			{
				strcpy(ReqProcDesc, pResult->RequestedProcedureLaterality);
				strcat(ReqProcDesc, " ");
			}
			strcat(ReqProcDesc, pResult->RequestedProcedureDescription);

			DICOMUtil::UpdateTagInDataset(pDataset, DCM_RequestedProcedureDescription, ReqProcDesc);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_StudyInstanceUID, pResult->StudyInstanceUID);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_AccessionNumber, pResult->AccesionNumber);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_RequestingPhysician, pResult->RequestingPhysician);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_ReferringPhysiciansName, pResult->ReferringPhysicianName);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_AdmissionID, pResult->VisitUID);

			DICOMUtil::UpdateTagInDataset(pDataset, DCM_PatientsName, pResult->PatientName);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_PatientID, pResult->PatientId);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_PatientsBirthDate, pResult->PatientBirthDate);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_PatientsBirthTime, pResult->PatientBirthTime);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_PatientsSex, pResult->PatientSex);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_PatientsWeight, (char *)NULL);

			pPatientSeq = new DcmSequenceOfItems(DCM_ReferencedPatientSequence);
			if(pPatientSeq)
			{
				pDataset->insert(pPatientSeq);
			}

			pPatientSeq = new DcmSequenceOfItems(DCM_ReferencedStudySequence);
			if(pPatientSeq)
			{
				pDataset->insert(pPatientSeq);
			}
		}
		return pDataset;
	}

	/////////////////////////////////////////////////////////////////////////
	//	Process a general purpose worklist query.
	bool C_FINDTransaction::ProcessGPWorklist(DcmDataset *pRequestDataset)
	{
		bool bRet                  = true;
		int iResultCount		   = 0;
		bool bCancelled            = false;
		bool bNextResult           = false;
		bool bMustSendResponce     = false;
		QUERY_STATUS QueryRet      = QS_OK_NO_RESULT;
		GPWLData GPReq;

		/////////////////////////////////////////////////////////////////////
		//	dcmtk variables.
		OFCondition cond           = EC_Normal;
		DcmDataset *pResultDataset = NULL;
		DcmDataset *pStatus        = NULL;

		/////////////////////////////////////////////////////////////////////
		//
		m_CurrentRsp.CommandField                           = DIMSE_C_FIND_RSP;
		m_CurrentRsp.msg.CFindRSP.MessageIDBeingRespondedTo = m_CurrentReq.msg.CFindRQ.MessageID;
		m_CurrentRsp.msg.CFindRSP.opts                      = O_FIND_AFFECTEDSOPCLASSUID;
		m_CurrentRsp.msg.CFindRSP.DimseStatus               = STATUS_Pending;	//	Assuming.

		strcpy(m_CurrentRsp.msg.CFindRSP.AffectedSOPClassUID, m_CurrentReq.msg.CFindRQ.AffectedSOPClassUID);

		/////////////////////////////////////////////////////////////////////
		//	Extract query data from the dataset.
		if(ExtractGPWorklistQry(pRequestDataset, &GPReq))
		{
			QueryRet = m_DataService.StartFindGPWorlkist(&GPReq);
			if(QueryRet == QS_OK_RESULT)
			{
				/////////////////////////////////////////////////////////////
				//	We have results, send them all.
				bNextResult = true;
				while(bNextResult)
				{
					bMustSendResponce = true;
					/////////////////////////////////////////////////////////
					//	Check for cancel request.
					cond = DIMSE_checkForCancelRQ(m_pAssociation, m_CurrentPresID, m_CurrentReq.msg.CFindRQ.MessageID);
					if(cond == EC_Normal)
					{
						//	We received a cancel request.
						bCancelled = true;
						m_CurrentRsp.msg.CFindRSP.DimseStatus = STATUS_FIND_Cancel_MatchingTerminatedDueToCancelRequest;
						m_CurrentRsp.msg.CFindRSP.DataSetType = DIMSE_DATASET_NULL;

						m_pServerLogger->LogMsg("Received cancel request.");
						m_pModalityLogger->LogMsg("Received cancel request.");
						m_pMailer->AddLine("Received cancel request.");

						break;
					}
					else if(cond == DIMSE_NODATAAVAILABLE)
					{
						//	DIMSE_checkForCancelRQ timeout, just continue
						//	since nothing has been received.
					}
					else
					{
						m_pModalityLogger->LogMsg("ERROR : %s", cond.text());
						m_pMailer->AddLine("%s", cond.text());
						m_CurrentRsp.msg.CFindRSP.DimseStatus = STATUS_FIND_Failed_UnableToProcess;
						m_CurrentRsp.msg.CFindRSP.DataSetType = DIMSE_DATASET_NULL;

						bRet = false;

						break;
					}
					m_DataService.GetReferencedStudies(&GPReq);
					strcpy(GPReq.m_SOPClassUID.Data, UID_GeneralPurposeScheduledProcedureStepSOPClass);

					pResultDataset = CreateGPWorklistDataset(pRequestDataset, &GPReq);
					if(bMustSendResponce)
					{
						++iResultCount;
						if(pResultDataset)
						{
							m_CurrentRsp.msg.CFindRSP.DimseStatus = STATUS_Pending;
							m_CurrentRsp.msg.CFindRSP.DataSetType = DIMSE_DATASET_PRESENT;

							/////////////////////////////////////////////////////////
							//	Send responce result.
							cond = DIMSE_sendMessageUsingMemoryData(m_pAssociation, m_CurrentPresID, &m_CurrentRsp, pStatus, pResultDataset, NULL, NULL);
							if(cond.bad())
							{
								m_pModalityLogger->LogMsg("ERROR : %s", cond.text());
								m_pModalityLogger->LogMsg("        Unable to send C-FIND (GP Worklist) responce.");
								m_pMailer->AddLine("%s", cond.text());
								m_pMailer->AddLine("Unable to send C-FIND (GP Worklist) responce.");

								bRet = false;

								break;
							}
							DICOMUtil::ClearDataset(&pResultDataset);
							DICOMUtil::ClearDataset(&pStatus);

							bNextResult = m_DataService.NextFindResult();
						}
						else
						{
							break;
						}
					}
				}

				if(!bCancelled)
				{
					if(bRet)
					{
						m_CurrentRsp.msg.CFindRSP.DimseStatus = STATUS_Success;
						m_CurrentRsp.msg.CFindRSP.DataSetType = DIMSE_DATASET_NULL;
					}
					else
					{
						m_CurrentRsp.msg.CFindRSP.DimseStatus = STATUS_FIND_Failed_UnableToProcess;
						m_CurrentRsp.msg.CFindRSP.DataSetType = DIMSE_DATASET_NULL;
					}
				}
			}
			else if(QueryRet == QS_ERROR)
			{
				m_CurrentRsp.msg.CFindRSP.DimseStatus = STATUS_FIND_Failed_UnableToProcess;
				m_CurrentRsp.msg.CFindRSP.DataSetType = DIMSE_DATASET_NULL;

				bRet  = false;
			}
			else
			{
				//	The request to the database was successful but there was no result.
				m_CurrentRsp.msg.CFindRSP.DimseStatus = STATUS_Success;
				m_CurrentRsp.msg.CFindRSP.DataSetType = DIMSE_DATASET_NULL;
				m_pModalityLogger->LogMsg("No result was found.");
				m_pMailer->AddLine("No result was found.");
			}
		}
		else
		{
			m_pServerLogger->LogMsg("ERROR : Unable to extract GPWL request data from dataset.");
			m_pModalityLogger->LogMsg("ERROR : Unable to extract GPWL request data from dataset.");
			m_pMailer->AddLine("ERROR : Unable to extract GPWL request data from dataset.");
		}

		cond = DIMSE_sendMessageUsingMemoryData(m_pAssociation, m_CurrentPresID, &m_CurrentRsp, pStatus, NULL, NULL, NULL);
		if(cond.bad())
		{
			m_pModalityLogger->LogMsg("ERROR : %s", cond.text());
			m_pModalityLogger->LogMsg("        Could not send responce.");
			m_pMailer->AddLine("%s", cond.text());
			m_pMailer->AddLine("Could not send responce.");

			bRet = false;
		}

		if(bCancelled)
		{	
			m_pServerLogger->LogMsg("C-FIND worklist cancelled.");
			m_pModalityLogger->LogMsg("C-FIND (GP Worklist) cancelled.");
			m_pMailer->AddLine("C-FIND (GP Worklist) cancelled.");
		}
		else
		{
			m_pModalityLogger->LogMsg("Number of result found : %d", iResultCount);
			m_pMailer->AddLine("Number of result found : %d", iResultCount);
		}
		DICOMUtil::ClearDataset(&pStatus);
		DICOMUtil::ClearDataset(&pResultDataset);

		return bRet;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	bool C_FINDTransaction::ExtractGPWorklistQry(DcmDataset *pDataset, GPWLData *pGPInfo)
	{
		OFCondition cond                = EC_Normal;
		const char *pValue              = NULL;
		DcmItem *pItem                  = NULL;
		DcmItem *pSubItem               = NULL;
		DcmElement *pCurElement         = NULL;
		DcmElement *pSubSequence        = NULL;
		DcmSequenceOfItems*pPerfCodeSeq = NULL;

		/////////////////////////////////////////////////////////////////////////
		//	Extract the first level tags (those that aren't in a sequence).
		/////////////////////////////////////////////////////////////////////////

		DICOMUtil::GetTagFromDataset(pDataset, DCM_GeneralPurposeScheduledProcedureStepStatus, &pGPInfo->m_GPProcedureStepStatus);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_GeneralPurposeScheduledProcedureStepPriority, &pGPInfo->m_GPScheduledProcStepPriority);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_InputAvailabilityFlag, &pGPInfo->m_InputAvailabilityFlag);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_ScheduledProcedureStepID, &pGPInfo->m_ScheduledProcStepId);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_PatientsName, &pGPInfo->m_PatientName);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_PatientID, &pGPInfo->m_PatientId);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_PatientsBirthDate, &pGPInfo->m_PatientBirthDate);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_PatientsSex, &pGPInfo->m_PatientSex);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_StudyInstanceUID, &pGPInfo->m_StudyInstanceUID);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_ScheduledProcedureStepStartDateAndTime, &pGPInfo->m_SchProcStepStartDate);

		/////////////////////////////////////////////////////////////////////////
		//	Extract scheduled wortitem code.
		cond = pDataset->findAndGetElement(DCM_ScheduledWorkitemCodeSequence, pCurElement);
		if(cond.good() && pCurElement)
		{
			pItem = ((DcmSequenceOfItems*)pCurElement)->getItem(0);
			if(pItem)
			{
				DICOMUtil::GetTagFromDataset(pItem, DCM_CodeValue, &pGPInfo->m_WorkItemCodeValue); 
				DICOMUtil::GetTagFromDataset(pItem, DCM_CodingSchemeDesignator, &pGPInfo->m_WorkItemCodeScheme); 
				DICOMUtil::GetTagFromDataset(pItem, DCM_CodeMeaning, &pGPInfo->m_WorkItemCodeMeaning);
			}
		}

		/////////////////////////////////////////////////////////////////////////
		//	Extract scheduled performer.
		cond = pDataset->findAndGetElement(DCM_ScheduledHumanPerformersSequence, pCurElement);
		if(cond.good() && pCurElement)
		{
			pItem = ((DcmSequenceOfItems*)pCurElement)->getItem(0);
			if(pItem)
			{
				cond = pItem->findAndGetSequence(DCM_HumanPerformerCodeSequence, pPerfCodeSeq);
				if(cond.good() && pPerfCodeSeq)
				{
					pSubItem = ((DcmSequenceOfItems*)pPerfCodeSeq)->getItem(0);
					if(pSubItem)
					{
						DICOMUtil::GetTagFromDataset(pSubItem, DCM_CodeValue, &pGPInfo->m_SchPerformerCodeValue);
						DICOMUtil::GetTagFromDataset(pSubItem, DCM_CodingSchemeDesignator, &pGPInfo->m_SchPerformerCodeScheme); 
						DICOMUtil::GetTagFromDataset(pSubItem, DCM_CodeMeaning, &pGPInfo->m_SchPerformerCodeMeaning);
					}
				}
			}
		}

		/////////////////////////////////////////////////////////////////////////
		//	Extract scheduled station name.
		cond = pDataset->findAndGetElement(DCM_ScheduledStationNameCodeSequence, pCurElement);
		if(cond.good() && pCurElement)
		{
			pItem = ((DcmSequenceOfItems*)pCurElement)->getItem(0);
			if(pItem)
			{
				DICOMUtil::GetTagFromDataset(pItem, DCM_CodeValue, &pGPInfo->m_SchStationNameCodeValue); 
				DICOMUtil::GetTagFromDataset(pItem, DCM_CodingSchemeDesignator, &pGPInfo->m_SchStationNameCodeScheme); 
				DICOMUtil::GetTagFromDataset(pItem, DCM_CodeMeaning, &pGPInfo->m_SchStationNameCodeMeaning); 
			}
		}

		/////////////////////////////////////////////////////////////////////////
		//	Extract study informations.
		cond = pDataset->findAndGetElement(DCM_ReferencedRequestSequence, pCurElement);
		if(cond.good() && pCurElement)
		{
			pItem = ((DcmSequenceOfItems*)pCurElement)->getItem(0);
			if(pItem)
			{
				DICOMUtil::GetTagFromDataset(pItem, DCM_RequestedProcedureDescription, &pGPInfo->m_ReqProcDescription);
				DICOMUtil::GetTagFromDataset(pItem, DCM_AccessionNumber, &pGPInfo->m_AccessionNumber);
				DICOMUtil::GetTagFromDataset(pItem, DCM_ModalitiesInStudy, &pGPInfo->m_ModalitiesInStudy);

				/////////////////////////////////////////////////////////////
				//	NOTE : for some reason, dcmtk doesn't have a defined
				//         constant for  tag 0x032, 0x1064 so we enter
				//		   the raw tag.
				cond = pItem->findAndGetElement(DcmTagKey(0x032, 0x1064), pSubSequence);
				if(cond.good() && pSubSequence)
				{
					pSubItem = ((DcmSequenceOfItems*)pSubSequence)->getItem(0);
					if(pSubItem)
					{
						DICOMUtil::GetTagFromDataset(pItem, DCM_CodeValue, &pGPInfo->m_ReqProcCodeValue);
						DICOMUtil::GetTagFromDataset(pItem, DCM_CodingSchemeDesignator, &pGPInfo->m_ReqProcCodeScheme);
						DICOMUtil::GetTagFromDataset(pItem, DCM_CodeMeaning, &pGPInfo->m_ReqProcCodeMeaning);
					}
				}
			}
		}
		return true;
	}

	////////////////////////////////////////////////////////////////////////////
	//
	DcmDataset *C_FINDTransaction::CreateGPWorklistDataset(DcmDataset *pReqDataset, GPWLData *pGPInfo)
	{
		bool bRetVal = false;
		OFCondition cond                       = EC_Normal;
		DcmDataset *pDataset                   = NULL;
		DcmElement *pSequence                  = NULL;
		DcmElement *pSubSequence               = NULL;
		TagData *pCurRefUID                    = NULL;
		DcmItem *pItem                         = NULL;
		DcmItem *pSubItem                      = NULL;
		DcmItem *pNewItem                      = NULL;
		DcmSequenceOfItems *pPerformerSequence = NULL;
		DcmSequenceOfItems *pRefSequence       = NULL;	//	Related studies.
		bool bAddSequence                      = false;

		pDataset = (DcmDataset *)pReqDataset->clone();
		if(pDataset)
		{
			/////////////////////////////////////////////////////////////////
			//
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_SOPClassUID, &pGPInfo->m_SOPClassUID);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_SOPInstanceUID, &pGPInfo->m_SOPInstanceUID);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_GeneralPurposeScheduledProcedureStepStatus, &pGPInfo->m_GPProcedureStepStatus);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_GeneralPurposeScheduledProcedureStepPriority, &pGPInfo->m_GPScheduledProcStepPriority);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_InputAvailabilityFlag, &pGPInfo->m_InputAvailabilityFlag);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_ScheduledProcedureStepID, &pGPInfo->m_ScheduledProcStepId);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_ScheduledProcedureStepStartDateAndTime, &pGPInfo->m_SchProcStepStartDate);

			/////////////////////////////////////////////////////////////////
			//	Patient informations.
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_PatientsName, &pGPInfo->m_PatientName);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_PatientID, &pGPInfo->m_PatientId);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_PatientsBirthDate, &pGPInfo->m_PatientBirthDate);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_PatientsSex, &pGPInfo->m_PatientSex);

			/////////////////////////////////////////////////////////////////
			//	Study info.
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_StudyDate, &pGPInfo->m_StudyDate);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_StudyTime, &pGPInfo->m_StudyTime);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_StudyInstanceUID, &pGPInfo->m_StudyInstanceUID);

			cond = pDataset->findAndGetElement(DCM_ScheduledWorkitemCodeSequence, pSequence);
			if(cond.good())
			{
				pItem = ((DcmSequenceOfItems*)pSequence)->getItem(0);
				if(pItem)
				{
					DICOMUtil::UpdateTagInDataset(pItem, DCM_CodeValue, &pGPInfo->m_WorkItemCodeValue);
					DICOMUtil::UpdateTagInDataset(pItem, DCM_CodingSchemeDesignator, &pGPInfo->m_WorkItemCodeScheme);
					DICOMUtil::UpdateTagInDataset(pItem, DCM_CodeMeaning, &pGPInfo->m_WorkItemCodeMeaning);
				}

				cond = pDataset->findAndGetSequence(DCM_HumanPerformerCodeSequence, pPerformerSequence);
				if(cond.good())
				{
					if(!pPerformerSequence)
					{
						pPerformerSequence = new DcmSequenceOfItems(DCM_ReferencedStudySequence);
						bAddSequence = true;
					}
				}
				else
				{
					pPerformerSequence = new DcmSequenceOfItems(DCM_HumanPerformerCodeSequence);
					bAddSequence = true;
				}

				if(pPerformerSequence)
				{
					pItem = pPerformerSequence->getItem(0);
					if(pItem)
					{
						DICOMUtil::UpdateTagInDataset(pItem, DCM_CodeValue, &pGPInfo->m_SchPerformerCodeValue);
						DICOMUtil::UpdateTagInDataset(pItem, DCM_CodingSchemeDesignator, &pGPInfo->m_SchPerformerCodeScheme);
						DICOMUtil::UpdateTagInDataset(pItem, DCM_CodeMeaning, &pGPInfo->m_SchPerformerCodeMeaning);
					}
					else
					{
						pItem = new DcmItem;
						if(pItem)
						{
							DICOMUtil::UpdateTagInDataset(pItem, DCM_CodeValue, &pGPInfo->m_SchPerformerCodeValue);
							DICOMUtil::UpdateTagInDataset(pItem, DCM_CodingSchemeDesignator, &pGPInfo->m_SchPerformerCodeScheme);
							DICOMUtil::UpdateTagInDataset(pItem, DCM_CodeMeaning, &pGPInfo->m_SchPerformerCodeMeaning);

							pPerformerSequence->insert(pItem);
						}
					}
				}

				if(bAddSequence)
				{
					pDataset->insert(pPerformerSequence);
				}

				cond = pDataset->findAndGetElement(DCM_ScheduledStationNameCodeSequence, pSequence);
				if(cond.good())
				{
					pItem = ((DcmSequenceOfItems*)pSequence)->getItem(0);
					if(pItem)
					{
						DICOMUtil::UpdateTagInDataset(pItem, DCM_CodeValue, &pGPInfo->m_SchStationNameCodeValue);
						DICOMUtil::UpdateTagInDataset(pItem, DCM_CodingSchemeDesignator, &pGPInfo->m_SchStationNameCodeScheme);
						DICOMUtil::UpdateTagInDataset(pItem, DCM_CodeMeaning, &pGPInfo->m_SchStationNameCodeMeaning);
					}
				}

				pSequence = NULL;
				cond = pDataset->findAndGetElement(DCM_ReferencedRequestSequence, pSequence);
				if(cond.good() && pSequence)
				{
					pItem = ((DcmSequenceOfItems*)pSequence)->getItem(0);

					DICOMUtil::UpdateTagInDataset(pItem, DCM_StudyInstanceUID, &pGPInfo->m_StudyInstanceUID);
					DICOMUtil::UpdateTagInDataset(pItem, DCM_RequestedProcedureDescription, &pGPInfo->m_ReqProcDescription);
					DICOMUtil::UpdateTagInDataset(pItem, DCM_AccessionNumber, &pGPInfo->m_AccessionNumber);
					DICOMUtil::UpdateTagInDataset(pItem, DCM_ModalitiesInStudy, &pGPInfo->m_ModalitiesInStudy);

					cond = pItem->findAndGetElement(DcmTagKey(0x032, 0x1064), pSubSequence);
					if(cond.good() && pSubSequence)
					{
						pSubItem = ((DcmSequenceOfItems*)pSubSequence)->getItem(0);
						if(pSubItem)
						{
							DICOMUtil::UpdateTagInDataset(pSubItem, DCM_CodeValue, &pGPInfo->m_ReqProcCodeValue);
							DICOMUtil::UpdateTagInDataset(pSubItem, DCM_CodingSchemeDesignator, &pGPInfo->m_ReqProcCodeScheme);
							DICOMUtil::UpdateTagInDataset(pSubItem, DCM_CodeMeaning, &pGPInfo->m_ReqProcCodeMeaning);
						}
					}

					pItem->findAndGetSequence(DCM_ReferencedStudySequence, pRefSequence);

					//	The sequence doesn't already exists so let's create it.
					if(!pRefSequence)
					{
						pRefSequence = new DcmSequenceOfItems(DCM_ReferencedStudySequence);
						bAddSequence = true;
					}

					if(pRefSequence)
					{
						pGPInfo->m_sReferencedStudies.SetAtFirst();
						while(pGPInfo->m_sReferencedStudies.GetCurrent(pCurRefUID))
						{
							pNewItem = new DcmItem;
							if(pNewItem)
							{
								DICOMUtil::UpdateTagInDataset(pNewItem, DCM_ReferencedSOPClassUID, (char *)NULL);
								DICOMUtil::UpdateTagInDataset(pNewItem, DCM_ReferencedSOPInstanceUID, pCurRefUID);
								pRefSequence->insert(pNewItem);
							}
							pGPInfo->m_sReferencedStudies.Next();
						}
						if(bAddSequence)
						{
							pItem->insert(pRefSequence);
						}
					}
				}
			}
		}
		else
		{
			m_pModalityLogger->LogMsg("ERROR : Failed to clone dataset in C_FINDTransaction::CreateGPWorklistDataset.");
			m_pMailer->AddLine("Failed to clone dataset in C_FINDTransaction::CreateGPWorklistDataset.");
		}
		return pDataset;
	}
}
