/////////////////////////////////////////////////////////////////////////////
//	CFindService.cpp
/////////////////////////////////////////////////////////////////////////////

#include "CFindService.h"
#include "../Utils/DataFotmat.h"
#include "../Utils/DBGTool.h"
#include "../Utils/TimeUtil.h"

/////////////////////////////////////////////////////////////////////////////
//
namespace imapacs
{
	/////////////////////////////////////////////////////////////////////////
	//
	bool CFindService::NextFindResult()
	{
		bool bRetVal = false;

		if(m_hCurrentFindCursor)
		{
			if(m_pDb->NextRecord(m_hCurrentFindCursor))
			{
				bRetVal = true;
			}
			else
			{
				m_pDb->DeleteCursor(m_hCurrentFindCursor);
				m_hCurrentFindCursor = NULL;
			}
		}
		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	QUERY_STATUS CFindService::StartFind(FindData *pReqData, int iRootLevel)
	{
		QUERY_STATUS eRetVal = QS_OK_RESULT;

		//	Delete old cursor if it exists.
		if(m_hCurrentFindCursor)
		{
			m_pDb->DeleteCursor(m_hCurrentFindCursor);
			m_hCurrentFindCursor = NULL;
		}

		//	Get a new cursor for the current query.
		m_hCurrentFindCursor = m_pDb->GetCursor();
		if(m_hCurrentFindCursor)
		{
			if(strcmp(pReqData->sQLevel, "PATIENT") == 0)
			{
				m_pModalityLogger->LogMsg("Query level : PATIENT");

				eRetVal = DoFindPatient(pReqData);
			}
			else if(strcmp(pReqData->sQLevel, "STUDY") == 0)
			{
				m_pModalityLogger->LogMsg("Query level : STUDY");

				eRetVal = DoFindStudy(pReqData);
			}
			else if(strcmp(pReqData->sQLevel, "SERIES") == 0)
			{
				m_pModalityLogger->LogMsg("Query level : SERIES");

				eRetVal = DoFindSeries(pReqData);
			}
			else if(strcmp(pReqData->sQLevel, "IMAGE") == 0)
			{
				m_pModalityLogger->LogMsg("Query level : IMAGE");

				eRetVal = DoFindObject(pReqData);
			}
			else
			{
				//	Invalid level.
				m_pModalityLogger->LogMsg("Invalid query level [%s].", pReqData->sQLevel);
				m_pMailer->AddLine("Invalid query level [%s].", pReqData->sQLevel);

				eRetVal = QS_ERROR;
			}
		}
		else
		{
			m_pModalityLogger->LogMsg("Unable to get database cursor in CFindService::StartFind.");
			m_pMailer->AddLine("Unable to get database cursor in CFindService::StartFind.");

			eRetVal = QS_ERROR;
		}
		return eRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	QUERY_STATUS CFindService::DoFindPatient(FindData *pReqData)
	{
		bool bSqlRet       = true;
		char sDateTime[33] = {0};
		char sNumber[17]   = {0};
		StringBuffer select;
		StringBuffer where;
		StringBuffer tmp;
		DBGTool dbg;

		if(m_pCfg->GetProfiling())
			dbg.StartProfiling(m_pSeverLogger, "Composing sql.");

		select.Format("SELECT PAT_ID, PAT_OID, PAT_NAME, PAT_BIRTH_DATE, PAT_BIRTH_TIME, PAT_SEX, PAT_AGE, PAT_SIZE, " \
			          "PAT_WEIGHT, PAT_MOTHER " \
					  "FROM PATIENT ");

		DataFormat::ComposeWhereClause("PAT_ID", pReqData->PatId.Data, &where);

		DataFormat::ComposeWhereClause("PAT_FIRST_NAME", pReqData->PatFirstName.Data, &where);
		DataFormat::ComposeWhereClause("PAT_BIRTH_NAME", pReqData->PatLastName.Data, &where);

		DataFormat::ComposeWhereClause("PAT_OID", pReqData->PatOtherId.Data, &where);
		DataFormat::ComposeWhereClause("PAT_BIRTH_DATE", pReqData->PatBirthDate.Data, &where);
		DataFormat::ComposeWhereClause("PAT_BIRTH_DATE", pReqData->PatBirthTime.Data, &where);
		DataFormat::ComposeWhereClause("PAT_AGE", pReqData->PatAge.Data, &where);

		select += where.GetBuffer();
		select += " ORDER BY PAT_ID";

		if(m_pCfg->GetProfiling())
			dbg.EndProfiling(m_pSeverLogger, NULL, "Sql composed.");

		if(m_pCfg->GetModalityDebug())
		{
			m_pModalityLogger->LogMsg(" ");
			m_pModalityLogger->LogMsg("CFindService::DoFindPatient : %s", (char *)select);
			m_pModalityLogger->LogMsg(" ");
		}

		if(m_pCfg->GetProfiling())
			dbg.StartProfiling(m_pSeverLogger, "Executing sql.");

		bSqlRet = m_pDb->SQLExec(m_hCurrentFindCursor, select.GetBuffer(), "%s%s%s%s%s%s%s%s%s%s",
							   pReqData->PatId.Data,
							   pReqData->PatOtherId.Data,
							   pReqData->PatName.Data,
							   pReqData->PatBirthDate.Data,
							   pReqData->PatBirthTime.Data,
							   pReqData->PatSex.Data,
							   pReqData->PatAge.Data,
							   pReqData->PatSize.Data,
							   pReqData->PatWeight.Data,
							   pReqData->PatMotherName.Data);

		if(m_pCfg->GetProfiling())
			dbg.EndProfiling(m_pSeverLogger, (char *)select, "Sql executed.");

		if(!bSqlRet)
		{
			if(m_pDb->GetLastOciErrorCode(m_hCurrentFindCursor) == 0)
			{
				m_pDb->DeleteCursor(m_hCurrentFindCursor);
				m_hCurrentFindCursor = NULL;

				return QS_OK_NO_RESULT;
			}
			else
			{
				m_pModalityLogger->LogMsg("%s", m_pDb->GetLastOciErrorText(m_hCurrentFindCursor));
				m_pModalityLogger->LogMsg("SQL :  %s", select.GetBuffer());
				m_pModalityLogger->LogMsg("Unable to execute query");

				m_pMailer->AddLine("%s", m_pDb->GetLastOciErrorText(m_hCurrentFindCursor));
				m_pMailer->AddLine("SQL :  %s", select.GetBuffer());
				m_pMailer->AddLine("Unable to execute query");

				m_pDb->DeleteCursor(m_hCurrentFindCursor);
				m_hCurrentFindCursor = NULL;

				return QS_ERROR;
			}
		}
		return QS_OK_RESULT;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	QUERY_STATUS CFindService::DoFindStudy(FindData *pReqData)
	{
		bool bSqlRet         = true;
		QUERY_STATUS eRetVal = QS_OK_NO_RESULT;
		char sDateTime[33]   = {0};
		char sNumber[17]     = {0};
		StringBuffer select;
		StringBuffer where(" WHERE STU_STATUS != 'CREATED' AND STU_STATUS != 'CANCELED' AND STU_STATUS != 'SCHEDULED' AND STU_STATUS != 'ARRIVED' AND STU_PAT_ID = PAT_ID ");
		StringBuffer tmp;

		// [Ref:0000722] - D�but
		/*select.Format("SELECT PAT_NAME, PAT_ID, PAT_BIRTH_DATE, PAT_SEX, PAT_OID, STU_INSTANCE_UID, STU_ID, STU_DATE, STU_TIME, STU_ACCESSION_NUMBER, SUBSTR(STU_DESC, 0, 64), " \
					  "STU_READ_PHYSICIAN, STU_VIS_ADM_DIAGNOSIS_DESC, STU_MODALITIES_IN_STUDY FROM STUDY, PATIENT ");*/
		select.Format("SELECT PAT_NAME, PAT_ID, PAT_BIRTH_DATE, PAT_SEX, PAT_OID, STU_INSTANCE_UID, STU_ID, STU_DATE, STU_TIME, STU_ACCESSION_NUMBER, SUBSTR(STU_DESC, 0, 64), " \
					  "STU_READ_PHYSICIAN, STU_VIS_ADM_DIAGNOSIS_DESC, STU_MODALITIES_IN_STUDY, STU_OTHER_NUMBER, STU_REF_PHYSICIAN, PAT_NAM, PAT_MOTHER FROM STUDY, PATIENT ");
		// [Ref:0000722] - Fin

		/////////////////////////////////////////////////////////////////////
		//	Build where clause.
		DataFormat::ComposeWhereClause("PAT_ID", pReqData->PatId.Data, &where);
		DataFormat::ComposeWhereClause("PAT_SEX", pReqData->PatSex.Data, &where);

		DataFormat::ComposeWhereClause("PAT_FIRST_NAME", pReqData->PatFirstName.Data, &where);
		DataFormat::ComposeWhereClause("PAT_BIRTH_NAME", pReqData->PatLastName.Data, &where);

		DataFormat::ComposeWhereClause("STU_INSTANCE_UID", pReqData->StudyInstanceUID.Data, &where);
		DataFormat::ComposeWhereClause("STU_ID", pReqData->StudyId.Data, &where);

		if(pReqData->StudyDate.Data[0])
		{
			DataFormat::MakeQueryDates(pReqData->StudyDate.Data,
				                       pReqData->StudyTime.Data,
									   &where,
									   "STU_DATE",
									   "STU_TIME");
		}

		DataFormat::ComposeWhereClause("STU_ACCESSION_NUMBER", pReqData->StudyAccessionNumber.Data, &where);
		DataFormat::ComposeWhereClause("STU_REF_PHYSICIAN", pReqData->StudyRefPhysicianName.Data, &where);
		DataFormat::ComposeWhereClauseWithLike("STU_OTHER_NUMBER", pReqData->StudyOtherNumber.Data, &where); // [Ref:0001562]
		DataFormat::ComposeWhereClause("STU_DESC", pReqData->StudyDesc.Data, &where);
		DataFormat::ComposeWhereClauseWithLike("STU_MODALITIES_IN_STUDY", pReqData->StudyModalitiesInStudy.Data, &where);

		select += where.GetBuffer();
		select += " ORDER BY STU_DATE, STU_TIME";

		// [Ref:0000722] - D�but
		/*bSqlRet = m_pDb->SQLExec(m_hCurrentFindCursor, select.GetBuffer(), "%s%s%s%s%s%s%s%s%s%s%s%s%s%s",
		                         pReqData->PatName.Data,
								 pReqData->PatId.Data,
								 pReqData->PatBirthDate.Data,
								 pReqData->PatSex.Data,
								 pReqData->PatOtherId.Data,
								 pReqData->StudyInstanceUID.Data,
								 pReqData->StudyId.Data,
								 pReqData->StudyDate.Data,
								 pReqData->StudyTime.Data,
								 pReqData->StudyAccessionNumber.Data,
								 pReqData->StudyDesc.Data,
								 pReqData->StudyReadPhysicianName.Data,
								 pReqData->StudyAdmDiag.Data,
								 pReqData->StudyModalitiesInStudy.Data);*/

		bSqlRet = m_pDb->SQLExec(m_hCurrentFindCursor, select.GetBuffer(), "%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s",
		                         pReqData->PatName.Data,
								 pReqData->PatId.Data,
								 pReqData->PatBirthDate.Data,
								 pReqData->PatSex.Data,
								 pReqData->PatOtherId.Data,
								 pReqData->StudyInstanceUID.Data,
								 pReqData->StudyId.Data,
								 pReqData->StudyDate.Data,
								 pReqData->StudyTime.Data,
								 pReqData->StudyAccessionNumber.Data,
								 pReqData->StudyDesc.Data,
								 pReqData->StudyReadPhysicianName.Data,
								 pReqData->StudyAdmDiag.Data,
								 pReqData->StudyModalitiesInStudy.Data,
								 pReqData->StudyOtherNumber.Data, 
								 pReqData->StudyRefPhysicianName.Data,
								 pReqData->PatOtherId.Data,
								 pReqData->PatMotherName.Data);
		// [Ref:0000722] - Fin

		if(m_pCfg->GetModalityDebug())
		{
			m_pModalityLogger->LogMsg(" ");
			m_pModalityLogger->LogMsg("CFindService::DoFindStudy : %s", (char *)select);
			m_pModalityLogger->LogMsg(" ");
		}

		if(bSqlRet)
		{
			eRetVal = QS_OK_RESULT;
		}
		else
		{
			if(m_pDb->GetLastOciErrorCode(m_hCurrentFindCursor) == 0)
			{
				eRetVal = QS_OK_NO_RESULT;

				m_pDb->DeleteCursor(m_hCurrentFindCursor);
				m_hCurrentFindCursor = NULL;
			}
			else
			{
				m_pModalityLogger->LogMsg("%s", m_pDb->GetLastOciErrorText(m_hCurrentFindCursor));
				m_pModalityLogger->LogMsg("SQL :  %s", select.GetBuffer());
				m_pModalityLogger->LogMsg("Unable to execute query");

				m_pMailer->AddLine("%s", m_pDb->GetLastOciErrorText(m_hCurrentFindCursor));
				m_pMailer->AddLine("SQL :  %s", select.GetBuffer());
				m_pMailer->AddLine("Unable to execute query");

				m_pDb->DeleteCursor(m_hCurrentFindCursor);
				m_hCurrentFindCursor = NULL;

				eRetVal = QS_ERROR;
			}
		}
		return eRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	QUERY_STATUS CFindService::DoFindSeries(FindData *pReqData)
	{
		bool bSqlRet       = true;
		char sDateTime[33] = {0};
		char sNumber[17]   = {0};

		StringBuffer select;
		StringBuffer where(" WHERE (SER_STATUS IS NULL OR SER_STATUS = 'CREATED')"); // Il ne faut pas v�rifier si c'est des canceled �a ne fonctionne pas
																					 // comme les s�ries on un status NULL lorsqu'elle ne sont pas annul�es
                                                                                     // on recherche les IS NULL
		StringBuffer from;
		StringBuffer tmp;

		select.Format("SELECT DISTINCT SER_PAT_ID, SER_STU_INSTANCE_UID, SER_INSTANCE_UID, SER_NUMBER, SER_MODALITY, SER_DESC, " \
					  "SER_BODY_PART, SER_LATERALITY, SER_VIEW_POSITION, SER_DATE, SER_TIME, SER_NUMBER ");

		from = "FROM SERIES";

		/////////////////////////////////////////////////////////////////////
		//	Build where clause.
		DataFormat::ComposeWhereClause("SER_MODALITY", pReqData->SeriesModality.Data, &where);
		DataFormat::ComposeWhereClause("SER_INSTANCE_UID", pReqData->SeriesInstanceUID.Data, &where);
		DataFormat::ComposeWhereClause("SER_NUMBER", pReqData->SeriesNumber.Data, &where);
		DataFormat::ComposeWhereClause("SER_STU_INSTANCE_UID", pReqData->StudyInstanceUID.Data, &where);
		DataFormat::ComposeWhereClause("SER_BODY_PART", pReqData->SeriesBodyPart.Data, &where);
		DataFormat::ComposeWhereClause("SER_STU_INSTANCE_UID", pReqData->StudyAccessionNumber.Data, &where);
		select += from;
		select += where.GetBuffer();

		select += " ORDER BY TO_NUMBER(SERIES.SER_NUMBER)";

		if(m_pCfg->GetModalityDebug())
		{
			m_pModalityLogger->LogMsg(" ");
			m_pModalityLogger->LogMsg("CFindService::DoFindSeries : %s", (char *)select);
			m_pModalityLogger->LogMsg(" ");
		}

		bSqlRet = m_pDb->SQLExec(m_hCurrentFindCursor, 
			                     select.GetBuffer(), 
			                     "%s%s%s%s%s%s%s%s%s%s%s%s",
							     pReqData->PatId.Data,
							     pReqData->StudyInstanceUID.Data,
							     pReqData->SeriesInstanceUID.Data,
							     pReqData->SeriesNumber.Data,
							     pReqData->SeriesModality.Data,
							     pReqData->SeriesDesc.Data,
							     pReqData->SeriesBodyPart.Data,
							     pReqData->SeriesLaterality.Data,
							     pReqData->SeriesViewPosition.Data,
								 pReqData->SeriesDate.Data,
								 pReqData->SeriesTime.Data,
								 pReqData->SeriesNumber.Data);

		/////////////////////////////////////////////////////////////////////
		//	Execute the sql.
		if(!bSqlRet)
		{
			if(m_pDb->GetLastOciErrorCode(m_hCurrentFindCursor) == 0)
			{
				m_pDb->DeleteCursor(m_hCurrentFindCursor);
				m_hCurrentFindCursor = NULL;

				return QS_OK_NO_RESULT;
			}
			else
			{
				m_pModalityLogger->LogMsg("%s", m_pDb->GetLastOciErrorText(m_hCurrentFindCursor));
				m_pModalityLogger->LogMsg("SQL:  %s", select.GetBuffer());
				m_pModalityLogger->LogMsg("Unable to execute query");

				m_pMailer->AddLine("%s", m_pDb->GetLastOciErrorText(m_hCurrentFindCursor));
				m_pMailer->AddLine("SQL:  %s", select.GetBuffer());
				m_pMailer->AddLine("Unable to execute query");

				m_pDb->DeleteCursor(m_hCurrentFindCursor);
				m_hCurrentFindCursor = NULL;

				return QS_ERROR;
			}
		}
		return QS_OK_RESULT;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	QUERY_STATUS CFindService::DoFindObject(FindData *pReqData)
	{
		bool bSqlRet       = true;
		char sDateTime[33] = {0};
		char sNumber[17]   = {0};

		StringBuffer select;
		StringBuffer where(" WHERE COI_TYPE != 'CANCELED' AND COI_TYPE NOT LIKE 'SR_%%' AND COI_TYPE != 'AUDIO'");
		StringBuffer tmp;

		select.Format("SELECT DISTINCT COI_PAT_ID, COI_STUDY_INSTANCE_UID, COI_SERIES_INSTANCE_UID, COI_INSTANCE_NUMBER, " \
					  "COI_SOP_INSTANCE_UID, COI_SOP_CLASS_UID, COI_DATE, COI_TIME FROM COMPOSITE_OBJECT_INSTANCE");

		/////////////////////////////////////////////////////////////////////////
		//	Build where clause.
		DataFormat::ComposeWhereClause("COI_PAT_ID", pReqData->PatId.Data, &where);
		DataFormat::ComposeWhereClause("COI_STUDY_INSTANCE_UID", pReqData->StudyInstanceUID.Data, &where);
		DataFormat::ComposeWhereClause("COI_SERIES_INSTANCE_UID", pReqData->SeriesInstanceUID.Data, &where);
		DataFormat::ComposeWhereClause("COI_INSTANCE_NUMBER", pReqData->InstanceNumber.Data, &where);
		DataFormat::ComposeWhereClause("COI_SOP_INSTANCE_UID", pReqData->ObjectInstanceUID.Data, &where);

		select += where.GetBuffer();
//		select += " ORDER BY COI_DATE, COI_TIME, TO_NUMBER(COMPOSITE_OBJECT_INSTANCE.COI_INSTANCE_NUMBER)";
		select += " ORDER BY TO_NUMBER(COMPOSITE_OBJECT_INSTANCE.COI_INSTANCE_NUMBER)";

		if(m_pCfg->GetModalityDebug())
		{
			m_pModalityLogger->LogMsg(" ");
			m_pModalityLogger->LogMsg("CFindService::DoFindObject SQL : %s", (char *)select);
			m_pModalityLogger->LogMsg(" ");
		}

		/////////////////////////////////////////////////////////////////////////
		//	Execute the sql.
		bSqlRet = m_pDb->SQLExec(m_hCurrentFindCursor, select.GetBuffer(), "%s%s%s%s%s%s%s%s",
							   pReqData->PatId.Data,
							   pReqData->StudyInstanceUID.Data,
							   pReqData->SeriesInstanceUID.Data,
							   pReqData->ObjectInstanceNumber.Data,
							   pReqData->ObjectInstanceUID.Data,
							   pReqData->ObjectSopClasUID.Data,
							   pReqData->ObjectAcquisitionDate.Data,
							   pReqData->ObjectAcquisitionTime.Data);
		if(!bSqlRet)
		{
			if(m_pDb->GetLastOciErrorCode(m_hCurrentFindCursor) == 0)
			{
				m_pDb->DeleteCursor(m_hCurrentFindCursor);
				m_hCurrentFindCursor = NULL;

				return QS_OK_NO_RESULT;
			}
			else
			{
				m_pModalityLogger->LogMsg("%s", m_pDb->GetLastOciErrorText(m_hCurrentFindCursor));
				m_pModalityLogger->LogMsg("SQL :  %s", select.GetBuffer());
				m_pModalityLogger->LogMsg("Unable to execute query");

				m_pMailer->AddLine("%s", m_pDb->GetLastOciErrorText(m_hCurrentFindCursor));
				m_pMailer->AddLine("SQL :  %s", select.GetBuffer());
				m_pMailer->AddLine("Unable to execute query");

				m_pDb->DeleteCursor(m_hCurrentFindCursor);
				m_hCurrentFindCursor = NULL;

				return QS_ERROR;
			}
		}
		return QS_OK_RESULT;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	QUERY_STATUS CFindService::StartFindWorklist(const char *pAeTitle, const WLData *pInfo, WLData *pResult)
	{
		bool bSqlRet            = true;
		char modality[65]       = {0};
		char sFirstModality[17] = {0};
		char sDateTime[33]      = {0};
		char sNumber[17]        = {0};
		QUERY_STATUS eRet       = QS_OK_NO_RESULT;
		DBGTool dbg;

		StringBuffer select;
		StringBuffer where(" WHERE ");
		StringBuffer Tmp;

		if(m_hCurrentFindCursor)
		{
			m_pDb->DeleteCursor(m_hCurrentFindCursor);
			m_hCurrentFindCursor = NULL;
		}

		m_hCurrentFindCursor = m_pDb->GetCursor();
		if(!m_hCurrentFindCursor)
		{
			m_pModalityLogger->LogMsg("ERROR : Failed to get cursor in CFindService::StartFindWorklist.");
			m_pMailer->AddLine("Failed to get cursor in CFindService::StartFindWorklist.");

			return QS_ERROR;
		}

		/////////////////////////////////////////////////////////////////////
		//	We check wich modality has been configured in the database.
		if(GetWorklistModality(pAeTitle, modality))
		{
			// Modality forced by system
			m_pModalityLogger->LogMsg("Modality changed for: [%s] by system", modality);
			
		}
		else
		{
			//	We didn't get a modality, use the one in the request.
			strcpy(modality, pInfo->Modality);
		}

		if(m_pCfg->GetProfiling())
			dbg.StartProfiling(m_pSeverLogger, "Composing sql.");

		/////////////////////////////////////////////////////////////////////////
		//	Compose the sql for the modality worklist.
		select.Format("SELECT DISTINCT sf_ae_title, sf_sps_start_date, sf_sps_start_time, sf_perf_phys, sf_sps_description, sf_sps_id, sf_contrast_agent, " \
							          "sf_sps_status, sf_rp_id, sf_rp_description, sf_rp_code_examen, sf_rp_lateralite, sf_rp_internalcode, sf_stu_instance_uid, " \
							          "sf_stu_accession_number, sf_stu_ref_physician, sf_pat_name, sf_pat_id, sf_pat_sex, sf_pat_birth_date, " \
							          "sf_pat_birth_time, sf_modality " \
					  "FROM schedule_find");


		where += "sf_sps_status IN ( ";
		where += m_pCfg->GetWorklistStatus();
		where += ") ";

		DataFormat::MakeQueryDates(pInfo->ScheduledProcedureStepStartDate,
			                       pInfo->ScheduledProcedureStepStartTime,
								   &where,
								   "sf_sps_start_date",
								   "sf_sps_start_time");

		if(modality[0])
		{
			if(strstr(modality, ";")!=NULL)
			{
				char *tmpModality=NULL;
				tmpModality = strtok(modality, ";");
				where += " AND (";
				while(tmpModality)
				{
					//	If the modality is still empty at that
					//	point, we return at least the first found.
					if(!pResult->Modality[0])
						strcpy(pResult->Modality, tmpModality);

					Tmp.Format(" sf_modality LIKE '%%%s%%'",  tmpModality);
					where += Tmp.GetBuffer();
					Tmp.Clear();
					tmpModality = strtok(NULL, ";");
					if(tmpModality)
						where += " OR ";
				}
				where += " ) ";
			}
			else
			{
				Tmp.Format(" AND sf_modality LIKE '%%%s%%'", modality);
				where += Tmp.GetBuffer();
				Tmp.Clear();

				strcpy(pResult->Modality, modality);
			}
		}
		else
		{
			strcpy(pResult->Modality, pInfo->Modality);
		}

		DataFormat::ComposeWhereClause("sf_perf_phys", pInfo->ScheduledPerformingPhysicianName, &where);

		DataFormat::ComposeWhereClause("SF_PAT_FIRST_NAME", pInfo->PatientFirstName, &where);
		DataFormat::ComposeWhereClause("SF_PAT_BIRTH_NAME", pInfo->PatientLastName, &where);

		DataFormat::ComposeWhereClause("sf_pat_id", pInfo->PatientId, &where);

		if(where.GetBufferLength() > 7)
		{
			select += where.GetBuffer();
		}

		select += " ORDER BY SF_SPS_START_DATE, SF_RP_ID, SF_SPS_ID";

		if(m_pCfg->GetProfiling())
			dbg.EndProfiling(m_pSeverLogger, NULL, "Sql composed.");

		if(m_pCfg->GetModalityDebug())
		{
			m_pModalityLogger->LogMsg(" ");
			m_pModalityLogger->LogMsg("CFindService::StartFindWorklist SQL : %s", (char *)select);
			m_pModalityLogger->LogMsg(" ");
		}

		if(m_pCfg->GetProfiling())
			dbg.StartProfiling(m_pSeverLogger, "Executing sql.");

		bSqlRet = m_pDb->SQLExec(m_hCurrentFindCursor, select.GetBuffer(), 
							     "%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s",
							     pResult->ScheduledStationAeTitle,
							     pResult->ScheduledProcedureStepStartDate,
							     pResult->ScheduledProcedureStepStartTime,
							     pResult->ScheduledPerformingPhysicianName,
							     pResult->ScheduledProcedureStepDescription,
							     pResult->ScheduledProcedureStepId,
							     pResult->RequestedContrastAgent,
							     pResult->ScheduledProcedureStepStatus,
							     pResult->RequestedProcedureId,
							     pResult->RequestedProcedureDescription,
							     pResult->RequestedProcedureCode,
							     pResult->RequestedProcedureLaterality,
							     pResult->RequestedProcedureInternalCode,
							     pResult->StudyInstanceUID,
							     pResult->AccesionNumber,
							     pResult->ReferringPhysicianName,
							     pResult->PatientName,
							     pResult->PatientId,
							     pResult->PatientSex,
							     pResult->PatientBirthDate,
							     pResult->PatientBirthTime,
								 pResult->sSpsModality);

		if(m_pCfg->GetProfiling())
			dbg.EndProfiling(m_pSeverLogger, (char *)select, "Sql executed.");

		if(bSqlRet)
		{
			eRet = QS_OK_RESULT;
		}
		else
		{
			if(m_pDb->GetLastOciErrorCode(m_hCurrentFindCursor) == 0)
			{
				eRet = QS_OK_NO_RESULT;

				if(m_hCurrentFindCursor)
				{
					m_pDb->DeleteCursor(m_hCurrentFindCursor);
					m_hCurrentFindCursor = NULL;
				}
			}
			else
			{
				m_pModalityLogger->LogMsg("ERROR : %s", m_pDb->GetLastOciErrorText(m_hCurrentFindCursor));
				m_pModalityLogger->LogMsg("SQL :  %s", select.GetBuffer());

				m_pMailer->AddLine("%s", m_pDb->GetLastOciErrorText(m_hCurrentFindCursor));
				m_pMailer->AddLine("SQL :  %s", select.GetBuffer());

				if(m_hCurrentFindCursor)
				{
					m_pDb->DeleteCursor(m_hCurrentFindCursor);
					m_hCurrentFindCursor = NULL;
				}

				eRet = QS_ERROR;
			}
		}
		return eRet;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	// [Ref:0000966] - D�but
	/*QUERY_STATUS CFindService::StartFindGPWorlkist(GPWLData *pRequestData)
	{
		bool bSqlRet       = true;
		char sDateTime[33] = {0};
		char sNumber[17]   = {0};
		QUERY_STATUS eRet  = QS_OK_NO_RESULT;
		StringBuffer select;
		StringBuffer where;
		StringBuffer Tmp;
		DBGTool dbg;

		char Modality[17]={0}; // [Ref:0000722]

		if(m_hCurrentFindCursor)
		{
			m_pDb->DeleteCursor(m_hCurrentFindCursor);
			m_hCurrentFindCursor = NULL;
		}

		m_hCurrentFindCursor = m_pDb->GetCursor();
		if(m_hCurrentFindCursor)
		{
			// [Ref:0000722] - D�but
			/*select.Format("SELECT DISTINCT GSPS_SOP_INSTANCE_UID, GSPS_PROC_ID, GSPS_STU_INSTANCE_UID, GSPS_WORKITEM_CODE, GSPS_START_DATE, " \
								 "GSPS_STATION_NAME, GSPS_PRIORITY, GSPS_STATUS, GSPS_INPUT_AVAILABLE, GSPS_PATIENT_NAME, " \
								 "GSPS_PATIENT_ID, GSPS_PATIENT_SEX, TO_CHAR(GSPS_PATIENT_BIRTH_DATE, 'YYYYMMDD'), GSPS_PERFORMER_CODE, GSPS_MODALITIES_IN_STUDY, " \
								 "GSPS_ACCESSION_NUMBER, GSPS_PROCEDURE_CODE, GSPS_PROCEDURE_DESCRIPTION, GSPS_STUDY_DATE, GSPS_STUDY_TIME, " \
								 "GSPS_PERFORMER_CODE " \
						  "FROM GP_SCHEDULED_PROCEDURE_STEP");

			DataFormat::ComposeWhereClause("GSPS_ACCESSION_NUMBER", pRequestData->m_AccessionNumber.Data, &where);
			if(pRequestData->m_GPProcedureStepStatus.Data[0])
			{
				DataFormat::ComposeWhereClause("GSPS_STATUS", pRequestData->m_GPProcedureStepStatus.Data, &where);
			}
			else
			{
				/////////////////////////////////////////////////////////////////////
				//	If no status has been specified, we look for SCHEDULED as
				//	well as the IN PROGRESS SPS. This way, we can see the ones
				//	that are being worked on.
				DataFormat::ComposeWhereClause("GSPS_STATUS", "SCHEDULED", &where);
				DataFormat::ComposeWhereClause("GSPS_STATUS", "IN PROGRESS", &where);
			}
			DataFormat::ComposeWhereClause("GSPS_PRIORITY", pRequestData->m_GPScheduledProcStepPriority.Data, &where);
			DataFormat::ComposeWhereClause("GSPS_INPUT_AVAILABLE", pRequestData->m_InputAvailabilityFlag.Data, &where);

			//	This is a particular case.
			if(pRequestData->m_ModalitiesInStudy.Data[0])
			{
				if(where.GetBufferLength())
				{
					Tmp.Format(" AND GSPS_MODALITIES_IN_STUDY LIKE '%s%s%s'", "%",
																			  pRequestData->m_ModalitiesInStudy.Data,
																			  "%");
				}
				else
				{
					Tmp.Format(" WHERE GSPS_MODALITIES_IN_STUDY LIKE '%s%s%s'", "%", 
																				pRequestData->m_ModalitiesInStudy.Data,
																				"%");
				}
				where += Tmp.GetBuffer();
			}

			DataFormat::ComposeWhereClause("GSPS_PATIENT_BIRTH_DATE", pRequestData->m_PatientBirthDate.Data, &where);
			DataFormat::ComposeWhereClause("GSPS_PATIENT_ID", pRequestData->m_PatientId.Data, &where);
			DataFormat::ComposeWhereClause("GSPS_PATIENT_NAME", pRequestData->m_PatientName.Data, &where);
			DataFormat::ComposeWhereClause("GSPS_PATIENT_SEX", pRequestData->m_PatientSex.Data, &where);
			DataFormat::ComposeWhereClause("GSPS_PROCEDURE_CODE", pRequestData->m_ReqProcCodeValue.Data, &where);
			DataFormat::ComposeWhereClause("GSPS_PROCEDURE_DESCRIPTION", pRequestData->m_ReqProcDescription.Data, &where);
			DataFormat::ComposeWhereClause("GSPS_PROC_ID", pRequestData->m_ScheduledProcStepId.Data, &where);
			DataFormat::ComposeWhereClause("GSPS_PERFORMER_CODE", pRequestData->m_SchPerformerCodeValue.Data, &where);
			DataFormat::ComposeWhereClause("GSPS_STU_INSTANCE_UID", pRequestData->m_StudyInstanceUID.Data, &where);
			DataFormat::ComposeWhereClause("GSPS_WORKITEM_CODE", pRequestData->m_WorkItemCodeValue.Data, &where);

			if(pRequestData->m_SchProcStepStartDate.Data[0])
			{
				DataFormat::MakeQueryDates(pRequestData->m_SchProcStepStartDate.Data, 
					                       NULL,
										   &where,
										   "GSPS_STUDY_DATE",
										   " ");
			}*//*

			if(m_pCfg->GetProfiling())
				dbg.StartProfiling(m_pSeverLogger, "Composing sql.");

			if(pRequestData->m_ModalitiesInStudy.Data[0])
			{
				sprintf(Modality, "'%s' PS_MODALITY", pRequestData->m_ModalitiesInStudy.Data);
			}
			else
			{
				strcpy(Modality, "PS_MODALITY");
			}
			
			select.Format("SELECT '1' SOP_INSTANCE, '1' PROC_ID, STU_INSTANCE_UID, '110005' WORKITEM_CODE, STU_DATE START_DATE, " \
					"'' STATION_NAME, STU_PRIORITY_ID, 'SCHEDULED' STATUS, 'TRUE' INPUT_AVAILABLE, PAT_NAME, PAT_ID, PAT_SEX, PAT_BIRTH_DATE, " \
					"%s, STU_ACCESSION_NUMBER, STU_OTHER_NUMBER, STU_DESC, STU_DATE, STU_TIME, PAT_MOTHER, PAT_OID, STU_REF_PHYSICIAN " \
					"FROM STUDY,PATIENT, PROCEDURE_STEPS ", Modality);

			where.Format("WHERE STU_STATUS = 'VERIFIED' " \
				"AND PAT_ID = STU_PAT_ID " \
				"AND PS_EXA_UID = STU_EXA_UID  " \
				"AND PS_STEP_NUMBER ='1' " \
				"AND STU_DATE >= '20071015' ");

			DataFormat::ComposeWhereClause("STU_ACCESSION_NUMBER", pRequestData->m_AccessionNumber.Data, &where);
			/*if(pRequestData->m_GPProcedureStepStatus.Data[0])
			{
				DataFormat::ComposeWhereClause("GSPS_STATUS", pRequestData->m_GPProcedureStepStatus.Data, &where);
			}
			else
			{
				/////////////////////////////////////////////////////////////////////
				//	If no status has been specified, we look for SCHEDULED as
				//	well as the IN PROGRESS SPS. This way, we can see the ones
				//	that are being worked on.
				DataFormat::ComposeWhereClause("GSPS_STATUS", "SCHEDULED", &where);
				DataFormat::ComposeWhereClause("GSPS_STATUS", "IN PROGRESS", &where);
			}*//*
			DataFormat::ComposeWhereClause("STU_PRIORITY_ID", pRequestData->m_GPScheduledProcStepPriority.Data, &where);
			//DataFormat::ComposeWhereClause("GSPS_INPUT_AVAILABLE", pRequestData->m_InputAvailabilityFlag.Data, &where);

			//	This is a particular case.
			if(pRequestData->m_ModalitiesInStudy.Data[0])
			{
				strcpy(Modality, pRequestData->m_ModalitiesInStudy.Data); // [Ref:0000722]
				if(where.GetBufferLength())
				{
					Tmp.Format(" AND PS_MODALITY LIKE '%s%s%s'", "%",
																			  pRequestData->m_ModalitiesInStudy.Data,
																			  "%");
				}
				else
				{
					Tmp.Format(" WHERE PS_MODALITY LIKE '%s%s%s'", "%", 
																				pRequestData->m_ModalitiesInStudy.Data,
																				"%");
				}
				where += Tmp.GetBuffer();
			}

			DataFormat::ComposeWhereClause("PAT_BIRTH_DATE", pRequestData->m_PatientBirthDate.Data, &where);
			DataFormat::ComposeWhereClause("PAT_ID", pRequestData->m_PatientId.Data, &where);
//			DataFormat::ComposeWhereClause("PAT_NAME", pRequestData->m_PatientName.Data, &where);
			DataFormat::ComposeWhereClause("PAT_FIRST_NAME", pRequestData->m_PatFirstName.Data, &where);
			DataFormat::ComposeWhereClause("PAT_BIRTH_NAME", pRequestData->m_PatLastName.Data, &where);

			DataFormat::ComposeWhereClause("PAT_SEX", pRequestData->m_PatientSex.Data, &where);
			DataFormat::ComposeWhereClause("STU_OTHER_NUMBER", pRequestData->m_ReqProcCodeValue.Data, &where);
			DataFormat::ComposeWhereClause("STU_DESC", pRequestData->m_ReqProcDescription.Data, &where);
			//DataFormat::ComposeWhereClause("GSPS_PROC_ID", pRequestData->m_ScheduledProcStepId.Data, &where);
			//DataFormat::ComposeWhereClause("GSPS_PERFORMER_CODE", pRequestData->m_SchPerformerCodeValue.Data, &where);
			DataFormat::ComposeWhereClause("STU_INSTANCE_UID", pRequestData->m_StudyInstanceUID.Data, &where);
			//DataFormat::ComposeWhereClause("GSPS_WORKITEM_CODE", pRequestData->m_WorkItemCodeValue.Data, &where);

			if(pRequestData->m_SchProcStepStartDate.Data[0])
			{
				DataFormat::MakeQueryDates(pRequestData->m_SchProcStepStartDate.Data, 
					                       NULL,
										   &where,
										   "STU_DATE",
										   " ");
			}

			select += where.GetBuffer();
			select += " ORDER BY STU_DATE DESC, STU_TIME DESC";

			// [Ref:0000722] - Fin

			if(m_pCfg->GetProfiling())
				dbg.EndProfiling(m_pSeverLogger, NULL, "Sql composed.");

			if(m_pCfg->GetModalityDebug())
			{
				m_pModalityLogger->LogMsg(" ");
				m_pModalityLogger->LogMsg("CFindService::StartFindGPWorlkist SQL : %s", (char *)select);
				m_pModalityLogger->LogMsg(" ");
			}

			if(m_pCfg->GetProfiling())
				dbg.StartProfiling(m_pSeverLogger, "Executing sql.");

			bSqlRet = m_pDb->SQLExec(m_hCurrentFindCursor, select.GetBuffer(), 
														   "%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s", 
														   pRequestData->m_SOPInstanceUID.Data,
														   pRequestData->m_ScheduledProcStepId.Data,
														   pRequestData->m_StudyInstanceUID.Data,
														   pRequestData->m_WorkItemCodeValue.Data,
														   pRequestData->m_SchProcStepStartDate.Data,
														   pRequestData->m_SchStationNameCodeValue.Data,
														   pRequestData->m_GPScheduledProcStepPriority.Data,
														   pRequestData->m_GPProcedureStepStatus.Data,
														   pRequestData->m_InputAvailabilityFlag.Data,
														   pRequestData->m_PatientName.Data,
														   pRequestData->m_PatientId.Data,
														   pRequestData->m_PatientSex.Data,
														   pRequestData->m_PatientBirthDate.Data,
														   pRequestData->m_ModalitiesInStudy.Data,
														   pRequestData->m_AccessionNumber.Data,
														   pRequestData->m_ReqProcCodeValue.Data,
														   pRequestData->m_ReqProcDescription.Data,
														   pRequestData->m_StudyDate.Data,
														   pRequestData->m_StudyTime.Data,
														   pRequestData->m_PatMother.Data,
														   pRequestData->m_PatOID.Data,
														   pRequestData->m_StudyReferingPhysician.Data);
			if(m_pCfg->GetProfiling())
				dbg.EndProfiling(m_pSeverLogger, (char *)select, "Sql executed.");

			if(bSqlRet)
			{
				eRet = QS_OK_RESULT;
			}
			else
			{
				if(m_pDb->GetLastOciErrorCode(m_hCurrentFindCursor) == 0)
				{
					eRet = QS_OK_NO_RESULT;

					m_pDb->DeleteCursor(m_hCurrentFindCursor);
					m_hCurrentFindCursor = NULL;
				}
				else
				{
					m_pModalityLogger->LogMsg("%s", m_pDb->GetLastOciErrorText(m_hCurrentFindCursor));
					m_pModalityLogger->LogMsg("SQL :  %s", select.GetBuffer());
					m_pModalityLogger->LogMsg("Unable to execute query");

					m_pMailer->AddLine("%s", m_pDb->GetLastOciErrorText(m_hCurrentFindCursor));
					m_pMailer->AddLine("SQL:  %s", select.GetBuffer());
					m_pMailer->AddLine("Unable to execute query");

					m_pDb->DeleteCursor(m_hCurrentFindCursor);
					m_hCurrentFindCursor = NULL;

					eRet = QS_ERROR;
				}
			}
		}
		else
		{
			m_pModalityLogger->LogMsg("ERROR : Failed to get database cursor in CFindService::StartFindGPWorlkist.");
			m_pMailer->AddLine("ERROR : Failed to get database cursor in CFindService::StartFindGPWorlkist.");

			eRet = QS_ERROR;
		}
		return eRet;
	}*/

	QUERY_STATUS CFindService::StartFindGPWorlkist(GPWLData *pRequestData)
	{
		bool bSqlRet       = true;
		char sDateTime[33] = {0};
		char sNumber[17]   = {0};
		QUERY_STATUS eRet  = QS_OK_NO_RESULT;
		StringBuffer select;
		StringBuffer where;
		StringBuffer Tmp;
		DBGTool dbg;
//[ref:0001311]-d�but.
//		char staffWhereClause[2001]={0};
		char staffWhereClause[4096]={0};
//[ref:0001311]-fin.
		char staffAeTitle[17]={0};

		char Modality[17]={0};

		if(m_hCurrentFindCursor)
		{
			m_pDb->DeleteCursor(m_hCurrentFindCursor);
			m_hCurrentFindCursor = NULL;
		}

		m_hCurrentFindCursor = m_pDb->GetCursor();
		if(m_hCurrentFindCursor)
		{
			// On va chercher la liste des clause de modalit� et code d'examen
			// pour le staff et pour la station.
			select.Format("SELECT SGWC_WHERE_CLAUSE, SGWC_AE_TITLE " \
							"FROM STAFF_GPWL_WHERE_CLAUSE " \
							"WHERE SGWC_STAFF_ID = '%s'", pRequestData->m_SchPerformerCodeValue.Data);

			if (!m_pDb->SQLExec(m_hCurrentFindCursor, select.GetBuffer(), "%s%s", staffWhereClause, staffAeTitle))
			{
				if(m_pDb->GetLastOciErrorCode(m_hCurrentFindCursor) > 0)
				{
					m_pModalityLogger->LogMsg("%s", m_pDb->GetLastOciErrorText(m_hCurrentFindCursor));
					m_pModalityLogger->LogMsg("SQL :  %s", select.GetBuffer());
					m_pModalityLogger->LogMsg("Unable to execute query");

					m_pMailer->AddLine("%s", m_pDb->GetLastOciErrorText(m_hCurrentFindCursor));
					m_pMailer->AddLine("SQL:  %s", select.GetBuffer());
					m_pMailer->AddLine("Unable to execute query");
				}
			}

			if(m_pCfg->GetProfiling())
				dbg.StartProfiling(m_pSeverLogger, "Composing sql.");

			// [Ref:0001562] - D�but
			/*select.Format("SELECT STU_PAT_ID, PAT_NAME, PAT_BIRTH_DATE, PAT_SEX, " \
						"PAT_NAM, PAT_MOTHER, STU_INSTANCE_UID, " \
						"DECODE(STU_PRIORITY_ID,'1','HIGH','2','MEDIUM','LOW'), " \
						"STU_DESC, STU_ACCESSION_NUMBER, STU_OTHER_NUMBER, " \
						"STU_DATE, STU_TIME, " \
						"DECODE(INSTR(STU_REF_PHYSICIAN,'#'),0,STU_REF_PHYSICIAN,SUBSTR(STU_REF_PHYSICIAN,1,INSTR(STU_REF_PHYSICIAN,'#')-1)), " \
						"STU_MODALITIES_IN_STUDY, " \
						"'1' SOP_INSTANCE_UID, '1' SCHEDULED_PROCEDURE_STEP_ID, '110005' WORKITEM_CODE, 'SCHEDULED' STATUS, 'TRUE' INPUT_AVAILABLE " \
						"FROM STUDY, PATIENT ");
						
			where.Format("WHERE STU_PAT_ID = PAT_ID " \
						"AND STU_STATUS = 'VERIFIED' " \
						"AND (STU_LOCK_STAFF_ID = '%s' OR STU_LOCK_STAFF_ID IS NULL)", pRequestData->m_SchPerformerCodeValue.Data);*/

			select.Format("SELECT STU_PAT_ID, PAT_NAME, PAT_BIRTH_DATE, PAT_SEX, " \
						"PAT_NAM, PAT_MOTHER, STU_INSTANCE_UID, " \
						"DECODE(STU_PRIORITY_ID,'1','HIGH','2','MEDIUM','LOW'), " \
						"STU_DESC, STU_ACCESSION_NUMBER, STU_OTHER_NUMBER, " \
						"DECODE(STU_ARRIVAL_DATE, NULL, STU_DATE, STU_ARRIVAL_DATE), DECODE(STU_ARRIVAL_TIME, NULL, STU_TIME, STU_ARRIVAL_TIME), " \
						"DECODE(INSTR(STU_REF_PHYSICIAN,'#'),0,STU_REF_PHYSICIAN,SUBSTR(STU_REF_PHYSICIAN,1,INSTR(STU_REF_PHYSICIAN,'#')-1)), " \
						"STU_MODALITIES_IN_STUDY, " \
						"'1' SOP_INSTANCE_UID, '1' SCHEDULED_PROCEDURE_STEP_ID, '110005' WORKITEM_CODE, 'SCHEDULED' STATUS, 'TRUE' INPUT_AVAILABLE, " \
						"STU_LOCK_STAFF_ID " \
						"FROM STUDY, PATIENT, EXAMENS, BODY_PART_POSITION ");

			where.Format("WHERE STU_PAT_ID = PAT_ID " \
						"AND TRIM(REPLACE(REPLACE(STU_EXA_UID, 'D', ''), 'G', '')) = EXA_UID(+) " \
						"AND EXA_BODY_PART_POS_UID = BPP_UID(+)" \
						"AND STU_STATUS = 'VERIFIED' ");// \
						//[Ref:0002233] - "AND (STU_LOCK_STAFF_ID = '%s' OR STU_LOCK_STAFF_ID IS NULL)", pRequestData->m_SchPerformerCodeValue.Data);
			// [Ref:0001562] - Fin
						

			
						//"AND STU_STATUS IN ('COMPLETED','VERIFIED') " \

			DataFormat::ComposeWhereClause("PAT_ID", pRequestData->m_PatientId.Data, &where);
			DataFormat::ComposeWhereClause("PAT_FIRST_NAME", pRequestData->m_PatFirstName.Data, &where);
			DataFormat::ComposeWhereClause("PAT_BIRTH_NAME", pRequestData->m_PatLastName.Data, &where);
			// #RAMQ optionnel

			DataFormat::ComposeWhereClause("STU_PRIORITY_ID", pRequestData->m_GPScheduledProcStepPriority.Data, &where);
			
			if(pRequestData->m_SchProcStepStartDate.Data[0])
			{
				DataFormat::MakeQueryDates(pRequestData->m_SchProcStepStartDate.Data, 
					                       NULL,
										   &where,
										   "STU_DATE",
										   " ");
			}
			DataFormat::ComposeWhereClauseWithLike("STU_MODALITIES_IN_STUDY", pRequestData->m_ModalitiesInStudy.Data, &where);
			DataFormat::ComposeWhereClause("STU_ACCESSION_NUMBER", pRequestData->m_AccessionNumber.Data, &where);	

			/******************************************************/
			/* Passe a faire avec d'autre tables pour composer */
			DataFormat::ComposeWhereClause("STU_OTHER_NUMBER", pRequestData->m_ReqProcCodeValue.Data, &where);
			/******************************************************/





			select += where.GetBuffer();

			if (strlen(staffWhereClause)>0)
			{
				select += " ";
				select += staffWhereClause;
			}

			// Il faudrait v�rifier si c'est pour une station particuli�re
			// mais pour le moment on ne s'en occupe pas.

			//select += " ORDER BY STU_PRIORITY_ID, STU_DATE, STU_TIME"; // [Ref:0001562]
			select += " ORDER BY STU_PRIORITY_ID, STU_ARRIVAL_DATE, STU_ARRIVAL_TIME, STU_PAT_ID, BPP_POSITION NULLS LAST, STU_OTHER_NUMBER";  // [Ref:0001562]

			if(m_pCfg->GetProfiling())
				dbg.EndProfiling(m_pSeverLogger, NULL, "Sql composed.");

			if(m_pCfg->GetModalityDebug())
			{
				m_pModalityLogger->LogMsg(" ");
				m_pModalityLogger->LogMsg("CFindService::StartFindGPWorlkist SQL : %s", (char *)select);
				m_pModalityLogger->LogMsg(" ");
			}

			if(m_pCfg->GetProfiling())
				dbg.StartProfiling(m_pSeverLogger, "Executing sql.");

			bSqlRet = m_pDb->SQLExec(m_hCurrentFindCursor, select.GetBuffer(), 
															"%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s", 
															pRequestData->m_PatientId.Data,
															pRequestData->m_PatientName.Data,
															pRequestData->m_PatientBirthDate.Data,
															pRequestData->m_PatientSex.Data,
															pRequestData->m_PatOID.Data,
															pRequestData->m_PatMother.Data,
															pRequestData->m_StudyInstanceUID.Data,
															pRequestData->m_GPScheduledProcStepPriority.Data,
															pRequestData->m_ReqProcDescription.Data,
															pRequestData->m_AccessionNumber.Data,
															pRequestData->m_ReqProcCodeValue.Data,
															pRequestData->m_StudyDate.Data,
															pRequestData->m_StudyTime.Data,
															pRequestData->m_StudyReferingPhysician.Data,
															pRequestData->m_ModalitiesInStudy.Data,
															pRequestData->m_SOPInstanceUID.Data,
															pRequestData->m_ScheduledProcStepId.Data,
															pRequestData->m_WorkItemCodeValue.Data,
															pRequestData->m_GPProcedureStepStatus.Data,
															pRequestData->m_InputAvailabilityFlag.Data,
															pRequestData->m_ActualPerformerCodeValue.Data);

			if(m_pCfg->GetProfiling())
				dbg.EndProfiling(m_pSeverLogger, (char *)select, "Sql executed.");

			if(bSqlRet)
			{
				eRet = QS_OK_RESULT;
			}
			else
			{
				if(m_pDb->GetLastOciErrorCode(m_hCurrentFindCursor) == 0)
				{
					eRet = QS_OK_NO_RESULT;

					m_pDb->DeleteCursor(m_hCurrentFindCursor);
					m_hCurrentFindCursor = NULL;
				}
				else
				{
					m_pModalityLogger->LogMsg("%s", m_pDb->GetLastOciErrorText(m_hCurrentFindCursor));
					m_pModalityLogger->LogMsg("SQL :  %s", select.GetBuffer());
					m_pModalityLogger->LogMsg("Unable to execute query");

					m_pMailer->AddLine("%s", m_pDb->GetLastOciErrorText(m_hCurrentFindCursor));
					m_pMailer->AddLine("SQL:  %s", select.GetBuffer());
					m_pMailer->AddLine("Unable to execute query");

					m_pDb->DeleteCursor(m_hCurrentFindCursor);
					m_hCurrentFindCursor = NULL;

					eRet = QS_ERROR;
				}
			}
		}
		else
		{
			m_pModalityLogger->LogMsg("ERROR : Failed to get database cursor in CFindService::StartFindGPWorlkist.");
			m_pMailer->AddLine("ERROR : Failed to get database cursor in CFindService::StartFindGPWorlkist.");

			eRet = QS_ERROR;
		}
		return eRet;
	}
	// [Ref:0000966] - Fin

	/////////////////////////////////////////////////////////////////////////
	//
	bool CFindService::GetWorklistModality(const char *pAeTitle, char *pModality)
	{
		bool bRetVal   = false;
		HANDLE hCursor = NULL;
		StringBuffer sql;
		DBGTool dbg;

		hCursor = m_pDb->GetCursor();
		if(hCursor)
		{
			sql.Format("SELECT %s.GET_WORKLIST_MODALITY('%s') FROM DUAL", m_pCfg->GetPkgName(),
																		  pAeTitle);
			if(m_pCfg->GetProfiling())
				dbg.StartProfiling(m_pSeverLogger, "Executing sql.");

			if(m_pCfg->GetModalityDebug())
			{
				m_pModalityLogger->LogMsg(" ");
				m_pModalityLogger->LogMsg("CFindService::GetWorklistModality SQL : %s", (char *)sql);
				m_pModalityLogger->LogMsg(" ");
			}

			if(m_pDb->SQLExec(hCursor, sql.GetBuffer(), "%s", pModality))
			{
				if(pModality[0])
					bRetVal = true;
			}
			else
			{
				if(m_pDb->GetLastOciErrorCode(hCursor) > 0)
				{
					m_pModalityLogger->LogMsg("ERROR : %s", m_pDb->GetLastOciErrorText(hCursor));
					m_pModalityLogger->LogMsg("SQL :  %s", sql.GetBuffer());

					m_pMailer->AddLine("%s", m_pDb->GetLastOciErrorText(hCursor));
					m_pMailer->AddLine("SQL :  %s", sql.GetBuffer());
				}
			}
			m_pDb->DeleteCursor(hCursor);
			hCursor = NULL;

			if(m_pCfg->GetProfiling())
				dbg.EndProfiling(m_pSeverLogger, (char *)sql, "Sql executed.");
		}
		else
		{
			m_pModalityLogger->LogMsg("ERROR : Failed to get cursor from database in CFindService::GetWorklistModality");
			m_pMailer->AddLine("Failed to get cursor from database in CFindService::GetWorklistModality");
		}
		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	bool CFindService::UseInternalCode(const char *pAeTitle)
	{
		bool bRetVal    = false;
		char sResult[5] = {0};
		HANDLE hCursor  = NULL;
		StringBuffer sql;
		DBGTool dbg;

		hCursor = m_pDb->GetCursor();
		if(hCursor)
		{
			sql.Format("SELECT %s.USE_INTERNAL_CODE('%s') FROM DUAL", m_pCfg->GetPkgName(), pAeTitle);

			if(m_pCfg->GetProfiling())
				dbg.StartProfiling(m_pSeverLogger, "Executing sql.");

			if(m_pCfg->GetModalityDebug())
			{
				m_pModalityLogger->LogMsg(" ");
				m_pModalityLogger->LogMsg("CFindService::UseInternalCode SQL : %s", (char *)sql);
				m_pModalityLogger->LogMsg(" ");
			}

			if(m_pDb->SQLExec(hCursor, sql.GetBuffer(), "%s", sResult))
			{
				if(sResult[0] == '1')
				{
					bRetVal = true;
				}
			}
			else
			{
				m_pModalityLogger->LogMsg("%s", m_pDb->GetLastOciErrorText(hCursor));
				m_pModalityLogger->LogMsg("SQL :  %s", sql.GetBuffer());

				m_pMailer->AddLine("%s", m_pDb->GetLastOciErrorText(hCursor));
				m_pMailer->AddLine("SQL :  %s", sql.GetBuffer());
			}
			m_pDb->DeleteCursor(hCursor);
			hCursor = NULL;

			if(m_pCfg->GetProfiling())
				dbg.EndProfiling(m_pSeverLogger, (char *)sql, "Sql executed.");
		}
		else
		{
			m_pModalityLogger->LogMsg("ERROR : Failed to get cursor in CFindService::UseInternalCode.");
			m_pMailer->AddLine("Failed to get cursor in CFindService::UseInternalCode.");
		}
		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//	RETIRED (should be removed).
	bool CFindService::GetScalingFactorBySOPClass(const char *pSOPClass, 
		                                          double *pScalingFactorX)
	{
		bool bRetVal    = false;
		char sValue[33] = {0};
		HANDLE hCursor  = NULL;
		StringBuffer sql;

		hCursor = m_pDb->GetCursor();
		if(hCursor)
		{
			sql.Format("");
			if(m_pDb->SQLExec(hCursor, (char *)sql, "%s", sValue))
			{
				if(sValue[0])
				{
					(*pScalingFactorX) = atof(sValue);
				}
				bRetVal = true;
			}
			else
			{
				if(m_pDb->GetLastOciErrorCode(hCursor) != 0)
				{
					m_pModalityLogger->LogMsg("ERRROR : %s", m_pDb->GetLastOciErrorText(hCursor));
					m_pModalityLogger->LogMsg("SQL    : %s", (char *)sql);

					m_pMailer->AddLine("ERRROR : %s", m_pDb->GetLastOciErrorText(hCursor));
					m_pMailer->AddLine("SQL    : %s", (char *)sql);
				}
				else
				{
					m_pModalityLogger->LogMsg("No scaling factor was found for SOP Class : %s", pSOPClass);
				}
			}
			m_pDb->DeleteCursor(hCursor);
			hCursor = NULL;
		}
		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	QUERY_STATUS CFindService::GetReferencedStudies(GPWLData *pRequestData)
	{
		QUERY_STATUS eRet          = QS_ERROR;
		bool bSqlRet               = false;
		TagData *pCurUID           = NULL;
		int iCnt                   = 0;
		int iMax                   = 0;
		char sExamCode[11]         = {0};
		char sNbRef[11]            = {0};
		char sStudyInstanceUID[65] = {0};
		HANDLE hHistorical         = NULL;
		HANDLE hStudy              = NULL;
		DBGTool dbg;

		StringBuffer select;

		hHistorical = m_pDb->GetCursor();
		if(hHistorical)
		{
			hStudy = m_pDb->GetCursor();
			if(hStudy)
			{
				select.Format("SELECT HI_REF_EXAMCODE, HI_ANTERIOR_NUMBEROF FROM HISTORICAL_OBJECTS WHERE HI_EXAMCODE = '%s'", 
					          pRequestData->m_ReqProcCodeValue.Data);

				if(m_pCfg->GetProfiling())
					dbg.StartProfiling(m_pSeverLogger, "Executing sql.");

				if(m_pCfg->GetModalityDebug())
				{
					m_pModalityLogger->LogMsg(" ");
					m_pModalityLogger->LogMsg("CFindService::GetReferencedStudies SQL : %s", (char *)select);
					m_pModalityLogger->LogMsg(" ");
				}

				bSqlRet = m_pDb->SQLExec(hHistorical, select.GetBuffer(), "%s%s", sExamCode, sNbRef);

				if(m_pCfg->GetProfiling())
					dbg.EndProfiling(m_pSeverLogger, (char *)select, "Sql executed.");

				if(bSqlRet)
				{
					while(bSqlRet)
					{
						iCnt = 0;
						iMax = atoi(sNbRef);
						select.Format("SELECT STU_INSTANCE_UID FROM STUDY WHERE STU_PAT_ID = '%s' AND STU_OTHER_NUMBER = '%s' AND STU_STATUS = 'READ' ORDER BY STU_DATE DESC", 
									  pRequestData->m_PatientId.Data,
									  sExamCode);

						if(m_pCfg->GetProfiling())
							dbg.StartProfiling(m_pSeverLogger, "Executing sql.");

						if(m_pCfg->GetModalityDebug())
						{
							m_pModalityLogger->LogMsg(" ");
							m_pModalityLogger->LogMsg("CFindService::GetReferencedStudies SQL : %s", (char *)select);
							m_pModalityLogger->LogMsg(" ");
						}

						if(m_pDb->SQLExec(hStudy, select.GetBuffer(), "%s", sStudyInstanceUID))
						{
							if(m_pCfg->GetProfiling())
								dbg.EndProfiling(m_pSeverLogger, (char *)select, "Sql executed.");

							eRet = QS_OK_RESULT;
							do
							{
								pCurUID = new TagData;
								if(pCurUID)
								{
									strcpy(pCurUID->Data, sStudyInstanceUID);
									pRequestData->m_sReferencedStudies.Add(pCurUID);
								}
								++iCnt;
							}while(m_pDb->NextRecord(hStudy) && (iCnt < iMax));
						}
						else
						{
							if(m_pCfg->GetProfiling())
								dbg.EndProfiling(m_pSeverLogger, (char *)select, "Sql executed.");

							if(m_pDb->GetLastOciErrorCode(hStudy) == 0)
							{
								eRet = QS_OK_NO_RESULT;
							}
							else
							{
								m_pModalityLogger->LogMsg("%s", m_pDb->GetLastOciErrorText(hStudy));
								m_pModalityLogger->LogMsg("SQL:  %s", select.GetBuffer());
								m_pModalityLogger->LogMsg("Unable to execute query");

								m_pMailer->AddLine("%s", m_pDb->GetLastOciErrorText(hStudy));
								m_pMailer->AddLine("SQL :  %s", select.GetBuffer());
								m_pMailer->AddLine("Unable to execute query");

								eRet = QS_ERROR;
								break;
							}
						}
						bSqlRet = m_pDb->NextRecord(hHistorical);
					}
				}
				else
				{
					if(m_pDb->GetLastOciErrorCode(hHistorical) == 0)
					{
						eRet = QS_OK_NO_RESULT;
					}
					else
					{
						m_pModalityLogger->LogMsg("%s", m_pDb->GetLastOciErrorText(hHistorical));
						m_pModalityLogger->LogMsg("SQL:  %s", select.GetBuffer());
						m_pModalityLogger->LogMsg("Unable to execute query");

						m_pMailer->AddLine("%s", m_pDb->GetLastOciErrorText(hHistorical));
						m_pMailer->AddLine("SQL :  %s", select.GetBuffer());
						m_pMailer->AddLine("Unable to execute query");

						eRet = QS_ERROR;
					}
				}
				m_pDb->DeleteCursor(hStudy);
				hStudy = NULL;
			}
			else
			{
				m_pModalityLogger->LogMsg("Failed to get hStudy cursor in CFindService::GetReferencedStudies.");
				m_pMailer->AddLine("Failed to get hStudy cursor in CFindService::GetReferencedStudies.");

				eRet = QS_ERROR;
			}
			m_pDb->DeleteCursor(hHistorical);
			hHistorical = NULL;
		}
		else
		{
			m_pModalityLogger->LogMsg("Failed to get hHistorical cursor in CFindService::GetReferencedStudies.");
			m_pMailer->AddLine("Failed to get hHistorical cursor in CFindService::GetReferencedStudies.");

			eRet = QS_ERROR;
		}
		return eRet;
	}
}
