/////////////////////////////////////////////////////////////////////////////
//	CStoreService.cpp
/////////////////////////////////////////////////////////////////////////////

#include "../Utils/DataFotmat.h"
#include "CStoreService.h"

/////////////////////////////////////////////////////////////////////////////
//
namespace imapacs
{
	/////////////////////////////////////////////////////////////////////////
	// [ref:0002072] We check if the study is in the INCOMING_STUDY table and need to be r�assign to another Patient ID
	bool CStoreService::CheckPatientId(char *pPatientId, const char *pStudyUid)
	{
		bool bRetVal       = false;
		char sResult[65]    = {0};
		HANDLE hCursor     = NULL;
		StringBuffer sql;
		DBGTool dbg;

		hCursor = m_pDb->GetCursor();
		if(hCursor)
		{
			if(m_pCfg->GetProfiling())
				dbg.StartProfiling(m_pSeverLogger, "Executing sql CHECK_PATIENT.");

			sql.Format("SELECT %s.CHECK_INCOMING_PATIENT_ID('%s', '%s') FROM DUAL", m_pCfg->GetPkgName(), pStudyUid, pPatientId);

			if(m_pCfg->GetModalityDebug())
			{
				m_pModalityLogger->LogMsg(" ");
				m_pModalityLogger->LogMsg("CStoreService::CheckPatientId SQL : %s", (char *)sql);
				m_pModalityLogger->LogMsg(" ");
			}

			if(m_pDb->SQLExec(hCursor, sql.GetBuffer(), "%s", sResult))
			{
				if(strlen(sResult))
					strcpy(pPatientId, sResult);
			}
			else
			{
				if(m_pDb->GetLastOciErrorCode(hCursor) != 0)
				{
					m_pModalityLogger->LogMsg("%s", m_pDb->GetLastOciErrorText(hCursor));
					m_pModalityLogger->LogMsg("SQL:  %s", sql.GetBuffer());

					m_pMailer->AddLine("%s", m_pDb->GetLastOciErrorText(hCursor));
					m_pMailer->AddLine("SQL:  %s", sql.GetBuffer());
				}
			}
			m_pDb->DeleteCursor(hCursor);
			hCursor = NULL;

			if(m_pCfg->GetProfiling())
				dbg.EndProfiling(m_pSeverLogger, (char *)sql, "Sql executed.");
		}
		else
		{
			m_pModalityLogger->LogMsg("ERROR : failed to get cursor in CStoreService::CheckPatientId.");
			m_pMailer->AddLine("Failed to get cursor in DataInterface::CheckPatientId");
		}
		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	bool CStoreService::PatientExists(const char *pPatientId)
	{
		bool bRetVal       = false;
		char sResult[5]    = {0};
		HANDLE hCursor     = NULL;
		StringBuffer sql;
		DBGTool dbg;

		hCursor = m_pDb->GetCursor();
		if(hCursor)
		{
			if(m_pCfg->GetProfiling())
				dbg.StartProfiling(m_pSeverLogger, "Executing sql VALIDATE_PATIENT.");

			sql.Format("SELECT %s.VALIDATE_PATIENT('%s') FROM DUAL", m_pCfg->GetPkgName(), pPatientId);

			if(m_pCfg->GetModalityDebug())
			{
				m_pModalityLogger->LogMsg(" ");
				m_pModalityLogger->LogMsg("CStoreService::PatientExists SQL : %s", (char *)sql);
				m_pModalityLogger->LogMsg(" ");
			}

			if(m_pDb->SQLExec(hCursor, sql.GetBuffer(), "%s", sResult))
			{
				if(sResult[0] == '1')	//	The patient exists.
				{
					bRetVal = true;
				}
			}
			else
			{
				if(m_pDb->GetLastOciErrorCode(hCursor) != 0)
				{
					m_pModalityLogger->LogMsg("%s", m_pDb->GetLastOciErrorText(hCursor));
					m_pModalityLogger->LogMsg("SQL:  %s", sql.GetBuffer());

					m_pMailer->AddLine("%s", m_pDb->GetLastOciErrorText(hCursor));
					m_pMailer->AddLine("SQL:  %s", sql.GetBuffer());
				}
			}
			m_pDb->DeleteCursor(hCursor);
			hCursor = NULL;

			if(m_pCfg->GetProfiling())
				dbg.EndProfiling(m_pSeverLogger, (char *)sql, "Sql executed.");
		}
		else
		{
			m_pModalityLogger->LogMsg("ERROR : failed to get cursor in CStoreService::PatientExists.");
			m_pMailer->AddLine("Failed to get cursor in DataInterface::PatientExists");
		}
		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	bool CStoreService::CreatePatient(PatientData *pInfo)
	{
		bool bRetVal           = false;
		bool bHasLastName      = false;
		HANDLE hCursor         = NULL;
		char sPatFirstName[65] = {0};
		char sPatLastName[65]  = {0};
		char sDateTime[33]     = {0};
		char sNumber[17]       = {0};
		int iPatNameLen        = 0;
		int i                  = 0;
		int j                  = 0;
		StringBuffer sql;
		DBGTool dbg;

		hCursor = m_pDb->GetCursor();
		if(hCursor)
		{
			iPatNameLen = strlen(pInfo->sPatientName);
			/////////////////////////////////////////////////////////////////
			//	Parse first name and last name from
			//	dicom field.
			for(i = 0, j = 0; i < iPatNameLen; ++i, ++j)
			{
				if(pInfo->sPatientName[i] == '^')
				{
					bHasLastName = true;
					{
						++i;
						j = 0;
					}
					if(i >= iPatNameLen)
						break;
				}

				if(bHasLastName)
					sPatFirstName[j] = pInfo->sPatientName[i];
				else
					sPatLastName[j] = pInfo->sPatientName[i];
			}

			if(m_pCfg->GetProfiling())
				dbg.StartProfiling(m_pSeverLogger, "Executing sql.");

			sql.Format("CALL %s.CREATE_PATIENT('%s', '%s', '%s', TO_DATE('%s', 'YYYYMMDD'), '%s', '%s')", m_pCfg->GetPkgName(),
																										  pInfo->sPatientId,
																										  sPatLastName,
																										  sPatFirstName,
																										  pInfo->sPatientBirthDate,
																										  pInfo->sPatientSex,
																										  pInfo->sPatientOtherId);

			if(m_pCfg->GetModalityDebug())
			{
				m_pModalityLogger->LogMsg(" ");
				m_pModalityLogger->LogMsg("CStoreService::CreatePatient SQL : %s", (char *)sql);
				m_pModalityLogger->LogMsg(" ");
			}

			if(m_pDb->SQLExec(hCursor, sql.GetBuffer()))
			{
				bRetVal = true;
			}
			else
			{
				if(m_pDb->GetLastOciErrorCode(hCursor) != 0)
				{
					m_pModalityLogger->LogMsg("%s", m_pDb->GetLastOciErrorText(hCursor));
					m_pModalityLogger->LogMsg("SQL:  %s", sql.GetBuffer());

					m_pMailer->AddLine("%s", m_pDb->GetLastOciErrorText(hCursor));
					m_pMailer->AddLine("SQL:  %s", sql.GetBuffer());
				}
			}
			m_pDb->DeleteCursor(hCursor);
			hCursor = NULL;

			if(m_pCfg->GetProfiling())
				dbg.EndProfiling(m_pSeverLogger, (char *)sql, "Sql executed.");
		}
		else
		{
			m_pModalityLogger->LogMsg("ERROR : Failed to get cursor in CStoreService::CreatePatient");
			m_pMailer->AddLine("Failed to get cursor in DataInterface::CreatePatient");
		}
		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	bool CStoreService::GetStudyAccessionNumber(const char *pStudyInstanceUID, char *pAccessionNb)
	{
		bool bRetVal = false;
		HANDLE hCursor = NULL;
		StringBuffer sql;
		DBGTool dbg;

		hCursor = m_pDb->GetCursor();
		if(hCursor)
		{
			if(m_pCfg->GetProfiling())
				dbg.StartProfiling(m_pSeverLogger, "Executing sql.");

			sql.Format("SELECT %s.GET_STUDY_ACCESSION_NUMBER('%s') FROM DUAL", m_pCfg->GetPkgName(), pStudyInstanceUID);

			if(m_pCfg->GetModalityDebug())
			{
				m_pModalityLogger->LogMsg(" ");
				m_pModalityLogger->LogMsg("CStoreService::GetStudyAccessionNumber SQL : %s", (char *)sql);
				m_pModalityLogger->LogMsg(" ");
			}

			if(m_pDb->SQLExec(hCursor, sql.GetBuffer(), "%s", pAccessionNb))
			{
				if(strcmp(pAccessionNb, "-1")!=0)
				{
					bRetVal = true;
				}
			}
			else
			{
				m_pModalityLogger->LogMsg("%s", m_pDb->GetLastOciErrorText(hCursor));
				m_pModalityLogger->LogMsg("SQL:  %s", sql.GetBuffer());

				m_pMailer->AddLine("%s", m_pDb->GetLastOciErrorText(hCursor));
				m_pMailer->AddLine("SQL:  %s", sql.GetBuffer());
			}
			m_pDb->DeleteCursor(hCursor);
			hCursor = NULL;
		}
		else
		{
			m_pModalityLogger->LogMsg("ERROR : Failed to get cursor in CStoreService::GetStudyAccessionNumber.");
			m_pMailer->AddLine("Failed to get cursor in CStoreService::GetStudyAccessionNumber");
		}

		if(m_pCfg->GetProfiling())
			dbg.EndProfiling(m_pSeverLogger, (char *)sql, "Sql executed.");

		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	bool CStoreService::GetNewAccessionNumber(char *pAccessionNb)
	{
		bool bRetVal = false;
		HANDLE hCursor = NULL;
		StringBuffer sql;
		DBGTool dbg;

		hCursor = m_pDb->GetCursor();
		if(hCursor)
		{
			if(m_pCfg->GetProfiling())
				dbg.StartProfiling(m_pSeverLogger, "Executing sql.");

			sql.Format("SELECT %s.GET_NEW_ACCESSION_NUMBER() FROM DUAL", m_pCfg->GetPkgName());

			if(m_pCfg->GetModalityDebug())
			{
				m_pModalityLogger->LogMsg(" ");
				m_pModalityLogger->LogMsg("CStoreService::GetNewAccessionNumber SQL : %s", (char *)sql);
				m_pModalityLogger->LogMsg(" ");
			}

			if(m_pDb->SQLExec(hCursor, sql.GetBuffer(), "%s", pAccessionNb))
			{
				bRetVal = true;
			}
			else
			{
				m_pModalityLogger->LogMsg("%s", m_pDb->GetLastOciErrorText(hCursor));
				m_pModalityLogger->LogMsg("SQL :  %s", sql.GetBuffer());

				m_pMailer->AddLine("%s", m_pDb->GetLastOciErrorText(hCursor));
				m_pMailer->AddLine("SQL :  %s", sql.GetBuffer());
			}
			m_pDb->DeleteCursor(hCursor);
			hCursor = NULL;

			if(m_pCfg->GetProfiling())
				dbg.EndProfiling(m_pSeverLogger, (char *)sql, "Sql executed.");
		}
		else
		{
			m_pModalityLogger->LogMsg("ERROR : Failed to get cursor in CStoreService::GetNewAccessionNumber");
			m_pMailer->AddLine("ERROR : Failed to get cursor in DataInterface::GetNewAccessionNumber");
		}
		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	bool CStoreService::UpdateStudyDate(StudyData *pStudyInfo)
	{
		bool bRetVal    = false;
		HANDLE hCursor  = NULL;
		StringBuffer sql;
		DBGTool dbg;

		hCursor = m_pDb->GetCursor();
		if(hCursor)
		{
			if(m_pCfg->GetProfiling())
				dbg.StartProfiling(m_pSeverLogger, "Executing sql.");

			if(m_pDb->SQLExec(hCursor, "SELECT TO_CHAR(SYSDATE,'YYYYMMDD') FROM DUAL", "%s", pStudyInfo->sStudyDate))
			{
				sql.Format("CALL %s.UPDATE_STUDY_DATE('%s','%s')", m_pCfg->GetPkgName(), 
					                                               pStudyInfo->sStudyInstanceUID, 
																   pStudyInfo->sStudyDate);

				if(m_pCfg->GetModalityDebug())
				{
					m_pModalityLogger->LogMsg(" ");
					m_pModalityLogger->LogMsg("CStoreService::UpdateStudyDate SQL : %s", (char *)sql);
					m_pModalityLogger->LogMsg(" ");
				}

				if(m_pDb->SQLExec(hCursor, sql.GetBuffer()))
				{
					bRetVal = true;
				}
				else
				{
					m_pModalityLogger->LogMsg("%s", m_pDb->GetLastOciErrorText(hCursor));	
					m_pModalityLogger->LogMsg("SQL:  %s", sql.GetBuffer());

					m_pMailer->AddLine("%s", m_pDb->GetLastOciErrorText(hCursor));
					m_pMailer->AddLine("SQL:  %s", sql.GetBuffer());
				}
			}
			else
			{
				m_pModalityLogger->LogMsg("%s", m_pDb->GetLastOciErrorText(hCursor));
				m_pModalityLogger->LogMsg("SQL :  %s", sql.GetBuffer());

				m_pMailer->AddLine("%s", m_pDb->GetLastOciErrorText(hCursor));
				m_pMailer->AddLine("SQL :  %s", sql.GetBuffer());
			}
			m_pDb->DeleteCursor(hCursor);
			hCursor = NULL;

			if(m_pCfg->GetProfiling())
				dbg.EndProfiling(m_pSeverLogger, (char *)sql, "Sql executed.");
		}
		else
		{
			m_pModalityLogger->LogMsg("ERROR : Failed to get cursor in  DataInterface::UpdateStudyDate.");
			m_pMailer->AddLine("Failed to get cursor in  DataInterface::UpdateStudyDate");
		}
		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	bool CStoreService::CreateStudy(StudyData *pStudy, PatientData *pPatient, bool bIsTransient)
	{
		bool bRetVal             = false;
		HANDLE hCursor           = NULL;
		char strDate[65]         = {0};
		char transientFlag[4]    = {0};
		char transientSource[65] = {0};
		char sDateTime[33]       = {0};
		char sNumber[17]         = {0};
		StringBuffer sql;
		DBGTool dbg;

		hCursor = m_pDb->GetCursor();
		if(hCursor)
		{
			if(m_pCfg->GetProfiling())
				dbg.StartProfiling(m_pSeverLogger, "Composing sql.");

			DataFormat::ConvertDateTime(strDate, pStudy->sStudyDate, pStudy->sStudyTime);

			//	Fetch the necessary informations for transient mode.
			if(bIsTransient)
			{
				strcpy(transientFlag, "1");
				sprintf(transientSource, "'%s'", pStudy->sInstitutionName);
			}
			else
			{
				strcpy(transientFlag, "0");
				strcpy(transientSource, "NULL");
			}

			sql.Format("CALL %s.CREATE_STUDY('%s', '%s', '%s', '%s', '%s', %s, '%s', '%s', '%s', '%s', '%s', %s, %s, '%s')",
					   m_pCfg->GetPkgName(),
					   pStudy->sStudyInstanceUID,
					   pPatient->sPatientId,
					   pStudy->sStudyId,
					   pStudy->sAccessionNumber,
					   pStudy->sStudyDesc,
					   strDate,
					   pStudy->sStudyRefPhysicianName,
					   pStudy->sPatientAge,
					   pStudy->sPatientSize,
					   pStudy->sPatientWeight,
					   transientFlag,				//	Flag for transient. Should come from configuration.
					   transientSource,				//	Transient source.
					   "NULL",						//	Transient expiration (configuration).
					   pStudy->sSendingAeTitle);

			if(m_pCfg->GetProfiling())
				dbg.EndProfiling(m_pSeverLogger, NULL, "Sql composing.");

			if(m_pCfg->GetProfiling())
				dbg.StartProfiling(m_pSeverLogger, "Executing sql.");

			if(m_pCfg->GetModalityDebug())
			{
				m_pModalityLogger->LogMsg(" ");
				m_pModalityLogger->LogMsg("CStoreService::CreateStudy SQL : %s", (char *)sql);
				m_pModalityLogger->LogMsg(" ");
			}

			if(m_pDb->SQLExec(hCursor, sql.GetBuffer()))
			{
				bRetVal = true;
			}
			else
			{
				m_pModalityLogger->LogMsg("ERROR : %s", m_pDb->GetLastOciErrorText(hCursor));
				m_pModalityLogger->LogMsg("SQL :  %s", sql.GetBuffer());

				m_pMailer->AddLine("%s", m_pDb->GetLastOciErrorText(hCursor));
				m_pMailer->AddLine("SQL :  %s", sql.GetBuffer());
			}
			m_pDb->DeleteCursor(hCursor);
			hCursor = NULL;

			if(m_pCfg->GetProfiling())
				dbg.EndProfiling(m_pSeverLogger, (char *)sql, "Sql executed.");
		}
		else
		{
			m_pModalityLogger->LogMsg("ERROR : Failed to get cursor in DataInterface::CreateStudy.");
			m_pMailer->AddLine("Failed to get cursor.");
		}
		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	bool CStoreService::GetModalitiesInStudy(const char *pStudyInstanceUID, char *pModalities)
	{
		bool bRetVal   = false;
		HANDLE hCursor = NULL;
		StringBuffer sql;

		hCursor = m_pDb->GetCursor();
		if(hCursor)
		{
			sql.Format("SELECT STU_MODALITIES_IN_STUDY FROM STUDY WHERE STU_INSTANCE_UID = '%s'", pStudyInstanceUID);

			if(m_pCfg->GetModalityDebug())
			{
				m_pModalityLogger->LogMsg(" ");
				m_pModalityLogger->LogMsg("CStoreService::GetModalitiesInStudy SQL : %s", (char *)sql);
				m_pModalityLogger->LogMsg(" ");
			}

			if(m_pDb->SQLExec(hCursor, sql.GetBuffer(), "%s", pModalities))
			{
				bRetVal = true;
			}
			else
			{
				m_pModalityLogger->LogMsg("ERROR : %s", m_pDb->GetLastOciErrorText(hCursor));
				m_pModalityLogger->LogMsg("SQL :  %s", sql.GetBuffer());

				m_pMailer->AddLine("%s", m_pDb->GetLastOciErrorText(hCursor));
				m_pMailer->AddLine("SQL :  %s", sql.GetBuffer());
			}
			m_pDb->DeleteCursor(hCursor);
			hCursor = NULL;
		}
		else
		{
			m_pModalityLogger->LogMsg("ERROR : Failed to get cursor in DataInterface::GetModalitiesInStudy.");
			m_pMailer->AddLine("Failed to get cursor in DataInterface::GetModalitiesInStudy");
		}
		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	bool CStoreService::UpdateModalitiesInStudy(const char *pStudyInstanceUID, const char *pModalities)
	{
		bool bRetVal   = false;
		HANDLE hCursor = NULL;
		StringBuffer sql;
		DBGTool dbg;

		hCursor = m_pDb->GetCursor();
		if(hCursor)
		{
			if(m_pCfg->GetProfiling())
				dbg.StartProfiling(m_pSeverLogger, "Executing sql.");

			sql.Format("UPDATE STUDY SET STU_MODALITIES_IN_STUDY = '%s' WHERE STU_INSTANCE_UID = '%s'", pModalities, pStudyInstanceUID);

			if(m_pCfg->GetModalityDebug())
			{
				m_pModalityLogger->LogMsg(" ");
				m_pModalityLogger->LogMsg("CStoreService::UpdateModalitiesInStudy SQL : %s", (char *)sql);
				m_pModalityLogger->LogMsg(" ");

			}

			if(m_pDb->SQLExec(hCursor, sql.GetBuffer()))
			{
				bRetVal = true;
			}
			else
			{
				m_pModalityLogger->LogMsg("ERROR : %s", m_pDb->GetLastOciErrorText(hCursor));
				m_pModalityLogger->LogMsg("SQL:  %s", sql.GetBuffer());

				m_pMailer->AddLine("ERROR : %s", m_pDb->GetLastOciErrorText(hCursor));
				m_pMailer->AddLine("SQL:  %s", sql.GetBuffer());
			}
			m_pDb->DeleteCursor(hCursor);
			hCursor = NULL;
		}
		else
		{
			m_pModalityLogger->LogMsg("ERROR : Failed to get cursor in CStoreService::UpdateModalitiesInStudy.");
			m_pMailer->AddLine("Failed to get cursor in CStoreService::UpdateModalitiesInStudy");
		}

		if(m_pCfg->GetProfiling())
			dbg.EndProfiling(m_pSeverLogger, (char *)sql, "Sql executed.");

		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	bool CStoreService::CreateSeries(ObjectData *pObject, StudyData *pStudy)
	{
		bool bRetVal       = false;
		HANDLE hCursor     = NULL;
		char strDate[65]   = {0};
		char sDateTime[33] = {0};
		char sNumber[17]   = {0};
		StringBuffer sql;
		DBGTool dbg;

		hCursor = m_pDb->GetCursor();
		if(hCursor)
		{
			if(m_pCfg->GetProfiling())
				dbg.StartProfiling(m_pSeverLogger, "Executing sql.");

			DataFormat::ConvertDateTime(strDate, pObject->sSerieDate, pObject->sSerieTime);

			sql.Format("CALL %s.CREATE_SERIE('%s', '%s', %s, '%s', '%s', %s, '%s', '%s', '%s')",
						m_pCfg->GetPkgName(),
						pStudy->sStudyInstanceUID,
						pObject->sSerieInstanceUID,
						strDate,
						pObject->sSerieDescription,
						pObject->sModality,
						"NULL",
						pObject->sSerieNumber,
						pObject->sLaterality,
						pObject->sBodyPart);

			if(m_pCfg->GetModalityDebug())
			{
				m_pModalityLogger->LogMsg(" ");
				m_pModalityLogger->LogMsg("CStoreService::CreateSeries SQL : %s", (char *)sql);
				m_pModalityLogger->LogMsg(" ");
			}

			if(m_pDb->SQLExec(hCursor, sql.GetBuffer()))
			{
				bRetVal = true;
			}
			else
			{
				m_pModalityLogger->LogMsg("ERROR : %s", m_pDb->GetLastOciErrorText(hCursor));
				m_pModalityLogger->LogMsg("SQL :  %s", sql.GetBuffer());

				m_pMailer->AddLine("%s", m_pDb->GetLastOciErrorText(hCursor));
				m_pMailer->AddLine("%SQL:  %s", sql.GetBuffer());			
			}
			m_pDb->DeleteCursor(hCursor);
			hCursor = NULL;

			if(m_pCfg->GetProfiling())
				dbg.EndProfiling(m_pSeverLogger, (char *)sql, "Sql executed.");
		}
		else
		{
			m_pModalityLogger->LogMsg("ERROR : Failed to get cursor in CStoreService::CreateSeries.");
			m_pMailer->AddLine("NULL study passed to CStoreService::CreateSeries");
		}
		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	bool CStoreService::CreateObject(ObjectData *pObject, StudyData *pStudy, const char *pStorageId)
	{
		bool bRetVal       = false;
		HANDLE hCursor     = NULL;
		char sStrDate[65]  = {0};
		char sDateTime[33] = {0};
		char sNumber[17]   = {0};
		StringBuffer sql;
		DBGTool dbg;

		hCursor = m_pDb->GetCursor();
		if(hCursor)
		{
			DataFormat::SOPClass2Type(pObject->sSopClassUID, pObject->sObjectType, pObject->sObjectDesc);
			DataFormat::ConvertDateTime(sStrDate, pObject->sSerieDate, pObject->sSerieTime);

			sql.Format("CALL %s.CREATE_OBJECT('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', %s, '%s', '%s')",
					   m_pCfg->GetPkgName(),
					   pObject->sSOPInstanceUID,
					   pObject->sSopClassUID,
					   pStudy->sStudyInstanceUID,
					   pObject->sSerieInstanceUID,
					   pObject->sModality,
					   pObject->sInstanceNumber,
					   pObject->sAeTitle,
					   pObject->sViewPos,
					   sStrDate,
					   pStorageId,
					   pObject->sObjectType);

			if(m_pCfg->GetModalityDebug())
			{
				m_pModalityLogger->LogMsg(" ");
				m_pModalityLogger->LogMsg("CStoreService::CreateObject SQL : %s", (char *)sql);
				m_pModalityLogger->LogMsg(" ");
			}

			if(m_pCfg->GetProfiling())
				dbg.StartProfiling(m_pSeverLogger, "Executing sql.");

			if(m_pDb->SQLExec(hCursor, sql.GetBuffer()))
			{
				bRetVal = true;
			}
			else
			{
				m_pModalityLogger->LogMsg("ERROR : %s", m_pDb->GetLastOciErrorText(hCursor));
				m_pModalityLogger->LogMsg("SQL :  %s", sql.GetBuffer());

				m_pMailer->AddLine("%s", m_pDb->GetLastOciErrorText(hCursor));
				m_pMailer->AddLine("SQL :  %s", sql.GetBuffer());
			}
			m_pDb->DeleteCursor(hCursor);
			hCursor = NULL;

			if(m_pCfg->GetProfiling())
				dbg.EndProfiling(m_pSeverLogger, (char *)sql, "Sql executed.");
		}
		else
		{
			m_pModalityLogger->LogMsg("ERROR : Failed to get cursor in CStoreService::CreateObject.");
			m_pMailer->AddLine("Failed to get cursor in CStoreService::CreateObject");
		}
		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	bool CStoreService::SetStudyRead(StudyData *pStudyInfo)
	{
		bool bRetVal   = false;
		HANDLE hCursor = NULL;
		StringBuffer sql;
		DBGTool dbg;

		hCursor = m_pDb->GetCursor();
		if(hCursor)
		{
			sql.Format("CALL %s.SET_STUDY_READ('%s','%s')",
				       m_pCfg->GetPkgName(),
					   pStudyInfo->sStudyInstanceUID,
					   pStudyInfo->sPhysicianReadingStudy);

			if(m_pCfg->GetProfiling())
				dbg.StartProfiling(m_pSeverLogger, "Executing sql.");

			if(m_pCfg->GetModalityDebug())
			{
				m_pModalityLogger->LogMsg(" ");
				m_pModalityLogger->LogMsg("CStoreService::SetStudyRead SQL : %s", (char *)sql);
				m_pModalityLogger->LogMsg(" ");
			}

			if(m_pDb->SQLExec(hCursor, sql.GetBuffer()))
			{
				bRetVal = true;
			}
			else
			{
				m_pModalityLogger->LogMsg("%s", m_pDb->GetLastOciErrorText(hCursor));
				m_pModalityLogger->LogMsg("SQL :  %s", sql.GetBuffer());

				m_pMailer->AddLine("%s", m_pDb->GetLastOciErrorText(hCursor));
				m_pMailer->AddLine("SQL :  %s", sql.GetBuffer());
			}
			m_pDb->DeleteCursor(hCursor);
			hCursor = NULL;

			if(m_pCfg->GetProfiling())
				dbg.EndProfiling(m_pSeverLogger, (char *)sql, "Sql executed.");
		}
		else
		{
			m_pModalityLogger->LogMsg("ERROR : NULL study info passed to CStoreService::SetStudyRead.");
			m_pMailer->AddLine("NULL study info passed to CStoreService::SetStudyRead.");
		}
		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	bool CStoreService::SetStudyTranscribed(StudyData *pStudyInfo)
	{
		bool bRetVal   = false;
		HANDLE hCursor = NULL;
		StringBuffer sql;
		DBGTool dbg;

		hCursor = m_pDb->GetCursor();
		if(hCursor)
		{
			if(m_pCfg->GetProfiling())
				dbg.StartProfiling(m_pSeverLogger, "Executing sql.");

			sql.Format("CALL %s.SET_STUDY_TRANSCRIBED('%s')", m_pCfg->GetPkgName(), pStudyInfo->sStudyInstanceUID);

			if(m_pCfg->GetModalityDebug())
			{
				m_pModalityLogger->LogMsg(" ");
				m_pModalityLogger->LogMsg("CStoreService::SetStudyTranscribed SQL : %s", (char *)sql);
				m_pModalityLogger->LogMsg(" ");
			}

			if(m_pDb->SQLExec(hCursor, sql.GetBuffer()))
			{
				bRetVal = true;
			}
			else
			{
				m_pModalityLogger->LogMsg("%s", m_pDb->GetLastOciErrorText(hCursor));
				m_pModalityLogger->LogMsg("SQL :  %s", sql.GetBuffer());

				m_pMailer->AddLine("%s", m_pDb->GetLastOciErrorText(hCursor));
				m_pMailer->AddLine("SQL :  %s", sql.GetBuffer());
			}
			m_pDb->DeleteCursor(hCursor);
			hCursor = NULL;

			if(m_pCfg->GetProfiling())
				dbg.EndProfiling(m_pSeverLogger, (char *)sql, "Sql executed");
		}
		else
		{
			m_pModalityLogger->LogMsg("ERROR : Failed to get cursor in  DataInterface::SetStudyTranscribed.");
			m_pMailer->AddLine("Failed to get cursor in  DataInterface::SetStudyTranscribed");
		}
		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	bool CStoreService::GetStudyInfoByAccession(const char *pAccessionNumber, 
		                                        char *pPatID, 
												char *pStudyInstanceUID, 
												char *pStudyDate, 
												char *pStatus)
	{
		bool bRetVal   = false;
		HANDLE hCursor = NULL;
		StringBuffer sql;

		hCursor = m_pDb->GetCursor();
		if(hCursor)
		{
			sql.Format("SELECT STU_PAT_ID, STU_INSTANCE_UID, STU_DATE, STU_STATUS " \
					   "FROM STUDY WHERE STU_ACCESSION_NUMBER = '%s'", pAccessionNumber);

			if(m_pCfg->GetModalityDebug())
			{
				m_pModalityLogger->LogMsg(" ");
				m_pModalityLogger->LogMsg("CStoreService::GetStudyInfoByAccession SQL : %s", (char *)sql);
				m_pModalityLogger->LogMsg(" ");
			}

			if(m_pDb->SQLExec(hCursor, sql.GetBuffer(),"%s%s%s%s", pPatID, pStudyInstanceUID, pStudyDate, pStatus))
			{
				bRetVal = true;
			}
			else
			{
				if(m_pDb->GetLastOciErrorCode(hCursor)>0)
				{
					m_pModalityLogger->LogMsg("%s", m_pDb->GetLastOciErrorText(hCursor));
					m_pModalityLogger->LogMsg("SQL :  %s", sql.GetBuffer());

					m_pMailer->AddLine("%s", m_pDb->GetLastOciErrorText(hCursor));
					m_pMailer->AddLine("SQL :  %s", sql.GetBuffer());
				}
			}
			m_pDb->DeleteCursor(hCursor);
			hCursor = NULL;
		}
		else
		{
			m_pModalityLogger->LogMsg("ERROR : Failed to get cursor in  DataInterface::GetStudyInfoByAccession.");
			m_pMailer->AddLine("Failed to get cursor in  DataInterface::GetStudyInfoByAccession");
		}
		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	bool CStoreService::CancelObject(const char *pCOIInstanceUID)
	{
		bool bRetVal   = false;
		HANDLE hCursor = NULL;
		StringBuffer sql;
		DBGTool dbg;

		hCursor = m_pDb->GetCursor();
		if(hCursor)
		{
			sql.Format("CALL %s.CANCEL_OBJECT('%s')", m_pCfg->GetPkgName(), pCOIInstanceUID);

			if(m_pCfg->GetProfiling())
				dbg.StartProfiling(m_pSeverLogger, "Executing sql.");

			if(m_pCfg->GetModalityDebug())
			{
				m_pModalityLogger->LogMsg(" ");
				m_pModalityLogger->LogMsg("CStoreService::CancelObject SQL : %s", (char *)sql);
				m_pModalityLogger->LogMsg(" ");
			}

			if(m_pDb->SQLExec(hCursor, sql.GetBuffer()))
			{
				bRetVal = true;
			}
			else
			{
				m_pModalityLogger->LogMsg(m_pDb->GetLastOciErrorText(hCursor));
				m_pModalityLogger->LogMsg("SQL :  %s", sql.GetBuffer());

				m_pMailer->AddLine(m_pDb->GetLastOciErrorText(hCursor));
				m_pMailer->AddLine("SQL :  %s", sql.GetBuffer());
			}
			m_pDb->DeleteCursor(hCursor);
			hCursor = NULL;

			if(m_pCfg->GetProfiling())
				dbg.EndProfiling(m_pSeverLogger, (char *)sql, "Sql executed.");
		}
		else
		{
			m_pModalityLogger->LogMsg("ERROR : Failed to get cursor in DataInterface::CancelObject.");
			m_pMailer->AddLine("Failed to get cursor in DataInterface::CancelObject");
		}
		return bRetVal;
	}
}
