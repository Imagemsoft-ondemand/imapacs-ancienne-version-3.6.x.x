/////////////////////////////////////////////////////////////////////////////
//	NCreateService.h
/////////////////////////////////////////////////////////////////////////////

#include "../PacsConfig.h"
#include "../DataStructures.h"
#include "../Utils/log.h"
#include "../Utils/Mailer.h"
#include "../Utils/Str.h"
#include "../Utils/TimeUtil.h"
#include "../Utils/DBGTool.h"
#include "../Utils/DataFotmat.h"
#include "../Utils/OCIDatabase.h"

#if !defined(__NCREATESERVICE_H_INCLUDED__)
#define      __NCREATESERVICE_H_INCLUDED__

/////////////////////////////////////////////////////////////////////////////
//
namespace imapacs
{
	/////////////////////////////////////////////////////////////////////////
	//
	class NCreateService
	{
	public:
		NCreateService():m_pModalityLogger(NULL),
			             m_pSeverLogger(NULL),
						 m_pMailer(NULL),
						 m_pCfg(NULL),
						 m_pDb(NULL),
						 m_iTransactionId(0)
		{}
		virtual ~NCreateService()
		{}

		void SetConfig(PacsConfig *pCfg)
		{
			m_pCfg            = pCfg; 
			m_pModalityLogger = pCfg->GetModalityLogger();
			m_pSeverLogger    = pCfg->GetServerLogger();
			m_pMailer         = pCfg->GetMailer();
			m_pDb             = pCfg->GetDb();
		}

		void SetCurrentDebugParams(const char *pDir, int TransactionId)
		{
			if(pDir)
			{
				m_sDebugDir.Format("%s", pDir);
				m_iTransactionId = TransactionId;
			}
		}

		bool PPSExists(const char *pPPSUID);
		bool CreatePPSEntry(CreatePPSData *pData);

		bool MPPSExists(const char *pMPPSUID);
		bool CreateMPPSEntry(MPPSData *pData);

		tool::COCIDatabase *GetDatabase()    {return m_pDb;}

		void Dispose()
		{
			m_sDebugDir.Clear();
		}

	protected:

		Log          *m_pModalityLogger;
		Log          *m_pSeverLogger;
		Mailer       *m_pMailer;
		PacsConfig   *m_pCfg;
		tool::COCIDatabase *m_pDb;

		//	For debug.
		StringBuffer  m_sDebugDir;
		int           m_iTransactionId;
	};
}
#endif	//	__NCREATESERVICE_H_INCLUDED__
