/////////////////////////////////////////////////////////////////////////////
//	NActionService.h
/////////////////////////////////////////////////////////////////////////////

#include "../DataStructures.h"
#include "../PacsConfig.h"
#include "../Utils/PacsCommon.h"
#include "../Utils/log.h"
#include "../Utils/Str.h"
#include "../Utils/TimeUtil.h"
#include "../Utils/DBGTool.h"
#include "../Utils/Mailer.h"
#include "../Utils/OCIDatabase.h"

#if !defined(__NACTIONSERVICE_H_INCLUDED__)
#define      __NACTIONSERVICE_H_INCLUDED__

/////////////////////////////////////////////////////////////////////////////
//
namespace imapacs
{
	class NActionService
	{
	public:
		NActionService():m_pModalityLogger(NULL),
			             m_pSeverLogger(NULL),
						 m_pMailer(NULL),
			             m_pCfg(NULL),
			             m_pDb(NULL),
						 m_iTransactionId(0)
		{}
		virtual ~NActionService() {}

		void SetConfig(PacsConfig *pCfg)     
		{
			m_pCfg            = pCfg; 
			m_pModalityLogger = pCfg->GetModalityLogger();
			m_pSeverLogger    = pCfg->GetServerLogger();
			m_pMailer         = pCfg->GetMailer();
			m_pDb             = pCfg->GetDb();
		}

		void SetCurrentDebugParams(const char *pDir, int TransactionId)
		{
			if(pDir)
			{
				m_sDebugDir.Format("%s", pDir);
				m_iTransactionId = TransactionId;
			}
		}

		/////////////////////////////////////////////////////////////////////
		//	Storage-commitment related methods.
		bool IsObjectStoredInDb(const char *pSOPClassUID, const char *pSOPInstanceUID);
		bool GetObjectFile(const char *pSOPInstanceUID, char *pObjectFile);
		bool GetStorageCommitmentAddress(const char *pAeTitle,
									     char *pStoCommitIp,
										 unsigned short *pStoCommitPort);

		/////////////////////////////////////////////////////////////////////
		//	GPSPS update related methods.
		bool SPSExists(const char *pSOPInstanceUID);
		bool SPSIsAvailable(const char *pSOPInstanceUID);
		bool UpdateSPS(const UpdateSPSData *pData, const char *pSOPInstanceUID);

		tool::COCIDatabase *GetDatabase()    {return m_pDb;}

		void Dispose()
		{
			m_sDebugDir.Clear();
		}

	protected:

		Log          *m_pModalityLogger;
		Log          *m_pSeverLogger;
		Mailer       *m_pMailer;
		PacsConfig   *m_pCfg;
		tool::COCIDatabase *m_pDb;

		//	For debug.
		StringBuffer  m_sDebugDir;
		int           m_iTransactionId;
	};
}

#endif	//	__NACTIONSERVICE_H_INCLUDED__
