/////////////////////////////////////////////////////////////////////////////
//	NCreateService.cpp
/////////////////////////////////////////////////////////////////////////////

#include "NCreateService.h"

/////////////////////////////////////////////////////////////////////////////
//
namespace imapacs
{
	/////////////////////////////////////////////////////////////////////////
	//
	bool NCreateService::PPSExists(const char *pPPSUID)
	{
		bool bRetVal       = false;
		HANDLE hCursor     = NULL;
		char sResult[11]   = {0};
		char sDateTime[33] = {0};
		char sNumber[17]   = {0};
		StringBuffer sql;
		DBGTool dbg;

		hCursor = m_pDb->GetCursor();
		if(hCursor)
		{
			sql.Format("SELECT 1 FROM GP_PERF_PSTEP WHERE GPP_INSTANCE_UID = '%s'", pPPSUID);

			if(m_pCfg->GetModalityDebug())
			{
				m_pModalityLogger->LogMsg(" ");
				m_pModalityLogger->LogMsg("NCreateService::PPSExists SQL : %s", (char *)sql);
				m_pModalityLogger->LogMsg(" ");
			}

			if(m_pCfg->GetProfiling())
				dbg.StartProfiling(m_pSeverLogger, "Executing sql.");

			if(m_pDb->SQLExec(hCursor, sql.GetBuffer(), "%s", sResult))
			{
				if(sResult[0] == '1')
				{
					bRetVal = true;
				}
			}
			else
			{
				if(m_pDb->GetLastOciErrorCode(hCursor) != 0)
				{
					//	We really had an error, log something.
					m_pModalityLogger->LogMsg("ERROR : %s", m_pDb->GetLastOciErrorText(hCursor));
					m_pMailer->AddLine("ERROR : %s", m_pDb->GetLastOciErrorText(hCursor));
				}
			}
			m_pDb->DeleteCursor(hCursor);
			hCursor = NULL;

			if(m_pCfg->GetProfiling())
				dbg.EndProfiling(m_pSeverLogger, (char *)sql, "Sql executed.");
		}
		else
		{
			m_pModalityLogger->LogMsg("ERROR : failed to get cursor in NCreateService::PPSExists.");
			m_pMailer->AddLine("ERROR : failed to get cursor in NCreateService::PPSExists.");
		}
		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	bool NCreateService::CreatePPSEntry(CreatePPSData *pData)
	{
		bool bRetVal        = false;
		char sStartDate[31] = {0};
		char sStartTime[31] = {0};
		char sDateTime[33]  = {0};
		char sNumber[17]    = {0};
		HANDLE hCursor      = NULL;
		StringBuffer sql;
		DBGTool dbg;

		hCursor = m_pDb->GetCursor();
		if(hCursor)
		{
			if(m_pCfg->GetProfiling())
				dbg.StartProfiling(m_pSeverLogger, "Composing sql.");

			if(pData->sPPSStartDate[0])
			{
				sprintf(sStartDate, "'%s'", pData->sPPSStartDate);
			}
			else
			{
				sprintf(sStartDate, "TO_CHAR(SYSDATE, 'YYYYMMDD')");
			}

			if(pData->sPPSStartTime[0])
			{
				sprintf(sStartTime, "'%s'", pData->sPPSStartTime);
			}
			else
			{
				sprintf(sStartTime, "TO_CHAR(SYSDATE, 'HH24MISS')");
			}

			sql.Format("INSERT INTO GP_PERF_PSTEP "\
					   "(GPP_INSTANCE_UID, GPP_ID, GPP_PERF_STAT_AE_TITLE, GPP_PERF_PS_START_DATE, GPP_PERF_PS_START_TIME, GPP_PERF_PS_STATUS, GPP_TRANSACTION_UID) "\
					   "VALUES ('%s', '%s', '%s', %s, %s, '%s', '%s')",
					   pData->sPPSInstanceUID,
					   pData->sPPSId,
					   pData->sPerformedStationName,
					   sStartDate,
					   sStartTime,
					   pData->sPPSStatus,
					   pData->sTransactionUID);

			if(m_pCfg->GetProfiling())
				dbg.EndProfiling(m_pSeverLogger, NULL, "Sql composed.");

			if(m_pCfg->GetModalityDebug())
			{
				m_pModalityLogger->LogMsg(" ");
				m_pModalityLogger->LogMsg("NCreateService::CreatePPSEntry SQL : %s", (char *)sql);
				m_pModalityLogger->LogMsg(" ");
			}

			if(m_pCfg->GetProfiling())
				dbg.StartProfiling(m_pSeverLogger, "Executing sql.");

			if(m_pDb->SQLExec(hCursor, sql.GetBuffer()))
			{
				m_pDb->SQLExec(hCursor, "COMMIT");

				bRetVal = true;
			}
			else
			{
				m_pModalityLogger->LogMsg("ERROR : %s", m_pDb->GetLastOciErrorText(hCursor));
				m_pModalityLogger->LogMsg("SQL : %s", sql.GetBuffer());
			}
			m_pDb->DeleteCursor(hCursor);
			hCursor = NULL;

			if(m_pCfg->GetProfiling())
				dbg.EndProfiling(m_pSeverLogger, (char *)sql, "Sql executed.");
		}
		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	bool NCreateService::MPPSExists(const char *pMPPSUID)
	{
		bool bRetVal       = false;
		HANDLE hCursor     = NULL;
		char sResult[5]    = {0};
		char sDateTime[33] = {0};
		char sNumber[17]   = {0};
		StringBuffer sql;
		DBGTool dbg;

		hCursor = m_pDb->GetCursor();
		if(hCursor)
		{
			if(m_pCfg->GetProfiling())
				dbg.StartProfiling(m_pSeverLogger, "Executing sql.");

			sql.Format("SELECT 1 FROM MODALITY_PERF_PROC_STEP WHERE MPPS_INSTANCE_UID = '%s'", pMPPSUID);

			if(m_pDb->SQLExec(hCursor, (char *)sql, "%s", sResult))
			{
				if(sResult[0] == '1')
				{
					bRetVal = true;
				}
			}
			else
			{
				if(m_pDb->GetLastOciErrorCode(hCursor) != 0)
				{
					m_pModalityLogger->LogMsg("ERROR : %s", m_pDb->GetLastOciErrorText(hCursor));
					m_pMailer->AddLine("ERROR : %s", m_pDb->GetLastOciErrorText(hCursor));
				}
			}
			m_pDb->DeleteCursor(hCursor);
			hCursor = NULL;

			if(m_pCfg->GetProfiling())
				dbg.EndProfiling(m_pSeverLogger, (char *)sql, "Sql executed.");

			if(m_pCfg->GetModalityDebug())
			{
				m_pModalityLogger->LogMsg(" ");
				m_pModalityLogger->LogMsg("NCreateService::MPPSExists SQL : %s", (char *)sql);
				m_pModalityLogger->LogMsg(" ");
			}
		}
		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	bool NCreateService::CreateMPPSEntry(MPPSData *pData)
	{
		bool bRetVal                         = false;
		bool bError                          = false;
		HANDLE hCursor                       = NULL;
		ScheduledStepAttribute *pSchStepAttr = NULL;
		char sDateTime[33]                   = {0};
		char sNumber[17]                     = {0};
		StringBuffer sql;
		DBGTool dbg;

		hCursor = m_pDb->GetCursor();
		if(hCursor)
		{
			if(m_pCfg->GetProfiling())
				dbg.StartProfiling(m_pSeverLogger, "Executing sql.");

			for(int i = 0; i < pData->maScheStepSttribSeq.GetSize(); ++i)
			{
				pData->maScheStepSttribSeq.Pop(pSchStepAttr);

				sql.Format("INSERT INTO MODALITY_PERF_PROC_STEP " \
						   "(MPPS_INSTANCE_UID, MPPS_PERF_STAT_AE_TITLE, MPPS_PERF_PS_START_DATE, MPPS_PERF_PS_START_TIME, " \
						   " MPPS_PERF_PS_STATUS, MPPS_STU_INSTANCE_UID, MPPS_REQ_PROC_ID, MPPS_SCHED_PROC_STEP_ID, MPPS_ID) " \
						   "VALUES " \
						   "('%s', '%s', TO_CHAR(SYSDATE, 'YYYYMMDD'), TO_CHAR(SYSDATE, 'HH24MISS'), '%s', '%s', '%s', " \
						   " '%s', '%s')",
						   pData->sTransactionUID,
						   pData->sPerformedStationAeTitle,
						   pData->sPPSStatus,
						   pSchStepAttr->sStudyInstanceUID,
						   pSchStepAttr->sRequestedProcedureID,
						   pSchStepAttr->sSPSID,
						   pData->sPPSId);

				if(m_pCfg->GetModalityDebug())
				{
					m_pModalityLogger->LogMsg(" ");
					m_pModalityLogger->LogMsg("NCreateService::CreateMPPSEntry SQL : %s", (char *)sql);
					m_pModalityLogger->LogMsg(" ");
				}

				if(!m_pDb->SQLExec(hCursor, (char *)sql))
				{
					m_pModalityLogger->LogMsg("ERROR : %s", m_pDb->GetLastOciErrorText(hCursor));
					m_pMailer->AddLine("ERROR : %s", m_pDb->GetLastOciErrorText(hCursor));
					bError = true;
				}
				delete pSchStepAttr;
			}
			pSchStepAttr = NULL;

			if(m_pCfg->GetProfiling())
				dbg.EndProfiling(m_pSeverLogger, (char *)sql, "Sql executed.");

			if(!bError)
			{
				bRetVal = true;
				m_pDb->SQLExec(hCursor, "COMMIT");
			}
			else
			{
				m_pDb->SQLExec(hCursor, "ROLLBACK");
			}
			m_pDb->DeleteCursor(hCursor);
			hCursor = NULL;
		}
		return bRetVal;
	}
}
