/////////////////////////////////////////////////////////////////////////////
//	CGetService.cpp
/////////////////////////////////////////////////////////////////////////////

#include "CGetService.h"

/////////////////////////////////////////////////////////////////////////////
//
namespace imapacs
{
	/////////////////////////////////////////////////////////////////////////
	//
	QUERY_STATUS CGetService::FindCandidates(FindData *pParams, Queue <MoveResult *> *pList)
	{
		bool bSqlRet         = true;
		QUERY_STATUS eRet    = QS_OK_NO_RESULT;
		HANDLE hCursor       = NULL;
		MoveResult *pResult  = NULL;
		MoveResult CurResult = {0};
		char sDateTime[33]   = {0};
		char sNumber[17]     = {0};
		DBGTool dbg;

		StringBuffer select;
		StringBuffer where;

		hCursor = m_pDb->GetCursor();
		if(hCursor)
		{
			if(m_pCfg->GetProfiling())
				dbg.StartProfiling(m_pSeverLogger, "Composing sql.");

			select.Format("SELECT DISTINCT COI_STUDY_INSTANCE_UID, STU_ACCESSION_NUMBER, SER_NUMBER, COI_INSTANCE_NUMBER, COI_SOP_CLASS_UID, "\
						  "COI_SOP_INSTANCE_UID FROM STUDY, SERIES, COMPOSITE_OBJECT_INSTANCE ");

			where.Format("WHERE COI_STUDY_INSTANCE_UID = STU_INSTANCE_UID AND COI_SERIES_INSTANCE_UID = SER_INSTANCE_UID");

			DataFormat::ComposeWhereClause("COI_STUDY_INSTANCE_UID", pParams->StudyInstanceUID.Data, &where);
			DataFormat::ComposeWhereClause("STU_ACCESSION_NUMBER", pParams->StudyAccessionNumber.Data, &where);
			DataFormat::ComposeWhereClause("COI_SERIES_INSTANCE_UID", pParams->SeriesInstanceUID.Data, &where);
			DataFormat::ComposeWhereClause("COI_INSTANCE_NUMBER", pParams->ObjectInstanceNumber.Data, &where);
			DataFormat::ComposeWhereClause("COI_SOP_CLASS_UID", pParams->ObjectSopClasUID.Data, &where);
			DataFormat::ComposeWhereClause("COI_SOP_INSTANCE_UID", pParams->ObjectInstanceUID.Data, &where);

//			DataFormat::MakeFindQueryDates(pParams->StudyDate.Data, pParams->StudyTime.Data, &where);
			DataFormat::MakeQueryDates(pParams->StudyDate.Data, 
				                       pParams->StudyTime.Data, 
									   &where,
									   "stu_date",
									   "stu_time");

			select += where.GetBuffer();

			if(m_pCfg->GetProfiling())
				dbg.EndProfiling(m_pSeverLogger, NULL, "Sql composed.");

			//	Set the query level.
			strcpy(CurResult.QueryLevel, pParams->sQLevel);

			if(m_pCfg->GetModalityDebug())
			{
				m_pModalityLogger->LogMsg(" ");
				m_pModalityLogger->LogMsg("CGetService::FindCandidates SQL : %s", (char *)select);
				m_pModalityLogger->LogMsg(" ");
			}

			if(m_pCfg->GetProfiling())
				dbg.StartProfiling(m_pSeverLogger, "Executing sql.");

			bSqlRet = m_pDb->SQLExec(hCursor, select.GetBuffer(), "%s%s%s%s%s%s", CurResult.StudyInstanceUID,
																				  CurResult.AccessionNumber,
																				  CurResult.SeriesNumber,
																				  CurResult.InstanceNumber,
																				  CurResult.SOPClassUID,
																				  CurResult.SOPInstanceUID);

			if(m_pCfg->GetProfiling())
				dbg.EndProfiling(m_pSeverLogger, (char *)select, "Sql Executed.");

			if(bSqlRet)
			{
				eRet = QS_OK_RESULT;

				//	Build the result list.
				do
				{
					pResult = new MoveResult;
					if(pResult)
					{
						memcpy(pResult, &CurResult, sizeof(MoveResult));
						pList->Add(pResult);
					}
					memset(&CurResult, 0, sizeof(MoveResult));

				}while(m_pDb->NextRecord(hCursor));
			}
			else
			{
				if(m_pDb->GetLastOciErrorCode(hCursor) == 0)
				{
					eRet = QS_OK_NO_RESULT;
				}
				else
				{
					m_pModalityLogger->LogMsg("ERROR : %s", m_pDb->GetLastOciErrorText(hCursor));
					m_pModalityLogger->LogMsg("SQL :  %s", select.GetBuffer());

					m_pMailer->AddLine("ERROR : %s", m_pDb->GetLastOciErrorText(hCursor));
					m_pMailer->AddLine("SQL :  %s", select.GetBuffer());

					eRet = QS_ERROR;
				}
			}
			m_pDb->DeleteCursor(hCursor);
			hCursor = NULL;
		}
		else
		{
			m_pModalityLogger->LogMsg("ERROR : Failed to get cursor in DataInterface::FindMoveCanditates.");
			m_pMailer->AddLine("Failed to get cursor in DataInterface::FindMoveCanditates");

			eRet = QS_ERROR;
		}
		pList->SetAtFirst();

		return eRet;
	}
}
