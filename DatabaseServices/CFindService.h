/////////////////////////////////////////////////////////////////////////////
//	CFindService.h
/////////////////////////////////////////////////////////////////////////////

#include "../PacsConfig.h"
#include "../DataStructures.h"
#include "../Utils/log.h"
#include "../Utils/Str.h"
#include "../Utils/Mailer.h"
#include "../Utils/DataFotmat.h"
#include "../Utils/OCIDatabase.h"
#include "../Utils/PacsCommon.h"

#if !defined(__CFINDSERVICE_H_INCLUDED__)
#define      __CFINDSERVICE_H_INCLUDED__

/////////////////////////////////////////////////////////////////////////////
//
namespace imapacs
{
	/////////////////////////////////////////////////////////////////////////
	//
	class CFindService
	{
	public:
		CFindService():m_pModalityLogger(NULL),
			           m_pSeverLogger(NULL),
					   m_pMailer(NULL),
					   m_pCfg(NULL),
					   m_pDb(NULL),
					   m_hCurrentFindCursor(NULL),
					   m_iTransactionId(0)
		{}
		virtual ~CFindService()
		{
			m_sDebugDir.Clear();
		}

		void SetConfig(PacsConfig *pCfg)
		{
			m_pCfg            = pCfg; 
			m_pModalityLogger = pCfg->GetModalityLogger();
			m_pSeverLogger    = pCfg->GetServerLogger();
			m_pMailer         = pCfg->GetMailer();
			m_pDb             = pCfg->GetDb();
		}

		void SetCurrentDebugParams(const char *pDir, int TransactionId)
		{
			if(pDir)
			{
				m_sDebugDir.Format("%s", pDir);
				m_iTransactionId = TransactionId;
			}
		}

		QUERY_STATUS StartFind(FindData *pReqData, int iRootLevel);
		QUERY_STATUS StartFindWorklist(const char *pAeTitle, const WLData *pInfo, WLData *pResult);
		QUERY_STATUS StartFindGPWorlkist(GPWLData *pRequestData);
		QUERY_STATUS GetReferencedStudies(GPWLData *pRequestData);
		bool NextFindResult();
		bool UseInternalCode(const char *pAeTitle);

		bool GetScalingFactorBySOPClass(const char *pSOPClass, double *pScalingFactorX);

		tool::COCIDatabase *GetDatabase()    {return m_pDb;}

		void Dispose()
		{
			m_sDebugDir.Clear();
		}

	protected:

		/////////////////////////////////////////////////////////////////////
		//	C-FIND query methods.
		QUERY_STATUS DoFindPatient(FindData *pReqData);
		QUERY_STATUS DoFindStudy(FindData *pReqData);
		QUERY_STATUS DoFindSeries(FindData *pReqData);
		QUERY_STATUS DoFindObject(FindData *pReqData);

		/////////////////////////////////////////////////////////////////////
		//	C-FIND modality worklist methods.
		bool GetWorklistModality(const char *pAeTitle, char *pModality);

		/////////////////////////////////////////////////////////////////////
		//	C-FIND general purpose worklist methods.

		Log          *m_pModalityLogger;
		Log          *m_pSeverLogger;
		Mailer       *m_pMailer;
		PacsConfig   *m_pCfg;
		tool::COCIDatabase *m_pDb;

		HANDLE        m_hCurrentFindCursor;

		//	For debug.
		StringBuffer  m_sDebugDir;
		int           m_iTransactionId;
	};
}

#endif	//	__CFINDSERVICE_H_INCLUDED__
