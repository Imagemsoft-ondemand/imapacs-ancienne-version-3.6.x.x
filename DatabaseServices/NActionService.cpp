/////////////////////////////////////////////////////////////////////////////
//	NActionService.cpp
/////////////////////////////////////////////////////////////////////////////

#include "../Utils/StringBuffer.h"
#include "NActionService.h"

namespace imapacs
{
	/////////////////////////////////////////////////////////////////////////
	//
	bool NActionService::IsObjectStoredInDb(const char *pSOPClassUID, 
		                                    const char *pSOPInstanceUID)
	{
		bool bRetVal       = false;
		char sResult[5]    = {0};
		char sDateTime[33] = {0};
		char sNumber[17]   = {0};
		HANDLE hCursor     = NULL;
		StringBuffer sql;
		DBGTool dbg;

		hCursor = m_pDb->GetCursor();
		if(hCursor)
		{
			sql.Format("SELECT %s.VALIDATE_OBJECT_STORED('%s', '%s') FROM DUAL", m_pCfg->GetPkgName(),
		                                                                         pSOPClassUID,
																				 pSOPInstanceUID);
			if(m_pCfg->GetModalityDebug())
			{
				m_pModalityLogger->LogMsg(" ");
				m_pModalityLogger->LogMsg("NActionService::IsObjectStoredInDb SQL : %s", (char *)sql);
				m_pModalityLogger->LogMsg(" ");
			}

			if(m_pCfg->GetProfiling())
				dbg.StartProfiling(m_pSeverLogger, "Executing sql.");

			if(m_pDb->SQLExec(hCursor, sql.GetBuffer(), "%s", sResult))
			{
				if(sResult[0] == '1')
				{
					bRetVal = true;
				}
			}
			else
			{
				m_pModalityLogger->LogMsg("%s", m_pDb->GetLastOciErrorText(hCursor));
				m_pModalityLogger->LogMsg("SQL:  %s", sql.GetBuffer());

				m_pMailer->AddLine("%s", m_pDb->GetLastOciErrorText(hCursor));
				m_pMailer->AddLine("SQL:  %s", sql.GetBuffer());
			}
			m_pDb->DeleteCursor(hCursor);
			hCursor = NULL;

			if(m_pCfg->GetProfiling())
				dbg.EndProfiling(m_pSeverLogger, (char *)sql, "Sql executed.");
		}
		else
		{
			m_pModalityLogger->LogMsg("ERROR : Failed to get cursor in  NActionService::IsObjectStoredInDb.");
			m_pMailer->AddLine("ERROR : Failed to get cursor in  NActionService::IsObjectStoredInDb.");
		}
		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	bool NActionService::GetObjectFile(const char *pSOPInstanceUID, char *pObjectFile)
	{
		bool bRetVal       = false;
		HANDLE hCursor     = NULL;
		char sDateTime[33] = {0};
		char sNumber[17]   = {0};
		StringBuffer sql;
		DBGTool dbg;

		if(pSOPInstanceUID)
		{
			if(pObjectFile)
			{
				hCursor = m_pDb->GetCursor();
				if(hCursor)
				{
					sql.Format("SELECT %s.GET_OBJECT_PATH('%s') FROM DUAL", 
							   m_pCfg->GetPkgName(),
							   pSOPInstanceUID);

					if(m_pCfg->GetModalityDebug())
					{
						m_pModalityLogger->LogMsg(" ");
						m_pModalityLogger->LogMsg("NActionService::GetObjectFile SQL : %s", (char *)sql);
						m_pModalityLogger->LogMsg(" ");
					}

					if(m_pCfg->GetProfiling())
						dbg.StartProfiling(m_pSeverLogger, "Executing sql.");

					if(m_pDb->SQLExec(hCursor, sql.GetBuffer(), "%s", pObjectFile))
					{
						bRetVal = true;
					}
					else
					{
						if(m_pDb->GetLastOciErrorCode(hCursor) != 0)
						{
							m_pModalityLogger->LogMsg("ERROR : %s", m_pDb->GetLastOciErrorText(hCursor));
							m_pModalityLogger->LogMsg("SQL:  %s", sql.GetBuffer());
						}
						else
						{
							m_pMailer->AddLine("%s", m_pDb->GetLastOciErrorText(hCursor));
							m_pMailer->AddLine("SQL:  %s", sql.GetBuffer());
						}
					}

					m_pDb->DeleteCursor(hCursor);
					hCursor = NULL;

					if(m_pCfg->GetProfiling())
						dbg.EndProfiling(m_pSeverLogger, (char *)sql, "Sql executed.");
				}
				else
				{
					m_pModalityLogger->LogMsg("ERROR : Failed to get cursor in DataInterface::GetObjectPath [%s line %d]",
											  __FILE__,
											  __LINE__);
					m_pMailer->AddLine("Failed to get cursor in DataInterface::GetObjectPath");
				}
			}
			else
			{
				m_pModalityLogger->LogMsg("ERROR : NULL pObjectFile passed to NActionService::GetObjectPathAndFile [%s line %d]",
										  __FILE__,
										  __LINE__);
				m_pMailer->AddLine("NULL pObjectFile passed to NActionService::GetObjectPathAndFile");
			}
		}
		else
		{
			m_pModalityLogger->LogMsg("ERROR : NULL sop instance uid passed to NActionService::GetObjectPathAndFile [%s line %d]",
				                      __FILE__,
							          __LINE__);
			m_pMailer->AddLine("NULL sop instance uid passed to NActionService::GetObjectPathAndFile");
		}
		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	bool NActionService::GetStorageCommitmentAddress(const char *pAeTitle,
									                 char *pStoCommitIp,
									                 unsigned short *pStoCommitPort)
	{
		bool bRetVal       = false;
		HANDLE hCursor     = NULL;
		char sPort[15]     = {0};
		char sDateTime[33] = {0};
		char sNumber[17]   = {0};
		StringBuffer sql;
		DBGTool dbg;

		if(!pAeTitle)
		{
			m_pModalityLogger->LogMsg("ERROR : NULL Ae Title passed to NActionService::GetStorageCommitmentAddress [%s line %d]",
		                              __FILE__,
		 				              __LINE__);
			m_pMailer->AddLine("NULL Ae Title passed to NActionService::GetStorageCommitmentAddress");
			return false;
		}

		if(!pStoCommitIp)
		{
			m_pModalityLogger->LogMsg("ERROR : NULL ip passed to NActionService::GetStorageCommitmentAddress [%s line %d]",
							          __FILE__,
							          __LINE__);
			m_pMailer->AddLine("NULL ip passed to NActionService::GetStorageCommitmentAddress");
			return false;
		}

		if(!pStoCommitPort)
		{
			m_pModalityLogger->LogMsg("ERROR : NULL port passed to NActionService::GetStorageCommitmentAddress [%s line %d]",
							          __FILE__,
							          __LINE__);
			m_pMailer->AddLine("NULL port passed to NActionService::GetStorageCommitmentAddress");
			return false;
		}

		hCursor = m_pDb->GetCursor();
		if(hCursor)
		{
			sql.Format("SELECT AT_ADDR_IP, AT_PORT_COMMIT FROM AE_TITLE WHERE AT_AE_TITLE = '%s'", pAeTitle);

			if(m_pCfg->GetModalityDebug())
			{
				m_pModalityLogger->LogMsg(" ");
				m_pModalityLogger->LogMsg("NActionService::GetStorageCommitmentAddress SQL : %s", (char *)sql);
				m_pModalityLogger->LogMsg(" ");
			}

			if(m_pCfg->GetProfiling())
				dbg.StartProfiling(m_pSeverLogger, "Executing sql.");

			if(m_pDb->SQLExec(hCursor, sql.GetBuffer(), "%s%s", pStoCommitIp, sPort))
			{
				(*pStoCommitPort) = atoi(sPort);
				bRetVal = true;
			}
			else
			{
				m_pModalityLogger->LogMsg("ERROR : %s", m_pDb->GetLastOciErrorText(hCursor));
				m_pModalityLogger->LogMsg("SQL : %s", sql.GetBuffer());

				m_pMailer->AddLine("%s", m_pDb->GetLastOciErrorText(hCursor));
				m_pMailer->AddLine("SQL :  %s", sql.GetBuffer());
			}
			m_pDb->DeleteCursor(hCursor);
			hCursor = NULL;

			if(m_pCfg->GetProfiling())
				dbg.EndProfiling(m_pSeverLogger, (char *)sql, "Sql executed.");
		}
		else
		{
			m_pModalityLogger->LogMsg("ERROR : Failed to get cursor in NActionService::GetStorageCommitmentAddress [%s line %d]",
							          __FILE__,
							          __LINE__);
			m_pMailer->AddLine("Failed to get cursor in NActionService::GetStorageCommitmentAddress");
		}
		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	bool NActionService::SPSExists(const char *pSOPInstanceUID)
	{
		bool bRetVal       = false;
		HANDLE hCursor     = NULL;
		char sResult[2]    = {0};
		char sDateTime[33] = {0};
		char sNumber[17]   = {0};
		StringBuffer sql;
		DBGTool dbg;

		hCursor = m_pDb->GetCursor();
		if(hCursor)
		{
			if(pSOPInstanceUID)
			{
				sql.Format("SELECT 1 FROM GP_SCHEDULED_PROCEDURE_STEP WHERE GSPS_SOP_INSTANCE_UID = '%s'", pSOPInstanceUID);

				if(m_pCfg->GetModalityDebug())
				{
					m_pModalityLogger->LogMsg(" ");
					m_pModalityLogger->LogMsg("NActionService::SPSExists : %s", (char *)sql);
					m_pModalityLogger->LogMsg(" ");
				}

				if(m_pCfg->GetProfiling())
					dbg.StartProfiling(m_pSeverLogger, "Executing sql.");

				if(m_pDb->SQLExec(hCursor, sql.GetBuffer(), "%s", sResult))
				{
					if(sResult[0] = '1')
						bRetVal = true;
				}
				else
				{
					m_pModalityLogger->LogMsg("ERROR %s", m_pDb->GetLastOciErrorText(hCursor));
					m_pMailer->AddLine("ERROR %s", m_pDb->GetLastOciErrorText(hCursor));
				}

				if(m_pCfg->GetProfiling())
					dbg.EndProfiling(m_pSeverLogger, (char *)sql, "Sql executed.");
			}
			else
			{
				m_pModalityLogger->LogMsg("ERROR : NULL pSOPInstanceUID passsed to DataInterface::SPSExists.");
				m_pMailer->AddLine("ERROR : NULL pSOPInstanceUID passsed to DataInterface::SPSExists.");
			}
			m_pDb->DeleteCursor(hCursor);
			hCursor = NULL;
		}
		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	bool NActionService::SPSIsAvailable(const char *pSOPInstanceUID)
	{
		bool bRetVal       = false;
		HANDLE hCursor     = NULL;
		char sResult[9]    = {0};
		char sDateTime[33] = {0};
		char sNumber[17]   = {0};
		StringBuffer sql;
		DBGTool dbg;

		hCursor = m_pDb->GetCursor();
		if(hCursor)
		{
			sql.Format("SELECT 1 FROM GP_SCHEDULED_PROCEDURE_STEP WHERE GSPS_STATUS = 'SCHEDULED' AND GSPS_SOP_INSTANCE_UID = '%s'", pSOPInstanceUID);

			if(m_pCfg->GetModalityDebug())
			{
				m_pModalityLogger->LogMsg(" ");
				m_pModalityLogger->LogMsg("NActionService::SPSIsAvailable : %s", (char *)sql);
				m_pModalityLogger->LogMsg(" ");
			}

			if(m_pCfg->GetProfiling())
				dbg.StartProfiling(m_pSeverLogger, "Executing sql.");

			if(m_pDb->SQLExec(hCursor, sql.GetBuffer(), "%s", sResult))
			{
				if(sResult[0] == '1')
				{
					bRetVal = true;
				}
			}
			else
			{
				m_pModalityLogger->LogMsg("ERROR %s", m_pDb->GetLastOciErrorText(hCursor));
				m_pMailer->AddLine("ERROR %s", m_pDb->GetLastOciErrorText(hCursor));
			}
			m_pDb->DeleteCursor(hCursor);
			hCursor = NULL;

			if(m_pCfg->GetProfiling())
				dbg.EndProfiling(m_pSeverLogger, (char *)sql, "Sql executed.");
		}
		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	bool NActionService::UpdateSPS(const UpdateSPSData *pData, const char *pSOPInstanceUID)
	{
		bool bRetVal       = false;
		HANDLE hCursor     = NULL;
		char sDateTime[33] = {0};
		char sNumber[17]   = {0};
		StringBuffer sql;
		DBGTool dbg;

		hCursor = m_pDb->GetCursor();
		if(hCursor)
		{
			sql.Format("UPDATE GP_SCHEDULED_PROCEDURE_STEP SET GSPS_STATUS = '%s', "\
															  "GSPS_TRANSACTION_UID = '%s', "\
															  "GSPS_CURRENT_PERFORMER_ID = '%s', "\
															  "GSPS_CURRENT_PERFORMER_NAME = '%s' "\
					   "WHERE GSPS_SOP_INSTANCE_UID = '%s'",
					   pData->sSPSStatus,
					   pData->sTransactionUID,
					   pData->sHumanPerformerCode,
					   pData->sPerformerName,
					   pSOPInstanceUID);

			if(m_pCfg->GetModalityDebug())
			{
				m_pModalityLogger->LogMsg(" ");
				m_pModalityLogger->LogMsg("NActionService::UpdateSPS : %s", (char *)sql);
				m_pModalityLogger->LogMsg(" ");
			}

			if(m_pCfg->GetProfiling())
				dbg.StartProfiling(m_pSeverLogger, "Executing sql.");

			if(m_pDb->SQLExec(hCursor, sql.GetBuffer()))
			{
				m_pDb->SQLExec(hCursor, "COMMIT");
				bRetVal = true;
			}
			else
			{
				m_pModalityLogger->LogMsg("ERROR : %s", m_pDb->GetLastOciErrorText(hCursor));
				m_pModalityLogger->LogMsg("        Failed to update SPS status.");

				m_pMailer->AddLine("ERROR : %s", m_pDb->GetLastOciErrorText(hCursor));
				m_pMailer->AddLine("        Failed to update SPS status.");
			}
			m_pDb->DeleteCursor(hCursor);
			hCursor = NULL;

			if(m_pCfg->GetProfiling())
				dbg.EndProfiling(m_pSeverLogger, (char *)sql, "Sql executed.");
		}
		else
		{
			m_pModalityLogger->LogMsg("ERROR : Failed to get cursor from database in DataInterface::UpdateSPS.");
			m_pMailer->AddLine("ERROR : Failed to get cursor from database in DataInterface::UpdateSPS.");
		}
		return bRetVal;
	}
}
