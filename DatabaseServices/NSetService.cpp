/////////////////////////////////////////////////////////////////////////////
//	NSetService.cpp
/////////////////////////////////////////////////////////////////////////////

#include "NSetService.h"

/////////////////////////////////////////////////////////////////////////////
//
namespace imapacs
{
	/////////////////////////////////////////////////////////////////////////
	//
	bool NSetService::PPSExists(const char *pPPSUID)
	{
		bool bRetVal       = false;
		HANDLE hCursor     = NULL;
		char sResult[11]   = {0};
		char sDateTime[33] = {0};
		char sNumber[17]   = {0};
		StringBuffer sql;
		DBGTool dbg;

		hCursor = m_pDb->GetCursor();
		if(hCursor)
		{
			sql.Format("SELECT 1 FROM GP_PERF_PSTEP WHERE GPP_INSTANCE_UID = '%s'", pPPSUID);

			if(m_pCfg->GetModalityDebug())
			{
				m_pModalityLogger->LogMsg(" ");
				m_pModalityLogger->LogMsg("NSetService::PPSExists SQL : %s", (char *)sql);
				m_pModalityLogger->LogMsg(" ");
			}

			if(m_pCfg->GetProfiling())
				dbg.StartProfiling(m_pSeverLogger, "Execuring sql.");

			if(m_pDb->SQLExec(hCursor, sql.GetBuffer(), "%s", sResult))
			{
				if(sResult[0] == '1')
				{
					bRetVal = true;
				}
			}
			else
			{
				if(m_pDb->GetLastOciErrorCode(hCursor) != 0)
				{
					//	We really had an error, log something.
					m_pModalityLogger->LogMsg("ERROR : %s", m_pDb->GetLastOciErrorText(hCursor));
					m_pMailer->AddLine("ERROR : %s", m_pDb->GetLastOciErrorText(hCursor));
				}
			}
			m_pDb->DeleteCursor(hCursor);
			hCursor = NULL;

			if(m_pCfg->GetProfiling())
				dbg.EndProfiling(m_pSeverLogger, (char *)sql, "Sql executed.");
		}
		else
		{
			m_pModalityLogger->LogMsg("ERROR : failed to get cursor in NSetService::PPSExists.");
			m_pMailer->AddLine("ERROR : failed to get cursor in NSetService::PPSExists.");
		}
		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	bool NSetService::UpdatePPS(CreatePPSData *pData)
	{
		bool bRetVal       = false;
		char sEndDate[31]  = {0};
		char sEndTime[31]  = {0};
		char sDateTime[33] = {0};
		char sNumber[17]   = {0};
		HANDLE hCursor     = NULL;
		StringBuffer sql;
		DBGTool dbg;

		hCursor = m_pDb->GetCursor();
		if(hCursor)
		{
			if(m_pCfg->GetProfiling())
				dbg.StartProfiling(m_pSeverLogger, "Composing sql.");

			if(pData->sPPSEndDate[0])
			{
				sprintf(sEndDate, "'%s'", pData->sPPSEndDate);
			}
			else
			{
				sprintf(sEndDate, "TO_CHAR(SYSDATE, 'YYYYMMDD')");
			}

			if(pData->sPPSEndTime[0])
			{
				sprintf(sEndTime, "'%s'", pData->sPPSEndTime);
			}
			else
			{
				sprintf(sEndTime, "TO_CHAR(SYSDATE, 'HH24MISS')");
			}

			sql.Format("UPDATE GP_PERF_PSTEP SET GPP_PERF_PS_END_DATE = %s, GPP_PERF_PS_END_TIME = %s, GPP_PERF_PS_STATUS = '%s' "\
				       "WHERE GPP_INSTANCE_UID = '%s'",
					   sEndDate,
					   sEndTime,
					   pData->sPPSStatus,
					   pData->sPPSInstanceUID);

			if(m_pCfg->GetProfiling())
				dbg.EndProfiling(m_pSeverLogger, NULL, "Sql executed.");

			if(m_pCfg->GetModalityDebug())
			{
				m_pModalityLogger->LogMsg(" ");
				m_pModalityLogger->LogMsg("NSetService::UpdatePPS SQL : %s", (char *)sql);
				m_pModalityLogger->LogMsg(" ");
			}

			if(m_pCfg->GetProfiling())
				dbg.StartProfiling(m_pSeverLogger, "Executing sql.");

			if(m_pDb->SQLExec(hCursor, sql.GetBuffer()))
			{
				m_pDb->SQLExec(hCursor, "COMMIT");

				bRetVal = true;
			}
			else
			{
				m_pModalityLogger->LogMsg("ERROR : %s", m_pDb->GetLastOciErrorText(hCursor));
				m_pModalityLogger->LogMsg("SQL : %s", sql.GetBuffer());
			}
			m_pDb->DeleteCursor(hCursor);
			hCursor = NULL;

			if(m_pCfg->GetProfiling())
				dbg.EndProfiling(m_pSeverLogger, (char *)sql, "Sql executed.");
		}
		else
		{
			m_pModalityLogger->LogMsg("ERROR : Failed to get database cursor in NSetService::UpdatePPS.");
		}
		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	bool NSetService::MPPSExists(const char *pMPPSUID)
	{
		bool bRetVal       = false;
		char sResult[5]    = {0};
		char sDateTime[33] = {0};
		char sNumber[17]   = {0};
		HANDLE hCursor     = NULL;
		StringBuffer sql;
		DBGTool dbg;

		hCursor = m_pDb->GetCursor();
		if(hCursor)
		{
			if(m_pCfg->GetProfiling())
				dbg.StartProfiling(m_pSeverLogger, "Executing sql.");

			sql.Format("SELECT 1 FROM MODALITY_PERF_PROC_STEP WHERE  MPPS_INSTANCE_UID = '%s'", pMPPSUID);

			if(m_pDb->SQLExec(hCursor, (char *)sql, "%s", sResult))
			{
				if(sResult[0] == '1')
				{
					bRetVal = true;
				}
			}
			else
			{
				if(m_pDb->GetLastOciErrorCode(hCursor))
				{
					m_pModalityLogger->LogMsg("ERROR : %s", m_pDb->GetLastOciErrorText(hCursor));
					m_pMailer->AddLine("ERROR : %s", m_pDb->GetLastOciErrorText(hCursor));
				}
			}
			m_pDb->DeleteCursor(hCursor);
			hCursor = NULL;

			if(m_pCfg->GetProfiling())
				dbg.EndProfiling(m_pSeverLogger, (char *)sql, "Sql executed.");

			if(m_pCfg->GetModalityDebug())
			{
				m_pModalityLogger->LogMsg(" ");
				m_pModalityLogger->LogMsg("NSetService::MPPSExists SQL : %s", (char *)sql);
				m_pModalityLogger->LogMsg(" ");
			}
		}
		else
		{
			m_pModalityLogger->LogMsg("ERROR : Failed to get cursor in NSetService::MPPSExists");
			m_pMailer->AddLine("ERROR : Failed to get cursor in NSetService::MPPSExists");
		}
		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	bool NSetService::UpdateMPPS(MPPSData *pData)
	{
		bool bRetVal       = false;
		HANDLE hCursor     = NULL;
		char sDateTime[33] = {0};
		char sNumber[17]   = {0};
		StringBuffer sql;
		DBGTool dbg;

		hCursor = m_pDb->GetCursor();
		if(hCursor)
		{
			if(m_pCfg->GetProfiling())
				dbg.StartProfiling(m_pSeverLogger, "Executing sql.");

			sql.Format("UPDATE MODALITY_PERF_PROC_STEP " \
				       "SET MPPS_PERF_PS_END_DATE = TO_CHAR(SYSDATE, 'YYYYMMDD'), " \
					   "    MPPS_PERF_PS_END_TIME = TO_CHAR(SYSDATE, 'HH24MISS'), " \
					   "    MPPS_PERF_PS_STATUS = '%s' " \
					   "WHERE MPPS_INSTANCE_UID = '%s'",
					   pData->sPPSStatus,
					   pData->sTransactionUID);

			if(m_pDb->SQLExec(hCursor, (char *)sql))
			{
				m_pDb->SQLExec(hCursor, "COMMIT");
				bRetVal = true;
			}
			else
			{
				m_pDb->SQLExec(hCursor, "ROLLBACK");

				m_pModalityLogger->LogMsg("ERROR : %s", m_pDb->GetLastOciErrorText(hCursor));
				m_pMailer->AddLine("ERROR : %s", m_pDb->GetLastOciErrorText(hCursor));
			}
			m_pDb->DeleteCursor(hCursor);
			hCursor=  NULL;

			if(m_pCfg->GetProfiling())
				dbg.EndProfiling(m_pSeverLogger, (char *)sql, "Sql executed.");

			if(m_pCfg->GetModalityDebug())
			{
				m_pModalityLogger->LogMsg(" ");
				m_pModalityLogger->LogMsg("NSetService::UpdateMPPS SQL : %s", (char *)sql);
				m_pModalityLogger->LogMsg(" ");
			}
		}
		else
		{
			m_pModalityLogger->LogMsg("ERROR : Failed to get cursor in NSetService::UpdateMPPS");
			m_pMailer->AddLine("ERROR : Failed to get cursor in NSetService::UpdateMPPS");
		}
		return bRetVal;
	}
}
