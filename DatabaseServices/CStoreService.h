/////////////////////////////////////////////////////////////////////////////
//	CStoreService.h
/////////////////////////////////////////////////////////////////////////////

#include "../DataStructures.h"
#include "../PacsConfig.h"
#include "../Utils/log.h"
#include "../Utils/Str.h"
#include "../Utils/TimeUtil.h"
#include "../Utils/DBGTool.h"
#include "../Utils/Mailer.h"
#include "../Utils/OCIDatabase.h"

#if !defined(__CSTORESERVICE_H_INCLUDED__)
#define      __CSTORESERVICE_H_INCLUDED__

/////////////////////////////////////////////////////////////////////////////
//
namespace imapacs
{
	/////////////////////////////////////////////////////////////////////////
	//
	class CStoreService
	{
	public:
		CStoreService():m_pModalityLogger(NULL),
			            m_pSeverLogger(NULL),
						m_pMailer(NULL),
						m_pCfg(NULL),
						m_pDb(NULL),
						m_iTransactionId(0)
		{}
		virtual ~CStoreService()
		{}

		void SetConfig(PacsConfig *pCfg)     
		{
			m_pCfg            = pCfg; 
			m_pModalityLogger = pCfg->GetModalityLogger();
			m_pSeverLogger    = pCfg->GetServerLogger();
			m_pMailer         = pCfg->GetMailer();
			m_pDb             = pCfg->GetDb();
		}

		void SetCurrentDebugParams(const char *pDir, int TransactionId)
		{
			if(pDir)
			{
				m_sDebugDir.Format("%s", pDir);
				m_iTransactionId = TransactionId;
			}
		}

		bool CheckPatientId(char *pPatientId, const char *pStudyUid);		//*** [ref:0002072]
		bool PatientExists(const char *pPatientId);
		bool CreatePatient(PatientData *pInfo);
		bool GetStudyAccessionNumber(const char *pStudyInstanceUID, char *pAccessionNb);
		bool GetNewAccessionNumber(char *pAccessionNb);
		bool UpdateStudyDate(StudyData *pStudyInfo);
		bool CreateStudy(StudyData *pStudy, PatientData *pPatient, bool bIsTransient);
		bool GetModalitiesInStudy(const char *pStudyInstanceUID, char *pModalities);
		bool UpdateModalitiesInStudy(const char *pStudyInstanceUID, const char *pModalities);
		bool CreateSeries(ObjectData *pObject, StudyData *pStudy);
		bool CreateObject(ObjectData *pObject, StudyData *pStudy, const char *pStorageId);
		bool SetStudyRead(StudyData *pStudyInfo);
		bool SetStudyTranscribed(StudyData *pStudyInfo);
		bool GetStudyInfoByAccession(const char *pAccessionNumber, 
			                         char *pPatID, 
									 char *pStudyInstanceUID, 
									 char *pStudyDate, 
									 char *pStatus);
		bool CancelObject(const char *pCOIInstanceUID);

		tool::COCIDatabase *GetDatabase()    {return m_pDb;}

		void Dispose()
		{
			m_sDebugDir.Clear();
		}

	protected:
		Log          *m_pModalityLogger;
		Log          *m_pSeverLogger;
		Mailer       *m_pMailer;
		PacsConfig   *m_pCfg;
		tool::COCIDatabase *m_pDb;

		//	For debug.
		StringBuffer  m_sDebugDir;
		int           m_iTransactionId;
	};
}

#endif	//	__CSTORESERVICE_H_INCLUDED__
