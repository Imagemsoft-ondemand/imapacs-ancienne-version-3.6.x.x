/////////////////////////////////////////////////////////////////////////////
//	CMoveService.h
/////////////////////////////////////////////////////////////////////////////

#include "../DataStructures.h"
#include "../Utils/log.h"
#include "../Utils/Str.h"
#include "../Utils/TimeUtil.h"
#include "../Utils/DBGTool.h"
#include "../Utils/Mailer.h"
#include "../Utils/OCIDatabase.h"
#include "../PacsConfig.h"
#include "../Utils/PacsCommon.h"

#if !defined(__CMOVESERVICE_H_INCLUDED__)
#define      __CMOVESERVICE_H_INCLUDED__

/////////////////////////////////////////////////////////////////////////////
//
namespace imapacs
{
	/////////////////////////////////////////////////////////////////////////
	//
	class CMoveService
	{
	public:
		CMoveService():m_iTransactionId(0)
		{}
		virtual ~CMoveService()
		{}

		void SetConfig(PacsConfig *pCfg)     
		{
			m_pCfg            = pCfg; 
			m_pModalityLogger = pCfg->GetModalityLogger();
			m_pSeverLogger    = pCfg->GetServerLogger();
			m_pMailer         = pCfg->GetMailer();
			m_pDb             = pCfg->GetDb();
		}

		void SetCurrentDebugParams(const char *pDir, int TransactionId)
		{
			if(pDir)
			{
				m_sDebugDir.Format("%s", pDir);
				m_iTransactionId = TransactionId;
			}
		}

		QUERY_STATUS FindCandidates(FindData *pParams, Queue <MoveResult *> *pList);

		tool::COCIDatabase *GetDatabase()    {return m_pDb;}

		void Dispose()
		{
			m_sDebugDir.Clear();
		}

	protected:

		Log          *m_pModalityLogger;
		Log          *m_pSeverLogger;
		Mailer       *m_pMailer;
		PacsConfig   *m_pCfg;
		tool::COCIDatabase *m_pDb;

		StringBuffer  m_sDebugDir;
		int           m_iTransactionId;	//	For debug.
	};
}

#endif	//	__CMOVESERVICE_H_INCLUDED__
