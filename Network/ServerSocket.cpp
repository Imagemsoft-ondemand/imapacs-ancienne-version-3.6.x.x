/////////////////////////////////////////////////////////////////////////////
//	ServerSocket.cpp
/////////////////////////////////////////////////////////////////////////////

#include <string.h>
#include "ServerSocket.h"

/////////////////////////////////////////////////////////////////////////////
//
namespace net
{

/////////////////////////////////////////////////////////////////////////////
//
ServerSocket::ServerSocket()
{
	m_iSrvFd = -1;
	m_iAccFd = -1;
	m_bIsIpc = false;
}

/////////////////////////////////////////////////////////////////////////////
//
ServerSocket::~ServerSocket()
{
}

/////////////////////////////////////////////////////////////////////////////
//
#if defined(WIN32)
int ServerSocket::GetSockOpt(int iLevel, int iOptName, char *pOptVal, int *pOptLen)
#else
int ServerSocket::GetSockOpt(int iLevel, int iOptName, char *pOptVal, socklen_t *pOptLen)	
#endif
{
	return getsockopt(m_iSrvFd, iLevel, iOptName, pOptVal, pOptLen);
}

/////////////////////////////////////////////////////////////////////////////
//
int ServerSocket::SetSockOpt(int iLevel, int iOptName, const char *pOptVal, int iOptLen)
{
	return setsockopt(m_iSrvFd, iLevel, iOptName, pOptVal, iOptLen);
}

/////////////////////////////////////////////////////////////////////////////
//
bool ServerSocket::Bind(unsigned short iPort, int iBackLog, bool bReusable, bool bListen)
{
	struct sockaddr_in host = {0};
	int iOpt                = 1;

	m_bIsIpc                = false;
	m_iSrvFd = socket(AF_INET,SOCK_STREAM,0);
	if(m_iSrvFd == -1)
		return false;

	host.sin_family      = AF_INET;
	host.sin_addr.s_addr = INADDR_ANY;
	host.sin_port        = htons(iPort);
	memset(host.sin_zero,0,8);

	if(bind(m_iSrvFd,(struct sockaddr *)&host,sizeof(struct sockaddr)) == -1)
	{
#if defined(WIN32)
		closesocket(m_iSrvFd);
#else
		close(m_iSrvFd);
#endif
		m_iSrvFd = -1;
		return false;
	}

	if(bReusable)
	{
		//	We don't return an error if it fails because all the rest
		//	can still work.
		setsockopt(m_iSrvFd, SOL_SOCKET, SO_REUSEADDR, (char *)&iOpt, sizeof(int));
	}

	if(bListen)
	{
		if(listen(m_iSrvFd,iBackLog) == -1)
		{
#if defined(WIN32)
			closesocket(m_iSrvFd);
#else
			close(m_iSrvFd);
#endif
			m_iSrvFd = -1;
			return false;
		}
	}
	return true;
}

/////////////////////////////////////////////////////////////////////////////
//	Binds and returns the next available port for the specified interface.
//	If the method fails, it will return -1.
unsigned short ServerSocket::BindNextLocalPort(int iBackLog, bool bReusable, bool bListen)
{
	unsigned short iLocalPort = 0;

	if(Bind(0, iBackLog, bReusable, bListen))
	{
		if(!GetSocketName(NULL, &iLocalPort))
		{
			Close();
		}
	}
	return iLocalPort;
}

/////////////////////////////////////////////////////////////////////////////
//
bool ServerSocket::GetSocketName(char *pIp, unsigned short *pPort)
{
	struct sockaddr_in local = {0};

#if defined(WIN32)
	int sinsize              = sizeof(struct sockaddr);
#else
	socklen_t sinsize        = sizeof(struct sockaddr);
#endif

	if(getsockname(m_iSrvFd,(struct sockaddr *)&local,&sinsize) == -1)
		return false;

	if(pPort)
		(*pPort) = ntohs(local.sin_port);

	if(pIp)
		strcpy(pIp,inet_ntoa(local.sin_addr));

	return true;
}

/////////////////////////////////////////////////////////////////////////////
//
#if !defined(WIN32)
bool ServerSocket::Bind(const char *pPath, int iBackLog, bool bReusable)
{
	struct sockaddr_un host = {0};
	int iOpt                = 1;
	m_bIsIpc                = true;

	unlink(pPath);

	m_iSrvFd = socket(AF_UNIX,SOCK_STREAM,0);
	if(m_iSrvFd == -1)
	{
		return false;
	}

	host.sun_family = AF_UNIX;
	strcpy(host.sun_path,pPath);

	if(bind(m_iSrvFd,(struct sockaddr *)&host,sizeof(host)) == -1)
	{
		close(m_iSrvFd);
		m_iSrvFd = -1;

		return false;
	}

	if(listen(m_iSrvFd,iBackLog) == -1)
	{
		close(m_iSrvFd);
		m_iSrvFd = -1;

		return false;
	}
	return true;
}
#endif

/////////////////////////////////////////////////////////////////////////////
//
int ServerSocket::Accept(int sec, int usec)
{
	int       ret                   = 0;
#if defined(WIN32)
	int       sinsize               = 0;
#else
	socklen_t sinsize               = 0;
#endif
	timeval tmout                   = {0};
	fd_set rset                     = {0};

	tmout.tv_sec  = sec;
	tmout.tv_usec = usec;

	FD_ZERO(&rset);
	FD_SET(m_iSrvFd,&rset);

	if((ret = select(m_iSrvFd + 1,&rset,NULL,NULL,&tmout)) > 0)
	{		
		if(FD_ISSET(m_iSrvFd,&rset))
		{
			if(!m_bIsIpc)
			{
				struct sockaddr_in remote = {0};
				sinsize = sizeof(struct sockaddr);
				if((m_iAccFd = accept(m_iSrvFd,(struct sockaddr*)&remote,&sinsize)) == -1)
					ret = -1;
			}
			else
			{
#if !defined(WIN32)
				struct sockaddr_un remote = {0};
				sinsize = sizeof(remote);
				if((m_iAccFd = accept(m_iSrvFd,(struct sockaddr*)&remote,&sinsize)) == -1)
					ret = -1;
#endif
			}
		}
	}
	else
	{
		ret = -1;
	}
	return ret;
}

/////////////////////////////////////////////////////////////////////////////
//
void ServerSocket::Close()
{
#if defined(WIN32)
	closesocket(m_iSrvFd);
#else
	close(m_iSrvFd);
#endif
	m_iSrvFd = -1;
}

/////////////////////////////////////////////////////////////////////////////
//	WARNING : This function needs to be tested under Unix at least for
//            compilation. I'm not sure about all the error codes, they
//            may not all be in errno.h.
char *ServerSocket::GetLastErrorString() const
{
#if defined(WIN32)
	switch(WSAGetLastError()) 
	{
		case WSAEINTR:           return "WSAEINTR : A blocking operation was interrupted by a call to WSACancelBlockingCall.";
		case WSAEBADF:           return "WSAEBADF : The file or socket handle supplied is not valid.";
		case WSAEACCES:          return "WSAEACCES : An attempt was made to access a socket in a way forbidden by its access permissions.";
		case WSAEDISCON:         return "WSAEDISCON : Returned by WSARecv or WSARecvFrom to indicate the remote party has initiated a graceful shutdown sequence.";
		case WSAEFAULT:          return "WSAEFAULT : The system detected an invalid pointer address in attempting to use a pointer argument in a call.";
		case WSAEINVAL:          return "WSAEINVAL : An invalid argument was supplied.";
		case WSAEMFILE:          return "WSAEMFILE : Too many open sockets.";
		case WSAEWOULDBLOCK:     return "WSAEWOULDBLOCK : A non-blocking socket operation could not be completed immediately.";
		case WSAEINPROGRESS:     return "WSAEINPROGRESS : A blocking operation is currently executing.";
		case WSAEALREADY:        return "WSAEALREADY : An operation was attempted on a non-blocking socket that already had an operation in progress.";
		case WSAENOTSOCK:        return "WSAENOTSOCK : An operation was attempted on something that is not a socket.";
		case WSAEDESTADDRREQ:    return "WSAEDESTADDRREQ : A required address was omitted from an operation on a socket.";
		case WSAEMSGSIZE:        return "WSAEMSGSIZE : A message sent on a datagram socket was larger than the internal message buffer or some other network limit, or the buffer used to receive a datagram into was smaller than the datagram itself.";
		case WSAEPROTOTYPE:      return "WSAEPROTOTYPE : A protocol was specified in the socket function call that does not support the semantics of the socket type requested.";
		case WSAENOPROTOOPT:     return "WSAENOPROTOOPT : An unknown, invalid, or unsupported option or level was specified in a getsockopt or setsockopt call.";
		case WSAEPROTONOSUPPORT: return "WSAEPROTONOSUPPORT : The requested protocol has not been configured into the system, or no implementation for it exists.";
		case WSAESOCKTNOSUPPORT: return "WSAESOCKTNOSUPPORT : The support for the specified socket type does not exist in this address family.";
		case WSAEOPNOTSUPP:      return "WSAEOPNOTSUPP : The attempted operation is not supported for the type of object referenced.";
		case WSAEPFNOSUPPORT:    return "WSAEPFNOSUPPORT : The protocol family has not been configured into the system or no implementation for it exists.";
		case WSAEAFNOSUPPORT:    return "WSAEAFNOSUPPORT : An address incompatible with the requested protocol was used.";
		case WSAEADDRINUSE:      return "WSAEADDRINUSE : Only one usage of each socket address (protocol/network address/port) is normally permitted.";
		case WSAEADDRNOTAVAIL:   return "WSAEADDRNOTAVAIL : The requested address is not valid in its context.";
		case WSAENETDOWN:        return "WSAENETDOWN : A socket operation encountered a dead network.";
		case WSAENETUNREACH:     return "WSAENETUNREACH : A socket operation was attempted to an unreachable network.";
		case WSAENETRESET:       return "WSAENETRESET : The connection has been broken due to keep-alive activity detecting a failure while the operation was in progress.";
		case WSAECONNABORTED:    return "WSAECONNABORTED : An established connection was aborted by the software in your host machine.";
		case WSAECONNRESET:      return "WSAECONNRESET : An existing connection was forcibly closed by the remote host.";
		case WSAENOBUFS:         return "WSAENOBUFS : An operation on a socket could not be performed because the system lacked sufficient buffer space or because a queue was full.";
		case WSAEISCONN:         return "WSAEISCONN : A connect request was made on an already connected socket.";
		case WSAENOTCONN:        return "WSAENOTCONN : A request to send or receive data was disallowed because the socket is not connected and (when sending on a datagram socket using a sendto call) no";
		case WSAESHUTDOWN:       return "WSAESHUTDOWN : A request to send or receive data was disallowed because the socket had already been shut down in that direction with a previous shutdown call.";
		case WSAETOOMANYREFS:    return "WSAETOOMANYREFS : Too many references to some kernel object.";
		case WSAETIMEDOUT:       return "WSAETIMEDOUT : A connection attempt failed because the connected party did not properly respond after a period of time, or established connection failed because connected host has failed to respond.";
		case WSAECONNREFUSED:    return "WSAECONNREFUSED : No connection could be made because the target machine actively refused it.";
		case WSAELOOP:           return "WSAELOOP : Cannot translate name.";
		case WSAENAMETOOLONG:    return "WSAENAMETOOLONG : Name component or name was too long.";
		case WSAEHOSTDOWN:       return "WSAEHOSTDOWN : A socket operation failed because the destination host was down.";
		case WSASYSNOTREADY:     return "WSASYSNOTREADY : WSAStartup cannot function at this time because the underlying system it uses to provide network services is currently unavailable.";
		case WSAVERNOTSUPPORTED: return "WSAVERNOTSUPPORTED : The Windows Sockets version requested is not supported.";
		case WSANOTINITIALISED:  return "WSANOTINITIALISED : Either the application has not called WSAStartup, or WSAStartup failed.";
		case WSAHOST_NOT_FOUND:  return "WSAHOST_NOT_FOUND : No such host is known.";
		case WSATRY_AGAIN:       return "WSATRY_AGAIN : This is usually a temporary error during hostname resolution and means that the local server did not receive a response from an authoritative server.";
		case WSANO_RECOVERY:     return "WSANO_RECOVERY : A non-recoverable error occurred during a database lookup.";
		case WSANO_DATA:         return "WSANO_DATA : The requested name is valid and was found in the database, but it does not have the correct associated data being resolved for.";

		default:                 return "NO ERROR";
	}
#else
	switch(errno)
	{
		case EINTR:              return "EINTR : A blocking call was interrupted by a signal.";
		case EBADF:              return "EBADF : Bad descriptor.";
		case EACCES:             return "EACCES : Permission denied.";
		case EFAULT:             return "EFAULT : A pointer was passed to a system system call that does not belong to the calling process addressing space.";
		case EINVAL:             return "EINVAL : Invalid argument.";
		case EMFILE:             return "EMFILE : Maximum number of descriptors reached.";
		case EWOULDBLOCK:        return "EWOULDBLOCK : A non-blocking socket operation could not be completed immediately.";
		case EINPROGRESS:        return "EINPROGRESS : A blocking operation is currently executing.";
		case EALREADY:           return "EALREADY : Non-blocking socket already have an operation in progress.";
		case ENOTSOCK:           return "ENOTSOCK : Not a socket.";
		case EDESTADDRREQ:       return "EDESTADDRREQ : Address omitted from an operation on a socket.";
		case EMSGSIZE:           return "EMSGSIZE : Datagram too large.";
		case EPROTOTYPE:         return "EPROTOTYPE : Bad protocol.";
		case ENOPROTOOPT:        return "ENOPROTOOPT : Invalid or unknown option or level.";
		case EAFNOSUPPORT:       return "EAFNOSUPPORT : Address incompatble with protocol.";
		case EADDRINUSE:         return "EADDRINUSE : Address or port already in use.";
		case EADDRNOTAVAIL:      return "EADDRNOTAVAIL : Requested address invalid in the context.";
		case ENETDOWN:           return "ENETDOWN : Network down.";
		case ESHUTDOWN:          return "ESHUTDOWN : A request to send or receive data was disallowed because the socket had already been shut down in that direction with a previous shutdown call.";
		case ETIMEDOUT:          return "ETIMEDOUT : Connecttion attempt timedout.";
		case ECONNREFUSED:       return "ECONNREFUSED : Connection refused.";
		case ENAMETOOLONG:       return "ENAMETOOLONG : A name or address was too long";
		case EHOSTDOWN:          return "EHOSTDOWN : Host is down.";

		default:                 return "NO ERROR";
	}
#endif
}

}	//	End namespace net.

