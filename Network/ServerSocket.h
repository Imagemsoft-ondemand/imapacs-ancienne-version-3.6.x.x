/////////////////////////////////////////////////////////////////////////////
//	ServerSocket.h
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//	Windows socket includes.
#if defined (WIN32)

#ifdef _USE_WSOCK_1_	//	To avoid conflicts when using this class with
						//	code already using winsock.h, define this for the project.
#include <Winsock.h>
#else
#include <Winsock2.h>
#endif

/////////////////////////////////////////////////////////////////////////////
//	Unix socket includes.
#else

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/time.h>
#include <sys/select.h>
#include <unistd.h>
#include <netdb.h>
#include <errno.h>

#endif

#if !defined(__SERVERSOCKET_H__)
#define      __SERVERSOCKET_H__

/////////////////////////////////////////////////////////////////////////////
//
namespace net
{

#define	SERVER_SOCKET_VERSION	"1.0.0.2"

class ServerSocket
{
public:
	ServerSocket();
	virtual ~ServerSocket();

	char *GetVersion() const                      {return SERVER_SOCKET_VERSION;}

#if defined(WIN32)
	int GetSockOpt(int iLevel, int iOptName, char *pOptVal, int *pOptLen);
#else
	int GetSockOpt(int iLevel, int iOptName, char *pOptVal, socklen_t *pOptLen);
#endif
	int SetSockOpt(int iLevel, int iOptName, const char *pOptVal, int iOptLen);

	bool Bind(unsigned short iPort, int iBackLog, bool bReusable = true, bool bListen = true);
	unsigned short BindNextLocalPort(int iBackLog = 10, bool bReusable = true, bool bListen = true);

	bool GetSocketName(char *pIp, unsigned short *pPort);

#if !defined(WIN32)
	bool Bind(const char *pPath, int iBackLog, bool bReusable = true);
#endif

	int Accept(int sec, int usec);

#if defined(WIN32)
	SOCKET GetDescriptor()                        {return m_iAccFd;}
#else
	int GetDescriptor()                           {return m_iAccFd;}
#endif

	void Close();
	char *GetLastErrorString() const;

private:

#if defined(WIN32)
	SOCKET m_iSrvFd;
	SOCKET m_iAccFd;
#else
	int    m_iSrvFd;
	int    m_iAccFd;
#endif
	bool   m_bIsIpc;

};

}	//	End namespace net.

#endif	//	__SERVERSOCKET_H__
