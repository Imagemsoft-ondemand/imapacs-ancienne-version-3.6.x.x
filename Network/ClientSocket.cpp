/////////////////////////////////////////////////////////////////////////////
//	ClientSocket.cpp
/////////////////////////////////////////////////////////////////////////////


#include <string.h>
#include "ClientSocket.h"

/////////////////////////////////////////////////////////////////////////////
//
namespace net
{

/////////////////////////////////////////////////////////////////////////////
//
ClientSocket::ClientSocket()
{
	m_iFd = -1;
}

/////////////////////////////////////////////////////////////////////////////
//
ClientSocket::~ClientSocket()
{
}

/////////////////////////////////////////////////////////////////////////////
//
bool ClientSocket::Connect(const char *pIPAddress, unsigned short iPort)
{
	struct sockaddr_in remote = {0};

	remote.sin_family      = AF_INET;
	remote.sin_addr.s_addr = inet_addr(pIPAddress);
	remote.sin_port        = htons(iPort);
	memset(remote.sin_zero,0,8);

	m_iFd = socket(AF_INET,SOCK_STREAM,0);
	if(m_iFd == -1)
	{
#if defined(WIN32)
		closesocket(m_iFd);
#else
		close(m_iFd);
#endif
		m_iFd = -1;
		return false;
	}

	if(connect(m_iFd,(struct sockaddr *)&remote,sizeof(struct sockaddr)) == -1)
	{
#if defined(WIN32)
		closesocket(m_iFd);
#else
		close(m_iFd);
#endif
		m_iFd = -1;
		return false;
	}
	return true;
}

/////////////////////////////////////////////////////////////////////////////
//
#if !defined(WIN32)
bool ClientSocket::Connect(const char *pPath)
{
	struct sockaddr_un srv = {0};

	m_iFd = socket(AF_UNIX,SOCK_STREAM,0);
	if(m_iFd == -1)
	{
		return false;
	}

	srv.sun_family = AF_UNIX;
	strcpy(srv.sun_path,pPath);

	if(connect(m_iFd,(struct sockaddr *)&srv,sizeof(srv)) == -1)
	{
		return false;
	}
	return true;
}
#endif

/////////////////////////////////////////////////////////////////////////////
//
int ClientSocket::Close()
{
	int iRet = -1;
#if defined(WIN32)
	iRet = closesocket(m_iFd);
#else
	iRet = close(m_iFd);
#endif
	m_iFd = -1;

	return iRet;
}

/////////////////////////////////////////////////////////////////////////////
//
int ClientSocket::Read(char *pBuffer, int iBytes)
{
	return recv(m_iFd,pBuffer,iBytes,0);
}

/////////////////////////////////////////////////////////////////////////////
//
int ClientSocket::Write(const char *pBuffer, int iBytes)
{
	return send(m_iFd,pBuffer,iBytes,0);
}

/////////////////////////////////////////////////////////////////////////////
//
int ClientSocket::Select(int sec, int usec)
{
	struct timeval tv = {0};

	tv.tv_sec  = sec;
	tv.tv_usec = usec;

	FD_ZERO(&m_rset);
	FD_SET(m_iFd,&m_rset);

	return select(m_iFd + 1,&m_rset,NULL,NULL,&tv);
}

/////////////////////////////////////////////////////////////////////////////
//
int ClientSocket::FdIsSet()
{
	return FD_ISSET(m_iFd,&m_rset);
}

/////////////////////////////////////////////////////////////////////////////
//
int ClientSocket::Peek()
{
	char byte = 0x00;

	return recv(m_iFd,&byte,1,MSG_PEEK);
}

/////////////////////////////////////////////////////////////////////////////
//
bool ClientSocket::GetHostName(char *pHostName, int iNameLen)
{
	if(gethostname(pHostName,iNameLen) == -1)
		return false;

	return true;
}

/////////////////////////////////////////////////////////////////////////////
//
bool ClientSocket::GetHostByName(const char *pHostName, char *pIPAddress)
{
	struct hostent *pHost = NULL;
	char *ptrtmp          = NULL;

	pHost = gethostbyname(pHostName);
	if(!pHost)
		return false;

	ptrtmp = inet_ntoa(*(struct in_addr*)pHost->h_addr);
	if(!ptrtmp)
		return false;

	strcpy(pIPAddress,ptrtmp);
	 
	return true;
}

/////////////////////////////////////////////////////////////////////////////
//
bool ClientSocket::GetHostByAddr(const char *pIPAddress, char *pHostName)
{
	struct in_addr addr   = {0};
	struct hostent *pHost = NULL;

	addr.s_addr = inet_addr(pIPAddress);

	pHost = gethostbyaddr((char *)&addr,sizeof(struct in_addr),AF_INET);
	if(!pHost)
		return false;

	strcpy(pHostName,pHost->h_name);

	return true;
}

/////////////////////////////////////////////////////////////////////////////
//
#if defined(WIN32)
int ClientSocket::GetSockOpt(int iLevel, int iOptName, char *pOptVal, int *pOptLen)
#else
int ClientSocket::GetSockOpt(int iLevel, int iOptName, char *pOptVal, socklen_t *pOptLen)
#endif
{
	return getsockopt(m_iFd, iLevel, iOptName, pOptVal, pOptLen);
}

/////////////////////////////////////////////////////////////////////////////
//
int ClientSocket::SetSockOpt(int iLevel, int iOptName, const char *pOptVal, int iOptLen)
{
	return setsockopt(m_iFd, iLevel, iOptName, pOptVal, iOptLen);
}

/////////////////////////////////////////////////////////////////////////////
//
bool ClientSocket::GetPeerIp(char *pIp)
{
	struct sockaddr_in addr = {0};

#if defined(WIN32)
	int size                = sizeof(struct sockaddr);
#else
    socklen_t size          = sizeof(struct sockaddr);
#endif

	if(getpeername(m_iFd,(struct sockaddr *)&addr,&size) == -1)
		return false;

	strcpy(pIp, inet_ntoa(addr.sin_addr));

	return true;
}

/////////////////////////////////////////////////////////////////////////////
//
bool ClientSocket::GetSockIp(char *pIp)
{
	struct sockaddr_in addr = {0};

#if defined(WIN32)
	int size                = sizeof(struct sockaddr);
#else
    socklen_t size          = sizeof(struct sockaddr);
#endif

	if(getsockname(m_iFd, (struct sockaddr *)&addr, &size) == -1)
		return false;

	strcpy(pIp, inet_ntoa(addr.sin_addr));

	return true;
}

/////////////////////////////////////////////////////////////////////////////
//
bool ClientSocket::GetPeerName(char *pPeerName)
{
	struct hostent *pHost   = NULL;
	struct sockaddr_in addr = {0};

#if defined(WIN32)
	int size                = sizeof(struct sockaddr);
#else
    socklen_t size          = sizeof(struct sockaddr);
#endif

	if(getpeername(m_iFd,(struct sockaddr *)&addr,&size) == -1)
		return false;

	pHost = gethostbyaddr((char *)&addr.sin_addr.s_addr,sizeof(struct in_addr),AF_INET);
	if(!pHost)
		return false;

	strcpy(pPeerName,pHost->h_name);

	return true;
}

/////////////////////////////////////////////////////////////////////////////
//
bool ClientSocket::GetSockName(char *pSockName)
{
	struct hostent *pHost   = NULL;
	struct sockaddr_in addr = {0};

#if defined(WIN32)
	int size                = sizeof(struct sockaddr);
#else
    socklen_t size          = sizeof(struct sockaddr);
#endif

	if(getsockname(m_iFd, (struct sockaddr *)&addr, &size) == -1)
		return false;

	pHost = gethostbyaddr((char *)&addr.sin_addr.s_addr,sizeof(struct in_addr),AF_INET);
	if(!pHost)
		return false;

	strcpy(pSockName,pHost->h_name);

	return true;
}

/////////////////////////////////////////////////////////////////////////////
//	WARNING : This function needs to be tested under Unix at least for
//            compilation. I'm not sure about all the error codes, they
//            may not all be in errno.h.
char *ClientSocket::GetLastErrorString() const
{
#if defined(WIN32)
	switch(WSAGetLastError()) 
	{
		case WSAEINTR:           return "WSAEINTR : A blocking operation was interrupted by a call to WSACancelBlockingCall.";
		case WSAEBADF:           return "WSAEBADF : The file or socket handle supplied is not valid.";
		case WSAEACCES:          return "WSAEACCES : An attempt was made to access a socket in a way forbidden by its access permissions.";
		case WSAEDISCON:         return "WSAEDISCON : Returned by WSARecv or WSARecvFrom to indicate the remote party has initiated a graceful shutdown sequence.";
		case WSAEFAULT:          return "WSAEFAULT : The system detected an invalid pointer address in attempting to use a pointer argument in a call.";
		case WSAEINVAL:          return "WSAEINVAL : An invalid argument was supplied.";
		case WSAEMFILE:          return "WSAEMFILE : Too many open sockets.";
		case WSAEWOULDBLOCK:     return "WSAEWOULDBLOCK : A non-blocking socket operation could not be completed immediately.";
		case WSAEINPROGRESS:     return "WSAEINPROGRESS : A blocking operation is currently executing.";
		case WSAEALREADY:        return "WSAEALREADY : An operation was attempted on a non-blocking socket that already had an operation in progress.";
		case WSAENOTSOCK:        return "WSAENOTSOCK : An operation was attempted on something that is not a socket.";
		case WSAEDESTADDRREQ:    return "WSAEDESTADDRREQ : A required address was omitted from an operation on a socket.";
		case WSAEMSGSIZE:        return "WSAEMSGSIZE : A message sent on a datagram socket was larger than the internal message buffer or some other network limit, or the buffer used to receive a datagram into was smaller than the datagram itself.";
		case WSAEPROTOTYPE:      return "WSAEPROTOTYPE : A protocol was specified in the socket function call that does not support the semantics of the socket type requested.";
		case WSAENOPROTOOPT:     return "WSAENOPROTOOPT : An unknown, invalid, or unsupported option or level was specified in a getsockopt or setsockopt call.";
		case WSAEPROTONOSUPPORT: return "WSAEPROTONOSUPPORT : The requested protocol has not been configured into the system, or no implementation for it exists.";
		case WSAESOCKTNOSUPPORT: return "WSAESOCKTNOSUPPORT : The support for the specified socket type does not exist in this address family.";
		case WSAEOPNOTSUPP:      return "WSAEOPNOTSUPP : The attempted operation is not supported for the type of object referenced.";
		case WSAEPFNOSUPPORT:    return "WSAEPFNOSUPPORT : The protocol family has not been configured into the system or no implementation for it exists.";
		case WSAEAFNOSUPPORT:    return "WSAEAFNOSUPPORT : An address incompatible with the requested protocol was used.";
		case WSAEADDRINUSE:      return "WSAEADDRINUSE : Only one usage of each socket address (protocol/network address/port) is normally permitted.";
		case WSAEADDRNOTAVAIL:   return "WSAEADDRNOTAVAIL : The requested address is not valid in its context.";
		case WSAENETDOWN:        return "WSAENETDOWN : A socket operation encountered a dead network.";
		case WSAENETUNREACH:     return "WSAENETUNREACH : A socket operation was attempted to an unreachable network.";
		case WSAENETRESET:       return "WSAENETRESET : The connection has been broken due to keep-alive activity detecting a failure while the operation was in progress.";
		case WSAECONNABORTED:    return "WSAECONNABORTED : An established connection was aborted by the software in your host machine.";
		case WSAECONNRESET:      return "WSAECONNRESET : An existing connection was forcibly closed by the remote host.";
		case WSAENOBUFS:         return "WSAENOBUFS : An operation on a socket could not be performed because the system lacked sufficient buffer space or because a queue was full.";
		case WSAEISCONN:         return "WSAEISCONN : A connect request was made on an already connected socket.";
		case WSAENOTCONN:        return "WSAENOTCONN : A request to send or receive data was disallowed because the socket is not connected and (when sending on a datagram socket using a sendto call) no";
		case WSAESHUTDOWN:       return "WSAESHUTDOWN : A request to send or receive data was disallowed because the socket had already been shut down in that direction with a previous shutdown call.";
		case WSAETOOMANYREFS:    return "WSAETOOMANYREFS : Too many references to some kernel object.";
		case WSAETIMEDOUT:       return "WSAETIMEDOUT : A connection attempt failed because the connected party did not properly respond after a period of time, or established connection failed because connected host has failed to respond.";
		case WSAECONNREFUSED:    return "WSAECONNREFUSED : No connection could be made because the target machine actively refused it.";
		case WSAELOOP:           return "WSAELOOP : Cannot translate name.";
		case WSAENAMETOOLONG:    return "WSAENAMETOOLONG : Name component or name was too long.";
		case WSAEHOSTDOWN:       return "WSAEHOSTDOWN : A socket operation failed because the destination host was down.";
		case WSASYSNOTREADY:     return "WSASYSNOTREADY : WSAStartup cannot function at this time because the underlying system it uses to provide network services is currently unavailable.";
		case WSAVERNOTSUPPORTED: return "WSAVERNOTSUPPORTED : The Windows Sockets version requested is not supported.";
		case WSANOTINITIALISED:  return "WSANOTINITIALISED : Either the application has not called WSAStartup, or WSAStartup failed.";
		case WSAHOST_NOT_FOUND:  return "WSAHOST_NOT_FOUND : No such host is known.";
		case WSATRY_AGAIN:       return "WSATRY_AGAIN : This is usually a temporary error during hostname resolution and means that the local server did not receive a response from an authoritative server.";
		case WSANO_RECOVERY:     return "WSANO_RECOVERY : A non-recoverable error occurred during a database lookup.";
		case WSANO_DATA:         return "WSANO_DATA : The requested name is valid and was found in the database, but it does not have the correct associated data being resolved for.";

		default:                 return "NO ERROR";
	}
#else
	switch(errno)
	{
		case EINTR:              return "EINTR : A blocking call was interrupted by a signal.";
		case EBADF:              return "EBADF : Bad descriptor.";
		case EACCES:             return "EACCES : Permission denied.";
		case EFAULT:             return "EFAULT : A pointer was passed to a system system call that does not belong to the calling process addressing space.";
		case EINVAL:             return "EINVAL : Invalid argument.";
		case EMFILE:             return "EMFILE : Maximum number of descriptors reached.";
		case EWOULDBLOCK:        return "EWOULDBLOCK : A non-blocking socket operation could not be completed immediately.";
		case EINPROGRESS:        return "EINPROGRESS : A blocking operation is currently executing.";
		case EALREADY:           return "EALREADY : Non-blocking socket already have an operation in progress.";
		case ENOTSOCK:           return "ENOTSOCK : Not a socket.";
		case EDESTADDRREQ:       return "EDESTADDRREQ : Address omitted from an operation on a socket.";
		case EMSGSIZE:           return "EMSGSIZE : Datagram too large.";
		case EPROTOTYPE:         return "EPROTOTYPE : Bad protocol.";
		case ENOPROTOOPT:        return "ENOPROTOOPT : Invalid or unknown option or level.";
		case EAFNOSUPPORT:       return "EAFNOSUPPORT : Address incompatble with protocol.";
		case EADDRINUSE:         return "EADDRINUSE : Address or port already in use.";
		case EADDRNOTAVAIL:      return "EADDRNOTAVAIL : Requested address invalid in the context.";
		case ENETDOWN:           return "ENETDOWN : Network down.";
		case ESHUTDOWN:          return "ESHUTDOWN : A request to send or receive data was disallowed because the socket had already been shut down in that direction with a previous shutdown call.";
		case ETIMEDOUT:          return "ETIMEDOUT : Connecttion attempt timedout.";
		case ECONNREFUSED:       return "ECONNREFUSED : Connection refused.";
		case ENAMETOOLONG:       return "ENAMETOOLONG : A name or address was too long";
		case EHOSTDOWN:          return "EHOSTDOWN : Host is down.";

		default:                 return "NO ERROR";
	}
#endif
}

}	//	End namespace net.
