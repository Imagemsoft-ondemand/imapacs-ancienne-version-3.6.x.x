/////////////////////////////////////////////////////////////////////////////
//	ClientSocket.h
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//	Windows socket include.
#if defined (WIN32)

#ifdef _USE_WSOCK_1_	//	To avoid conflicts when using this class with
						//	code already using winsock.h, define this for the project.

#include <Winsock.h>
#else
#include <Winsock2.h>
#endif

/////////////////////////////////////////////////////////////////////////////
//	Unix socket includes.
#else

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/time.h>
#include <sys/select.h>
#include <unistd.h>
#include <netdb.h>
#include <errno.h>

#endif

#if !defined(__CLIENTSOCKET_H__)
#define      __CLIENTSOCKET_H__

/////////////////////////////////////////////////////////////////////////////
//
namespace net
{

#define	CLIENT_SOCKET_VERSION	"1.0.0.2"

/////////////////////////////////////////////////////////////////////////////
//
class ClientSocket
{
public:
	ClientSocket();
	virtual ~ClientSocket();

	char *GetVersion() const                                        {return CLIENT_SOCKET_VERSION;}

	bool Connect(const char *pIPAddress, unsigned short iPort);

#if !defined(WIN32)
	bool Connect(const char *pPath);
#endif

	int Close();

	int Read(char *pBuffer, int iBytes);
	int Write(const char *pBuffer, int iBytes);

	int Select(int sec, int usec);
	int FdIsSet();
	int Peek();

	bool GetPeerIp(char *pIp);
	bool GetSockIp(char *pIp);

	bool GetPeerName(char *pPeerName);
	bool GetSockName(char *pSockName);

	bool GetHostName(char *pHostName, int iNameLen);
	bool GetHostByName(const char *pHostName, char *pIPAddress);
	bool GetHostByAddr(const char *pIPAddress, char *pHostName);

#if defined(WIN32)
	int GetSockOpt(int iLevel, int iOptName, char *pOptVal, int *pOptLen);
#else
	int GetSockOpt(int iLevel, int iOptName, char *pOptVal, socklen_t *pOptLen);
#endif
	int SetSockOpt(int iLevel, int iOptName, const char *pOptVal, int iOptLen);

	char *GetLastErrorString() const;

#if defined(WIN32)
	void SetDescriptor(SOCKET fd)                                   {m_iFd = fd;}
	SOCKET GetDescriptor()                                          {return m_iFd;}
#else
	void SetDescriptor(int fd)                                      {m_iFd = fd;}
	int GetDescriptor()                                             {return m_iFd;}
#endif

private:

#if defined(WIN32)
	SOCKET m_iFd;
#else
	int    m_iFd;
#endif
	fd_set m_rset;

};

}	//	End namespace net.

#endif	//	__CLIENTSOCKET_H__
