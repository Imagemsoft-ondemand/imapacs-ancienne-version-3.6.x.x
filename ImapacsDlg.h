/////////////////////////////////////////////////////////////////////////////
//	ImapacsDlg.h
/////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Descriptor.h"

/////////////////////////////////////////////////////////////////////////////
//
class CImapacsDlg : public CDialog
{
// Construction
public:
	CImapacsDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_IMAPACS_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedRun();
	afx_msg void OnBnClickedRunLoop();
};
