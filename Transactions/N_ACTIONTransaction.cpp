/////////////////////////////////////////////////////////////////////////////
//	N_ACTIONTransaction.cpp
/////////////////////////////////////////////////////////////////////////////

#include "../Utils/DICOMUtil.h"
#include "../Utils/TimeUtil.h"
#include "../Utils/CDCMClient.h"
#include "../DataStructures.h"
#include "N_ACTIONTransaction.h"

/////////////////////////////////////////////////////////////////////////////
//
namespace imapacs
{
	/////////////////////////////////////////////////////////////////////////
	//	Initialization of the N-ACTION transaction class.
	bool N_ACTIONTransaction::Init(T_ASC_Association *pAssociation,
		                           T_DIMSE_Message *pMsg,
								   T_ASC_PresentationContextID PresID,
								   PacsConfig *pCfg)
	{
		bool bRetVal = false;

		memcpy(&m_CurrentReq, pMsg, sizeof(T_DIMSE_Message));
		memset(&m_CurrentRsp, 0, sizeof(T_DIMSE_Message));

		m_CurrentRsp.CommandField             = DIMSE_C_ECHO_RSP;
		m_CurrentRsp.msg.CEchoRSP.DataSetType = DIMSE_DATASET_NULL;
		m_CurrentRsp.msg.CEchoRSP.DimseStatus = STATUS_Success;	//	Assuming.

		m_pAssociation  = pAssociation;
		m_CurrentPresID = PresID;

		m_pCfg = pCfg;

		//	For convenience.
		m_pServerLogger   = pCfg->GetServerLogger();
		m_pModalityLogger = pCfg->GetModalityLogger();
		m_pMailer         = pCfg->GetMailer();

		m_pServerLogger->LogMsg("[%s] - N-ACTION request.", pAssociation->params->DULparams.callingAPTitle);
		m_pModalityLogger->LogMsg("N-ACTION request.");
		m_pMailer->AddLine("N-ACTION request.");

		if(pCfg->GetModalityDebug())
		{
			//	Generate a transaction id to group
			//	transaction related file names.
			srand((unsigned int)time(NULL));
			m_iTransactionId = rand();

			m_sCurrentDebugDir.Format("%s%s", DEBUG_DIRECTORY, pAssociation->params->DULparams.callingAPTitle);
			os::MkDir((char *)m_sCurrentDebugDir);
		}
		m_DataService.SetCurrentDebugParams((char *)m_sCurrentDebugDir, m_iTransactionId);
		m_DataService.SetConfig(pCfg);

		bRetVal = true;

		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//	Process all supported transactions base on the N-ACTION service.
	bool N_ACTIONTransaction::ProcessTransaction()
	{
		bool bRetVal                           = false;
		OFCondition cond                       = EC_Normal;
		DcmDataset *pDataset                   = NULL;
		DcmDataset *pCmommand                  = NULL;
		T_ASC_PresentationContextID DataPresID = 0x00;
		DBGTool dbg;

		if(m_pCfg->GetModalityDebug())
		{
			m_pServerLogger->LogMsg("--N_ACTIONTransaction::ProcessTransaction()");
		}

		/////////////////////////////////////////////////////////////////////
		//	Read dataset if any.
		if(m_CurrentReq.msg.NActionRQ.DataSetType == DIMSE_DATASET_PRESENT)
		{
			if(m_pCfg->GetProfiling())
				dbg.StartProfiling(m_pServerLogger, "Receiving request dataset.");

			cond = DIMSE_receiveDataSetInMemory(m_pAssociation, DIMSE_BLOCKING, 0, &DataPresID, &pDataset, NULL, NULL);

			if(m_pCfg->GetProfiling())
				dbg.EndProfiling(m_pServerLogger, NULL, "Dataset received.");

			if(cond.bad())
			{
				m_pModalityLogger->LogMsg("ERROR : %s", cond.text());
				m_pMailer->AddLine("%s", cond.text());

				DICOMUtil::ClearDataset(&pDataset);
				return false;
			}
			m_CurrentDataPresID = DataPresID;
		}

		/////////////////////////////////////////////////////////////////////
		//	Filter transactions by requested SOP class.
		if(strcmp(m_CurrentReq.msg.NActionRQ.RequestedSOPClassUID, UID_StorageCommitmentPushModelSOPClass) == 0)
		{
			m_pServerLogger->LogMsg("[%s] - Storage commitment.", m_pAssociation->params->DULparams.callingAPTitle);
			m_pModalityLogger->LogMsg("Storage commitment.");
			m_pModalityLogger->LogMsg(" ");
			m_pMailer->AddLine("Storage commitment.");

			bRetVal = ProcessStorageCommitment(pDataset);
		}
		else if(strcmp(m_CurrentReq.msg.NActionRQ.RequestedSOPClassUID, UID_RETIRED_GeneralPurposeScheduledProcedureStepSOPClass) == 0)
		{
			m_pServerLogger->LogMsg("[%s] - Update GP-SPS.", m_pAssociation->params->DULparams.callingAPTitle);
			m_pModalityLogger->LogMsg("Update GP-SPS.");
			m_pMailer->AddLine("Update GP-SPS.");

			bRetVal = ProcessUpdateGPSPS(pDataset);
		}
		else
		{
			m_pServerLogger->LogMsg("[%s] - Unsupported N-ACTION SOP class UID : [%s].", 
				                    m_pAssociation->params->DULparams.callingAPTitle,
									m_CurrentReq.msg.NActionRQ.RequestedSOPClassUID);

			m_pModalityLogger->LogMsg("Unsupported N-ACTION SOP class UID.");
			m_pMailer->AddLine("Unsupported N-ACTION SOP class UID.");
		}

		DICOMUtil::ClearDataset(&pDataset);

		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//	Class cleanup.
	void N_ACTIONTransaction::Shutdown()
	{
		m_DataService.Dispose();
		m_sCurrentDebugDir.Clear();
	}

	/////////////////////////////////////////////////////////////////////////
	//	Process a storage commitment request from a client. Only the
	//	storage commitment push model SOP class is supported.
	bool N_ACTIONTransaction::ProcessStorageCommitment(DcmDataset *pDataset)
	{
		bool bRetVal                    = false;
		bool bMustReleaseAssociation    = false;
		OFCondition cond                = EC_Normal;
		char sRemoteIP[21]              = {0};
		unsigned short iRemotePort      = 0;
		T_ASC_Association *pAssociation = 0;
		StorageCommitmentActionReq pReqData;
		CDCMClient DicomClient;

		if(ProcessStorageCommitment_N_ACTION(pDataset, &pReqData))
		{
			/////////////////////////////////////////////////////////////////
			//	This modality is configured to use another association
			//	for the N-EVENT-REPORT transaction.
			if(m_pCfg->GetCommitAddressAndPort(m_pAssociation->params->DULparams.callingAPTitle,
				                               sRemoteIP,
											   &iRemotePort))
			{
				DicomClient.SetCalledAE(m_pAssociation->params->DULparams.callingAPTitle);
				DicomClient.SetCallingAE("IMAPACS");
				DicomClient.SetRemoteIP(sRemoteIP);
				DicomClient.SetRemotePort(iRemotePort);
				DicomClient.SetConfig(m_pCfg);

				if(m_pCfg->GetModalityDebug())
				{
					m_pModalityLogger->LogMsg("Will use secondary association for responce :");
					m_pModalityLogger->LogMsg("---------------------------------------------");
					m_pModalityLogger->LogMsg("   Address          : %s", sRemoteIP);
					m_pModalityLogger->LogMsg("   Port             : %d", iRemotePort);
					m_pModalityLogger->LogMsg("   Calling ae title : %s", "IMAPACS");
					m_pModalityLogger->LogMsg("   Called ae title  : %s", m_pAssociation->params->DULparams.callingAPTitle);
					m_pModalityLogger->LogMsg(" ");
				}

				if(DicomClient.CreateAssociation(false))
				{
					bMustReleaseAssociation = true;

					if(DicomClient.RequestAssociation())
					{
						if(m_pCfg->GetModalityDebug())
						{
							m_pModalityLogger->LogMsg("Secondary association established.");
						}
						pAssociation = DicomClient.GetAssociation();
					}
					else
					{
						m_pModalityLogger->LogMsg("ERROR : %s", DicomClient.GetLastErrorText());
						m_pModalityLogger->LogMsg("      : Failed to establish association with client.");

						m_pMailer->AddLine("ERROR : %s", DicomClient.GetLastErrorText());
						m_pMailer->AddLine("      : Failed to establish association with client.");
					}
				}
				else
				{
					m_pModalityLogger->LogMsg("ERROR : %s", DicomClient.GetLastErrorText());
					m_pModalityLogger->LogMsg("        Could not create association in N_ACTIONTransaction::ProcessStorageCommitment.");

					m_pMailer->AddLine("ERROR : %s", DicomClient.GetLastErrorText());
					m_pMailer->AddLine("        Could not create association in N_ACTIONTransaction::ProcessStorageCommitment.");
				}
			}
			else
			{
				/////////////////////////////////////////////////////////////
				//	Use the same association for N-EVENT-REPORT.
				pAssociation = m_pAssociation;

				if(m_pCfg->GetModalityDebug())
				{
					m_pModalityLogger->LogMsg("Will current association for responce.");
				}
			}

			if(pAssociation)
			{
				strcpy(pReqData.sCalledAeTitle, pAssociation->params->DULparams.calledAPTitle);

				bRetVal = ProcessStorageCommitment_N_EVENT_REPORT(&pReqData, pAssociation);

				if(bMustReleaseAssociation)
				{
					DicomClient.CloseAssociation();
				}
			}
		}
		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//	
	bool N_ACTIONTransaction::ProcessStorageCommitment_N_ACTION(DcmDataset *pDataset, StorageCommitmentActionReq *pReqData)
	{
		bool bRetVal        = false;
		OFCondition cond    = EC_Normal;
		DcmDataset *pRspCmd = 0;
		char sDateTime[21]  = {0};
		StringBuffer dump;
		DBGTool dbg;

		/////////////////////////////////////////////////////////////////////
		//	Initialize responce header.
		m_CurrentRsp.CommandField                             = DIMSE_N_ACTION_RSP;
		m_CurrentRsp.msg.NActionRSP.DimseStatus               = STATUS_Success;		//	Assuming.
		m_CurrentRsp.msg.NActionRSP.MessageIDBeingRespondedTo = m_CurrentReq.msg.NActionRQ.MessageID;
		m_CurrentRsp.msg.NActionRSP.ActionTypeID              = m_CurrentReq.msg.NActionRQ.ActionTypeID;
		m_CurrentRsp.msg.NActionRSP.DataSetType               = DIMSE_DATASET_NULL;
		m_CurrentRsp.msg.NActionRSP.opts                      = O_NACTION_AFFECTEDSOPCLASSUID |
															    O_NACTION_AFFECTEDSOPINSTANCEUID |
															    O_NACTION_ACTIONTYPEID;

		strcpy(m_CurrentRsp.msg.NActionRSP.AffectedSOPClassUID, m_CurrentReq.msg.NActionRQ.RequestedSOPClassUID);
		strcpy(m_CurrentRsp.msg.NActionRSP.AffectedSOPInstanceUID, m_CurrentReq.msg.NActionRQ.RequestedSOPInstanceUID);

		/////////////////////////////////////////////////////////////////////
		//	Get data from request.
		ExtractStorageCommitmentRequestData(pDataset, pReqData);

		//	Dump the dataset if debug mode is
		//	on for that AE title.
		if(m_pCfg->GetModalityDebug())
		{
			Time::GetDateTime(sDateTime);
			dump.Format("%s/StorageCommitmentRQ%s_%d.dcm", (char *)m_sCurrentDebugDir, sDateTime, m_iTransactionId);

			if(pDataset)
			{
				cond = pDataset->saveFile((char *)dump, EXS_LittleEndianExplicit);
				if(cond.bad())
				{
					m_pModalityLogger->LogMsg("ERROR %s", cond.text());
					m_pModalityLogger->LogMsg("      Failed to save debug dataset.");
				}
			}

			m_pModalityLogger->LogMsg("Sending N-ACTION responce.");
		}

		/////////////////////////////////////////////////////////////////////
		//	Send N-ACTION responce.
		cond = DIMSE_sendMessageUsingMemoryData(m_pAssociation,
												m_CurrentPresID,
												&m_CurrentRsp,
												NULL, 
												NULL, 
												NULL, 
												NULL, 
												&pRspCmd);

		if(cond.good() && (m_CurrentRsp.msg.NActionRSP.DimseStatus  == STATUS_Success))
		{
			if(m_pCfg->GetModalityDebug())
			{
				m_pModalityLogger->LogMsg("N-ACTION responce successfully sent.");
				m_pModalityLogger->LogMsg(" ");
			}
			bRetVal = true;
		}
		else
		{
			m_pModalityLogger->LogMsg("ERROR : %s", cond.text());
			m_pModalityLogger->LogMsg("        Unable to send N-ACTION responce.");

			m_pMailer->AddLine("%s", cond.text());
			m_pMailer->AddLine("Unable to send N-ACTION responce.");
		}
		DICOMUtil::ClearDataset(&pRspCmd);

		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//	Process the N-EVENT-REPORT-RQ from the server using the
	//	appropriate association.
	bool N_ACTIONTransaction::ProcessStorageCommitment_N_EVENT_REPORT(StorageCommitmentActionReq *pReqData, 
		                                                              T_ASC_Association *pAssociation)
	{
		bool bRetVal                                 = false;
		int iEventTypeId                             = 0;
		char sDateTime[21]                           = {0};
		DcmDataset *pDataset                         = NULL;
		DcmDataset *pStatus                          = NULL;
		OFCondition cond                             = EC_Normal;
		T_ASC_PresentationContextID CurrentPresCtxID = 0;
		T_ASC_PresentationContextID RspPresCtxID     = 0;
		T_DIMSE_Message N_Event;
		T_DIMSE_Message N_EventRsp;
		StringBuffer dump;

		memset(&N_Event, 0, sizeof(T_DIMSE_Message));
		memset(&N_EventRsp, 0, sizeof(T_DIMSE_Message));

		N_Event.CommandField                    = DIMSE_N_EVENT_REPORT_RQ;
		N_Event.msg.NEventReportRSP.DimseStatus = STATUS_Success;
		N_Event.msg.NEventReportRQ.MessageID    = m_pAssociation->nextMsgID++;
		N_Event.msg.NEventReportRQ.DataSetType  = DIMSE_DATASET_PRESENT;

		strcpy(N_Event.msg.NEventReportRQ.AffectedSOPClassUID, UID_StorageCommitmentPushModelSOPClass);
		strcpy(N_Event.msg.NEventReportRQ.AffectedSOPInstanceUID, UID_StorageCommitmentPushModelSOPInstance);

		if(m_pCfg->GetModalityDebug())
		{
			m_pModalityLogger->LogMsg("Processing N-EVENT-REPORT ...");
			m_pModalityLogger->LogMsg(" ");
		}

		//	Create the dataset for the N-EVENT-REPORT request.
		pDataset = Create_N_EVENT_REPORT_RQ(pReqData, &iEventTypeId);
		if(pDataset)
		{
			N_Event.msg.NEventReportRQ.EventTypeID = iEventTypeId;

			CurrentPresCtxID = ASC_findAcceptedPresentationContextID(pAssociation, UID_StorageCommitmentPushModelSOPClass);
			if(CurrentPresCtxID)
			{
				if(m_pCfg->GetModalityDebug())
				{
					m_pModalityLogger->LogMsg("Sending N-EVENT-REPORT-RQ to client.");
				}

				cond = DIMSE_sendMessageUsingMemoryData(pAssociation, CurrentPresCtxID, &N_Event, NULL, pDataset, NULL, NULL, &pStatus);
				if(cond.good())
				{
					if(m_pCfg->GetModalityDebug())
					{
						m_pModalityLogger->LogMsg("Reading N-EVENT-REPORT-RSP from client.");
					}

					//	Read the client's responce (if any).
					cond = DIMSE_receiveCommand(pAssociation,
						                        DIMSE_BLOCKING,
												0,
												&RspPresCtxID,
												&N_EventRsp,
												NULL);
					if(cond.good())
					{
						if(m_pCfg->GetModalityDebug())
						{
							m_pModalityLogger->LogMsg("N-EVENT-REPORT-RSP successfully sent.");
							m_pModalityLogger->LogMsg(" ");
						}
						bRetVal = true;
					}
					else
					{
						if(cond == DUL_PEERABORTEDASSOCIATION)
						{
							m_pModalityLogger->LogMsg("WARNING :  Failed to read N-EVENT-REPORT-RSP");
							m_pModalityLogger->LogMsg("           Peer aborted association.");
							m_pModalityLogger->LogMsg("           This is a legitimate behavior.");

							bRetVal = true;
						}
						else
						{
							m_pModalityLogger->LogMsg("ERROR : %s", cond.text());
							m_pMailer->AddLine("ERROR : %s", cond.text());
						}
					}
				}
				else
				{
					m_pModalityLogger->LogMsg("ERROR : %s", cond.text());
					m_pModalityLogger->LogMsg("        Could not send N-EVENT-REPORT-RQ to client.");

					m_pMailer->AddLine("ERROR : %s", cond.text());
					m_pMailer->AddLine("        Could not send N-EVENT-REPORT-RQ to client.");
				}
			}
			else
			{
				m_pModalityLogger->LogMsg("ERROR : Invalid presentation context id");
				m_pMailer->AddLine("ERROR : Invalid presentation context id");
			}

			//	Dump the dataset if debug mode is
			//	on for that AE title.
			if(m_pCfg->GetModalityDebug())
			{
				Time::GetDateTime(sDateTime);
				dump.Format("%s/StorageCommitment_EVEN_RQ%s_%d.dcm", (char *)m_sCurrentDebugDir, sDateTime, m_iTransactionId);

				if(pDataset)
				{
					cond = pDataset->saveFile((char *)dump, EXS_LittleEndianExplicit);
					if(cond.bad())
					{
						m_pModalityLogger->LogMsg("ERROR %s", cond.text());
						m_pModalityLogger->LogMsg("      Failed to save debug dataset.");
					}
				}
			}
			DICOMUtil::ClearDataset(&pStatus);
			DICOMUtil::ClearDataset(&pDataset);
		}
		//	NOTE :
		//	Create_N_EVENT_REPORT_RQ method already logs
		//	error message in case pDataset is NULL.
		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	bool N_ACTIONTransaction::ExtractStorageCommitmentRequestData(DcmDataset *pDataset, 
		                                                          StorageCommitmentActionReq *pReqData)
	{
		bool bRetVal                     = false;
		OFCondition cond                 = EC_Normal;
		DcmItem *pItem                   = 0;
		unsigned long iSeqCount          = 0;
		char sSOPClassUID[65]            = {0};
		char sSOPInstanceUID[65]         = {0};
		SOPClassInstancePaire *pSopPaire = 0;
		DcmSequenceOfItems *pRefSOPSeq   = 0;

		if(pDataset && pReqData)
		{
			DICOMUtil::GetTagFromDataset(pDataset, DCM_TransactionUID, pReqData->sTransactionUID);

			if(pReqData->sTransactionUID[0])
			{
				if(m_pCfg->GetModalityDebug())
				{
					m_pModalityLogger->LogMsg("Extracting infos from received dataset.");
					m_pModalityLogger->LogMsg(" ");
					m_pModalityLogger->LogMsg("Transaction UID (0x0008, 0x1195) : %s", pReqData->sTransactionUID);
					m_pModalityLogger->LogMsg(" ");
				}

				cond = pDataset->findAndGetSequence(DCM_ReferencedSOPSequence, pRefSOPSeq);
				if(cond.good())
				{
					iSeqCount = pRefSOPSeq->card();
					if(iSeqCount)
					{
						if(m_pCfg->GetModalityDebug())
						{
							m_pModalityLogger->LogMsg("Storage commitment requested for objects :");
							m_pModalityLogger->LogMsg(" ");
						}

						/////////////////////////////////////////////////////////////
						//	If the sequence attribute is existent and non-empty, we
						//	need to check every item in the sequence for completeness.
						for(unsigned long i = 0; i < iSeqCount; ++i)
						{
							pItem = pRefSOPSeq->getItem(i);
							if(pItem)
							{
								DICOMUtil::GetTagFromDataset(pItem, DCM_ReferencedSOPClassUID, sSOPClassUID);
								DICOMUtil::GetTagFromDataset(pItem, DCM_ReferencedSOPInstanceUID, sSOPInstanceUID);

								if(m_pCfg->GetModalityDebug())
								{
									m_pModalityLogger->LogMsg(" ");
									m_pModalityLogger->LogMsg("  SOP Class    : %s", sSOPClassUID);
									m_pModalityLogger->LogMsg("  SOP Instance : %s", sSOPInstanceUID);
									m_pModalityLogger->LogMsg(" ");
								}

								pSopPaire = new SOPClassInstancePaire(sSOPClassUID, sSOPInstanceUID);
								if(pSopPaire)
								{
									pReqData->m_aObjectCheckList.Add(pSopPaire);
								}
							}
						}
						bRetVal = true;
					}
					else
					{
						m_pModalityLogger->LogMsg("ERROR : Referenced SOP sequence (0x0008, 0x1199) is present but has no element.");
						m_CurrentRsp.msg.NActionRSP.DimseStatus = STATUS_N_AttributeListError;
					}
				}
				else
				{
					m_pModalityLogger->LogMsg("ERROR : Missing Referenced SOP sequence (0x0008, 0x1199) in client dataset.");
					m_CurrentRsp.msg.NActionRSP.DimseStatus = STATUS_N_MissingAttribute;
				}
			}
			else
			{
				m_pModalityLogger->LogMsg("ERROR : Missing Transaction UID (0x0008, 0x1195) in client dataset.");
				m_CurrentRsp.msg.NActionRSP.DimseStatus = STATUS_N_MissingAttribute;
			}
		}
		else
		{
			m_pModalityLogger->LogMsg("ERROR : Either pDataset or pReqData in N_ACTIONTransaction::ExtractStorageCommitmentRequestData was NULL.");
			m_CurrentRsp.msg.NActionRSP.DimseStatus = STATUS_N_ProcessingFailure;
		}
		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	DcmDataset *N_ACTIONTransaction::Create_N_EVENT_REPORT_RQ(StorageCommitmentActionReq *pReqData, int *pEventTypeId)
	{
		DcmDataset *pDataset               = NULL;
		SOPClassInstancePaire *pCurrentRef = NULL;
		DcmItem *pRefSequence              = NULL;
		DcmItem *pFailedSequence           = NULL;
		DcmItem *pCurrentSequence          = NULL;
		OFCondition cond                   = EC_Normal;

		(*pEventTypeId)                    = 2;

		pDataset  = new DcmDataset;
		if(pDataset)
		{
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_TransactionUID, pReqData->sTransactionUID);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_RetrieveAETitle, pReqData->sCalledAeTitle);

			if(m_pCfg->GetModalityDebug())
			{
				m_pModalityLogger->LogMsg("Object list validation :");
				m_pModalityLogger->LogMsg(" ");
			}

			 cond = pDataset->findOrCreateSequenceItem(DCM_ReferencedSOPSequence, pRefSequence, -2 /*-2 = append new*/);
			 if(cond.good())
			 {
				//	Check all the objects requested by the client.
				while(pReqData->m_aObjectCheckList.Pop(pCurrentRef))
				{
					if(ValidateObjectCommited(pCurrentRef->sSOPClassUID, pCurrentRef->sSOPInstanceUID))
					{
						(*pEventTypeId)  = 1;

						if(m_pCfg->GetModalityDebug())
						{
							m_pModalityLogger->LogMsg(" -- COMMITED --");
							m_pModalityLogger->LogMsg("  SOP Class    : %s", pCurrentRef->sSOPClassUID);
							m_pModalityLogger->LogMsg("  SOP Instance : %s", pCurrentRef->sSOPInstanceUID);
							m_pModalityLogger->LogMsg(" --------------");
							m_pModalityLogger->LogMsg(" ");
						}
						pCurrentSequence = pRefSequence;
					}
					else
					{
						if(m_pCfg->GetModalityDebug())
						{
							m_pModalityLogger->LogMsg(" -- NOT COMMITED --");
							m_pModalityLogger->LogMsg("  SOP Class    : %s", pCurrentRef->sSOPClassUID);
							m_pModalityLogger->LogMsg("  SOP Instance : %s", pCurrentRef->sSOPInstanceUID);
							m_pModalityLogger->LogMsg(" ------------------");
						}

						cond = pDataset->findOrCreateSequenceItem(DCM_FailedSOPSequence, pFailedSequence, -2 /*-2 = append new*/);
						if(cond.good())
						{
							pCurrentSequence = pFailedSequence;
						}
						else
						{
							m_pModalityLogger->LogMsg("ERROR : %s", cond.text());
							m_pModalityLogger->LogMsg("        Could not find or create failed sop sequence.");

							m_pMailer->AddLine("ERROR : %s", cond.text());
							m_pMailer->AddLine("        Could not find or create failed sop sequence.");
						}
					}

					if(pCurrentSequence)
					{
						pCurrentSequence->putAndInsertString(DCM_ReferencedSOPClassUID, pCurrentRef->sSOPClassUID);
						pCurrentSequence->putAndInsertString(DCM_ReferencedSOPInstanceUID, pCurrentRef->sSOPInstanceUID);
					}
					delete pCurrentRef;
					pCurrentRef = NULL;
				}
			 }
			 else
			 {
				m_pModalityLogger->LogMsg("ERROR : %s", cond.text());
				m_pModalityLogger->LogMsg("        Could not find or create referenced sop sequence.");

				m_pMailer->AddLine("ERROR : %s", cond.text());
				m_pMailer->AddLine("        Could not find or create referenced sop sequence.");
			 }
		}
		else
		{
			m_pModalityLogger->LogMsg("ERROR : Failed to allocate dataset in N_ACTIONTransaction::Create_N_EVENT_REPORT_RQ");
			m_pMailer->AddLine("ERROR : Failed to allocate dataset in N_ACTIONTransaction::Create_N_EVENT_REPORT_RQ");
		}
		return pDataset;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	bool N_ACTIONTransaction::ValidateObjectCommited(const char *pSOPClassUID, const char *pSOPInstanceUID)
	{
		bool bRetVal           = false;
		char sObjectFile[2049] = {0};

		if(m_DataService.IsObjectStoredInDb(pSOPClassUID, pSOPInstanceUID))
		{
			if(m_DataService.GetObjectFile(pSOPInstanceUID, sObjectFile))
			{
				if(DICOMUtil::CheckDICOMFile(sObjectFile, m_pModalityLogger,  m_pMailer))
				{
					bRetVal = true;
				}
			}
			else
			{
				m_pModalityLogger->LogMsg("ERROR : Object with InstanceUID storage location not found.", pSOPInstanceUID);
				m_pMailer->AddLine("ERROR : Object with InstanceUID storage location not found.", pSOPInstanceUID);
			}
		}
		else
		{
			m_pModalityLogger->LogMsg("ERROR : Object with InstanceUID %s not in database.", pSOPInstanceUID);
			m_pMailer->AddLine("ERROR : Object with InstanceUID %s not in database.", pSOPInstanceUID);
		}
		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	DcmDataset *N_ACTIONTransaction::CreateReferenceSOPClassDataset(Queue <Reference *> *pCommitedList,
												                    Queue <Reference *> *pFailedList,
												                    const char *pTransactionUID,
												                    const char *pCalledAETitle)
	{
		OFCondition cond     = EC_Normal;
		DcmItem *pItem       = NULL;
		Reference *pRef      = NULL;
		DBGTool dbg;
		DcmDataset *pDataset = new DcmDataset;

		if(m_pCfg->GetModalityDebug())
		{
			m_pModalityLogger->LogMsg("--N_ACTIONTransaction::CreateReferenceSOPClassDataset");
		}

		if(m_pCfg->GetProfiling())
			dbg.StartProfiling(m_pServerLogger, "Creating reference SOP class dataset.");

		if(pDataset)
		{
			pDataset->putAndInsertOFStringArray(DCM_TransactionUID, pTransactionUID);
			pDataset->putAndInsertOFStringArray(DCM_RetrieveAETitle, pCalledAETitle);

			while(pCommitedList->Pop(pRef))
			{
				//	Create a new item (and the sequence if required).
				cond = pDataset->findOrCreateSequenceItem(DCM_ReferencedSOPSequence, pItem, -2 /*-2 = append new*/);
				if(cond.good())
				{
					//	Store the instance level attributes.
					pItem->putAndInsertOFStringArray(DCM_ReferencedSOPClassUID, pRef->SOPClassUID);
					pItem->putAndInsertOFStringArray(DCM_ReferencedSOPInstanceUID, pRef->SOPInstanceUID);
				}
				delete pRef;
				pRef = NULL;
			}

			while(pFailedList->Pop(pRef))
			{
				//	Create a new item (and the sequence if required).
				cond = pDataset->findOrCreateSequenceItem(DCM_FailedSOPSequence, pItem, -2 /*-2 = append new*/);
				if(cond.good())
				{
					//	Store the instance level attributes.
					pItem->putAndInsertOFStringArray(DCM_ReferencedSOPClassUID, pRef->SOPClassUID);
					pItem->putAndInsertOFStringArray(DCM_ReferencedSOPInstanceUID, pRef->SOPInstanceUID);

					/////////////////////////////////////////////////////////////
					//	No such object instance. See page 929 (C.22.1.4) of
					//	DICOM part 3 for a list of possible codes.
					pItem->putAndInsertUint16(DCM_FailureReason, 0x0112);
				}
				delete pRef;
				pRef = NULL;
			}
		}

		if(m_pCfg->GetProfiling())
			dbg.EndProfiling(m_pServerLogger, NULL, "Reference SOP clas dataset created.");

		return pDataset;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	bool N_ACTIONTransaction::ProcessUpdateGPSPS(DcmDataset *pDataset)
	{
		bool bRetVal = false;

		/////////////////////////////////////////////////////////////////////////
		//	dcmtk variables.
		OFCondition cond           = EC_Normal;
		DcmDataset *pRspDataset    = NULL;
		DcmDataset *pStatus        = NULL;
		char sDateTime[33]         = {0};
		StringBuffer dump;
		DBGTool dbg;

		UpdateSPSData UpdateReqData;

		m_CurrentRsp.CommandField                             = DIMSE_N_ACTION_RSP;
		m_CurrentRsp.msg.NActionRSP.ActionTypeID              = m_CurrentReq.msg.NActionRQ.ActionTypeID;
		m_CurrentRsp.msg.NActionRSP.DataSetType               = DIMSE_DATASET_NULL;
		m_CurrentRsp.msg.NActionRSP.DimseStatus               = STATUS_Success;		//	Assuming.
		m_CurrentRsp.msg.NActionRSP.MessageIDBeingRespondedTo = m_CurrentReq.msg.NActionRQ.MessageID;
		m_CurrentRsp.msg.NActionRSP.opts                      = 0;

		strcpy(m_CurrentRsp.msg.NActionRSP.AffectedSOPClassUID, m_CurrentReq.msg.NActionRQ.RequestedSOPClassUID);
		strcpy(m_CurrentRsp.msg.NActionRSP.AffectedSOPInstanceUID, m_CurrentReq.msg.NActionRQ.RequestedSOPInstanceUID);

		/////////////////////////////////////////////////////////////////////////
		//	Extract request parameters.
		if(ExtractGP_SPS_Update(pDataset, &UpdateReqData))
		{
			//	Dump the dataset if debug mode is
			//	on for that AE title.
			if(m_pCfg->GetModalityDebug())
			{
				Time::GetDate(sDateTime);		//	Get the date for the sub-directory.
				dump.Format("%s/%s_UpdateGPSPS_%d/", (char *)m_sCurrentDebugDir, sDateTime, m_iTransactionId);

				os::MkDir((char *)dump);

				Time::GetTimeOfDay(sDateTime);	//	Get the time for the file.
				dump += "Query_";
				dump += sDateTime;
				dump += ".dcm";

				cond = pDataset->saveFile((char *)dump, EXS_LittleEndianExplicit);
				if(cond.bad())
				{
					m_pModalityLogger->LogMsg("ERROR %s", cond.text());
					m_pModalityLogger->LogMsg("      Failed to save debug dataset.");
				}
			}
			if(m_DataService.SPSExists(m_CurrentReq.msg.NActionRQ.RequestedSOPInstanceUID))
			{
				if(m_DataService.SPSIsAvailable(m_CurrentReq.msg.NActionRQ.RequestedSOPInstanceUID))
				{
					if(m_DataService.UpdateSPS(&UpdateReqData, m_CurrentReq.msg.NActionRQ.RequestedSOPInstanceUID))
					{
						bRetVal = true;
					}
					else
					{
						m_CurrentRsp.msg.NActionRSP.DimseStatus = STATUS_N_ProcessingFailure;
					}
				}
				else
				{
					m_CurrentRsp.msg.NActionRSP.DimseStatus = STATUS_N_DuplicateSOPInstance;
				}
			}
			else
			{
				m_CurrentRsp.msg.NActionRSP.DimseStatus = STATUS_N_InvalidObjectInstance;
			}
		}
		else
		{
			m_pModalityLogger->LogMsg("ERROR : Failed to extract dataset from SPS update.");
			m_pMailer->AddLine("ERROR : Failed to extract dataset from SPS update.");

			m_CurrentRsp.msg.NActionRSP.DimseStatus = STATUS_N_ProcessingFailure;
		}
		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	bool N_ACTIONTransaction::ExtractGP_SPS_Update(DcmDataset *pDataset, UpdateSPSData *pGLSPSData)
	{
		bool bRetVal                   = true;
		OFCondition cond               = EC_Normal;
		const char *pValue             = NULL;
		DcmElement *pHumanPerformerSeq = NULL;
		DcmElement *pPerformerCodeSeq  = NULL;   
		DcmItem *pPerformerItem        = NULL;
		DcmItem *pPerformerCodeItem    = NULL;
		DBGTool dbg;

		if(m_pCfg->GetProfiling())
			dbg.StartProfiling(m_pServerLogger, "Extracting dataset.");

		DICOMUtil::GetTagFromDataset(pDataset, DCM_TransactionUID, pGLSPSData->sTransactionUID);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_RETIRED_GeneralPurposeScheduledProcedureStepStatus, pGLSPSData->sSPSStatus);

		cond = pDataset->findAndGetElement(DCM_ActualHumanPerformersSequence, pHumanPerformerSeq);
		if(cond.good() && pHumanPerformerSeq)
		{
			pPerformerItem = ((DcmSequenceOfItems*)pHumanPerformerSeq)->getItem(0);
			if(pPerformerItem)
			{
				cond = pPerformerItem->findAndGetElement(DCM_HumanPerformerCodeSequence, pPerformerCodeSeq);
				if(cond.good() && pPerformerCodeSeq)
				{
					pPerformerCodeItem = ((DcmSequenceOfItems*)pPerformerCodeSeq)->getItem(0);
					if(pPerformerCodeItem)
					{
						DICOMUtil::GetTagFromDataset(pPerformerCodeItem, DCM_CodeValue, pGLSPSData->sHumanPerformerCode);
						DICOMUtil::GetTagFromDataset(pPerformerCodeItem, DCM_CodingSchemeDesignator, pGLSPSData->sHumanPerformerCodeScheme);
						DICOMUtil::GetTagFromDataset(pPerformerCodeItem, DCM_CodeMeaning, pGLSPSData->sHumanPerformerCodeMeaning);
					}
				}
				DICOMUtil::GetTagFromDataset(pPerformerItem, DCM_HumanPerformerOrganization, pGLSPSData->sPerformerOrganization);
				DICOMUtil::GetTagFromDataset(pPerformerItem, DCM_HumanPerformerName, pGLSPSData->sPerformerName);
			}
			else
			{
				bRetVal = false;
			}
		}
		else
		{
			bRetVal = false;
		}

		if(m_pCfg->GetProfiling())
			dbg.EndProfiling(m_pServerLogger, NULL, "Dataset extracted.");

		return bRetVal;
	}
}
