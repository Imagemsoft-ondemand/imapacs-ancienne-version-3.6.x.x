/////////////////////////////////////////////////////////////////////////////
//	C_FINDTransaction.h
/////////////////////////////////////////////////////////////////////////////

#include "ITransaction.h"
#include "../Utils/log.h"
#include "../Utils/Mailer.h"
#include "../Utils/DICOMUtil.h"
#include "../PacsConfig.h"
#include "../DatabaseServices/CFindService.h"

#if !defined(__C_FINDTRANSACTION_H_INCLUDED__)
#define      __C_FINDTRANSACTION_H_INCLUDED__

/////////////////////////////////////////////////////////////////////////////
//
namespace imapacs
{
	class C_FINDTransaction : public ITransaction
	{
	public:
		C_FINDTransaction():m_pAssociation(NULL),
			                      m_CurrentDataPresID(0x00),
								  m_CurrentPresID(0x00),
								  m_pServerLogger(NULL),
								  m_pModalityLogger(NULL),
								  m_pMailer(NULL),
								  m_pCfg(NULL),
								  m_iTransactionId(0)
		{}
		virtual ~C_FINDTransaction()
		{}

		bool Init(T_ASC_Association *pAssociation,
			      T_DIMSE_Message *pMsg,
				  T_ASC_PresentationContextID PresID,
			      PacsConfig *pCfg);

		bool ProcessTransaction();
		void SetTransactionType(const char *transactionType){strcpy(m_transactionType,transactionType);}
		char *GetTransactionType(){return m_transactionType;}
		void Shutdown();

	protected:

		/////////////////////////////////////////////////////////////////////
		//	C-FIND query methods.
		bool ProcessFindQuery(DcmDataset *pRequest, int iRlevel);
		bool ExtractFindData(DcmDataset *pDataset, FindData *pFindData);
		DcmDataset *CreateFindResultDataset(FindData *pResponceData, 
			                                DcmDataset *pRequestDataset, 
									        int iQLevel);

		/////////////////////////////////////////////////////////////////////
		//	C-FIND modality worklist methods.
		bool ProcessWorklist(DcmDataset *pRequest);
		bool ExtractWorklistQry(DcmDataset *pDataset, WLData *pInfo);
		DcmDataset *CreateFindWorklistResultDataset(WLData *pResult);

		/////////////////////////////////////////////////////////////////////
		//	C-FIND general purpose worklist methods.
		bool ProcessGPWorklist(DcmDataset *pRequestDataset);
		bool ExtractGPWorklistQry(DcmDataset *pDataset, GPWLData *pGPInfo);
		DcmDataset *CreateGPWorklistDataset(DcmDataset *pReqDataset, GPWLData *pGPInfo);

		T_ASC_Association          *m_pAssociation;
		T_DIMSE_Message             m_CurrentReq;
		T_DIMSE_Message             m_CurrentRsp;
		T_ASC_PresentationContextID m_CurrentDataPresID;
		T_ASC_PresentationContextID m_CurrentPresID;
		Log                        *m_pServerLogger;
		Log                        *m_pModalityLogger;
		Mailer                     *m_pMailer;
		PacsConfig                 *m_pCfg;

		CFindService                m_DataService;

		char						m_transactionType[64];

		//	For debug purpose.
		StringBuffer                m_sCurrentDebugDir;
		int                         m_iTransactionId;
	};
}

#endif	//	__C_FINDSEARCHTRANSACTION_H_INCLUDED__
