/////////////////////////////////////////////////////////////////////////////
//	C_FINDTransaction.cpp
/////////////////////////////////////////////////////////////////////////////

#include "../Utils/PatchModule.h"
#include "../Utils/TimeUtil.h"
#include "../Utils/Str.h"
#include "../Utils/DBGTool.h"
#include "C_FINDTransaction.h"

/////////////////////////////////////////////////////////////////////////////
//
namespace imapacs
{
	/////////////////////////////////////////////////////////////////////////
	//
	bool C_FINDTransaction::Init(T_ASC_Association *pAssociation, 
		                               T_DIMSE_Message *pMsg, 
									   T_ASC_PresentationContextID PresID, 
									   PacsConfig *pCfg)
	{
		bool bRetVal = false;

		memcpy(&m_CurrentReq, pMsg, sizeof(T_DIMSE_Message));
		memset(&m_CurrentRsp, 0, sizeof(T_DIMSE_Message));

		m_CurrentRsp.CommandField                           = DIMSE_C_GET_RSP;
		m_CurrentRsp.msg.CFindRSP.DataSetType               = DIMSE_DATASET_NULL;	//	Assuming
		m_CurrentRsp.msg.CFindRSP.DimseStatus               = STATUS_Success;		//	Assuming.
		m_CurrentRsp.msg.CFindRSP.MessageIDBeingRespondedTo = m_CurrentReq.msg.CFindRQ.MessageID;

		strcpy(m_CurrentRsp.msg.CFindRSP.AffectedSOPClassUID, m_CurrentReq.msg.CFindRQ.AffectedSOPClassUID);

		m_pAssociation  = pAssociation;
		m_CurrentPresID = PresID;

		m_pCfg = pCfg;

		//	For convenience.
		m_pServerLogger   = pCfg->GetServerLogger();
		m_pModalityLogger = pCfg->GetModalityLogger();
		m_pMailer         = pCfg->GetMailer();

		m_pServerLogger->LogMsg("[%s] - C-FIND request.", pAssociation->params->DULparams.callingAPTitle);
		m_pModalityLogger->LogMsg("C-FIND request.");
		m_pMailer->AddLine("C-FIND request.");

		if(pCfg->GetModalityDebug())
		{
			//	Generate a transaction id to group
			//	transaction related file names.
			srand((unsigned int)time(NULL));
			m_iTransactionId = rand();

			m_sCurrentDebugDir.Format("%s%s", DEBUG_DIRECTORY, pAssociation->params->DULparams.callingAPTitle);
			os::MkDir((char *)m_sCurrentDebugDir);

			m_DataService.SetCurrentDebugParams((char *)m_sCurrentDebugDir, m_iTransactionId);
		}
		m_DataService.SetConfig(pCfg);
		bRetVal = true;

		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	bool C_FINDTransaction::ProcessTransaction()
	{
		OFCondition cond     = EC_Normal;
		bool bRetVal         = true;
		DcmDataset *pStatus  = NULL;
		DcmDataset *pRequest = NULL;
		char sDateTime[33]   = {0};
		StringBuffer dump;

		/////////////////////////////////////////////////////////////////////
		//	Read request dataset.
		cond = DIMSE_receiveDataSetInMemory(m_pAssociation,
											DIMSE_BLOCKING,
											0,
											&m_CurrentPresID,
											&pRequest,
											NULL,
											NULL);
		if(cond.good())
		{
			//	Dump the dataset if debug mode is
			//	on for that AE title.
			if(m_pCfg->GetModalityDebug())
			{
				Time::GetDate(sDateTime);		//	Get the date for the sub-directory.
				dump.Format("%s/%s_C_Find_%d/", (char *)m_sCurrentDebugDir, sDateTime, m_iTransactionId);

				os::MkDir((char *)dump);

				Time::GetTimeOfDay(sDateTime);	//	Get the time for the file.
				dump += "Query_";
				dump += sDateTime;
				dump += ".dcm";

				cond = pRequest->saveFile((char *)dump, EXS_LittleEndianExplicit);
				if(cond.bad())
				{
					m_pModalityLogger->LogMsg("ERROR %s", cond.text());
					m_pModalityLogger->LogMsg("      Failed to save debug dataset.");
				}
			}

			/////////////////////////////////////////////////////////////////////
			//	What kind of C-FIND are we dealing with ?
			if(strcmp(m_CurrentReq.msg.CFindRQ.AffectedSOPClassUID, UID_FINDPatientRootQueryRetrieveInformationModel) == 0)
			{
				m_pModalityLogger->LogMsg("[%s] - C-FIND Patient Root.", m_pAssociation->params->DULparams.callingAPTitle);
				m_pModalityLogger->LogMsg("C-FIND Patient Root.");
				m_pMailer->AddLine("C-FIND Patient Root.");

				m_pCfg->InsertEvent("C-FIND (PATIENT)", 
									NULL, 
									"IMAPACS",
									m_pAssociation->params->DULparams.callingAPTitle, 
									NULL,
									NULL,
									NULL,
									NULL, 
									NULL, 
									NULL, 
									NULL, 
									NULL, 
									NULL, 
									NULL);

				bRetVal = ProcessFindQuery(pRequest, PATIENT_ROOT);
			}
			else if(strcmp(m_CurrentReq.msg.CFindRQ.AffectedSOPClassUID, UID_FINDStudyRootQueryRetrieveInformationModel) == 0)
			{
				m_pModalityLogger->LogMsg("[%s] - C-FIND Study Root.", m_pAssociation->params->DULparams.callingAPTitle);
				m_pModalityLogger->LogMsg("C-FIND Study Root.");
				m_pMailer->AddLine("C-FIND Study Root.");

				m_pCfg->InsertEvent("C-FIND (STUDY)", 
									NULL,
									"IMAPACS",
									m_pAssociation->params->DULparams.callingAPTitle,
									NULL,
									NULL,
									NULL,
									NULL,
									NULL,
									NULL,
									NULL,
									NULL,
									NULL,
									NULL);

				bRetVal = ProcessFindQuery(pRequest, STUDY_ROOT);
			}
			else if(strcmp(m_CurrentReq.msg.CFindRQ.AffectedSOPClassUID, UID_RETIRED_FINDPatientStudyOnlyQueryRetrieveInformationModel) == 0)
			{
				m_pModalityLogger->LogMsg("[%s] - C-FIND Patient-Study Root.", m_pAssociation->params->DULparams.callingAPTitle);
				m_pModalityLogger->LogMsg("C-FIND Patient-Study Root.");
				m_pMailer->AddLine("C-FIND Patient-Study Root.");

				m_pCfg->InsertEvent("C-FIND (PATIENT_STUDY)", 
									NULL,
									"IMAPACS",
									m_pAssociation->params->DULparams.callingAPTitle,
									NULL,
									NULL,
									NULL,
									NULL,
									NULL,
									NULL,
									NULL,
									NULL,
									NULL,
									NULL);

				bRetVal = ProcessFindQuery(pRequest, PATIENT_STUDY_ROOT);
			}
			else if(strcmp(m_CurrentReq.msg.CFindRQ.AffectedSOPClassUID, UID_FINDModalityWorklistInformationModel) == 0)
			{
				m_pServerLogger->LogMsg("[%s] - C-FIND Worklist.", m_pAssociation->params->DULparams.callingAPTitle);
				m_pModalityLogger->LogMsg("C-FIND Worklist.");

				m_pMailer->AddLine("C-FIND Worklist.");

				m_pCfg->InsertEvent("C-FIND WORKLIST", 
									NULL, 
									"IMAPACS",
									m_pAssociation->params->DULparams.callingAPTitle, 
									NULL, 
									NULL, 
									NULL, 
									NULL, 
									NULL, 
									NULL, 
									NULL, 
									NULL, 
									NULL, 
									NULL);

				bRetVal = ProcessWorklist(pRequest);
			}
			else if(strcmp(m_CurrentReq.msg.CFindRQ.AffectedSOPClassUID, UID_RETIRED_FINDGeneralPurposeWorklistInformationModel) == 0)
			{
				m_pServerLogger->LogMsg("[%s] - C-FIND General purpose worklist.", m_pAssociation->params->DULparams.callingAPTitle);
				m_pModalityLogger->LogMsg("C-FIND General purpose worklist.");

				m_pMailer->AddLine("C-FIND General purpose worklist.");

				m_pCfg->InsertEvent("C-FIND GPWL", 
									NULL, 
									"IMAPACS",
									m_pAssociation->params->DULparams.callingAPTitle, 
									NULL, 
									NULL, 
									NULL, 
									NULL, 
									NULL, 
									NULL, 
									NULL, 
									NULL, 
									NULL, 
									NULL);

				bRetVal = ProcessGPWorklist(pRequest);
			}
			else
			{
				m_CurrentRsp.CommandField                           = DIMSE_C_FIND_RSP;
				m_CurrentRsp.msg.CFindRSP.MessageIDBeingRespondedTo = m_CurrentReq.msg.CFindRQ.MessageID;
				m_CurrentRsp.msg.CFindRSP.DimseStatus               = STATUS_FIND_Refused_SOPClassNotSupported;
				m_CurrentRsp.msg.CFindRSP.DataSetType               = DIMSE_DATASET_NULL;

				strcpy(m_CurrentRsp.msg.CFindRSP.AffectedSOPClassUID, m_CurrentReq.msg.CFindRQ.AffectedSOPClassUID);

				m_pServerLogger->LogMsg("[%s] - Unsupported or unknown Root query uid : %s", 
					                    m_pAssociation->params->DULparams.callingAPTitle, 
										m_CurrentReq.msg.CFindRQ.AffectedSOPClassUID);

				m_pModalityLogger->LogMsg("Unsupported or unknown Root query uid : %s", m_CurrentReq.msg.CFindRQ.AffectedSOPClassUID);
				m_pMailer->AddLine("Unsupported or unknown Root query uid : [%s]", m_CurrentReq.msg.CFindRQ.AffectedSOPClassUID);

				cond = DIMSE_sendMessageUsingMemoryData(m_pAssociation, m_CurrentPresID, &m_CurrentRsp, pStatus, NULL, NULL, NULL);
				if(cond.bad())
				{
					m_pModalityLogger->LogMsg("ERROR : %s", cond.text());
					m_pModalityLogger->LogMsg("        Could not send C-FIND responce.");

					m_pMailer->AddLine("%s", cond.text());
					m_pMailer->AddLine("Could not send C-FIND responce.");
				}
			}
		}
		else
		{
			m_pModalityLogger->LogMsg("ERROR : %s", cond.text());
			m_pModalityLogger->LogMsg("Could not receive C-FIND dataset.");

			m_pMailer->AddLine("ERROR : %s", cond.text());
			m_pMailer->AddLine("Could not receive C-FIND dataset.");
		}
		DICOMUtil::ClearDataset(&pRequest);
		DICOMUtil::ClearDataset(&pStatus);

		//return true;  // [Ref:0001032]
		return bRetVal; // [Ref:0001032]
	}

	/////////////////////////////////////////////////////////////////////////
	//
	void C_FINDTransaction::Shutdown()
	{
		m_DataService.Dispose();
		m_sCurrentDebugDir.Clear();
	}

	/////////////////////////////////////////////////////////////////////////
	//	Process a C-FIND query based on the root level.
	bool C_FINDTransaction::ProcessFindQuery(DcmDataset *pRequest, int iRlevel)
	{
		bool bRetVal          = true;
		bool bCancelled       = false;
		int iResultCount      = 0;
		unsigned int iTime    = 0;
		char sNumber[17]      = {0};
		QUERY_STATUS QueryRet = QS_OK_NO_RESULT;
		char sDateTime[33]    = {0};
		StringBuffer dump;
		DBGTool dbg;

		/////////////////////////////////////////////////////////////////////////
		//	dcmtk variables.
		OFCondition cond      = EC_Normal;
		DcmDataset *pResult   = NULL;
		DcmDataset *pStatus   = NULL;

		if(m_pCfg->GetModalityDebug())
		{
			m_pModalityLogger->LogMsg("--ProcessFindQuery()");
		}

		/////////////////////////////////////////////////////////////////////////
		//	Query variables.
		FindData FindRequestData;

		m_CurrentRsp.CommandField                           = DIMSE_C_FIND_RSP;
		m_CurrentRsp.msg.CFindRSP.MessageIDBeingRespondedTo = m_CurrentReq.msg.CFindRQ.MessageID;
		m_CurrentRsp.msg.CFindRSP.DimseStatus               = STATUS_Pending;
		m_CurrentRsp.msg.CFindRSP.opts						= O_FIND_AFFECTEDSOPCLASSUID;

		strcpy(m_CurrentRsp.msg.CFindRSP.AffectedSOPClassUID, m_CurrentReq.msg.CFindRQ.AffectedSOPClassUID);

		if(ExtractFindData(pRequest, &FindRequestData))
		{
			QueryRet = m_DataService.StartFind(&FindRequestData, iRlevel);
			if(QueryRet == QS_OK_RESULT)
			{
				/////////////////////////////////////////////////////////////////
				//	We have results, send them.
				do
				{
					++iResultCount;

					/////////////////////////////////////////////////////////////
					//	Did we receive a cancel request ?
					cond = DIMSE_checkForCancelRQ(m_pAssociation, m_CurrentPresID, m_CurrentReq.msg.CMoveRQ.MessageID);
					if(cond == EC_Normal)
					{
						bCancelled = true;

						m_CurrentReq.msg.CFindRSP.DimseStatus = STATUS_FIND_Cancel_MatchingTerminatedDueToCancelRequest;
						m_CurrentReq.msg.CFindRSP.DataSetType = DIMSE_DATASET_NULL;

						m_pServerLogger->LogMsg("[%s] - Received cancel request.", m_pAssociation->params->DULparams.callingAPTitle);
						m_pModalityLogger->LogMsg("Received cancel request.");
						m_pMailer->AddLine("Received cancel request.");

						break;
					}
					else if(cond == DIMSE_NODATAAVAILABLE)
					{
						//	Normal timeout, just continue
						//	since nothing has been received.
					}
					else
					{
						m_pModalityLogger->LogMsg("ERROR : %s", cond.text());
						m_pMailer->AddLine("%s", cond.text());

						m_CurrentRsp.msg.CFindRSP.DimseStatus = STATUS_FIND_Failed_UnableToProcess;
						m_CurrentRsp.msg.CFindRSP.DataSetType = DIMSE_DATASET_NULL;

						break;
					}

					/////////////////////////////////////////////////////////////
					//	Create responce dataset.
					pResult = CreateFindResultDataset(&FindRequestData, pRequest, iRlevel);
					if(pResult)
					{
						//	Dump the responce dataset if debug
						//	mode is on for that AE title.
						if(m_pCfg->GetModalityDebug())
						{
							Time::GetDate(sDateTime);		//	Get the date for the sub-directory.
							dump.Format("%s/%s_C_Find_%d/", (char *)m_sCurrentDebugDir, sDateTime, m_iTransactionId);

							os::MkDir((char *)dump);

							Time::GetTimeOfDay(sDateTime);	//	Get the time for the file.
							dump += "Responce_";
							dump += sDateTime;
							sprintf(sNumber, "%d", iResultCount);
							dump += "_";
							dump += sNumber;
							dump += ".dcm";

							cond = pResult->saveFile((char *)dump, EXS_LittleEndianExplicit);
							if(cond.bad())
							{
								m_pModalityLogger->LogMsg("ERROR %s", cond.text());
								m_pModalityLogger->LogMsg("      Failed to save debug dataset.");
							}
						}

						//	There will be another result after this one.
						m_CurrentRsp.msg.CFindRSP.DimseStatus = STATUS_Pending;
						m_CurrentRsp.msg.CFindRSP.DataSetType = DIMSE_DATASET_PRESENT;

						if(m_pCfg->GetProfiling())
							dbg.StartProfiling(m_pServerLogger, "Sending responce.");

						/////////////////////////////////////////////////////////
						//	Send responce result.
						cond = DIMSE_sendMessageUsingMemoryData(m_pAssociation, m_CurrentPresID, &m_CurrentRsp, pStatus, pResult, NULL, NULL, NULL);
						if(cond.bad())
						{
							m_pModalityLogger->LogMsg("ERROR : %s", cond.text());
							m_pModalityLogger->LogMsg("        Unable to send C-FIND responce.");

							m_pMailer->AddLine("%s", cond.text());
							m_pMailer->AddLine("Unable to send C-FIND responce.");

							DICOMUtil::ClearDataset(&pResult);
							DICOMUtil::ClearDataset(&pStatus);

							m_CurrentRsp.msg.CFindRSP.DimseStatus = STATUS_FIND_Failed_UnableToProcess;
							m_CurrentRsp.msg.CFindRSP.DataSetType = DIMSE_DATASET_NULL;

							bRetVal = false;

							DICOMUtil::ClearDataset(&pResult);
							DICOMUtil::ClearDataset(&pStatus);

							break;
						}
						DICOMUtil::ClearDataset(&pResult);
						DICOMUtil::ClearDataset(&pStatus);

						if(m_pCfg->GetProfiling())
							dbg.EndProfiling(m_pServerLogger, NULL, "Responce sent.");
					}
					else
					{
						m_CurrentRsp.msg.CFindRSP.DimseStatus = STATUS_MOVE_Refused_OutOfResourcesSubOperations;
						m_CurrentRsp.msg.CFindRSP.DataSetType = DIMSE_DATASET_NULL;

						m_pModalityLogger->LogMsg("ERROR : Unable to allocate responce dataset.");
						m_pMailer->AddLine("Unable to allocate responce dataset.");

						bRetVal = false;

						break;
					}
				}while(m_DataService.NextFindResult());

				if(!bCancelled)
				{
					if(bRetVal)
					{
						m_CurrentRsp.msg.CFindRSP.DimseStatus = STATUS_Success;
						m_CurrentRsp.msg.CFindRSP.DataSetType = DIMSE_DATASET_NULL;
					}
					else
					{
						m_CurrentRsp.msg.CFindRSP.DimseStatus = STATUS_FIND_Failed_UnableToProcess;
						m_CurrentRsp.msg.CFindRSP.DataSetType = DIMSE_DATASET_NULL;
					}
				}
			}
			else if(QueryRet == QS_ERROR)
			{
				/////////////////////////////////////////////////////////////////
				//	A database related error occured.
				m_CurrentRsp.msg.CFindRSP.DimseStatus = STATUS_FIND_Failed_UnableToProcess;
				m_CurrentRsp.msg.CFindRSP.DataSetType = DIMSE_DATASET_NULL;

				m_pModalityLogger->LogMsg("        Unable to find result for the query.");
				m_pMailer->AddLine("Unable to find result for the query.");

				bRetVal  = false;
			}
			else
			{
				/////////////////////////////////////////////////////////////////
				//	The request to the database was successful but there was no
				//	result.
				m_CurrentRsp.msg.CFindRSP.DimseStatus = STATUS_Success;
				m_CurrentRsp.msg.CFindRSP.DataSetType = DIMSE_DATASET_NULL;

				m_pModalityLogger->LogMsg("No result was found.");
				m_pMailer->AddLine("No result was found.");
			}
		}
		else
		{
			m_CurrentRsp.msg.CFindRSP.DimseStatus = STATUS_FIND_Failed_UnableToProcess;
			m_CurrentRsp.msg.CFindRSP.DataSetType = DIMSE_DATASET_NULL;

			m_pModalityLogger->LogMsg("Unable to extract data from find request.");
			m_pMailer->AddLine("Unable to extract data from find request.");

			bRetVal  = false;
		}

		if(m_pCfg->GetProfiling())
			dbg.StartProfiling(m_pServerLogger, "Sending responce.");

		/////////////////////////////////////////////////////////////////////////
		//	Send last responce.
		cond = DIMSE_sendMessageUsingMemoryData(m_pAssociation, m_CurrentPresID, &m_CurrentRsp, pStatus, NULL, NULL, NULL);
		if(cond.bad())
		{
			m_pModalityLogger->LogMsg("ERROR : %s", cond.text());
			m_pModalityLogger->LogMsg("        Could not send responce.");

			m_pMailer->AddLine("%s", cond.text());
			m_pMailer->AddLine("Could not send responce.");

			bRetVal = false;
		}

		if(m_pCfg->GetProfiling())
			dbg.EndProfiling(m_pServerLogger, NULL, "Responce sent.");

		if(bCancelled)
		{
			m_pServerLogger->LogMsg("[%s] - C-FIND cancelled.", m_pAssociation->params->DULparams.callingAPTitle);
			m_pModalityLogger->LogMsg("C-FIND cancelled.");
			m_pMailer->AddLine("C-FIND cancelled.");
		}
		else
		{
			m_pModalityLogger->LogMsg("Number of result found : %d", iResultCount);
			m_pMailer->AddLine("Number of result found : %d", iResultCount);
		}

		DICOMUtil::ClearDataset(&pResult);
		DICOMUtil::ClearDataset(&pStatus);

		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	bool C_FINDTransaction::ExtractFindData(DcmDataset *pDataset, FindData *pFindData)
	{
		bool bRetVal       = false;
		OFCondition cond   = EC_Normal;
		unsigned int iTime = 0;
		DBGTool dbg;
		DcmElement *pElement = NULL;
		DcmItem *pItem		= NULL;

		if(m_pCfg->GetProfiling())
			dbg.StartProfiling(m_pServerLogger, "Extracting C-Find request dataset.");

		DICOMUtil::GetTagFromDataset(pDataset, DCM_QueryRetrieveLevel, pFindData->sQLevel);

		/////////////////////////////////////////////////////////////////////////
		//	Extract patient infos.
		DICOMUtil::GetTagFromDataset(pDataset, DCM_PatientID, &pFindData->PatId);

		DICOMUtil::GetTagFromDataset(pDataset, DCM_PatientName, &pFindData->PatName);

		Str::SplitNamesUsingToken(pFindData->PatName.Data,
			                      pFindData->PatFirstName.Data, 
								  pFindData->PatLastName.Data, '^');

		DICOMUtil::GetTagFromDataset(pDataset, DCM_PatientBirthDate, &pFindData->PatBirthDate);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_PatientBirthTime, &pFindData->PatBirthTime);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_OtherPatientIDs, &pFindData->PatOtherId);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_PatientSex, &pFindData->PatSex);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_OtherPatientNames, &pFindData->PatOtherName);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_PatientAge, &pFindData->PatAge);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_PatientSize, &pFindData->PatSize);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_PatientWeight, &pFindData->PatWeight);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_PatientMotherBirthName, &pFindData->PatMotherName);

		/////////////////////////////////////////////////////////////////////////
		//	Extract study infos.
		DICOMUtil::GetTagFromDataset(pDataset, DCM_StudyInstanceUID, &pFindData->StudyInstanceUID);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_StudyDate, &pFindData->StudyDate);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_StudyTime, &pFindData->StudyTime);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_StudyDescription, &pFindData->StudyDesc);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_StudyID, &pFindData->StudyId);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_AccessionNumber, &pFindData->StudyAccessionNumber);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_AdmittingDiagnosesDescription, &pFindData->StudyAdmDiag);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_ReferringPhysicianName, &pFindData->StudyRefPhysicianName);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_NameOfPhysiciansReadingStudy, &pFindData->StudyReadPhysicianName);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_ModalitiesInStudy, &pFindData->StudyModalitiesInStudy);

		// [Ref:0000722] - D�but
		/////////////////////////////////////////////////////////////////////////
		// On extract le code d'examen
		cond = pDataset->findAndGetElement(DCM_ProcedureCodeSequence, pElement);
		if(cond.good() && pElement)
		{
			pItem = ((DcmSequenceOfItems*)pElement)->getItem(0);
			if(pItem)
				DICOMUtil::GetTagFromDataset(pItem, DCM_CodeValue, &pFindData->StudyOtherNumber); 
		}
		// [Ref:0000722] - Fin


		/////////////////////////////////////////////////////////////////////////
		//	Extract series infos.
		DICOMUtil::GetTagFromDataset(pDataset, DCM_SeriesInstanceUID, &pFindData->SeriesInstanceUID);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_SeriesDate, &pFindData->SeriesDate);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_SeriesTime, &pFindData->SeriesTime);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_SeriesDescription, &pFindData->SeriesDesc);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_Laterality, &pFindData->SeriesLaterality);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_Modality, &pFindData->SeriesModality);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_SeriesNumber, &pFindData->SeriesNumber);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_ViewPosition, &pFindData->SeriesViewPosition);

		/////////////////////////////////////////////////////////////////////////
		//	Extract object infos.
		DICOMUtil::GetTagFromDataset(pDataset, DCM_SOPClassUID, &pFindData->ObjectSopClasUID);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_SOPInstanceUID, &pFindData->ObjectInstanceUID);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_InstanceNumber, &pFindData->ObjectInstanceNumber);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_AcquisitionDate, &pFindData->ObjectAcquisitionDate);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_AcquisitionTime, &pFindData->ObjectAcquisitionTime);

		//pDataset->saveFile("/test.dcm", EXS_LittleEndianExplicit);

		if(pFindData->sQLevel[0])
		{
			bRetVal = true;
		}
		else
		{
			m_pModalityLogger->LogMsg("Unable to get query level from request dataset in C_FINDTransaction::ExtractFindData");
			m_pMailer->AddLine("Unable to get query level from request dataset in C_FINDTransaction::ExtractFindData");
		}

		if(m_pCfg->GetProfiling())
			dbg.EndProfiling(m_pServerLogger, NULL, "C-Find request extracted.");

		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	DcmDataset *C_FINDTransaction::CreateFindResultDataset(FindData *pResponceData, 
		                                                   DcmDataset *pRequestDataset, 
								                           int iQLevel)
	{
		DcmDataset *pDataset		= NULL;
		DcmItem *pProcCodeSeq		= NULL;
		/*DcmSequenceOfItems *pProcCodeSeq		= NULL;
		DcmItem *pItem				= NULL;*/
		char sCharacterSet[65]		= {0};
		OFCondition cond			= EC_Normal;
		unsigned int iTime			= 0;
		DBGTool dbg;

		if(m_pCfg->GetProfiling())
			dbg.StartProfiling(m_pServerLogger, "Creating responce dataset.");

		/////////////////////////////////////////////////////////////////////
		//	NOTE : This typecast is safe. The overridden method in DcmDataset
		//         creates a copy wich is a DcmDataset but it is cast as a
		//         DcmObject to have the same prototype as the base class.
		/////////////////////////////////////////////////////////////////////
		pDataset = (DcmDataset *)pRequestDataset->clone();
		if(pDataset)
		{
			/////////////////////////////////////////////////////////////////
			//	Update patient infos.
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_PatientID, &pResponceData->PatId, m_pCfg->MustRemoveAccents());
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_PatientName, &pResponceData->PatName, m_pCfg->MustRemoveAccents());
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_PatientBirthDate, &pResponceData->PatBirthDate);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_PatientBirthTime, &pResponceData->PatBirthTime);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_OtherPatientIDs, &pResponceData->PatOtherId, m_pCfg->MustRemoveAccents());
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_PatientSex, &pResponceData->PatSex);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_OtherPatientNames, &pResponceData->PatOtherName, m_pCfg->MustRemoveAccents());
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_PatientAge, &pResponceData->PatAge);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_PatientSize, &pResponceData->PatSize);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_PatientWeight, &pResponceData->PatWeight);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_PatientMotherBirthName, &pResponceData->PatMotherName, m_pCfg->MustRemoveAccents());

			/////////////////////////////////////////////////////////////////
			//	Update study infos.
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_StudyInstanceUID, &pResponceData->StudyInstanceUID);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_StudyDate, &pResponceData->StudyDate);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_StudyTime, &pResponceData->StudyTime);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_StudyDescription, &pResponceData->StudyDesc, m_pCfg->MustRemoveAccents());
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_RequestedProcedureDescription, &pResponceData->StudyDesc, m_pCfg->MustRemoveAccents()) ;// [Ref:0000722]
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_StudyID, &pResponceData->StudyId);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_AccessionNumber, &pResponceData->StudyAccessionNumber);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_AdmittingDiagnosesDescription, &pResponceData->StudyAdmDiag, m_pCfg->MustRemoveAccents());
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_ReferringPhysicianName, &pResponceData->StudyRefPhysicianName, m_pCfg->MustRemoveAccents());
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_NameOfPhysiciansReadingStudy, &pResponceData->StudyReadPhysicianName, m_pCfg->MustRemoveAccents());
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_ModalitiesInStudy, &pResponceData->StudyModalitiesInStudy);

			// [Ref:0000722] - D�but
			cond = pDataset->findOrCreateSequenceItem(DCM_ProcedureCodeSequence,
													  pProcCodeSeq,
													  0);
			if(cond.good())
			{
				pProcCodeSeq->putAndInsertString(DCM_CodeValue, (char *)&pResponceData->StudyOtherNumber.Data);
				pProcCodeSeq->putAndInsertString(DCM_CodingSchemeDesignator, "IMAGEM");
				pProcCodeSeq->putAndInsertString(DCM_CodeMeaning, (char *)&pResponceData->StudyDesc.Data);
			}
			// [Ref:0000722] - Fin

			/////////////////////////////////////////////////////////////////
			//	Update series infos.
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_SeriesInstanceUID, &pResponceData->SeriesInstanceUID);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_SeriesDate, &pResponceData->SeriesDate);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_SeriesTime, &pResponceData->SeriesTime);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_SeriesDescription, &pResponceData->SeriesDesc, m_pCfg->MustRemoveAccents());
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_Laterality, &pResponceData->SeriesLaterality);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_Modality, &pResponceData->SeriesModality);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_SeriesNumber, &pResponceData->SeriesNumber);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_ViewPosition, &pResponceData->SeriesViewPosition, m_pCfg->MustRemoveAccents());
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_BodyPartExamined, &pResponceData->SeriesBodyPart);

			/////////////////////////////////////////////////////////////////
			//	Update object infos.
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_SOPClassUID, &pResponceData->ObjectSopClasUID);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_SOPInstanceUID, &pResponceData->ObjectInstanceUID);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_InstanceNumber, &pResponceData->ObjectInstanceNumber);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_AcquisitionDate, &pResponceData->ObjectAcquisitionDate);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_AcquisitionTime, &pResponceData->ObjectAcquisitionTime);

			/////////////////////////////////////////////////////////////////
			//	Character set.
			strcpy(sCharacterSet, m_pCfg->GetCharterSet());
			if(sCharacterSet[0])
			{
				DICOMUtil::UpdateTagInDataset(pDataset, DCM_SpecificCharacterSet, sCharacterSet);
			}
		}
		else
		{
			m_pModalityLogger->LogMsg("ERROR : Unable to clone dataset in C_FINDTransaction::CreateFindResultDataset.");
			m_pMailer->AddLine("Unable to clone dataset in C_FINDTransaction::CreateFindResultDataset");
		}

		if(m_pCfg->GetProfiling())
			dbg.EndProfiling(m_pServerLogger, NULL, "Responce dataset created.");

		return pDataset;
	}

	/////////////////////////////////////////////////////////////////////////
	//	Process a modality worklist query.
	bool C_FINDTransaction::ProcessWorklist(DcmDataset *pRequest)
	{
		bool bRetVal                    = true;
		QUERY_STATUS QueryRet           = QS_OK_NO_RESULT;
		WLData Query		            = {0};
		WLData Result		            = {0};
		char oldRequestedProcedure[129] = {0};
		char sNumber[17]                = {0};
		int iResultCount	            = 0;
		int iTime                       = 0;
		char sDateTime[33]              = {0};
		PatchParams PatchInfos;
		StringBuffer dump;
		PatchModule Patch;
		DBGTool dbg;

		/////////////////////////////////////////////////////////////////////
		//	dcmtk variables.
		OFCondition cond           = EC_Normal;
		DcmDataset *pResultDataset = NULL;
		DcmDataset *pStatus        = NULL;

		if(m_pCfg->GetModalityDebug())
		{
			m_pModalityLogger->LogMsg("--ProcessWorklist()");
		}

		m_CurrentRsp.CommandField                           = DIMSE_C_FIND_RSP;
		m_CurrentRsp.msg.CFindRSP.MessageIDBeingRespondedTo = m_CurrentReq.msg.CFindRQ.MessageID;
		m_CurrentRsp.msg.CFindRSP.DimseStatus               = STATUS_Pending;	//	Assuming.
		m_CurrentRsp.msg.CFindRSP.opts                      = O_FIND_AFFECTEDSOPCLASSUID;

		strcpy(m_CurrentRsp.msg.CFindRSP.AffectedSOPClassUID, m_CurrentReq.msg.CFindRQ.AffectedSOPClassUID);

		/////////////////////////////////////////////////////////////////////
		//
		if(ExtractWorklistQry(pRequest, &Query))
		{
			QueryRet = m_DataService.StartFindWorklist(m_pAssociation->params->DULparams.callingAPTitle, &Query, &Result);
			if(QueryRet == QS_OK_RESULT)
			{
				//////////////////////////////////
				//	We have results, send them.
				//////////////////////////////////
				do
				{
					//	Ugly hack because of database structure.
					//	This shit needs to be fixed some day.
					if(strcmp(Result.RequestedProcedureId, oldRequestedProcedure)!= 0)
					{
						++iResultCount;

						m_CurrentRsp.msg.CFindRSP.DimseStatus = STATUS_Pending;
						m_CurrentRsp.msg.CFindRSP.DataSetType = DIMSE_DATASET_PRESENT;

						pResultDataset = CreateFindWorklistResultDataset(&Result);
						if(pResultDataset)
						{
							//	Dump the responce dataset if debug
							//	mode is on for that AE title.
							if(m_pCfg->GetModalityDebug())
							{
								Time::GetDate(sDateTime);		//	Get the date for the sub-directory.
								dump.Format("%s/%s_C_Find_%d/", (char *)m_sCurrentDebugDir, sDateTime, m_iTransactionId);

								os::MkDir((char *)dump);

								Time::GetTimeOfDay(sDateTime);	//	Get the time for the file.
								dump += "WorklistResponce_";
								dump += sDateTime;
								sprintf(sNumber, "%d", iResultCount);
								dump += "_";
								dump += sNumber;
								dump += ".dcm";

								cond = pResultDataset->saveFile((char *)dump, EXS_LittleEndianExplicit);
								if(cond.bad())
								{
									m_pModalityLogger->LogMsg("ERROR %s", cond.text());
									m_pModalityLogger->LogMsg("      Failed to save debug dataset.");
								}
							}

							//	Patch for Swissray ddr.
							strcpy(PatchInfos.sPatchId, "00001");
							PatchInfos.pCallingAeTitle = m_pAssociation->params->DULparams.callingAPTitle;
							PatchInfos.pDataset        = pResultDataset;
							PatchInfos.pModalityLogger = m_pModalityLogger;
							PatchInfos.pMailer         = m_pMailer;
							PatchInfos.pDb             = m_DataService.GetDatabase();
							PatchInfos.pCfg            = m_pCfg;

							Patch.Dispatcher(&PatchInfos);

							if(m_pCfg->GetProfiling())
								dbg.StartProfiling(m_pServerLogger, "Sending responce.");

							/////////////////////////////////////////////////
							//	Send responce result.
							cond = DIMSE_sendMessageUsingMemoryData(m_pAssociation, m_CurrentPresID, &m_CurrentRsp, pStatus, pResultDataset, NULL, NULL);
							if(cond.bad())
							{
								m_pModalityLogger->LogMsg("ERROR : %s", cond.text());
								m_pModalityLogger->LogMsg("        Unable to send C-FIND responce.");

								m_pMailer->AddLine("%s", cond.text());
								m_pMailer->AddLine("Unable to send C-FIND responce.");

								DICOMUtil::ClearDataset(&pResultDataset);
								DICOMUtil::ClearDataset(&pStatus);

								bRetVal = false;

								break;
							}
							DICOMUtil::ClearDataset(&pResultDataset);
							DICOMUtil::ClearDataset(&pStatus);

							if(m_pCfg->GetProfiling())
								dbg.EndProfiling(m_pServerLogger, NULL, "Responce sent.");
						}
						strcpy(oldRequestedProcedure, Result.RequestedProcedureId);
					}
				}while(m_DataService.NextFindResult());
			}
			else if(QueryRet == QS_ERROR)
			{
				m_CurrentRsp.msg.CFindRSP.DimseStatus = STATUS_FIND_Failed_UnableToProcess;
				m_CurrentRsp.msg.CFindRSP.DataSetType = DIMSE_DATASET_NULL;

				bRetVal  = false;
			}
			else
			{
				//	The request to the database was successful but there was no result.
				m_CurrentRsp.msg.CFindRSP.DimseStatus = STATUS_Success;
				m_CurrentRsp.msg.CFindRSP.DataSetType = DIMSE_DATASET_NULL;

				m_pModalityLogger->LogMsg("No result was found.");

				m_pMailer->AddLine("No result was found.");
			}
		}
		else
		{
			m_CurrentRsp.msg.CFindRSP.DimseStatus = STATUS_FIND_Failed_UnableToProcess;
			m_CurrentRsp.msg.CFindRSP.DataSetType = DIMSE_DATASET_NULL;

			m_pModalityLogger->LogMsg("Unable to extract data from find worklist request.");

			m_pMailer->AddLine("Unable to extract data from find worklist request.");

			bRetVal  = false;
		}

		if(bRetVal)
		{
			m_CurrentRsp.msg.CFindRSP.DimseStatus = STATUS_Success;
			m_CurrentRsp.msg.CFindRSP.DataSetType = DIMSE_DATASET_NULL;
		}
		else
		{
			m_CurrentRsp.msg.CFindRSP.DimseStatus = STATUS_FIND_Failed_UnableToProcess;
			m_CurrentRsp.msg.CFindRSP.DataSetType = DIMSE_DATASET_NULL;
		}

		if(m_pCfg->GetProfiling())
			dbg.StartProfiling(m_pServerLogger, "Sending responce.");

		cond = DIMSE_sendMessageUsingMemoryData(m_pAssociation, m_CurrentPresID, &m_CurrentRsp, pStatus, NULL, NULL, NULL);
		if(cond.bad())
		{
			m_pModalityLogger->LogMsg("ERROR : %s", cond.text());
			m_pModalityLogger->LogMsg("        Could not send responce.");

			m_pMailer->AddLine("%s", cond.text());
			m_pMailer->AddLine("Could not send responce.");

			bRetVal = false;
		}
		
		m_pModalityLogger->LogMsg("Number of result found : %d", iResultCount);
		m_pMailer->AddLine("Number of result found : %d", iResultCount);

		DICOMUtil::ClearDataset(&pResultDataset);
		DICOMUtil::ClearDataset(&pStatus);

		if(m_pCfg->GetProfiling())
			dbg.EndProfiling(m_pServerLogger, NULL, "Responce sent.");

		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////////
	//
	bool C_FINDTransaction::ExtractWorklistQry(DcmDataset *pDataset, WLData *pInfo)
	{
		bool bRetVal          = false;
		OFCondition cond      = EC_Normal;
		char *pValue          = NULL;
		DcmElement *pElement  = NULL;
		DcmElement *pSequence = NULL;
		DcmItem    *pDi       = NULL;
		int iElemCount        = 0;
		unsigned int iTime    = 0;
		DBGTool dbg;

		if(m_pCfg->GetModalityDebug())
		{
			m_pModalityLogger->LogMsg("--ExtractWorklistQry()");
		}

		if(m_pCfg->GetProfiling())
			dbg.StartProfiling(m_pServerLogger, "Extracting C-FIND Worklist dataset.");

		if(pDataset)
		{
			cond = pDataset->findAndGetElement(DCM_ScheduledProcedureStepSequence, pSequence);
			if(cond.good())
			{
				DICOMUtil::GetTagFromDataset(pDataset, DCM_PatientName, pInfo->PatientName);

				Str::SplitNamesUsingToken(pInfo->PatientName, pInfo->PatientFirstName, pInfo->PatientLastName, '^');

				DICOMUtil::GetTagFromDataset(pDataset, DCM_PatientID, pInfo->PatientId);

				iElemCount = (int)((DcmSequenceOfItems*)pSequence)->card();
				if(iElemCount > 0)
				{
					//	If the sequence attribute is present and non-empty,
					//	we need to check every item in the sequence for completeness.
					//	iElemCount should always be one or zero anyway.
					for(int i = 0; i < iElemCount; ++i)
					{
						pDi = ((DcmSequenceOfItems*)pSequence)->getItem(i);
						if(pDi)
						{
							DICOMUtil::GetTagFromDataset(pDi, DCM_ScheduledStationAETitle, pInfo->ScheduledStationAeTitle);
							DICOMUtil::GetTagFromDataset(pDi, DCM_ScheduledProcedureStepStartDate,pInfo->ScheduledProcedureStepStartDate);
							DICOMUtil::GetTagFromDataset(pDi, DCM_ScheduledProcedureStepStartTime, pInfo->ScheduledProcedureStepStartTime);
							DICOMUtil::GetTagFromDataset(pDi, DCM_Modality, pInfo->Modality);
							DICOMUtil::GetTagFromDataset(pDi, DCM_ScheduledPerformingPhysicianName, pInfo->ScheduledPerformingPhysicianName);

							bRetVal = true;
						}
						else
						{
							m_pModalityLogger->LogMsg("ERROR : NULL element in scheduled procedure step sequence.");
							m_pMailer->AddLine("NULL element in scheduled procedure step sequence.");
						}
					}
				}
				else
				{
					//	We have no query criteria, send everything.
					//	We will put nothing in the where clause of
					//	our select. It may take some time ...
				}
			}
			else
			{
				//	If the sequence attribute isn't 
				//	present, we want to return true.
				bRetVal = true;
			}
		}

		if(m_pCfg->GetProfiling())
			dbg.EndProfiling(m_pServerLogger, NULL, "Requested extracted.");

		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	DcmDataset *C_FINDTransaction::CreateFindWorklistResultDataset(WLData *pResult)
	{
		DcmDataset *pDataset      = NULL;
		DcmItem *pItem            = NULL;
		char CodeValue[129]       = {0};
		char ReqProcDesc[129]     = {0};
		char LargeValue[129]      = {0};
		char Value[65]            = {0};
		char sCharacterSet[65]    = {0};
		OFCondition cond          = EC_Normal;
		unsigned int iTime        = 0;
		DBGTool dbg;

		if(m_pCfg->GetProfiling())
			dbg.StartProfiling(m_pServerLogger, "Creating responce dataset.");

		pDataset = new DcmDataset;
		if(pDataset)
		{
			DataFormat::RemoveMatriculeFromPhysicianName(pResult->ReferringPhysicianName);

			/////////////////////////////////////////////////////////////////////
			//	If there was no forced modality and no requested modality, we
			//	use the first returned by the select.
			DataFormat::CopyAndTrimToFirstModality(pResult->Modality, pResult->sSpsModality, ';');

			DICOMUtil::UpdateTagInDataset(pDataset, DCM_PatientName, pResult->PatientName, m_pCfg->MustRemoveAccents());
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_PatientID, pResult->PatientId, m_pCfg->MustRemoveAccents());
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_PatientBirthDate, pResult->PatientBirthDate);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_PatientBirthTime, pResult->PatientBirthTime);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_PatientSex, pResult->PatientSex);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_StudyInstanceUID, pResult->StudyInstanceUID);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_AccessionNumber, pResult->AccesionNumber);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_RequestingPhysician, pResult->RequestingPhysician, m_pCfg->MustRemoveAccents());

			DICOMUtil::UpdateTagInDataset(pDataset, DCM_ReferringPhysicianName, pResult->ReferringPhysicianName, m_pCfg->MustRemoveAccents());
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_AdmissionID, pResult->VisitUID);

			strcpy(LargeValue, pResult->RequestedProcedureCode);
			strcat(LargeValue, " - ");

			if(strlen(pResult->RequestedProcedureLaterality) > 0)
			{
				strcat(LargeValue, pResult->RequestedProcedureLaterality);
				strcat(LargeValue, " ");
			}
			strcat(LargeValue, pResult->RequestedProcedureDescription);

			strncpy(Value, LargeValue, 63);
			Value[64] = 0x00;

			if(m_pCfg->MustRemoveAccents())
			{
				os::RemoveAccents(Value);
			}
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_RequestedProcedureDescription, Value);

			/////////////////////////////////////////////////////////////////
			//	Character set.
			strcpy(sCharacterSet, m_pCfg->GetCharterSet());
			if(sCharacterSet[0])
			{
				DICOMUtil::UpdateTagInDataset(pDataset, DCM_SpecificCharacterSet, sCharacterSet);
			}

			/////////////////////////////////////////////////////////////////
			//	Requested procedure code sequence (0x0032, 0x1064).
			cond = pDataset->findOrCreateSequenceItem(DCM_RequestedProcedureCodeSequence,
													  pItem,
													  -2);
			if(cond.good())
			{
				if(m_DataService.UseInternalCode(m_pAssociation->params->DULparams.callingAPTitle))
				{
					strcpy(CodeValue, pResult->RequestedProcedureInternalCode);
					strcat(CodeValue, pResult->RequestedProcedureLaterality);
				}
				else
				{
					strcpy(CodeValue, pResult->RequestedProcedureCode);
				}

				if(strlen(pResult->RequestedProcedureLaterality) > 0)
				{
					strcpy(ReqProcDesc, pResult->RequestedProcedureLaterality);
					strcat(ReqProcDesc, " ");
				}

				strcat(ReqProcDesc, pResult->RequestedProcedureDescription);
				if(m_pCfg->MustRemoveAccents())
				{
					os::RemoveAccents(ReqProcDesc);
				}

				if(m_pCfg->MustRemoveAccents())
				{
					os::RemoveAccents(CodeValue);
				}
				pItem->putAndInsertString(DCM_CodeValue, CodeValue);
				pItem->putAndInsertString(DCM_CodingSchemeDesignator, "IMAGEM");
				pItem->putAndInsertString(DCM_CodeMeaning, ReqProcDesc);
			}

			/////////////////////////////////////////////////////////////////
			//	Scheduled procedure sequence (0x0040, 0x0100).
			cond = pDataset->findOrCreateSequenceItem(DCM_ScheduledProcedureStepSequence,
													  pItem,
													  -2);
			if(cond.good())
			{
				DICOMUtil::UpdateTagInDataset(pItem, DCM_ScheduledStationAETitle, m_pAssociation->params->DULparams.callingAPTitle);
				DICOMUtil::UpdateTagInDataset(pItem, DCM_ScheduledProcedureStepStartDate, pResult->ScheduledProcedureStepStartDate);
				DICOMUtil::UpdateTagInDataset(pItem, DCM_ScheduledProcedureStepStartTime, pResult->ScheduledProcedureStepStartTime);
				DICOMUtil::UpdateTagInDataset(pItem, DCM_Modality, pResult->Modality);
				DICOMUtil::UpdateTagInDataset(pItem, DCM_ScheduledPerformingPhysicianName, pResult->ScheduledPerformingPhysicianName);

				strncpy(Value, pResult->RequestedProcedureDescription, 63);
				Value[64] = 0x00;

				if(m_pCfg->MustRemoveAccents())
				{
					os::RemoveAccents(Value);
				}
				DICOMUtil::UpdateTagInDataset(pItem, DCM_ScheduledProcedureStepDescription, Value);

				DICOMUtil::UpdateTagInDataset(pItem, DCM_ScheduledProcedureStepLocation, pResult->ScheduledProcedureStepLocation, m_pCfg->MustRemoveAccents());
				DICOMUtil::UpdateTagInDataset(pItem, DCM_PreMedication, pResult->PreMedication, m_pCfg->MustRemoveAccents());
				DICOMUtil::UpdateTagInDataset(pItem, DCM_ScheduledProcedureStepID, pResult->ScheduledProcedureStepId);
				DICOMUtil::UpdateTagInDataset(pItem, DCM_RequestedContrastAgent, pResult->RequestedContrastAgent, m_pCfg->MustRemoveAccents());
				DICOMUtil::UpdateTagInDataset(pItem, DCM_ScheduledProcedureStepStatus, pResult->ScheduledProcedureStepStatus);
			}
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_RequestedProcedureID, pResult->RequestedProcedureId);
		}

		if(m_pCfg->GetProfiling())
			dbg.EndProfiling(m_pServerLogger, NULL, "Sql composed.");

		return pDataset;
	}

	/////////////////////////////////////////////////////////////////////////
	//	Process a general purpose worklist query.
	bool C_FINDTransaction::ProcessGPWorklist(DcmDataset *pRequestDataset)
	{
		bool bRet                  = true;
		int iResultCount		   = 0;
		bool bCancelled            = false;
		bool bNextResult           = false;
		bool bMustSendResponce     = false;
		QUERY_STATUS QueryRet      = QS_OK_NO_RESULT;
		char sDateTime[33]         = {0};
		char sNumber[17]           = {0};
		unsigned int iTime         = 0;
		StringBuffer dump;
		GPWLData GPReq;
		DBGTool dbg;

		/////////////////////////////////////////////////////////////////////
		//	dcmtk variables.
		OFCondition cond           = EC_Normal;
		DcmDataset *pResultDataset = NULL;
		DcmDataset *pStatus        = NULL;

		/////////////////////////////////////////////////////////////////////
		//
		m_CurrentRsp.CommandField                           = DIMSE_C_FIND_RSP;
		m_CurrentRsp.msg.CFindRSP.MessageIDBeingRespondedTo = m_CurrentReq.msg.CFindRQ.MessageID;
		m_CurrentRsp.msg.CFindRSP.opts                      = O_FIND_AFFECTEDSOPCLASSUID;
		m_CurrentRsp.msg.CFindRSP.DimseStatus               = STATUS_Pending;	//	Assuming.

		strcpy(m_CurrentRsp.msg.CFindRSP.AffectedSOPClassUID, m_CurrentReq.msg.CFindRQ.AffectedSOPClassUID);

		/////////////////////////////////////////////////////////////////////
		//	Extract query data from the dataset.
		if(ExtractGPWorklistQry(pRequestDataset, &GPReq))
		{
			QueryRet = m_DataService.StartFindGPWorlkist(&GPReq);
			if(QueryRet == QS_OK_RESULT)
			{
				/////////////////////////////////////////////////////////////
				//	We have results, send them all.
				bNextResult = true;
				while(bNextResult)
				{
					bMustSendResponce = true;
					/////////////////////////////////////////////////////////
					//	Check for cancel request.
					cond = DIMSE_checkForCancelRQ(m_pAssociation, m_CurrentPresID, m_CurrentReq.msg.CFindRQ.MessageID);
					if(cond == EC_Normal)
					{
						//	We received a cancel request.
						bCancelled = true;
						m_CurrentRsp.msg.CFindRSP.DimseStatus = STATUS_FIND_Cancel_MatchingTerminatedDueToCancelRequest;
						m_CurrentRsp.msg.CFindRSP.DataSetType = DIMSE_DATASET_NULL;

						m_pServerLogger->LogMsg("Received cancel request.");
						m_pModalityLogger->LogMsg("Received cancel request.");
						m_pMailer->AddLine("Received cancel request.");

						break;
					}
					else if(cond == DIMSE_NODATAAVAILABLE)
					{
						//	DIMSE_checkForCancelRQ timeout, just continue
						//	since nothing has been received.
					}
					else
					{
						m_pModalityLogger->LogMsg("ERROR : %s", cond.text());
						m_pMailer->AddLine("%s", cond.text());
						m_CurrentRsp.msg.CFindRSP.DimseStatus = STATUS_FIND_Failed_UnableToProcess;
						m_CurrentRsp.msg.CFindRSP.DataSetType = DIMSE_DATASET_NULL;

						bRet = false;

						break;
					}
					//m_DataService.GetReferencedStudies(&GPReq);
					strcpy(GPReq.m_SOPClassUID.Data, UID_RETIRED_GeneralPurposeScheduledProcedureStepSOPClass);

					pResultDataset = CreateGPWorklistDataset(pRequestDataset, &GPReq);
					if(bMustSendResponce)
					{
						++iResultCount;
						if(pResultDataset)
						{
							//	Dump the responce dataset if debug
							//	mode is on for that AE title.
							if(m_pCfg->GetModalityDebug())
							{
								Time::GetDate(sDateTime);		//	Get the date for the sub-directory.
								dump.Format("%s/%s_C_Find_%d/", (char *)m_sCurrentDebugDir, sDateTime, m_iTransactionId);

								os::MkDir((char *)dump);

								Time::GetTimeOfDay(sDateTime);	//	Get the time for the file.
								dump += "GPWorklistResponce_";
								dump += sDateTime;
								sprintf(sNumber, "%d", iResultCount);
								dump += "_";
								dump += sNumber;
								dump += ".dcm";

								cond = pResultDataset->saveFile((char *)dump, EXS_LittleEndianExplicit);
								if(cond.bad())
								{
									m_pModalityLogger->LogMsg("ERROR %s", cond.text());
									m_pModalityLogger->LogMsg("      Failed to save debug dataset.");
								}
							}

							m_CurrentRsp.msg.CFindRSP.DimseStatus = STATUS_Pending;
							m_CurrentRsp.msg.CFindRSP.DataSetType = DIMSE_DATASET_PRESENT;

							if(m_pCfg->GetProfiling())
								dbg.StartProfiling(m_pServerLogger, "Sending responce.");

							/////////////////////////////////////////////////////////
							//	Send responce result.
							cond = DIMSE_sendMessageUsingMemoryData(m_pAssociation, m_CurrentPresID, &m_CurrentRsp, pStatus, pResultDataset, NULL, NULL);
							if(cond.bad())
							{
								m_pModalityLogger->LogMsg("ERROR : %s", cond.text());
								m_pModalityLogger->LogMsg("        Unable to send C-FIND (GP Worklist) responce.");
								m_pMailer->AddLine("%s", cond.text());
								m_pMailer->AddLine("Unable to send C-FIND (GP Worklist) responce.");

								bRet = false;

								break;
							}
							DICOMUtil::ClearDataset(&pResultDataset);
							DICOMUtil::ClearDataset(&pStatus);

							if(m_pCfg->GetProfiling())
								dbg.EndProfiling(m_pServerLogger, NULL, "Responce sent.");

							bNextResult = m_DataService.NextFindResult();
						}
						else
						{
							break;
						}
					}
				}

				if(!bCancelled)
				{
					if(bRet)
					{
						m_CurrentRsp.msg.CFindRSP.DimseStatus = STATUS_Success;
						m_CurrentRsp.msg.CFindRSP.DataSetType = DIMSE_DATASET_NULL;
					}
					else
					{
						m_CurrentRsp.msg.CFindRSP.DimseStatus = STATUS_FIND_Failed_UnableToProcess;
						m_CurrentRsp.msg.CFindRSP.DataSetType = DIMSE_DATASET_NULL;
					}
				}
			}
			else if(QueryRet == QS_ERROR)
			{
				m_CurrentRsp.msg.CFindRSP.DimseStatus = STATUS_FIND_Failed_UnableToProcess;
				m_CurrentRsp.msg.CFindRSP.DataSetType = DIMSE_DATASET_NULL;

				bRet  = false;
			}
			else
			{
				//	The request to the database was successful but there was no result.
				m_CurrentRsp.msg.CFindRSP.DimseStatus = STATUS_Success;
				m_CurrentRsp.msg.CFindRSP.DataSetType = DIMSE_DATASET_NULL;
				m_pModalityLogger->LogMsg("No result was found.");
				m_pMailer->AddLine("No result was found.");
			}
		}
		else
		{
			m_CurrentRsp.msg.CFindRSP.DimseStatus = STATUS_FIND_Failed_UnableToProcess;
			m_pServerLogger->LogMsg("ERROR : Unable to extract GPWL request data from dataset.");
			m_pModalityLogger->LogMsg("ERROR : Unable to extract GPWL request data from dataset.");
			m_pMailer->AddLine("ERROR : Unable to extract GPWL request data from dataset.");
		}

		if(m_pCfg->GetProfiling())
			dbg.StartProfiling(m_pServerLogger, "Sending responce.");

		cond = DIMSE_sendMessageUsingMemoryData(m_pAssociation, m_CurrentPresID, &m_CurrentRsp, pStatus, NULL, NULL, NULL);
		if(cond.bad())
		{
			m_pModalityLogger->LogMsg("ERROR : %s", cond.text());
			m_pModalityLogger->LogMsg("        Could not send responce.");
			m_pMailer->AddLine("%s", cond.text());
			m_pMailer->AddLine("Could not send responce.");

			bRet = false;
		}

		if(m_pCfg->GetProfiling())
			dbg.EndProfiling(m_pServerLogger, NULL, "Responce sent.");

		if(bCancelled)
		{	
			m_pServerLogger->LogMsg("C-FIND worklist cancelled.");
			m_pModalityLogger->LogMsg("C-FIND (GP Worklist) cancelled.");
			m_pMailer->AddLine("C-FIND (GP Worklist) cancelled.");
		}
		else
		{
			m_pModalityLogger->LogMsg("Number of result found : %d", iResultCount);
			m_pMailer->AddLine("Number of result found : %d", iResultCount);
		}
		DICOMUtil::ClearDataset(&pStatus);
		DICOMUtil::ClearDataset(&pResultDataset);

		return bRet;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	bool C_FINDTransaction::ExtractGPWorklistQry(DcmDataset *pDataset, GPWLData *pGPInfo)
	{
		OFCondition cond                = EC_Normal;
		const char *pValue              = NULL;
		DcmItem *pItem                  = NULL;
		DcmItem *pSubItem               = NULL;
		DcmElement *pCurElement         = NULL;
		DcmElement *pSubSequence        = NULL;
		DcmSequenceOfItems*pPerfCodeSeq = NULL;
		unsigned int iTime              = 0;
		StringBuffer dump;
		DBGTool dbg;

		if(m_pCfg->GetProfiling())
			dbg.StartProfiling(m_pServerLogger, "Extracting request data.");

		/////////////////////////////////////////////////////////////////////////
		//	Extract the first level tags (those that aren't in a sequence).
		/////////////////////////////////////////////////////////////////////////

		DICOMUtil::GetTagFromDataset(pDataset, DCM_RETIRED_GeneralPurposeScheduledProcedureStepStatus, &pGPInfo->m_GPProcedureStepStatus);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_RETIRED_GeneralPurposeScheduledProcedureStepPriority, &pGPInfo->m_GPScheduledProcStepPriority);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_InputAvailabilityFlag, &pGPInfo->m_InputAvailabilityFlag);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_ScheduledProcedureStepID, &pGPInfo->m_ScheduledProcStepId);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_PatientName, &pGPInfo->m_PatientName);

		Str::SplitNamesUsingToken(pGPInfo->m_PatientName.Data, pGPInfo->m_PatFirstName.Data, pGPInfo->m_PatLastName.Data, '^');

		DICOMUtil::GetTagFromDataset(pDataset, DCM_PatientID, &pGPInfo->m_PatientId);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_PatientBirthDate, &pGPInfo->m_PatientBirthDate);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_PatientSex, &pGPInfo->m_PatientSex);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_StudyInstanceUID, &pGPInfo->m_StudyInstanceUID);
		DICOMUtil::GetTagFromDataset(pDataset, /*DCM_ScheduledProcedureStepStartDateAndTime*/DcmTagKey(0x0040, 0x4005), &pGPInfo->m_SchProcStepStartDate);

		/////////////////////////////////////////////////////////////////////////
		//	Extract scheduled wortitem code.
		cond = pDataset->findAndGetElement(DCM_ScheduledWorkitemCodeSequence, pCurElement);
		if(cond.good() && pCurElement)
		{
			pItem = ((DcmSequenceOfItems*)pCurElement)->getItem(0);
			if(pItem)
			{
				DICOMUtil::GetTagFromDataset(pItem, DCM_CodeValue, &pGPInfo->m_WorkItemCodeValue); 
				DICOMUtil::GetTagFromDataset(pItem, DCM_CodingSchemeDesignator, &pGPInfo->m_WorkItemCodeScheme); 
				DICOMUtil::GetTagFromDataset(pItem, DCM_CodeMeaning, &pGPInfo->m_WorkItemCodeMeaning);
			}
		}

		/////////////////////////////////////////////////////////////////////////
		//	Extract scheduled performer.
		cond = pDataset->findAndGetElement(DCM_ScheduledHumanPerformersSequence, pCurElement);
		if(cond.good() && pCurElement)
		{
			pItem = ((DcmSequenceOfItems*)pCurElement)->getItem(0);
			if(pItem)
			{
				cond = pItem->findAndGetSequence(DCM_HumanPerformerCodeSequence, pPerfCodeSeq);
				if(cond.good() && pPerfCodeSeq)
				{
					pSubItem = ((DcmSequenceOfItems*)pPerfCodeSeq)->getItem(0);
					if(pSubItem)
					{
						DICOMUtil::GetTagFromDataset(pSubItem, DCM_CodeValue, &pGPInfo->m_SchPerformerCodeValue);
						DICOMUtil::GetTagFromDataset(pSubItem, DCM_CodingSchemeDesignator, &pGPInfo->m_SchPerformerCodeScheme); 
						DICOMUtil::GetTagFromDataset(pSubItem, DCM_CodeMeaning, &pGPInfo->m_SchPerformerCodeMeaning);
					}
				}
			}
		}

		/////////////////////////////////////////////////////////////////////////
		//	Extract scheduled station name.
		cond = pDataset->findAndGetElement(DCM_ScheduledStationNameCodeSequence, pCurElement);
		if(cond.good() && pCurElement)
		{
			pItem = ((DcmSequenceOfItems*)pCurElement)->getItem(0);
			if(pItem)
			{
				DICOMUtil::GetTagFromDataset(pItem, DCM_CodeValue, &pGPInfo->m_SchStationNameCodeValue); 
				DICOMUtil::GetTagFromDataset(pItem, DCM_CodingSchemeDesignator, &pGPInfo->m_SchStationNameCodeScheme); 
				DICOMUtil::GetTagFromDataset(pItem, DCM_CodeMeaning, &pGPInfo->m_SchStationNameCodeMeaning); 
			}
		}

		/////////////////////////////////////////////////////////////////////////
		//	Extract study informations.
		cond = pDataset->findAndGetElement(DCM_ReferencedRequestSequence, pCurElement);
		if(cond.good() && pCurElement)
		{
			pItem = ((DcmSequenceOfItems*)pCurElement)->getItem(0);
			if(pItem)
			{
				DICOMUtil::GetTagFromDataset(pItem, DCM_RequestedProcedureDescription, &pGPInfo->m_ReqProcDescription);
				DICOMUtil::GetTagFromDataset(pItem, DCM_AccessionNumber, &pGPInfo->m_AccessionNumber);
				DICOMUtil::GetTagFromDataset(pItem, DCM_ModalitiesInStudy, &pGPInfo->m_ModalitiesInStudy);

				/////////////////////////////////////////////////////////////
				//	NOTE : for some reason, dcmtk doesn't have a defined
				//         constant for  tag 0x032, 0x1064.
				cond = pItem->findAndGetElement(DcmTagKey(0x032, 0x1064), pSubSequence);
				if(cond.good() && pSubSequence)
				{
					pSubItem = ((DcmSequenceOfItems*)pSubSequence)->getItem(0);
					if(pSubItem)
					{
						DICOMUtil::GetTagFromDataset(pSubItem, DCM_CodeValue, &pGPInfo->m_ReqProcCodeValue);
						DICOMUtil::GetTagFromDataset(pSubItem, DCM_CodingSchemeDesignator, &pGPInfo->m_ReqProcCodeScheme);
						DICOMUtil::GetTagFromDataset(pSubItem, DCM_CodeMeaning, &pGPInfo->m_ReqProcCodeMeaning);
					}
				}
			}
		}

		if(m_pCfg->GetProfiling())
			dbg.EndProfiling(m_pServerLogger, NULL, "Request extracted.");

		return true;
	}

	////////////////////////////////////////////////////////////////////////////
	//
	DcmDataset *C_FINDTransaction::CreateGPWorklistDataset(DcmDataset *pReqDataset, GPWLData *pGPInfo)
	{
		bool bRetVal = false;
		OFCondition cond                       = EC_Normal;
		char sCharacterSet[65]                 = {0};
		DcmDataset *pDataset                   = NULL;
		DcmElement *pSequence                  = NULL;
		DcmElement *pSubSequence               = NULL;
		TagData *pCurRefUID                    = NULL;
		DcmItem *pItem                         = NULL;
		DcmItem *pSubItem                      = NULL;
		DcmItem *pNewItem                      = NULL;
		DcmSequenceOfItems *pPerformerSequence = NULL;
		DcmSequenceOfItems *pRefSequence       = NULL;	//	Related studies.
		bool bAddSequence                      = false;
		unsigned int iTime                     = 0;
		DBGTool dbg;

		if(m_pCfg->GetProfiling())
			dbg.StartProfiling(m_pServerLogger, "Creating responce dataset.");

		pDataset = (DcmDataset *)pReqDataset->clone();
		if(pDataset)
		{
			/////////////////////////////////////////////////////////////////
			//
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_SOPClassUID, &pGPInfo->m_SOPClassUID);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_SOPInstanceUID, &pGPInfo->m_SOPInstanceUID);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_RETIRED_GeneralPurposeScheduledProcedureStepStatus, &pGPInfo->m_GPProcedureStepStatus);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_RETIRED_GeneralPurposeScheduledProcedureStepPriority, &pGPInfo->m_GPScheduledProcStepPriority);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_InputAvailabilityFlag, &pGPInfo->m_InputAvailabilityFlag);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_ScheduledProcedureStepID, &pGPInfo->m_ScheduledProcStepId);
			DICOMUtil::UpdateTagInDataset(pDataset, /*DCM_ScheduledProcedureStepStartDateAndTime*/DcmTagKey(0x0040, 0x4005), &pGPInfo->m_SchProcStepStartDate);

			/////////////////////////////////////////////////////////////////
			//	Patient informations.
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_PatientName, &pGPInfo->m_PatientName, m_pCfg->MustRemoveAccents());
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_PatientID, &pGPInfo->m_PatientId);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_PatientBirthDate, &pGPInfo->m_PatientBirthDate);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_PatientSex, &pGPInfo->m_PatientSex);

			DICOMUtil::UpdateTagInDataset(pDataset, DCM_OtherPatientIDs, &pGPInfo->m_PatOID);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_PatientMotherBirthName, &pGPInfo->m_PatMother, m_pCfg->MustRemoveAccents());

			/////////////////////////////////////////////////////////////////
			//	Study info.
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_StudyDate, &pGPInfo->m_StudyDate);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_StudyTime, &pGPInfo->m_StudyTime);
			DICOMUtil::UpdateTagInDataset(pDataset, DCM_StudyInstanceUID, &pGPInfo->m_StudyInstanceUID);

			DICOMUtil::UpdateTagInDataset(pDataset, DCM_ReferringPhysicianName, &pGPInfo->m_StudyReferingPhysician, m_pCfg->MustRemoveAccents());  // [Ref:0000722]

			/////////////////////////////////////////////////////////////////
			//	Character set.
			strcpy(sCharacterSet, m_pCfg->GetCharterSet());
			if(sCharacterSet[0])
			{
				DICOMUtil::UpdateTagInDataset(pDataset, DCM_SpecificCharacterSet, sCharacterSet);
			}

			cond = pDataset->findAndGetElement(DCM_ScheduledWorkitemCodeSequence, pSequence);
			if(cond.good())
			{
				pItem = ((DcmSequenceOfItems*)pSequence)->getItem(0);
				if(pItem)
				{
					DICOMUtil::UpdateTagInDataset(pItem, DCM_CodeValue, &pGPInfo->m_WorkItemCodeValue);
					DICOMUtil::UpdateTagInDataset(pItem, DCM_CodingSchemeDesignator, &pGPInfo->m_WorkItemCodeScheme);
					DICOMUtil::UpdateTagInDataset(pItem, DCM_CodeMeaning, &pGPInfo->m_WorkItemCodeMeaning);
				}

				cond = pDataset->findAndGetSequence(DCM_HumanPerformerCodeSequence, pPerformerSequence);
				if(cond.good())
				{
					if(!pPerformerSequence)
					{
						pPerformerSequence = new DcmSequenceOfItems(DCM_HumanPerformerCodeSequence);
						bAddSequence = true;
					}
				}
				else
				{
					pPerformerSequence = new DcmSequenceOfItems(DCM_HumanPerformerCodeSequence);
					bAddSequence = true;
				}

				if(pPerformerSequence)
				{
					pItem = pPerformerSequence->getItem(0);
					if(pItem)
					{
						DICOMUtil::UpdateTagInDataset(pItem, DCM_CodeValue, &pGPInfo->m_SchPerformerCodeValue, m_pCfg->MustRemoveAccents());
						DICOMUtil::UpdateTagInDataset(pItem, DCM_CodingSchemeDesignator, &pGPInfo->m_SchPerformerCodeScheme, m_pCfg->MustRemoveAccents());
						DICOMUtil::UpdateTagInDataset(pItem, DCM_CodeMeaning, &pGPInfo->m_SchPerformerCodeMeaning, m_pCfg->MustRemoveAccents());
					}
					else
					{
						pItem = new DcmItem;
						if(pItem)
						{
							DICOMUtil::UpdateTagInDataset(pItem, DCM_CodeValue, &pGPInfo->m_SchPerformerCodeValue, m_pCfg->MustRemoveAccents());
							DICOMUtil::UpdateTagInDataset(pItem, DCM_CodingSchemeDesignator, &pGPInfo->m_SchPerformerCodeScheme, m_pCfg->MustRemoveAccents());
							DICOMUtil::UpdateTagInDataset(pItem, DCM_CodeMeaning, &pGPInfo->m_SchPerformerCodeMeaning, m_pCfg->MustRemoveAccents());

							pPerformerSequence->insert(pItem);
						}
					}
				}

				if(bAddSequence)
				{
					pDataset->insert(pPerformerSequence);
				}

				cond = pDataset->findAndGetElement(DCM_ScheduledStationNameCodeSequence, pSequence);
				if(cond.good())
				{
					pItem = ((DcmSequenceOfItems*)pSequence)->getItem(0);
					if(pItem)
					{
						DICOMUtil::UpdateTagInDataset(pItem, DCM_CodeValue, &pGPInfo->m_SchStationNameCodeValue, m_pCfg->MustRemoveAccents());
						DICOMUtil::UpdateTagInDataset(pItem, DCM_CodingSchemeDesignator, &pGPInfo->m_SchStationNameCodeScheme, m_pCfg->MustRemoveAccents());
						DICOMUtil::UpdateTagInDataset(pItem, DCM_CodeMeaning, &pGPInfo->m_SchStationNameCodeMeaning, m_pCfg->MustRemoveAccents());
					}
				}

				pSequence = NULL;
				cond = pDataset->findAndGetElement(DCM_ReferencedRequestSequence, pSequence);
				if(cond.good() && pSequence)
				{
					pItem = ((DcmSequenceOfItems*)pSequence)->getItem(0);

					DICOMUtil::UpdateTagInDataset(pItem, DCM_StudyInstanceUID, &pGPInfo->m_StudyInstanceUID);
					DICOMUtil::UpdateTagInDataset(pItem, DCM_RequestedProcedureDescription, &pGPInfo->m_ReqProcDescription);
					DICOMUtil::UpdateTagInDataset(pItem, DCM_AccessionNumber, &pGPInfo->m_AccessionNumber);
					DICOMUtil::UpdateTagInDataset(pItem, DCM_ModalitiesInStudy, &pGPInfo->m_ModalitiesInStudy);

					cond = pItem->findAndGetElement(DcmTagKey(0x032, 0x1064), pSubSequence);
					if(cond.good() && pSubSequence)
					{
						pSubItem = ((DcmSequenceOfItems*)pSubSequence)->getItem(0);
						if(pSubItem)
						{
							DICOMUtil::UpdateTagInDataset(pSubItem, DCM_CodeValue, &pGPInfo->m_ReqProcCodeValue, m_pCfg->MustRemoveAccents());
							DICOMUtil::UpdateTagInDataset(pSubItem, DCM_CodingSchemeDesignator, &pGPInfo->m_ReqProcCodeScheme, m_pCfg->MustRemoveAccents());
							DICOMUtil::UpdateTagInDataset(pSubItem, DCM_CodeMeaning, &pGPInfo->m_ReqProcCodeMeaning, m_pCfg->MustRemoveAccents());
						}
					}

					pItem->findAndGetSequence(DCM_ReferencedStudySequence, pRefSequence);

					//	The sequence doesn't already exists so let's create it.
					if(!pRefSequence)
					{
						pRefSequence = new DcmSequenceOfItems(DCM_ReferencedStudySequence);
						bAddSequence = true;
					}

					if(pRefSequence)
					{
						pGPInfo->m_sReferencedStudies.SetAtFirst();
						while(pGPInfo->m_sReferencedStudies.GetCurrent(pCurRefUID))
						{
							pNewItem = new DcmItem;
							if(pNewItem)
							{
								DICOMUtil::UpdateTagInDataset(pNewItem, DCM_ReferencedSOPClassUID, (char *)NULL);
								DICOMUtil::UpdateTagInDataset(pNewItem, DCM_ReferencedSOPInstanceUID, pCurRefUID);
								pRefSequence->insert(pNewItem);
							}
							pGPInfo->m_sReferencedStudies.Next();
						}
						if(bAddSequence)
						{
							pItem->insert(pRefSequence);
						}
					}
				}
			}

			//[Ref:0002233] Actual Human Performer
			DcmElement *pActualHumanPerformersSequence = NULL;
			DcmElement *pHumanPerformerSequence = NULL;

			pActualHumanPerformersSequence = new DcmSequenceOfItems(DCM_ActualHumanPerformersSequence);
			
			pItem = new DcmItem;
			if(pItem)
				((DcmSequenceOfItems*)pActualHumanPerformersSequence)->insert(pItem);

			pHumanPerformerSequence = new DcmSequenceOfItems(DCM_HumanPerformerCodeSequence);
			pItem->insert(pHumanPerformerSequence);

			pItem = new DcmItem;
			

			if(pItem)
			{
				((DcmSequenceOfItems*)pHumanPerformerSequence)->insert(pItem);
				DICOMUtil::UpdateTagInDataset(pItem, DCM_CodeValue, &pGPInfo->m_ActualPerformerCodeValue, m_pCfg->MustRemoveAccents());
				DICOMUtil::UpdateTagInDataset(pItem, DCM_CodeMeaning, &pGPInfo->m_ActualPerformerCodeValue, m_pCfg->MustRemoveAccents());
			}

			pDataset->insert(pActualHumanPerformersSequence);
			//[Ref:0002233] - End

		}
		else
		{
			m_pModalityLogger->LogMsg("ERROR : Failed to clone dataset in C_FINDTransaction::CreateGPWorklistDataset.");
			m_pMailer->AddLine("Failed to clone dataset in C_FINDTransaction::CreateGPWorklistDataset.");
		}

		if(m_pCfg->GetProfiling())
			dbg.EndProfiling(m_pServerLogger, NULL, "Responce dataset created.");

		return pDataset;
	}
}
