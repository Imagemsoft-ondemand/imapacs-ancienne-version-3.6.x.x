/////////////////////////////////////////////////////////////////////////////
//	C_GETTransaction.cpp
/////////////////////////////////////////////////////////////////////////////

#include "../Utils/TimeUtil.h"
#include "C_GETTransaction.h"

/////////////////////////////////////////////////////////////////////////////
//
namespace imapacs
{
	/////////////////////////////////////////////////////////////////////////
	//
	bool C_GETTransaction::Init(T_ASC_Association *pAssociation, 
		                        T_DIMSE_Message *pMsg, 
								T_ASC_PresentationContextID PresID, 
								PacsConfig *pCfg)
	{
		bool bRetVal= false;

		memcpy(&m_CurrentReq, pMsg, sizeof(T_DIMSE_Message));
		memset(&m_CurrentRsp, 0, sizeof(T_DIMSE_Message));

		m_CurrentRsp.CommandField                          = DIMSE_C_GET_RSP;
		m_CurrentRsp.msg.CGetRSP.DataSetType               = DIMSE_DATASET_NULL;	//	Assuming
		m_CurrentRsp.msg.CGetRSP.DimseStatus               = STATUS_Success;		//	Assuming.
		m_CurrentRsp.msg.CGetRSP.MessageIDBeingRespondedTo = m_CurrentReq.msg.CGetRQ.MessageID;

		strcpy(m_CurrentRsp.msg.CGetRSP.AffectedSOPClassUID, m_CurrentReq.msg.CGetRQ.AffectedSOPClassUID);

		m_pAssociation  = pAssociation;
		m_CurrentPresID = PresID;

		m_pCfg = pCfg;

		//	For convenience.
		m_pServerLogger   = pCfg->GetServerLogger();
		m_pModalityLogger = pCfg->GetModalityLogger();
		m_pMailer         = pCfg->GetMailer();

		m_pServerLogger->LogMsg("[%s] - C-GET request.", pAssociation->params->DULparams.callingAPTitle);
		m_pModalityLogger->LogMsg("C-GET request.");
		m_pMailer->AddLine("C-GET request.");

		if(pCfg->GetModalityDebug())
		{
			//	Generate a transaction id to group
			//	transaction related file names.
			srand((unsigned int)time(NULL));
			m_iTransactionId = rand();

			m_sCurrentDebugDir.Format("%s%s", DEBUG_DIRECTORY, pAssociation->params->DULparams.callingAPTitle);
			os::MkDir((char *)m_sCurrentDebugDir);
		}

		m_DataService.SetCurrentDebugParams(m_sCurrentDebugDir, m_iTransactionId);
		m_DataService.SetConfig(pCfg);

		m_Client.SetConfig(pCfg);

		bRetVal = true;

		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	bool C_GETTransaction::ProcessTransaction()
	{
		bool bRetVal        = true;
		bool bCancelled     = false;
		char sDateTime[33]  = {0};
		StringBuffer dump;
		DBGTool dbg;
		
		/////////////////////////////////////////////////////////////////////////
		//	dcmtk variables.
		OFCondition cond     = EC_Normal;
		DcmDataset *pResult  = NULL;
		DcmDataset *pRequest = NULL;

		if(m_pCfg->GetProfiling())
			dbg.StartProfiling(m_pServerLogger, "Reading dataset.");

		/////////////////////////////////////////////////////////////////////////
		//	Read request dataset.
		cond = DIMSE_receiveDataSetInMemory(m_pAssociation,
											DIMSE_BLOCKING,
											0,
											&m_CurrentDataPresID,
											&pRequest,
											NULL,
											NULL);

		if(m_pCfg->GetProfiling())
			dbg.EndProfiling(m_pServerLogger, NULL, "Dataset received.");

		//	Dump the dataset if debug mode is
		//	on for that AE title.
		if(m_pCfg->GetModalityDebug())
		{
			Time::GetDate(sDateTime);		//	Get the date for the sub-directory.
			dump.Format("%s/%s_C_Get_%d/", (char *)m_sCurrentDebugDir, sDateTime, m_iTransactionId);

			os::MkDir((char *)dump);

			Time::GetTimeOfDay(sDateTime);	//	Get the time for the file.
			dump += "Query_";
			dump += sDateTime;
			dump += ".dcm";

			cond = pRequest->saveFile((char *)dump, EXS_LittleEndianExplicit);
			if(cond.bad())
			{
				m_pModalityLogger->LogMsg("ERROR %s", cond.text());
				m_pModalityLogger->LogMsg("      Failed to save debug dataset.");
			}
		}

		if(cond.good())
		{
			if(m_CurrentDataPresID == m_CurrentPresID)
			{
				/////////////////////////////////////////////////////////////////////
				//	Check if SOP class UID is supported and wich one it is.
				if(strcmp(m_CurrentReq.msg.CGetRQ.AffectedSOPClassUID, UID_GETPatientRootQueryRetrieveInformationModel) == 0)
				{
					m_pServerLogger->LogMsg("Patient Root.");
					m_pModalityLogger->LogMsg("Patient Root.");
					m_pMailer->AddLine("Patient Root.");

					m_pCfg->InsertEvent("C-GET (PATIENT)",
										NULL,
										"IMAPACS",
										m_pAssociation->params->DULparams.callingAPTitle,
										NULL,
										NULL,
										NULL,
										NULL,
										NULL,
										NULL,
										NULL,
										NULL,
										NULL,
										NULL);

					bRetVal = ProcessGetQuery(pRequest, PATIENT_ROOT);
				}
				else if(strcmp(m_CurrentReq.msg.CGetRQ.AffectedSOPClassUID, UID_RETIRED_GETPatientStudyOnlyQueryRetrieveInformationModel) == 0)
				{
					m_pServerLogger->LogMsg("Study Root.");
					m_pModalityLogger->LogMsg("Study Root.");
					m_pMailer->AddLine("Study Root.");

					m_pCfg->InsertEvent("C-GET (STUDY)", 
										NULL, 
										"IMAPACS",
										m_pAssociation->params->DULparams.callingAPTitle, 
										NULL, 
										NULL, 
										NULL, 
										NULL, 
										NULL, 
										NULL, 
										NULL, 
										NULL, 
										NULL, 
										NULL);

					bRetVal = ProcessGetQuery(pRequest, STUDY_ROOT);
				}
				else if(strcmp(m_CurrentReq.msg.CGetRQ.AffectedSOPClassUID, UID_GETStudyRootQueryRetrieveInformationModel) == 0)
				{
					m_pServerLogger->LogMsg("Patient-Study Root.");
					m_pModalityLogger->LogMsg("Patient-Study Root.");
					m_pMailer->AddLine("Patient-Study Root.");

					m_pCfg->InsertEvent("C-GET (STUDY)", 
										NULL, 
										"IMAPACS",
										m_pAssociation->params->DULparams.callingAPTitle, 
										NULL, 
										NULL, 
										NULL, 
										NULL, 
										NULL, 
										NULL, 
										NULL, 
										NULL, 
										NULL, 
										NULL);

					bRetVal = ProcessGetQuery(pRequest, PATIENT_STUDY_ROOT);
				}
				else
				{
					m_CurrentRsp.msg.CGetRSP.DimseStatus = STATUS_MOVE_Failed_SOPClassNotSupported;
					m_CurrentRsp.msg.CGetRSP.DataSetType = DIMSE_DATASET_NULL;
				}
			}
			else
			{
				m_pModalityLogger->LogMsg("ERROR : Request and dataset presentation id mismatch.");
				m_pMailer->AddLine("ERROR : Request and dataset presentation id mismatch.");
			}
		}
		else
		{
			m_pModalityLogger->LogMsg("ERROR : %s", cond.text());
			m_pModalityLogger->LogMsg("        Could not receive C-GET dataset.");

			m_pMailer->AddLine("%s", cond.text());
			m_pMailer->AddLine("Could not receive C-GET dataset.");
		}
		DICOMUtil::ClearDataset(&pRequest);
		DICOMUtil::ClearDataset(&pResult);

		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	void C_GETTransaction::Shutdown()
	{
		m_DataService.Dispose();
		m_sCurrentDebugDir.Clear();
	}

	/////////////////////////////////////////////////////////////////////////
	//
	bool C_GETTransaction::ProcessGetQuery(DcmDataset *pReqDataset, int iQueryRoot)
	{
		bool bRetVal               = true;
		bool bCancelled            = false;
		char sObjectLocation[2049] = {0};
		int iElemCount             = 0;
		float fScaleFactorX        = -1.0f;
		float fScaleFactorY        = -1.0f;
		QUERY_STATUS Status        = QS_OK_RESULT;
		DBGTool dbg;

		StringBuffer FailedUIDs;	//	Failed UID instance list for responce.

		/////////////////////////////////////////////////////////////////////////
		//	NOTE : we use the same structure as in the C-MOVE since
		//	       it's the same fields. Maybe it should be renamed.
		/////////////////////////////////////////////////////////////////////////

		FindData MoveParams;
		MoveResult *pCandidate   = NULL;
		Queue <MoveResult *> CandidateList;

		/////////////////////////////////////////////////////////////////////////
		//	dcmtk variables.
		OFCondition cond         = EC_Normal;
		STORE_STATUS StoreStatus = SS_SUCCESS;
		DcmDataset *pStatus      = NULL;
		DcmDataset *pResult      = NULL;
		DIC_US iCompleted        = 0;
		DIC_US iFailed           = 0;
		DIC_US iWarning          = 0;
		DIC_US iRemaining        = 0;
		DIC_US DbStatus          = STATUS_Pending;	//	Assuming.

		strcpy(m_CurrentRsp.msg.CGetRSP.AffectedSOPClassUID, m_CurrentRsp.msg.CGetRSP.AffectedSOPClassUID);

		m_Client.SetAssociation(m_pAssociation);

		if(ExtractFindData(pReqDataset, &MoveParams))
		{
			Status = m_DataService.FindCandidates(&MoveParams, &CandidateList);
			if(Status == QS_OK_RESULT)
			{
				iRemaining = CandidateList.GetSize();
				DbStatus   = STATUS_Pending;

				/////////////////////////////////////////////////////////////////
				//	Try to send all objects.
				while(CandidateList.GetCurrent(pCandidate))
				{
					/////////////////////////////////////////////////////////////
					//	Check for cancel request.
					cond = DIMSE_checkForCancelRQ(m_pAssociation, m_CurrentPresID, m_CurrentReq.msg.CGetRQ.MessageID);
					if(cond == EC_Normal)
					{
						//	We received a cancel request.
						bCancelled = true;
						DbStatus   = STATUS_MOVE_Cancel_SubOperationsTerminatedDueToCancelIndication;

						m_pServerLogger->LogMsg("Received cancel request.");
						m_pModalityLogger->LogMsg("Received cancel request.");
						m_pMailer->AddLine("Received cancel request.");

						break;
					}
					else if(cond == DIMSE_NODATAAVAILABLE)
					{
						//	Normal timeout, just continue
						//	since nothing has been received.
					}
					else
					{
						m_pModalityLogger->LogMsg("ERROR : %s", cond.text());
						m_pMailer->AddLine("%s", cond.text());

						DbStatus = STATUS_MOVE_Failed_UnableToProcess;

						break;
					}

					/////////////////////////////////////////////////////////////
					//
					if(m_pCfg->FindObjectLocation(pCandidate->SOPInstanceUID, sObjectLocation))
					{
						--iRemaining;
//TEST
//strcpy(sObjectLocation, "/cr.dcm");
//END TEST
						if(MoveParams.bHasScaleFactors)
						{
							StoreStatus = ProcessStoreRescale(&MoveParams, sObjectLocation);
						}
						else
						{
							if(m_pCfg->GetProfiling())
								dbg.StartProfiling(m_pServerLogger, "Sending object to client.");

							StoreStatus = m_Client.StoreObject(sObjectLocation);

							if(m_pCfg->GetProfiling())
								dbg.EndProfiling(m_pServerLogger, NULL, "Object sent.");
						}

						switch(StoreStatus)
						{
							case SS_SUCCESS:
								++iCompleted;
							break;

							case SS_WARNING:
								++iWarning;
							break;

							case SS_ERROR:
							{
								++iFailed;										
								if(FailedUIDs.GetBufferLength() > 0)
									FailedUIDs += "\\";

								FailedUIDs += pCandidate->SOPInstanceUID;
							}
							break;
						}
					}
					else
					{
						++iFailed;										
						if(FailedUIDs.GetBufferLength() > 0)
							FailedUIDs += "\\";

						FailedUIDs += pCandidate->SOPInstanceUID;

						m_pModalityLogger->LogMsg("        Failed to find object with instance %s", pCandidate->SOPInstanceUID);
						m_pMailer->AddLine("Failed to find object with instance [%s]", pCandidate->SOPInstanceUID);
					}

					if(CandidateList.Next())
					{
						DbStatus = STATUS_Pending;

						PrepareGetResponce(DbStatus, iCompleted, iFailed, iWarning, iRemaining, &FailedUIDs, &pResult, bCancelled);

						if(m_pCfg->GetProfiling())
							dbg.StartProfiling(m_pServerLogger, "Sending responce.");

						cond = SendC_GETResponce(pResult);
						if(cond.bad())
						{
							m_pModalityLogger->LogMsg("ERROR : %s", cond.text());
							m_pModalityLogger->LogMsg("        Unabel to send move responce.");

							m_pMailer->AddLine("%s", cond.text());
							m_pMailer->AddLine("Unabel to send move responce.");

							bRetVal = false;
							break;
						}

						if(m_pCfg->GetProfiling())
							dbg.EndProfiling(m_pServerLogger, NULL, "Responce sent.");
					}
				}

				if(iFailed == 0)
				{
					DbStatus = STATUS_Success;
				}
				else
				{
					DbStatus = STATUS_STORE_Refused_OutOfResources;
				}
			}
			else if(Status == QS_OK_NO_RESULT)
			{
				DbStatus = STATUS_Success;
				bRetVal = true;

				m_pModalityLogger->LogMsg("No result found.");
				m_pMailer->AddLine("No result found.");
			}
			else
			{
				DbStatus = STATUS_GET_Failed_UnableToProcess;
				bRetVal = false;

				m_pModalityLogger->LogMsg("Unable to process C-GET.");
				m_pMailer->AddLine("Unable to process C-GET.");
			}

			PrepareGetResponce(DbStatus, iCompleted, iFailed, iWarning, iRemaining, &FailedUIDs, &pResult, bCancelled);

			if(m_pCfg->GetProfiling())
				dbg.StartProfiling(m_pServerLogger, "Sending responce.");

			cond = SendC_GETResponce(pResult);
			if(cond.bad())
			{
				m_pModalityLogger->LogMsg("ERROR : %s", cond.text());
				m_pModalityLogger->LogMsg("        Unabel to send move responce.");

				m_pMailer->AddLine("%s", cond.text());
				m_pMailer->AddLine("Unabel to send move responce.");
			}
			ClearCandidateList(&CandidateList);

			if(m_pCfg->GetProfiling())
				dbg.EndProfiling(m_pServerLogger, NULL, "Responce sent.");
		}
		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	bool C_GETTransaction::ExtractFindData(DcmDataset *pDataset, FindData *pFindData)
	{
		bool bRetVal           = false;
		char sScaleFactors[65] = {0};
		const char *pValue     = NULL;
		OFCondition cond       = EC_Normal;
		DBGTool dbg;

		if(m_pCfg->GetProfiling())
			dbg.StartProfiling(m_pServerLogger, "Extracting dataset.");

		DICOMUtil::GetTagFromDataset(pDataset, DCM_QueryRetrieveLevel, pFindData->sQLevel);

		/////////////////////////////////////////////////////////////////////////
		//	Extract patient infos.
		DICOMUtil::GetTagFromDataset(pDataset, DCM_PatientID, &pFindData->PatId);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_PatientName, &pFindData->PatName);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_PatientBirthDate, &pFindData->PatBirthDate);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_PatientBirthTime, &pFindData->PatBirthTime);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_OtherPatientIDs, &pFindData->PatOtherId);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_PatientSex, &pFindData->PatSex);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_OtherPatientNames, &pFindData->PatOtherName);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_PatientAge, &pFindData->PatAge);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_PatientSize, &pFindData->PatSize);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_PatientWeight, &pFindData->PatWeight);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_PatientMotherBirthName, &pFindData->PatMotherName);

		/////////////////////////////////////////////////////////////////////////
		//	Extract study infos.
		DICOMUtil::GetTagFromDataset(pDataset, DCM_StudyInstanceUID, &pFindData->StudyInstanceUID);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_StudyDate, &pFindData->StudyDate);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_StudyTime, &pFindData->StudyTime);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_StudyDescription, &pFindData->StudyDesc);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_StudyID, &pFindData->StudyId);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_AccessionNumber, &pFindData->StudyAccessionNumber);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_AdmittingDiagnosesDescription, &pFindData->StudyAdmDiag);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_ReferringPhysicianName, &pFindData->StudyRefPhysicianName);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_NameOfPhysiciansReadingStudy, &pFindData->StudyReadPhysicianName);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_ModalitiesInStudy, &pFindData->StudyModalitiesInStudy);

		/////////////////////////////////////////////////////////////////////////
		//	Extract series infos.
		DICOMUtil::GetTagFromDataset(pDataset, DCM_SeriesInstanceUID, &pFindData->SeriesInstanceUID);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_SeriesDate, &pFindData->SeriesDate);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_SeriesTime, &pFindData->SeriesTime);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_SeriesDescription, &pFindData->SeriesDesc);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_Laterality, &pFindData->SeriesLaterality);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_Modality, &pFindData->SeriesModality);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_SeriesNumber, &pFindData->SeriesNumber);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_ViewPosition, &pFindData->SeriesViewPosition);

		/////////////////////////////////////////////////////////////////////////
		//	Extract object infos.
		DICOMUtil::GetTagFromDataset(pDataset, DCM_SOPClassUID, &pFindData->ObjectSopClasUID);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_SOPInstanceUID, &pFindData->ObjectInstanceUID);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_InstanceNumber, &pFindData->ObjectInstanceNumber);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_AcquisitionDate, &pFindData->ObjectAcquisitionDate);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_AcquisitionTime, &pFindData->ObjectAcquisitionTime);

		/////////////////////////////////////////////////////////////////////////
		//	Private tags if any.
		DICOMUtil::GetTagFromDataset(pDataset, DCM_ImagemRequestedScale, sScaleFactors);

		if(sScaleFactors[0])
		{
			pFindData->bHasScaleFactors = true;
			DataFormat::ExtractScalingFactors(&pFindData->fXscaleFactor, &pFindData->fYscaleFactor, sScaleFactors);
		}
		else
		{
			pFindData->bHasScaleFactors = false;
		}
		//
		/////////////////////////////////////////////////////////////////////////

		if(pFindData->sQLevel[0])
		{
			bRetVal = true;
		}
		else
		{
			m_pModalityLogger->LogMsg("ERROR : %s", cond.text());
			m_pModalityLogger->LogMsg("         Unable to get query level from request dataset in DICOMSession::ExtractFindData.");

			m_pMailer->AddLine("Unable to get query level from request dataset in DICOMSession::ExtractFindData.");
		}

		if(m_pCfg->GetProfiling())
			dbg.EndProfiling(m_pServerLogger, NULL, "Dataset extracted.");

		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	void C_GETTransaction::PrepareGetResponce(DIC_US OperationStatus,
						                      DIC_US iCompleted, 
							                  DIC_US iFailed,
							                  DIC_US iWarning,
							                  DIC_US iRemaining,
							                  StringBuffer *pFailedUIDList,
							                  DcmDataset **pResult,
							                  bool bCancelled)
	{
		DIC_US Status = STATUS_Pending;
		if(!bCancelled)
		{
			if(OperationStatus != STATUS_Pending)
			{
				//////////////////////////////////////////////////
				//	Adjust the final status if any sub-operations
				//	failed or had warnings .
				if((iFailed > 0) || (iWarning > 0))
				{
					Status = STATUS_MOVE_Warning_SubOperationsCompleteOneOrMoreFailures;
				}

				/////////////////////////////////////////////////////
				//	If all the sub-operations failed then we need to
				//	generate a failed or refused status. cf. DICOM
				//	part 4, C.4.2.3.1, we choose to generate a
				//	"Refused - Out of Resources - Unable to perform
				//	suboperations" status.
				if((iFailed > 0) && ((iCompleted + iWarning) == 0)) 
				{
					Status = STATUS_MOVE_Refused_OutOfResourcesSubOperations;
				}

				//	We no longer have results to send.
				if(iFailed == 0)
				{
					if((OperationStatus != STATUS_MOVE_Refused_OutOfResourcesSubOperations) &&
					   (OperationStatus != STATUS_MOVE_Failed_MoveDestinationUnknown))
						Status = STATUS_Success;
					else
					{
						Status = OperationStatus;
					}
				}
			}

			/////////////////////////////////////////////////////////////////////
			//
			if((m_CurrentRsp.msg.CGetRSP.DimseStatus != STATUS_Pending) && (m_CurrentRsp.msg.CGetRSP.DimseStatus != STATUS_Success))
			{
				(*pResult) = CreateMoveResponceDataset(pFailedUIDList->GetBuffer());
			}
		}
		else
		{
			Status = OperationStatus;

			m_pServerLogger->LogMsg("C-GET cancelled.");
			m_pModalityLogger->LogMsg("C-GET cancelled.");
			m_pMailer->AddLine("C-GET cancelled.");
		}

		/////////////////////////////////////////////////////////////////////
		//	Fill the remaining responce fields.
		m_CurrentRsp.msg.CGetRSP.DimseStatus                    = Status;
		m_CurrentRsp.msg.CGetRSP.NumberOfRemainingSubOperations = iRemaining;
		m_CurrentRsp.msg.CGetRSP.NumberOfCompletedSubOperations = iCompleted;
		m_CurrentRsp.msg.CGetRSP.NumberOfFailedSubOperations    = iFailed;
		m_CurrentRsp.msg.CGetRSP.NumberOfWarningSubOperations   = iWarning;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	DcmDataset *C_GETTransaction::CreateMoveResponceDataset(const char *pFailedUIDList)
	{
		OFBool bOk           = OFTrue;
		DcmDataset *pDataset = NULL;

		if(pFailedUIDList)
		{
			pDataset = new DcmDataset;
			if(pDataset)
			{
				bOk = DU_putStringDOElement(pDataset,
											DCM_FailedSOPInstanceUIDList,
											pFailedUIDList);
				if(!bOk)
				{
					m_pModalityLogger->LogMsg("WARNING : Unable to create failed UID list for C-MOVE responce.");
					m_pModalityLogger->LogMsg("          Unable to put value in tag [%s]", pFailedUIDList);

					m_pMailer->AddLine("Unable to create failed UID list for C-MOVE responce.");
					m_pMailer->AddLine("Unable to put value in tag [%s]", pFailedUIDList);
				}
			}
		}
		return pDataset;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	OFCondition C_GETTransaction::SendC_GETResponce(DcmDataset *pResult)
	{
		OFCondition cond    = EC_Normal;
		DcmDataset *pStatus = NULL;

		m_CurrentRsp.msg.CGetRSP.opts = O_GET_AFFECTEDSOPCLASSUID;

		switch(m_CurrentRsp.msg.CGetRSP.DimseStatus)
		{
			case STATUS_Success:
			case STATUS_Pending:
			{
				//	Success cannot have a Failed SOP Instance UID list
				//	(no failures). Pending may not send such a list.
				m_CurrentRsp.msg.CGetRSP.DataSetType = DIMSE_DATASET_NULL;
				pResult = NULL;  //	Zero our local pointer.
			}
			break;

			default:
			{
				//	Send it if provided.
				if(pResult == NULL) 
					m_CurrentRsp.msg.CGetRSP.DataSetType = DIMSE_DATASET_NULL;
				else 
					m_CurrentRsp.msg.CGetRSP.DataSetType = DIMSE_DATASET_PRESENT;
			}
			break;
		}

		//	Make sure the numberOf fields are conformant with
		//	the status (see Part 4, C.4.2.1.6-9)
		m_CurrentRsp.msg.CGetRSP.opts = (O_GET_NUMBEROFREMAININGSUBOPERATIONS |
									     O_GET_NUMBEROFCOMPLETEDSUBOPERATIONS |
									     O_GET_NUMBEROFFAILEDSUBOPERATIONS    |
									     O_GET_NUMBEROFWARNINGSUBOPERATIONS);

		switch(m_CurrentRsp.msg.CGetRSP.DimseStatus)
		{
			case STATUS_Pending:
			case STATUS_MOVE_Cancel_SubOperationsTerminatedDueToCancelIndication:
			{
				//	Who cares.
			}
			break;

			default:
			//	Remaining sub-operations may not be in responses 
			//	with a status of Warning, Failed, Refused or Successful.
			m_CurrentRsp.msg.CGetRSP.opts &= (~ O_GET_NUMBEROFREMAININGSUBOPERATIONS);
			break;
		}
		m_CurrentRsp.msg.CGetRSP.opts |= m_CurrentRsp.msg.CGetRSP.opts;

		cond = DIMSE_sendMessageUsingMemoryData(m_pAssociation,
												m_CurrentPresID,
												&m_CurrentRsp,
												NULL,
												pResult,
												NULL,
												NULL);

		DICOMUtil::ClearDataset(&pStatus);

		return cond;
	}

	/////////////////////////////////////////////////////////////////////////
	//	Manage the logic for scaling the image according to the received
	//	scaling factors. This is mostly validation code.
	STORE_STATUS C_GETTransaction::ProcessStoreRescale(FindData *pParams,
		                                               const char *psObjectLocation)
	{
		STORE_STATUS eRetVal     = SS_ERROR;
		OFCondition cond         = EC_Normal;
		DcmDataset *pSrcDataset  = NULL;
		double fFactorX          = 0.0f;
		double fFactorY          = 0.0f;
		int iPreserveAspectRatio = 1;
		bool bScale              = true;
		DcmFileFormat File;

		cond = File.loadFile(psObjectLocation);
		if(cond.good())
		{
			pSrcDataset = File.getDataset();
			if(pSrcDataset)
			{
				//	Generate the scaled image according to the
				//	parameters received.
				if(pParams->fXscaleFactor < 0)
				{
					if(pParams->fYscaleFactor > 0)
					{
						//	We scale in Y according to the y scaling
						//	factor. Scaling will be done in x so that
						//	aspect ratio is perserved.
						iPreserveAspectRatio = 1;
						fFactorX             = 0.0f;
						fFactorY             = pParams->fYscaleFactor;
						bScale               = true;
					}
					else
					{
						//	No scaling will occure, parameters are
						//	ambiguous.
						bScale = false;
					}
				}
				else
				{
					if(pParams->fXscaleFactor > 0)
					{
						if(pParams->fYscaleFactor < 0)
						{
							//	We scale in X according to the x scaling
							//	factor. Scaling will be done in y so that
							//	aspect ratio is perserved.
							iPreserveAspectRatio = 1;
							fFactorX             = pParams->fXscaleFactor;
							fFactorY             = 0.0f;
							bScale               = true;
						}
						else
						{
							if(pParams->fYscaleFactor > 0)
							{
								//	We scale according to both scaling
								//	factors. Aspect ration will not be
								//	preseved.
								iPreserveAspectRatio = 0;
								fFactorX             = pParams->fXscaleFactor;
								fFactorY             = pParams->fYscaleFactor;
								bScale               = true;
							}
							else
							{
								//	Zero is an invalid scaling factor,
								//	do not scale.
								bScale = false;
							}
						}
					}
					else
					{
						//	Zero is an invalid scaling factor,
						//	do not scale.
						bScale = false;
					}
				}

				if(bScale)
				{
					if(ScaleImageIntoDataset(pSrcDataset, fFactorX, fFactorY, iPreserveAspectRatio))
					{
						eRetVal = m_Client.StoreObject(pSrcDataset);
					}
				}
				else
				{
					eRetVal = m_Client.StoreObject(pSrcDataset);
				}
			}
		}
		return eRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	bool C_GETTransaction::ScaleImageIntoDataset(DcmDataset *pDataset,
		                                         const double fScaleFactorX,
											     const double fScaleFactorY,
												 int iPreserveAspectRatio)
	{
		bool bRetVal             = false;
		DicomImage *pImage       = NULL;
		DicomImage *pScaledImage = NULL;
		E_TransferSyntax xfer    = EXS_Unknown;

		xfer = pDataset->getOriginalXfer();
		DcmXfer XferSyn(xfer);

		pDataset->chooseRepresentation(xfer, NULL);
		if(pDataset->canWriteXfer(xfer))
		{
			pImage = new DicomImage(pDataset,
				                    xfer,
									CIF_MayDetachPixelData);
			if(pImage)
			{
				pScaledImage = pImage->createScaledImage(fScaleFactorX,
					                                     fScaleFactorY,
														 1,
														 iPreserveAspectRatio);
				if(pScaledImage)
				{
					if(pScaledImage->writeImageToDataset((*pDataset)))
					{
						bRetVal = true;
					}
					delete pScaledImage;
					pScaledImage = NULL;
				}
				delete pImage;
				pImage = NULL;
			}
		}
		return bRetVal;
	}
}
