/////////////////////////////////////////////////////////////////////////////
//	C_ECHOTransaction.cpp
/////////////////////////////////////////////////////////////////////////////

#include "../Utils/DICOMUtil.h"
#include "C_ECHOTransaction.h"

/////////////////////////////////////////////////////////////////////////////
//
namespace imapacs
{
	/////////////////////////////////////////////////////////////////////////
	//
	C_ECHOTransaction::~C_ECHOTransaction()
	{
	}

	/////////////////////////////////////////////////////////////////////////
	//	Pre-processing initializations.
	bool C_ECHOTransaction::Init(T_ASC_Association *pAssociation,
		                         T_DIMSE_Message *pMsg,
								 T_ASC_PresentationContextID PresID,
		                         PacsConfig *pCfg)
	{
		bool bRetVal = false;

		memcpy(&m_CurrentReq, pMsg, sizeof(T_DIMSE_Message));
		memset(&m_CurrentRsp, 0, sizeof(T_DIMSE_Message));

		m_CurrentRsp.CommandField             = DIMSE_C_ECHO_RSP;
		m_CurrentRsp.msg.CEchoRSP.DataSetType = DIMSE_DATASET_NULL;
		m_CurrentRsp.msg.CEchoRSP.DimseStatus = STATUS_Success;	//	Assuming.
		m_CurrentRsp.msg.CEchoRSP.opts        = O_ECHO_AFFECTEDSOPCLASSUID;

		m_pAssociation  = pAssociation;
		m_CurrentPresID = PresID;

		//	For convenience.
		m_pServerLogger   = pCfg->GetServerLogger();
		m_pModalityLogger = pCfg->GetModalityLogger();
		m_pMailer         = pCfg->GetMailer();

		m_pServerLogger->LogMsg("[%s] - C-ECHO request.", pAssociation->params->DULparams.callingAPTitle);
		m_pModalityLogger->LogMsg("C-ECHO request.");
		m_pMailer->AddLine("C-ECHO request.");

		bRetVal = true;

		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//	Process a C-ECHO transaction.
	bool C_ECHOTransaction::ProcessTransaction()
	{
		bool bRetVal        = false;
		OFCondition cond    = EC_Normal;
		DcmDataset *pStatus = NULL;

		/////////////////////////////////////////////////////////////////////
		//	Setup responce.
		m_CurrentRsp.msg.CEchoRSP.MessageIDBeingRespondedTo = m_CurrentReq.msg.CEchoRQ.MessageID;
		strcpy(m_CurrentRsp.msg.CEchoRSP.AffectedSOPClassUID, m_CurrentReq.msg.CEchoRQ.AffectedSOPClassUID);

		cond = DIMSE_sendMessageUsingMemoryData(m_pAssociation,
												m_CurrentPresID,
												&m_CurrentRsp,
												pStatus,
												NULL,
												NULL,
												NULL);
		if(cond.good())
		{
			bRetVal = true;
		}
		else
		{
			m_pModalityLogger->LogMsg("ERROR : %s", cond.text());
			m_pModalityLogger->LogMsg("        Failed to send C-ECHO responce.");
			m_pMailer->AddLine("Failed to send C-ECHO responce.");
		}

		DICOMUtil::ClearDataset(&pStatus);

		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//	Post-processing cleanup.
	void C_ECHOTransaction::Shutdown()
	{
		//	No special cleanup is needed for that transaction.
	}
}
