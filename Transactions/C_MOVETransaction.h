/////////////////////////////////////////////////////////////////////////////
//	C_MOVETransaction.h
/////////////////////////////////////////////////////////////////////////////

#include "ITransaction.h"
#include "../DatabaseServices/CMoveService.h"
#include "../Utils/CDCMClient.h"
#include "../Utils/Str.h"
#include "../Utils/DBGTool.h"
#include "../Utils/TimeUtil.h"

#if !defined(__C_MOVETRANSACTION_H_INCLUDED__)
#define      __C_MOVETRANSACTION_H_INCLUDED__

/////////////////////////////////////////////////////////////////////////////
//
namespace imapacs
{
	/////////////////////////////////////////////////////////////////////////
	//
	class C_MOVETransaction : public ITransaction
	{
	public:
		C_MOVETransaction():m_pAssociation(NULL),
			                m_pServerLogger(NULL),
							m_pModalityLogger(NULL),
							m_pMailer(NULL),
							m_pCfg(NULL),
							m_bRemoveAccents(false),
							m_iTransactionId(0)
		{}
		virtual ~C_MOVETransaction()
		{}

		bool Init(T_ASC_Association *pAssociation,
			      T_DIMSE_Message *pMsg,
				  T_ASC_PresentationContextID PresID,
			      PacsConfig *pCfg);

		bool ProcessTransaction();
		void SetTransactionType(const char *transactionType){strcpy(m_transactionType,transactionType);}
		char *GetTransactionType(){return m_transactionType;}
		void Shutdown();

	protected:

		bool ProcessMoveQuery(DcmDataset *pRequest, int iLevel);
		bool ExtractFindData(DcmDataset *pDataset, FindData *pFindData);
		void PrepareMoveResponce(DIC_US OperationStatus,
							     DIC_US iCompleted,
								 DIC_US iFailed,
								 DIC_US iWarning,
								 DIC_US iRemaining,
								 StringBuffer *pFailedUIDList,
								 DcmDataset **pResult,
								 bool bCancelled);
		DcmDataset *CreateMoveResponceDataset(const char *pFailedUIDList);

		OFCondition SendC_MOVEResponce(DcmDataset *pResult);

		void ClearCandidateList(Queue <MoveResult *> *pList)
		{
			MoveResult *pItem = NULL;

			while(pList->Pop(pItem))
			{
				delete pItem;
				pItem = NULL;
			}
		}

		T_ASC_Association          *m_pAssociation;
		T_DIMSE_Message             m_CurrentReq;
		T_DIMSE_Message             m_CurrentRsp;
		T_ASC_PresentationContextID m_CurrentPresID;
		T_ASC_PresentationContextID m_CurrentDataPresID;

		Log                        *m_pServerLogger;
		Log                        *m_pModalityLogger;
		Mailer                     *m_pMailer;
		PacsConfig                 *m_pCfg;

		CMoveService                m_DataService;
		CDCMClient                  m_Client;

		char						m_transactionType[64];

		bool                        m_bRemoveAccents;

		StringBuffer                m_sCurrentDebugDir;
		int                         m_iTransactionId;	//	For debug purpose.
	};
}

#endif	//	__C_MOVETRANSACTION_H_INCLUDED__
