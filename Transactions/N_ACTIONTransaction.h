/////////////////////////////////////////////////////////////////////////////
//	N_ACTIONTransaction.h
/////////////////////////////////////////////////////////////////////////////

#include "../Utils/log.h"
#include "../Utils/Mailer.h"
#include "../DatabaseServices/NActionService.h"
#include "../PacsConfig.h"
#include "ITransaction.h"

#if !defined(__N_ACTIONTRANSACTION_H_INCLUDED__)
#define      __N_ACTIONTRANSACTION_H_INCLUDED__

/////////////////////////////////////////////////////////////////////////////
//
namespace imapacs
{
	/////////////////////////////////////////////////////////////////////////
	//
	class N_ACTIONTransaction : public ITransaction
	{
	public:
		N_ACTIONTransaction():m_pAssociation(NULL),
			                  m_pClientAssociation(NULL),
			                  m_pServerLogger(NULL),
							  m_pModalityLogger(NULL),
							  m_pMailer(NULL),
							  m_pCfg(NULL),
							  m_CurrentPresID(0),
							  m_CurrentDataPresID(0),
							  m_bRemoveAccents(false),
							  m_iTransactionId(0)
		{}
		virtual ~N_ACTIONTransaction() {}

		bool Init(T_ASC_Association *pAssociation,
	              T_DIMSE_Message *pMsg,
				  T_ASC_PresentationContextID PresID,
				  PacsConfig *pCfg);

		bool ProcessTransaction();
		void SetTransactionType(const char *transactionType){strcpy(m_transactionType,transactionType);}
		char *GetTransactionType(){return m_transactionType;}
		void Shutdown();

	protected:

		bool ProcessStorageCommitment(DcmDataset *pDataset);
		bool ProcessStorageCommitment_N_ACTION(DcmDataset *pDataset, StorageCommitmentActionReq *pReqData);
		bool ProcessStorageCommitment_N_EVENT_REPORT(StorageCommitmentActionReq *pReqData, 
			                                         T_ASC_Association *pAssociation);
		
		bool ExtractStorageCommitmentRequestData(DcmDataset *pDataset, StorageCommitmentActionReq *pReqData);
		DcmDataset *Create_N_EVENT_REPORT_RQ(StorageCommitmentActionReq *pReqData, int *pEventTypeId);

		bool ValidateObjectCommited(const char *pSOPClassUID, const char *pSOPInstanceUID);

		DcmDataset *CreateReferenceSOPClassDataset(Queue <Reference *> *pCommitedList,
												   Queue <Reference *> *pFailedList,
												   const char *pTransactionUID,
												   const char *pCalledAETitle);


		bool ProcessUpdateGPSPS(DcmDataset *pDataset);
		bool ExtractGP_SPS_Update(DcmDataset *pDataset, UpdateSPSData *pGLSPSData);

		T_ASC_Association          *m_pAssociation;
		T_ASC_Association          *m_pClientAssociation;
		T_DIMSE_Message             m_CurrentReq;
		T_DIMSE_Message             m_CurrentRsp;
		T_ASC_PresentationContextID m_CurrentPresID;
		T_ASC_PresentationContextID m_CurrentDataPresID;

		Log                        *m_pServerLogger;
		Log                        *m_pModalityLogger;
		Mailer                     *m_pMailer;
		PacsConfig                 *m_pCfg;

		NActionService              m_DataService;

		char						m_transactionType[64];

		bool                        m_bRemoveAccents;
		StringBuffer                m_sCurrentDebugDir;
		int                         m_iTransactionId;	//	For debug purpose.
	};
}

#endif	//	__N_ACTIONTRANSACTION_H_INCLUDED__
