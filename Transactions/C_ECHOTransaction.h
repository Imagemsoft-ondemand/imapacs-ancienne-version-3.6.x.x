/////////////////////////////////////////////////////////////////////////////
//	C_ECHOTransaction.h
/////////////////////////////////////////////////////////////////////////////

#include "../Utils/log.h"
#include "../Utils/Mailer.h"
#include "../PacsConfig.h"
#include "ITransaction.h"

#if !defined(__C_ECHOTRANSACTION_H_INCLUDED__)
#define      __C_ECHOTRANSACTION_H_INCLUDED__

/////////////////////////////////////////////////////////////////////////////
//
namespace imapacs
{
	class C_ECHOTransaction : public ITransaction
	{
	public:
		C_ECHOTransaction():m_pAssociation(NULL),
			                m_CurrentPresID(0),
							m_pServerLogger(NULL),
							m_pMailer(NULL),
							m_pModalityLogger(NULL)
		{}
		virtual ~C_ECHOTransaction();

		bool Init(T_ASC_Association *pAssociation,
			      T_DIMSE_Message *pMsg,
				  T_ASC_PresentationContextID PresID,
			      PacsConfig *pCfg);

		bool ProcessTransaction();
		void SetTransactionType(const char *transactionType){strcpy(m_transactionType,transactionType);}
		char *GetTransactionType(){return m_transactionType;}
		void Shutdown();

	protected:

		T_ASC_Association          *m_pAssociation;
		T_DIMSE_Message             m_CurrentReq;
		T_ASC_PresentationContextID m_CurrentPresID;
		Log                        *m_pServerLogger;
		Log                        *m_pModalityLogger;
		Mailer                     *m_pMailer;

		T_DIMSE_Message             m_CurrentRsp;
		
		char						m_transactionType[64];
	};
}

#endif	//	__C_ECHOTRANSACTION_H_INCLUDED__
