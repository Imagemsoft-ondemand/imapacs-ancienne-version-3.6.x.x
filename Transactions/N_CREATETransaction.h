/////////////////////////////////////////////////////////////////////////////
//	N_CREATETransaction.h
/////////////////////////////////////////////////////////////////////////////

#include "ITransaction.h"
#include "../Utils/log.h"
#include "../Utils/Mailer.h"
#include "../Utils/DICOMUtil.h"
#include "../PacsConfig.h"
#include "../DatabaseServices/NCreateService.h"

#if !defined(__N_CREATETRANSACTION_H_INCLUDED__)
#define      __N_CREATETRANSACTION_H_INCLUDED__

/////////////////////////////////////////////////////////////////////////////
//
namespace imapacs
{
	/////////////////////////////////////////////////////////////////////////
	//
	class N_CREATETransaction : public ITransaction
	{
	public:
		N_CREATETransaction():m_pAssociation(NULL),
			                  m_CurrentDataPresID(0x00),
							  m_CurrentPresID(0x00),
							  m_pServerLogger(NULL),
							  m_pModalityLogger(NULL),
							  m_pMailer(NULL),
							  m_pCfg(NULL),
							  m_bRemoveAccents(false),
							  m_iTransactionId(0)
		{}
		virtual ~N_CREATETransaction()
		{}

		bool Init(T_ASC_Association *pAssociation,
			      T_DIMSE_Message *pMsg,
				  T_ASC_PresentationContextID PresID,
			      PacsConfig *pCfg);

		bool ProcessTransaction();
		void SetTransactionType(const char *transactionType){strcpy(m_transactionType,transactionType);}
		char *GetTransactionType(){return m_transactionType;}
		void Shutdown();

	protected:

		/////////////////////////////////////////////////////////////////////
		//	GP-PPS creation related methods.
		bool GPCreatePPS();
		bool ExtractCreatePPSData(DcmDataset *pReqDataset, CreatePPSData *pReqData);

		/////////////////////////////////////////////////////////////////////
		//	Methods related to the creation of MPPS.
		bool MPPSCreate();
		bool ExtractCreateMPPSData(DcmDataset *pReqDataset, MPPSData *pReqData);

		T_ASC_Association          *m_pAssociation;
		T_DIMSE_Message             m_CurrentReq;
		T_DIMSE_Message             m_CurrentRsp;
		T_ASC_PresentationContextID m_CurrentDataPresID;
		T_ASC_PresentationContextID m_CurrentPresID;
		Log                        *m_pServerLogger;
		Log                        *m_pModalityLogger;
		Mailer                     *m_pMailer;
		PacsConfig                 *m_pCfg;

		NCreateService              m_DataService;

		char						m_transactionType[64];

		bool                        m_bRemoveAccents;

		StringBuffer                m_sCurrentDebugDir;
		int                         m_iTransactionId;
	};
}

#endif	//	__N_CREATETRANSACTION_H_INCLUDED__
