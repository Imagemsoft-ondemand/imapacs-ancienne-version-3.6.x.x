/////////////////////////////////////////////////////////////////////////////
//	TransactionFactory.h
/////////////////////////////////////////////////////////////////////////////

#include "../PacsConfig.h"
#include "../Utils/DicomHeaders.h"

#include "ITransaction.h"
#include "C_ECHOTransaction.h"
#include "N_ACTIONTransaction.h"
#include "C_STORETransaction.h"
#include "C_FINDTransaction.h"
#include "C_GETTransaction.h"
#include "C_MOVETransaction.h"
#include "N_SETTransaction.h"
#include "N_CREATETransaction.h"

#if !defined(__TRANSACTIONFACTORY_H_INCLUDED__)
#define      __TRANSACTIONFACTORY_H_INCLUDED__

/////////////////////////////////////////////////////////////////////////////
//
namespace imapacs
{
	/////////////////////////////////////////////////////////////////////////
	//
	class TransactionFactory
	{
	public:

		/////////////////////////////////////////////////////////////////////
		//
		static ITransaction *CreateTransaction(T_ASC_Association *pAssociation,
			                                   T_DIMSE_Message *pMsg, 
			                                   T_ASC_PresentationContextID PresID,
											   PacsConfig *pCfg)
		{
			char sTransactionCode[21]  = {0};
			ITransaction *pTransaction = NULL;

			switch(pMsg->CommandField)
			{
				case DIMSE_C_STORE_RQ:
				{
					pTransaction = new C_STORETransaction;
					pTransaction->SetTransactionType("C-STORE");
				}
				break;

				case DIMSE_C_GET_RQ:
				{
					pTransaction = new C_GETTransaction;
					pTransaction->SetTransactionType("C-GET");
				}
				break;

				case DIMSE_C_FIND_RQ:
				{
					pTransaction = new C_FINDTransaction;
					pTransaction->SetTransactionType("C-FIND");
				}
				break;

				case DIMSE_C_MOVE_RQ:
				{
					pTransaction = new C_MOVETransaction;
					pTransaction->SetTransactionType("C-MOVE");
				}
				break;

				case DIMSE_C_ECHO_RQ:
				{
					pTransaction = new C_ECHOTransaction;
					pTransaction->SetTransactionType("C-ECHO");
				}
				break;

				case DIMSE_N_SET_RQ:
				{
					pTransaction = new N_SETTransaction;
					pTransaction->SetTransactionType("N-SET");
				}
				break;

				case DIMSE_N_ACTION_RQ:
				{
					pTransaction = new N_ACTIONTransaction;
					pTransaction->SetTransactionType("N-ACTION");
				}
				break;

				case DIMSE_N_CREATE_RQ:
				{
					pTransaction = new N_CREATETransaction;
					pTransaction->SetTransactionType("N-CREATE");
				}
				break;

				default:	//	Unknown or unsupported DICOM command.
				{
					sprintf(sTransactionCode, "command : %0000x", pMsg->CommandField);

					pCfg->GetServerLogger()->LogMsg("ERROR : Unknown or unsupported DICOM command.");
					pCfg->GetServerLogger()->LogMsg("        %s", sTransactionCode);
				}
			}

			if(pTransaction)
			{
				if(!pTransaction->Init(pAssociation,
					                   pMsg,
									   PresID,
									   pCfg))
				{
					delete pTransaction;
					pTransaction = NULL;
				}
			}
			else
			{
				pCfg->GetServerLogger()->LogMsg("ERROR : Failed to allocate transaction object.");
			}
			return pTransaction;
		}
	};
}

#endif	//	__TRANSACTIONFACTORY_H_INCLUDED__
