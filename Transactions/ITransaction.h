/////////////////////////////////////////////////////////////////////////////
//	ITransaction.h
/////////////////////////////////////////////////////////////////////////////

#include "../Utils/DicomHeaders.h"
#include "../Utils/PacsCommon.h"
#include "../DataStructures.h"

#if !defined(__ITRANSACTION_H_INCLUDED__)
#define      __ITRANSACTION_H_INCLUDED__

/////////////////////////////////////////////////////////////////////////////
//
namespace imapacs
{
	class PacsConfig;

	/////////////////////////////////////////////////////////////////////////
	//
	class ITransaction
	{
	public:
		virtual bool Init(T_ASC_Association *pAssociation,
			              T_DIMSE_Message *pMsg,
						  T_ASC_PresentationContextID PresID,
			              PacsConfig *pCfg)                                 = 0;

		virtual bool ProcessTransaction()                                   = 0;
		virtual void SetTransactionType(const char *transactionType)		= 0;
		virtual char *GetTransactionType()									= 0;
		virtual void Shutdown()                                             = 0;
	};
}

#endif	//	__ITRANSACTION_H_INCLUDED__
