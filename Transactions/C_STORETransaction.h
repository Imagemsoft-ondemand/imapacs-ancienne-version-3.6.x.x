/////////////////////////////////////////////////////////////////////////////
//	C_STORETransaction.h
/////////////////////////////////////////////////////////////////////////////

#include "../Utils/log.h"
#include "../Utils/Mailer.h"
#include "../DICOMCompressor.h"
#include "../DatabaseServices/CStoreService.h"
#include "../PacsConfig.h"
#include "ITransaction.h"

#if !defined(__C_STORETRANSACTION_H_INCLUDED__)
#define      __C_STORETRANSACTION_H_INCLUDED__

/////////////////////////////////////////////////////////////////////////////
//
namespace imapacs
{
	/////////////////////////////////////////////////////////////////////////
	//
	class C_STORETransaction : public ITransaction
	{
	public:
		C_STORETransaction():m_pAssociation(NULL),
			                 m_pServerLogger(NULL),
							 m_pModalityLogger(NULL),
							 m_pMailer(NULL),
							 m_pCfg(NULL),
							 m_bRemoveAccents(false),
							 m_iTransactionId(0)

		{memset(m_transactionType, 0, sizeof(m_transactionType));}
		virtual ~C_STORETransaction()
		{}

		bool Init(T_ASC_Association *pAssociation,
			      T_DIMSE_Message *pMsg,
				  T_ASC_PresentationContextID PresID,
			      PacsConfig *pCfg);

		bool ProcessTransaction();
		void SetTransactionType(const char *transactionType){strcpy(m_transactionType,transactionType);}
		char *GetTransactionType(){return m_transactionType;}
		void Shutdown();

	protected:

		bool ExtractPatientInformation(DcmDataset *pDataset, PatientData *pInfo);
		bool ExtractStudyInformation(DcmDataset *pDataset, StudyData *pInfo);
		bool ExtractObjectInformation(DcmDataset *pDataset, ObjectData *pInfo);

		bool SaveObject(DcmDataset *pDataset, 
			            StudyData *pStudy, 
						ObjectData *pObject, 
						const char *pBasePath);

		bool SaveUncompressed(const char *pFile, DcmDataset *pDataset, E_TransferSyntax eTrnSyn);
		bool SaveCompressed(const char *pFile, DcmDataset *pDataset, int iCompression);
		bool SaveAsIs(const char *pDestinationFile, DcmDataset *pDataset);

		DcmDataset *ScaleImage(DcmDataset *pImageDataset, double fScaleFactor);

		T_ASC_Association          *m_pAssociation;
		T_DIMSE_Message             m_CurrentReq;
		T_DIMSE_Message             m_CurrentRsp;
		T_ASC_PresentationContextID m_CurrentPresID;
		T_ASC_PresentationContextID m_CurrentDataPresID;

		Log                        *m_pServerLogger;
		Log                        *m_pModalityLogger;
		Mailer                     *m_pMailer;
		PacsConfig                 *m_pCfg;

		CStoreService               m_DataService;
		DICOMCompressor             m_Compressor;

		char						m_transactionType[64];

		bool                        m_bRemoveAccents;

		StringBuffer                m_sCurrentDebugDir;
		int                         m_iTransactionId;
	};
}

#endif	//	__C_STORETRANSACTION_H_INCLUDED__
