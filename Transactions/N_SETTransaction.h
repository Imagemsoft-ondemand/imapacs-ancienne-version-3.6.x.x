/////////////////////////////////////////////////////////////////////////////
//	N_SETTransaction.h
/////////////////////////////////////////////////////////////////////////////

#include "ITransaction.h"
#include "../Utils/log.h"
#include "../Utils/Mailer.h"
#include "../Utils/DICOMUtil.h"
#include "../Utils/TimeUtil.h"
#include "../Utils/DBGTool.h"
#include "../Utils/Str.h"
#include "../PacsConfig.h"
#include "../DatabaseServices/NSetService.h"

#if !defined(__N_SETTRANSACTION_H_INCLUDED__)
#define      __N_SETTRANSACTION_H_INCLUDED__

/////////////////////////////////////////////////////////////////////////////
//
namespace imapacs
{
	/////////////////////////////////////////////////////////////////////////
	//
	class N_SETTransaction : public ITransaction
	{
	public:
		N_SETTransaction():m_pAssociation(NULL),
			               m_CurrentDataPresID(0x00),
						   m_CurrentPresID(0x00),
						   m_pServerLogger(NULL),
						   m_pModalityLogger(NULL),
						   m_pMailer(NULL),
						   m_pCfg(NULL),
						   m_bRemoveAccents(false),
						   m_iTransactionId(0)
		{}
		virtual ~N_SETTransaction()
		{}

		bool Init(T_ASC_Association *pAssociation,
			      T_DIMSE_Message *pMsg,
				  T_ASC_PresentationContextID PresID,
			      PacsConfig *pCfg);

		bool ProcessTransaction();
		void SetTransactionType(const char *transactionType){strcpy(m_transactionType,transactionType);}
		char *GetTransactionType(){return m_transactionType;}
		void Shutdown();

	protected:

		bool ProcessUpdatePPS();
		bool ExtractUpdatePPSData(DcmDataset *pDataset, CreatePPSData *pReqData);

		bool ProcessUpdateMPPS();
		bool ExtractUpdateMPPSData(DcmDataset *pDataset, MPPSData *pReqData);

		T_ASC_Association          *m_pAssociation;
		T_DIMSE_Message             m_CurrentReq;
		T_DIMSE_Message             m_CurrentRsp;
		T_ASC_PresentationContextID m_CurrentDataPresID;
		T_ASC_PresentationContextID m_CurrentPresID;
		Log                        *m_pServerLogger;
		Log                        *m_pModalityLogger;
		Mailer                     *m_pMailer;
		PacsConfig                 *m_pCfg;

		NSetService                 m_DataService;

		char						m_transactionType[64];

		bool                        m_bRemoveAccents;

		StringBuffer                m_sCurrentDebugDir;
		int                         m_iTransactionId;
	};
}

#endif	//	__N_SETTRANSACTION_H_INCLUDED__
