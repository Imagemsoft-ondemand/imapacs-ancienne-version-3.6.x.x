/////////////////////////////////////////////////////////////////////////////
//	N_CREATETransaction.cpp
/////////////////////////////////////////////////////////////////////////////

#include "N_CREATETransaction.h"
#include "../Utils/TimeUtil.h"

/////////////////////////////////////////////////////////////////////////////
//
namespace imapacs
{
	/////////////////////////////////////////////////////////////////////////
	//
	bool N_CREATETransaction::Init(T_ASC_Association *pAssociation,
			                       T_DIMSE_Message *pMsg,
				                   T_ASC_PresentationContextID PresID,
			                       PacsConfig *pCfg)
	{
		bool bRetVal = false;

		memcpy(&m_CurrentReq, pMsg, sizeof(T_DIMSE_Message));	//	We use our own copy of the command.
		memset(&m_CurrentRsp, 0, sizeof(T_DIMSE_Message));

		m_CurrentRsp.CommandField                             = DIMSE_N_CREATE_RSP;
		m_CurrentRsp.msg.NCreateRSP.DataSetType               = DIMSE_DATASET_NULL;	//	Assuming.
		m_CurrentRsp.msg.NCreateRSP.DimseStatus               = STATUS_Success;		//	Assuming.
		m_CurrentRsp.msg.NCreateRSP.MessageIDBeingRespondedTo = m_CurrentReq.msg.NCreateRQ.MessageID;

		strcpy(m_CurrentRsp.msg.NCreateRSP.AffectedSOPClassUID, m_CurrentReq.msg.NCreateRQ.AffectedSOPClassUID);
		strcpy(m_CurrentRsp.msg.NCreateRSP.AffectedSOPInstanceUID, m_CurrentReq.msg.NCreateRQ.AffectedSOPInstanceUID);

		m_pAssociation  = pAssociation;
		m_CurrentPresID = PresID;

		m_pCfg = pCfg;

		//	For convenience.
		m_pServerLogger   = pCfg->GetServerLogger();
		m_pModalityLogger = pCfg->GetModalityLogger();
		m_pMailer         = pCfg->GetMailer();

		m_pServerLogger->LogMsg("[%s] - N-CREATE request.", pAssociation->params->DULparams.callingAPTitle);
		m_pModalityLogger->LogMsg("N-CREATE request.");
		m_pMailer->AddLine("N-CREATE request.");

		if(pCfg->GetModalityDebug())
		{
			//	Generate a transaction id to group
			//	transaction related files.
			srand((unsigned int)time(NULL));
			m_iTransactionId = rand();

			m_sCurrentDebugDir.Format("%s%s", DEBUG_DIRECTORY, pAssociation->params->DULparams.callingAPTitle);
			os::MkDir((char *)m_sCurrentDebugDir);

			m_DataService.SetCurrentDebugParams((char *)m_sCurrentDebugDir, m_iTransactionId);
		}
		m_DataService.SetConfig(pCfg);
		bRetVal = true;

		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	bool N_CREATETransaction::ProcessTransaction()
	{
		bool bRetVal     = false;
		OFCondition cond = EC_Normal;
		DBGTool dbg;

		if(strcmp(m_CurrentReq.msg.NCreateRQ.AffectedSOPClassUID, UID_RETIRED_GeneralPurposePerformedProcedureStepSOPClass) == 0)
		{
			m_pModalityLogger->LogMsg("GP-PPS creation.");
			m_pMailer->AddLine("GP-PPS creation.");

			bRetVal = GPCreatePPS();
		}
		else if(strcmp(m_CurrentReq.msg.NCreateRQ.AffectedSOPClassUID, UID_ModalityPerformedProcedureStepSOPClass) == 0)
		{
			m_pModalityLogger->LogMsg("MPPS creation (IN PROGRESS)");
			m_pMailer->AddLine("MPPS creation (IN PROGRESS)");

			bRetVal = MPPSCreate();
		}
		else
		{
			//	Unsupported SOP class.
			m_pModalityLogger->LogMsg("ERROR : Unsupported SOP class [%s]", m_CurrentReq.msg.NCreateRQ.AffectedSOPClassUID);
			m_pMailer->AddLine("Unsupported SOP class [%s]", m_CurrentReq.msg.NCreateRQ.AffectedSOPClassUID);

			m_pModalityLogger->LogMsg("ERROR : Unsupported SOP class UID : [%s].", m_CurrentReq.msg.NSetRQ.RequestedSOPInstanceUID);
			m_pMailer->AddLine("ERROR : Unsupported SOP class UID : [%s].", m_CurrentReq.msg.NSetRQ.RequestedSOPInstanceUID);

			m_CurrentRsp.msg.NCreateRSP.DimseStatus = STATUS_N_SOPClassNotSupported;

			cond = DIMSE_sendMessageUsingMemoryData(m_pAssociation,
													m_CurrentPresID,
													&m_CurrentRsp,
													NULL, 
													NULL, 
													NULL, 
													NULL, 
													NULL);
			if(cond.bad())
			{
				m_pModalityLogger->LogMsg("ERROR : %s", cond.text());
				m_pModalityLogger->LogMsg("        Failed to send responce to client.");

				m_pMailer->AddLine("ERROR : %s", cond.text());
			}
		}
		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	void N_CREATETransaction::Shutdown()
	{
		m_DataService.Dispose();
		m_sCurrentDebugDir.Clear();
	}

	/////////////////////////////////////////////////////////////////////////
	//
	bool N_CREATETransaction::GPCreatePPS()
	{
		bool bRetVal         = false;
		char sDateTime[33]   = {0};
		OFCondition cond     = EC_Normal;
		DcmDataset *pResult  = NULL;
		DcmDataset *pRequest = NULL;
		CreatePPSData ReqData;
		StringBuffer dump;
		DBGTool dbg;

		if(m_pCfg->GetProfiling())
			dbg.StartProfiling(m_pServerLogger, "Receiving dataset.");

		/////////////////////////////////////////////////////////////////////
		//	Read request dataset.
		cond = DIMSE_receiveDataSetInMemory(m_pAssociation,
											DIMSE_BLOCKING,
											0,
											&m_CurrentDataPresID,
											&pRequest,
											NULL,
											NULL);
		if(m_pCfg->GetProfiling())
			dbg.EndProfiling(m_pServerLogger, NULL,	"Dataset received.");

		if(cond.good())
		{
			//	Dump the dataset if debug mode is
			//	on for that AE title.
			if(m_pCfg->GetModalityDebug())
			{
				Time::GetDate(sDateTime);		//	Get the date for the sub-directory.
				dump.Format("%s/%s_GPCreatePPS_%d/", (char *)m_sCurrentDebugDir, sDateTime, m_iTransactionId);

				os::MkDir((char *)dump);

				Time::GetTimeOfDay(sDateTime);	//	Get the time for the file.
				dump += "Query_";
				dump += sDateTime;
				dump += ".dcm";

				cond = pRequest->saveFile((char *)dump, EXS_LittleEndianExplicit);
				if(cond.bad())
				{
					m_pModalityLogger->LogMsg("ERROR %s", cond.text());
					m_pModalityLogger->LogMsg("      Failed to save debug dataset.");
				}
				dump.Clear();
			}

			if(m_CurrentDataPresID == m_CurrentPresID)
			{
				if(ExtractCreatePPSData(pRequest, &ReqData))
				{
					if(!m_DataService.PPSExists(m_CurrentReq.msg.NCreateRQ.AffectedSOPInstanceUID))
					{
						if(m_DataService.CreatePPSEntry(&ReqData))
						{
							m_CurrentRsp.msg.NCreateRSP.DimseStatus = STATUS_Success;
							bRetVal = true;
						}
						else
						{
							m_CurrentRsp.msg.NCreateRSP.DimseStatus = STATUS_N_AttributeListError;

							m_pModalityLogger->LogMsg("Failed to create PPS into database.");
							m_pMailer->AddLine("Failed to create PPS into database.");
						}
					}
					else
					{
						m_CurrentRsp.msg.NCreateRSP.DimseStatus = STATUS_N_DuplicateSOPInstance;

						m_pModalityLogger->LogMsg("PPS UID already exists [%s].", m_CurrentReq.msg.NCreateRQ.AffectedSOPInstanceUID);
						m_pMailer->AddLine("PPS UID already exists [%s].", m_CurrentReq.msg.NCreateRQ.AffectedSOPInstanceUID);
					}
				}
				else
				{
					m_CurrentRsp.msg.NCreateRSP.DimseStatus = STATUS_N_ProcessingFailure;

					m_pModalityLogger->LogMsg("        Unable to extract data from request dataset.");
					m_pMailer->AddLine("Unable to extract data from request dataset.");
				}
			}
			else
			{
				m_CurrentRsp.msg.NCreateRSP.DimseStatus = STATUS_N_ResourceLimitation;

				m_pModalityLogger->LogMsg("ERROR : Request and dataset presentation id mismatch.");
				m_pMailer->AddLine("ERROR : Request and dataset presentation id mismatch.");
			}
		}
		else
		{
			m_CurrentRsp.msg.NCreateRSP.DimseStatus = STATUS_N_ResourceLimitation;

			m_pModalityLogger->LogMsg("ERROR : %s", cond.text());
			m_pModalityLogger->LogMsg("        Could not receive N-CREATE dataset.");

			m_pMailer->AddLine("%s", cond.text());
			m_pMailer->AddLine("Could not receive N-CREATE dataset.");
		}

		if(m_pCfg->GetProfiling())
			dbg.StartProfiling(m_pServerLogger, "Sending responce.");

		cond = DIMSE_sendMessageUsingMemoryData(m_pAssociation,
												m_CurrentPresID,
												&m_CurrentRsp,
												NULL,
												NULL,
												NULL,
												NULL,
												NULL);
		if(m_pCfg->GetProfiling())
			dbg.EndProfiling(m_pServerLogger, NULL, "Responce sent.");

		if(cond.bad())
		{
			m_pModalityLogger->LogMsg("WARNING : Failed to send responce to client.");
			m_pModalityLogger->LogMsg("          %s", cond.text());

			m_pMailer->AddLine("WARNING : Failed to send responce to client.");
			m_pMailer->AddLine("          %s", cond.text());
		}
		DICOMUtil::ClearDataset(&pRequest);
		DICOMUtil::ClearDataset(&pResult);

		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	bool N_CREATETransaction::ExtractCreatePPSData(DcmDataset *pReqDataset, CreatePPSData *pReqData)
	{
		bool bRetVal                = false;
		DcmSequenceOfItems *pSeq    = NULL;
		DcmSequenceOfItems *pSubSeq = NULL;
		DcmItem *pItem              = NULL;
		DcmItem *pSubItem           = NULL;
		OFCondition cond            = EC_Normal;
		DBGTool dbg;

		if(pReqDataset)
		{
			if(pReqData)
			{
				if(m_pCfg->GetProfiling())
					dbg.StartProfiling(m_pServerLogger, "Extracting dataset.");

				strcpy(pReqData->sPPSInstanceUID, m_CurrentReq.msg.NCreateRQ.AffectedSOPInstanceUID);

				/////////////////////////////////////////////////////////////
				//	Patient data.
				DICOMUtil::GetTagFromDataset(pReqDataset, DCM_PatientName, pReqData->sPatientName);
				DICOMUtil::GetTagFromDataset(pReqDataset, DCM_PatientID, pReqData->sPatientId);
				DICOMUtil::GetTagFromDataset(pReqDataset, DCM_PatientBirthDate, pReqData->sPatientBirthDate);
				DICOMUtil::GetTagFromDataset(pReqDataset, DCM_PatientSex, pReqData->sPatientSex);

				/////////////////////////////////////////////////////////////
				//	PPS data.
				DICOMUtil::GetTagFromDataset(pReqDataset, DCM_PerformedProcedureStepID, pReqData->sPPSId);
				DICOMUtil::GetTagFromDataset(pReqDataset, DCM_PerformedProcedureStepStartDate, pReqData->sPPSStartDate);
				DICOMUtil::GetTagFromDataset(pReqDataset, DCM_PerformedProcedureStepStartTime, pReqData->sPPSStartTime);
				DICOMUtil::GetTagFromDataset(pReqDataset, DCM_RETIRED_GeneralPurposePerformedProcedureStepStatus, pReqData->sPPSStatus);

				/////////////////////////////////////////////////////////////
				//	Referenced GP-SPS.
				cond = pReqDataset->findAndGetSequence(DCM_RETIRED_ReferencedGeneralPurposeScheduledProcedureStepSequence, pSeq);
				if(cond.good() && pSeq)
				{
					pItem = pSeq->getItem(0);
					if(pItem)
					{
						DICOMUtil::GetTagFromDataset(pItem, DCM_ReferencedSOPClassUID, pReqData->sRefSPSSOPClassUID);
						DICOMUtil::GetTagFromDataset(pItem, DCM_ReferencedSOPInstanceUID, pReqData->sRefSPSSOPInstanceUID);
						DICOMUtil::GetTagFromDataset(pItem, DCM_RETIRED_ReferencedGeneralPurposeScheduledProcedureStepTransactionUID, pReqData->sTransactionUID);
					}
				}

				/////////////////////////////////////////////////////////////
				//	Performer sequence (the id and name of the physycian
				//	creating the PPS.
				pSeq = NULL;
				cond = pReqDataset->findAndGetSequence(DCM_ActualHumanPerformersSequence, pSeq);
				if(cond.good() && pSeq)
				{
					pItem = pSeq->getItem(0);
					if(pItem)
					{
						cond = pItem->findAndGetSequence(DCM_HumanPerformerCodeSequence, pSubSeq);
						if(cond.good() && pSubSeq)
						{
							pSubItem = pSubSeq->getItem(0);
							if(pSubItem)
							{
								DICOMUtil::GetTagFromDataset(pSubItem, DCM_CodeValue, pReqData->sPerformerId);
//								DICOMUtil::GetTagFromDataset(pSubItem, DCM_CodingSchemeDesignator, );
//								DICOMUtil::GetTagFromDataset(pSubItem, DCM_CodeMeaning, );
							}
						}
						DICOMUtil::GetTagFromDataset(pItem, DCM_HumanPerformerName, pReqData->sPerformerName);
						DICOMUtil::GetTagFromDataset(pItem, DCM_HumanPerformerOrganization, pReqData->sPerformerOrganization);
					}
				}

				/////////////////////////////////////////////////////////////
				//	Performed station (basically the AE title).
				pSeq = NULL;
				cond = pReqDataset->findAndGetSequence(DCM_PerformedStationNameCodeSequence, pSeq);
				if(cond.good() && pSeq)
				{
					pItem = pSeq->getItem(0);
					if(pItem)
					{
						DICOMUtil::GetTagFromDataset(pItem, DCM_CodeValue, pReqData->sPerformedStationName);
//						DICOMUtil::GetTagFromDataset(pItem, DCM_CodingSchemeDesignator, );
//						DICOMUtil::GetTagFromDataset(pItem, DCM_CodeMeaning, );
					}
				}

				/////////////////////////////////////////////////////////////
				//	Workitem code sequence.
				pSeq = NULL;
				cond = pReqDataset->findAndGetSequence(DCM_PerformedWorkitemCodeSequence, pSeq);
				if(cond.good() && pSeq)
				{
					pItem = pSeq->getItem(0);
					if(pItem)
					{
						DICOMUtil::GetTagFromDataset(pItem, DCM_CodeValue, pReqData->sWorkitemCode);
//						DICOMUtil::GetTagFromDataset(pItem, DCM_CodingSchemeDesignator, );
//						DICOMUtil::GetTagFromDataset(pItem, DCM_CodeMeaning, );
					}
				}
				bRetVal = true;
			}
			else
			{
				m_pModalityLogger->LogMsg("ERROR : NULL request data passed to N_CREATETransaction::ExtractCreatePPSData.");
				m_pMailer->AddLine("ERROR : NULL request data passed to N_CREATETransaction::ExtractCreatePPSData.");
			}

			if(m_pCfg->GetProfiling())
				dbg.EndProfiling(m_pServerLogger, NULL, "Dataset extracted.");
		}
		else
		{
			m_pModalityLogger->LogMsg("ERROR : NULL dataset passed to N_CREATETransaction::ExtractCreatePPSData.");
			m_pMailer->AddLine("ERROR : NULL dataset passed to N_CREATETransaction::ExtractCreatePPSData.");
		}
		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//	Process the creation of a modality performed procedure step.
	bool N_CREATETransaction::MPPSCreate()
	{
		bool bRetVal         = false;
		OFCondition cond     = EC_Normal;
		DcmDataset *pRequest = NULL;
		DcmDataset *pResult  = NULL;
		char sDateTime[33]   = {0};
		MPPSData ReqData;
		StringBuffer dump;
		DBGTool dbg;

		if(m_pCfg->GetProfiling())
			dbg.StartProfiling(m_pServerLogger, "Receicing dataset.");

		/////////////////////////////////////////////////////////////////////
		//	Read request dataset.
		cond = DIMSE_receiveDataSetInMemory(m_pAssociation,
											DIMSE_BLOCKING,
											0,
											&m_CurrentDataPresID,
											&pRequest,
											NULL,
											NULL);

		if(m_pCfg->GetProfiling())
			dbg.EndProfiling(m_pServerLogger, NULL, "Dataset received.");

		if(m_pCfg->GetModalityDebug())
		{
			Time::GetDate(sDateTime);		//	Get the date for the sub-directory.
			dump.Format("%s/%s_GPCreateMPPS_%d/", (char *)m_sCurrentDebugDir, sDateTime, m_iTransactionId);

			os::MkDir((char *)dump);

			Time::GetTimeOfDay(sDateTime);	//	Get the time for the file.
			dump += "Query_";
			dump += sDateTime;
			dump += ".dcm";

			cond = pRequest->saveFile((char *)dump, EXS_LittleEndianExplicit);
			if(cond.bad())
			{
				m_pModalityLogger->LogMsg("ERROR %s", cond.text());
				m_pModalityLogger->LogMsg("      Failed to save debug dataset.");
			}
			dump.Clear();
		}

		if(cond.good())
		{
			if(m_CurrentDataPresID == m_CurrentPresID)
			{
				if(ExtractCreateMPPSData(pRequest, &ReqData))
				{
					if(!m_DataService.MPPSExists(ReqData.sTransactionUID))
					{
						if(m_DataService.CreateMPPSEntry(&ReqData))
						{
							m_CurrentRsp.msg.NCreateRSP.DimseStatus = STATUS_Success;
							bRetVal = true;
						}
						else
						{
							m_CurrentRsp.msg.NCreateRSP.DimseStatus = STATUS_N_AttributeListError;

							m_pModalityLogger->LogMsg("Failed to create PPS into database.");
							m_pMailer->AddLine("Failed to create PPS into database.");
						}
					}
					else
					{
						m_CurrentRsp.msg.NCreateRSP.DimseStatus = STATUS_N_DuplicateSOPInstance;

						m_pModalityLogger->LogMsg("MPPS UID already exists [%s].", ReqData.sTransactionUID);
						m_pMailer->AddLine("MPPS UID already exists [%s].", ReqData.sTransactionUID);
					}
				}
				else
				{
					m_CurrentRsp.msg.NCreateRSP.DimseStatus = STATUS_N_ProcessingFailure;

					m_pModalityLogger->LogMsg("        Unable to extract data from request dataset.");
					m_pMailer->AddLine("Unable to extract data from request dataset.");
				}
			}
			else
			{
				m_CurrentRsp.msg.NCreateRSP.DimseStatus = STATUS_N_ResourceLimitation;

				m_pModalityLogger->LogMsg("ERROR : Request and dataset presentation id mismatch.");
				m_pMailer->AddLine("ERROR : Request and dataset presentation id mismatch.");
			}
		}
		else
		{
			m_CurrentRsp.msg.NCreateRSP.DimseStatus = STATUS_N_ResourceLimitation;

			m_pModalityLogger->LogMsg("ERROR : %s", cond.text());
			m_pModalityLogger->LogMsg("        Failed to receive request dataset.");
		}

		if(m_pCfg->GetProfiling())
			dbg.StartProfiling(m_pServerLogger, "Sending responce.");

		cond = DIMSE_sendMessageUsingMemoryData(m_pAssociation,
												m_CurrentPresID,
												&m_CurrentRsp,
												NULL,
												NULL,
												NULL,
												NULL,
												NULL);
		if(m_pCfg->GetProfiling())
			dbg.EndProfiling(m_pServerLogger, NULL, "Responce sent.");

		if(m_pCfg->GetModalityDebug())
		{
			Time::GetDate(sDateTime);		//	Get the date for the sub-directory.
			dump.Format("%s/%s_GPCreateMPPS_%d/", (char *)m_sCurrentDebugDir, sDateTime, m_iTransactionId);

			os::MkDir((char *)dump);

			Time::GetTimeOfDay(sDateTime);	//	Get the time for the file.
			dump += "Rsp_";
			dump += sDateTime;
			dump += ".dcm";

			cond = pRequest->saveFile((char *)dump, EXS_LittleEndianExplicit);
			if(cond.bad())
			{
				m_pModalityLogger->LogMsg("ERROR %s", cond.text());
				m_pModalityLogger->LogMsg("      Failed to save debug dataset.");
			}
			dump.Clear();
		}

		if(cond.bad())
		{
			m_pModalityLogger->LogMsg("WARNING : Failed to send responce to client.");
			m_pModalityLogger->LogMsg("          %s", cond.text());

			m_pMailer->AddLine("WARNING : Failed to send responce to client.");
			m_pMailer->AddLine("          %s", cond.text());
		}
		DICOMUtil::ClearDataset(&pRequest);
		DICOMUtil::ClearDataset(&pResult);

		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	bool N_CREATETransaction::ExtractCreateMPPSData(DcmDataset *pReqDataset, MPPSData *pReqData)
	{
		bool bRetVal                = false;
		OFCondition cond            = EC_Normal;
		DcmItem *pItem              = NULL;
		DcmSequenceOfItems *pSeq    = NULL;
		ScheduledStepAttribute SchStepSttr;
		DBGTool dbg;

		if(m_pCfg->GetProfiling())
			dbg.StartProfiling(m_pServerLogger, "Extracting request dataset.");

		if(pReqData && pReqData)
		{
			strcpy(pReqData->sTransactionUID, m_CurrentReq.msg.NCreateRQ.AffectedSOPInstanceUID);

			DICOMUtil::GetTagFromDataset(pReqDataset, DCM_PatientName, pReqData->sPatientName);
			DICOMUtil::GetTagFromDataset(pReqDataset, DCM_PatientID, pReqData->sPatientId);
			DICOMUtil::GetTagFromDataset(pReqDataset, DCM_PerformedStationAETitle, pReqData->sPerformedStationAeTitle);
			DICOMUtil::GetTagFromDataset(pReqDataset, DCM_PerformedStationName, pReqData->sPerformedStationName);
			DICOMUtil::GetTagFromDataset(pReqDataset, DCM_PerformedLocation, pReqData->sPerformerLocation);
			DICOMUtil::GetTagFromDataset(pReqDataset, DCM_PerformedProcedureStepStartDate, pReqData->sPPSStartDate);
			DICOMUtil::GetTagFromDataset(pReqDataset, DCM_PerformedProcedureStepStartTime, pReqData->sPPSStartTime);
			DICOMUtil::GetTagFromDataset(pReqDataset, DCM_PerformedProcedureStepEndDate, pReqData->sPPSEndDate);
			DICOMUtil::GetTagFromDataset(pReqDataset, DCM_PerformedProcedureStepEndTime, pReqData->sPPSEndTime);
			DICOMUtil::GetTagFromDataset(pReqDataset, DCM_PerformedProcedureStepID, pReqData->sPPSId);
			DICOMUtil::GetTagFromDataset(pReqDataset, DCM_PerformedProcedureStepDescription, pReqData->sPPSDescription);
			DICOMUtil::GetTagFromDataset(pReqDataset, DCM_CommentsOnThePerformedProcedureStep, pReqData->sPPSComment);
			DICOMUtil::GetTagFromDataset(pReqDataset, DCM_Modality, pReqData->sModality);
			DICOMUtil::GetTagFromDataset(pReqDataset, DCM_StudyID, pReqData->sStudyId);
			DICOMUtil::GetTagFromDataset(pReqDataset, DCM_PerformedProcedureStepStatus, pReqData->sPPSStatus);

			cond = pReqDataset->findAndGetSequence(DCM_ScheduledStepAttributesSequence, pSeq);
			if(cond.good() && pSeq)
			{
				for(int i = 0; i < (int)pSeq->card(); ++i)
				{
					pItem = pSeq->getItem(i);
					if(pItem)
					{
						DICOMUtil::GetTagFromDataset(pItem, DCM_AccessionNumber, SchStepSttr.sAccessionNumber);
						DICOMUtil::GetTagFromDataset(pItem, DCM_ScheduledProcedureStepID, SchStepSttr.sSPSID);
						DICOMUtil::GetTagFromDataset(pItem, DCM_RequestedProcedureID, SchStepSttr.sRequestedProcedureID);

						DICOMUtil::GetTagFromDataset(pItem, DCM_StudyInstanceUID, SchStepSttr.sStudyInstanceUID);
						DICOMUtil::GetTagFromDataset(pItem, DCM_RequestedProcedureDescription, SchStepSttr.sReqProcDescription);
						DICOMUtil::GetTagFromDataset(pItem, DCM_ScheduledProcedureStepDescription, SchStepSttr.sScheProcStepDesc);

						pReqData->AddPerScheStepAttr(SchStepSttr);
					}
				}
				bRetVal = true;
			}
		}

		if(m_pCfg->GetProfiling())
			dbg.EndProfiling(m_pServerLogger, NULL, "Request dataset extracted.");

		return bRetVal;
	}

}	//	End namespace imapacs.
