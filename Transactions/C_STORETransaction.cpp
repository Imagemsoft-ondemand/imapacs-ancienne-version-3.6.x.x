/////////////////////////////////////////////////////////////////////////////
//	C_STORETransaction.cpp
/////////////////////////////////////////////////////////////////////////////

#ifdef WIN32
#include "../stdafx.h"
#endif

#include "../Utils/DICOMUtil.h"
#include "../Utils/DataFotmat.h"
#include "../Utils/CDCMClient.h"
#include "../Utils/DBGTool.h"
#include "../Utils/Str.h"
#include "../Utils/TimeUtil.h"
#include "../Utils/os.h"
#include "../Utils/PatchModule.h"
#include "C_STORETransaction.h"

#ifdef WIN32
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
#endif

/////////////////////////////////////////////////////////////////////////////
//
namespace imapacs
{
	/////////////////////////////////////////////////////////////////////////
	//
	bool C_STORETransaction::Init(T_ASC_Association *pAssociation,
			                      T_DIMSE_Message *pMsg,
				                  T_ASC_PresentationContextID PresID,
			                      PacsConfig *pCfg)
	{
		bool bRetVal = false;

		memcpy(&m_CurrentReq, pMsg, sizeof(T_DIMSE_Message));
		memset(&m_CurrentRsp, 0, sizeof(T_DIMSE_Message));

		m_CurrentRsp.CommandField                            = DIMSE_C_STORE_RSP;
		m_CurrentRsp.msg.CStoreRSP.DataSetType               = DIMSE_DATASET_NULL;	//	Assuming
		m_CurrentRsp.msg.CStoreRSP.DimseStatus               = STATUS_Success;		//	Assuming.
		m_CurrentRsp.msg.CStoreRSP.MessageIDBeingRespondedTo = m_CurrentReq.msg.CStoreRQ.MessageID;
		m_CurrentRsp.msg.CStoreRSP.opts                      = O_STORE_AFFECTEDSOPCLASSUID | 
		                                                       O_STORE_AFFECTEDSOPINSTANCEUID;

		strcpy(m_CurrentRsp.msg.CStoreRSP.AffectedSOPClassUID, m_CurrentReq.msg.CStoreRQ.AffectedSOPClassUID);
		strcpy(m_CurrentRsp.msg.CStoreRSP.AffectedSOPInstanceUID, m_CurrentReq.msg.CStoreRQ.AffectedSOPInstanceUID);

		m_pAssociation  = pAssociation;
		m_CurrentPresID = PresID;

		m_pCfg = pCfg;

		//	For convenience.
		m_pServerLogger   = pCfg->GetServerLogger();
		m_pModalityLogger = pCfg->GetModalityLogger();
		m_pMailer         = pCfg->GetMailer();

		m_pServerLogger->LogMsg("[%s] - C-STORE request.", pAssociation->params->DULparams.callingAPTitle);

		m_pModalityLogger->LogMsg(" ");
		m_pModalityLogger->LogMsg("C-STORE request.");
		m_pModalityLogger->LogMsg(" ");
		m_pMailer->AddLine("C-STORE request.");

		if(pCfg->GetModalityDebug())
		{
			//	Generate a transaction id to group
			//	transaction related file names.
			srand((unsigned int)time(NULL));
			m_iTransactionId = rand();

			m_sCurrentDebugDir.Format("%s%s", DEBUG_DIRECTORY, pAssociation->params->DULparams.callingAPTitle);
			os::MkDir((char *)m_sCurrentDebugDir);
		}

		m_DataService.SetCurrentDebugParams((char *)m_sCurrentDebugDir, m_iTransactionId);
		m_DataService.SetConfig(pCfg);

		bRetVal = true;

		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	bool C_STORETransaction::ProcessTransaction()
	{
		/////////////////////////////////////////////////////////////////////////
		//	dcmtk variables.
		OFCondition cond                = EC_Normal;
		char sBasePath[256]             = {0};
		char sStorageId[65]             = {0};
		char sModalitiesInStudy[65]     = {0};
		char sQaIp[33]                  = {0};
		unsigned short iQaPort          = 0;
		double fScaleFactor             = 0.0;
		char sQaAeTitle[17]             = {0};
		char sNumericValue[65]          = {0};
		PatientData PatientInfo         = {0};
		StudyData StudyInfo             = {0};
		ObjectData ObjectInfo           = {0};
		char sTempAccessionNb[17]       = {0};
		DcmDataset *pStatus             = NULL;
		DcmDataset *pImageDataset       = NULL;
		DcmDataset *pScaledImageDataset = NULL;
		STORE_STATUS StoreRet           = SS_ERROR;
		CDCMClient Client;
		PatchModule Patch;
		PatchParams PatchInfos;
		DBGTool dbg;

		if(m_pCfg->GetModalityDebug())
		{
			m_pModalityLogger->LogMsg("--C_STORE Transaction::ProcessTransaction()");
		}

		if(m_pCfg->GetProfiling())
			dbg.StartProfiling(m_pServerLogger, "Receving request dataset.");

		/////////////////////////////////////////////////////////////////////////
		//	Read the image dataset from the calling application.
		cond = DIMSE_receiveDataSetInMemory(m_pAssociation,
											DIMSE_BLOCKING,
											0,
											&m_CurrentPresID,
											&pImageDataset,
											NULL,
											NULL);

		if(m_pCfg->GetProfiling())
			dbg.EndProfiling(m_pServerLogger, NULL, "Sql composed.");

		if(cond.bad())
		{
			m_pModalityLogger->LogMsg("ERROR : %s", cond.text());
			m_pMailer->AddLine("%s", cond.text());

			DICOMUtil::ClearDataset(&pImageDataset);

			return false;
		}

		/////////////////////////////////////////////////////////////////////////
		//	Get the patient informations.
		if(ExtractPatientInformation(pImageDataset, &PatientInfo))
		{
			//*** [ref:0002072] We check if the study need to be reassign to antoher patient ID
			if(m_pCfg->IsTransient())
			{
				StudyData StudyInfo_Tmp = {0};
				char sPatientID_tmp[65] = {0};
				strcpy(sPatientID_tmp, PatientInfo.sPatientId);
				if(ExtractStudyInformation(pImageDataset, &StudyInfo_Tmp))
					m_DataService.CheckPatientId(sPatientID_tmp, StudyInfo_Tmp.sStudyInstanceUID);

				if(strcmp(PatientInfo.sPatientId, sPatientID_tmp) != 0)
				{
					strncpy(PatientInfo.sPatientId, sPatientID_tmp, sizeof(PatientInfo.sPatientId));
					PatientInfo.sPatientId[sizeof(PatientInfo.sPatientId)-1] = 0x00;

					pImageDataset->putAndInsertString(DCM_PatientID, sPatientID_tmp);
				}
			}

			if(!m_DataService.PatientExists(PatientInfo.sPatientId))
			{
				//	No patient with that id, create it.
				if(m_DataService.CreatePatient(&PatientInfo))
				{
					m_pModalityLogger->LogMsg("Patient created     : %s (%s)", PatientInfo.sPatientName,
																			   PatientInfo.sPatientId);

					m_pMailer->AddLine("Patient created:\n\tPatient ID:  [%s]\n\tPatient Name: [%s]", PatientInfo.sPatientId,
																									  PatientInfo.sPatientName);
				}
			}
			else
			{
				m_pModalityLogger->LogMsg("Patient             : %s (%s)",	PatientInfo.sPatientName, 
																			PatientInfo.sPatientId);

				m_pMailer->AddLine("Patient ID:  [%s]\nPatient Name:  [%s]", PatientInfo.sPatientId, 
																			 PatientInfo.sPatientName);

			}
		}
		else if(!PatientInfo.sPatientId[0])
		{
			//	No patient Id was specified, reject.
			m_CurrentRsp.msg.CStoreRSP.DimseStatus = STATUS_STORE_Refused_OutOfResources;

			if(m_pModalityLogger)
			{
				m_pModalityLogger->LogMsg("No patient id in received dataset.");
				m_pModalityLogger->LogMsg("Sending error.");
				m_pModalityLogger->LogMsg("Not saving.");
			}

			if(m_pMailer)
			{
				m_pMailer->AddLine("No patient id in received dataset.");
				m_pMailer->AddLine("Sending error.");
				m_pMailer->AddLine("Not saving.");
			}
		}

		if(m_CurrentRsp.msg.CStoreRSP.DimseStatus == STATUS_Success)
		{
			//	Get the study informations.
			if(ExtractStudyInformation(pImageDataset, &StudyInfo))
			{
				m_pModalityLogger->LogMsg(" ");
				m_pModalityLogger->LogMsg("Study instance UID  : %s", StudyInfo.sStudyInstanceUID);
				m_pModalityLogger->LogMsg(" ");

				m_pModalityLogger->LogMsg("Accession number    : %s", StudyInfo.sAccessionNumber);
				m_pModalityLogger->LogMsg(" ");

				m_pMailer->AddLine("Study instance UID : [%s]", StudyInfo.sStudyInstanceUID);

				PatchInfos.Reset();

				strcpy(PatchInfos.sPatchId, "00003");
				PatchInfos.pCallingAeTitle   = m_pAssociation->params->DULparams.callingAPTitle;
				PatchInfos.pStudyInstanceUID = StudyInfo.sStudyInstanceUID;
				PatchInfos.pPkgName          = m_pCfg->GetPkgName();
				PatchInfos.pStudyDate        = StudyInfo.sStudyDate;
				PatchInfos.pModalityLogger   = m_pModalityLogger;
				PatchInfos.pMailer           = m_pMailer;
				PatchInfos.pDb               = m_DataService.GetDatabase();
				PatchInfos.pAccessionNumber  = StudyInfo.sAccessionNumber;
				PatchInfos.pPatientId        = PatientInfo.sPatientId;
				PatchInfos.pImplementationId = m_pCfg->GetImplementationId();
				PatchInfos.pDataset          = pImageDataset;
				PatchInfos.pCfg              = m_pCfg;

				Patch.Dispatcher(&PatchInfos);

				PatchInfos.Reset();

				strcpy(PatchInfos.sPatchId, "00005");
				PatchInfos.pCallingAeTitle   = m_pAssociation->params->DULparams.callingAPTitle;
				PatchInfos.pStudyInstanceUID = StudyInfo.sStudyInstanceUID;
				PatchInfos.pPkgName          = m_pCfg->GetPkgName();
				PatchInfos.pStudyDate        = StudyInfo.sStudyDate;
				PatchInfos.pModalityLogger   = m_pModalityLogger;
				PatchInfos.pMailer           = m_pMailer;
				PatchInfos.pDb               = m_DataService.GetDatabase();
				PatchInfos.pAccessionNumber  = StudyInfo.sAccessionNumber;
				PatchInfos.pPatientId        = PatientInfo.sPatientId;
				PatchInfos.pImplementationId = m_pCfg->GetImplementationId();
				PatchInfos.pDataset          = pImageDataset;
				PatchInfos.pCfg              = m_pCfg;

				Patch.Dispatcher(&PatchInfos);

				strcpy(PatchInfos.sPatchId, "00012");
				PatchInfos.pCallingAeTitle   = m_pAssociation->params->DULparams.callingAPTitle;
				PatchInfos.pStudyInstanceUID = StudyInfo.sStudyInstanceUID;
				PatchInfos.pPkgName          = m_pCfg->GetPkgName();
				PatchInfos.pStudyDate        = StudyInfo.sStudyDate;
				PatchInfos.pModalityLogger   = m_pModalityLogger;
				PatchInfos.pMailer           = m_pMailer;
				PatchInfos.pDb               = m_DataService.GetDatabase();
				PatchInfos.pAccessionNumber  = StudyInfo.sAccessionNumber;
				PatchInfos.pPatientId        = PatientInfo.sPatientId;
				PatchInfos.pImplementationId = m_pCfg->GetImplementationId();
				PatchInfos.pDataset          = pImageDataset;
				PatchInfos.pCfg              = m_pCfg;

				Patch.Dispatcher(&PatchInfos);

				//	If this is not one of our study, make sure the accession
				//	number will not be repeated in the database.
				if(!Str::StringHasSubstring(StudyInfo.sStudyInstanceUID, m_pCfg->GetImplementationId()))
				{
					if(!m_DataService.GetStudyAccessionNumber(StudyInfo.sStudyInstanceUID, sTempAccessionNb))
					{
						m_pModalityLogger->LogMsg("WARNING : No accession number in received object.");

						m_DataService.GetNewAccessionNumber(StudyInfo.sAccessionNumber);

						//	Set the right accession into the dicom object.
						pImageDataset->putAndInsertString(DCM_AccessionNumber, StudyInfo.sAccessionNumber);

						m_pModalityLogger->LogMsg("Accession number set to : %s", StudyInfo.sAccessionNumber);
						m_pModalityLogger->LogMsg("");
					}
					else
					{
						//	Set the right accession into the dicom object.
						strcpy(StudyInfo.sAccessionNumber, sTempAccessionNb);
						pImageDataset->putAndInsertString(DCM_AccessionNumber, StudyInfo.sAccessionNumber);
					}
				}
				else
				{
					//	If this is one of our studies, make sure
					//	it has a date.
					if(strlen(StudyInfo.sStudyDate)==0)
					{
						//	The study doesn't have a date,
						//	use the current date.
						if(m_DataService.UpdateStudyDate(&StudyInfo))
							pImageDataset->putAndInsertString(DCM_StudyDate, StudyInfo.sStudyDate);
						else
						{
							m_pModalityLogger->LogMsg("Unable to update study date.");
							m_pMailer->AddLine("Unable to update study date.");
						}

						m_pModalityLogger->LogMsg("Study without date.");
						m_pModalityLogger->LogMsg("Study Instance UID:  %s", StudyInfo.sStudyInstanceUID);
						m_pModalityLogger->LogMsg("Study Date:  %s", StudyInfo.sStudyDate);

						m_pMailer->AddLine("Study without date.");
						m_pMailer->AddLine("Study Instance UID:  %s", StudyInfo.sStudyInstanceUID);
						m_pMailer->AddLine("Study Date:  %s", StudyInfo.sStudyDate);
					}
				}

				strcpy(StudyInfo.sSendingAeTitle, m_pAssociation->params->DULparams.callingAPTitle);

 				if(!m_DataService.CreateStudy(&StudyInfo, &PatientInfo, m_pCfg->IsTransient()))
				{
					m_pModalityLogger->LogMsg("        Failed to create study.");
					m_pMailer->AddLine("Failed to create study.");
				}
			}
			else if(!StudyInfo.sStudyInstanceUID[0])
			{
				m_pModalityLogger->LogMsg("ERROR : No study instance UID in received dataset.");
				m_pModalityLogger->LogMsg("Will not save.");

				m_pMailer->AddLine("No study instance UID in received dataset.");
				m_pMailer->AddLine("Will not save.");

				m_CurrentRsp.msg.CStoreRSP.DimseStatus = STATUS_STORE_Refused_OutOfResources;
			}
		}

		if(m_CurrentRsp.msg.CStoreRSP.DimseStatus == STATUS_Success)
		{
			//	Get the study informations.
			if(ExtractObjectInformation(pImageDataset, &ObjectInfo))
			{
				m_pModalityLogger->LogMsg("Serie instance UID  : %s", ObjectInfo.sSerieInstanceUID);
				m_pModalityLogger->LogMsg("COI instance UID    : %s", ObjectInfo.sSOPInstanceUID);

				m_pMailer->AddLine("Serie instance UID  :  [%s]", ObjectInfo.sSerieInstanceUID);
				m_pMailer->AddLine("COI instance UID    :  [%s]", ObjectInfo.sSOPInstanceUID);

				strcpy(ObjectInfo.sAeTitle, m_pAssociation->params->DULparams.callingAPTitle);

				m_DataService.GetModalitiesInStudy(StudyInfo.sStudyInstanceUID, sModalitiesInStudy);

				if(!strstr(sModalitiesInStudy, ObjectInfo.sModality))
				{
					// [Ref:0000722] - D�but
					//strcat(sModalitiesInStudy, "\\"); 
					if (strlen(sModalitiesInStudy)>0) 
						strcat(sModalitiesInStudy, "\\");
					// [Ref:0000722] - Fin
					strcat(sModalitiesInStudy, ObjectInfo.sModality);

					m_DataService.UpdateModalitiesInStudy(StudyInfo.sStudyInstanceUID, sModalitiesInStudy);
				}

				//	Checking dataset integrity vs request.
				PatchModule Patch; // [Ref:0000577]

				PatchInfos.Reset();

				strcpy(PatchInfos.sPatchId, "00008");
				PatchInfos.pCallingAeTitle = m_pAssociation->params->DULparams.callingAPTitle;
				PatchInfos.pDb             = m_DataService.GetDatabase();
				PatchInfos.pModalityLogger = m_pModalityLogger;
				PatchInfos.pMailer         = m_pMailer;
				PatchInfos.pCurrentReq     = &m_CurrentReq;
				PatchInfos.pObjectInfo     = &ObjectInfo;
				PatchInfos.pCfg            = m_pCfg;

				Patch.Dispatcher(&PatchInfos);

				if(strcmp(ObjectInfo.sSopClassUID, m_CurrentReq.msg.CStoreRQ.AffectedSOPClassUID) != 0)
				{
					m_CurrentRsp.msg.CStoreRSP.DimseStatus = STATUS_STORE_Error_DataSetDoesNotMatchSOPClass;

					m_pModalityLogger->LogMsg("ERROR : Dataset does not match SOP Class.");
					m_pModalityLogger->LogMsg("        Dataset SOP Class %s", ObjectInfo.sSopClassUID);
					m_pModalityLogger->LogMsg("        Request SOP Class %s", m_CurrentReq.msg.CStoreRQ.AffectedSOPClassUID);

					m_pModalityLogger->LogMsg("Not storing image, sending error.");
					
					m_pMailer->AddLine("Dataset does not match SOP Class.");
					m_pMailer->AddLine("Dataset SOP Class [%s]", ObjectInfo.sSopClassUID);
					m_pMailer->AddLine("Request SOP Class [%s]", m_CurrentReq.msg.CStoreRQ.AffectedSOPClassUID);

					m_pMailer->AddLine("Not storing image, sending error.");
				}
				else if(strcmp(ObjectInfo.sSOPInstanceUID, m_CurrentReq.msg.CStoreRQ.AffectedSOPInstanceUID) != 0)
				{
					m_CurrentRsp.msg.CStoreRSP.DimseStatus = STATUS_STORE_Error_DataSetDoesNotMatchSOPClass;

					m_pModalityLogger->LogMsg("ERROR : Dataset does not match SOP Instance.");
					m_pModalityLogger->LogMsg("        Dataset SOP Class %s", ObjectInfo.sSOPInstanceUID);
					m_pModalityLogger->LogMsg("        Request SOP Class %s", m_CurrentReq.msg.CStoreRQ.AffectedSOPInstanceUID);

					m_pModalityLogger->LogMsg("Not storing image, sending error.");

					m_pMailer->AddLine("Dataset does not match SOP Instance.");
					m_pMailer->AddLine("Dataset SOP Class [%s]", ObjectInfo.sSOPInstanceUID);
					m_pMailer->AddLine("Request SOP Class [%s]", m_CurrentReq.msg.CStoreRQ.AffectedSOPInstanceUID);

					m_pMailer->AddLine("Not storing image, sending error.");
				}
				else if(ObjectInfo.sSerieInstanceUID[0] == 0x00)
				{
					m_CurrentRsp.msg.CStoreRSP.DimseStatus = STATUS_STORE_Refused_OutOfResources;

					m_pModalityLogger->LogMsg("ERROR : No series instance UID in received dataset.");
					m_pModalityLogger->LogMsg("Not storing image, sending error.");

					m_pMailer->AddLine("No series instance UID in received dataset.");
					m_pMailer->AddLine("Not storing image, sending error.");
				}
				else
				{
					if(!m_DataService.CreateSeries(&ObjectInfo, &StudyInfo))
					{
						m_pModalityLogger->LogMsg("        Failed to create serie.");
						m_pMailer->AddLine("Failed to create serie.");
					}

					if(m_pCfg->GetMFS(StudyInfo.sStudyInstanceUID, sBasePath))
					{
						DataFormat::GetStorageIdFromMFS(sBasePath, sStorageId);
					}
					else
					{
						m_pModalityLogger->LogMsg("        Failed to get a storage location.");
						m_pMailer->AddLine("Failed to get a storage location.");

						m_CurrentRsp.msg.CStoreRSP.DimseStatus = STATUS_STORE_Refused_OutOfResources;
					}

					if(!m_DataService.CreateObject(&ObjectInfo, &StudyInfo, sStorageId))
					{
						m_pModalityLogger->LogMsg("        Failed to create object.");
						m_pMailer->AddLine("Failed to create object.");

					}

					if(strcmp(ObjectInfo.sObjectType, "AUDIO")==0)
					{
						//	It's an sudio dicom object, set the read date
						//	to the current date and set the status to read.
						if(!m_DataService.SetStudyRead(&StudyInfo))
						{
							m_pModalityLogger->LogMsg("        Failed to set study read.");
							m_pMailer->AddLine("Failed to set study read.");
						}
					}
					else if(strcmp(ObjectInfo.sObjectType, "SR_UNVERIFIED")==0)
					{
						// [Ref:IVPACS-37] - D�but
						//	It's a structured report dicom object. Set the current
						//	date as the read date.
						/*if(!m_DataService.SetStudyTranscribed(&StudyInfo))
						{
							m_pModalityLogger->LogMsg("        Failed to set study transcribed.");
							m_pMailer->AddLine("Failed to set study transcribed.");
						}*/
						// [Ref:IVPACS-37] - Fin
					}
				}
			}
		}
		
		//	Patch for Agfa CR85.
		PatchInfos.Reset();

		strcpy(PatchInfos.sPatchId, "00004");
		PatchInfos.pCallingAeTitle   = m_pAssociation->params->DULparams.callingAPTitle;
		PatchInfos.pStudyInstanceUID = StudyInfo.sStudyInstanceUID;
		PatchInfos.pPkgName          = m_pCfg->GetPkgName();
		PatchInfos.pStudyDate        = StudyInfo.sStudyDate;
		PatchInfos.pModalityLogger   = m_pModalityLogger;
		PatchInfos.pMailer           = m_pMailer;
		PatchInfos.pDb               = m_DataService.GetDatabase();
		PatchInfos.pAccessionNumber  = StudyInfo.sAccessionNumber;
		PatchInfos.pPatientId        = PatientInfo.sPatientId;
		PatchInfos.pImplementationId = m_pCfg->GetImplementationId();
		PatchInfos.pDataset          = pImageDataset;
		PatchInfos.pCfg              = m_pCfg;

		Patch.Dispatcher(&PatchInfos);

		//	Patch for Philips DIDILB.
		PatchInfos.Reset();

		strcpy(PatchInfos.sPatchId, "00006");
		PatchInfos.pCallingAeTitle   = m_pAssociation->params->DULparams.callingAPTitle;
		PatchInfos.pStudyInstanceUID = StudyInfo.sStudyInstanceUID;
		PatchInfos.pPkgName          = m_pCfg->GetPkgName();
		PatchInfos.pStudyDate        = StudyInfo.sStudyDate;
		PatchInfos.pModalityLogger   = m_pModalityLogger;
		PatchInfos.pMailer           = m_pMailer;
		PatchInfos.pDb               = m_DataService.GetDatabase();
		PatchInfos.pAccessionNumber  = StudyInfo.sAccessionNumber;
		PatchInfos.pPatientId        = PatientInfo.sPatientId;
		PatchInfos.pImplementationId = m_pCfg->GetImplementationId();
		PatchInfos.pDataset          = pImageDataset;
		PatchInfos.pCfg              = m_pCfg;

		Patch.Dispatcher(&PatchInfos);

		if(m_pCfg->GetProfiling())
			dbg.StartProfiling(m_pServerLogger, "Calling InsertEvent");

		m_pCfg->InsertEvent("C-STORE",
							NULL,
							"IMAPACS",
							m_pAssociation->params->DULparams.callingAPTitle,
							NULL,
							PatientInfo.sPatientId,
							ObjectInfo.sObjectType,
							ObjectInfo.sSOPInstanceUID,
							NULL,
							NULL,
							NULL,
							NULL,
							NULL,
							NULL);

		if(m_pCfg->GetProfiling())
			dbg.EndProfiling(m_pServerLogger, NULL, "InsertEvent called");

		//	Everything looks fine, let's save the object.
		if(m_CurrentRsp.msg.CStoreRSP.DimseStatus == STATUS_Success)
		{
			//	Store patch for Swissray.
			PatchInfos.Reset();

			strcpy(PatchInfos.sPatchId, "00002");
			PatchInfos.pDb             = m_DataService.GetDatabase();
			PatchInfos.pCallingAeTitle = m_pAssociation->params->DULparams.callingAPTitle;
			PatchInfos.pDataset        = pImageDataset;
			PatchInfos.pModalityLogger = m_pModalityLogger;
			PatchInfos.pMailer         = m_pMailer;
			PatchInfos.pCfg            = m_pCfg;

			Patch.Dispatcher(&PatchInfos);

			// [Ref:0000578] - D�but
			//	Store Patch for APLIO
			PatchInfos.Reset();

			strcpy(PatchInfos.sPatchId, "00009");
			PatchInfos.pDb             = m_DataService.GetDatabase();
			PatchInfos.pCallingAeTitle = m_pAssociation->params->DULparams.callingAPTitle;
			PatchInfos.pDataset        = pImageDataset;
			PatchInfos.pModalityLogger = m_pModalityLogger;
			PatchInfos.pMailer         = m_pMailer;
			PatchInfos.pCfg            = m_pCfg;

			Patch.Dispatcher(&PatchInfos);

			// [Ref:0000578] - Fin

			//	Save the object on disk.
			if(!SaveObject(pImageDataset, &StudyInfo, &ObjectInfo, sBasePath))
			{
				m_pServerLogger->LogMsg("[%s] - C-STORE RQ with error.", m_pAssociation->params->DULparams.callingAPTitle);
				m_pModalityLogger->LogMsg("C-STORE RQ with error.");
				m_pMailer->AddLine("C-STORE RQ with error.");

				m_CurrentRsp.msg.CStoreRSP.DimseStatus = STATUS_STORE_Refused_OutOfResources;
			}
		}

		/////////////////////////////////////////////////////////////////////////
		//	Send the C-STORE responce.
		cond = DIMSE_sendMessageUsingMemoryData(m_pAssociation, 
												m_CurrentPresID, 
												&m_CurrentRsp,
												pStatus, 
												NULL, 
												NULL, 
												NULL);
		if(cond.bad())
		{
			m_pModalityLogger->LogMsg("ERROR : %s", cond.text());
			m_pMailer->AddLine("%s", cond.text());

			DICOMUtil::ClearDataset(&pImageDataset);
			DICOMUtil::ClearDataset(&pStatus);

			return false;
		}
		//
		/////////////////////////////////////////////////////////////////////

		/////////////////////////////////////////////////////////////////////
		//	Generate the scaled image and send it to the QA service if there
		//	is one configured for this calling AE title.
		if(DICOMUtil::IsImageType(pImageDataset))
		{
			if(m_pCfg->GetQaRoutingInfos(m_pAssociation->params->DULparams.callingAPTitle, &fScaleFactor, sQaIp, &iQaPort, sQaAeTitle))
			{
				/////////////////////////////////////////////////////////////
				//	Setup client.
				Client.SetConfig(m_pCfg);
				Client.SetRemoteIP(sQaIp);
				Client.SetRemotePort(iQaPort);
				Client.SetCallingAE(m_pCfg->GetServerAeTitle());
				Client.SetCalledAE(sQaAeTitle);

				sprintf(sNumericValue, "%f", fScaleFactor);
				m_pModalityLogger->LogMsg("Scaling image at %s.", sNumericValue);

				pScaledImageDataset = ScaleImage(pImageDataset, fScaleFactor);
				if(pScaledImageDataset)
				{
					if(Client.CreateAssociation())
					{
						if(Client.RequestAssociation())
						{
							m_pModalityLogger->LogMsg("Sending scaled images to %s @ %s:%d", sQaAeTitle,
								                                                             sQaIp,
																							 iQaPort);
							if(Client.StoreObject(pScaledImageDataset) == SS_ERROR)
							{
								m_pModalityLogger->LogMsg("Failed to store to QA.");
							}
						}
						else
						{
							m_pModalityLogger->LogMsg("Failed to establish association : %s @ %s:%d.", sQaAeTitle,
								                                                                       sQaIp,
																							           iQaPort);
						}
						Client.CloseAssociation();
					}
					else
					{
						m_pModalityLogger->LogMsg("Failed to allocate client association.");
					}
					DICOMUtil::ClearDataset(&pScaledImageDataset);
				}
				else
				{
					m_pModalityLogger->LogMsg("        Failed to scale image.");
				}
			}
		}
		DICOMUtil::ClearDataset(&pImageDataset);
		DICOMUtil::ClearDataset(&pStatus);

		if(m_CurrentRsp.msg.CStoreRSP.DimseStatus != STATUS_Success)
		{
			return false;
		}
		return true;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	void C_STORETransaction::Shutdown()
	{
		m_DataService.Dispose();
		m_sCurrentDebugDir.Clear();
	}

	/////////////////////////////////////////////////////////////////////////
	//
	bool C_STORETransaction::ExtractPatientInformation(DcmDataset *pDataset, PatientData *pInfo)
	{
		bool bRetVal = false;
		DBGTool dbg;

		if(m_pCfg->GetModalityDebug())
		{
			m_pModalityLogger->LogMsg("C_STORETransaction::ExtractPatientInformation");
		}

		if(m_pCfg->GetProfiling())
			dbg.StartProfiling(m_pServerLogger, "Extracting patient data from dataset.");

		if(pDataset)
		{
			DICOMUtil::GetTagFromDataset(pDataset, DCM_PatientID, pInfo->sPatientId);
			DICOMUtil::GetTagFromDataset(pDataset, DCM_PatientName, pInfo->sPatientName);
			DICOMUtil::GetTagFromDataset(pDataset, DCM_PatientSex, pInfo->sPatientSex);
			DICOMUtil::GetTagFromDataset(pDataset, DCM_PatientBirthDate, pInfo->sPatientBirthDate);
			DICOMUtil::GetTagFromDataset(pDataset, DCM_PatientBirthTime, pInfo->sPatientBirthTime);
			DICOMUtil::GetTagFromDataset(pDataset, DCM_OtherPatientIDs, pInfo->sPatientOtherId);

			if(!pInfo->sPatientName[0])
			{
				m_pModalityLogger->LogMsg("WARNING : missing patient name (0010, 0010) in recceived dataset.");
				m_pMailer->AddLine("Missing patient name (0010, 0010) in recceived dataset.");
			}

			if(!pInfo->sPatientId[0])
			{
				m_pModalityLogger->LogMsg("WARNING : missing patient name (0010, 0010) in recceived dataset.");
				m_pMailer->AddLine("Missing patient name (0010, 0010) in recceived dataset.");
			}
			bRetVal = true;
		}
		else
		{
			m_pModalityLogger->LogMsg("NULL dataset passed to DICOMSession::ExtractPatientInformation");
			m_pMailer->AddLine("NULL dataset passed to DICOMSession::ExtractPatientInformation");
		}

		if(m_pCfg->GetProfiling())
			dbg.EndProfiling(m_pServerLogger, NULL, "Patient data extracted.");

		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	bool C_STORETransaction::ExtractStudyInformation(DcmDataset *pDataset, StudyData *pInfo)
	{
		bool bRetVal = false;
		DBGTool dbg;

		if(m_pCfg->GetModalityDebug())
		{
			m_pModalityLogger->LogMsg("--C_STORETransaction::ExtractStudyInformation()");
		}

		if(m_pCfg->GetProfiling())
			dbg.StartProfiling(m_pServerLogger, "Extracting study info from dataset.");

		DICOMUtil::GetTagFromDataset(pDataset, DCM_StudyInstanceUID, pInfo->sStudyInstanceUID);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_StudyID, pInfo->sStudyId);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_StudyDescription, pInfo->sStudyDesc);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_ReferringPhysicianName, pInfo->sStudyRefPhysicianName);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_NameOfPhysiciansReadingStudy, pInfo->sPhysicianReadingStudy);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_StudyDate, pInfo->sStudyDate);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_StudyTime, pInfo->sStudyTime);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_AccessionNumber, pInfo->sAccessionNumber);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_PatientAge, pInfo->sPatientAge);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_PatientSize, pInfo->sPatientSize);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_PatientWeight, pInfo->sPatientWeight);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_InstitutionName, pInfo->sInstitutionName);

		if(pInfo->sStudyInstanceUID[0])
		{
			bRetVal = true;
		}

		if(m_pCfg->GetProfiling())
			dbg.EndProfiling(m_pServerLogger, NULL, "Study info extracted.");

		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	bool C_STORETransaction::ExtractObjectInformation(DcmDataset *pDataset, ObjectData *pInfo)
	{
		bool bRetVal = false;
		DBGTool dbg;

		if(m_pCfg->GetModalityDebug())
		{
			m_pModalityLogger->LogMsg("--C_STORETransaction::ExtractObjectInformation()");
		}

		if(m_pCfg->GetProfiling())
			dbg.StartProfiling(m_pServerLogger, "Extracting object infos from dataset.");

		DICOMUtil::GetTagFromDataset(pDataset, DCM_SeriesInstanceUID, pInfo->sSerieInstanceUID);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_SOPInstanceUID, pInfo->sSOPInstanceUID);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_SOPClassUID, pInfo->sSopClassUID);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_Modality, pInfo->sModality);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_SeriesNumber, pInfo->sSerieNumber);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_InstanceNumber, pInfo->sInstanceNumber);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_SeriesDescription,	pInfo->sSerieDescription);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_ViewPosition, pInfo->sViewPos);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_Laterality, pInfo->sLaterality);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_BodyPartExamined, pInfo->sBodyPart);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_SeriesDate, pInfo->sSerieDate);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_SeriesTime, pInfo->sSerieTime);
		DICOMUtil::GetTagFromDataset(pDataset, DCM_StationName, pInfo->sStationName);

		if(pInfo->sSerieInstanceUID[0])
		{
			bRetVal = true;
		}
		else
		{
			m_pModalityLogger->LogMsg("WARNING : Missing serie instance uid (0020, 000e) in received dataset.");
			m_pMailer->AddLine("Missing serie instance uid (0020, 000e) in received dataset.");

			bRetVal = false;
		}

		if(m_pCfg->GetProfiling())
			dbg.EndProfiling(m_pServerLogger, NULL, "Object infos extracted.");

		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//	Takes care of all the logic to save a dicom object on
	//	the file system.
	bool C_STORETransaction::SaveObject(DcmDataset *pDataset, 
			                            StudyData *pStudy, 
					                    ObjectData *pObject, 
					                    const char *pBasePath)
	{
		bool bRetVal       = true;
		int iCompression   = 0;
		float fElapsedTime = 0.0f;
		StringBuffer sFullPathName;
		StringBuffer sOldFile;
		PatchParams PatchInfos;
		PatchModule Patch;
		DBGTool dbg;

		sFullPathName.Format("%s", pBasePath);

		if(m_pCfg->GetModalityDebug())
		{
			m_pModalityLogger->LogMsg("--C_STORETransaction::SaveObject()");
		}

		/////////////////////////////////////////////////////////////////////////
		//	Create the directory if it doesn't already exist.
		os::CreateFullDirectory(sFullPathName.GetBuffer());

		sFullPathName += "/";
		sFullPathName += pStudy->sStudyInstanceUID;

		/////////////////////////////////////////////////////////////////////////
		//	Create the directory if it doesn't already exist.
		os::MkDir(sFullPathName.GetBuffer());

		sFullPathName += "/";
		sFullPathName += pObject->sSerieInstanceUID;

		/////////////////////////////////////////////////////////////////////////
		//	Create the directory if it doesn't already exist.
		os::MkDir(sFullPathName.GetBuffer());

		sFullPathName += "/";
		sFullPathName += pObject->sSOPInstanceUID;
		sFullPathName += ".dcm";

		if(m_pCfg->GetProfiling())
			dbg.StartProfiling(m_pServerLogger, "Calling FileExists");

		/////////////////////////////////////////////////////////////////////////
		//	If the file already exists, rename it with a .old extension.
		if(os::FileExists(sFullPathName.GetBuffer()))
		{
			m_pModalityLogger->LogMsg("File [%s] already exists.", sFullPathName.GetBuffer());
			m_pModalityLogger->LogMsg("Renaming with .old extension.");

			m_pMailer->AddLine("File [%s] already exists.", sFullPathName.GetBuffer());
			m_pMailer->AddLine("Renaming with .old extension.");

			sOldFile.Format("%s.old", sFullPathName.GetBuffer());

			os::FileMove(sFullPathName.GetBuffer(), sOldFile.GetBuffer());
			sOldFile.Clear();
		}

		if(m_pCfg->GetProfiling())
			dbg.EndProfiling(m_pServerLogger, NULL, "FileExists called.");

		if(DICOMUtil::IsImageType(pObject->sSopClassUID))
		{
			if(m_pCfg->GetProfiling())
				dbg.StartProfiling(m_pServerLogger, "Calling m_pCfg->SaveCompressed");

			iCompression = m_pCfg->SaveCompressed(m_pAssociation->params->DULparams.callingAPTitle);

			if(m_pCfg->GetProfiling())
				dbg.EndProfiling(m_pServerLogger, NULL, "m_pCfg->SaveCompressed called");

			if(iCompression)
			{
				//	Try to save compressed.
				if(!SaveCompressed(sFullPathName.GetBuffer(), pDataset, iCompression))
				{
					m_pModalityLogger->LogMsg("ERROR : Failed to compress object.");
					if(iCompression == 1)
					{
						m_pModalityLogger->LogMsg("       %s\n", m_Compressor.GetLastErrorText());
					}

					m_pModalityLogger->LogMsg("Attempting to save object uncompressed ...");

					m_pMailer->AddLine("Failed to compress object.");

					if(iCompression == 1)
					{
						m_pMailer->AddLine("%s", m_Compressor.GetLastErrorText());
					}
	
					m_pMailer->AddLine("Attempting to save object uncompressed ...");

					//	Since compression failed, try to save uncompressed.
					if(SaveUncompressed(sFullPathName.GetBuffer(), pDataset, EXS_LittleEndianExplicit))
					{
						m_pModalityLogger->LogMsg("Object saved uncompressed.");
						m_pMailer->AddLine("Object saved uncompressed.");
					}
					else
					{
						m_pModalityLogger->LogMsg("ERROR : Failed to save object uncompressed.");
						m_pMailer->AddLine("ERROR : Failed to save object uncompressed.");

						bRetVal = false;
					}

					if(!bRetVal)
					{
						m_pModalityLogger->LogMsg("ERROR : Failed to save object.");
						m_pMailer->AddLine("Failed to save object.");

						if(!m_DataService.CancelObject(pObject->sSOPInstanceUID))
						{
							m_pModalityLogger->LogMsg("        Can not cancel object.");
							m_pMailer->AddLine("Can not cancel object.");
						}
					}
				}
			}
			else
			{
				//	Save uncompressed.
				if(!SaveUncompressed(sFullPathName.GetBuffer(), pDataset, EXS_LittleEndianExplicit))
				{
					if(m_pModalityLogger)
						m_pModalityLogger->LogMsg("ERROR : Failed to save object.");

					if(m_pMailer)
						m_pMailer->AddLine("Failed to save object.");

					if(!m_DataService.CancelObject(pObject->sSOPInstanceUID))
					{
						if(m_pModalityLogger)
							m_pModalityLogger->LogMsg("        Can not cancel object.");
						
						if(m_pMailer)
							m_pMailer->AddLine("Can not cancel object.");
					}
					bRetVal = false;
				}
			}
		}
		else
		{
			strcpy(PatchInfos.sPatchId, "00010");
			PatchInfos.pCallingAeTitle = m_pAssociation->params->DULparams.callingAPTitle;
			PatchInfos.pCfg            = m_pCfg;
			PatchInfos.pDb             = m_DataService.GetDatabase();
			PatchInfos.pModalityLogger = m_pModalityLogger;
			PatchInfos.pMailer         = m_pMailer;
			PatchInfos.pObjectLocation = sFullPathName.GetBuffer();
			PatchInfos.pDataset        = pDataset;

			if(!Patch.Dispatcher(&PatchInfos))	//	This patch overwrite the save.
			{
				//	It's not an image, save it as is (no compression).
				if(!SaveUncompressed(sFullPathName.GetBuffer(), pDataset, EXS_LittleEndianExplicit))
				{
					if(m_pModalityLogger)
						m_pModalityLogger->LogMsg("ERROR : Failed to save object.");

					if(m_pMailer)
						m_pMailer->AddLine("Failed to save object.");

					if(!m_DataService.CancelObject(pObject->sSOPInstanceUID))
					{
						if(m_pModalityLogger)
							m_pModalityLogger->LogMsg("        Can not cancel object.");

						if(m_pMailer)
							m_pMailer->AddLine("Can not cancel object.");
					}
					bRetVal = false;
				}
			}
		}
		m_pModalityLogger->LogMsg("        Saving:  %s %s.", pObject->sObjectDesc, pObject->sSopClassUID);
		m_pMailer->AddLine("Saving:  %s %s.", pObject->sObjectType, pObject->sSopClassUID);

		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	bool C_STORETransaction::SaveUncompressed(const char *pFile, DcmDataset *pDataset, E_TransferSyntax eTrnSyn)
	{
		bool bRetVal     = false;
		OFCondition cond = EC_Normal;
		DBGTool dbg;

		if(m_pCfg->GetModalityDebug())
		{
			m_pModalityLogger->LogMsg("--C_STORETransaction::SaveUncompressed()");
		}

		DcmFileFormat DICOMFile(pDataset);

		if(eTrnSyn <= 3)	//	Saving uncompressed only.
		{
			pDataset->chooseRepresentation(eTrnSyn, NULL);

			if(m_pCfg->GetProfiling())
				dbg.StartProfiling(m_pServerLogger, "Saving dicom object uncompressed.");

			cond = DICOMFile.saveFile(pFile,
									  EXS_LittleEndianExplicit,
									  EET_ExplicitLength, 
									  EGL_recalcGL,
									  EPD_noChange,
									  (Uint32) 0,
									  (Uint32) 0);

			if(m_pCfg->GetProfiling())
				dbg.EndProfiling(m_pServerLogger, NULL, "Object saved.");

			if(cond.good())
			{
				bRetVal = true;
			}
			else
			{
				m_pModalityLogger->LogMsg("ERROR : unable to save dicom file.");
				m_pModalityLogger->LogMsg("        %s", cond.text());
				m_pModalityLogger->LogMsg("        Was trying to save : [%s]", pFile);
				
				m_pMailer->AddLine("unable to save dicom file.");
				m_pMailer->AddLine("%s", cond.text());
				m_pMailer->AddLine("Was trying to save : [%s]", pFile);
			}
		}
		else
		{
			m_pModalityLogger->LogMsg("Invalid transfer syntax to save uncompressed.");
		}
		DICOMFile.clear();

		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	bool C_STORETransaction::SaveCompressed(const char *pFile, DcmDataset *pDataset, int iCompression)
	{
		bool bRetVal       = false;
		char sCommand[513] = {0};
#ifdef WIN32
		int iPid           = 0;
#else
		pid_t iPid         = 0;
#endif
		StringBuffer Tmp;
		DBGTool dbg;

		switch(iCompression)
		{
			case 1:	//	DCMTK compression.
			{
				m_pModalityLogger->LogMsg("-Using DCMTK compression.");

				if(m_pCfg->GetProfiling())
					dbg.StartProfiling(m_pServerLogger, "Calling m_Compressor.CompressFromDatasetToFile.");

				if(m_Compressor.CompressFromDatasetToFile(pFile, pDataset))
				{
					bRetVal = true;
				}
				else
				{
					m_pModalityLogger->LogMsg("ERROR : Failed to compress object.");
					m_pModalityLogger->LogMsg("       %s\n", m_Compressor.GetLastErrorText());
					m_pModalityLogger->LogMsg("Attempting to save object uncompressed ...");

					m_pMailer->AddLine("Failed to compress object.");
					m_pMailer->AddLine("%s", m_Compressor.GetLastErrorText());
					m_pMailer->AddLine("Attempting to save object uncompressed ...");

					//	Since compression failed, try to save uncompressed.
					if(SaveUncompressed(pFile, pDataset, EXS_LittleEndianExplicit))
					{
						m_pModalityLogger->LogMsg("Object saved uncompressed.");
						m_pMailer->AddLine("Object saved uncompressed.");

						bRetVal = true;
					}
					else
					{
						m_pModalityLogger->LogMsg("ERROR : Failed to save object uncompressed.");
						m_pMailer->AddLine("ERROR : Failed to save object uncompressed.");
					}
				}

				if(m_pCfg->GetProfiling())
					dbg.EndProfiling(m_pServerLogger, NULL, "m_Compressor.CompressFromDatasetToFile called.");
			}
			break;

			case 2:
			{
				//	External compressor (this is a patch to accomodate ImaView).
				m_pModalityLogger->LogMsg("-Using external compressor.");

				//	Generate temporary file.
				iPid = getpid();
				Tmp.Format("%s/%d.dcm", m_pCfg->GetTmpPath(), iPid);

				//	Save uncompressed temporary file.
				if(SaveUncompressed(Tmp.GetBuffer(), pDataset, EXS_LittleEndianImplicit))
				{
					//	On compresse le fichier temporaire
					sprintf(sCommand,"CompDCM %s %s comp", Tmp.GetBuffer(), pFile);
					if(system(sCommand) != -1)
					{
						if(!os::FileExists(pFile))
						{
							//	Compression failed.
							m_pModalityLogger->LogMsg("Compression failed.");

							if(m_pCfg->GetProfiling())
								dbg.StartProfiling(m_pServerLogger, "Calling FileMove");

							if(os::FileMove(Tmp.GetBuffer(), pFile))
							{
								bRetVal = true;
							}
							else
							{
								m_pModalityLogger->LogMsg("ERROR : impossible to move files : src : %s, dest : %s", Tmp.GetBuffer(),
									                                                                                pFile);
								m_pMailer->AddLine("ERROR : impossible to move files : src : %s, dest : %s", Tmp.GetBuffer(),
									                                                                                pFile);
							}

							if(m_pCfg->GetProfiling())
								dbg.EndProfiling(m_pServerLogger, NULL, "FileMove called.");
						}
						else
						{
							bRetVal = true;
						}
					}
					else
					{
						//	Compression failed.
						m_pModalityLogger->LogMsg("Failed to launch compressor.");

						if(m_pCfg->GetProfiling())
							dbg.StartProfiling(m_pServerLogger, "Calling MoveFile.");

						if(os::FileMove(Tmp.GetBuffer(), pFile))
						{
							bRetVal = true;
						}
						else
						{
							m_pModalityLogger->LogMsg("ERROR : impossible to compress files : src : %s, dest : %s", Tmp.GetBuffer(),
								                                                                                pFile);
							m_pMailer->AddLine("ERROR : impossible to compress files : src : %s, dest : %s", Tmp.GetBuffer(),
								                                                                                pFile);
						}

						if(m_pCfg->GetProfiling())
							dbg.EndProfiling(m_pServerLogger, NULL, "FileMove called.");
					}
					os::RemoveFile(Tmp.GetBuffer());
				}
			}
			break;

			if(!bRetVal)
			{
				m_pModalityLogger->LogMsg("ERROR : Failed to save object.");
				m_pMailer->AddLine("Failed to save object.");
			}
		}
		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//	Saves the object with the original transfer syntax of the dataset.
	bool C_STORETransaction::SaveAsIs(const char *pDestinationFile, DcmDataset *pDataset)
	{
		bool bRetVal     = false;
		OFCondition cond = EC_Normal;
		DBGTool dbg;

		if(m_pCfg->GetModalityDebug())
		{
			m_pModalityLogger->LogMsg("---C_STORETransaction::SaveAsIs()");
		}

		if(pDestinationFile && pDataset)
		{
			E_TransferSyntax OrginalXfer = pDataset->getOriginalXfer();

			if(OrginalXfer == EXS_Unknown)
			{
				OrginalXfer = EXS_LittleEndianExplicit;
			}

			DcmXfer XferSyn(OrginalXfer);	//	For logging only.

			DcmFileFormat File(pDataset);

			if(m_pCfg->GetProfiling())
				dbg.StartProfiling(m_pServerLogger, "Saving object uncompressed.");

			cond = File.saveFile(pDestinationFile,
				                 OrginalXfer,
								 EET_ExplicitLength,
								 EGL_recalcGL,
								 EPD_noChange,
								 (Uint32)0,
								 (Uint32)0);

			if(m_pCfg->GetProfiling())
				dbg.EndProfiling(m_pServerLogger, NULL, "Object saved.");

			if(cond.good())
			{
				bRetVal = true;
			}
			else
			{
				m_pModalityLogger->LogMsg("ERROR : %s", cond.text());
				m_pModalityLogger->LogMsg("         could not save object with original xfer : %s", XferSyn.getXferName());

				m_pMailer->AddLine("ERROR : %s", cond.text());
				m_pMailer->AddLine("         could not save object with original xfer : %s", XferSyn.getXferName());
			}
		}
		else
		{
			m_pModalityLogger->LogMsg("ERROR : NULL parameter passed to C_STORETransaction::SaveAsIs");
			m_pMailer->AddLine("ERROR : NULL parameter passed to C_STORETransaction::SaveAsIs");
		}
		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	DcmDataset *C_STORETransaction::ScaleImage(DcmDataset *pImageDataset, double fScaleFactor)
	{
		DcmDataset *pDataset     = NULL;
		DicomImage *pScaledImage = NULL;
		DicomImage *pImage       = NULL;
		double fCenter           = 0.0;
		double fWidth            = 0.0;

		if(pImageDataset)
		{
			pDataset = (DcmDataset *)pImageDataset->clone();
			if(pDataset)
			{
				pImage = new DicomImage(pImageDataset, EXS_Unknown, 0L);
				if(pImage)
				{
					pScaledImage = pImage->createScaledImage(fScaleFactor,
															 0.0,
															 1,
															 1);
					if(pScaledImage)
					{
						if(!pScaledImage->writeImageToDataset((*pDataset)))
						{
							m_pModalityLogger->LogMsg("ERROR : DicomImage::writeImageToDataset failed in C_STORETransaction::ScaleImage.");
						}
					}
					else
					{
						m_pModalityLogger->LogMsg("ERROR : Failed to ceate scaled image in C_STORETransaction::ScaleImage.");
					}
				}
				else
				{
					m_pModalityLogger->LogMsg("ERROR : Failed to create DicomImage object from dataset in C_STORETransaction::ScaleImage.");
				}
			}
			else
			{
				m_pModalityLogger->LogMsg("ERROR : Failed to clone dataset in C_STORETransaction::ScaleImage.");
			}
		}
		else
		{
			m_pModalityLogger->LogMsg("ERROR : NULL dataset passed to C_STORETransaction::ScaleImage.");
		}

		/////////////////////////////////////////////////////////////////////////
		//	Cleanup.
		DICOMUtil::ClearDicomImage(&pImage);
		DICOMUtil::ClearDicomImage(&pScaledImage);

		return pDataset;
	}
}
