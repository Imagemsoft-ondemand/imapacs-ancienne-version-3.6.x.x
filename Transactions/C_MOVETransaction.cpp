/////////////////////////////////////////////////////////////////////////////
//	C_MOVETransaction.cpp
/////////////////////////////////////////////////////////////////////////////

#include "../Utils/DICOMUtil.h"
#include "C_MOVETransaction.h"

/////////////////////////////////////////////////////////////////////////////
//
namespace imapacs
{
	/////////////////////////////////////////////////////////////////////////
	//
	bool C_MOVETransaction::Init(T_ASC_Association *pAssociation, 
		                         T_DIMSE_Message *pMsg, 
								 T_ASC_PresentationContextID PresID, 
								 imapacs::PacsConfig *pCfg)
	{
		bool bRetVal = false;

		memcpy(&m_CurrentReq, pMsg, sizeof(T_DIMSE_Message));
		memset(&m_CurrentRsp, 0, sizeof(T_DIMSE_Message));

		m_CurrentRsp.CommandField                            = DIMSE_C_MOVE_RSP;
		m_CurrentRsp.msg.CMoveRSP.DataSetType               = DIMSE_DATASET_NULL;	//	Assuming
		m_CurrentRsp.msg.CMoveRSP.DimseStatus               = STATUS_Success;		//	Assuming.
		m_CurrentRsp.msg.CMoveRSP.MessageIDBeingRespondedTo = m_CurrentReq.msg.CMoveRQ.MessageID;
		m_CurrentRsp.msg.CMoveRSP.opts                      = O_MOVE_AFFECTEDSOPCLASSUID;

		strcpy(m_CurrentRsp.msg.CMoveRSP.AffectedSOPClassUID, m_CurrentReq.msg.CMoveRQ.AffectedSOPClassUID);

		m_pAssociation  = pAssociation;
		m_CurrentPresID = PresID;

		m_pCfg = pCfg;

		//	For convenience.
		m_pServerLogger   = pCfg->GetServerLogger();
		m_pModalityLogger = pCfg->GetModalityLogger();
		m_pMailer         = pCfg->GetMailer();

		m_pServerLogger->LogMsg("[%s] - C-MOVE request.", pAssociation->params->DULparams.callingAPTitle);
		m_pModalityLogger->LogMsg("C-MOVE request.");
		m_pMailer->AddLine("C-MOVE request.");

		if(pCfg->GetModalityDebug())
		{
			//	Generate a transaction id to group
			//	transaction related file names.
			srand((unsigned int)time(NULL));
			m_iTransactionId = rand();

			m_sCurrentDebugDir.Format("%s%s", DEBUG_DIRECTORY, pAssociation->params->DULparams.callingAPTitle);
			os::MkDir((char *)m_sCurrentDebugDir);
		}

		m_DataService.SetConfig(pCfg);
		m_DataService.SetCurrentDebugParams(m_sCurrentDebugDir, m_iTransactionId);
		m_Client.SetConfig(pCfg);

		bRetVal = true;

		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	bool C_MOVETransaction::ProcessTransaction()
	{
		bool bRetVal         = false;
		OFCondition cond     = EC_Normal;
		DcmDataset *pResult  = NULL;
		DcmDataset *pRequest = NULL;
		char sDateTime[33]   = {0};
		StringBuffer dump;
		DBGTool dbg;

		if(m_pCfg->GetModalityDebug())
		{
			m_pModalityLogger->LogMsg("--C_MOVETransaction::ProcessTransaction()");
		}

		if(m_pCfg->GetProfiling())
			dbg.StartProfiling(m_pServerLogger, "Receiving request dataset.");

		/////////////////////////////////////////////////////////////////////
		//	Read request dataset.
		cond = DIMSE_receiveDataSetInMemory(m_pAssociation,
										    DIMSE_BLOCKING,
										    0,
										    &m_CurrentDataPresID,
										    &pRequest,
										    NULL,
										    NULL);
		if(m_pCfg->GetProfiling())
			dbg.EndProfiling(m_pServerLogger, NULL, "Dataset received.");

		if(cond.good())
		{
			//	Dump the dataset if debug mode is
			//	on for that AE title.
			if(m_pCfg->GetModalityDebug())
			{
				Time::GetDate(sDateTime);		//	Get the date for the sub-directory.
				dump.Format("%s/%s_C_Move_%d/", (char *)m_sCurrentDebugDir, sDateTime, m_iTransactionId);

				os::MkDir((char *)dump);

				Time::GetTimeOfDay(sDateTime);	//	Get the time for the file.
				dump += "Query_";
				dump += sDateTime;
				dump += ".dcm";

				cond = pRequest->saveFile((char *)dump, EXS_LittleEndianExplicit);
				if(cond.bad())
				{
					m_pModalityLogger->LogMsg("ERROR %s", cond.text());
					m_pModalityLogger->LogMsg("      Failed to save debug dataset.");
				}
			}

			if(m_CurrentPresID == m_CurrentDataPresID)
			{
				/////////////////////////////////////////////////////////////
				//	Check if SOP class UID is supported.
				if(strcmp(m_CurrentReq.msg.CMoveRQ.AffectedSOPClassUID, UID_MOVEPatientRootQueryRetrieveInformationModel) == 0)
				{
					m_pServerLogger->LogMsg("[%s] - Patient Root.", m_pAssociation->params->DULparams.callingAPTitle);
					m_pModalityLogger->LogMsg("[%s] - Patient Root.", m_pAssociation->params->DULparams.callingAPTitle);
					m_pMailer->AddLine("Patient Root.");

					m_pCfg->InsertEvent("C-MOVE (PATIENT)",
										NULL,
										"IMAPACS",
										m_pAssociation->params->DULparams.callingAPTitle,
										NULL,
										NULL,
										NULL,
										NULL,
										NULL,
										NULL,
										NULL, 
										NULL,
										NULL,
										NULL);

					bRetVal = ProcessMoveQuery(pRequest, PATIENT_ROOT);
				}
				else if(strcmp(m_CurrentReq.msg.CMoveRQ.AffectedSOPClassUID, UID_MOVEStudyRootQueryRetrieveInformationModel) == 0)
				{
					m_pServerLogger->LogMsg("[%s] - Study Root.", m_pAssociation->params->DULparams.callingAPTitle);
					m_pModalityLogger->LogMsg("Study Root.");
					m_pMailer->AddLine("Study Root.");

					m_pCfg->InsertEvent("C-MOVE (STUDY)",
										NULL,
										"IMAPACS",
										m_pAssociation->params->DULparams.callingAPTitle,
										NULL,
										NULL,
										NULL,
										NULL,
										NULL,
										NULL,
										NULL,
										NULL,
										NULL,
										NULL);

					bRetVal = ProcessMoveQuery(pRequest, STUDY_ROOT);
				}
				else if(strcmp(m_CurrentReq.msg.CMoveRQ.AffectedSOPClassUID, UID_RETIRED_MOVEPatientStudyOnlyQueryRetrieveInformationModel) == 0)
				{
					m_pServerLogger->LogMsg("[%s] - Patient-Study Root.", m_pAssociation->params->DULparams.callingAPTitle);
					m_pModalityLogger->LogMsg("Patient-Study Root.");
					m_pMailer->AddLine("Patient-Study Root.");

					m_pCfg->InsertEvent("C-MOVE (PATIENT_STUDY)",
										NULL,
										"IMAPACS",
										m_pAssociation->params->DULparams.callingAPTitle,
										NULL,
										NULL,
										NULL,
										NULL,
										NULL,
										NULL,
										NULL,
										NULL,
										NULL,
										NULL);

					bRetVal = ProcessMoveQuery(pRequest, PATIENT_STUDY_ROOT);
				}
				else
				{
					m_CurrentReq.msg.CMoveRSP.DimseStatus = STATUS_MOVE_Failed_SOPClassNotSupported;
					m_CurrentReq.msg.CMoveRSP.DataSetType = DIMSE_DATASET_NULL;

					m_pServerLogger->LogMsg("[%s] - Unsupported or unknown Root query uid : %s", 
						                    m_pAssociation->params->DULparams.callingAPTitle, 
											m_CurrentReq.msg.CMoveRQ.AffectedSOPClassUID);

					m_pModalityLogger->LogMsg("Unsupported or unknown Root query uid : %s", 
						                      m_CurrentReq.msg.CMoveRQ.AffectedSOPClassUID);

					m_pMailer->AddLine("Unsupported or unknown Root query uid : [%s]", m_CurrentReq.msg.CMoveRQ.AffectedSOPClassUID);

					cond = SendC_MOVEResponce(pResult);
					if(cond.bad())
					{
						m_pModalityLogger->LogMsg("ERROR : %s", cond.text());
						m_pMailer->AddLine("%s", cond.text());
					}
				}
			}
			else
			{
				m_pModalityLogger->LogMsg("ERROR : Presentation contexts of command and data don't match.");
				m_pMailer->AddLine("Presentation contexts of command and data don't match.");
			}
		}
		else
		{
			m_pModalityLogger->LogMsg("ERROR : %s", cond.text());
			m_pModalityLogger->LogMsg("        Could not receive C-MOVE dataset.");

			m_pMailer->AddLine("ERROR : %s", cond.text());
			m_pMailer->AddLine("        Could not receive C-MOVE dataset.");
		}

		DICOMUtil::ClearDataset(&pRequest);

		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	void C_MOVETransaction::Shutdown()
	{
		m_DataService.Dispose();
		m_sCurrentDebugDir.Clear();
	}

	/////////////////////////////////////////////////////////////////////////
	//
	bool C_MOVETransaction::ProcessMoveQuery(DcmDataset *pRequest,
											 int iLevel)
	{
		bool bRetVal               = true;
		bool bCancelled            = false;
		char sRemoteIP[21]         = {0};
		char sObjectLocation[1025] = {0};
		QUERY_STATUS Status        = QS_OK_RESULT;
		unsigned short iRemotePort = 0;
		MoveResult *pCandidate     = NULL;
		DcmDataset *pDataset       = NULL;
		StringBuffer FailedUIDs;	//	Failed UID instance list for responce.
		DBGTool dbg;

		FindData MoveParams;
		Queue <MoveResult *> CandidateList;

		/////////////////////////////////////////////////////////////////////////
		//	dcmtk variables.
		OFCondition cond           = EC_Normal;
		STORE_STATUS StoreStatus   = SS_SUCCESS;
		DIC_US DbStatus            = STATUS_Pending;	//	Assuming.
		DcmDataset *pStatus        = NULL;
		DcmDataset *pResult        = NULL;
		DIC_US iCompleted          = 0;
		DIC_US iFailed             = 0;
		DIC_US iWarning            = 0;
		DIC_US iRemaining          = 0;

		if(m_pCfg->GetModalityDebug())
		{
			m_pModalityLogger->LogMsg("--C_MOVETransaction::ProcessMoveQuery()");
		}

		if(ExtractFindData(pRequest, &MoveParams))
		{
			Status = m_DataService.FindCandidates(&MoveParams, &CandidateList);
			if(Status == QS_OK_RESULT)
			{
				iRemaining = CandidateList.GetSize();
				DbStatus   = STATUS_Pending;

				if(m_pCfg->GetMoveDestinationAddress(m_CurrentReq.msg.CMoveRQ.MoveDestination, sRemoteIP, &iRemotePort))
				{
					/////////////////////////////////////////////////////////////
					//	Setup the client.
					m_Client.SetCalledAE(m_CurrentReq.msg.CMoveRQ.MoveDestination);
					m_Client.SetCallingAE(m_pCfg->GetServerAeTitle());
					m_Client.SetConfig(m_pCfg);
					m_Client.SetRemoteIP(sRemoteIP);
					m_Client.SetRemotePort(iRemotePort);

					m_pModalityLogger->LogMsg("Establishing association with %s", m_CurrentReq.msg.CMoveRQ.MoveDestination);
					m_pModalityLogger->LogMsg("Address                       %s", sRemoteIP);
					m_pModalityLogger->LogMsg("Port                          %d", iRemotePort);

					if(m_Client.CreateAssociation())		//	Create the association (allocate memory and such).
					{
						if(m_pCfg->GetProfiling())
							dbg.StartProfiling(m_pServerLogger, "Establishing association.");

						if(m_Client.RequestAssociation())	//	Establish the association (including the negociation).
						{
							if(m_pCfg->GetProfiling())
								dbg.EndProfiling(m_pServerLogger, NULL, "Sql Executed.");

							/////////////////////////////////////////////////
							//	Try to send all objects.
							while(CandidateList.GetCurrent(pCandidate))
							{
								/////////////////////////////////////////////
								//	Check for cancel request.
								cond = DIMSE_checkForCancelRQ(m_pAssociation, m_CurrentPresID, m_CurrentReq.msg.CMoveRQ.MessageID);
								if(cond == EC_Normal)
								{
									//	We received a cancel request.
									bCancelled = true;
									DbStatus   = STATUS_MOVE_Cancel_SubOperationsTerminatedDueToCancelIndication;

									m_pServerLogger->LogMsg("[%s] - Received cancel request.", 
										                    m_pAssociation->params->DULparams.callingAPTitle);
									m_pModalityLogger->LogMsg("Received cancel request.");

									m_pMailer->AddLine("Received cancel request.");

									break;
								}
								else if(cond == DIMSE_NODATAAVAILABLE)
								{
									//	Normal timeout, just continue
									//	since nothing has been received.
								}
								else
								{
									m_pModalityLogger->LogMsg("ERROR : %s", cond.text());
									m_pMailer->AddLine("%s", cond.text());

									DbStatus = STATUS_MOVE_Failed_UnableToProcess;

									break;
								}

								if(m_pCfg->FindObjectLocation(pCandidate->SOPInstanceUID, sObjectLocation))
								{
									--iRemaining;

									if(m_pCfg->GetProfiling())
										dbg.StartProfiling(m_pServerLogger, "Sending object to client.");

									StoreStatus = m_Client.StoreObject(sObjectLocation, m_pAssociation->params->DULparams.callingAPTitle);
									switch(StoreStatus)
									{
										case SS_SUCCESS:
											++iCompleted;
										break;

										case SS_WARNING:
											++iWarning;
										break;

										case SS_ERROR:
										{
											++iFailed;										
											if(FailedUIDs.GetBufferLength() > 0)
												FailedUIDs += "\\";

											FailedUIDs += pCandidate->SOPInstanceUID;
										}
										break;
									}

									if(m_pCfg->GetProfiling())
										dbg.EndProfiling(m_pServerLogger, NULL, "Object sent.");
								}
								else
								{
									++iFailed;										
									if(FailedUIDs.GetBufferLength() > 0)
										FailedUIDs += "\\";

									FailedUIDs += pCandidate->SOPInstanceUID;

									m_pModalityLogger->LogMsg("        Failed to find object with instance %s", pCandidate->SOPInstanceUID);

									m_pMailer->AddLine("Failed to find object with instance [%s]", pCandidate->SOPInstanceUID);
								}

								if(CandidateList.Next())
								{
									DbStatus = STATUS_Pending;

									PrepareMoveResponce(DbStatus, iCompleted, iFailed, iWarning, iRemaining, &FailedUIDs, &pResult, bCancelled);

									if(m_pCfg->GetProfiling())
										dbg.StartProfiling(m_pServerLogger, "Sending responce.");

									cond = SendC_MOVEResponce(pResult);
									if(cond.bad())
									{
										m_pModalityLogger->LogMsg("ERROR : %s", cond.text());
										m_pModalityLogger->LogMsg("        Unable to send move responce.");

										m_pMailer->AddLine("%s", cond.text());
										m_pMailer->AddLine("Unable to send move responce.");
									}
									DICOMUtil::ClearDataset(&pResult);

									if(m_pCfg->GetProfiling())
										dbg.EndProfiling(m_pServerLogger, NULL, "Responce sent.");
								}
								else
								{
									DbStatus = STATUS_Success;
								}
							}
							m_Client.CloseAssociation();
						}
						else
						{
							//	Failed to establish association.
							DbStatus = STATUS_MOVE_Refused_OutOfResourcesSubOperations;

							m_pModalityLogger->LogMsg("ERROR : %s", m_Client.GetLastErrorText());
							m_pModalityLogger->LogMsg("        Failed to establish association data.");

							m_pMailer->AddLine("%s", m_Client.GetLastErrorText());
							m_pMailer->AddLine("Failed to establish association data.");

							bRetVal = false;
						}
					}
					else
					{
						//	Failed to create (allocate) association parameters.
						DbStatus = STATUS_MOVE_Refused_OutOfResourcesSubOperations;
						m_pModalityLogger->LogMsg("ERROR : Failed to allocate association data.");
						m_pMailer->AddLine("Failed to allocate association data.");

						bRetVal = false;
					}
				}
				else
				{
					DbStatus = STATUS_MOVE_Failed_MoveDestinationUnknown;

					m_pModalityLogger->LogMsg("Failed to get remote address and port for move destination AE title : %s", m_CurrentReq.msg.CMoveRQ.MoveDestination);
					m_pMailer->AddLine("Failed to get remote address and port for move destination AE title : [%s]", m_CurrentReq.msg.CMoveRQ.MoveDestination);
					
					bRetVal = false;
				}

			}
			else if(Status == QS_OK_NO_RESULT)
			{
				DbStatus = STATUS_Success;

				m_pModalityLogger->LogMsg("No result.");
				m_pMailer->AddLine("No result.");
			}
			else
			{
				DbStatus = STATUS_MOVE_Refused_OutOfResourcesSubOperations;
			}

			PrepareMoveResponce(DbStatus, iCompleted, iFailed, iWarning, iRemaining, &FailedUIDs, &pResult, bCancelled);

			if(m_pCfg->GetProfiling())
				dbg.StartProfiling(m_pServerLogger, "Sending responce.");

			cond = SendC_MOVEResponce(pResult);
			if(cond.bad())
			{
				m_pModalityLogger->LogMsg("ERROR : %s", cond.text());
				m_pModalityLogger->LogMsg("        Unabel to send move responce.");

				m_pMailer->AddLine("%s", cond.text());
				m_pMailer->AddLine("Unabel to send move responce.");
			}
			DICOMUtil::ClearDataset(&pResult);

			if(m_pCfg->GetProfiling())
				dbg.EndProfiling(m_pServerLogger, NULL, "Responce sent.");
		}

		ClearCandidateList(&CandidateList);

		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	bool C_MOVETransaction::ExtractFindData(DcmDataset *pDataset, FindData *pFindData)
	{
		bool bRetVal = false;
		DBGTool dbg;

		if(m_pCfg->GetProfiling())
			dbg.StartProfiling(m_pServerLogger, "Extracting dataset.");

		DICOMUtil::GetTagFromDataset(pDataset, DCM_QueryRetrieveLevel, pFindData->sQLevel);
		if(pFindData->sQLevel[0])
		{
			/////////////////////////////////////////////////////////////////////////
			//	Extract patient infos.
			DICOMUtil::GetTagFromDataset(pDataset, DCM_PatientID, &pFindData->PatId);
			DICOMUtil::GetTagFromDataset(pDataset, DCM_PatientName, &pFindData->PatName);
			DICOMUtil::GetTagFromDataset(pDataset, DCM_PatientBirthDate, &pFindData->PatBirthDate);
			DICOMUtil::GetTagFromDataset(pDataset, DCM_PatientBirthTime, &pFindData->PatBirthTime);
			DICOMUtil::GetTagFromDataset(pDataset, DCM_OtherPatientIDs, &pFindData->PatOtherId);
			DICOMUtil::GetTagFromDataset(pDataset, DCM_PatientSex, &pFindData->PatSex);
			DICOMUtil::GetTagFromDataset(pDataset, DCM_OtherPatientNames, &pFindData->PatOtherName);
			DICOMUtil::GetTagFromDataset(pDataset, DCM_PatientAge, &pFindData->PatAge);
			DICOMUtil::GetTagFromDataset(pDataset, DCM_PatientSize, &pFindData->PatSize);
			DICOMUtil::GetTagFromDataset(pDataset, DCM_PatientWeight, &pFindData->PatWeight);
			DICOMUtil::GetTagFromDataset(pDataset, DCM_PatientMotherBirthName, &pFindData->PatMotherName);

			/////////////////////////////////////////////////////////////////////////
			//	Extract study infos.
			DICOMUtil::GetTagFromDataset(pDataset, DCM_StudyInstanceUID, &pFindData->StudyInstanceUID);
			DICOMUtil::GetTagFromDataset(pDataset, DCM_StudyDate, &pFindData->StudyDate);
			DICOMUtil::GetTagFromDataset(pDataset, DCM_StudyTime, &pFindData->StudyTime);
			DICOMUtil::GetTagFromDataset(pDataset, DCM_StudyDescription, &pFindData->StudyDesc);
			DICOMUtil::GetTagFromDataset(pDataset, DCM_StudyID, &pFindData->StudyId);
			DICOMUtil::GetTagFromDataset(pDataset, DCM_AccessionNumber, &pFindData->StudyAccessionNumber);
			DICOMUtil::GetTagFromDataset(pDataset, DCM_AdmittingDiagnosesDescription, &pFindData->StudyAdmDiag);
			DICOMUtil::GetTagFromDataset(pDataset, DCM_ReferringPhysicianName, &pFindData->StudyRefPhysicianName);
			DICOMUtil::GetTagFromDataset(pDataset, DCM_NameOfPhysiciansReadingStudy, &pFindData->StudyReadPhysicianName);
			DICOMUtil::GetTagFromDataset(pDataset, DCM_ModalitiesInStudy, &pFindData->StudyModalitiesInStudy);

			/////////////////////////////////////////////////////////////////////////
			//	Extract series infos.
			DICOMUtil::GetTagFromDataset(pDataset, DCM_SeriesInstanceUID, &pFindData->SeriesInstanceUID);
			DICOMUtil::GetTagFromDataset(pDataset, DCM_SeriesDate, &pFindData->SeriesDate);
			DICOMUtil::GetTagFromDataset(pDataset, DCM_SeriesTime, &pFindData->SeriesTime);
			DICOMUtil::GetTagFromDataset(pDataset, DCM_SeriesDescription, &pFindData->SeriesDesc);
			DICOMUtil::GetTagFromDataset(pDataset, DCM_Laterality, &pFindData->SeriesLaterality);
			DICOMUtil::GetTagFromDataset(pDataset, DCM_Modality, &pFindData->SeriesModality);
			DICOMUtil::GetTagFromDataset(pDataset, DCM_SeriesNumber, &pFindData->SeriesNumber);
			DICOMUtil::GetTagFromDataset(pDataset, DCM_ViewPosition, &pFindData->SeriesViewPosition);

			/////////////////////////////////////////////////////////////////////////
			//	Extract object infos.
			DICOMUtil::GetTagFromDataset(pDataset, DCM_SOPClassUID, &pFindData->ObjectSopClasUID);
			DICOMUtil::GetTagFromDataset(pDataset, DCM_SOPInstanceUID, &pFindData->ObjectInstanceUID);
			DICOMUtil::GetTagFromDataset(pDataset, DCM_InstanceNumber, &pFindData->ObjectInstanceNumber);
			DICOMUtil::GetTagFromDataset(pDataset, DCM_AcquisitionDate, &pFindData->ObjectAcquisitionDate);
			DICOMUtil::GetTagFromDataset(pDataset, DCM_AcquisitionTime, &pFindData->ObjectAcquisitionTime);

			bRetVal = true;
		}
		else
		{
			m_pModalityLogger->LogMsg("ERROR : Unable to get query level from request dataset in DICOMSession::ExtractFindData.");
			m_pMailer->AddLine("Unable to get query level from request dataset in DICOMSession::ExtractFindData.");
		}

		if(m_pCfg->GetProfiling())
			dbg.EndProfiling(m_pServerLogger, NULL, "Dataset extracted.");

		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	void C_MOVETransaction::PrepareMoveResponce(DIC_US OperationStatus,
						                        DIC_US iCompleted,
							                    DIC_US iFailed,
							                    DIC_US iWarning,
							                    DIC_US iRemaining,
							                    StringBuffer *pFailedUIDList,
							                    DcmDataset **pResult,
							                    bool bCancelled)
	{
		DIC_US Status = STATUS_Pending;
		if(!bCancelled)
		{
			if(OperationStatus != STATUS_Pending)
			{
				/////////////////////////////////////////////////////////////
				//	Adjust the final status if any sub-operations
				//	failed or had warnings .
				if((iFailed > 0) || (iWarning > 0))
				{
					Status = STATUS_MOVE_Warning_SubOperationsCompleteOneOrMoreFailures;
				}

				/////////////////////////////////////////////////////////////
				//	If all the sub-operations failed then we need to
				//	generate a failed or refused status. cf. DICOM
				//	part 4, C.4.2.3.1, we choose to generate a
				//	"Refused - Out of Resources - Unable to perform
				//	suboperations" status.
				if((iFailed > 0) && ((iCompleted + iWarning) == 0)) 
				{
					Status = STATUS_MOVE_Refused_OutOfResourcesSubOperations;
				}

				//	We no longer have results to send.
				if(iFailed == 0)
				{
					if((OperationStatus != STATUS_MOVE_Refused_OutOfResourcesSubOperations) &&
					   (OperationStatus != STATUS_MOVE_Failed_MoveDestinationUnknown))
						Status = STATUS_Success;
					else
					{
						Status = OperationStatus;
					}
				}
			}

			/////////////////////////////////////////////////////////////////////
			//
			if((m_CurrentRsp.msg.CMoveRSP.DimseStatus != STATUS_Pending) && (m_CurrentRsp.msg.CMoveRSP.DimseStatus != STATUS_Success))
			{
				(*pResult) = CreateMoveResponceDataset(pFailedUIDList->GetBuffer());
			}
		}
		else
		{
			Status = OperationStatus;

			m_pServerLogger->LogMsg("[%s] - C-MOVE cancelled.", m_pAssociation->params->DULparams.callingAPTitle);
			m_pModalityLogger->LogMsg("C-MOVE cancelled.");
			m_pMailer->AddLine("C-MOVE cancelled.");
		}

		/////////////////////////////////////////////////////////////////////
		//	Fill the remaining responce fields.
		m_CurrentRsp.msg.CMoveRSP.DimseStatus                    = Status;
		m_CurrentRsp.msg.CMoveRSP.NumberOfRemainingSubOperations = iRemaining;
		m_CurrentRsp.msg.CMoveRSP.NumberOfCompletedSubOperations = iCompleted;
		m_CurrentRsp.msg.CMoveRSP.NumberOfFailedSubOperations    = iFailed;
		m_CurrentRsp.msg.CMoveRSP.NumberOfWarningSubOperations   = iWarning;
		m_CurrentRsp.msg.CMoveRSP.opts = m_CurrentRsp.msg.CMoveRSP.opts | O_MOVE_NUMBEROFREMAININGSUBOPERATIONS
								                                        | O_MOVE_NUMBEROFCOMPLETEDSUBOPERATIONS
								                                        | O_MOVE_NUMBEROFFAILEDSUBOPERATIONS
								                                        | O_MOVE_NUMBEROFWARNINGSUBOPERATIONS;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	DcmDataset *C_MOVETransaction::CreateMoveResponceDataset(const char *pFailedUIDList)
	{
		OFBool bOk           = OFTrue;
		DcmDataset *pDataset = NULL;

		if(pFailedUIDList)
		{
			pDataset = new DcmDataset;
			if(pDataset)
			{
				bOk = DU_putStringDOElement(pDataset,
											DCM_FailedSOPInstanceUIDList,
											pFailedUIDList);
				if(!bOk)
				{
					m_pModalityLogger->LogMsg("WARNING : Unable to create failed UID list for C-MOVE responce.");
					m_pModalityLogger->LogMsg("          Unable to put value in tag [%s]", pFailedUIDList);

					m_pMailer->AddLine("Unable to create failed UID list for C-MOVE responce.");
					m_pMailer->AddLine("Unable to put value in tag [%s]", pFailedUIDList);
				}
			}
		}
		return pDataset;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	OFCondition C_MOVETransaction::SendC_MOVEResponce(DcmDataset *pResult)
	{
		OFCondition cond    = EC_Normal;
		DcmDataset *pStatus = NULL;

		if(m_pCfg->GetModalityDebug())
		{
			m_pModalityLogger->LogMsg("--C_MOVETransaction::SendC_MOVEResponce()");
		}

		switch(m_CurrentRsp.msg.CMoveRSP.DimseStatus)
		{
			case STATUS_Success:
			case STATUS_Pending:
			{
				//	Success cannot have a Failed SOP Instance UID list (no failures).
				//	Pending may not send such a list. 
				m_CurrentRsp.msg.CMoveRSP.DataSetType = DIMSE_DATASET_NULL;
				pResult = NULL;	//	Zero our local pointer.
			}
			break;

			default:
			{
				//	Send it if provided.
				if(pResult == NULL) 
					m_CurrentRsp.msg.CMoveRSP.DataSetType = DIMSE_DATASET_NULL;
				else 
					m_CurrentRsp.msg.CMoveRSP.DataSetType = DIMSE_DATASET_PRESENT;
			}
		}

     //	Make sure the numberOf fields are conformant with
     //	the status (see Part 4, C.4.2.1.6-9)
    /*pRsp->msg.CMoveRSP.opts = (O_MOVE_AFFECTEDSOPCLASSUID			 |
							   O_MOVE_NUMBEROFREMAININGSUBOPERATIONS |
		                       O_MOVE_NUMBEROFCOMPLETEDSUBOPERATIONS |
							   O_MOVE_NUMBEROFFAILEDSUBOPERATIONS    |
							   O_MOVE_NUMBEROFWARNINGSUBOPERATIONS);*/

		switch(m_CurrentRsp.msg.CMoveRSP.DimseStatus)
		{
			case STATUS_Pending:
			case STATUS_MOVE_Cancel_SubOperationsTerminatedDueToCancelIndication:
			{
				//	Who cares.
			}
			break;

			default:
				//	Remaining sub-operations may not be in responses 
				//	with a status of Warning, Failed, Refused or Successful.
				m_CurrentRsp.msg.CMoveRSP.opts &= (~ O_MOVE_NUMBEROFREMAININGSUBOPERATIONS);
		}

		m_CurrentRsp.msg.CMoveRSP.opts |= m_CurrentRsp.msg.CMoveRSP.opts;

		cond = DIMSE_sendMessageUsingMemoryData(m_pAssociation,
												m_CurrentPresID,
												&m_CurrentRsp,
												NULL,
												pResult,
												NULL,
												NULL);

		DICOMUtil::ClearDataset(&pStatus);
		return cond;
	}
}
