/////////////////////////////////////////////////////////////////////////////
//	C_GETTransaction.h
/////////////////////////////////////////////////////////////////////////////

#include "ITransaction.h"
#include "../Utils/log.h"
#include "../PacsConfig.h"
#include "../Utils/DICOMUtil.h"
#include "../Utils/TimeUtil.h"
#include "../Utils/Str.h"
#include "../Utils/DBGTool.h"
#include "../Utils/CDCMClient.h"
#include "../DatabaseServices/CGetService.h"

#if !defined(__C_GETTRANSACTION_H_INCLUDED__)
#define      __C_GETTRANSACTION_H_INCLUDED__

/////////////////////////////////////////////////////////////////////////////
//
namespace imapacs
{
	class C_GETTransaction : public ITransaction
	{
	public:
		C_GETTransaction():m_pAssociation(NULL),
			               m_pServerLogger(NULL),
						   m_pModalityLogger(NULL),
						   m_pMailer(NULL),
						   m_pCfg(NULL),
						   m_CurrentDataPresID(0x00),
						   m_CurrentPresID(0x00),
						   m_bRemoveAccents(false),
						   m_iTransactionId(0)
		{}
		virtual ~C_GETTransaction()
		{}

		bool Init(T_ASC_Association *pAssociation,
			      T_DIMSE_Message *pMsg,
				  T_ASC_PresentationContextID PresID,
			      PacsConfig *pCfg);

		bool ProcessTransaction();
		void SetTransactionType(const char *transactionType){strcpy(m_transactionType,transactionType);}
		char *GetTransactionType(){return m_transactionType;}
		void Shutdown();

	protected:

		bool ProcessGetQuery(DcmDataset *pReqDataset, int iQueryRoot);
		bool ExtractFindData(DcmDataset *pDataset, FindData *pFindData);
		void PrepareGetResponce(DIC_US OperationStatus,
							    DIC_US iCompleted, 
								DIC_US iFailed,
								DIC_US iWarning,
								DIC_US iRemaining,
								StringBuffer *pFailedUIDList,
								DcmDataset **pResult,
								bool bCancelled);

		bool ScaleImageIntoDataset(DcmDataset *pDataset,
			                       const double fScaleFactorX,
							       const double fScaleFactorY,
							       int iPreserveAspectRatio = 1);

		DcmDataset *CreateMoveResponceDataset(const char *pFailedUIDList);
		OFCondition SendC_GETResponce(DcmDataset *pResult);

		STORE_STATUS ProcessStoreRescale(FindData *pParams, const char *psObjectLocation);

		void ClearCandidateList(Queue <MoveResult *> *pList)
		{
			MoveResult *pItem = NULL;

			while(pList->Pop(pItem))
			{
				delete pItem;
				pItem = NULL;
			}
		}

		T_ASC_Association          *m_pAssociation;
		T_DIMSE_Message             m_CurrentReq;
		T_DIMSE_Message             m_CurrentRsp;
		T_ASC_PresentationContextID m_CurrentDataPresID;
		T_ASC_PresentationContextID m_CurrentPresID;
		Log                        *m_pServerLogger;
		Log                        *m_pModalityLogger;
		Mailer                     *m_pMailer;
		PacsConfig                 *m_pCfg;

		CGetService                 m_DataService;

		CDCMClient                  m_Client;

		char						m_transactionType[64];

		bool                        m_bRemoveAccents;

		StringBuffer                m_sCurrentDebugDir;
		int                         m_iTransactionId;	//	For debug purpose.
	};
}

#endif	//	__C_GETTRANSACTION_H_INCLUDED__
