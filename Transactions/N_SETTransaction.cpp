/////////////////////////////////////////////////////////////////////////////
//	N_SETTransaction.cpp
/////////////////////////////////////////////////////////////////////////////

#include "N_SETTransaction.h"
#include "../Utils/TimeUtil.h"

/////////////////////////////////////////////////////////////////////////////
//
namespace imapacs
{
	/////////////////////////////////////////////////////////////////////////
	//
	bool N_SETTransaction::Init(T_ASC_Association *pAssociation,
			                    T_DIMSE_Message *pMsg,
				                T_ASC_PresentationContextID PresID,
			                    PacsConfig *pCfg)
	{
		bool bRetVal = false;

		memcpy(&m_CurrentReq, pMsg, sizeof(T_DIMSE_Message));	//	We use our own copy of the command.
		memset(&m_CurrentRsp, 0, sizeof(T_DIMSE_Message));

		m_CurrentRsp.CommandField                          = DIMSE_N_SET_RSP;
		m_CurrentRsp.msg.NSetRSP.DataSetType               = DIMSE_DATASET_NULL;	//	Assuming.
		m_CurrentRsp.msg.NSetRSP.DimseStatus               = STATUS_Success;		//	Assuming.
		m_CurrentRsp.msg.NSetRSP.MessageIDBeingRespondedTo = m_CurrentReq.msg.NSetRQ.MessageID;

		strcpy(m_CurrentRsp.msg.NSetRSP.AffectedSOPClassUID, m_CurrentReq.msg.NSetRQ.RequestedSOPClassUID);
		strcpy(m_CurrentRsp.msg.NSetRSP.AffectedSOPInstanceUID, m_CurrentReq.msg.NSetRQ.RequestedSOPInstanceUID);

		m_pAssociation  = pAssociation;
		m_CurrentPresID = PresID;

		m_pCfg = pCfg;

		//	For convenience.
		m_pServerLogger   = pCfg->GetServerLogger();
		m_pModalityLogger = pCfg->GetModalityLogger();
		m_pMailer         = pCfg->GetMailer();

		m_pServerLogger->LogMsg("[%s] - N-SET request.", pAssociation->params->DULparams.callingAPTitle);
		m_pModalityLogger->LogMsg("N-SET request.");
		m_pMailer->AddLine("N-SET request.");

		if(pCfg->GetModalityDebug())
		{
			//	Generate a transaction id to group
			//	transaction related file names.
			srand((unsigned int)time(NULL));
			m_iTransactionId = rand();

			m_sCurrentDebugDir.Format("%s%s", DEBUG_DIRECTORY, pAssociation->params->DULparams.callingAPTitle);
			os::MkDir((char *)m_sCurrentDebugDir);

			m_DataService.SetCurrentDebugParams((char *)m_sCurrentDebugDir, m_iTransactionId);
		}
		m_DataService.SetConfig(pCfg);
		bRetVal = true;

		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	bool N_SETTransaction::ProcessTransaction()
	{
		bool bRetVal     = false;
		OFCondition cond = EC_Normal;
		DBGTool dbg;

		if(strcmp(m_CurrentReq.msg.NSetRQ.RequestedSOPClassUID, UID_RETIRED_GeneralPurposePerformedProcedureStepSOPClass) == 0)
		{
			m_pModalityLogger->LogMsg("PPS update.");
			m_pMailer->AddLine("PPS update.");

			bRetVal = ProcessUpdatePPS();
		}
		if(strcmp(m_CurrentReq.msg.NSetRQ.RequestedSOPClassUID, UID_ModalityPerformedProcedureStepSOPClass) == 0)
		{
			m_pModalityLogger->LogMsg("MPPS update.");
			m_pMailer->AddLine("MPPS update.");

			bRetVal = ProcessUpdateMPPS();
		}
		else
		{
			m_pModalityLogger->LogMsg("ERROR : Unsupported SOP class UID : [%s].", m_CurrentReq.msg.NSetRQ.RequestedSOPInstanceUID);
			m_pMailer->AddLine("ERROR : Unsupported SOP class UID : [%s].", m_CurrentReq.msg.NSetRQ.RequestedSOPInstanceUID);

			m_CurrentRsp.msg.NSetRSP.DimseStatus = STATUS_N_SOPClassNotSupported;

			if(m_pCfg->GetProfiling())
				dbg.StartProfiling(m_pServerLogger, "Sending responce.");

			/////////////////////////////////////////////////////////////////////
			//	Send responce.
			cond = DIMSE_sendMessageUsingMemoryData(m_pAssociation,
													m_CurrentPresID,
													&m_CurrentRsp,
													NULL,
													NULL,
													NULL,
													NULL,
													NULL);

			if(m_pCfg->GetProfiling())
				dbg.EndProfiling(m_pServerLogger, NULL, "Responce sent.");

			if(cond.bad())
			{
				m_pModalityLogger->LogMsg("WARNING : %s", cond.text());
				m_pModalityLogger->LogMsg("          Failed to send responce to client.");

				m_pMailer->AddLine("ERROR : %s", cond.text());
				m_pMailer->AddLine("          Failed to send responce to client.");
			}
		}
		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	void N_SETTransaction::Shutdown()
	{
		m_DataService.Dispose();
		m_sCurrentDebugDir.Clear();
	}

	/////////////////////////////////////////////////////////////////////////
	//
	bool N_SETTransaction::ProcessUpdatePPS()
	{
		bool bRetVal = false;
		CreatePPSData ReqData;

		/////////////////////////////////////////////////////////////////////
		//	dcmtk variables.
		OFCondition cond     = EC_Normal;
		DcmDataset *pResult  = NULL;
		DcmDataset *pRequest = NULL;
		char sDateTime[33]   = {0};
		StringBuffer dump;
		DBGTool dbg;

		if(m_pCfg->GetProfiling())
			dbg.StartProfiling(m_pServerLogger, "Receiving dataset.");

		/////////////////////////////////////////////////////////////////////
		//	Read request dataset.
		cond = DIMSE_receiveDataSetInMemory(m_pAssociation,
											DIMSE_BLOCKING,
											0,
											&m_CurrentDataPresID,
											&pRequest,
											NULL,
											NULL);
		if(m_pCfg->GetProfiling())
			dbg.EndProfiling(m_pServerLogger, NULL, "Dataset received.");

		if(cond.good())
		{
			//	Dump the dataset if debug mode is
			//	on for that AE title.
			if(m_pCfg->GetModalityDebug())
			{
				Time::GetDate(sDateTime);		//	Get the date for the sub-directory.
				dump.Format("%s/%s_C_Get_%d/", (char *)m_sCurrentDebugDir, sDateTime, m_iTransactionId);

				os::MkDir((char *)dump);

				Time::GetTimeOfDay(sDateTime);	//	Get the time for the file.
				dump += "Query_";
				dump += sDateTime;
				dump += ".dcm";

				cond = pRequest->saveFile((char *)dump, EXS_LittleEndianExplicit);
				if(cond.bad())
				{
					m_pModalityLogger->LogMsg("ERROR %s", cond.text());
					m_pModalityLogger->LogMsg("      Failed to save debug dataset.");
				}
			}

			if(m_CurrentDataPresID == m_CurrentPresID)
			{
				if(ExtractUpdatePPSData(pRequest, &ReqData))
				{
					if(m_DataService.PPSExists(m_CurrentReq.msg.NSetRQ.RequestedSOPInstanceUID))
					{
						if(m_DataService.UpdatePPS(&ReqData))
						{
							m_CurrentRsp.msg.NSetRSP.DimseStatus = STATUS_Success;
							bRetVal = true;
						}
						else
						{
							m_CurrentRsp.msg.NCreateRSP.DimseStatus = STATUS_N_AttributeListError;

							m_pModalityLogger->LogMsg("Failed to update PPS into database.");
							m_pMailer->AddLine("Failed to update PPS into database.");
						}
					}
					else
					{
						m_CurrentRsp.msg.NSetRSP.DimseStatus = STATUS_N_NoSuchObjectInstance;

						m_pModalityLogger->LogMsg("ERROR : PPS UID does not exist.");
						m_pMailer->AddLine("ERROR : PPS UID does not exist.");
					}
				}
				else
				{
					m_CurrentRsp.msg.NSetRSP.DimseStatus = STATUS_N_ProcessingFailure;

					m_pModalityLogger->LogMsg("        Unable to extract data from request dataset.");
					m_pMailer->AddLine("Unable to extract data from request dataset.");
				}
			}
			else
			{
				m_CurrentRsp.msg.NCreateRSP.DimseStatus = STATUS_N_ResourceLimitation;

				m_pModalityLogger->LogMsg("ERROR : Request and dataset presentation id mismatch.");
				m_pMailer->AddLine("ERROR : Request and dataset presentation id mismatch.");
			}
		}
		else
		{
			m_CurrentRsp.msg.NCreateRSP.DimseStatus = STATUS_N_ResourceLimitation;

			m_pModalityLogger->LogMsg("ERROR : %s", cond.text());
			m_pModalityLogger->LogMsg("        Could not receive N-SET dataset.");

			m_pMailer->AddLine("%s", cond.text());
			m_pMailer->AddLine("Could not receive N-SET dataset.");
		}

		if(m_pCfg->GetProfiling())
			dbg.StartProfiling(m_pServerLogger, "Sending responce.");

		/////////////////////////////////////////////////////////////////////
		//	Send responce.
		cond = DIMSE_sendMessageUsingMemoryData(m_pAssociation,
												m_CurrentPresID,
												&m_CurrentRsp,
												NULL,
												NULL,
												NULL,
												NULL,
												NULL);

		if(m_pCfg->GetProfiling())
			dbg.EndProfiling(m_pServerLogger, NULL, "Responce sent.");

		if(cond.bad())
		{
			m_pModalityLogger->LogMsg("WARNING : %s", cond.text());
			m_pModalityLogger->LogMsg("          Failed to send responce to client.");

			m_pMailer->AddLine("ERROR : %s", cond.text());
			m_pMailer->AddLine("          Failed to send responce to client.");
		}
		DICOMUtil::ClearDataset(&pRequest);
		DICOMUtil::ClearDataset(&pResult);

		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	bool N_SETTransaction::ExtractUpdatePPSData(DcmDataset *pDataset, CreatePPSData *pReqData)
	{
		bool bRetVal = false;
		DcmSequenceOfItems *pSeq    = NULL;
		DcmSequenceOfItems *pSubSeq = NULL;
		DcmItem *pItem              = NULL;
		DcmItem *pSubItem           = NULL;
		OFCondition cond            = EC_Normal;
		DBGTool dbg;

		if(pDataset)
		{
			if(pReqData)
			{
				if(m_pCfg->GetProfiling())
					dbg.StartProfiling(m_pServerLogger, "Extracting dataset.");

				strcpy(pReqData->sPPSInstanceUID, m_CurrentReq.msg.NCreateRQ.AffectedSOPInstanceUID);

				/////////////////////////////////////////////////////////////
				//	PPS data.
				DICOMUtil::GetTagFromDataset(pDataset, DCM_PerformedProcedureStepID, pReqData->sPPSId);
				DICOMUtil::GetTagFromDataset(pDataset, DCM_PerformedProcedureStepStartDate, pReqData->sPPSStartDate);
				DICOMUtil::GetTagFromDataset(pDataset, DCM_PerformedProcedureStepStartTime, pReqData->sPPSStartTime);
				DICOMUtil::GetTagFromDataset(pDataset, DCM_RETIRED_GeneralPurposePerformedProcedureStepStatus, pReqData->sPPSStatus);

				/////////////////////////////////////////////////////////////
				//	Referenced GP-SPS.
				cond = pDataset->findAndGetSequence(DCM_RETIRED_ReferencedGeneralPurposeScheduledProcedureStepSequence, pSeq);
				if(cond.good() && pSeq)
				{
					pItem = pSeq->getItem(0);
					if(pItem)
					{
						DICOMUtil::GetTagFromDataset(pItem, DCM_ReferencedSOPClassUID, pReqData->sRefSPSSOPClassUID);
						DICOMUtil::GetTagFromDataset(pItem, DCM_ReferencedSOPInstanceUID, pReqData->sRefSPSSOPInstanceUID);
						DICOMUtil::GetTagFromDataset(pItem, DCM_RETIRED_ReferencedGeneralPurposeScheduledProcedureStepTransactionUID, pReqData->sTransactionUID);
					}
				}

				/////////////////////////////////////////////////////////////
				//	Performer sequence (the id and name of the physycian
				//	creating the PPS.
				pSeq = NULL;
				cond = pDataset->findAndGetSequence(DCM_ActualHumanPerformersSequence, pSeq);
				if(cond.good() && pSeq)
				{
					pItem = pSeq->getItem(0);
					if(pItem)
					{
						cond = pItem->findAndGetSequence(DCM_HumanPerformerCodeSequence, pSubSeq);
						if(cond.good() && pSubSeq)
						{
							pSubItem = pSubSeq->getItem(0);
							if(pSubItem)
							{
								DICOMUtil::GetTagFromDataset(pSubItem, DCM_CodeValue, pReqData->sPerformerId);
//								DICOMUtil::GetTagFromDataset(pSubItem, DCM_CodingSchemeDesignator, );
//								DICOMUtil::GetTagFromDataset(pSubItem, DCM_CodeMeaning, );
							}
						}
						DICOMUtil::GetTagFromDataset(pItem, DCM_HumanPerformerName, pReqData->sPerformerName);
						DICOMUtil::GetTagFromDataset(pItem, DCM_HumanPerformerOrganization, pReqData->sPerformerOrganization);
					}
				}

				/////////////////////////////////////////////////////////////
				//	Performed station (basically the AE title).
				pSeq = NULL;
				cond = pDataset->findAndGetSequence(DCM_PerformedStationNameCodeSequence, pSeq);
				if(cond.good() && pSeq)
				{
					pItem = pSeq->getItem(0);
					if(pItem)
					{
						DICOMUtil::GetTagFromDataset(pItem, DCM_CodeValue, pReqData->sPerformedStationName);
//						DICOMUtil::GetTagFromDataset(pItem, DCM_CodingSchemeDesignator, );
//						DICOMUtil::GetTagFromDataset(pItem, DCM_CodeMeaning, );
					}
				}

				/////////////////////////////////////////////////////////////
				//	Workitem code sequence.
				pSeq = NULL;
				cond = pDataset->findAndGetSequence(DCM_PerformedWorkitemCodeSequence, pSeq);
				if(cond.good() && pSeq)
				{
					pItem = pSeq->getItem(0);
					if(pItem)
					{
						DICOMUtil::GetTagFromDataset(pItem, DCM_CodeValue, pReqData->sWorkitemCode);
//						DICOMUtil::GetTagFromDataset(pItem, DCM_CodingSchemeDesignator, );
//						DICOMUtil::GetTagFromDataset(pItem, DCM_CodeMeaning, );
					}
				}
				bRetVal = true;

				if(m_pCfg->GetProfiling())
					dbg.EndProfiling(m_pServerLogger, NULL, "Dataset extracted.");
			}
		}
		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	bool N_SETTransaction::ProcessUpdateMPPS()
	{
		bool bRetVal         = false;
		OFCondition cond     = EC_Normal;
		DcmDataset *pRequest = NULL;
		DcmDataset *pResult  = NULL;
		char sDateTime[33]   = {0};
		MPPSData ReqData;
		StringBuffer dump;
		DBGTool dbg;

		if(m_pCfg->GetProfiling())
			dbg.StartProfiling(m_pServerLogger, "Receicing dataset.");

		/////////////////////////////////////////////////////////////////////
		//	Read request dataset.
		cond = DIMSE_receiveDataSetInMemory(m_pAssociation,
											DIMSE_BLOCKING,
											0,
											&m_CurrentDataPresID,
											&pRequest,
											NULL,
											NULL);

		if(m_pCfg->GetProfiling())
			dbg.EndProfiling(m_pServerLogger, NULL, "Dataset received.");

		if(m_pCfg->GetModalityDebug())
		{
			Time::GetDate(sDateTime);		//	Get the date for the sub-directory.
			dump.Format("%s/%s_UpdateMPPS_%d/", (char *)m_sCurrentDebugDir, sDateTime, m_iTransactionId);

			os::MkDir((char *)dump);

			Time::GetTimeOfDay(sDateTime);	//	Get the time for the file.
			dump += "Query_";
			dump += sDateTime;
			dump += ".dcm";

			cond = pRequest->saveFile((char *)dump, EXS_LittleEndianExplicit);
			if(cond.bad())
			{
				m_pModalityLogger->LogMsg("ERROR %s", cond.text());
				m_pModalityLogger->LogMsg("      Failed to save debug dataset.");
			}
			dump.Clear();
		}

		if(cond.good())
		{
			if(m_CurrentDataPresID == m_CurrentPresID)
			{
				if(ExtractUpdateMPPSData(pRequest, &ReqData))
				{
					if(m_DataService.MPPSExists(ReqData.sTransactionUID))
					{
						if(m_DataService.UpdateMPPS(&ReqData))
						{
							m_CurrentRsp.msg.NSetRSP.DimseStatus = STATUS_Success;
							bRetVal = true;
						}
						else
						{
							m_CurrentRsp.msg.NCreateRSP.DimseStatus = STATUS_N_AttributeListError;

							m_pModalityLogger->LogMsg("ERROR : Failed to update MPPS with uid %s", ReqData.sTransactionUID);
							m_pMailer->AddLine("ERROR : Failed to update MPPS with uid %s", ReqData.sTransactionUID);
						}
					}
					else
					{
						m_CurrentRsp.msg.NSetRSP.DimseStatus = STATUS_N_NoSuchObjectInstance;

						m_pModalityLogger->LogMsg("ERROR : MPPS with uid %s does not exist.", ReqData.sTransactionUID);
						m_pMailer->AddLine("ERROR : MPPS with uid %s does not exist.", ReqData.sTransactionUID);
					}
				}
				else
				{
					m_CurrentRsp.msg.NCreateRSP.DimseStatus = STATUS_N_ProcessingFailure;

					m_pModalityLogger->LogMsg("        Unable to extract data from request dataset.");
					m_pMailer->AddLine("Unable to extract data from request dataset.");
				}
			}
			else
			{
				m_CurrentRsp.msg.NCreateRSP.DimseStatus = STATUS_N_ResourceLimitation;

				m_pModalityLogger->LogMsg("ERROR : Request and dataset presentation id mismatch.");
				m_pMailer->AddLine("ERROR : Request and dataset presentation id mismatch.");
			}
		}
		else
		{
			m_pModalityLogger->LogMsg("ERROR : %s", cond.text());
			m_pModalityLogger->LogMsg("        Failed to receive request dataset.");
		}

		if(m_pCfg->GetProfiling())
			dbg.StartProfiling(m_pServerLogger, "Sending responce.");

		cond = DIMSE_sendMessageUsingMemoryData(m_pAssociation,
												m_CurrentPresID,
												&m_CurrentRsp,
												NULL,
												NULL,
												NULL,
												NULL,
												NULL);
		if(m_pCfg->GetProfiling())
			dbg.EndProfiling(m_pServerLogger, NULL, "Responce sent.");

		if(m_pCfg->GetModalityDebug())
		{
			Time::GetDate(sDateTime);		//	Get the date for the sub-directory.
			dump.Format("%s/%s_UpdateMPPS_%d/", (char *)m_sCurrentDebugDir, sDateTime, m_iTransactionId);

			os::MkDir((char *)dump);

			Time::GetTimeOfDay(sDateTime);	//	Get the time for the file.
			dump += "Rsp_";
			dump += sDateTime;
			dump += ".dcm";

			cond = pRequest->saveFile((char *)dump, EXS_LittleEndianExplicit);
			if(cond.bad())
			{
				m_pModalityLogger->LogMsg("ERROR %s", cond.text());
				m_pModalityLogger->LogMsg("      Failed to save debug dataset.");
			}
			dump.Clear();
		}

		if(cond.bad())
		{
			m_pModalityLogger->LogMsg("WARNING : Failed to send responce to client.");
			m_pModalityLogger->LogMsg("          %s", cond.text());

			m_pMailer->AddLine("WARNING : Failed to send responce to client.");
			m_pMailer->AddLine("          %s", cond.text());
		}
		DICOMUtil::ClearDataset(&pRequest);
		DICOMUtil::ClearDataset(&pResult);

		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	bool N_SETTransaction::ExtractUpdateMPPSData(DcmDataset *pDataset, MPPSData *pReqData)
	{
		bool bRetVal = false;

		if(pDataset && pReqData)
		{
			strcpy(pReqData->sTransactionUID, m_CurrentReq.msg.NSetRQ.RequestedSOPInstanceUID);

			DICOMUtil::GetTagFromDataset(pDataset, DCM_PatientName, pReqData->sPatientName);
			DICOMUtil::GetTagFromDataset(pDataset, DCM_PatientID, pReqData->sPatientId);
			DICOMUtil::GetTagFromDataset(pDataset, DCM_PerformedStationAETitle, pReqData->sPerformedStationAeTitle);
			DICOMUtil::GetTagFromDataset(pDataset, DCM_PerformedStationName, pReqData->sPerformedStationName);
			DICOMUtil::GetTagFromDataset(pDataset, DCM_PerformedLocation, pReqData->sPerformerLocation);
			DICOMUtil::GetTagFromDataset(pDataset, DCM_PerformedProcedureStepStartDate, pReqData->sPPSStartDate);
			DICOMUtil::GetTagFromDataset(pDataset, DCM_PerformedProcedureStepStartTime, pReqData->sPPSStartTime);
			DICOMUtil::GetTagFromDataset(pDataset, DCM_PerformedProcedureStepEndDate, pReqData->sPPSEndDate);
			DICOMUtil::GetTagFromDataset(pDataset, DCM_PerformedProcedureStepEndTime, pReqData->sPPSEndTime);
			DICOMUtil::GetTagFromDataset(pDataset, DCM_PerformedProcedureStepID, pReqData->sPPSId);
			DICOMUtil::GetTagFromDataset(pDataset, DCM_PerformedProcedureStepDescription, pReqData->sPPSDescription);
			DICOMUtil::GetTagFromDataset(pDataset, DCM_CommentsOnThePerformedProcedureStep, pReqData->sPPSComment);
			DICOMUtil::GetTagFromDataset(pDataset, DCM_Modality, pReqData->sModality);
			DICOMUtil::GetTagFromDataset(pDataset, DCM_StudyID, pReqData->sStudyId);
			DICOMUtil::GetTagFromDataset(pDataset, DCM_PerformedProcedureStepStatus, pReqData->sPPSStatus);

			bRetVal = true;
		}
		return bRetVal;
	}
}
