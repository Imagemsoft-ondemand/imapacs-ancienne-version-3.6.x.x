//////////////////////////////////////////////////////////////////////////////
//
//	File			:	myservice.h
//	Description		:	Definition of CNTService.
//	Author			:	Laval Bolduc.
//	Modif.			:	2002/08/31.
//
//////////////////////////////////////////////////////////////////////////////

#ifndef __NTSERVICE_H__
#define __NTSERVICE_H__

#include <Winsock2.h>

#include <Winsvc.h>
#include <stdio.h>
#include <time.h>

#define SERVICE_CONTROL_USER		128
#define MAX_ERROR_MESSAGE_LENGTH	2049

#include "PacsServer.h"

///////////////////////////////////////////////
// To write events in the sevice's log file.
void LogEv(char* pathname,char *msg);

class CNTService
{
public:

	/////////////////////////////////////////////////////////////////
	// Service internal functions
    CNTService(const char* szServiceName);
    virtual ~CNTService();

    BOOL ParseStandardArgs(int argc, char* argv[]);
    BOOL IsInstalled();
    BOOL Install();
    BOOL Uninstall();

    BOOL StartService();
    void SetStatus(DWORD dwState);
    BOOL Initialize();
    void Run();
	BOOL OnInit();
    void OnStop();
    void OnInterrogate();
    void OnPause();
    void OnContinue();
    void OnShutdown();

	void ServiceStopped();

    BOOL OnUserControl(DWORD dwOpcode);
    
    /////////////////////////////////////////////////////////////////
	// Statics Callbacks.
    static void WINAPI ServiceMain(DWORD dwArgc, LPTSTR* lpszArgv);
    static void WINAPI Handler(DWORD dwOpcode);
	//
	/////////////////////////////////////////////////////////////////

	/////////////////////////////////////////////////////////////////
	// Service internal private variables.
	char m_szServiceName[64];
    int m_iMajorVersion;
    int m_iMinorVersion;
    SERVICE_STATUS_HANDLE m_hServiceStatus;
    SERVICE_STATUS m_Status;
    BOOL m_bIsRunning;

	char m_logfname[_MAX_PATH];
	char m_ServiceMessage[MAX_ERROR_MESSAGE_LENGTH];

    HANDLE m_hEventSource;

    static CNTService* m_pThis; // Nasty hack to get object ptr
	//
	/////////////////////////////////////////////////////////////////

private:

	void GetLastErrorText(char *pMessage, int iMaxLength);

	imapacs::PacsServer mPacsServer;
};

#endif // __NTSERVICE_H__
