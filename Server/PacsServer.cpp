/////////////////////////////////////////////////////////////////////////////
//	File		:	PacsServer.h
//	Description	:	Implementation.
/////////////////////////////////////////////////////////////////////////////

#include "PacsServer.h"
#include "SessionThread.h"
#include "../Utils/PacsCommon.h"


/////////////////////////////////////////////////////////////////////////////
//
namespace imapacs
{

/////////////////////////////////////////////////////////////////////////////
//
bool PacsServer::Initialize()
{
	bool bRetVal = false;

	if(ReadConfiguration(CONFIGURATION_FILE))
	{
		if(mAccepter.Bind(miPort, 10))
		{
			bRetVal = true;
		}
	}
	return bRetVal;
}

/////////////////////////////////////////////////////////////////////////////
//
int PacsServer::Run()
{
	if(Initialize())
	{
		while(m_bIsRunning)
		{
			if(mAccepter.Accept(0,100000) > 0)
			{
				SessionThread *pSession = new(std::nothrow) SessionThread(mAccepter.GetDescriptor());
				if(pSession)
				{
					if(pSession->Start())
					{
						mSessionList.Add(pSession);
					}
					else
					{
						delete pSession;
						pSession = 0;
					}
				}
			}
			CleanupSessionList();
		}
		Shutdown();
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////
//
void PacsServer::Shutdown()
{
	mAccepter.Close();
	CleanupSessionList(true);
}

/////////////////////////////////////////////////////////////////////////////
//
bool PacsServer::ReadConfiguration(const char *pCfgFile)
{
	bool bRetVal    = false;
	char sLine[256] = {0};
	char *pTmp      = NULL;

	if(pCfgFile)
	{
		FILE *fp = fopen(pCfgFile, "r");
		if(fp)
		{
			while(fgets(sLine,512,fp) != NULL)
			{
				pTmp = strpbrk(sLine, "=");
				if(pTmp)
				{
					while(fgets(sLine,512,fp) != NULL)
					{
						pTmp = strpbrk(sLine, "=");
						if(pTmp)
						{
							pTmp[strlen(pTmp)-1] = 0x00;
							if(strncmp(sLine, "port", 4) == 0)
							{
								miPort = static_cast<unsigned short>(atoi(&pTmp[1]));
							}
						}
					}

					if(miPort > 0)
					{
						bRetVal = true;
					}
				}
			}
			fclose(fp);
			fp = 0;
		}
	}
	return bRetVal;
}

/////////////////////////////////////////////////////////////////////////////
//
void PacsServer::CleanupSessionList(bool bClearAll)
{
	SessionThread *pSession = 0;

	if(!bClearAll)
	{
		if(mSessionList.SetAtFirst())
		{
			while(mSessionList.GetCurrent(pSession))
			{
				if(!pSession->IsRunning())
				{
					pSession->WaitForThread();

					delete pSession;
					pSession = 0;

					mSessionList.RemoveCurrent();
				}
				mSessionList.Next();

				break;	//	Just remove one.
			}
		}
	}
	else
	{
		//	Stop and clear all the sessions.
		//	This will wait for each session
		//	to naturally terminate so as
		//	long a client maintains an
		//	association, this will wait.
		while(mSessionList.Pop(pSession))
		{
			pSession->Stop();
			delete pSession;
			pSession = 0;
		}
	}
}

}	//	End namespace imapacs.
