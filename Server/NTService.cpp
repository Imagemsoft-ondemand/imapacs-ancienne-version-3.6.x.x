/////////////////////////////////////////////////////////////////////////////
//
//	File			:	NTService.cpp
//	Description		:	Implementation of the CNTService class.
//	Author			:	Laval Bolduc.
//	Modif.			:	2002/08/31
//
/////////////////////////////////////////////////////////////////////////////

#include "NTService.h"

/////////////////////////////////////////////////////////////////////////////
// Statics variable.
CNTService* CNTService::m_pThis = NULL;

/////////////////////////////////////////////////////////////////////////////
// Construction.
CNTService::CNTService(const char* szServiceName)
{
    /////////////////////////////////////////////////////////////////////////
	// Copies current object's address to access
	// it from the static membre function.
    // CAUTION: this limit the application to only one object CNTService.
    m_pThis = this;
    
    /////////////////////////////////////////////////////////////////////////
	// Default version number.
    strncpy(m_szServiceName, szServiceName, 40);
    m_iMajorVersion = 1;
    m_iMinorVersion = 0;
    m_hEventSource = NULL;

    /////////////////////////////////////////////////////////////////////////
	// �tat initial du service.
    m_hServiceStatus = NULL;
    m_Status.dwServiceType = SERVICE_WIN32_OWN_PROCESS;
    m_Status.dwCurrentState = SERVICE_STOPPED;
    m_Status.dwControlsAccepted = SERVICE_ACCEPT_STOP;
    m_Status.dwWin32ExitCode = 0;
    m_Status.dwServiceSpecificExitCode = 0;
    m_Status.dwCheckPoint = 0;
    m_Status.dwWaitHint = 0;
    m_bIsRunning = FALSE;

	::GetModuleFileName(NULL, m_logfname, _MAX_PATH);

	m_logfname[strlen(m_logfname) - 3] = 0x00;
	strcat(m_logfname,"log");
}

/////////////////////////////////////////////////////////////////////////////
// Destruction.
CNTService::~CNTService()
{
    if(m_hEventSource) 
	{
        ::DeregisterEventSource(m_hEventSource);
    }
}

/////////////////////////////////////////////////////////////////////////////
// Parse command line arguments.
// Return TRUE argumenta found are ok, FALSE otherwise.
BOOL CNTService::ParseStandardArgs(int argc, char* argv[])
{
    /////////////////////////////////////////////////////////////////////////
	// Check if we know command line argument.
    if(argc <= 1) return FALSE;

    if(_stricmp(argv[1], "-v") == 0) 
	{

        printf("%s Version %d.%d\n",
               m_szServiceName, m_iMajorVersion, m_iMinorVersion);

        printf("Le service %s installe!\n",
               IsInstalled() ? "est" : "n'est pas");

        return TRUE; // Say we processed the argument

    } 
	else if(_stricmp(argv[1], "-i") == 0) 
	{

		/////////////////////////////////////////////////////////////////////
		// Installation request.
		if(IsInstalled()) 
		{
			printf("%s est deja installe\n", m_szServiceName);
		}
		else 
		{
			/////////////////////////////////////////////////////////////////
			// Try to instal a running copy.
			if(Install()) 
			{
				printf("%s installe!\n", m_szServiceName);
			} 
			else 
				printf("%s Installation manquee. Erreur %d\n", m_szServiceName, GetLastError());
		}
		return TRUE;

    } 
	else if(_stricmp(argv[1], "-u") == 0) 
	{

        /////////////////////////////////////////////////////////////////////
		// Uninstall request.
        if(!IsInstalled()) 
		{
            printf("%s n'est pas installe!\n", m_szServiceName);
        } 
		else 
		{
			/////////////////////////////////////////////////////////////////
			// Try to unistall copy.
            if(Uninstall()) 
			{
                /////////////////////////////////////////////////////////////
				// Use path executable path.
                char szFilePath[_MAX_PATH];
                ::GetModuleFileName(NULL, szFilePath, sizeof(szFilePath));

                printf("%s desintalle. (Vous devez detruire le fichier (%s) vous-m�me.)\n",
                       m_szServiceName, szFilePath);
            } 
			else 
                printf("Impossible d'enlever %s. Erreur %d\n", m_szServiceName, GetLastError());
        }
        return TRUE;    
    }  
	/////////////////////////////////////////////////////////////////////////
	// We do not know anything about these command
	// line arguments.
    return FALSE;
}

/////////////////////////////////////////////////////////////////////////////
// Check if the service is already installed.
BOOL CNTService::IsInstalled()
{
    BOOL bResult = FALSE;

    /////////////////////////////////////////////////////////////////////////
	// Open the  Service Control Manager
    SC_HANDLE hSCM = ::OpenSCManager(NULL,						// Local machine
                                     NULL,						// ServicesActive database
                                     SC_MANAGER_ALL_ACCESS);	// Full access
    if(hSCM) 
	{
        /////////////////////////////////////////////////////////////////////
		// Try to open service.
        SC_HANDLE hService = ::OpenService(hSCM,
                                           m_szServiceName,
                                           SERVICE_QUERY_CONFIG);
        if(hService) 
		{
            bResult = TRUE;
            ::CloseServiceHandle(hService);
        }

        ::CloseServiceHandle(hSCM);
    }
    return bResult;
}

/////////////////////////////////////////////////////////////////////////////
// Service installation.
BOOL CNTService::Install()
{
	char szModulePath[_MAX_PATH] = {0};

    /////////////////////////////////////////////////////////////////////////
	// Open the Service Control Manager
    SC_HANDLE hSCM = ::OpenSCManager(NULL,						// Local machine
                                     NULL,						// ServicesActive database
                                     SC_MANAGER_ALL_ACCESS);	// Full access
    if(!hSCM)
	{
		GetLastErrorText(m_ServiceMessage, sizeof(m_ServiceMessage));
		return FALSE;
	}

    /////////////////////////////////////////////////////////////////////////
	// Take the executable's path.
	char szFilePath[_MAX_PATH] = {0};
    ::GetModuleFileName(NULL, szFilePath, sizeof(szFilePath));

	strcpy(szModulePath,m_logfname);
	for(int i = strlen(szModulePath); i>0; --i)
	{
		if(szModulePath[i] == '\\')
		{
			szModulePath[i+1] = 0x00;
			break;
		}
	}

    /////////////////////////////////////////////////////////////////////////
	// Create the service.
    SC_HANDLE hService = ::CreateService(hSCM,
                           m_szServiceName,
                           m_szServiceName,
                           SERVICE_ALL_ACCESS | SERVICE_STOP,
                           SERVICE_WIN32_OWN_PROCESS | SERVICE_INTERACTIVE_PROCESS,
						   SERVICE_AUTO_START,    // Start automatically.
                           SERVICE_ERROR_NORMAL,
                           szFilePath,
                           NULL,	// Group Order.
                           NULL,	// Dependences.
                           NULL,
                           NULL,	// Account.
                           NULL);	// Password.
    if (!hService) 
	{
        ::CloseServiceHandle(hSCM);
		GetLastErrorText(m_ServiceMessage, sizeof(m_ServiceMessage));

        return FALSE;
    }

    char szKey[256];
    HKEY hKey = NULL;

    strcpy(szKey, "SYSTEM\\CurrentControlSet\\Services\\EventLog\\Application\\");
    strcat(szKey, m_szServiceName);
    if (::RegCreateKey(HKEY_LOCAL_MACHINE, szKey, &hKey) != ERROR_SUCCESS) 
	{
        ::CloseServiceHandle(hService);
        ::CloseServiceHandle(hSCM);

		GetLastErrorText(m_ServiceMessage, sizeof(m_ServiceMessage));

        return FALSE;
    }

    /////////////////////////////////////////////////////////////////////////
	// Add the Event ID message-file name at 'EventMessageFile' subkey.
    ::RegSetValueEx(hKey,
                    "EventMessageFile",
                    0,
                    REG_EXPAND_SZ, 
                    (CONST BYTE*)szFilePath,
                    strlen(szFilePath) + 1);

    ///////////////////////////////////////////////
	// Flags intialisation.
    DWORD dwData = EVENTLOG_ERROR_TYPE | EVENTLOG_WARNING_TYPE | EVENTLOG_INFORMATION_TYPE;
    ::RegSetValueEx(hKey,
                    "TypesSupported",
                    0,
                    REG_DWORD,
                    (CONST BYTE*)&dwData,
                     sizeof(DWORD));

    ::RegCloseKey(hKey);

	if(::RegCreateKey(HKEY_LOCAL_MACHINE,"SOFTWARE\\ServiceRegistry\\",&hKey) != ERROR_SUCCESS)
	{
		::CloseServiceHandle(hService);
        ::CloseServiceHandle(hSCM);

		GetLastErrorText(m_ServiceMessage, sizeof(m_ServiceMessage));

		return FALSE;
	}

	
	::RegSetValueEx(hKey,
		            "regpath",
					0,
					REG_SZ,
					(CONST BYTE*)szModulePath,
					strlen(szModulePath) + 1);

	::RegCloseKey(hKey);
	hKey = NULL;

    ::CloseServiceHandle(hService);
    ::CloseServiceHandle(hSCM);

	sprintf(m_ServiceMessage,"Service %s installed.\n", this->m_szServiceName);
	LogEv(m_logfname,m_ServiceMessage);

    return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// Unistall the service (dose not remove the
// executable file.
BOOL CNTService::Uninstall()
{
    /////////////////////////////////////////////////////////////////////////
	// Open the Service Control Manager.
    SC_HANDLE hSCM = ::OpenSCManager(NULL,						// Local machine.
                                     NULL,						// ServicesActive database.
                                     SC_MANAGER_ALL_ACCESS);	// Full access.
    if (!hSCM) return FALSE;

    BOOL bResult = FALSE;
    SC_HANDLE hService = ::OpenService(hSCM,
                                       m_szServiceName,
                                       DELETE);
    if (hService) 
	{
        if (::DeleteService(hService)) 
		{
            bResult = TRUE;

			sprintf(m_ServiceMessage,"Service %s unistalled.\n", m_szServiceName);
			LogEv(m_logfname,m_ServiceMessage);
        } 
		else 
		{
			GetLastErrorText(m_ServiceMessage, sizeof(m_ServiceMessage));
        }
        ::CloseServiceHandle(hService);
    }
    ::CloseServiceHandle(hSCM);

    return bResult;
}

/////////////////////////////////////////////////////////////////////////////
// D�marage et enregistrement du service.
BOOL CNTService::StartService()
{
    SERVICE_TABLE_ENTRY st[] = 
	{
        {m_szServiceName, ServiceMain},
        {NULL, NULL}
    };

    BOOL b = ::StartServiceCtrlDispatcher(st);
	return b;
}

/////////////////////////////////////////////////////////////////////////////
// Main (callback).
void CNTService::ServiceMain(DWORD dwArgc, LPTSTR* lpszArgv)
{
    /////////////////////////////////////////////////////////////////////////
	// Get pointer on the object.
    CNTService* pService = m_pThis;

    pService->m_Status.dwCurrentState = SERVICE_START_PENDING;
    pService->m_hServiceStatus = RegisterServiceCtrlHandler(pService->m_szServiceName,
                                                            Handler);
    if(pService->m_hServiceStatus == NULL) 
	{
		sprintf(pService->m_ServiceMessage,"Service %s Control handler not installed.\n",pService->m_szServiceName);
		LogEv(pService->m_logfname,pService->m_ServiceMessage);

        return;
    }

    if(pService->Initialize()) 
	{
        ///////////////////////////////////////////////////////////////////// 
		// When Run returns, the service as stopped.
        pService->m_bIsRunning                = TRUE;
        pService->m_Status.dwWin32ExitCode    = 0;
        pService->m_Status.dwCheckPoint       = 0;
		pService->m_Status.dwControlsAccepted = (SERVICE_ACCEPT_STOP | 
												 SERVICE_ACCEPT_PAUSE_CONTINUE |
												 SERVICE_ACCEPT_SHUTDOWN);
        pService->m_Status.dwWaitHint = 0;

        pService->Run();

		pService->ServiceStopped();
    }

    /////////////////////////////////////////////////////////////////////////
	// Notify the Service Control Manager.
    pService->SetStatus(SERVICE_STOPPED);
}

/////////////////////////////////////////////////////////////////////////////
// Status function.
void CNTService::SetStatus(DWORD dwState)
{
    m_Status.dwCurrentState = dwState;
    ::SetServiceStatus(m_hServiceStatus, &m_Status);
}

/////////////////////////////////////////////////////////////////////////////
// Service initialisation.
BOOL CNTService::Initialize()
{
    SetStatus(SERVICE_START_PENDING);
    
    BOOL bResult = OnInit(); 
    

	/////////////////////////////////////////////////////////////////////////
	// Final state.
    m_Status.dwWin32ExitCode = GetLastError();
    m_Status.dwCheckPoint = 0;
    m_Status.dwWaitHint = 0;

    if(!bResult) 
	{
		sprintf(m_ServiceMessage,"Service %s initialisation msg failed.\n",m_szServiceName);
		LogEv(m_logfname,m_ServiceMessage);

        SetStatus(SERVICE_STOPPED);
        return FALSE;    
    }
    
	sprintf(m_ServiceMessage,"Service %s started.\n",m_szServiceName);
	LogEv(m_logfname,m_ServiceMessage);

    SetStatus(SERVICE_RUNNING);

    return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// This function dose the job the service as to do. When it
// returns, the service as stoped. This where the user's code
// is going.
void CNTService::Run()
{
	if(mPacsServer.Start())
	{
		m_bIsRunning = TRUE;
		mPacsServer.WaitForThread();
	}
}

/////////////////////////////////////////////////////////////////////////////
// Static (callback) message handler.
void CNTService::Handler(DWORD dwOpcode)
{
    /////////////////////////////////////////////////////////////////////////
	// Obtient un pointeur sur l'object.
    CNTService* pService = m_pThis;
    
    switch(dwOpcode) 
	{
		case SERVICE_CONTROL_STOP:         // 1
			pService->SetStatus(SERVICE_STOP_PENDING);
			pService->OnStop();
			pService->m_bIsRunning = FALSE;
			sprintf(pService->m_ServiceMessage,"Service %s stoped.\n",pService->m_szServiceName);
			LogEv(pService->m_logfname,pService->m_ServiceMessage);
			break;

		case SERVICE_CONTROL_PAUSE:        // 2
			pService->OnPause();
			sprintf(pService->m_ServiceMessage,"Service %s paused.\n",pService->m_szServiceName);
			LogEv(pService->m_logfname,pService->m_ServiceMessage);
			break;

		case SERVICE_CONTROL_CONTINUE:     // 3
			pService->OnContinue();
			sprintf(pService->m_ServiceMessage,"Service %s continued.\n",pService->m_szServiceName);
			LogEv(pService->m_logfname,pService->m_ServiceMessage);
			break;

		case SERVICE_CONTROL_INTERROGATE:  // 4
			pService->OnInterrogate();
			sprintf(pService->m_ServiceMessage,"Service %s interrogated.\n",pService->m_szServiceName);
			LogEv(pService->m_logfname,pService->m_ServiceMessage);
			break;

		case SERVICE_CONTROL_SHUTDOWN:     // 5
			pService->OnShutdown();
			sprintf(pService->m_ServiceMessage,"Service %s shut down.\n",pService->m_szServiceName);
			LogEv(pService->m_logfname,pService->m_ServiceMessage);
			break;

		default:
			//////////////////////////////////////
			// User defined codes.
			if ((dwOpcode >= SERVICE_CONTROL_USER)) 
				if (!pService->OnUserControl(dwOpcode))
				{
					sprintf(pService->m_ServiceMessage,"Service %s unknown request.\n",pService->m_szServiceName);
					LogEv(pService->m_logfname,pService->m_ServiceMessage);
				}
			else 
			{
				sprintf(pService->m_ServiceMessage,"Service %s bad request.\n",pService->m_szServiceName);
				LogEv(pService->m_logfname,pService->m_ServiceMessage);
			}
		break;
    }
    ///////////////////////////////////////////////
	// Set current status.
    ::SetServiceStatus(pService->m_hServiceStatus, &pService->m_Status);
}
        
/////////////////////////////////////////////////////////////////////////////
// Call when the service is initialized for the first time.
BOOL CNTService::OnInit()
{
	FILE *fp             = NULL;
	char str[513]        = {0};
	unsigned short iPort = 0;
	unsigned long type   = 0;
	unsigned long size   = 0;
	char *pTmp           = NULL;
	HKEY hKey            = NULL;
	WSAData ws           = {0};

	/////////////////////////////////////////////////////////////////////////
	//	Windows socket initialisation.
	if(WSAStartup(0x0202,&ws))
	{
		LogEv(m_logfname,"Windows socket initialisation failed.");
		LogEv(m_logfname,"Service not started.");

		return FALSE;
	}
	//
	/////////////////////////////////////////////////////////////////////////

	/////////////////////////////////////////////////////////////////////////
	//	Read configuration.
	fp = fopen("C:\\config.cfg","r");
	if(!fp)
	{
		LogEv(m_logfname,"Unable to open configuration file");
		LogEv(m_logfname,"Service not started.");

		return FALSE;
	}

	while(fgets(str,512,fp) != NULL)
	{
		pTmp = strpbrk(str,"=");
		if(pTmp)
		{
			pTmp[strlen(pTmp)-2] = 0x00;
			if(strstr(str,"port"))
			{
				iPort = atoi(&pTmp[1]);
			}
		}
	}
	fclose(fp);
	fp = NULL;
	//
	/////////////////////////////////////////////////////////////////////////


	if(::RegOpenKeyEx(HKEY_LOCAL_MACHINE,"SOFTWARE\\ServiceRegistry\\",0,KEY_ALL_ACCESS,&hKey) != ERROR_SUCCESS)
	{
		LogEv(m_logfname,"Unabel to find install path.");
		LogEv(m_logfname,"Service not started.");

		return FALSE;
	}

	size = 512;

	if(::RegQueryValueEx(hKey,"regpath",NULL,&type,(LPBYTE)str,&size) != ERROR_SUCCESS)
	{
		LogEv(m_logfname,"Failed to read install path");
		LogEv(m_logfname,"Service not started.");

		return FALSE;
	}
	str[size] = 0x00;

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// Called when service is stopped.
void CNTService::OnStop()
{
	mPacsServer.Stop();
}

/////////////////////////////////////////////////////////////////////////////
// Called when service is interrogated.
void CNTService::OnInterrogate()
{
}

/////////////////////////////////////////////////////////////////////////////
// Called when service is put on pause.
void CNTService::OnPause()
{
}

/////////////////////////////////////////////////////////////////////////////
// Called when service is set to continue.
void CNTService::OnContinue()
{
}

/////////////////////////////////////////////////////////////////////////////
// Called when service is shut down.
void CNTService::OnShutdown()
{
}

/////////////////////////////////////////////////////////////////////////////
//	Called after the service have been 
//	stopped (after Run exited).
void CNTService::ServiceStopped()
{
	WSACleanup();
}

/////////////////////////////////////////////////////////////////////////////
// Called when service gets message from the
// SCM.
BOOL CNTService::OnUserControl(DWORD dwOpcode)
{
    return FALSE;
}

/////////////////////////////////////////////////////////////////////////////
//
void CNTService::GetLastErrorText(char *pMessage, int iMaxLength)
{
	char *lpMsgBuf = 0;
	DWORD dw       = GetLastError(); 

	if(pMessage)
	{
		FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | 
					  FORMAT_MESSAGE_FROM_SYSTEM,
					  NULL,
					  dw,
					  MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
					 (LPTSTR) &lpMsgBuf,
					  0,
					  NULL);

		strncpy(pMessage, lpMsgBuf, iMaxLength);
		pMessage[iMaxLength - 1] = 0x00;

		LocalFree(lpMsgBuf);
		lpMsgBuf = 0;
	}
}

/////////////////////////////////////////////////////////////////////////////
// To write events in the sevice's log file.
void LogEv(char* pathname,char *msg)
{
	FILE *fd = NULL;

	time_t t = 0;
	tm *datetime = NULL;

	time(&t);
	datetime = localtime(&t);

	if((fd = fopen(pathname,"a"))!=NULL)
	{
		fprintf(fd,"%02d:%02d:%02d  %d/%d/%d  %s\n",datetime->tm_hour,
													  datetime->tm_min,
													  datetime->tm_sec,
													  datetime->tm_mday,
													  datetime->tm_mon+1,
													  datetime->tm_year+1900,
													  msg);
		 
		fclose(fd);
		fd = NULL;
	}
}
