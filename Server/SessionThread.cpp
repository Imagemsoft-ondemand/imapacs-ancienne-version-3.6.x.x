/////////////////////////////////////////////////////////////////////////////
//	File		:	SessionThread.cpp
//	Description	:	Implementation.
/////////////////////////////////////////////////////////////////////////////

#include "SessionThread.h"
#include "../Utils/os.h"

/////////////////////////////////////////////////////////////////////////////
//
namespace imapacs
{

/////////////////////////////////////////////////////////////////////////////
//
bool SessionThread::Initialize()
{
	bool bRetVal = false;

	mSession.SetDescriptor(miFd);

	/////////////////////////////////////////////////////////////////////////
	//	Read configuration file.
	if(mConfig.ReadConfigFromFile(CONFIGURATION_FILE))
	{
		/////////////////////////////////////////////////////////////////////
		//	Create directories in case they don't exist.
		os::MkDir(mConfig.GetLogPath(), 0777);
		os::MkDir(mConfig.GetTmpPath(), 0777);
		os::MkDir(DEBUG_DIRECTORY, 0777);

		/////////////////////////////////////////////////////////////////////
		//	Setup logger's paths.
		mConfig.GetServerLogger()->SetLogFilePath(mConfig.GetLogPath());
		mConfig.GetModalityLogger()->SetLogFilePath(mConfig.GetLogPath());

		mSession.SetCfg(&mConfig);
		mSession.SetServerLogger(mConfig.GetServerLogger());
		mSession.SetModalityLogger(mConfig.GetModalityLogger());

		mConfig.GetServerLogger()->LogMsg(BEGIN_LOG_LINE);
		mConfig.GetServerLogger()->LogMsg("Imapacs version %s started ...", IMAPACS_VERSION);

		if(mConfig.IsTransient())
			mConfig.GetServerLogger()->LogMsg("Transient mode activated");

		mConfig.GetServerLogger()->LogMsg(" ");

		// Make sure data dictionary is loaded.
		if(dcmDataDict.isDictionaryLoaded())
		{
			if(mConfig.ConnectDb())				//	Connect to database.
			{
				if(mConfig.ReadConfigFromDb())	//	Read configuration from database.
				{
					if(mSession.Init())
					{
						bRetVal = true;
					}
					else
					{
						mConfig.GetServerLogger()->LogMsg("ERROR : Unable to initialise DICOMSession.");
						mConfig.CloseDb();
					}
				}
				else
				{
					mConfig.GetServerLogger()->LogMsg("ERROR : Unable to load configuration from database.");
					mConfig.CloseDb();
				}
			}
			else
			{
				mConfig.GetServerLogger()->LogMsg("ERROR : Unable to connect to database %s@%s", 
					                              mConfig.GetDbUser(), 
												  mConfig.GetDbSource());
			}
		}
		else
		{
			mConfig.GetServerLogger()->LogMsg("Warning: no data dictionary loaded, check environment variable: %s\n",
											  DCM_DICT_ENVIRONMENT_VARIABLE);
		}
	}
	return bRetVal;
}

/////////////////////////////////////////////////////////////////////////////
//
int SessionThread::Run()
{
	if(Initialize())
	{
		m_bIsRunning = true;

		if(!mSession.Run())
		{
			if(mConfig.SendMail())
			{
				if(!mConfig.GetMailer()->SendMail())
				{
					mConfig.GetServerLogger()->LogMsg("Error:  %s", mConfig.GetMailer()->GetLastError());
					mConfig.GetServerLogger()->LogMsg("         Could not send mail.");
				}
			}
		}
		Shutdown();

		m_bIsRunning = false;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////
//
void SessionThread::Shutdown()
{
	mSession.Shutdown();
}

}
