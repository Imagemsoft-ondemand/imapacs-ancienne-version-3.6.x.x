/////////////////////////////////////////////////////////////////////////////
//	File		:	SessionThread.h
//	Description	:	Class representing an association with a client.
/////////////////////////////////////////////////////////////////////////////

#include "../Utils/PThreadBase.h"
#include "../Network/ClientSocket.h"
#include "../DICOMSession.h"

#if !defined(__SESSIONTHREAD_H_INCLUDED__)
#define      __SESSIONTHREAD_H_INCLUDED__

/////////////////////////////////////////////////////////////////////////////
//
namespace imapacs
{

/////////////////////////////////////////////////////////////////////////////
//
class SessionThread : public util::PThreadBase
{
public:

	//	Default constructor.
	SessionThread(SOCKET iFd)
		:PThreadBase(), miFd(iFd)
	{}

	//	Destructor.
	~SessionThread()
	{}

	//	One time server initialization.
	bool Initialize();

	//	Thread worker method.
	int Run();

	//	Cleanup method.
	void Shutdown();

private:

	SOCKET            miFd;

	PacsConfig        mConfig;
	DICOMSession      mSession;
};

}

#endif	//	__SESSIONTHREAD_H_INCLUDED__
