/////////////////////////////////////////////////////////////////////////////
//	File		:	PacsServer.h
//	Description	:	Class representing an instance of the server. It creates
//					a thread that accepts connections and start a session
//					thread for every connection. Each of these session thread
//					is an independent association.
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//
#include "../Utils/PThreadBase.h"
#include "../Network/ServerSocket.h"
#include "../Network/ClientSocket.h"
#include "../Queue.h"

#if !defined(__PACSSERVER_H_INCLUDED__)
#define      __PACSSERVER_H_INCLUDED__

/////////////////////////////////////////////////////////////////////////////
//
namespace imapacs
{

class SessionThread;

/////////////////////////////////////////////////////////////////////////////
//
class PacsServer : public util::PThreadBase
{
public:

	//	Default constructor.
	PacsServer():PThreadBase(), miPort(0)
	{}

	//	Destructor.
	~PacsServer()
	{}

	//	One time server initialization.
	bool Initialize();

	//	Thread worker method.
	int Run();

	//	Cleanup method.
	void Shutdown();

private:

	bool ReadConfiguration(const char *pCfgFile);
	void CleanupSessionList(bool bClearAll = false);

	net::ServerSocket       mAccepter;
	Queue <SessionThread *> mSessionList;
	unsigned short          miPort;
};

}	//	End namespace imapacs.

#endif	//	__PACSSERVER_H_INCLUDED__
