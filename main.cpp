///////////////////////////////////////////////////////////////////////////////////////////////////
///	File		:	main.cpp
///	Description	:	implementation of the Linux specific application's entry point.
///////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////////
//
#include <atomic>
#include <stdio.h>
#include <fstream>
#include <iostream>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <netinet/in.h>

#include "ServerMain.h"
#include "PacsConfig.h"
#include "./Utils/os.h"
#include "./Utils/log.h"
#include "./Utils/PacsCommon.h"

///	Global flag indicating the running state. It is used to stop the server from a
///	signal handler.
///
static std::atomic_bool stopRequested;

///	Handler responsible to process SIGTERM signal. This signal is sent by the operating
///	system to tell the application to terminate itself. This is the opportunity for the
///	server to terminate gracefully. If the server doesn't terminate in a timely manner,
///	the operating system will terminate the server forcefully using SIGKILL.
///
///	sig					:	indicates the signal received.
///
extern void terminate(int sig);

///	Returns the description for the specified error code for the socket function.
///
///	errcode				:	code d'erreur (errno).
///
///	Return value		:	description for the specified error code for the socket function.
///
extern const char* getSocketLastError(int errcode);

///	Returns the description for the specified error code for the setsockopt function.
///
///	errcode				:	code d'erreur (errno).
///
///	Return value		:	description for the specified error code for the setsockopt function.
///
extern const char* getSetsockoptError(int errcode);

///	Returns the description for the specified error code for the bind function.
///
///	errcode				:	code d'erreur (errno).
///
///	Return value		:	description for the specified error code for the bind function.
///
extern const char* getBindLastError(int errcode);

///	Returns the description for the specified error code for the listen function.
///
///	errcode				:	code d'erreur (errno).
///
///	Return value		:	description for the specified error code for the listen function.
///
extern const char* getListenLastError(int errcode);

///	Returns the description for the specified error code for the accept function.
///
///	errcode				:	code d'erreur (errno).
///
///	Return value		:	description for the specified error code for the accept function.
///
extern const char* getAcceptLastError(int errcode);

///	Returns the description for the specified error code for the select function.
///
///	errcode				:	code d'erreur (errno).
///
///	Return value		:	description for the specified error code for the select function.
///
extern const char* getSelectError(int errcode);

///	Writes the Imapacs.pid file in the working directory containing the process id.
///
///	filename			:	file where the pid is written.
///
extern void writepidFile(const char* filename);

///////////////////////////////////////////////////////////////////////////////////////////////////
//
int main(int argc, char **argv)
{
	struct sockaddr_in ServerAddress;
	struct sockaddr_in ClientAddress;

	imapacs::PacsConfig Config;
	Log* logger;
	int returnValue = 0;
	int port = 0;

	socklen_t len;
	int listener;
	int peer;

	//	Get command-line arguments.
	//
	Config.ParseArgs(argc, argv);

	if(Config.GetDisplayAbout())
	{
		std::cout << std::endl << PRODUCT_FAMILY << std::endl;
		std::cout << APP_NAME << std::endl;
		std::cout << IMAPACS_VERSION << std::endl;
		std::cout << ABOUT_MESSAGE << std::endl;

		return 0;
	}

	if(!Config.ReadConfigFromFile(CONFIGURATION_FILE))
	{
		Config.GetServerLogger()->SetLogFilePath("./");	
		Config.GetServerLogger()->LogMsg("Failed to read configuration file %s", CONFIGURATION_FILE);

		return -1;
	}

	Config.GetServerLogger()->SetLogFilePath(Config.GetLogPath());

	logger = Config.GetServerLogger();

	stopRequested.store(false);

	listener = socket(AF_INET, SOCK_STREAM, 0);

	if(listener == -1)
	{
		logger->LogMsg("ERROR : socket failed : %s", getSocketLastError(errno));
		return -1;
	}

	//	Enable SO_REUSEADDR so the server can be restarted on the same address immediately after
	//	a shutdown or a crash.
	//
	int optval = 1;

	if(setsockopt(listener, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval)) == -1)
	{
		close(listener);

		logger->LogMsg("ERROR : setsockopt failed to set option SO_REUSEADDR : %s", getSetsockoptError(errno));
		return -1;
	}

	optval = 1;

	if(setsockopt(listener, SOL_SOCKET, SO_REUSEPORT, &optval, sizeof(optval)) == -1)
	{
		close(listener);

		logger->LogMsg("ERROR : setsockopt failed to set option SO_REUSEPORT : %s", getSetsockoptError(errno));
		return -1;
	}

	ServerAddress.sin_family      = AF_INET;
	ServerAddress.sin_addr.s_addr = htonl(INADDR_ANY);

	if(Config.IsTransient())
		port = Config.GetTransientPort();
	else
		port = Config.GetPort();

	ServerAddress.sin_port = htons(port);

	if(bind(listener, (struct sockaddr *)&ServerAddress, sizeof(ServerAddress)) == -1)
	{
		close(listener);
		
		logger->LogMsg("ERROR : bind failed : %s", getBindLastError(errno));
		return -1;
	}

	if(listen(listener, 100) == -1)
	{
		close(listener);

		logger->LogMsg("ERROR : socket failed : %s", getListenLastError(errno));
		return -1;
	}

	if(Config.IsTransient())
		writepidFile("./Imapacs_Transient.pid");
	else
		writepidFile("./Imapacs.pid");

	logger->LogMsg("Server listeninig on port: %d\n", port);

	//	Initialize application's signal handlers.
	//
	signal(SIGCHLD, SIG_IGN);	//	Ignore child processes termination notifications.
	signal(SIGTERM, terminate);

	int r;
	fd_set rset;
	struct timeval tv;

	pid_t serverPid = getpid();

	//	Server loop.
	//
	while(!stopRequested.load())
	{
		FD_ZERO(&rset);
		FD_SET(listener, &rset);

		tv.tv_sec  = 0;
		tv.tv_usec = 15000;		//	15 ms.

		r = select(listener + 1, &rset, nullptr, nullptr, &tv);

		if((r == -1))
		{
			if(errno == EINTR)	//	Interrupted by SIGTERM.
				break;

			close(listener);

			logger->LogMsg("ERROR : socket failed : %s", getSelectError(errno));
			return -1;
		}

		if(r == 0)
			continue;	//	Timeout, there is no incomming connection.

		//	Accept incomming connections.
		//
		len  = sizeof(ClientAddress);
		peer = accept(listener, (struct sockaddr *)&ClientAddress, &len);

		if(peer > 0)
		{
			//	Create a child to process client's requests.
			//
			if(fork() == 0)
			{
				//	This is child process, just read and process client's requests.
				//
				ServerMain main;
				main.Process(peer, Config);

				break;
			}
			else
				close(peer);	//	Server process.
		}
		else
			logger->LogMsg("ERROR : accept failed : %s", getAcceptLastError(errno));	
	}

	if(getpid() == serverPid)	//	Avoid this if in the child process.
	{
		close(listener);
		logger->LogMsg("PACS server's listener shut down.");
	}

	return returnValue;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
///	Handler responsible to process SIGTERM signal. This signal is sent by the operating
///	system to tell the application to terminate itself. This is the opportunity for the
///	server to terminate gracefully. If the server doesn't terminate in a timely manner,
///	the operating system will terminate the server forcefully using SIGKILL.
///
///	sig					:	indicates the signal received.
///
void terminate(int sig)
{
	stopRequested.store(true);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
///	Returns the description for the specified error code for the socket function.
///
///	errcode				:	code d'erreur (errno).
///
///	Return value		:	description for the specified error code for the socket function.
///
const char* getSocketLastError(int errcode)
{
	switch(errcode)
	{
		case EACCES:
			return "Permission to create a socket of the specified type and/or protocol is denied.";
		break;

		case EAFNOSUPPORT:
			return "The implementation does not support the specified address family.";
		break;

		case EINVAL:
			return "Unknown protocol, or protocol family not available.";
		break;

		case EMFILE:
			return "The per-process limit on the number of open file descriptors has been reached.";
		break;

		case ENFILE:
			return "The system-wide limit on the total number of open files has been reached.";
		break;

		case ENOBUFS:
		case ENOMEM:
			return "Insufficient memory is available.  The socket cannot be created until sufficient resources are freed.";
		break;

		case EPROTONOSUPPORT:
			return "The protocol type or the specified protocol is not supported within this domain.";
		break;

		default:
			return "";
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////
///	Returns the description for the specified error code for the setsockopt function.
///
///	errcode				:	code d'erreur (errno).
///
///	Return value		:	description for the specified error code for the setsockopt function.
///
const char* getSetsockoptError(int errcode)
{
	switch(errcode)
	{
		case EBADF:
			return "The socket argument is not a valid file descriptor.";
		break;

		case EDOM:
			return "The send and receive timeout values are too big to fit into the timeout fields in the socket structure.";
		break;

		case EINVAL:
			return "The specified option is invalid at the specified socket level or the socket has been shut down.";
		break;

		case EISCONN:
			return "The socket is already connected, and a specified option cannot be set while the socket is connected.";
		break;

		case ENOPROTOOPT:
			return "The option is not supported by the protocol.";
		break;

		case ENOTSOCK:
			return "The socket argument does not refer to a socket.";
		break;

		default:
			return "";
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////
///	Returns the description for the specified error code for the bind function.
///
///	errcode				:	code d'erreur (errno).
///
///	Return value		:	description for the specified error code for the bind function.
///
const char* getBindLastError(int errcode)
{
	switch(errcode)
	{
		case EACCES:
			return "he address is protected, and the user is not the superuser or search permission is denied on a component of the path prefix.";
		break;

		case EADDRINUSE:
			return "The given address is already in use or bind port is zero and all port numbers in the ephemeral port range are currently in use.";
		break;

		case EBADF:
			return "socket is not a valid file descriptor.";
		break;

		case EINVAL:
			return "The socket is already bound to an address or addrlen is wrong, or addr is not a valid address for this socket's domain.";
		break;

		case ENOTSOCK:
			return "The file descriptor does not refer to a socket.";
		break;

		case EADDRNOTAVAIL:
			return "A nonexistent interface was requested or the requested address was not local.";
		break;

		case EFAULT:
			return "addr points outside the user's accessible address space.";
		break;

		case ELOOP:
			return "Too many symbolic links were encountered in resolving addr.";
		break;

		case ENAMETOOLONG:
			return "addr is too long.";
		break;

		case ENOENT:
			return "A component in the directory prefix of the socket pathname does not exist.";
		break;

		case ENOMEM:
			return "Insufficient kernel memory was available.";
		break;

		case ENOTDIR:
			return "A component of the path prefix is not a directory.";
		break;

		case EROFS:
			return "The socket inode would reside on a read-only filesystem.";
		break;

		default:
			return "";
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////
///	Returns the description for the specified error code for the listen function.
///
///	errcode				:	code d'erreur (errno).
///
///	Return value		:	description for the specified error code for the listen function.
///
const char* getListenLastError(int errcode)
{
	switch(errcode)
	{
		case EADDRINUSE:
			return "Another socket is already listening on the same port or socket not bound to the address (missing call to bind).";
		break;

		case EBADF:
			return "Socket is not a valid file descriptor.";
		break;

		case ENOTSOCK:
			return "Descriptor is not a socket.";
		break;

		case EOPNOTSUPP:
			return "The socket is not of a type that supports the listen() operation.";
		break;

		default:
			return "";
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////
///	Returns the description for the specified error code for the accept function.
///
///	errcode				:	code d'erreur (errno).
///
///	Return value		:	description for the specified error code for the accept function.
///
const char* getAcceptLastError(int errcode)
{
	switch(errcode)
	{
		case EWOULDBLOCK:
			return "The socket is marked nonblocking and no connections are present to be accepted.";
		break;

		case EBADF:
			return "Socket is not an open file descriptor.";
		break;

		case ECONNABORTED:
			return "Connection has been aborted.";
		break;

		case EFAULT:
			return "The addr struct argument is not in a writable part of the user address space.";
		break;

		case EINTR:
			return "accept system call was interrupted by a signal that was caught before a valid connection arrived.";
		break;

		case EINVAL:
			return "Socket is not listening for connections, or addrlen is invalid.";
		break;

		case EMFILE:
			return "he per-process limit on the number of open file descriptors has been reached.";
		break;

		case ENFILE:
			return "The system-wide limit on the total number of open files has been reached.";
		break;

		case ENOBUFS:
		case ENOMEM:
			return "Not enough free memory.";
		break;

		case ENOTSOCK:
			return "The file descriptor sockfd does not refer to a socket.";
		break;

		case EOPNOTSUPP:
			return "The referenced socket is not of type SOCK_STREAM.";
		break;

		case EPROTO:
			return "Protocol error.";
		break;

		case EPERM:
			return "Firewall rules forbid connection.";
		break;

		default:
			return "";
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////
///	Returns the description for the specified error code for the select function.
///
///	errcode				:	code d'erreur (errno).
///
///	Return value		:	description for the specified error code for the select function.
///
 const char* getSelectError(int errcode)
{
	switch(errcode)
	{
		case EBADF:
			return "One or more of the file descriptor sets specified a file descriptor that is not a valid open file descriptor.";
		break;

		case EINTR:
			return "The function was interrupted before any of the selected events occurred and before the timeout interval expired.";
		break;

		case EINVAL:
			return "An invalid timeout interval was specified or the listener + 1 argument is less than 0 or greater than FD_SETSIZE or one of the specified file descriptors refers to a STREAM or multiplexer that is linked (directly or indirectly) downstream from a multiplexer.";
		break;
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////
///	Writes the Imapacs.pid file in the working directory containing the process id.
///
///	filename			:	file where the pid is written.
///
void writepidFile(const char* filename)
{
	char pid[256 + 1] = {0};
	std::ofstream file;
	sprintf(pid, "%d", getpid());

	remove(filename);	//	Remove existing pid file if any.

	file.open(filename);
	file << pid;
	file.close();
}
