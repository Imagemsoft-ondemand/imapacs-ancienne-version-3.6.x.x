/////////////////////////////////////////////////////////////////////////////
//	DICOMSession.h
/////////////////////////////////////////////////////////////////////////////

#include <string.h>
#include <stdlib.h>

#ifndef WIN32
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#endif

#include "./Utils/DicomHeaders.h"
#include "./Utils/PacsCommon.h"
#include "PacsConfig.h"
#include "Descriptor.h"

#if !defined(__DICOMSESSION_H_INCLUDED__)
#define      __DICOMSESSION_H_INCLUDED__

/////////////////////////////////////////////////////////////////////////////
//
namespace tool
{
class COCIDatabase;
}

/////////////////////////////////////////////////////////////////////////////
//
namespace imapacs
{
	/////////////////////////////////////////////////////////////////////////
	//
	class DICOMSession
	{
	public:
		DICOMSession():m_pNetwork(NULL),
			           m_pAssociation(NULL),
					   m_pCfg(NULL),
		               m_iFd(-1),
		               m_pServerLogger(NULL),
		               m_pModalityLogger(NULL),
		               m_pMailer(NULL),
					   m_pDb(NULL),
					   m_iPreferedTrnSynOpt(EXS_LittleEndianExplicit)
		{
#ifndef WIN32
			m_iTmpFileDescriptorStdErr = -1;
			m_iTmpFileDescriptorStdOut = -1;
			m_sTmpStdOutFileName[0]    = 0x00;
			m_sTmpStdErrFileName[0]    = 0x00;
#endif
		}
		virtual ~DICOMSession()                {}

		void SetCfg(PacsConfig *pCfg)          {m_pCfg = pCfg;}
		void SetServerLogger(Log *pLogger)     {m_pServerLogger = pLogger;}
		void SetModalityLogger(Log *pLogger)   {m_pModalityLogger = pLogger;}

		bool Init();
		bool Run();
		void Shutdown();

		void AbortAssociation();
		void CloseAssociation();

		static void InitializeCodecs();
		static void ClearCodecs();

#ifdef WIN32
		void SetDescriptor(SOCKET iFd);
#else
		void SetDescriptor(int iFd);
		int MakeTempFile(char *pTemplate);
		void ClearTmpFile();
#endif

	protected:

		bool IsKnownAETitle(const char *pAETitle);
		bool NegociateAssociation();
		bool RejectAssociation(ASC_REFUSE_REASON Reason);
		bool AcknoledgeAssociation();

//		int MakeTmpFile(char *pTemplate);

		SOCKET_DESCRIPTOR	 m_iFd;

		T_ASC_Network       *m_pNetwork;
		T_ASC_Association   *m_pAssociation;
		int                  m_iPreferedTrnSynOpt;

		PacsConfig          *m_pCfg;

		Log                 *m_pServerLogger;
		Log                 *m_pModalityLogger;
		Mailer              *m_pMailer;
		tool::COCIDatabase  *m_pDb;

#ifndef WIN32
		int                  m_iTmpFileDescriptorStdOut;
		int                  m_iTmpFileDescriptorStdErr;

		char                 m_sTmpStdOutFileName[81];
		char                 m_sTmpStdErrFileName[81];
#endif
	};
}

#endif	//	__DICOMSESSION_H_INCLUDED__
