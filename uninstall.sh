#!/bin/sh
#   Run like this: sudo ./uninstall.sh

#!/bin/sh
# This script must be executed with root privilege.

ROOT_DIR="/opt/imagem/"
INSTALL_DIR="$ROOT_DIR/imapacs"

if [ -d $ROOT_DIR ]
then
    if [ -d $INSTALL_DIR ]
    then

        systemctl stop imapacs.service
        systemctl stop imapacs_transient.service

        rm -Rf $INSTALL_DIR
        rm -f /etc/systemd/system/imapacs.service
        rm -f /etc/systemd/system/imapacs_transient.service
        rm -f /etc/ld.so.conf.d/imapacs.conf

        systemctl daemon-reload

        ldconfig

        if [[ -z "$(ls -A $ROOT_DIR)" ]]
        then
            rm -Rf $ROOT_DIR
        fi
    fi
fi
