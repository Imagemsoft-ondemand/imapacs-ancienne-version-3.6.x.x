/////////////////////////////////////////////////////////////////////////////
//	PacsConfig.h
/////////////////////////////////////////////////////////////////////////////

#include <string>

#include "./Utils/log.h"
#include "./Utils/Mailer.h"
#include "./Utils/Str.h"
#include "./Utils/DBGTool.h"
#include "./Utils/TimeUtil.h"
#include "./Utils/PacsCommon.h"
#include "./Utils/OCIDatabase.h"
#include "DataStructures.h"

#if !defined(__PACSCONFIG_H_INCLUDED__)
#define      __PACSCONFIG_H_INCLUDED__

/////////////////////////////////////////////////////////////////////////////
//
namespace imapacs
{
	/////////////////////////////////////////////////////////////////////////
	//
	class PacsConfig
	{
	public:
		PacsConfig():m_hCursor(NULL), m_bSendMail(false), m_bTransient(false), 
			         m_bModalityDebug(false), m_bRemoveAccents(false),
					 m_bProfiling(false), m_bDisplayAbout(false)
		{
			m_sDbHost[0]           = 0x00;
			m_sDbPort[0]           = 0x00;
			m_sDbSource[0]         = 0x00;
			m_sDbUser[0]           = 0x00;
			m_sDbPassword[0]       = 0x00;
			m_sServerAeTitle[0]    = 0x00;
			m_sWorklistStatus[0]   = 0x00;
			m_sImplementationId[0] = 0x00;
			m_sMailSubject[0]      = 0x00;

			m_sCharacterSet[0]     = 0x00;

			strcpy(m_sPkgName, PKG_NAME);	//	Default value.
			strcpy(m_sLogPath, LOG_PATH);	//	Default value.
			strcpy(m_sTmpPath, TMP_PATH);	//	Default value.

			m_Port = 104;					//	Default listening port.
			m_TransientPort = 105;			//	Default transient listening port.
		}
		virtual ~PacsConfig()
		{}

		bool SendMail() const                    {return m_bSendMail;}
		bool IsTransient() const                 {return m_bTransient;}
		const char *GetDbSource() const          {return &m_sDbSource[0];}
		const char *GetDbUser() const            {return &m_sDbUser[0];}
		const char *GetImplementationId() const  {return &m_sImplementationId[0];}
		const char *GetServerAeTitle() const     {return &m_sServerAeTitle[0];}
		const char *GetWorklistStatus() const    {return &m_sWorklistStatus[0];}
		const char *GetPkgName() const           {return &m_sPkgName[0];}
		const char *GetLogPath() const           {return &m_sLogPath[0];}
		const char *GetTmpPath() const           {return &m_sTmpPath[0];}
		const char *GetCharterSet() const        {return &m_sCharacterSet[0];}
		bool GetProfiling() const                {return m_bProfiling;}
		bool GetDisplayAbout() const             {return m_bDisplayAbout;}

		int GetPort() const                      {return m_Port;}
		int GetTransientPort() const             {return m_TransientPort;}

		bool ConnectDb();
		bool ReadConfigFromFile(const char *pCfgFile);
		bool ReadConfigFromDb();
		bool LoadModalityConfigurations(const char *pAeTitle);
		void CloseDb();

		bool GetModalityDebug() const            {return m_bModalityDebug;}      
		bool MustRemoveAccents() const           {return m_bRemoveAccents;}

		bool GetMFS(const char *pStudyInstanceUID, char *pMfs);

		bool InsertEvent(const char *pEventType,
						 const char *pStudyInstanceUID,
						 const char *pApplication,
						 const char *pSource,
						 const char *pOperator,
						 const char *pPatId,
						 const char *pCOIType,
						 const char *pCOIInstanceUID,
						 const char *pCOISopClassDesc,
						 const char *pArgv1,
						 const char *pArgv2,
						 const char *pArgv3,
						 const char *pArgv4,
						 const char *pArgv5);

		int SaveCompressed(const char *pAeTitle);

		bool GetMoveDestinationAddress(const char *pMoveToAeTitle,
									   char *pMoveToIP,
									   unsigned short *pMoveToPort);

		bool GetCommitAddressAndPort(const char *pCommitAeTitle,
									 char *pCommitIP,
									 unsigned short *pCommitPort);

		bool FindObjectLocation(const char *pSOPInstanceUID, char *pObjectLocation);

		bool GetQaRoutingInfos(const char *pAeTitle,
			                   double *pScaleFactor,
							   char *pIp,
							   unsigned short *pPort,
							   char *pDestinationAeTitle);

		void ParseArgs(int iArgc, char **pArgv);

		Log *GetModalityLogger()         {return &m_ModalityLogger;}
		Log *GetServerLogger()           {return &m_ServerLogger;}
		Mailer *GetMailer()              {return &m_Mailer;}
		tool::COCIDatabase *GetDb()      {return &m_Db;}

		const char **GetTransferList(const char *pSOPClassUID, int *iCount, bool bAccepted = true);
		const char **GetSOPList(int *iSOPCount, bool bStorage = true);

		/////////////////////////////////////////////////////////////////////
		//
		static void ClearStringList(const char **pList, int iCount)
		{
			if(pList)
			{
				for(int i = 0; i < iCount; ++i)
				{
					if(pList[i])
					{
						delete [] pList[i];
						pList[i] = NULL;
					}
				}
				delete [] pList;
				pList = NULL;
			}
		}

		///
		///
		void LogConfiguration(Log* logger);

		const char* GetLastError() const {return m_LastError.c_str();}

	private:

		Log							m_ModalityLogger;
		Log							m_ServerLogger;

		Mailer						m_Mailer;

		tool::COCIDatabase			m_Db;

		std::string                 m_LastError;

		char						m_sPkgName[33];
		char						m_sLogPath[_MAX_PATH];
		char						m_sTmpPath[_MAX_PATH];

		char						m_sDbHost[128 + 1];
		char						m_sDbPort[16 + 1];
		char						m_sDbSource[33];
		char						m_sDbUser[33];
		char						m_sDbPassword[33];

		char						m_sServerAeTitle[17];

		char						m_sWorklistStatus[128 + 1];
		char						m_sImplementationId[128 + 1];

		char						m_sMailSubject[128 + 1];
		bool						m_bSendMail;

		bool						m_bTransient;

		bool						m_bProfiling;

		char						m_sCharacterSet[128 + 1];

		HANDLE						m_hCursor;

		int                         m_Port;
		int                         m_TransientPort;

		//	Per modality configurations. These values are loaded from the
		//	database per ae title.
		//
		bool						m_bModalityDebug:1;
		bool						m_bRemoveAccents:1;

		bool						m_bDisplayAbout:1;
	};
}

#endif	//	__PACSCONFIG_H_INCLUDED__
