/////////////////////////////////////////////////////////////////////////////
//	ImapacsDlg.cpp
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Imapacs.h"
#include "ImapacsDlg.h"

#include "ServerMain.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

/////////////////////////////////////////////////////////////////////////////
//
CImapacsDlg::CImapacsDlg(CWnd* pParent /*=NULL*/) : CDialog(CImapacsDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

/////////////////////////////////////////////////////////////////////////////
//
void CImapacsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

/////////////////////////////////////////////////////////////////////////////
//
BEGIN_MESSAGE_MAP(CImapacsDlg, CDialog)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDOK, &CImapacsDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CImapacsDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_RUN, &CImapacsDlg::OnBnClickedRun)
	ON_BN_CLICKED(IDC_RUN_LOOP, &CImapacsDlg::OnBnClickedRunLoop)
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CImapacsDlg message handlers
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//
BOOL CImapacsDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	SetDlgItemText(IDC_PORT, "104");

	return TRUE;  // return TRUE  unless you set the focus to a control
}

/////////////////////////////////////////////////////////////////////////////
//	If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//
void CImapacsDlg::OnPaint()
{
	if(IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

/////////////////////////////////////////////////////////////////////////////
//	The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CImapacsDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

/////////////////////////////////////////////////////////////////////////////
//
void CImapacsDlg::OnBnClickedOk()
{
	OnOK();
}

/////////////////////////////////////////////////////////////////////////////
//
void CImapacsDlg::OnBnClickedCancel()
{
	OnCancel();
}

/////////////////////////////////////////////////////////////////////////////
//
void CImapacsDlg::OnBnClickedRun()
{
	char sPort[21]            = {0};
	unsigned short iPort      = -1;

	SOCKET listener           = -1;
	int sinsize               = sizeof(struct sockaddr);
	struct sockaddr_in host   = {0};
	struct sockaddr_in remote = {0};

	GetDlgItemText(IDC_PORT, sPort, 20);
	if(!sPort[0])
	{
		AfxMessageBox("No port wis configured.");
		return;
	}

	iPort = atoi(sPort);

	listener = socket(AF_INET, SOCK_STREAM, 0);
	if(listener == -1)
	{
		AfxMessageBox("socket failed !");
		return;
	}

	host.sin_family      = AF_INET;
	host.sin_addr.s_addr = INADDR_ANY;
	host.sin_port        = htons(iPort);

	int ret = bind(listener, (struct sockaddr *)&host, sizeof(struct sockaddr));
	if(ret == -1)
	{
		AfxMessageBox("bind failed !");
		closesocket(listener);
		return;
	}

	if(listen(listener, 10) == -1)
	{
		AfxMessageBox("listen failed !");
		closesocket(listener);
		return;
	}

	GetDlgItem(IDC_RUN)->EnableWindow(FALSE);
	GetDlgItem(IDC_RUN_LOOP)->EnableWindow(FALSE);

	iDescriptor = accept(listener, (struct sockaddr *)&remote, &sinsize);
	if(iDescriptor == -1)
	{
		AfxMessageBox("accept failed !");
		closesocket(listener);
		return;
	}

	int iArgc    = 1;
	char *pArg[] = {"Imapacs"};

//	int iArgc    = 2;
//	char *pArg[] = {"Imapacs", "-version"};

	ServerMain(iArgc, pArg);

	closesocket(iDescriptor);
	iDescriptor = -1;

	closesocket(listener);
	listener = -1;

	GetDlgItem(IDC_RUN)->EnableWindow(TRUE);
	GetDlgItem(IDC_RUN_LOOP)->EnableWindow(TRUE);
}

/////////////////////////////////////////////////////////////////////////////
//
void CImapacsDlg::OnBnClickedRunLoop()
{
	char sPort[21]            = {0};
	unsigned short iPort      = -1;

	SOCKET listener           = -1;
	int sinsize               = sizeof(struct sockaddr);
	struct sockaddr_in host   = {0};
	struct sockaddr_in remote = {0};

	GetDlgItemText(IDC_PORT, sPort, 20);
	if(!sPort[0])
	{
		AfxMessageBox("No port wis configured.");
		return;
	}

	iPort = atoi(sPort);

	listener = socket(AF_INET, SOCK_STREAM, 0);
	if(listener == -1)
	{
		AfxMessageBox("socket failed !");
		return;
	}

	host.sin_family      = AF_INET;
	host.sin_addr.s_addr = INADDR_ANY;
	host.sin_port        = htons(iPort);

	int ret = bind(listener, (struct sockaddr *)&host, sizeof(struct sockaddr));
	if(ret == -1)
	{
		AfxMessageBox("bind failed !");
		closesocket(listener);
		return;
	}

	if(listen(listener, 10) == -1)
	{
		AfxMessageBox("listen failed !");
		closesocket(listener);
		return;
	}

	GetDlgItem(IDC_RUN)->EnableWindow(FALSE);
	GetDlgItem(IDC_RUN_LOOP)->EnableWindow(FALSE);

	while(true)
	{
		iDescriptor = accept(listener, (struct sockaddr *)&remote, &sinsize);
		if(iDescriptor == -1)
		{
			AfxMessageBox("accept failed !");
			closesocket(listener);
			return;
		}

		char *pArg = {"Imapacs"};

		ServerMain(1, &pArg);
	}

	closesocket(iDescriptor);
	iDescriptor = -1;

	closesocket(listener);
	listener = -1;

	GetDlgItem(IDC_RUN)->EnableWindow(TRUE);
	GetDlgItem(IDC_RUN_LOOP)->EnableWindow(TRUE);
}
