/////////////////////////////////////////////////////////////////////////////
//	DICOMCompressor.h
/////////////////////////////////////////////////////////////////////////////

#include "./Utils/DicomHeaders.h"

#if !defined(__DICOMCOMPRESSOR_H_INCLUDED__)
#define      __DICOMCOMPRESSOR_H_INCLUDED__

class DICOMCompressor
{
public:
	DICOMCompressor();
	virtual ~DICOMCompressor();

	static bool Init();
	static void Shutdown();

	bool CompressFromDatasetToFile(const char *pFileName, DcmDataset *pDataset);

	bool UncompressToFile(DcmDataset *pSrcDataset, const char *pDestFile, E_TransferSyntax WantedTrnSyn = EXS_LittleEndianExplicit);

	bool UncompressToFile(const char *pSrcFile, const char *pDestFile, E_TransferSyntax WantedTrnSyn = EXS_LittleEndianExplicit);

	const char *GetLastErrorText() const         {return m_pLastErrorText;}

	bool FixCompressedImage(const char *pFile);

private:

	void SetLastErrorText(const char *pText);

	char *m_pLastErrorText;
	bool  m_bInited;
};

#endif	//	__DICOMCOMPRESSOR_H_INCLUDED__
