///////////////////////////////////////////////////////////////////////////////////////////////////
//	ServerMain.cpp
///////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef WIN32
#include "stdafx.h"
#endif

#include "./Transactions/ITransaction.h"
#include "./Utils/PacsCommon.h"
#include "DICOMSession.h"
#include "PacsConfig.h"
#include "./Utils/os.h"

#include "ServerMain.h"

#ifdef WIN32
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////
//
void ServerMain::SetSocket(SOCKET_DESCRIPTOR s)
{
	peer_ = s;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
//
void ServerMain::Process(SOCKET_DESCRIPTOR socket, imapacs::PacsConfig& Config)
{
	Log* logger = nullptr;	//	Server logger.
	imapacs::DICOMSession Session;

	Session.SetDescriptor(socket);

	//	Read configuration file.
	//
	if(Config.ReadConfigFromFile(CONFIGURATION_FILE))
	{
		//	Create directories in case they don't exist.
		//
		imapacs::os::MkDir(Config.GetLogPath(), 0777);
		imapacs::os::MkDir(Config.GetTmpPath(), 0777);
		imapacs::os::MkDir(DEBUG_DIRECTORY, 0777);

		//	Setup logger's paths.
		//
		Config.GetServerLogger()->SetLogFilePath(Config.GetLogPath());
		Config.GetModalityLogger()->SetLogFilePath(Config.GetLogPath());

		logger = Config.GetServerLogger();

		Session.SetCfg(&Config);

		logger->LogMsg(BEGIN_LOG_LINE);
		logger->LogMsg("Imapacs version %s started ...", IMAPACS_VERSION);

		if(Config.IsTransient())
			logger->LogMsg("Transient mode activated");

		logger->LogMsg(" ");

		// Make sure data dictionary is loaded.
		//
		if(!dcmDataDict.isDictionaryLoaded())
		{
			DcmDataDictionary &dict = dcmDataDict.wrlock();
			dict.loadDictionary("./dicom.dic");
			dcmDataDict.unlock();

			if(!dcmDataDict.isDictionaryLoaded())
			{
				logger->LogMsg("Warning: no data dictionary loaded, check environment variable: %s\n", DCM_DICT_ENVIRONMENT_VARIABLE);
				return;
			}
		}

		if(Config.ConnectDb())				//	Connect to database.
		{
			if(Config.ReadConfigFromDb())	//	Read configuration from database.
			{
				imapacs::DICOMSession::InitializeCodecs();

				if(Session.Init())
				{
					if(!Session.Run())
					{
						if(Config.SendMail())
						{
							if(!Config.GetMailer()->SendMail())
							{
								logger->LogMsg("Error:  %s", Config.GetMailer()->GetLastError());
								logger->LogMsg("         Could not send mail.");
							}
						}
					}
					Session.Shutdown();
				}
				else
					logger->LogMsg("ERROR : Unable to initialise DICOMSession.");

				imapacs::DICOMSession::ClearCodecs();
			}
			else
				logger->LogMsg("ERROR : Unable to load configuration from database.");

			Config.CloseDb();
		}
		else
		{
			logger->LogMsg("ERROR : Unable to connect to database %s@%s", Config.GetDbUser(), Config.GetDbSource());
			logger->LogMsg("        %s", Config.GetLastError());
		}
	}
	else
		logger->LogMsg("ERROR : Unable to read configuration file : %s", CONFIGURATION_FILE);

	Session.CloseAssociation();

	logger->LogMsg(" ");
	logger->LogMsg("Instance exited.");

	logger->LogMsg(END_LOG_LINE);

#ifndef WIN32
	Session.ClearTmpFile();
#endif
}
