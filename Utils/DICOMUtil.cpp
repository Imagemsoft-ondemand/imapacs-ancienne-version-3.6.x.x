/////////////////////////////////////////////////////////////////////////////
//	DICOMUtil.cpp
/////////////////////////////////////////////////////////////////////////////

#ifdef WIN32
#include "../stdafx.h"
#endif

#include "DICOMUtil.h"

#ifdef WIN32
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
#endif

/////////////////////////////////////////////////////////////////////////////
//
namespace imapacs
{
	/////////////////////////////////////////////////////////////////////////
	//
	const char* DICOMUtil::storageUid[] = {"1.2.840.10008.5.1.4.1.1.13.1.3"};

	/////////////////////////////////////////////////////////////////////////
	//
	bool DICOMUtil::IsImageType(const char *pSOPClassUID)
	{
		if(!pSOPClassUID)
			return false;

		if((strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.2") == 0)        ||
		   (strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.1") == 0)        ||
		   (strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.1.3") == 0)      ||
		   (strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.1.3.1") == 0)    ||
		   (strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.1.2") == 0)      ||
		   (strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.1.2.1") == 0)    ||
		   (strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.1.1") == 0)      ||
		   (strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.1.1.1") == 0)    ||
		   (strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.2.1") == 0)      ||
		   (strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.2.1") == 0)      ||
		   (strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.4.1") == 0)      ||
		   (strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.12.1.1") == 0)   ||
		   (strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.12.2.1") == 0)   ||
		   (strcmp(pSOPClassUID,"1.2.840.10008.5.1.1.30") == 0)           ||
		   (strcmp(pSOPClassUID,"1.2.840.10008.5.1.1.29") == 0)           ||
		   (strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.4") == 0)        ||
		   (strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.4.2") == 0)      ||
		   (strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.7.2") == 0)      ||
		   (strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.7.3") == 0)      ||
		   (strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.7.1") == 0)      ||
		   (strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.7.4") == 0)      ||
		   (strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.20") == 0)       ||
		   (strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.77.1.5.2") == 0) ||
		   (strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.77.1.5.1") == 0) ||
		   (strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.128") == 0)      ||
		   (strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.5") == 0)        ||
		   (strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.6") == 0)        ||
		   (strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.3") == 0)        ||
		   (strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.77.1") == 0)     ||
		   (strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.77.2") == 0)     ||
		   (strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.12.3") == 0)     ||
		   (strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.481.1") == 0)    ||
		   (strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.7") == 0)        ||
		   (strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.6.1") == 0)      ||
		   (strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.3.1") == 0)      ||
		   (strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.77.1.1") == 0)   ||
		   (strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.77.1.2") == 0)   ||
		   (strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.77.1.4") == 0)   ||
		   (strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.77.1.3") == 0)   ||
		   (strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.77.1.1.1") == 0) ||
		   (strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.77.1.2.1") == 0) ||
		   (strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.77.1.4.1") == 0) ||
		   (strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.12.1") == 0)     ||
		   (strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.12.2") == 0)     ||
		   (strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.13.1.3") == 0))
		{
			return true;
		}
		return false;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	bool DICOMUtil::IsImageType(DcmDataset *pDataset)
	{
		const char *pSOPClassUID = NULL;

		if(pDataset)
		{
			pDataset->findAndGetString(DCM_SOPClassUID, pSOPClassUID);
			if(pSOPClassUID)
			{
				return IsImageType(pSOPClassUID);
			}
		}
		return false;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	bool DICOMUtil::isStorageAbstractSyntax(const char* abstractSyntax)
	{
		bool b = false;

		for(size_t i = 0; i < (sizeof(storageUid) / sizeof(const char*)); ++i)
		{
			if(strcmp(abstractSyntax, storageUid[i]) == 0)
			{
				b = true;
				break;
			}
		}

		return (b || dcmIsaStorageSOPClassUID(abstractSyntax));
	}
}
