/////////////////////////////////////////////////////////////////////////////
//	PacsCommon.h
/////////////////////////////////////////////////////////////////////////////

#if !defined(__PACSCOMMON_H_INCLUDED__)
#define      __PACSCOMMON_H_INCLUDED__

/////////////////////////////////////////////////////////////////////////////
namespace imapacs
{
	/////////////////////////////////////////////////////////////////////////
	//	Constants.
	/////////////////////////////////////////////////////////////////////////

	/////////////////////////////////////////////////////////////////////////
	//
	#define	IMAPACS_VERSION				"3.6.0.3"
	#define PRODUCT_FAMILY				"Interview"
	#define	APP_NAME					"IMAPACS"
	#define	ABOUT_MESSAGE				"Copyright (c)2001-2020\nIMAGEM Solutions TI Sante,\nTous droits reserves.\n\nIMAGEM Solutions TI Sante\n930, rue Jacques-Cartier Est, Suite A-204\nChicoutimi (Quebec),\nCanada  G7H 7K9\nTelephone   : 418 602-3495\nTelecopieur : 418 543-4812\nCourriel    : support@imagemsoft.com\nSite web    : http://www.imagemsoft.com\n\n"
	#define	CONFIGURATION_FILE			"./Imapacs.cfg"
	#define	LOG_PATH					"../log"
	#define TMP_PATH					"../tmpimg"
	#define DEBUG_DIRECTORY             "./debugdata/"
	#define	BEGIN_LOG_LINE				"*******************************************"
	#define	END_LOG_LINE				"*******************************************"
	#define	BEGIN_TRANSACTION_LINE		"-------------------------------------------"
	#define	END_TRANSACTION_LINE		"-------------------------------------------"
	#define	PERCENT_SIGN				0x25	//	Because gcc is strict with '%%'.
	#define PKG_NAME					"PKG_PACS"

	/////////////////////////////////////////////////////////////////////////
	//	Enums.
	/////////////////////////////////////////////////////////////////////////

	/////////////////////////////////////////////////////////////////////////
	//
	enum QUERY_STATUS
	{
		QS_OK_NO_RESULT = 0,
		QS_OK_RESULT    = 1,
		QS_ERROR        = 3
	};

	/////////////////////////////////////////////////////////////////////////
	//
	enum ASC_REFUSE_REASON
	{
		ARR_TOOMANYASSOCIATIONS = 0,
		ARR_BADAPPCONTEXT       = 1,
		ARR_BADAEPEER           = 2,
		ARR_BADAESERVICE        = 3,
		ARR_NOSPECIFICREASON    = 4
	};

	/////////////////////////////////////////////////////////////////////////////
	//	Imagem private tags.
	/////////////////////////////////////////////////////////////////////////////

	#define	DCM_ImagemRequestedScale	DcmTagKey(0x0041, 0x1002)	//	Rescale down tag, lenght = 64.
}

#endif	//	__PACSCOMMON_H_INCLUDED__
