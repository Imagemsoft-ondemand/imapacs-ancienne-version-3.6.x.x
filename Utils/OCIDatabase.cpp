/////////////////////////////////////////////////////////////////////////////////////////
//	File		:	OCIatabase.cpp
//	Description	:	Implementation.
/////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////
//

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "OCIDatabase.h"

#ifdef WINDOWS
#include "stdafx.h"
#include <new>
#else
#include <unistd.h>
#include <dlfcn.h>
#endif

#define MAXBUFLEN 1024

/////////////////////////////////////////////////////////////////////////////////////////
//
namespace tool
{

/////////////////////////////////////////////////////////////////////////////////////////
//
COCIDatabase::COCIDatabase() : mIsUsingSsl(false), mhDllHandle(NULL)
{
	m_svchp = NULL;
	m_authp = NULL;
	m_srvhp = NULL;
	m_authp = NULL;
	m_ocienv = NULL;
	m_svchp = NULL;
	m_srvhp = NULL;

	m_Connected = false;

	msEmptyString[0] = 0x00;
	msEmptyString[1] = 0x00;

	memset(m_DataSource, 0, sizeof(m_DataSource));
	memset(m_User, 0, sizeof(m_User));
	memset(m_Password, 0, sizeof(m_Password));
	memset(m_sLastError, 0, sizeof(m_sLastError));

	m_Host.clear();

#ifdef _WINDOWS
	ResetPointers();
#endif
}

/////////////////////////////////////////////////////////////////////////////////////////
//
COCIDatabase::~COCIDatabase()
{
	Close();
}

/////////////////////////////////////////////////////////////////////////////////////////
//
const char *COCIDatabase::GetVersion()
{
	return OCI_VERSION;
}

/////////////////////////////////////////////////////////////////////////////////////////
//
HANDLE COCIDatabase::GetCursor(bool scrollable)
{
	if (!m_Connected)
	{
		bool bRetry = true;
		HANDLE cursor;

		do
		{
			cursor = Open(mHostConnectInfo.c_str(),
						  mPortConnectInfo.c_str(),
						  mDataSourceConnectInfo.c_str(),
						  mUserConnectInfo.c_str(),
						  mPasswordConnectInfo.c_str(),
						  mpClientConnectInfo.c_str(),
						  mIsUsingSsl,
						  mpNlsLangConnectInfo.c_str());

			if (cursor == NULL)
			{
#ifdef WINDOWS
				if (MessageBox(NULL, L"DataBase has lost connection. Check your network setting then retry.", L"Database connection error", MB_RETRYCANCEL | MB_SYSTEMMODAL) != IDRETRY)
					bRetry = false;
#endif
			}

		} while (cursor == NULL && bRetry);

		return cursor;
	}

#ifdef _DEBUG
	envStruct *envVar = new envStruct;
#else
	envStruct *envVar = new (std::nothrow) envStruct;
#endif

	if (envVar != NULL)
	{
		memset(envVar->ErrMsg, 0, sizeof(envVar->ErrMsg));

		envVar->ErrCode = 0;
		envVar->RecordCount = 0;
		envVar->scrollable = scrollable;
		envVar->stmthp = NULL;

		_OCIHandleAlloc((dvoid *)m_ocienv, (dvoid **)&envVar->ocierr, OCI_HTYPE_ERROR,
						(size_t)0, (dvoid **)0);
	}
	return envVar;
}

/////////////////////////////////////////////////////////////////////////////////////////
//
bool COCIDatabase::DeleteCursor(HANDLE cursor)
{
	if (cursor != NULL)
	{
		envStruct *stenvStruct = (envStruct *)cursor;

		if (stenvStruct->stmthp)
			_OCIStmtRelease(stenvStruct->stmthp, stenvStruct->ocierr, NULL, 0, OCI_DEFAULT);

		if (stenvStruct->ocierr != NULL)
			_OCIHandleFree((dvoid *)stenvStruct->ocierr, OCI_HTYPE_ERROR);

		ClearOCIDateList(&stenvStruct->dtCollection);

		ClearDbVarList(&stenvStruct->VarList);

		if (stenvStruct == m_Cursor)
		{
			m_Cursor = NULL;
		}

		delete stenvStruct;
		stenvStruct = NULL;
	}
	return true;
}

/////////////////////////////////////////////////////////////////////////////////////////
//
void COCIDatabase::FreeHandle()
{
	if (m_svchp)
	{
		_OCIHandleFree((void *)m_svchp, OCI_HTYPE_SVCCTX);
		m_svchp = NULL;
	}

	if (m_authp)
	{
		_OCIHandleFree((void *)m_authp, OCI_HTYPE_SESSION);
		m_authp = NULL;
	}

	if (m_srvhp)
	{
		_OCIHandleFree((void *)m_srvhp, OCI_HTYPE_SERVER);
		m_srvhp = NULL;
	}

	if (m_ocienv)
	{
		_OCIHandleFree((void *)m_ocienv, OCI_HTYPE_ENV);
		m_ocienv = NULL;
	}
}

/////////////////////////////////////////////////////////////////////////////////////////
//
HANDLE COCIDatabase::Open(const char *pHost,
						  const char *pPort,
						  const char *DataSource,
						  const char *User,
						  const char *Password,
						  const char *pClient,
						  bool useSsl,
						  const char *pNlsLang)
{
	char tempVar[2048 + 1];
	mHostConnectInfo = pHost;
	mPortConnectInfo = pPort;
	mDataSourceConnectInfo = DataSource;
	mUserConnectInfo = User;
	mPasswordConnectInfo = Password;
	mpClientConnectInfo = pClient;
	mpNlsLangConnectInfo = pNlsLang;

	sword retcode = 0;
	OCIError *ocierr = NULL;

	std::string sOracleClientPath;

	if (!pClient)
	{
		return (NULL);
	}

	mIsUsingSsl = useSsl;

	if (pPort && pPort[0] &&
		DataSource && DataSource[0] &&
		User && User[0] &&
		Password && Password[0] &&
		pNlsLang && pNlsLang[0])
	{
		if(m_Connected)
		{
			Close();
			UnloadClient();
		}

#ifdef WINDOWS
		SetEnvironmentVariableA("NLS_LANG", pNlsLang);
#else
		sprintf(tempVar, "NLS_LANG=%s", pNlsLang);
		putenv(tempVar); //	Directory where sqlnat.ora is to be found.
#endif
		char sDirectory[2048 + 1] = {0};

#ifdef WINDOWS
		GetCurrentDirectoryA(sizeof(sDirectory), sDirectory);
#else
		if(getcwd(sDirectory, sizeof(sDirectory)) == NULL)
		{
			sprintf(m_sLastError, "Failed to getcwd");
			return NULL;
		}
#endif
		CreateSqlnet(sDirectory);

#ifdef WINDOWS
		SetEnvironmentVariableA("TNS_ADMIN", sDirectory);   //	Directory where sqlnat.ora is to be found.
		SetEnvironmentVariableA("ORACLE_HOME", sDirectory); //	Directory where tnsname.ora is to be found.
#else
		sprintf(tempVar, "TNS_ADMIN=%s", sDirectory);
		putenv(tempVar); //	Directory where sqlnat.ora is to be found.
		sprintf(tempVar, "ORACLE_HOME=%s", sDirectory);
		putenv(tempVar); //	Directory where tnsname.ora is to be found.
#endif

#ifdef WINDOWS
		// sOracleClientPath.assign(pClient);

		//  if ((sOracleClientPath.length() > 0) && (sOracleClientPath[sOracleClientPath.length() - 1] != '/'))
		//  	sOracleClientPath += "/";
#ifdef _WIN64
		sOracleClientPath += "x64/";
#endif
		sOracleClientPath += "oci.dll";
#else
		sOracleClientPath += "libclntsh.so.12.1";
#endif
		if(!LoadClient(sOracleClientPath.c_str()))
		{
			sprintf(m_sLastError, "Failed to load oci Oracle client : %s", sOracleClientPath.c_str());
			return NULL;
		}

		strcpy(m_DataSource, DataSource);
		strcpy(m_User, User);
		strcpy(m_Password, Password);

		if(pHost && pHost[0])
			m_Host.assign(pHost);
		else
			m_Host.clear();

		//	Alloc oci and environment.
		retcode = _OCIEnvCreate(&m_ocienv, OCI_THREADED | OCI_OBJECT, NULL, NULL, NULL, NULL, 0, NULL);

		if(retcode != OCI_SUCCESS)
		{
			strncpy(m_sLastError, "Failed to create environment handle.", sizeof(m_sLastError));

			UnloadClient();
			return NULL;
		}

		retcode = _OCIHandleAlloc((dvoid *)m_ocienv, (dvoid **)&ocierr, OCI_HTYPE_ERROR,
								  (size_t)0, (dvoid **)0);

		if(retcode != OCI_SUCCESS)
		{
			strncpy(m_sLastError, "Failed to create allocate error handle.", sizeof(m_sLastError));

			FreeHandle();
			UnloadClient();

			return NULL;
		}

		//	Server contexts.
		retcode = _OCIHandleAlloc(m_ocienv, (void **)&m_srvhp, OCI_HTYPE_SERVER, 0, 0);

		if(retcode != OCI_SUCCESS)
		{
			SetLastOciConnectionError(ocierr);

			_OCIHandleFree((void *)ocierr, OCI_HTYPE_ERROR);

			FreeHandle();

			UnloadClient();

			return NULL;
		}

		retcode = _OCIHandleAlloc(m_ocienv, (void **)&m_svchp, OCI_HTYPE_SVCCTX, 0, 0);

		if(retcode != OCI_SUCCESS)
		{
			SetLastOciConnectionError(ocierr);

			_OCIHandleFree((void *)ocierr, OCI_HTYPE_ERROR);

			FreeHandle();

			UnloadClient();

			return NULL;
		}

		char connectionString[100] = {0};
		sprintf(connectionString, "//%s:%s/%s", pHost, pPort, DataSource);

		//	Establish link with the server and database.
		retcode = _OCIServerAttach(m_srvhp,
								   ocierr,
								   (text *)connectionString,
								   (sb4)strlen(connectionString),
								   OCI_DEFAULT);

		if(retcode != OCI_SUCCESS)
		{
			SetLastOciConnectionError(ocierr);

			_OCIServerDetach(m_srvhp, ocierr, OCI_DEFAULT);
			_OCIHandleFree((void *)ocierr, OCI_HTYPE_ERROR);

			FreeHandle();

			UnloadClient();

			return NULL;
		}

		//	Set attribute server context in the service context.
		//
		retcode = _OCIAttrSet(m_svchp, OCI_HTYPE_SVCCTX, (dvoid *)m_srvhp, (ub4)0, OCI_ATTR_SERVER, (OCIError *)ocierr);

		if(retcode != OCI_SUCCESS)
		{
			SetLastOciConnectionError(ocierr);

			_OCIServerDetach(m_srvhp, ocierr, OCI_DEFAULT);
			_OCIHandleFree((void *)ocierr, OCI_HTYPE_ERROR);

			FreeHandle();

			UnloadClient();

			return NULL;
		}

		retcode = _OCIHandleAlloc(m_ocienv, (void **)&m_authp, (ub4)OCI_HTYPE_SESSION, (size_t)0, (dvoid **)0);

		if(retcode != OCI_SUCCESS)
		{
			SetLastOciConnectionError(ocierr);

			_OCIServerDetach(m_srvhp, ocierr, OCI_DEFAULT);
			_OCIHandleFree((void *)ocierr, OCI_HTYPE_ERROR);

			FreeHandle();

			UnloadClient();

			return NULL;
		}

		retcode = _OCIAttrSet(m_authp, (ub4)OCI_HTYPE_SESSION, (dvoid *)m_User, (ub4)strlen((char *)m_User), (ub4)OCI_ATTR_USERNAME, ocierr);

		if(retcode != OCI_SUCCESS)
		{
			SetLastOciConnectionError(ocierr);

			_OCIServerDetach(m_srvhp, ocierr, OCI_DEFAULT);
			_OCIHandleFree((void *)ocierr, OCI_HTYPE_ERROR);

			FreeHandle();

			UnloadClient();

			return NULL;
		}

		retcode = _OCIAttrSet(m_authp, (ub4)OCI_HTYPE_SESSION, (dvoid *)m_Password, (ub4)strlen((char *)m_Password), (ub4)OCI_ATTR_PASSWORD, ocierr);

		if(retcode != OCI_SUCCESS)
		{
			SetLastOciConnectionError(ocierr);

			_OCIServerDetach(m_srvhp, ocierr, OCI_DEFAULT);
			_OCIHandleFree((void *)ocierr, OCI_HTYPE_ERROR);

			FreeHandle();

			UnloadClient();

			return NULL;
		}

		retcode = _OCISessionBegin(m_svchp, ocierr, m_authp, OCI_CRED_RDBMS, (ub4)OCI_DEFAULT);
		
		if(retcode != OCI_SUCCESS)
		{
			SetLastOciConnectionError(ocierr);

			_OCISessionEnd(m_svchp, ocierr, m_authp, OCI_DEFAULT);
			_OCIServerDetach(m_srvhp, ocierr, OCI_DEFAULT);
			_OCIHandleFree((void *)ocierr, OCI_HTYPE_ERROR);

			FreeHandle();

			UnloadClient();

			return NULL;
		}

		_OCIAttrSet((dvoid *)m_svchp, (ub4)OCI_HTYPE_SVCCTX, (dvoid *)m_authp, (ub4)0, (ub4)OCI_ATTR_SESSION, ocierr);

		if(retcode != OCI_SUCCESS)
			SetLastOciConnectionError(ocierr);

		_OCIHandleFree((dvoid *)ocierr, OCI_HTYPE_ERROR);

		m_Connected = true;

		m_Cursor = GetCursor();

		return m_Cursor;
	}
	return (NULL);
}

/////////////////////////////////////////////////////////////////////////////////////////
//
bool COCIDatabase::Close()
{
	OCIError *ocierr = NULL;
	if (m_Connected)
	{
		if (m_Cursor != NULL)
		{
			DeleteCursor(m_Cursor);
		}

		_OCIHandleAlloc((dvoid *)m_ocienv,
						(dvoid **)&ocierr,
						OCI_HTYPE_ERROR,
						(size_t)0,
						(dvoid **)0);

		_OCISessionEnd(m_svchp, ocierr, m_authp, OCI_DEFAULT);
		_OCIServerDetach(m_srvhp, ocierr, OCI_DEFAULT);

		FreeHandle();

#ifdef _WINDOWS
		UnloadClient();
#endif
	}
	m_Connected = false;

	return true;
}

/////////////////////////////////////////////////////////////////////////////////////////
//
bool COCIDatabase::isConnect()
{
	return m_Connected;
}

/////////////////////////////////////////////////////////////////////////////////////////
//
bool COCIDatabase::NewStatement(HANDLE cursor)
{
	if (cursor != NULL)
	{
		sword retcode = 0;
		envStruct *stenvStruct = (envStruct *)cursor;

		if (stenvStruct->stmthp)
			_OCIStmtRelease(stenvStruct->stmthp, stenvStruct->ocierr, NULL, 0, OCI_DEFAULT);

		stenvStruct->ErrCode = 0;
		stenvStruct->RecordCount = 0;
		memset(stenvStruct->ErrMsg, 0, sizeof(stenvStruct->ErrMsg));

		ClearOCIDateList(&stenvStruct->dtCollection);
	}
	else
		return false;
	return true;
}

/////////////////////////////////////////////////////////////////////////////////////////
//
bool COCIDatabase::SQLExec(HANDLE &cursor, const char *sql)
{
	std::vector<char *> vector;
	return SQLExec(cursor, sql, &vector);
}

/////////////////////////////////////////////////////////////////////////////////////////
//
bool COCIDatabase::SQLExec(HANDLE &cursor, const char *select, const char *format, ...)
{
	std::vector<char *> vector;

	va_list varlist;

	va_start(varlist, format);

	for (int i = 0; i < (int)strlen(format); i++)
	{
		switch (format[i])
		{
		case 'l':
			break;

		case 's':
			vector.push_back(va_arg(varlist, char *));

			break;

		case 'f':
			break;
		}
	}
	va_end(varlist);

	return SQLExec(cursor, select, &vector);
}

/////////////////////////////////////////////////////////////////////////////////////////
//
bool COCIDatabase::SQLExec(HANDLE &cursor, const char *select, std::vector<char *> *returnedValues, bool retry)
{
	if (cursor != NULL)
	{
		envStruct *stenvStruct = (envStruct *)cursor;
		sword RetVal;

		if (!m_Connected)
			return RetrySqlExec(cursor, select, returnedValues);

		ClearDbVarList(&stenvStruct->VarList);

		//////////////////////////////////////////////////////////////
		// On enl�ve les erreurs avant d'ex�cuter un nouveau Select
		// Sylvain 20060828
		stenvStruct->ErrCode = 0;
		stenvStruct->RecordCount = 0;
		memset(stenvStruct->ErrMsg, 0, sizeof(stenvStruct->ErrMsg));

		NewStatement(cursor);
		RetVal = _OCIStmtPrepare2(m_svchp, &stenvStruct->stmthp, stenvStruct->ocierr, (unsigned char *)select, (ub4)strlen(select), NULL, 0, (ub4)OCI_NTV_SYNTAX, (ub4)OCI_DEFAULT);

		if ((RetVal != OCI_SUCCESS) &&
			(RetVal != OCI_SUCCESS_WITH_INFO))
		{
			SetLastOciError(cursor, RetVal);

			if (retry && !m_Connected)
				return RetrySqlExec(cursor, select, returnedValues);

			return false;
		}

		if (returnedValues)
		{
			int count = 1;
			for (auto it = returnedValues->begin(); it != returnedValues->end(); it++)
			{
				dbVar *v = new dbVar();
				v->type = SQLT_STR;
				v->var.s = *it;
				BindField(stenvStruct, count++, SQLT_STR, (void *)v->var.s, 65535, (sb4 *)&v->size, (void *)&v->indicator);
				stenvStruct->VarList.push_back(v);
			}
		}

		if (stenvStruct->scrollable)
			RetVal = _OCIStmtExecute(m_svchp, stenvStruct->stmthp, stenvStruct->ocierr, (ub4)1, (ub4)0,
									 (CONST OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_STMT_SCROLLABLE_READONLY);
		else
			RetVal = _OCIStmtExecute(m_svchp, stenvStruct->stmthp, stenvStruct->ocierr, (ub4)1, (ub4)0,
									 (CONST OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);

		if (RetVal == OCI_SUCCESS || RetVal == OCI_SUCCESS_WITH_INFO)
		{
			return true;
		}
		else
		{
			if (RetVal != OCI_NO_DATA)
			{
				SetLastOciError(cursor, RetVal);

				if (retry && !m_Connected)
					return RetrySqlExec(cursor, select, returnedValues);

				return false;
			}
			return false;
		}
	}
	return false;
}

bool COCIDatabase::RetrySqlExec(HANDLE &cursor, const char *select, std::vector<char *> *returnedValues)
{
	if (!m_Connected)
	{
		bool bRetry = true;

		do
		{
			cursor = Open(mHostConnectInfo.c_str(),
						  mPortConnectInfo.c_str(),
						  mDataSourceConnectInfo.c_str(),
						  mUserConnectInfo.c_str(),
						  mPasswordConnectInfo.c_str(),
						  mpClientConnectInfo.c_str(),
						  mIsUsingSsl,
						  mpNlsLangConnectInfo.c_str());

			if (cursor == NULL)
			{
#ifdef WINDOWS
				if (MessageBox(NULL, L"DataBase has lost connection. Check your network setting then retry.", L"Database connection error", MB_RETRYCANCEL) != IDRETRY)
					bRetry = false;
#endif
			}

		} while (cursor == NULL && bRetry);

		if (cursor != NULL)
			return SQLExec(cursor, select, returnedValues, false);
	}
	return false;
}

/////////////////////////////////////////////////////////////////////////////////////////
///	Creates the sqlnet.ora file with ssl configurations in the specified directory.
///
///	directory				:	Directory where the sqlnet.ora will be created.
///
///	Return value			:	true if successfule, false if not.
///
bool COCIDatabase::CreateSqlnet(const char *directory)
{
	bool success = false;
	std::string file(directory);
	std::string ociLogDirectory(directory);
	std::string certificatesDirectory(directory);

	if ((file.length() > 0) && (file[file.length() - 1] != '\\'))
	{
		file += "\\";
	}
	file += "sqlnet.ora";

	if ((ociLogDirectory.length() > 0) && (ociLogDirectory[ociLogDirectory.length() - 1] != '\\'))
	{
		ociLogDirectory += "\\";
	}
	ociLogDirectory += "OciLog";

	if ((certificatesDirectory.length() > 0) && (certificatesDirectory[certificatesDirectory.length() - 1] != '\\'))
	{
		certificatesDirectory += "\\";
	}
	certificatesDirectory += "certs";

	FILE *fp = fopen(file.c_str(), "wb");
	if (fp)
	{
		fprintf(fp, "#SQLNET.AUTHENTICATION_SERVICES= (NONE)\r\n\r\n");
		fprintf(fp, "SQLNET.AUTHENTICATION_SERVICES= (BEQ, TCPS)\r\n\r\n");

		//
		//	Values for this option are :
		//
		//	off or 0		for no trace output.
		//	user or 4		for user trace information.
		//	admin or 10		for administration trace information.
		//	support or 16	for Oracle Support Services trace information.
		//
		//	Reference : https://docs.oracle.com/cd/E11882_01/network.112/e10835/sqlnet.htm#NETRF250
		//
		//	NOTE 1 : Activating this option creates log files every time the application
		//	connects or do some casual operations especially if the option is support.
		//
		//	NOTE 2 : There is also an option called TRACE_LEVEL_SERVER to get informations
		//	about the server errors. The values are the same as the TRACE_LEVEL_CLIENT opriotn.
		//
		fprintf(fp, "TRACE_LEVEL_CLIENT = %s\r\n\r\n", "off");

		fprintf(fp, "NAMES.DIRECTORY_PATH= (TNSNAMES, EZCONNECT)\r\n\r\n");

		fprintf(fp, "ADR_BASE = %s\n\n", ociLogDirectory.c_str());

		fprintf(fp, "SSL_CLIENT_AUTHENTICATION = FALSE\r\n\r\n");

		fprintf(fp, "wallet_location = (SOURCE=(METHOD=File)(METHOD_DATA=(DIRECTORY=%s)))\r\n\r\n", certificatesDirectory.c_str());

		fprintf(fp, "SSL_SERVER_DN_MATCH=(OFF)\r\n\r\n");

		fprintf(fp, "SSL_VERSION=UNDETERMINED\r\n\r\n");

		fprintf(fp, "SSL_CIPHER_SUITES= (SSL_RSA_WITH_3DES_EDE_CBC_SHA, SSL_RSA_WITH_RC4_128_MD5, SSL_RSA_WITH_DES_CBC_SHA, SSL_RSA_EXPORT_WITH_DES40_CBC_SHA, SSL_DH_anon_WITH_RC4_128_MD5, SSL_DH_anon_WITH_3DES_EDE_CBC_SHA, SSL_RSA_EXPORT_WITH_RC4_40_MD5, SSL_DH_anon_WITH_DES_CBC_SHA)\r\n\r\n");

		success = true;

		fclose(fp);
	}
	return success;
}

/////////////////////////////////////////////////////////////////////////////////////////
//
bool COCIDatabase::BindField(HANDLE cursor,
							 ub4 Column,
							 ub2 CTypeCode,
							 void *vPtr,
							 sb4 vPtrLength,
							 sb4 *LengthNeeded,
							 void *indicator)
{
	if (cursor != NULL)
	{
		sword RetCode;
		envStruct *stenvStruct = (envStruct *)cursor;
		if (!stenvStruct->stmthp)
			return false;

		RetCode = _OCIDefineByPos(stenvStruct->stmthp,
								  &stenvStruct->defnp,
								  stenvStruct->ocierr,
								  /*1*/ Column,
								  (dvoid *)vPtr,
								  vPtrLength,
								  CTypeCode,
								  /*indicator, reason for null error.*/ (dvoid *)indicator, (ub2 *)0, (ub2 *)0, OCI_DEFAULT);

		if ((RetCode != OCI_SUCCESS) &&
			(RetCode != OCI_SUCCESS_WITH_INFO))
		{
			SetLastOciError(cursor, RetCode);
			return false;
		}
	}
	else
		return false;
	return true;
}

/////////////////////////////////////////////////////////////////////////////////////////
//
bool COCIDatabase::prepareStatement(HANDLE cursor, const char *Select)
{
	bool retval = false;

	if (cursor != NULL)
	{
		sword retcode = 0;
		envStruct *stenvStruct = (envStruct *)cursor;

		NewStatement(cursor);

		retcode = _OCIStmtPrepare2(m_svchp, &stenvStruct->stmthp, stenvStruct->ocierr, (unsigned char *)Select, (ub4)strlen(Select), NULL, 0, (ub4)OCI_NTV_SYNTAX, (ub4)OCI_DEFAULT);

		if ((retcode != OCI_SUCCESS) && (retcode != OCI_SUCCESS_WITH_INFO))
			SetLastOciError(cursor, retcode);
		else
			retval = false;
	}
	return retval;
}

/////////////////////////////////////////////////////////////////////////////////////////
//
bool COCIDatabase::executeQuery(HANDLE cursor)
{
	sword retcode = 0;

	if (cursor != NULL)
	{
		envStruct *stenvStruct = (envStruct *)cursor;

		stenvStruct->ErrCode = 0;
		stenvStruct->RecordCount = 0;

		memset(stenvStruct->ErrMsg, 0, sizeof(stenvStruct->ErrMsg));

		if (!m_Connected)
			return false;

		if (stenvStruct->scrollable)
			retcode = _OCIStmtExecute(m_svchp, stenvStruct->stmthp, stenvStruct->ocierr, (ub4)1, (ub4)0,
									  (CONST OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_STMT_SCROLLABLE_READONLY);
		else
			retcode = _OCIStmtExecute(m_svchp, stenvStruct->stmthp, stenvStruct->ocierr, (ub4)1, (ub4)0,
									  (CONST OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);

		if (retcode == OCI_SUCCESS || retcode == OCI_SUCCESS_WITH_INFO)
		{
			return true;
		}
		else
		{
			if (retcode != OCI_NO_DATA)
			{
				SetLastOciError(cursor, retcode);
			}
			return false;
		}
	}
	return false;
}

/////////////////////////////////////////////////////////////////////////////////////////
//
bool COCIDatabase::executeInsert(HANDLE cursor)
{
	sword retcode = 0;

	if (cursor != NULL)
	{
		envStruct *stenvStruct = (envStruct *)cursor;

		stenvStruct->ErrCode = 0;
		stenvStruct->RecordCount = 0;
		memset(stenvStruct->ErrMsg, 0, sizeof(stenvStruct->ErrMsg));

		if (!m_Connected)
			return false;

		if (stenvStruct->scrollable)
			retcode = _OCIStmtExecute(m_svchp, stenvStruct->stmthp, stenvStruct->ocierr, (ub4)1, (ub4)0,
									  (CONST OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_STMT_SCROLLABLE_READONLY);
		else
			retcode = _OCIStmtExecute(m_svchp, stenvStruct->stmthp, stenvStruct->ocierr, (ub4)1, (ub4)0,
									  (CONST OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);

		if (retcode == OCI_SUCCESS || retcode == OCI_SUCCESS_WITH_INFO)
		{
			return true;
		}
		else
		{
			if (retcode != OCI_NO_DATA)
			{
				SetLastOciError(cursor, retcode);
			}
			return false;
		}
	}
	return false;
}

/////////////////////////////////////////////////////////////////////////////////////////
//
bool COCIDatabase::executeUpdate(HANDLE cursor)
{
	sword retcode = 0;

	if (cursor != NULL)
	{
		envStruct *stenvStruct = (envStruct *)cursor;

		stenvStruct->ErrCode = 0;
		stenvStruct->RecordCount = 0;
		memset(stenvStruct->ErrMsg, 0, sizeof(stenvStruct->ErrMsg));

		if (!m_Connected)
			return false;

		if (stenvStruct->scrollable)
			retcode = _OCIStmtExecute(m_svchp, stenvStruct->stmthp, stenvStruct->ocierr, (ub4)1, (ub4)0,
									  (CONST OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_STMT_SCROLLABLE_READONLY);
		else
			retcode = _OCIStmtExecute(m_svchp, stenvStruct->stmthp, stenvStruct->ocierr, (ub4)1, (ub4)0,
									  (CONST OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);

		if (retcode == OCI_SUCCESS || retcode == OCI_SUCCESS_WITH_INFO)
		{
			return true;
		}
		else
		{
			if (retcode != OCI_NO_DATA)
			{
				SetLastOciError(cursor, retcode);
			}
			return false;
		}
	}
	return false;
}

/////////////////////////////////////////////////////////////////////////////////////////
//
bool COCIDatabase::executeDelete(HANDLE cursor)
{
	sword retcode = 0;

	if (cursor != NULL)
	{
		envStruct *stenvStruct = (envStruct *)cursor;

		stenvStruct->ErrCode = 0;
		stenvStruct->RecordCount = 0;
		memset(stenvStruct->ErrMsg, 0, sizeof(stenvStruct->ErrMsg));

		if (!m_Connected)
			return false;

		if (stenvStruct->scrollable)
			retcode = _OCIStmtExecute(m_svchp, stenvStruct->stmthp, stenvStruct->ocierr, (ub4)1, (ub4)0,
									  (CONST OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_STMT_SCROLLABLE_READONLY);
		else
			retcode = _OCIStmtExecute(m_svchp, stenvStruct->stmthp, stenvStruct->ocierr, (ub4)1, (ub4)0,
									  (CONST OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);

		if (retcode == OCI_SUCCESS || retcode == OCI_SUCCESS_WITH_INFO)
		{
			return true;
		}
		else
		{
			if (retcode != OCI_NO_DATA)
			{
				SetLastOciError(cursor, retcode);
			}
			return false;
		}
	}
	return false;
}

/////////////////////////////////////////////////////////////////////////////////////////
// Get the Locator for the requested CLOB.
// Returns true if successful, false otherwise.
bool COCIDatabase::SQLGetClob(HANDLE cursor, const char *Select)
{
	sword retval;

	if (!m_Connected)
		return false;

	if (cursor != NULL)
	{
		envStruct *stenvStruct = (envStruct *)cursor;

		sword RetVal;
		OCIDefine *defnp1;

		RetVal = _OCIStmtPrepare2(m_svchp, &stenvStruct->stmthp, stenvStruct->ocierr, (unsigned char *)Select,
								  (ub4)strlen(Select), NULL, 0, (ub4)OCI_NTV_SYNTAX,
								  (ub4)OCI_DEFAULT);

		if ((RetVal != OCI_SUCCESS) && (RetVal != OCI_SUCCESS_WITH_INFO))
			return false;

		RetVal = _OCIDefineByPos(stenvStruct->stmthp, &defnp1, stenvStruct->ocierr, (ub4)1,
								 (dvoid *)&stenvStruct->loblocator, (sb4)0,
								 (ub2)SQLT_CLOB, (dvoid *)0,
								 (ub2 *)0, (ub2 *)0, (ub4)OCI_DEFAULT);

		if (RetVal != OCI_SUCCESS)
			return false;

		if (stenvStruct->scrollable)
			RetVal = _OCIStmtExecute(m_svchp, stenvStruct->stmthp, stenvStruct->ocierr, (ub4)1, (ub4)0,
									 (CONST OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_STMT_SCROLLABLE_READONLY);
		else
			RetVal = _OCIStmtExecute(m_svchp, stenvStruct->stmthp, stenvStruct->ocierr, (ub4)1, (ub4)0,
									 (CONST OCISnapshot *)NULL, (OCISnapshot *)NULL, OCI_DEFAULT);

		if (RetVal == OCI_SUCCESS || RetVal == OCI_SUCCESS_WITH_INFO)
			retval = true;
		else
		{
			SetLastOciError(stenvStruct->ocierr, RetVal);
			retval = false;
		}
	}

	return retval ? true : false;
}

/////////////////////////////////////////////////////////////////////////////////////////
// Read the selected CLOB's data and write it to pathname.
// Returns true if successful, false otherwise.
bool COCIDatabase::GetClobDataIntoFile(HANDLE cursor, const char *Select, const char *pathname)
{
	if (!m_Connected)
		return false;

	ub4 amt = 0;
	ub4 offset = 0;
	sword RetVal = 0;
	ub1 bufp[1025] = {0};
	ub4 buflen = 0;

	boolean done = FALSE; // Read loop control variable.
	FILE *fd = NULL;

	if (cursor != NULL)
	{
		envStruct *stenvStruct = (envStruct *)cursor;

		ClearDbVarList(&stenvStruct->VarList);

		if ((Select == NULL) || (pathname == NULL))
			return false;

		// Allocate the Source (bfile) & destination (clob) locators desriptors.
		_OCIDescriptorAlloc((dvoid *)m_ocienv, (dvoid **)&stenvStruct->loblocator,
							(ub4)OCI_DTYPE_LOB, (size_t)0, (dvoid **)0);

		// Select the CLOB.
		if (SQLGetClob(cursor, Select) == false)
		{
			// Free resources held by the locators.
			(void)_OCIDescriptorFree((dvoid *)stenvStruct->loblocator, (ub4)OCI_DTYPE_LOB);
			return false;
		}

		// Open the CLOB.
		RetVal = _OCILobOpen(m_svchp, stenvStruct->ocierr, stenvStruct->loblocator, OCI_LOB_READONLY);

		if (RetVal != OCI_SUCCESS)
		{
			// Free resources held by the locators.
			(void)_OCIDescriptorFree((dvoid *)stenvStruct->loblocator, (ub4)OCI_DTYPE_LOB);
			return false;
		}

		// Setting the amt to the buffer length. Note here that amt is in bytes
		// since we are using a CLOB.
		buflen = sizeof(bufp);

		// Process the data in pieces.
		offset = 1;
		memset(bufp, 0x00, 1024);

		fd = fopen(pathname, "wb");

		//**************************************************************************
		// Si pathname existe on doit le d�truire ********************************** <<<<<<<<<<<<
		//**************************************************************************

		if (fd == NULL)
		{
			// Closing the CLOB is mandatory if you have opened it.
			_OCILobClose(m_svchp, stenvStruct->ocierr, stenvStruct->loblocator);

			// Free resources held by the locators.
			_OCIDescriptorFree((dvoid *)stenvStruct->loblocator, (ub4)OCI_DTYPE_LOB);
			return false;
		}

		while (done != TRUE)
		{
			RetVal = _OCILobRead(m_svchp, stenvStruct->ocierr, stenvStruct->loblocator, &amt, offset, (dvoid *)bufp,
								 buflen, (dvoid *)0,
								 /*(sb4 (*)(dvoid *, dvoid *, ub4, ub1))*/ 0,
								 (ub2)0, (ub1)SQLCS_IMPLICIT);

			switch (RetVal)
			{
			// Only one piece since amt == bufp
			// Process the data in bufp. amt will give the amount of data just read in
			// bufp. This is in bytes for CLOBs and in characters for fixed
			// width CLOBS and in bytes for variable width CLOBs.
			case OCI_SUCCESS:
				fwrite(bufp, amt, 1, fd);
				fclose(fd);
				fd = NULL;
				done = TRUE;
				break;

			// An error as occured. Close the file.
			case OCI_ERROR:
				fclose(fd);
				fd = NULL;
				done = TRUE;
				break;

			// More data needs to be read.
			case OCI_NEED_DATA:
				fwrite(bufp, amt, 1, fd);
				break;

			// An unexpected error as occured in OCILobRead().
			default:
				if (fd != NULL)
				{
					fclose(fd);
					fd = NULL;
				}
				done = TRUE;
				break;
			} // End switch.
		}	 // End while loop.

		if (fd != NULL)
		{
			fclose(fd);
			fd = NULL;
		}

		// Closing the CLOB is mandatory if you have opened it.
		RetVal = _OCILobClose(m_svchp, stenvStruct->ocierr, stenvStruct->loblocator);

		// Free resources held by the locators.
		(void)_OCIDescriptorFree((dvoid *)stenvStruct->loblocator, (ub4)OCI_DTYPE_LOB);
	}

	return true;
}

/////////////////////////////////////////////////////////////////////////////////////////
// Write the selected CLOB's data, taking the data from pathname.
// The record must have been inserted whith the non CLOB fiels
// Before calling this function.
// Returns true if successful, false otherwise.
bool COCIDatabase ::SaveFileDataIntoClob(HANDLE cursor, const char *Select, const char *pathname)
{
	if (!m_Connected)
		return false;

	FILE *fd = NULL;
	unsigned int amt = 0;
	unsigned int offset = 0;
	unsigned int remainder = 0, nbytes = 0;
	boolean last = FALSE;
	ub1 bufp[MAXBUFLEN] = {0};
	sb4 err = 0;
	sword RetVal = 0;
	char *filebuffer = NULL;

	if (cursor != NULL)
	{
		envStruct *stenvStruct = (envStruct *)cursor;

		ub4 Total = /*2.5**/ MAXBUFLEN; // total amount of data to write to the CLOB in bytes>.

		if ((Select == NULL) || (pathname == NULL))
			return false;

		if ((fd = fopen(pathname, "rb")) == NULL)
			return false;

		// Allocate data buffer and read data from the buffer.
		Total = GetFileSize(fd);
		filebuffer = new char[Total];
		size_t readSize = fread((char *)filebuffer, 1, Total, fd);
		fclose(fd);
		fd = NULL;

		// Allocate the locators desriptors.
		(void)_OCIDescriptorAlloc((dvoid *)m_ocienv, (dvoid **)&stenvStruct->loblocator,
								  (ub4)OCI_DTYPE_LOB, (size_t)0, (dvoid **)0);

		// Select the CLOB.
		if (SQLGetClob(cursor, Select) == false)
		{
			// Free resources held by the locators.
			(void)_OCIDescriptorFree((dvoid *)stenvStruct->loblocator, (ub4)OCI_DTYPE_LOB);
			return false;
		}

		// Open the CLOB.
		RetVal = _OCILobOpen(m_svchp, stenvStruct->ocierr, stenvStruct->loblocator, OCI_LOB_READWRITE);

		if (RetVal != OCI_SUCCESS)
		{
			// Free resources held by the locators.
			(void)_OCIDescriptorFree((dvoid *)stenvStruct->loblocator, (ub4)OCI_DTYPE_LOB);
			return false;
		}

		if (Total > MAXBUFLEN)
		{
			nbytes = MAXBUFLEN; // We will use streaming via standard polling.
		}
		else
		{
			nbytes = Total; // Only a single write is required.
		}
		memcpy(bufp, filebuffer, nbytes);

		// Fill the buffer with nbytes worth of data.
		remainder = Total - nbytes;

		// Setting Amount to 0 streams the data until use specifies OCI_LAST_PIECE.
		amt = 0;

		offset = 1;

		if (0 == remainder) // Only a single write.
		{
			amt = nbytes;
			memcpy(bufp, filebuffer, Total); // L.B.	2002/11/27.
											 // Put the data into the buffer.
			// Here, (Total <= MAXBUFLEN ) so we can write in one piece.
			_OCILobWrite(m_svchp, stenvStruct->ocierr, stenvStruct->loblocator, &amt,
						 offset, bufp, nbytes,
						 OCI_ONE_PIECE, (dvoid *)0,
						 (sb4(*)(dvoid *, dvoid *, ub4 *, ub1 *))0,
						 0, SQLCS_IMPLICIT);
		}
		else
		{
			// Here (Total > MAXBUFLEN ) so we use streaming via standard polling
			// write the first piece. Specifying first initiates polling.
			memcpy(bufp, filebuffer, nbytes);
			err = _OCILobWrite(m_svchp, stenvStruct->ocierr, stenvStruct->loblocator, &amt,
							   offset, bufp, nbytes,
							   OCI_FIRST_PIECE, (dvoid *)0,
							   (sb4(*)(dvoid *, dvoid *, ub4 *, ub1 *))0,
							   0, SQLCS_IMPLICIT);

			if (err != OCI_NEED_DATA)
				last = FALSE;

			// Write the next (interim) and last pieces.
			do
			{
				if (remainder > MAXBUFLEN)
				{
					nbytes = MAXBUFLEN; // Still have more pieces to go.
				}
				else
				{
					nbytes = remainder; // Here, (remainder <= MAXBUFLEN).
					last = TRUE;		// This is going to be the final piece.
				}
				memcpy(bufp, filebuffer + (Total - remainder), nbytes);
				// Fill the Buffer with nbytes worth of data.
				if (last)
				{
					// Specifying LAST terminates polling.
					err = _OCILobWrite(m_svchp, stenvStruct->ocierr, stenvStruct->loblocator, &amt,
									   offset, bufp, nbytes,
									   OCI_LAST_PIECE, (dvoid *)0,
									   (sb4(*)(dvoid *, dvoid *, ub4 *, ub1 *))0,
									   0, SQLCS_IMPLICIT);

					if (err != OCI_SUCCESS)
					{
					}
				}
				else
				{
					err = _OCILobWrite(m_svchp, stenvStruct->ocierr, stenvStruct->loblocator, &amt,
									   offset, bufp, nbytes,
									   OCI_NEXT_PIECE, (dvoid *)0,
									   (sb4(*)(dvoid *, dvoid *, ub4 *, ub1 *))0,
									   0, SQLCS_IMPLICIT);

					if (err != OCI_NEED_DATA)
					{
					}
				}
				// Determine how much is left to write.
				remainder = remainder - nbytes;
			} while (!last);
		}
		// At this point, (remainder == 0).
		// Closing the LOB is mandatory if you have opened it.
		_OCILobClose(m_svchp, stenvStruct->ocierr, stenvStruct->loblocator);

		delete[] filebuffer;
		filebuffer = NULL;
	}

	return true;
}
/////////////////////////////////////////////////////////////////////////////////////////
// Read the selected CLOB's data and write it to pathname.
// Returns true if successful, false otherwise.
bool COCIDatabase::GetClobDataIntoBuffer(HANDLE cursor, const char *Select, char **buffer)
{
	if (!m_Connected)
		return false;

	ub4 amt = 0;
	ub4 offset = 0;
	sword RetVal = 0;
	ub1 bufp[1025] = {0};
	ub4 buflen = 0;
	boolean done = FALSE; // Read loop control variable.

	OCILobLocator *clob = NULL;

	if (cursor != NULL)
	{
		envStruct *stenvStruct = (envStruct *)cursor;

		ClearDbVarList(&stenvStruct->VarList);

		if ((Select == NULL) || (buffer == NULL))
			return false;

		// Allocate the Source (bfile) & destination (clob) locators desriptors.
		(void)_OCIDescriptorAlloc((dvoid *)m_ocienv, (dvoid **)&stenvStruct->loblocator,
								  (ub4)OCI_DTYPE_LOB, (size_t)0, (dvoid **)0);

		// Select the CLOB.
		if (SQLGetClob(cursor, Select) == false)
		{
			// Free resources held by the locators.
			(void)_OCIDescriptorFree((dvoid *)stenvStruct->loblocator, (ub4)OCI_DTYPE_LOB);
			return false;
		}

		// Open the CLOB.
		RetVal = _OCILobOpen(m_svchp, stenvStruct->ocierr, stenvStruct->loblocator, OCI_LOB_READONLY);

		if (RetVal != OCI_SUCCESS)
		{
			// Free resources held by the locators.
			(void)_OCIDescriptorFree((dvoid *)stenvStruct->loblocator, (ub4)OCI_DTYPE_LOB);
			return false;
		}

		// Setting the amt to the buffer length. Note here that amt is in bytes
		// since we are using a CLOB.
		buflen = sizeof(bufp);

		// Process the data in pieces.
		offset = 1;
		memset(bufp, 0x00, sizeof(bufp));

		int dataSize = 0;

		while (done != TRUE)
		{
			RetVal = _OCILobRead(m_svchp, stenvStruct->ocierr, stenvStruct->loblocator, &amt, offset, (dvoid *)bufp,
								 buflen, (dvoid *)0,
								 /*(sb4 (*)(dvoid *, dvoid *, ub4, ub1))*/ 0,
								 (ub2)0, (ub1)SQLCS_IMPLICIT);

			switch (RetVal)
			{
			// Only one piece since amt == bufp
			// Process the data in bufp. amt will give the amount of data just read in
			// bufp. This is in bytes for CLOBs and in characters for fixed
			// width CLOBS and in bytes for variable width CLOBs.
			case OCI_SUCCESS:
				if (*buffer == NULL)
				{
					*buffer = (char *)malloc(amt + 1);
					memset(*buffer, 0, amt + 1);
				}
				else
				{
					*buffer = ((char *)realloc(*buffer, dataSize + 1 + amt));
					memset(*buffer + dataSize, 0, amt + 1);
				}
				strncpy(*buffer + dataSize, (char *)bufp, amt);
				dataSize += amt;
				done = TRUE;
				break;

			// An error as occured. Close the file.
			case OCI_ERROR:
				done = TRUE;
				break;

			// More data needs to be read.
			case OCI_NEED_DATA:
				if (*buffer == NULL)
				{
					*buffer = (char *)malloc(amt + 1);
					memset(*buffer, 0, amt + 1);
				}
				else
				{
					*buffer = ((char *)realloc(*buffer, dataSize + 1 + amt));
					memset(*buffer + dataSize, 0, amt + 1);
				}
				strncpy(*buffer + dataSize, (char *)bufp, amt);
				dataSize += amt;
				break;

			// An unexpected error as occured in OCILobRead().
			default:
				done = TRUE;
				break;
			} // End switch.
		}	 // End while loop.

		// Closing the CLOB is mandatory if you have opened it.
		RetVal = _OCILobClose(m_svchp, stenvStruct->ocierr, stenvStruct->loblocator);

		// Free resources held by the locators.
		(void)_OCIDescriptorFree((dvoid *)stenvStruct->loblocator, (ub4)OCI_DTYPE_LOB);
	}

	return true;
}

/////////////////////////////////////////////////////////////////////////////////////////
// Write the selected CLOB's data, taking the data from pathname.
// The record must have been inserted whith the non CLOB fiels
// Before calling this function.
// Returns true if successful, false otherwise.
bool COCIDatabase ::SaveBufferDataIntoClob(HANDLE cursor, const char *Select, const char *buffer)
{
	if (!m_Connected)
		return false;

	ub4 amt = 0;
	ub4 offset = 0;
	unsigned int remainder = 0, nbytes = 0;
	boolean last = FALSE;
	ub1 bufp[MAXBUFLEN] = {0};
	sb4 err = 0;
	sword RetVal = 0;
	char *filebuffer = NULL;

	if (cursor != NULL)
	{
		envStruct *stenvStruct = (envStruct *)cursor;

		ub4 Total = /*2.5**/ MAXBUFLEN; // total amount of data to write to the CLOB in bytes>.

		if ((Select == NULL) || (buffer == NULL))
			return false;

		// Allocate data buffer and read data from the buffer.
		Total = (ub4)strlen(buffer) + 1;
#ifdef _DEBUG
		filebuffer = new char[Total];
#else
		filebuffer = new (std::nothrow) char[Total];
#endif
		strcpy(filebuffer, buffer);

		// Allocate the locators desriptors.
		(void)_OCIDescriptorAlloc((dvoid *)m_ocienv, (dvoid **)&stenvStruct->loblocator,
								  (ub4)OCI_DTYPE_LOB, (size_t)0, (dvoid **)0);

		// Select the CLOB.

		if (SQLGetClob(cursor, Select) == false)
		{
			// Free resources held by the locators.
			(void)_OCIDescriptorFree((dvoid *)stenvStruct->loblocator, (ub4)OCI_DTYPE_LOB);
			return false;
		}

		// Open the CLOB.
		RetVal = _OCILobOpen(m_svchp, stenvStruct->ocierr, stenvStruct->loblocator, OCI_LOB_READWRITE);

		if (RetVal != OCI_SUCCESS)
		{
			// Free resources held by the locators.
			(void)_OCIDescriptorFree((dvoid *)stenvStruct->loblocator, (ub4)OCI_DTYPE_LOB);
			return false;
		}

		if (Total > MAXBUFLEN)
		{
			nbytes = MAXBUFLEN; // We will use streaming via standard polling.
		}
		else
		{
			nbytes = Total; // Only a single write is required.
		}
		memcpy(bufp, filebuffer, nbytes);

		// Fill the buffer with nbytes worth of data.
		remainder = Total - nbytes;

		// Setting Amount to 0 streams the data until use specifies OCI_LAST_PIECE.
		amt = 0;

		offset = 1;

		if (0 == remainder) // Only a single write.
		{
			amt = nbytes;
			memcpy(bufp, filebuffer, Total); // L.B.	2002/11/27.
											 // Put the data into the buffer.
			// Here, (Total <= MAXBUFLEN ) so we can write in one piece.
			_OCILobWrite(m_svchp, stenvStruct->ocierr, stenvStruct->loblocator, &amt,
						 offset, bufp, nbytes,
						 OCI_ONE_PIECE, (dvoid *)0,
						 (sb4(*)(dvoid *, dvoid *, ub4 *, ub1 *))0,
						 0, SQLCS_IMPLICIT);
		}
		else
		{
			// Here (Total > MAXBUFLEN ) so we use streaming via standard polling
			// write the first piece. Specifying first initiates polling.
			memcpy(bufp, filebuffer, nbytes);
			err = _OCILobWrite(m_svchp, stenvStruct->ocierr, stenvStruct->loblocator, &amt,
							   offset, bufp, nbytes,
							   OCI_FIRST_PIECE, (dvoid *)0,
							   (sb4(*)(dvoid *, dvoid *, ub4 *, ub1 *))0,
							   0, SQLCS_IMPLICIT);

			if (err != OCI_NEED_DATA)
				last = FALSE;

			// Write the next (interim) and last pieces.
			do
			{
				if (remainder > MAXBUFLEN)
				{
					nbytes = MAXBUFLEN; // Still have more pieces to go.
				}
				else
				{
					nbytes = remainder; // Here, (remainder <= MAXBUFLEN).
					last = TRUE;		// This is going to be the final piece.
				}
				memcpy(bufp, filebuffer + (Total - remainder), nbytes);
				// Fill the Buffer with nbytes worth of data.
				if (last)
				{
					// Specifying LAST terminates polling.
					err = _OCILobWrite(m_svchp, stenvStruct->ocierr, stenvStruct->loblocator, &amt,
									   offset, bufp, nbytes,
									   OCI_LAST_PIECE, (dvoid *)0,
									   (sb4(*)(dvoid *, dvoid *, ub4 *, ub1 *))0,
									   0, SQLCS_IMPLICIT);

					if (err != OCI_SUCCESS)
					{
					}
				}
				else
				{
					err = _OCILobWrite(m_svchp, stenvStruct->ocierr, stenvStruct->loblocator, &amt,
									   offset, bufp, nbytes,
									   OCI_NEXT_PIECE, (dvoid *)0,
									   (sb4(*)(dvoid *, dvoid *, ub4 *, ub1 *))0,
									   0, SQLCS_IMPLICIT);

					if (err != OCI_NEED_DATA)
					{
					}
				}
				// Determine how much is left to write.
				remainder = remainder - nbytes;
			} while (!last);
		}
		// At this point, (remainder == 0).
		// Closing the LOB is mandatory if you have opened it.
		_OCILobClose(m_svchp, stenvStruct->ocierr, stenvStruct->loblocator);

		delete[] filebuffer;
		filebuffer = NULL;
	}

	return true;
}

/////////////////////////////////////////////////////////////////////////////////////////
//	Getes the file size. Used for blod reading.
long COCIDatabase ::GetFileSize(FILE *fd) const
{
	long size = 0;

	fseek(fd, 0L, SEEK_END);
	size = ftell(fd);
	fseek(fd, 0L, SEEK_SET);

	return size;
}

/////////////////////////////////////////////////////////////////////////////////////////
//
bool COCIDatabase::setOutputString(HANDLE cursor, int elemID, char *colValue, int maxValueSize)
{
	if (cursor != NULL)
	{
		sb2 indicator = 0;
		sword retcode = 0;
		envStruct *stenvStruct = (envStruct *)cursor;
		retcode = _OCIDefineByPos(stenvStruct->stmthp,
								  &stenvStruct->defnp,
								  stenvStruct->ocierr,
								  elemID,
								  (dvoid *)colValue,
								  maxValueSize,
								  SQLT_STR,
								  &indicator, (ub2 *)0, (ub2 *)0, OCI_DEFAULT);

		if (retcode != OCI_SUCCESS)
		{
			SetLastOciError(cursor, retcode);
			return false;
		}
		return true;
	}
	return false;
}

/////////////////////////////////////////////////////////////////////////////////////////
//
bool COCIDatabase::setOutputInt(HANDLE cursor, int elemID, int *colValue)
{
	if (cursor != NULL)
	{
		sword retcode = 0;
		sb2 indicator = 0;
		envStruct *stenvStruct = (envStruct *)cursor;
		retcode = _OCIDefineByPos(stenvStruct->stmthp,
								  &stenvStruct->defnp,
								  stenvStruct->ocierr,
								  elemID,
								  (dvoid *)colValue,
								  sizeof(int),
								  SQLT_INT,
								  /*indicator, reason for null error.*/ (dvoid *)&indicator, (ub2 *)0, (ub2 *)0, OCI_DEFAULT);

		if (retcode != OCI_SUCCESS)
		{
			SetLastOciError(cursor, retcode);
			return false;
		}

		return true;
	}
	return false;
}

/////////////////////////////////////////////////////////////////////////////////////////
//
bool COCIDatabase::setOutputLong(HANDLE cursor, int elemID, long *colValue)
{
	if (cursor != NULL)
	{
		sword retcode = 0;
		sb2 indicator = 0;
		envStruct *stenvStruct = (envStruct *)cursor;
		retcode = _OCIDefineByPos(stenvStruct->stmthp,
								  &stenvStruct->defnp,
								  stenvStruct->ocierr,
								  elemID,
								  (dvoid *)colValue,
								  sizeof(int),
								  SQLT_LNG,
								  /*indicator, reason for null error.*/ (dvoid *)&indicator, (ub2 *)0, (ub2 *)0, OCI_DEFAULT);
		if (retcode != OCI_SUCCESS)
		{
			SetLastOciError(cursor, retcode);
			return false;
		}
		return true;
	}
	return false;
}

/////////////////////////////////////////////////////////////////////////////////////////
//
bool COCIDatabase::setOutputDate(HANDLE cursor, int elemID, OCIDate *dt)
{
	if (cursor != NULL)
	{
		sword retcode = 0;
		sb2 indicator = 0;
		envStruct *stenvStruct = (envStruct *)cursor;
		retcode = _OCIDefineByPos(stenvStruct->stmthp,
								  &stenvStruct->defnp,
								  stenvStruct->ocierr,
								  elemID,
								  (void *)dt,
								  sizeof(OCIDate),
								  SQLT_ODT,
								  /*indicator, reason for null error.*/ (dvoid *)&indicator, (ub2 *)0, (ub2 *)0, OCI_DEFAULT);
		if (retcode != OCI_SUCCESS)
		{
			SetLastOciError(cursor, retcode);
			return false;
		}
		return true;
	}
	return false;
}

/////////////////////////////////////////////////////////////////////////////////////////
//
bool COCIDatabase::setInputString(HANDLE cursor, int elemID, char *str)
{
	if (cursor != NULL)
	{
		sword retcode = 0;
		envStruct *stenvStruct = (envStruct *)cursor;
		retcode = _OCIBindByPos(stenvStruct->stmthp, &stenvStruct->bndp, stenvStruct->ocierr, elemID, str, (sb4)strlen(str), SQLT_CHR, NULL, NULL, NULL, 0, NULL, OCI_DEFAULT);
		if (retcode != OCI_SUCCESS)
		{
			SetLastOciError(cursor, retcode);
			return false;
		}
		return true;
	}
	return false;
}

/////////////////////////////////////////////////////////////////////////////////////////
//
bool COCIDatabase::setInputInt(HANDLE cursor, int elemID, int *num)
{
	if (cursor != NULL)
	{
		sword retcode = 0;
		envStruct *stenvStruct = (envStruct *)cursor;

		retcode = _OCIBindByPos(stenvStruct->stmthp, &stenvStruct->bndp, stenvStruct->ocierr, elemID, num, sizeof(num), SQLT_INT, NULL, NULL, NULL, 0, NULL, OCI_DEFAULT);
		if (retcode != OCI_SUCCESS)
		{
			SetLastOciError(cursor, retcode);
			return false;
		}
		return true;
	}
	return false;
}

/////////////////////////////////////////////////////////////////////////////////////////
//
bool COCIDatabase::setInputLong(HANDLE cursor, int elemID, long *num)
{
	if (cursor != NULL)
	{
		sword retcode = 0;
		envStruct *stenvStruct = (envStruct *)cursor;
		retcode = _OCIBindByPos(stenvStruct->stmthp, &stenvStruct->bndp, stenvStruct->ocierr, elemID, num, sizeof(num), SQLT_LNG, NULL, NULL, NULL, 0, NULL, OCI_DEFAULT);
		if (retcode != OCI_SUCCESS)
		{
			SetLastOciError(cursor, retcode);
			return false;
		}
		return true;
	}
	return false;
}

/////////////////////////////////////////////////////////////////////////////////////////
//
bool COCIDatabase::setInputDate(HANDLE cursor, int elemID, OCIDate *date)
{
	if (cursor != NULL)
	{
		sword retcode = 0;
		envStruct *stenvStruct = (envStruct *)cursor;
		retcode = _OCIBindByPos(stenvStruct->stmthp, &stenvStruct->bndp, stenvStruct->ocierr, elemID, (void *)date, sizeof(OCIDate), SQLT_ODT, NULL, NULL, NULL, 0, NULL, OCI_DEFAULT);
		if (retcode != OCI_SUCCESS)
		{
			SetLastOciError(cursor, retcode);
			return false;
		}
		return true;
	}
	return false;
}

/////////////////////////////////////////////////////////////////////////////////////////
//
bool COCIDatabase::setInputDate(HANDLE cursor, int elemID, int year, int month, int day, int hour, int min, int sec)
{
	OCIDate oDate;

	if (cursor != NULL)
	{
		sword retcode = 0;
		envStruct *stenvStruct = (envStruct *)cursor;

		oDate.OCIDateYYYY = year;
		oDate.OCIDateMM = month;
		oDate.OCIDateDD = day;
		oDate.OCIDateTime.OCITimeHH = hour;
		oDate.OCIDateTime.OCITimeMI = min;
		oDate.OCIDateTime.OCITimeSS = sec;

		retcode = _OCIBindByPos(stenvStruct->stmthp, &stenvStruct->bndp, stenvStruct->ocierr, elemID, (void *)&oDate, sizeof(oDate), SQLT_ODT, NULL, NULL, NULL, 0, NULL, OCI_DEFAULT);
		if (retcode != OCI_SUCCESS)
		{
			SetLastOciError(cursor, retcode);
			return false;
		}
		return true;
	}
	return false;
}

/////////////////////////////////////////////////////////////////////////////////////////
//
bool COCIDatabase::setInputDate(HANDLE cursor, int elemID, char *date, char *format)
{
	OCIDate *oDate = new OCIDate;

	oDate->OCIDateYYYY = 0;
	oDate->OCIDateMM = 0;
	oDate->OCIDateDD = 0;
	oDate->OCIDateTime.OCITimeHH = 0;
	oDate->OCIDateTime.OCITimeMI = 0;
	oDate->OCIDateTime.OCITimeSS = 0;

	if (cursor != NULL)
	{
		sword retcode = 0;
		envStruct *stenvStruct = (envStruct *)cursor;

		sword retval = _OCIDateFromText(stenvStruct->ocierr, (text *)date, (ub4)strlen(date), (text *)format, (ub1)strlen(format), 0, 0, oDate);
		stenvStruct->dtCollection.push_back(oDate);
		//int size = stenvStruct->dtCollection.size();

		retcode = _OCIBindByPos(stenvStruct->stmthp, &stenvStruct->bndp, stenvStruct->ocierr, elemID, (void *)oDate, sizeof(OCIDate), SQLT_ODT, NULL, NULL, NULL, 0, NULL, OCI_DEFAULT);
		if (retcode != OCI_SUCCESS)
		{
			SetLastOciError(cursor, retcode);
			return false;
		}
		return true;
	}
	return false;
}

/////////////////////////////////////////////////////////////////////////////////////////
//
bool COCIDatabase::ConvertDateToText(OCIDate *ocidate, char *date, char *format)
{
	if (m_Cursor != NULL)
	{
		sword retcode = 0;
		unsigned int buffsize = 65;
		envStruct *stenvStruct = (envStruct *)m_Cursor;

		retcode = _OCIDateToText(stenvStruct->ocierr, ocidate, (text *)format, (ub1)strlen(format), 0, 0, &buffsize, (text *)date);
		if (retcode != OCI_SUCCESS)
		{
			SetLastOciError(m_Cursor, retcode);
			return false;
		}

		return true;
	}
	return false;
}

/////////////////////////////////////////////////////////////////////////////////////////
//
bool COCIDatabase::ConvertTextToDate(char *date, OCIDate *ocidate, char *format)
{
	if (m_Cursor != NULL)
	{
		sword retcode = 0;
		envStruct *stenvStruct = (envStruct *)m_Cursor;

		retcode = _OCIDateFromText(stenvStruct->ocierr, (text *)date, (ub1)strlen(date), (text *)format, (ub4)strlen(format), 0, 0, ocidate);
		if (retcode != OCI_SUCCESS)
		{
			SetLastOciError(m_Cursor, retcode);
			return false;
		}

		return true;
	}
	return false;
}

/////////////////////////////////////////////////////////////////////////////////////////
//
bool COCIDatabase::FirstRecord(HANDLE cursor)
{
	if (cursor != NULL)
	{
		sword retcode = 0;
		envStruct *stenvStruct = (envStruct *)cursor;

		if (!stenvStruct->stmthp)
			return false;

		/*****************************************/
		/* On memset les variables au FirstRecord */
		dbVar *v;
		std::list<dbVar *>::iterator it;
		for (it = stenvStruct->VarList.begin(); it != stenvStruct->VarList.end(); ++it)
		{
			v = (*it);
			if (v)
			{
				v->var.s[0] = 0x00;
			}
		}

		/*****************************************/

		retcode = _OCIStmtFetch2(stenvStruct->stmthp, stenvStruct->ocierr, 1, OCI_FETCH_FIRST, 0, OCI_DEFAULT);

		if ((retcode != OCI_SUCCESS) &&
			(retcode != OCI_SUCCESS_WITH_INFO))
		{
			if (retcode != OCI_NO_DATA)
				SetLastOciError(cursor, retcode);
			return false;
		}
	}
	else
		return false;
	return true;
}

/////////////////////////////////////////////////////////////////////////////////////////
//
bool COCIDatabase::NextRecord(HANDLE cursor)
{
	if (cursor != NULL)
	{
		sword retcode = 0;
		envStruct *stenvStruct = (envStruct *)cursor;

		if (!stenvStruct->stmthp)
			return false;

		/*****************************************/
		/* On memset les variables au nextrecord */
		dbVar *v;
		std::list<dbVar *>::iterator it;
		for (it = stenvStruct->VarList.begin(); it != stenvStruct->VarList.end(); ++it)
		{
			v = (*it);
			if (v)
			{
				v->var.s[0] = 0x00;
			}
		}
		/*****************************************/

		retcode = _OCIStmtFetch2(stenvStruct->stmthp, stenvStruct->ocierr, 1, OCI_FETCH_NEXT, 0, OCI_DEFAULT);

		if ((retcode != OCI_SUCCESS) &&
			(retcode != OCI_SUCCESS_WITH_INFO))
		{
			if (retcode != OCI_NO_DATA)
			{
				SetLastOciError(cursor, retcode);
			}
			return false;
		}
	}
	else
		return false;
	return true;
}

/////////////////////////////////////////////////////////////////////////////////////////
//
bool COCIDatabase::PrevRecord(HANDLE cursor)
{
	envStruct *stenvStruct = (envStruct *)cursor;
	if (cursor != NULL)
	{
		sword retcode = 0;
		if (!stenvStruct->stmthp)
			return false;

		/*****************************************/
		/* On memset les variables au PrevRecord */
		dbVar *v;
		std::list<dbVar *>::iterator it;
		for (it = stenvStruct->VarList.begin(); it != stenvStruct->VarList.end(); ++it)
		{
			v = (*it);
			if (v)
			{
				v->var.s[0] = 0x00;
			}
		}

		/*****************************************/

		retcode = _OCIStmtFetch2(stenvStruct->stmthp, stenvStruct->ocierr, 1, OCI_FETCH_PRIOR, 0, OCI_DEFAULT);
		if ((retcode != OCI_SUCCESS) &&
			(retcode != OCI_SUCCESS_WITH_INFO))
		{
			if (retcode != OCI_NO_DATA)
				SetLastOciError(cursor, retcode);
			return false;
		}
	}
	else
		return false;
	return true;
}

/////////////////////////////////////////////////////////////////////////////////////////
//
bool COCIDatabase::LastRecord(HANDLE cursor)
{
	if (cursor != NULL)
	{
		sword retcode = 0;
		envStruct *stenvStruct = (envStruct *)cursor;
		if (!stenvStruct->stmthp)
			return false;

		/*****************************************/
		/* On memset les variables au LastRecord */
		dbVar *v;
		std::list<dbVar *>::iterator it;
		for (it = stenvStruct->VarList.begin(); it != stenvStruct->VarList.end(); ++it)
		{
			v = (*it);
			if (v)
			{
				v->var.s[0] = 0x00;
			}
		}

		/*****************************************/

		retcode = _OCIStmtFetch2(stenvStruct->stmthp, stenvStruct->ocierr, 1, OCI_FETCH_LAST, 0, OCI_DEFAULT);
		if ((retcode != OCI_SUCCESS) &&
			(retcode != OCI_SUCCESS_WITH_INFO))
		{
			if (retcode != OCI_NO_DATA)
				SetLastOciError(cursor, retcode);
			return false;
		}
	}
	else
		return false;
	return true;
}

/////////////////////////////////////////////////////////////////////////////////////////
//
bool COCIDatabase::FetchRecord(HANDLE cursor, int rowNum)
{
	if (cursor != NULL)
	{
		sword retcode = 0;
		envStruct *stenvStruct = (envStruct *)cursor;
		if (!stenvStruct->stmthp)
			return false;

		/*****************************************/
		/* On memset les variables au LastRecord */
		dbVar *v;
		std::list<dbVar *>::iterator it;
		for (it = stenvStruct->VarList.begin(); it != stenvStruct->VarList.end(); ++it)
		{
			v = (*it);
			if (v)
			{
				v->var.s[0] = 0x00;
			}
		}

		/*****************************************/

		retcode = _OCIStmtFetch2(stenvStruct->stmthp, stenvStruct->ocierr, 1, OCI_FETCH_ABSOLUTE, rowNum, OCI_DEFAULT);
		if ((retcode != OCI_SUCCESS) &&
			(retcode != OCI_SUCCESS_WITH_INFO))
		{
			if (retcode != OCI_NO_DATA)
				SetLastOciError(cursor, retcode);
			return false;
		}
	}
	else
		return false;
	return true;
}

/////////////////////////////////////////////////////////////////////////////////////////
//
int COCIDatabase::GetCurrentPosition(HANDLE cursor)
{
	sword retval;
	int row_position = 0;

	if (cursor == NULL)
		return 0;

	envStruct *stenvStruct = (envStruct *)cursor;

	retval = _OCIAttrGet(stenvStruct->stmthp, OCI_HTYPE_STMT, (dvoid *)&row_position, (ub4 *)0, OCI_ATTR_CURRENT_POSITION, stenvStruct->ocierr);
	if (retval != OCI_SUCCESS)
	{
		SetLastOciError(cursor, retval);
	}

	return row_position;
}

/////////////////////////////////////////////////////////////////////////////////////////
//
int COCIDatabase::GetRecordCount(HANDLE cursor)
{
	if (cursor == NULL)
		return 0;

	envStruct *stenvStruct = (envStruct *)cursor;

	if (stenvStruct->RecordCount == 0)
	{
		LastRecord(cursor);
		_OCIAttrGet(stenvStruct->stmthp, OCI_HTYPE_STMT, (dvoid *)&stenvStruct->RecordCount, (ub4 *)0, OCI_ATTR_ROW_COUNT, stenvStruct->ocierr);
		FirstRecord(cursor);
	}

	return stenvStruct->RecordCount;
}

/////////////////////////////////////////////////////////////////////////////////////////
//
bool COCIDatabase::Commit(HANDLE cursor)
{
	if (cursor != NULL)
	{
		sword retcode = 0;
		envStruct *stenvStruct = (envStruct *)cursor;

		NewStatement(cursor);
		retcode = _OCIStmtPrepare2(m_svchp, &stenvStruct->stmthp, stenvStruct->ocierr, (unsigned char *)"COMMIT", (ub4)strlen("COMMIT"), NULL, 0, (ub4)OCI_NTV_SYNTAX, (ub4)OCI_DEFAULT);

		if ((retcode == OCI_SUCCESS) || (retcode == OCI_SUCCESS_WITH_INFO))
		{
			if (stenvStruct->scrollable)
				retcode = _OCIStmtExecute(m_svchp,
										  stenvStruct->stmthp,
										  stenvStruct->ocierr,
										  (ub4)1, (ub4)0,
										  (CONST OCISnapshot *)NULL,
										  (OCISnapshot *)NULL,
										  OCI_STMT_SCROLLABLE_READONLY);
			else
				retcode = _OCIStmtExecute(m_svchp,
										  stenvStruct->stmthp,
										  stenvStruct->ocierr,
										  (ub4)1,
										  (ub4)0,
										  (CONST OCISnapshot *)NULL,
										  (OCISnapshot *)NULL,
										  OCI_DEFAULT);

			if ((retcode == OCI_SUCCESS) || (retcode == OCI_SUCCESS_WITH_INFO))
			{
				return true;
			}
		}
		SetLastOciError(cursor, retcode);
	}
	return false;
}

/////////////////////////////////////////////////////////////////////////////////////////
//
bool COCIDatabase::Rollback(HANDLE cursor)
{
	if (cursor != NULL)
	{
		sword retcode = 0;
		envStruct *stenvStruct = (envStruct *)cursor;

		NewStatement(cursor);
		retcode = _OCIStmtPrepare2(m_svchp, &stenvStruct->stmthp, stenvStruct->ocierr, (unsigned char *)"ROLLBACK", (ub4)strlen("ROLLBACK"), NULL, 0, (ub4)OCI_NTV_SYNTAX, (ub4)OCI_DEFAULT);

		if ((retcode == OCI_SUCCESS) || (retcode == OCI_SUCCESS_WITH_INFO))
		{
			if (stenvStruct->scrollable)
				retcode = _OCIStmtExecute(m_svchp,
										  stenvStruct->stmthp,
										  stenvStruct->ocierr,
										  (ub4)1,
										  (ub4)0,
										  (CONST OCISnapshot *)NULL,
										  (OCISnapshot *)NULL,
										  OCI_STMT_SCROLLABLE_READONLY);
			else
				retcode = _OCIStmtExecute(m_svchp,
										  stenvStruct->stmthp,
										  stenvStruct->ocierr,
										  (ub4)1,
										  (ub4)0,
										  (CONST OCISnapshot *)NULL,
										  (OCISnapshot *)NULL,
										  OCI_DEFAULT);

			if ((retcode == OCI_SUCCESS) || (retcode == OCI_SUCCESS_WITH_INFO))
			{
				return true;
			}
		}
		SetLastOciError(cursor, retcode);
	}
	return false;
}

/////////////////////////////////////////////////////////////////////////////////////////
//
int COCIDatabase::SetLastOciError(HANDLE cursor, sword status)
{
	envStruct *stenvStruct = (envStruct *)cursor;
	stenvStruct->ErrCode = 0;

	switch (status)
	{
	case OCI_SUCCESS:
		break;
	case OCI_SUCCESS_WITH_INFO:
		strcpy((char *)stenvStruct->ErrMsg, "Error - OCI_SUCCESS_WITH_INFO\n");
		break;
	case OCI_NEED_DATA:
		strcpy((char *)stenvStruct->ErrMsg, "Error - OCI_NEED_DATA\n");
		break;
	case OCI_NO_DATA:
		strcpy((char *)stenvStruct->ErrMsg, "Error - OCI_NODATA\n");
		break;
	case OCI_ERROR:
		(void)_OCIErrorGet((dvoid *)stenvStruct->ocierr, (ub4)1, (text *)NULL, (sb4 *)&stenvStruct->ErrCode /* &errcode*/,
						   (unsigned char *)stenvStruct->ErrMsg, (ub4) /*sizeof(errbuf)*/ 512, OCI_HTYPE_ERROR);
		break;
	case OCI_INVALID_HANDLE:
		strcpy((char *)stenvStruct->ErrMsg, "Error - OCI_INVALID_HANDLE\n");
		break;
	case OCI_STILL_EXECUTING:
		strcpy((char *)stenvStruct->ErrMsg, "Error - OCI_STILL_EXECUTE\n");
		break;
	case OCI_CONTINUE:
		strcpy((char *)stenvStruct->ErrMsg, "Error - OCI_CONTINUE\n");
		break;

	default:
		break;
	}

	// Erreur de d�connection
	if (stenvStruct->ErrCode == 1012 || stenvStruct->ErrCode == 3113 || stenvStruct->ErrCode == 3114 || stenvStruct->ErrCode == 12571 || stenvStruct->ErrCode == 3135 || stenvStruct->ErrCode == 12547) // [Ref:PSSCRIPT-921]
	{
#ifdef _WINDOWS
		UnloadClient();
#endif
		m_Connected = false;
	}
	return stenvStruct->ErrCode;
}

/////////////////////////////////////////////////////////////////////////////////////////
//
void COCIDatabase::SetLastOciConnectionError(OCIError *pOciErr)
{
	int iCode = 0; // Dummy.

	_OCIErrorGet((dvoid *)pOciErr,
				 (ub4)1,
				 (text *)NULL,
				 (sb4 *)&iCode,
				 (text *)m_sLastError,
				 (ub4)sizeof(m_sLastError),
				 OCI_HTYPE_ERROR);
}

/////////////////////////////////////////////////////////////////////////////////////////
//
char *COCIDatabase::GetLastOciErrorText(HANDLE cursor)
{
	if (cursor == NULL)
		return msEmptyString;

	envStruct *stenvStruct = (envStruct *)cursor;

	return stenvStruct->ErrMsg;
}

/////////////////////////////////////////////////////////////////////////////////////////
//
int COCIDatabase::GetLastOciErrorCode(HANDLE cursor)
{
	if (cursor == NULL)
		return 0;

	envStruct *stenvStruct = (envStruct *)cursor;

	return stenvStruct->ErrCode;
}

/////////////////////////////////////////////////////////////////////////////////////////
//	OCI dll client methods.
/////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////
//
bool COCIDatabase::LoadClient(const char *pClient)
{
	bool bRetVal = false;

#ifdef WINDOWS
	mhDllHandle = LoadLibraryA(pClient);
#else
	mhDllHandle = dlopen(pClient, RTLD_NOW);
#endif

	if (mhDllHandle)
		bRetVal = LinkProcs();

	return bRetVal;
}

/////////////////////////////////////////////////////////////////////////////////////////
//
void COCIDatabase::UnloadClient()
{
	if (mhDllHandle)
	{
#ifdef WINDOWS
		FreeLibrary(mhDllHandle);
#else
		dlclose(mhDllHandle);
#endif
		mhDllHandle = NULL;

		ResetPointers();
	}
}

#ifdef WINDOWS
#define GetProcAddressDef GetProcAddress
#else
#define GetProcAddressDef dlsym
#endif
/////////////////////////////////////////////////////////////////////////////////////////
//
bool COCIDatabase::LinkProcs()
{
	mpfOCIEnvCreate = (PTR_OCIEnvCreate)GetProcAddressDef(mhDllHandle, "OCIEnvCreate");
	if (!mpfOCIEnvCreate)
	{
		UnloadClient();
		return false;
	}

	mpfOCIHandleAlloc = (PTR_OCIHandleAlloc)GetProcAddressDef(mhDllHandle, "OCIHandleAlloc");
	if (!mpfOCIHandleAlloc)
	{
		UnloadClient();
		return false;
	}

	mpfOCIHandleFree = (PTR_OCIHandleFree)GetProcAddressDef(mhDllHandle, "OCIHandleFree");
	if (!mpfOCIHandleFree)
	{
		UnloadClient();
		return false;
	}

	mpfOCIServerAttach = (PTR_OCIServerAttach)GetProcAddressDef(mhDllHandle, "OCIServerAttach");
	if (!mpfOCIServerAttach)
	{
		UnloadClient();
		return false;
	}

	mpfOCIServerDetach = (PTR_OCIServerDetach)GetProcAddressDef(mhDllHandle, "OCIServerDetach");
	if (!mpfOCIServerDetach)
	{
		UnloadClient();
		return false;
	}

	mpfOCIAttrSet = (PTR_OCIAttrSet)GetProcAddressDef(mhDllHandle, "OCIAttrSet");
	if (!mpfOCIAttrSet)
	{
		UnloadClient();
		return false;
	}

	mpfOCISessionBegin = (PTR_OCISessionBegin)GetProcAddressDef(mhDllHandle, "OCISessionBegin");
	if (!mpfOCISessionBegin)
	{
		UnloadClient();
		return false;
	}

	mpfOCISessionEnd = (PTR_OCISessionEnd)GetProcAddressDef(mhDllHandle, "OCISessionEnd");
	if (!mpfOCISessionEnd)
	{
		UnloadClient();
		return false;
	}

	mpfOCIStmtRelease = (PTR_OCIStmtRelease)GetProcAddressDef(mhDllHandle, "OCIStmtRelease");
	if (!mpfOCIStmtRelease)
	{
		UnloadClient();
		return false;
	}

	mpfOCIErrorGet = (PTR_OCIErrorGet)GetProcAddressDef(mhDllHandle, "OCIErrorGet");
	if (!mpfOCIErrorGet)
	{
		UnloadClient();
		return false;
	}

	mpfOCIStmtPrepare2 = (PTR_OCIStmtPrepare2)GetProcAddressDef(mhDllHandle, "OCIStmtPrepare2");
	if (!mpfOCIStmtPrepare2)
	{
		UnloadClient();
		return false;
	}

	mpfOCIStmtExecute = (PTR_OCIStmtExecute)GetProcAddressDef(mhDllHandle, "OCIStmtExecute");
	if (!mpfOCIStmtExecute)
	{
		UnloadClient();
		return false;
	}

	mpfOCIDefineByPos = (PTR_OCIDefineByPos)GetProcAddressDef(mhDllHandle, "OCIDefineByPos");
	if (!mpfOCIDefineByPos)
	{
		UnloadClient();
		return false;
	}

	mpfOCIDescriptorAlloc = (PTR_OCIDescriptorAlloc)GetProcAddressDef(mhDllHandle, "OCIDescriptorAlloc");
	if (!mpfOCIDescriptorAlloc)
	{
		UnloadClient();
		return false;
	}

	mpfOCIDescriptorFree = (PTR_OCIDescriptorFree)GetProcAddressDef(mhDllHandle, "OCIDescriptorFree");
	if (!mpfOCIDescriptorFree)
	{
		UnloadClient();
		return false;
	}

	mpfOCILobOpen = (PTR_OCILobOpen)GetProcAddressDef(mhDllHandle, "OCILobOpen");
	if (!mpfOCILobOpen)
	{
		UnloadClient();
		return false;
	}

	mpfOCILobClose = (PTR_OCILobClose)GetProcAddressDef(mhDllHandle, "OCILobClose");
	if (!mpfOCILobClose)
	{
		UnloadClient();
		return false;
	}

	mpfOCILobRead = (PTR_OCILobRead)GetProcAddressDef(mhDllHandle, "OCILobRead");
	if (!mpfOCILobRead)
	{
		UnloadClient();
		return false;
	}

	mpfOCILobWrite = (PTR_OCILobWrite)GetProcAddressDef(mhDllHandle, "OCILobWrite");
	if (!mpfOCILobWrite)
	{
		UnloadClient();
		return false;
	}

	mpfOCIBindByPos = (PTR_OCIBindByPos)GetProcAddressDef(mhDllHandle, "OCIBindByPos");
	if (!mpfOCIBindByPos)
	{
		UnloadClient();
		return false;
	}

	mpfOCIDateFromText = (PTR_OCIDateFromText)GetProcAddressDef(mhDllHandle, "OCIDateFromText");
	if (!mpfOCIDateFromText)
	{
		UnloadClient();
		return false;
	}

	mpfOCIDateToText = (PTR_OCIDateToText)GetProcAddressDef(mhDllHandle, "OCIDateToText");
	if (!mpfOCIDateToText)
	{
		UnloadClient();
		return false;
	}

	mpfOCIStmtFetch2 = (PTR_OCIStmtFetch2)GetProcAddressDef(mhDllHandle, "OCIStmtFetch2");
	if (!mpfOCIStmtFetch2)
	{
		UnloadClient();
		return false;
	}

	mpfOCIAttrGet = (PTR_OCIAttrGet)GetProcAddressDef(mhDllHandle, "OCIAttrGet");
	if (!mpfOCIAttrGet)
	{
		UnloadClient();
		return false;
	}
	return true;
}

/////////////////////////////////////////////////////////////////////////////////////////
//
void COCIDatabase::ResetPointers()
{
	mpfOCIEnvCreate = NULL;
	mpfOCIHandleAlloc = NULL;
	mpfOCIHandleFree = NULL;
	mpfOCIServerAttach = NULL;
	mpfOCIServerDetach = NULL;
	mpfOCIAttrSet = NULL;
	mpfOCISessionBegin = NULL;
	mpfOCISessionEnd = NULL;
	mpfOCIStmtRelease = NULL;
	mpfOCIErrorGet = NULL;
	mpfOCIStmtPrepare2 = NULL;
	mpfOCIStmtExecute = NULL;
	mpfOCIDefineByPos = NULL;
	mpfOCIDescriptorAlloc = NULL;
	mpfOCIDescriptorFree = NULL;
	mpfOCILobOpen = NULL;
	mpfOCILobClose = NULL;
	mpfOCILobRead = NULL;
	mpfOCILobWrite = NULL;
	mpfOCIBindByPos = NULL;
	mpfOCIDateFromText = NULL;
	mpfOCIDateToText = NULL;
	mpfOCIStmtFetch2 = NULL;
	mpfOCIAttrGet = NULL;
}

/////////////////////////////////////////////////////////////////////////////////////////
//
void COCIDatabase::ClearOCIDateList(std::list<OCIDate *> *pList)
{
	OCIDate *pCur = NULL;
	std::list<OCIDate *>::iterator it;

	while (pList->size())
	{
		it = pList->begin();
		pCur = (*it);

		delete pCur;

		pList->erase(it);
	}
	pCur = NULL;
}

/////////////////////////////////////////////////////////////////////////////////////////
//
void COCIDatabase::ClearDbVarList(std::list<dbVar *> *pList)
{
	dbVar *pCur = NULL;
	std::list<dbVar *>::iterator it;

	while (pList->size())
	{
		it = pList->begin();
		pCur = (*it);

		delete pCur;

		pList->erase(it);
	}
}

} // namespace tool
