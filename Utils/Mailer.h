// Mailer.h: interface for the Mailer class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MAILER_H__1749E0E5_B780_4CDE_A2C8_8D2CD0C3A07A__INCLUDED_)
#define AFX_MAILER_H__1749E0E5_B780_4CDE_A2C8_8D2CD0C3A07A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "SMTPClient.h"
#include "StringBuffer.h"

class Mailer  
{
public:
	Mailer();
	virtual ~Mailer();

	void SetMailFrom(const char *mailAdress);
	void SetMailTo(const char *mailAdress);
	void SetMailServer(const char *serverIP);
	void SetMailPort(unsigned int mailPort);
	void SetSubject(const char *subject);
	bool SendMail(char *subject=NULL, char *message=NULL);
	void AddLine(char *line,...);
	void ClearMail();
	char *GetLastError();


private:
	char m_mailFrom[128];
	char m_mailTo[128];
	char m_serverIP[65];
	unsigned int m_serverPort;
	SMTPClient mailClient;

	StringBuffer m_Body;
	char m_Subject[128];
};

#endif // !defined(AFX_MAILER_H__1749E0E5_B780_4CDE_A2C8_8D2CD0C3A07A__INCLUDED_)
