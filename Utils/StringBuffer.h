/////////////////////////////////////////////////////////////////////////////
//	StringBuffer.h
/////////////////////////////////////////////////////////////////////////////

#if !defined(__STRING_BUFFER_H__)
#define      __STRING_BUFFER_H__

#define	STRINGBUFFER_VERSION	"1.1.0.0"

/////////////////////////////////////////////////////////////////////////////
//
class StringBuffer
{
public:
	StringBuffer();
	StringBuffer(const char *pStr);
	StringBuffer(const StringBuffer& sStr);

	virtual ~StringBuffer();

	char *GetVersion() const                               {return STRINGBUFFER_VERSION;}

	int GetBufferLength() const                            {return m_iBufferLength;}
	char *GetBuffer()                                      {return m_pBuffer;}

	void SetBuffer(const char *pBuffer);
	void Clear();

	int Format(const char *pFormat, ...);

	int ReplaceChar(const int cToReplace, const int cToReplaceBy);
	int ReplaceChar(char *pStr, const int cToReplace, const int cToReplaceBy);

	static void RemoveCharacter(char *pString, const char cChar);
	void RemoveCharacter(const char cChar);

	/////////////////////////////////////////////////////////////////////////
	//	Operators.
	StringBuffer &operator=(const char *pChunk);
	StringBuffer &operator=(const StringBuffer &Str);
	StringBuffer &operator+=(const char *pChunk);
	StringBuffer &operator+=(const StringBuffer &Str);
	operator char *()                                      {return m_pBuffer;}

private:

	int   m_iBufferLength;
	char *m_pBuffer;
};

#endif	//	__STRING_BUFFER_H__
