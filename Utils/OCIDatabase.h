/////////////////////////////////////////////////////////////////////////////////////
//	File		:	OCIatabase.h
//	Description	:	Wrapper around oracle oci client library.
/////////////////////////////////////////////////////////////////////////////////////

#if !defined(__OCIDATABASE_H__)
#define __OCIDATABASE_H__

/////////////////////////////////////////////////////////////////////////////////////
//
//#include <Windows.h>
#define HANDLE void *

/////////////////////////////////////////////////////////////////////////////////////
//
#define OCI_VERSION "2.3.0.0"

/////////////////////////////////////////////////////////////////////////////////////
//
#include "oratypes.h"
#include "oci.h"
#include "string.h"
#include <list>
#include <vector>
#include <string>

/////////////////////////////////////////////////////////////////////////////////////
//	Function pointers definitions.
typedef sword (*PTR_OCIEnvCreate)(OCIEnv **envp,
								  ub4 mode,
								  void *ctxp,
								  void *(*malocfp)(void *ctxp, size_t size),
								  void *(*ralocfp)(void *ctxp, void *memptr, size_t newsize),
								  void (*mfreefp)(void *ctxp, void *memptr),
								  size_t xtramem_sz,
								  void **usrmempp);

typedef sword (*PTR_OCIHandleAlloc)(const void *parenth,
									void **hndlpp,
									const ub4 type,
									const size_t xtramem_sz,
									void **usrmempp);

typedef sword (*PTR_OCIHandleFree)(void *hndlp, const ub4 type);

typedef sword (*PTR_OCIServerAttach)(OCIServer *srvhp,
									 OCIError *errhp,
									 const OraText *dblink,
									 sb4 dblink_len,
									 ub4 mode);

typedef sword (*PTR_OCIServerDetach)(OCIServer *srvhp, OCIError *errhp, ub4 mode);

typedef sword (*PTR_OCIAttrSet)(void *trgthndlp,
								ub4 trghndltyp,
								void *attributep,
								ub4 size,
								ub4 attrtype,
								OCIError *errhp);

typedef sword (*PTR_OCISessionBegin)(OCISvcCtx *svchp,
									 OCIError *errhp,
									 OCISession *usrhp,
									 ub4 credt,
									 ub4 mode);

typedef sword (*PTR_OCISessionEnd)(OCISvcCtx *svchp,
								   OCIError *errhp,
								   OCISession *usrhp,
								   ub4 mode);

typedef sword (*PTR_OCIStmtRelease)(OCIStmt *stmtp,
									OCIError *errhp,
									const OraText *key,
									ub4 key_len,
									ub4 mode);

typedef sword (*PTR_OCIErrorGet)(void *hndlp,
								 ub4 recordno,
								 OraText *sqlstate,
								 sb4 *errcodep,
								 OraText *bufp,
								 ub4 bufsiz,
								 ub4 type);

typedef sword (*PTR_OCIStmtPrepare2)(OCISvcCtx *svchp,
									 OCIStmt **stmtp,
									 OCIError *errhp,
									 const OraText *stmt,
									 ub4 stmt_len,
									 const OraText *key,
									 ub4 key_len,
									 ub4 language,
									 ub4 mode);

typedef sword (*PTR_OCIStmtExecute)(OCISvcCtx *svchp,
									OCIStmt *stmtp,
									OCIError *errhp,
									ub4 iters,
									ub4 rowoff,
									const OCISnapshot *snap_in,
									OCISnapshot *snap_out,
									ub4 mode);

typedef sword (*PTR_OCIDefineByPos)(OCIStmt *stmtp,
									OCIDefine **defnp,
									OCIError *errhp,
									ub4 position,
									void *valuep,
									sb4 value_sz,
									ub2 dty,
									void *indp,
									ub2 *rlenp,
									ub2 *rcodep,
									ub4 mode);

typedef sword (*PTR_OCIDescriptorAlloc)(const void *parenth,
										void **descpp,
										const ub4 type,
										const size_t xtramem_sz,
										void **usrmempp);

typedef sword (*PTR_OCIDescriptorFree)(void *descp, const ub4 type);

typedef sword (*PTR_OCILobOpen)(OCISvcCtx *svchp,
								OCIError *errhp,
								OCILobLocator *locp,
								ub1 mode);

typedef sword (*PTR_OCILobClose)(OCISvcCtx *svchp,
								 OCIError *errhp,
								 OCILobLocator *locp);

typedef sword (*PTR_OCILobRead)(OCISvcCtx *svchp,
								OCIError *errhp,
								OCILobLocator *locp,
								ub4 *amtp,
								ub4 offset,
								void *bufp,
								ub4 bufl,
								void *ctxp,
								OCICallbackLobRead cbfp,
								ub2 csid,
								ub1 csfrm);

typedef sword (*PTR_OCILobWrite)(OCISvcCtx *svchp,
								 OCIError *errhp,
								 OCILobLocator *locp,
								 ub4 *amtp,
								 ub4 offset,
								 void *bufp,
								 ub4 buflen,
								 ub1 piece,
								 void *ctxp,
								 OCICallbackLobWrite cbfp,
								 ub2 csid,
								 ub1 csfrm);

typedef sword (*PTR_OCIBindByPos)(OCIStmt *stmtp,
								  OCIBind **bindp,
								  OCIError *errhp,
								  ub4 position,
								  void *valuep,
								  sb4 value_sz,
								  ub2 dty,
								  void *indp,
								  ub2 *alenp,
								  ub2 *rcodep,
								  ub4 maxarr_len,
								  ub4 *curelep,
								  ub4 mode);

typedef sword (*PTR_OCIDateFromText)(OCIError *err,
									 const oratext *date_str,
									 ub4 d_str_length,
									 const oratext *fmt,
									 ub1 fmt_length,
									 const oratext *lang_name,
									 ub4 lang_length,
									 OCIDate *date);

typedef sword (*PTR_OCIDateToText)(OCIError *err,
								   const OCIDate *date,
								   const oratext *fmt,
								   ub1 fmt_length,
								   const oratext *lang_name,
								   ub4 lang_length,
								   ub4 *buf_size,
								   oratext *buf);

typedef sword (*PTR_OCIStmtFetch2)(OCIStmt *stmtp,
								   OCIError *errhp,
								   ub4 nrows,
								   ub2 orientation,
								   sb4 scrollOffset,
								   ub4 mode);

typedef sword (*PTR_OCIAttrGet)(const void *trgthndlp,
								ub4 trghndltyp,
								void *attributep,
								ub4 *sizep,
								ub4 attrtype,
								OCIError *errhp);

/////////////////////////////////////////////////////////////////////////////////////
//
namespace tool
{

/////////////////////////////////////////////////////////////////////////////////////
//
class COCIStringFormatter
{
public:
	const char *Format(const char *string)
	{
		mStr = string ? string : "";

		//*** duplicate '
		for (int i = 0; i < (int)mStr.size(); i++)
		{
			if (mStr[i] == '\'')
			{
				mStr.insert(i + 1, "\'");
				i++;
			}
		}

		return mStr.c_str();
	}

	std::string mStr;
};

/////////////////////////////////////////////////////////////////////////////////////
//
class COCIDatabase
{
	typedef struct dbVar_Tag
	{
		int type;
		long size;
		sb2 indicator;
		union VARTYPE {
			char *s;
			long *l;
			double *lf;
		} var;
	} dbVar;

	typedef struct dbEnv_Tag
	{
		OCIStmt *stmthp;
		OCIDefine *defnp;
		OCIBind *bndp;
		std::list<OCIDate *> dtCollection;
		std::list<dbVar *> VarList;
		OCIError *ocierr;
		OCILobLocator *loblocator;
		int ErrCode;
		char ErrMsg[512];
		int RecordCount;
		bool scrollable;
	} envStruct;

public:
	COCIDatabase();
	virtual ~COCIDatabase();

	const char *GetVersion();

	inline const char *GetConnectionLastError() const
	{
		return m_sLastError;
	}

	HANDLE GetCursor(bool scrollable = false);
	bool DeleteCursor(HANDLE cursor);

	HANDLE Open(const char *pHost,
				const char *pPort,
				const char *DataSource,
				const char *User,
				const char *Password,
				const char *pClient,
				bool useSsl = false,
				const char *pNlsLang = "CANADIAN FRENCH_CANADA.WE8MSWIN1252");

	bool Close();
	bool isConnect();

	/////////////////////////////////////////////////////////////////////////////////////
	//	(Deprecated - Utiliser les prepare, setOutput..., setInput... pour utliliser les
	//	bindvariables correctement).
	bool SQLExec(HANDLE &cursor, const char *sql);															  // Pour requ�tes INSERT DELETE UPDATE et SELECT
	bool SQLExec(HANDLE &cursor, const char *select, const char *Format, ...);								  // Pour SELECT Seulement avec format des donn�es
	bool SQLExec(HANDLE &cursor, const char *select, std::vector<char *> *returnedValues, bool retry = true); // Pour SELECT Seulement avec retour de donn�es
	bool BindField(HANDLE cursor, ub4 Column, ub2 CTypeCode, void *vPtr, sb4 vPtrLength, sb4 *LengthNeeded, void *indicator);

	/////////////////////////////////////////////////////////////////////////////////////
	//
	bool prepareStatement(HANDLE cursor, const char *Select);
	bool executeQuery(HANDLE cursor);
	bool executeInsert(HANDLE cursor);
	bool executeUpdate(HANDLE cursor);
	bool executeDelete(HANDLE cursor);

	/////////////////////////////////////////////////////////////////////////////////////
	//	CLOB support functions.
	bool GetClobDataIntoFile(HANDLE cursor, const char *Select, const char *pathname);
	bool SaveFileDataIntoClob(HANDLE cursor, const char *Select, const char *pathname);
	bool GetClobDataIntoBuffer(HANDLE cursor, const char *Select, char **buffer);
	bool SaveBufferDataIntoClob(HANDLE cursor, const char *Select, const char *buffer);

	/////////////////////////////////////////////////////////////////////////////////////
	//	Set output variables
	bool setOutputString(HANDLE cursor, int elemID, char *colValue, int maxValueSize);
	bool setOutputInt(HANDLE cursor, int elemID, int *colValue);
	bool setOutputLong(HANDLE cursor, int elemID, long *colValue);
	bool setOutputDate(HANDLE cursor, int elemID, OCIDate *dt);

	/////////////////////////////////////////////////////////////////////////////////////
	//	Set input variables
	bool setInputString(HANDLE cursor, int elemID, char *str);
	bool setInputInt(HANDLE cursor, int elemID, int *num);
	bool setInputLong(HANDLE cursor, int elemID, long *num);
	bool setInputDate(HANDLE cursor, int elemID, OCIDate *date);
	bool setInputDate(HANDLE cursor, int elemID, int year, int month, int day, int hour, int min, int sec);
	bool setInputDate(HANDLE cursor, int elemID, char *date, char *format);

	/////////////////////////////////////////////////////////////////////////////////////
	//	Date conversion
	bool ConvertDateToText(OCIDate *ocidate, char *date, char *format);
	bool ConvertTextToDate(char *date, OCIDate *ocidate, char *format);

	/////////////////////////////////////////////////////////////////////////////////////
	//	Fetch Data.
	bool FirstRecord(HANDLE cursor);
	bool NextRecord(HANDLE cursor);
	bool PrevRecord(HANDLE cursor);
	bool LastRecord(HANDLE cursor);
	bool FetchRecord(HANDLE cursor, int rowNum);

	int GetRecordCount(HANDLE cursor);
	int GetCurrentPosition(HANDLE cursor);

	bool Commit(HANDLE cursor);
	bool Rollback(HANDLE cursor);

	char *GetLastOciErrorText(HANDLE cursor);
	int GetLastOciErrorCode(HANDLE cursor);

	const char* GetLastError() const {return m_sLastError;}

private:
	bool RetrySqlExec(HANDLE &cursor, const char *select, std::vector<char *> *returnedValues);

	///	Creates the sqlnet.ora file with ssl configurations in the specified directory.
	///
	///	directory				:	Directory where the sqlnet.ora will be created.
	///
	///	Return value			:	true if successfule, false if not.
	///
	bool CreateSqlnet(const char *directory);

	/////////////////////////////////////////////////////////////////////////////////////
	//	OCI dll client.
	bool LoadClient(const char *pClient);
	void UnloadClient();
	bool LinkProcs();
	void ResetPointers();

	/////////////////////////////////////////////////////////////////////////////////////
	//
	inline sword _OCIEnvCreate(OCIEnv **envp,
							   ub4 mode,
							   void *ctxp,
							   void *(*malocfp)(void *ctxp, size_t size),
							   void *(*ralocfp)(void *ctxp, void *memptr, size_t newsize),
							   void (*mfreefp)(void *ctxp, void *memptr),
							   size_t xtramem_sz,
							   void **usrmempp)
	{
		return mpfOCIEnvCreate(envp, mode, ctxp, malocfp, ralocfp, mfreefp, xtramem_sz, usrmempp);
	}

	/////////////////////////////////////////////////////////////////////////////////////
	//
	inline sword _OCIHandleAlloc(const void *parenth,
								 void **hndlpp,
								 const ub4 type,
								 const size_t xtramem_sz,
								 void **usrmempp)
	{
		return mpfOCIHandleAlloc(parenth, hndlpp, type, xtramem_sz, usrmempp);
	}

	/////////////////////////////////////////////////////////////////////////////////////
	//
	inline sword _OCIHandleFree(void *hndlp, const ub4 type)
	{
		return mpfOCIHandleFree(hndlp, type);
	}

	/////////////////////////////////////////////////////////////////////////////////////
	//
	inline sword _OCIServerAttach(OCIServer *srvhp,
								  OCIError *errhp,
								  const OraText *dblink,
								  sb4 dblink_len,
								  ub4 mode)
	{
		return mpfOCIServerAttach(srvhp, errhp, dblink, dblink_len, mode);
	}

	/////////////////////////////////////////////////////////////////////////////////////
	//
	inline sword _OCIServerDetach(OCIServer *srvhp, OCIError *errhp, ub4 mode)
	{
		return mpfOCIServerDetach(srvhp, errhp, mode);
	}

	/////////////////////////////////////////////////////////////////////////////////////
	//
	inline sword _OCIAttrSet(void *trgthndlp,
							 ub4 trghndltyp,
							 void *attributep,
							 ub4 size,
							 ub4 attrtype,
							 OCIError *errhp)
	{
		return mpfOCIAttrSet(trgthndlp, trghndltyp, attributep, size, attrtype, errhp);
	}

	/////////////////////////////////////////////////////////////////////////////////////
	//
	inline sword _OCISessionBegin(OCISvcCtx *svchp,
								  OCIError *errhp,
								  OCISession *usrhp,
								  ub4 credt,
								  ub4 mode)
	{
		return mpfOCISessionBegin(svchp, errhp, usrhp, credt, mode);
	}

	/////////////////////////////////////////////////////////////////////////////////////
	//
	inline sword _OCISessionEnd(OCISvcCtx *svchp,
								OCIError *errhp,
								OCISession *usrhp,
								ub4 mode)
	{
		return mpfOCISessionEnd(svchp, errhp, usrhp, mode);
	}

	/////////////////////////////////////////////////////////////////////////////////////
	//
	inline sword _OCIStmtRelease(OCIStmt *stmtp,
								 OCIError *errhp,
								 const OraText *key,
								 ub4 key_len,
								 ub4 mode)
	{
		return mpfOCIStmtRelease(stmtp, errhp, key, key_len, mode);
	}

	/////////////////////////////////////////////////////////////////////////////////////
	//
	inline sword _OCIErrorGet(void *hndlp,
							  ub4 recordno,
							  OraText *sqlstate,
							  sb4 *errcodep,
							  OraText *bufp,
							  ub4 bufsiz,
							  ub4 type)
	{
		return mpfOCIErrorGet(hndlp, recordno, sqlstate, errcodep, bufp, bufsiz, type);
	}

	/////////////////////////////////////////////////////////////////////////////////////
	//
	inline sword _OCIStmtPrepare2(OCISvcCtx *svchp,
								  OCIStmt **stmtp,
								  OCIError *errhp,
								  const OraText *stmt,
								  ub4 stmt_len,
								  const OraText *key,
								  ub4 key_len,
								  ub4 language,
								  ub4 mode)
	{
		return mpfOCIStmtPrepare2(svchp, stmtp, errhp, stmt, stmt_len, key, key_len, language, mode);
	}

	/////////////////////////////////////////////////////////////////////////////////////
	//
	inline sword _OCIStmtExecute(OCISvcCtx *svchp,
								 OCIStmt *stmtp,
								 OCIError *errhp,
								 ub4 iters,
								 ub4 rowoff,
								 const OCISnapshot *snap_in,
								 OCISnapshot *snap_out,
								 ub4 mode)
	{
		return mpfOCIStmtExecute(svchp, stmtp, errhp, iters, rowoff, snap_in, snap_out, mode);
	}

	/////////////////////////////////////////////////////////////////////////////////////
	//
	inline sword _OCIDefineByPos(OCIStmt *stmtp,
								 OCIDefine **defnp,
								 OCIError *errhp,
								 ub4 position,
								 void *valuep,
								 sb4 value_sz,
								 ub2 dty,
								 void *indp,
								 ub2 *rlenp,
								 ub2 *rcodep,
								 ub4 mode)
	{
		return mpfOCIDefineByPos(stmtp, defnp, errhp, position, valuep, value_sz, dty, indp, rlenp, rcodep, mode);
	}

	/////////////////////////////////////////////////////////////////////////////////////
	//
	inline sword _OCIDescriptorAlloc(const void *parenth,
									 void **descpp,
									 const ub4 type,
									 const size_t xtramem_sz,
									 void **usrmempp)
	{
		return mpfOCIDescriptorAlloc(parenth, descpp, type, xtramem_sz, usrmempp);
	}

	/////////////////////////////////////////////////////////////////////////////////////
	//
	sword _OCIDescriptorFree(void *descp, const ub4 type)
	{
		return mpfOCIDescriptorFree(descp, type);
	}

	/////////////////////////////////////////////////////////////////////////////////////
	//
	inline sword _OCILobOpen(OCISvcCtx *svchp,
							 OCIError *errhp,
							 OCILobLocator *locp,
							 ub1 mode)
	{
		return mpfOCILobOpen(svchp, errhp, locp, mode);
	}

	/////////////////////////////////////////////////////////////////////////////////////
	//
	inline sword _OCILobClose(OCISvcCtx *svchp,
							  OCIError *errhp,
							  OCILobLocator *locp)
	{
		return mpfOCILobClose(svchp, errhp, locp);
	}

	/////////////////////////////////////////////////////////////////////////////////////
	//
	inline sword _OCILobRead(OCISvcCtx *svchp,
							 OCIError *errhp,
							 OCILobLocator *locp,
							 ub4 *amtp,
							 ub4 offset,
							 void *bufp,
							 ub4 bufl,
							 void *ctxp,
							 OCICallbackLobRead cbfp,
							 ub2 csid,
							 ub1 csfrm)
	{
		return mpfOCILobRead(svchp, errhp, locp, amtp, offset, bufp, bufl, ctxp, cbfp, csid, csfrm);
	}

	/////////////////////////////////////////////////////////////////////////////////////
	//
	inline sword _OCILobWrite(OCISvcCtx *svchp,
							  OCIError *errhp,
							  OCILobLocator *locp,
							  ub4 *amtp,
							  ub4 offset,
							  void *bufp,
							  ub4 buflen,
							  ub1 piece,
							  void *ctxp,
							  OCICallbackLobWrite cbfp,
							  ub2 csid,
							  ub1 csfrm)
	{
		return mpfOCILobWrite(svchp, errhp, locp, amtp, offset, bufp, buflen, piece, ctxp, cbfp, csid, csfrm);
	}

	/////////////////////////////////////////////////////////////////////////////////////
	//
	inline sword _OCIBindByPos(OCIStmt *stmtp,
							   OCIBind **bindp,
							   OCIError *errhp,
							   ub4 position,
							   void *valuep,
							   sb4 value_sz,
							   ub2 dty,
							   void *indp,
							   ub2 *alenp,
							   ub2 *rcodep,
							   ub4 maxarr_len,
							   ub4 *curelep,
							   ub4 mode)
	{
		return mpfOCIBindByPos(stmtp, bindp, errhp, position, valuep, value_sz, dty, indp, alenp, rcodep, maxarr_len, curelep, mode);
	}

	/////////////////////////////////////////////////////////////////////////////////////
	//
	inline sword _OCIDateFromText(OCIError *err,
								  const oratext *date_str,
								  ub4 d_str_length,
								  const oratext *fmt,
								  ub1 fmt_length,
								  const oratext *lang_name,
								  ub4 lang_length,
								  OCIDate *date)
	{
		return mpfOCIDateFromText(err, date_str, d_str_length, fmt, fmt_length, lang_name, lang_length, date);
	}

	/////////////////////////////////////////////////////////////////////////////////////
	//
	inline sword _OCIDateToText(OCIError *err,
								const OCIDate *date,
								const oratext *fmt,
								ub1 fmt_length,
								const oratext *lang_name,
								ub4 lang_length,
								ub4 *buf_size,
								oratext *buf)
	{
		return mpfOCIDateToText(err, date, fmt, fmt_length, lang_name, lang_length, buf_size, buf);
	}

	/////////////////////////////////////////////////////////////////////////////////////
	//
	inline sword _OCIStmtFetch2(OCIStmt *stmtp,
								OCIError *errhp,
								ub4 nrows,
								ub2 orientation,
								sb4 scrollOffset,
								ub4 mode)
	{
		return mpfOCIStmtFetch2(stmtp, errhp, nrows, orientation, scrollOffset, mode);
	}

	/////////////////////////////////////////////////////////////////////////////////////
	//
	inline sword _OCIAttrGet(const void *trgthndlp,
							 ub4 trghndltyp,
							 void *attributep,
							 ub4 *sizep,
							 ub4 attrtype,
							 OCIError *errhp)
	{
		return mpfOCIAttrGet(trgthndlp, trghndltyp, attributep, sizep, attrtype, errhp);
	}

	//
	/////////////////////////////////////////////////////////////////////////////////////

	//	Helpers to clear lists.
	void ClearOCIDateList(std::list<OCIDate *> *pList);
	void ClearDbVarList(std::list<dbVar *> *pList);

	bool SQLGetClob(HANDLE cursor, const char *Select);
	long GetFileSize(FILE *fd) const;
	void FreeHandle();
	bool NewStatement(HANDLE cursor);

	int SetLastOciError(HANDLE cursor, sword status);

	void SetLastOciConnectionError(OCIError *pOciErr);

	bool m_Connected;

	char m_DataSource[65];
	char m_User[65];
	char m_Password[65];
	std::string m_Host;

	//OCI STUFF
	OCIEnv *m_ocienv;
	OCIDescribe *m_dschndl1;
	OCIDescribe *m_dschndl2;
	OCIDescribe *m_dschndl3;

	OCISession *m_authp;
	OCIServer *m_srvhp;
	OCISvcCtx *m_svchp;

	char m_sLastError[2049];
	HANDLE m_Cursor;

	char msEmptyString[2];

	bool mIsUsingSsl;

	std::string mHostConnectInfo;
	std::string mPortConnectInfo;
	std::string mDataSourceConnectInfo;
	std::string mUserConnectInfo;
	std::string mPasswordConnectInfo;
	std::string mpClientConnectInfo;
	std::string mpNlsLangConnectInfo;

#ifdef _WINDOWS
	/////////////////////////////////////////////////////////////////////////////////////
	//	OCI dll client variables.
	HINSTANCE mhDllHandle; //	Handle of the client library's dll.
#else
	void *mhDllHandle;
#endif

	//	Dll client proc pointers.
	PTR_OCIEnvCreate mpfOCIEnvCreate;
	PTR_OCIHandleAlloc mpfOCIHandleAlloc;
	PTR_OCIHandleFree mpfOCIHandleFree;
	PTR_OCIServerAttach mpfOCIServerAttach;
	PTR_OCIServerDetach mpfOCIServerDetach;
	PTR_OCIAttrSet mpfOCIAttrSet;
	PTR_OCISessionBegin mpfOCISessionBegin;
	PTR_OCISessionEnd mpfOCISessionEnd;
	PTR_OCIStmtRelease mpfOCIStmtRelease;
	PTR_OCIErrorGet mpfOCIErrorGet;
	PTR_OCIStmtPrepare2 mpfOCIStmtPrepare2;
	PTR_OCIStmtExecute mpfOCIStmtExecute;
	PTR_OCIDefineByPos mpfOCIDefineByPos;
	PTR_OCIDescriptorAlloc mpfOCIDescriptorAlloc;
	PTR_OCIDescriptorFree mpfOCIDescriptorFree;
	PTR_OCILobOpen mpfOCILobOpen;
	PTR_OCILobClose mpfOCILobClose;
	PTR_OCILobRead mpfOCILobRead;
	PTR_OCILobWrite mpfOCILobWrite;
	PTR_OCIBindByPos mpfOCIBindByPos;
	PTR_OCIDateFromText mpfOCIDateFromText;
	PTR_OCIDateToText mpfOCIDateToText;
	PTR_OCIStmtFetch2 mpfOCIStmtFetch2;
	PTR_OCIAttrGet mpfOCIAttrGet;
};

} // namespace tool

#endif
