/////////////////////////////////////////////////////////////////////////////
//	Str.h
/////////////////////////////////////////////////////////////////////////////

#include <string.h>

#if !defined(_STR_H_INCLUDED__)
#define      _STR_H_INCLUDED__

/////////////////////////////////////////////////////////////////////////////
//	Utility class for string manipulation.
class Str
{
public:

	/////////////////////////////////////////////////////////////////////////
	//	Returns true if there is at least one occurence of pSubString in
	//	pString. If pSubString is an empty tring, i.e. pSubString = "",
	//	then the method returns false unlike strstr.
	static bool StringHasSubstring(const char *pString, const char *pSubString)
	{
		bool bFound  = false;
		int iSubLen  = strlen(pSubString);
		int iLen     = strlen(pString);
		int iMaxStr  = (iLen - iSubLen) + 1;

		//	If the substring is longer then the string,
		//	it can not be contained in the string.
		if((iSubLen <= iLen) && pSubString[0])
		{
			for(int i = 0; ((i < iMaxStr) && !bFound); ++i)
			{
				bFound  = true;
				for(int j = 0; j < iSubLen; ++j)
				{
					if(pSubString[j] != pString[j + i])
					{
						bFound = false;
						break;
					}
				}
			}
		}
		return bFound;
	}

	/////////////////////////////////////////////////////////////////////////
	//	This method is used to split the name field of dicom objects into
	//	first name and last name using cToken as separator. Id is assumed
	//	that last name is before token and first is after.
	static void SplitNamesUsingToken(const char *pName,
		                             char *pFirstName,
									 char *pLastName,
									 const char cToken)
	{
		int iNameLen = 0;
		int i        = 0;
		int j        = 0;
		if(pName && pName[0] && pFirstName && pLastName)
		{
			iNameLen = strlen(pName);
			for(i = 0; i < iNameLen; ++i)
			{
				if(pName[i] != cToken)
				{
					pLastName[i] = pName[i];
				}
				else
				{
					//pLastName[i] = 0x00; // [Ref:0000722]
					break;
				}
			}
			pLastName[i] = 0x00; // [Ref:0000722]
			++i;

			for(;i < iNameLen; ++i)
			{
				if(pName[i] != cToken)
				{
					pFirstName[j] = pName[i];
					++j;
				}
				else
				{
					break;
				}
			}
			pFirstName[j] = 0x00;
		}
	}
};

#endif	//	_STR_H_INCLUDED__
