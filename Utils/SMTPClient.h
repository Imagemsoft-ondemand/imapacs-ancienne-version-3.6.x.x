/////////////////////////////////////////////////////////////////////////////
//	SMTPClient.h
//
//	NOTE	:	Under windows, the calls to WSAStartup and WSACleanup
//				are the responsability of the application.
/////////////////////////////////////////////////////////////////////////////

#include "../Queue.h"
#include "StringBuffer.h"

#if defined(WIN32)

#include <winsock.h>

#else

#include <sys/types.h>		//	Unix socket includes.
#include <sys/socket.h>
#include <sys/un.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/time.h>
#include <sys/select.h>
#include <unistd.h>
#include <netdb.h>
#include <errno.h>

#include <pthread.h>		//	For mutex.

#endif

#if !defined(__SMTPCLIENT_H__)
#define      __SMTPCLIENT_H__

#define BASE64_MAXLINE		76
#define EOL					"\r\n"

#ifdef _DEBUG

#define	READ_TIMEOUT		6	//	Default timeout of 60 seconds when debugging.

#else

#define	READ_TIMEOUT		2	//	Default timeout of 2 seconds for normal release code.

#endif

#define	MAILER_VERSION		"SMTPClient 1.0.0.1"

/////////////////////////////////////////////////////////////////////////////
//	Character sets.
#define	US_ASCII			"us-ascii"
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//
class SMTPClient
{
public:
	SMTPClient();
	virtual ~SMTPClient();

	char *GetVersion() const                                    {return MAILER_VERSION;}

	/////////////////////////////////////////////////////////////////////////
	class Recipient
	{
	public:
		Recipient();
		Recipient(const char *pName, const char *pAddress);
		virtual ~Recipient();

		void Set(const char *pName, const char *pAddress);

		char *GetName() const                                   {return m_pName;}
		char *GetAddress() const                                {return m_pAddress;}

		char *GetRegularFormat();

		void Clear();


	private:
		char *m_pName;
		char *m_pAddress;

		char *m_pRegForm;
	};

	/////////////////////////////////////////////////////////////////////////
	class Attachment
	{
	public:
		Attachment();
		virtual ~Attachment();

		void SetData(char *pBuffer, int iBufferLength)          {m_pBuffer = pBuffer; m_iBufferLenght = iBufferLength;}
		void SetFileName(const char *pFname)                    {if(pFname) m_FileName.Format("%s", pFname);}

		char *GetDataBuffer()                                   {return m_pBuffer;}
		int GetDataBufferLength()                               {return m_iBufferLenght;}

		char *GetFileName()                                     {return m_FileName.GetBuffer();}

	private:

		char *m_pBuffer;
		int   m_iBufferLenght;

		StringBuffer m_FileName;
	};

	bool Connect(const char *pHostName, unsigned short iPort = 25);
	bool EndSession();

	/////////////////////////////////////////////////////////////////////////
	//	Message handling methods.
	void Clear();
	void SetSendTo(const char *pName, const char *pMailAddress);
	void SetFrom(const char *pName, const char *pMailAddress);
	void SetReplyTo(const char *pName, const char *pMailAddress);
	void SetSubject(const char *pSubject);
	void SetCharacterSet(const char *pCharSet);
	
	bool AddAttachment(const char *pFileName, const char *pLocalPath);
	bool AddAttachment(char *pBuffer, int iLength, const char *pFileName);

	void SetMessageBody(const char *pText);

	void AddCC(const char *pName, const char *pMailAddress);

	bool SendMessage();

	char *GetRecipientAddrress() const                          {return m_To.GetAddress();}
	char *GetRecipientName() const                              {return m_To.GetName();}
	char *GetSenderAddress() const                              {return m_From.GetAddress();}
	char *GetSenderName() const                                 {return m_From.GetAddress();}
	char *GetSubject() const                                    {return m_pSubject;}

	char *GetLastError() const                                  {return m_pLastError;}

private:

	/////////////////////////////////////////////////////////////////////////
	//	Error managment methods.
	void SetLastError(const char *pErrorText);
	char *GetLastSocketErrorString() const;

	/////////////////////////////////////////////////////////////////////////
	//	Base 64 encoding methods.	
	int GetBase64BufferLength(int iInputBufferSize);
	bool EncodeBase64(const char *pInputBufer, 
		              int iInputBufferLength, 
					  char *pOutputBuffer, 
					  int iOutputBufferSize, 
					  int *iBytesWritten);

	/////////////////////////////////////////////////////////////////////////
	//	SMTP protocol methods.
	bool SendRCPT(Recipient *pRecipient);
	bool ReadResponce(int iExpectedCode);
	bool SendHeader(const char *pCc);

	/////////////////////////////////////////////////////////////////////////
	//	Network helpers.
	int Select(int sec, int usec);
	int FdIsSet();
	int Peek();
	int WriteData(const char *pBuffer, int iLength);
	int Close();

	/////////////////////////////////////////////////////////////////////////
	//	Time related methods as well as the mutex for thread safety (we are
	//	using localtime).
	bool GetTimeStamp(char *pDateTime);

#if defined(WIN32)
	HANDLE               m_hMutex;
#else
	pthread_mutex_t      m_hMutex;
#endif

	void Lock();
	void Unlock();


	/////////////////////////////////////////////////////////////////////////
	//	Message data.
	Recipient            m_To;
	Recipient            m_From;
	Recipient            m_ReplyTo;

	Queue <Recipient *>  m_CCList;
	Queue <Attachment *> m_aAttachmentList;

	char                *m_pMessageBody;
	char                *m_pSubject;
	char                *m_pCharSet;


	char                *m_pLastError;

#if defined(WIN32)
	SOCKET               m_iSocket;
#else
	int                  m_iSocket;
#endif

	char                *m_pBase64tab;

	fd_set               m_rset;
};

#endif	//	__SMTPCLIENT_H__
