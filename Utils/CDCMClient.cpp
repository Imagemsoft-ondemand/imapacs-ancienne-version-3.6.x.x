/////////////////////////////////////////////////////////////////////////////
//	CDCMClient.cpp
/////////////////////////////////////////////////////////////////////////////

#ifdef WIN32
#include "../stdafx.h"
#endif

#include "PatchModule.h"
#include "CDCMClient.h"

#ifdef WIN32
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
#endif

/////////////////////////////////////////////////////////////////////////////
//
namespace imapacs
{
	/////////////////////////////////////////////////////////////////////////
	//
	CDCMClient::CDCMClient()
	{
		m_sRemoteIP[0]            = 0x00;
		m_iRemotePort             = 0;
		m_sCallingAeTitle[0]      = 0x00;
		m_sCalledAeTitle[0]       = 0x00;

		m_pLogger                 = NULL;
		m_pLoggerModality         = NULL;
		m_pMailer				  = NULL;

		m_pNetwork                = NULL;
		m_pAssociation            = NULL;
		m_pParameters             = NULL;

		m_pCfg                    = NULL;

		m_PreferedNetOutputTrnSyn = EXS_LittleEndianExplicit;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	CDCMClient::~CDCMClient()
	{
	}

	/////////////////////////////////////////////////////////////////////////
	//
	void CDCMClient::SetConfig(PacsConfig *pCfg)
	{
		if(pCfg)
		{
			m_pCfg            = pCfg;
			m_pLoggerModality = pCfg->GetModalityLogger();
			m_pLogger         = pCfg->GetServerLogger();
			m_pMailer         = pCfg->GetMailer();
		}
	}

	/////////////////////////////////////////////////////////////////////////
	//	Allocates some data structures and parameters for the network layer
	//	of the association.
	bool CDCMClient::CreateAssociation(bool bStorage)
	{
		OFCondition cond = EC_Normal;
		char sPeer[65]   = {0};

		cond = ASC_initializeNetwork(NET_REQUESTOR, 0, 1000, &m_pNetwork);
		if(cond.bad())
		{
			m_sLastErrorText.Format("%s", cond.text());
			return false;
		}

		cond = ASC_createAssociationParameters(&m_pParameters, ASC_DEFAULTMAXPDU);
		if(cond.bad())
		{
			m_sLastErrorText.Format("%s", cond.text());

			if(m_pNetwork)
			{
				ASC_dropNetwork(&m_pNetwork);				// Delete net structure.
				m_pNetwork = NULL;
			}
			return false;
		}

		cond = ASC_setAPTitles(m_pParameters, m_sCallingAeTitle, m_sCalledAeTitle, NULL);
		if(cond.bad())
		{
			m_sLastErrorText.Format("%s", cond.text());

			if(m_pNetwork)
			{
				ASC_dropNetwork(&m_pNetwork);				// Delete net structure.
				m_pNetwork = NULL;
			}

			if(m_pParameters)
			{
				delete m_pParameters;
				m_pParameters = NULL;
			}
			return false;
		}

		sprintf(sPeer, "%s:%d", m_sRemoteIP, m_iRemotePort);
		cond = ASC_setPresentationAddresses(m_pParameters, " ", sPeer);
		if(cond.bad())
		{
			m_sLastErrorText.Format("%s", cond.text());

			if(m_pNetwork)
			{
				ASC_dropNetwork(&m_pNetwork);				// Delete net structure.
				m_pNetwork = NULL;
			}

			if(m_pParameters)
			{
				delete m_pParameters;
				m_pParameters = NULL;
			}
			return false;
		}

		if(!AddAllPresentationContexts(m_pParameters, bStorage))
		{
			return false;
		}
		return true;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	STORE_STATUS CDCMClient::StoreObject(const char *pFileName, const char *pCallingAE)
	{
		bool bMustDeleteFile         = false;
		OFCondition cond             = EC_Normal;
		T_ASC_PresentationContext pc = {0};
		char sAcceptedTrnSyn[65]     = {0};
		char sFileTrnSyn[65]         = {0};
		char sTmp[51]                = {0};
		DcmDataset *pStatus          = NULL;
		DcmDataset *pObjectDataset   = NULL;
		DIC_UI SopClass              = {0};
		DIC_UI SopInstance           = {0};
		PatchParams PatchInfos;
		T_ASC_PresentationContextID presId ;
		DcmFileFormat DicomFile;

		STORE_STATUS RetVal          = SS_SUCCESS;

		PatchModule Patch;

		StringBuffer Str;

		T_DIMSE_C_StoreRQ req;
		T_DIMSE_C_StoreRSP rsp;

		memset(&req, 0, sizeof(T_DIMSE_C_StoreRQ));
		memset(&rsp, 0, sizeof(T_DIMSE_C_StoreRSP));

		if(!pFileName || !m_pAssociation)
		{
			m_sLastErrorText.Format("NULL Pointer passed to CDCMClient::StoreObject [%s line %d]", 
									__FILE__, __LINE__);
			return SS_ERROR;
		}

		DIC_US msgId = m_pAssociation->nextMsgID++;

		m_Compressor.FixCompressedImage(pFileName);	//	Necessary patch in case old 
													//	broken images are requested.

		/////////////////////////////////////////////////////////////////////
		//	Load the object from the file.
		cond = DicomFile.loadFile(pFileName);
		if(cond.bad())
		{
			m_sLastErrorText.Format("%s", cond.text());
			return SS_ERROR;
		}

		pObjectDataset = DicomFile.getDataset();

		/////////////////////////////////////////////////////////////////////
		//	Figure out which SOP class and SOP instance is encapsulated
		//	in the file.
		if(!DU_findSOPClassAndInstanceInDataSet(pObjectDataset,
												SopClass, 
												SopInstance, 
												OFFalse))
		{
			m_sLastErrorText.Format("No SOP Class & Instance UIDs in file: %s", pFileName);

			return SS_ERROR;
		}

		/////////////////////////////////////////////////////////////////////
		//	Figure out which of the accepted presentation contexts should
		//	be used.
		DcmXfer filexfer(pObjectDataset->getOriginalXfer());

		if(filexfer.getXfer() != EXS_Unknown) 
		{
			presId = ASC_findAcceptedPresentationContextID(m_pAssociation, SopClass, filexfer.getXferID());
		}
		else
		{
			presId = ASC_findAcceptedPresentationContextID(m_pAssociation, SopClass);
		}	

		if(presId == 0)
		{
			const char *pModalityName = dcmSOPClassUIDToModality(SopClass);
			if(!pModalityName)
			{
				pModalityName = dcmFindNameOfUID(SopClass);
			}

			if(!pModalityName)
			{
				pModalityName = "Unknown SOP class";
			}
			m_sLastErrorText.Format("No presentation context for: (%s) %s", pModalityName, SopClass);

			return SS_ERROR;
		}

		/////////////////////////////////////////////////////////////////////
		//	Check proposed vs accepted transfert syntaxe.
		cond = ASC_findAcceptedPresentationContext(m_pAssociation->params, presId, &pc);
		if(cond.bad())
		{
			m_sLastErrorText.Format("%s", cond.text());
			return SS_ERROR;
		}

		strcpy(sAcceptedTrnSyn, pc.acceptedTransferSyntax);
		strcpy(sFileTrnSyn, filexfer.getXferID());

		/////////////////////////////////////////////////////////////////////
		//	Build request.
		req.MessageID   = m_pAssociation->nextMsgID++;
		req.DataSetType = DIMSE_DATASET_PRESENT;
		req.Priority    = DIMSE_PRIORITY_MEDIUM;

		strcpy(req.AffectedSOPClassUID, SopClass);
		strcpy(req.AffectedSOPInstanceUID, SopInstance);

		if(DICOMUtil::IsImageType(SopClass))
		{
			if(filexfer.isEncapsulated())	//	Image is compressed.
			{
				//	Decompress image into a temporary file.
				sprintf(sTmp, "%d", getpid());
				Str.Format("%s/%s_ucomp.dcm", m_pCfg->GetTmpPath(), sTmp);

				bMustDeleteFile = true;	//	We need to delete the temporary file.

				if(!m_Compressor.UncompressToFile(pObjectDataset, Str.GetBuffer(), EXS_LittleEndianExplicit))
				{
					if(m_pLoggerModality)
					{
						m_pLoggerModality->LogMsg("ERROR : %s", cond.text());
						m_pLoggerModality->LogMsg("        Could not uncompress file.");
					}
					if(m_pMailer)
					{
						m_pMailer->AddLine("%s", cond.text());
						m_pMailer->AddLine("Could not uncompress file.");
					}
					RetVal = SS_ERROR;
				}
				else
				{
					if(m_pLoggerModality)
						m_pLoggerModality->LogMsg("Image was uncompressed as : %s", Str.GetBuffer());
					if (m_pMailer)
						m_pMailer->AddLine("Image was uncompressed as : [%s]", Str.GetBuffer());
				}
			}
			else
			{
				//	Image isn't compressed, send it.
				Str.Format("%s", pFileName);
			}
		}
		else
		{
			//	It's not an image, send it as is (no decompression).
			Str.Format("%s", pFileName);
		}

		if(RetVal == SS_SUCCESS)
		{
			DcmFileFormat Object;

			cond = Object.loadFile(Str.GetBuffer());

			pObjectDataset = Object.getDataset();

			//	Patch for reformat (CTJONQ).
			PatchInfos.Reset();

			strcpy(PatchInfos.sPatchId, "00007");
			PatchInfos.pCallingAeTitle = pCallingAE;
			PatchInfos.pModalityLogger = m_pLoggerModality;
			PatchInfos.pMailer         = m_pMailer;
			PatchInfos.pDataset        = pObjectDataset;
			PatchInfos.pDb             = m_pCfg->GetDb();
			PatchInfos.pCfg            = m_pCfg;

			Patch.Dispatcher(&PatchInfos);

			cond = DIMSE_storeUser(m_pAssociation,
								   presId,
								   &req,
								   NULL, 
								   pObjectDataset,
								   NULL,
								   NULL,
								   DIMSE_BLOCKING, 
								   0,
								   &rsp,
								   &pStatus,
								   NULL,
								   OFstatic_cast(long, OFStandard::getFileSize(pFileName)));
			if(cond.good())
			{
				if(rsp.DimseStatus == STATUS_Success) 
				{
					RetVal = SS_SUCCESS;
				}
				else if((rsp.DimseStatus & 0xf000) == 0xb000)
				{
					RetVal = SS_WARNING;
				}
				else
				{
					RetVal = SS_ERROR;
				}
			}
			else
			{
				if(m_pLoggerModality)
					m_pLoggerModality->LogMsg("ERROR : %s", cond.text());

				if(m_pMailer)
					m_pMailer->AddLine("%s", cond.text());
				RetVal = SS_ERROR;
			}
		}
		DICOMUtil::ClearDataset(&pStatus);

		if(bMustDeleteFile)
		{
			unlink(Str.GetBuffer());
		}
		return RetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	STORE_STATUS CDCMClient::StoreObject(DcmDataset *pDataset)
	{
		STORE_STATUS RetVal                 = SS_ERROR;
		OFCondition cond                    = EC_Normal;
		T_ASC_PresentationContext pc        = {0};
		char sAcceptedTrnSyn[65]            = {0};
		char sFileTrnSyn[65]                = {0};
		DcmDataset *pStatus                 = NULL;
		DIC_UI SopClass                     = {0};
		DIC_UI SopInstance                  = {0};
		T_ASC_PresentationContextID presId  = 0x00;
		T_DIMSE_C_StoreRQ req;
		T_DIMSE_C_StoreRSP rsp;

		if(pDataset)
		{
			if(m_pAssociation)
			{
				memset(&req, 0, sizeof(T_DIMSE_C_StoreRQ));
				memset(&rsp, 0, sizeof(T_DIMSE_C_StoreRSP));

				DIC_US msgId = m_pAssociation->nextMsgID++;

				if(DU_findSOPClassAndInstanceInDataSet(pDataset,
													   SopClass, 
													   SopInstance, 
													   OFFalse))
				{
					/////////////////////////////////////////////////////////
					//	Figure out which of the accepted presentation
					//	contexts should be used.
					DcmXfer filexfer(pDataset->getOriginalXfer());

					if(filexfer.getXfer() != EXS_Unknown) 
					{
						presId = ASC_findAcceptedPresentationContextID(m_pAssociation, SopClass, filexfer.getXferID());
					}
					else
					{
						presId = ASC_findAcceptedPresentationContextID(m_pAssociation, SopClass);
					}

					if(presId)
					{
						cond = ASC_findAcceptedPresentationContext(m_pAssociation->params, presId, &pc);
						if(cond.good())
						{
							strcpy(sAcceptedTrnSyn, pc.acceptedTransferSyntax);
							strcpy(sFileTrnSyn, filexfer.getXferID());

							/////////////////////////////////////////////////
							//	Build request.
							req.MessageID   = m_pAssociation->nextMsgID++;
							req.DataSetType = DIMSE_DATASET_PRESENT;
							req.Priority    = DIMSE_PRIORITY_MEDIUM;

							strcpy(req.AffectedSOPClassUID, SopClass);
							strcpy(req.AffectedSOPInstanceUID, SopInstance);

							cond = DIMSE_storeUser(m_pAssociation,
												   presId,
												   &req,
												   NULL, 
												   pDataset,
												   NULL,
												   NULL,
												   DIMSE_BLOCKING, 
												   0,
												   &rsp,
												   &pStatus,
												   NULL);
							if(cond.good())
							{
								if(rsp.DimseStatus == STATUS_Success) 
								{
									RetVal = SS_SUCCESS;
								}
								else if((rsp.DimseStatus & 0xf000) == 0xb000)
								{
									RetVal = SS_WARNING;
								}
								else
								{
									RetVal = SS_ERROR;
								}
							}
							else
							{
								if(m_pLoggerModality)
									m_pLoggerModality->LogMsg("ERROR : %s", cond.text());

								if(m_pMailer)
									m_pMailer->AddLine("%s", cond.text());

								RetVal = SS_ERROR;
							}
						}
						else
						{
							m_sLastErrorText.Format("%s", cond.text());
						}
					}
					else
					{
						const char *pModalityName = dcmSOPClassUIDToModality(SopClass);
						if(!pModalityName)
						{
							pModalityName = dcmFindNameOfUID(SopClass);
						}

						if(!pModalityName)
						{
							pModalityName = "Unknown SOP class";
						}
						m_sLastErrorText.Format("No presentation context for: (%s) %s", pModalityName, SopClass);
					}
				}
				else
				{
					m_sLastErrorText.Format("No SOP Class or Instance UIDs in dataset passed to CDCMClient::StoreObject.");
				}
			}
			else
			{
				if(m_pLoggerModality)
				{
					m_pLogger->LogMsg("ERROR : NULL association CDCMClient::StoreObject.");
				}
			}
		}
		else
		{
			if(m_pLoggerModality)
			{
				m_pLogger->LogMsg("ERROR : NULL dataset passed to CDCMClient::StoreObject.");
			}
		}
		DICOMUtil::ClearDataset(&pStatus);

		return RetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	bool CDCMClient::RequestAssociation()
	{
		bool bRet = true;

		OFCondition cond = ASC_requestAssociation(m_pNetwork, m_pParameters, &m_pAssociation);
		if(cond.bad())
		{
			if(cond == DUL_ASSOCIATIONREJECTED)
			{
				T_ASC_RejectParameters rej;
				ASC_getRejectParameters(m_pParameters, &rej);
			}

			CloseAssociation();
			m_sLastErrorText.Format("%s", cond.text());

			bRet               = false;
		}
		return bRet;
	}

	/////////////////////////////////////////////////////////////////////////
	//	Releases association, closes the network connection and frees the
	//	underlying data structures.
	void CDCMClient::CloseAssociation()
	{
		if(m_pAssociation)
		{
			ASC_releaseAssociation(m_pAssociation);		// Release association.
			ASC_destroyAssociation(&m_pAssociation);	// Delete assoc structure.
		}
		else if(m_pParameters)
		{
			delete m_pParameters;
		}

		if(m_pNetwork)
		{
			ASC_dropNetwork(&m_pNetwork);				// Delete network structure.
		}
		m_pAssociation     = NULL;
		m_pParameters      = NULL;
		m_pNetwork         = NULL;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	E_TransferSyntax CDCMClient::GetTrnSynFromStr(const char *pTrnSyn)
	{
		E_TransferSyntax TrnSyntax = EXS_Unknown;

		if(strcmp(pTrnSyn, UID_LittleEndianImplicitTransferSyntax) == 0)
		{
			TrnSyntax = EXS_LittleEndianImplicit;
		}
		else if(strcmp(pTrnSyn, UID_LittleEndianExplicitTransferSyntax) == 0)
		{
			TrnSyntax = EXS_LittleEndianExplicit;
		}
		else if(strcmp(pTrnSyn, UID_BigEndianExplicitTransferSyntax) == 0)
		{
			TrnSyntax = EXS_BigEndianExplicit;
		}
		return TrnSyntax;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	bool CDCMClient::AddAllPresentationContexts(T_ASC_Parameters *pParams, bool bStorage)
	{
		bool bRetVal               = false;
		OFCondition cond           = EC_Normal;
		int i                      = 0;
		int iPid                   = 1;
		const char **pSopClassList = NULL;
		int iSopClassCount         = 0;
		const char **pTrnSynList   = NULL;
		int iTrnSynCount           = 0;

		if(!pParams)
			return false;

		/////////////////////////////////////////////////////////////////////
		//
		pSopClassList = m_pCfg->GetSOPList(&iSopClassCount, bStorage);
		if(pSopClassList)
		{
			for(i = 0; (i < iSopClassCount) && cond.good(); ++i)
			{
				pTrnSynList = m_pCfg->GetTransferList(pSopClassList[i], &iTrnSynCount, false);
				if(pTrnSynList)
				{
					cond = ASC_addPresentationContext(pParams,
													  iPid,
													  pSopClassList[i],
													  pTrnSynList,
													  iTrnSynCount);

					PacsConfig::ClearStringList(pTrnSynList, iTrnSynCount);
					pTrnSynList = NULL;
				}
				iPid += 2;	//	Presentation context id's must all be odd.
			}
			PacsConfig::ClearStringList(pSopClassList, iSopClassCount);
			pSopClassList = NULL;

			if(cond.good())
			{
				bRetVal = true;
			}
			else
			{
				m_sLastErrorText.Format("%s", cond.text());
			}
		}
		else
		{
			m_pLoggerModality->LogMsg("ERROR : No SOP class found.");
			m_pMailer->AddLine("ERROR : No SOP class found.");
		}
		return bRetVal;
	}
}
