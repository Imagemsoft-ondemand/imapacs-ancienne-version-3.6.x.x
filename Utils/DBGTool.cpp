/////////////////////////////////////////////////////////////////////////////
//	DBGTool.h
/////////////////////////////////////////////////////////////////////////////

#ifdef WIN32
#include "../stdafx.h"
#endif

#include <stdio.h>
#include <string.h>
#include <stdarg.h>

#ifdef WIN32
#include <Windows.h>
#else
#include <sys/time.h>
#endif

#include "DBGTool.h"

#ifdef WIN32
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
#endif

/////////////////////////////////////////////////////////////////////////////
//
char *DBGTool::MakeFormattedString(const char *pFormat, ...)
{
	int iLen               = 0;
	int iFormatLen         = 0;
	int iRetVal            = -1;
	char sNumericValue[41] = {0};	//	To determine the size of numerical values.
	const char *pStr       = NULL;
	char *pBuffer          = NULL;
	va_list	Varlist;

	if(pFormat)
	{
		iLen = iFormatLen = strlen(pFormat);
		va_start(Varlist, pFormat);

		for(int i = 0; i < iFormatLen; ++i)
		{
			if(pFormat[i] == '%')
			{
				++i;
				switch(pFormat[i])
				{
					case 's':
					{
						pStr = va_arg(Varlist, char*);
						if(pStr)
						{
							iLen = (iLen + strlen(pStr)) + 1;
						}
					}
					break;

					case 'd':
					{
						sprintf(sNumericValue, "%d", va_arg(Varlist, int));
						iLen = (iLen + strlen(sNumericValue)) + 1;
					}
					break;

					case 'f':
					{
						sprintf(sNumericValue, "%f", va_arg(Varlist, double));
						iLen = (iLen + strlen(sNumericValue)) + 1;
					}
					break;

					case 'x':
					{
						sprintf(sNumericValue, "%x", va_arg(Varlist, int));
						iLen = (iLen + strlen(sNumericValue)) + 1;
					}
					break;
				}
			}
		}
		va_end(Varlist);

		pBuffer = new char[iLen + 1];
		if(pBuffer)
		{
			va_start(Varlist, pFormat);
			iRetVal = vsprintf(pBuffer, pFormat, Varlist);
			va_end(Varlist);
		}
	}
	return pBuffer;
}

/////////////////////////////////////////////////////////////////////////////
//
void DBGTool::OutputLineToFile(const char *pFile,
		                         const char *pFormat, ...)
{
	int iLen               = 0;
	int iFormatLen         = 0;
	int iRetVal            = -1;
	char sNumericValue[41] = {0};	//	To determine the size of numerical values.
	const char *pStr       = NULL;
	char *pBuffer          = NULL;
	FILE *pFd              = NULL;
	va_list	Varlist;

	if(pFormat)
	{
		iLen = iFormatLen = strlen(pFormat);
		va_start(Varlist, pFormat);

		for(int i = 0; i < iFormatLen; ++i)
		{
			if(pFormat[i] == '%')
			{
				++i;
				switch(pFormat[i])
				{
					case 's':
					{
						pStr = va_arg(Varlist, char*);
						if(pStr)
							iLen = (iLen + strlen(pStr)) + 1;
					}
					break;

					case 'd':
					{
						sprintf(sNumericValue, "%d", va_arg(Varlist, int));
						iLen = (iLen + strlen(sNumericValue)) + 1;
					}
					break;

					case 'f':
					{
						sprintf(sNumericValue, "%f", va_arg(Varlist, double));
						iLen = (iLen + strlen(sNumericValue)) + 1;
					}
					break;

					case 'x':
					{
						sprintf(sNumericValue, "%x", va_arg(Varlist, int));
						iLen = (iLen + strlen(sNumericValue)) + 1;
					}
					break;
				}
			}
		}
		va_end(Varlist);

		pBuffer = new char[iLen + 1];
		if(pBuffer)
		{
			va_start(Varlist, pFormat);
			iRetVal = vsprintf(pBuffer, pFormat, Varlist);
			va_end(Varlist);

			pFd = fopen(pFile, "a");
			if(pFd)
			{
				fwrite(pBuffer, 1, iRetVal, pFd);
				fwrite("\n", 1, 1, pFd);

				fclose(pFd);
				pFd = NULL;
			}
			delete [] pBuffer;
			pBuffer = NULL;
		}
	}
}

/////////////////////////////////////////////////////////////////////////////
//	On unix, the line will be written to stderr. With Visual studio, it will
//	be written to the output window.
void DBGTool::OutputLine(const char *pFormat, ...)
{
	int iLen               = 0;
	int iFormatLen         = 0;
	int iRetVal            = -1;
	char sNumericValue[41] = {0};	//	To determine the size of numerical values.
	const char *pStr       = NULL;
	char *pBuffer          = NULL;
	va_list	Varlist;

	if(pFormat)
	{
		iLen = iFormatLen = strlen(pFormat);
		va_start(Varlist, pFormat);

		for(int i = 0; i < iFormatLen; ++i)
		{
			if(pFormat[i] == '%')
			{
				++i;
				switch(pFormat[i])
				{
					case 's':
					{
						pStr = va_arg(Varlist, char*);
						if(pStr)
						{
							iLen = (iLen + strlen(pStr)) + 1;
						}
					}
					break;

					case 'd':
					{
						sprintf(sNumericValue, "%d", va_arg(Varlist, int));
						iLen = (iLen + strlen(sNumericValue)) + 1;
					}
					break;

					case 'f':
					{
						sprintf(sNumericValue, "%f", va_arg(Varlist, double));
						iLen = (iLen + strlen(sNumericValue)) + 1;
					}
					break;

					case 'x':
					{
						sprintf(sNumericValue, "%x", va_arg(Varlist, int));
						iLen = (iLen + strlen(sNumericValue)) + 1;
					}
					break;
				}
			}
		}
		va_end(Varlist);

		pBuffer = new char[iLen + 1];
		if(pBuffer)
		{
			va_start(Varlist, pFormat);
			iRetVal = vsprintf(pBuffer, pFormat, Varlist);
			va_end(Varlist);

#ifdef WIN32
			OutputDebugString(pBuffer);
			OutputDebugString("\n");
#else
			fprintf(stderr, "%s", pBuffer);
#endif

			delete [] pBuffer;
			pBuffer = NULL;
		}
	}
}


/////////////////////////////////////////////////////////////////////////////
//
void DBGTool::StartTimer()
{
#ifdef WIN32
	if(m_bHasPerformanceTimer)
	{
		LARGE_INTEGER iTime;

		if(m_bMultiCore)
			m_ThreadAffinityMask = SetThreadAffinityMask(GetCurrentThread(), 1);

		QueryPerformanceCounter(&iTime);

		m_iStartTime = (unsigned int)((iTime.QuadPart * 1000 / m_Frequency.QuadPart));
	}
	else
	{
		m_iStartTime = GetTickCount();
	}
#else
	timeval tv = {0};
	gettimeofday(&tv, 0);

	m_iStartTime = (unsigned int)(tv.tv_sec * 1000) + (tv.tv_usec / 1000);
#endif
}

/////////////////////////////////////////////////////////////////////////////
//
void DBGTool::StopTimer()
{
#ifdef WIN32
	if(m_bHasPerformanceTimer)
	{
		LARGE_INTEGER iTime;

		if(m_bMultiCore)
			SetThreadAffinityMask(GetCurrentThread(), m_ThreadAffinityMask);

		QueryPerformanceCounter((LARGE_INTEGER*)&iTime);

		m_iEndTime = (unsigned int)((iTime.QuadPart * 1000 / m_Frequency.QuadPart));
	}
	else
	{
		m_iEndTime = GetTickCount();
	}
#else
	timeval tv = {0};
	gettimeofday(&tv, 0);

	m_iEndTime = (unsigned int)(tv.tv_sec * 1000) + (tv.tv_usec / 1000);
#endif
}

/////////////////////////////////////////////////////////////////////////////
//
unsigned int DBGTool::GetIntervalInMili() const
{
	return (m_iEndTime - m_iStartTime);
}
