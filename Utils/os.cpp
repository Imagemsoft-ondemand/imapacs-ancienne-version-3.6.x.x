/////////////////////////////////////////////////////////////////////////////
//	os.cpp
/////////////////////////////////////////////////////////////////////////////

#ifdef WIN32
#include "../stdafx.h"
#endif

#include <string.h>
#include "os.h"

#ifdef WIN32
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
#endif

/////////////////////////////////////////////////////////////////////////////
//
namespace imapacs
{
	/////////////////////////////////////////////////////////////////////////
	//
	int os::MkDir(const char *pDir, int iMode)
	{
#if defined(WIN32)
		return _mkdir(pDir);
#else
		return mkdir(pDir, iMode);
#endif
	}

	/////////////////////////////////////////////////////////////////////////
	//
	bool os::FileExists(const char *pFile)
	{
		if(!pFile)
			return false;

		FILE *fp = fopen(pFile, "r");
		if(!fp)
			return false;

		fclose(fp);
		fp = NULL;

		return true;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	bool os::FileMove(const char *pSource, const char *pDestination)
	{
		bool bRet = true;

		if(!pSource || !pDestination)
			return false;
#ifdef WIN32
		if(!MoveFileEx(pSource, pDestination, MOVEFILE_REPLACE_EXISTING))
			bRet = false;
#else
		if(rename(pSource, pDestination))
			bRet = false;
#endif
		return bRet;
	}

	/////////////////////////////////////////////////////////////////////////
	//	Returns false as soon as one of the directory creation failed. Since
	//	creation of a directory will fail if the directory already exists,
	//	the return value isn't always miningful.
	bool os::CreateFullDirectory(const char *pPath, int iMode)
	{
		bool bRet  = true;
		int iLen   = 0;
		int i      = 0;
		char *pDir = NULL;

		if(!pPath)
			return false;

		iLen = strlen(pPath);

		pDir = new char[iLen + 1];
		if(!pDir)
		{
			return false;
		}

		//	We don't want to use mkdir on the root.
		if(pPath[0] == '/')
		{
			pDir[0] = pPath[0];
			++i;
		}

		while(i < iLen)
		{
			if(pPath[i] == '/')
			{
				pDir[i] = 0x00;
				if(MkDir(pDir, iMode))
					bRet = false;

				pDir[i] = pPath[i];
			}
			else
			{
				pDir[i] = pPath[i];
			}
			++i;
		}

		pDir[i] = 0x00;
		if(MkDir(pDir, iMode))
			bRet = false;

		delete [] pDir;
		pDir = NULL;

		return bRet;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	bool os::RemoveFile(const char *pFile)
	{
		bool bRetVal = false;

		if(!remove(pFile))	//	This function returns 0 when successful.
			bRetVal = true;

		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////
	//
	void os::RemoveAccents(char *pSrc)
	{
		unsigned char c = 0x00;

		if(pSrc && pSrc[0])
		{
			for(int i = (strlen(pSrc) - 1); i > 0; --i)
			{
				c = (unsigned char)pSrc[i];

				if(c >= 192 && c <= 197) //ÀÁÂÃÄÅ
					pSrc[i] = 'A';
				else if(c >= 224 && c <= 229) //àáâãäå
					pSrc[i] = 'a';
				else if(c == 199) //Ç
					pSrc[i] = 'C';
				else if(c == 231) //ç
					pSrc[i] = 'c';
				else if(c >= 200 && c <= 203) //ÈÉÊË
					pSrc[i] = 'E';
				else if(c >= 232 && c <= 235) //èéêë
					pSrc[i] = 'e';
				else if(c >= 204 && c <= 207) //ÌÍÎÏ
					pSrc[i] = 'I';
				else if(c >= 236 && c <= 239) //ìíîï
					pSrc[i] = 'i';
				else if(c == 209) //Ñ
					pSrc[i] = 'N';
				else if(c == 241) //ñ
					pSrc[i] = 'n';
				else if(c >= 201 && c <= 214) //ÒÓÔÕÖ
					pSrc[i] = 'O';
				else if(c >= 242 && c <= 246) //òóôõö
					pSrc[i] = 'o';
				else if(c >= 217 && c <= 220) //ÙÚÛÜ
					pSrc[i] = 'U';
				else if(c >= 249 && c <= 252) //ùúûü'
					pSrc[i] = 'u';
				else if(c == 221) //Ý
					pSrc[i] = 'Y';
				else if(c == 253) //ý
					pSrc[i] = 'y';
				else if(c > 127)
					pSrc[i] = ' ';
			}
		}
	}
}
