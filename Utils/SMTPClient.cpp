/////////////////////////////////////////////////////////////////////////////
//	SMTPClient.cpp
/////////////////////////////////////////////////////////////////////////////

#ifdef WIN32
#include "../stdafx.h"
#endif

#include "SMTPClient.h"

#include <string.h>
#include <stdlib.h>
#include <time.h>

#ifdef WIN32
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
#endif

/////////////////////////////////////////////////////////////////////////////
//
SMTPClient::SMTPClient()
{
	m_iSocket      = -1;
	m_pLastError   = NULL;
	m_pMessageBody = NULL;
	m_pCharSet     = NULL;

	m_pSubject     = new char[21];
	if(m_pSubject)
	{
		strcpy(m_pSubject, "(No Subject)");
	}

	m_pCharSet = new char[9];
	if(m_pCharSet)
	{
		strcpy(m_pCharSet, "us-ascii");
	}

#if defined (WIN32)
	m_hMutex       = CreateMutex(NULL,FALSE,NULL);
#else
	pthread_mutex_init(&m_hMutex,NULL);
#endif

	m_pBase64tab   = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                                      "abcdefghijklmnopqrstuvwxyz0123456789+/";
}

/////////////////////////////////////////////////////////////////////////////
//
SMTPClient::~SMTPClient()
{
	Recipient *pCur  = NULL;
	Attachment *pAtt = NULL;

	if(m_pLastError)
	{
		delete [] m_pLastError;
		m_pLastError = NULL;
	}

	if(m_pMessageBody)
	{
		delete [] m_pMessageBody;
		m_pMessageBody = NULL;
	}

	if(m_pSubject)
	{
		delete [] m_pSubject;
		m_pSubject = NULL;
	}

	if(m_pCharSet)
	{
		delete [] m_pCharSet;
		m_pCharSet = NULL;
	}

	while(m_CCList.Pop(pCur))
	{
		delete pCur;
		pCur = NULL;
	}

	while(m_aAttachmentList.Pop(pAtt))
	{
		delete pAtt;
		pAtt = NULL;
	}

#if defined(WIN32)
	if(m_hMutex)
	{
		CloseHandle(m_hMutex);
	}
#else
	pthread_mutex_destroy(&m_hMutex);
#endif
}

/////////////////////////////////////////////////////////////////////////////
//	Establishes the connection to the mail server. It will resove the ip
//	address if a name is given as the host. Also takes care of the
//	handshake with the mail server.
bool SMTPClient::Connect(const char *pHostName, unsigned short iPort)
{
	StringBuffer Buff;
	struct hostent *pHost     = NULL;
	char *ptrtmp              = NULL;
	char *pErr                = NULL;
	char sIpAddress[21]       = {0};
	char sLocalHostName[129]  = {0};
	struct sockaddr_in remote = {0};

	if(!pHostName)
		return false;

	Lock();	//	To make the call to gethostbyname thread safe.

	pHost = gethostbyname(pHostName);
	if(!pHost)
	{
		pErr = GetLastSocketErrorString();
		if(pErr)
		{
			SetLastError(pErr);
			return false;
		}
		return false;
	}

	ptrtmp = inet_ntoa(*(struct in_addr*)pHost->h_addr);
	if(!ptrtmp)
	{
		pErr = GetLastSocketErrorString();
		if(pErr)
		{
			SetLastError(pErr);
			return false;
		}
		return false;
	}
	strcpy(sIpAddress, ptrtmp);

	Unlock();

	m_iSocket = socket(AF_INET, SOCK_STREAM, 0);
	if(m_iSocket == -1)
	{
		Close();

		pErr = GetLastSocketErrorString();
		if(pErr)
		{
			SetLastError(pErr);
			Close();

			return false;
		}
	}

	remote.sin_family      = AF_INET;
	remote.sin_addr.s_addr = inet_addr(sIpAddress);
	remote.sin_port        = htons(iPort);
	memset(remote.sin_zero,0,8);

	if(connect(m_iSocket, (struct sockaddr *)&remote, sizeof(struct sockaddr)) == -1)
	{
		Close();

		pErr = GetLastSocketErrorString();
		if(pErr)
		{
			SetLastError(pErr);
			return false;
		}
		return false;
	}

	//	Handshake with the server.
	if(!ReadResponce(220))
	{
		SetLastError(GetLastSocketErrorString());
		Close();

		return false;
	}

	if(gethostname(sLocalHostName, 128) == 0)
	{
		Buff.Format("HELO %s\r\n", sLocalHostName);
	}
	else
	{
		Buff.Format("HELO Unknown\r\n");
	}

	if(WriteData(Buff.GetBuffer(), Buff.GetBufferLength()) <= 0)
	{
		SetLastError(GetLastSocketErrorString());
		Close();

		return false;
	}

	if(!ReadResponce(250))
	{
		SetLastError(GetLastSocketErrorString());
		Close();

		return false;
	}
	return true;
}

/////////////////////////////////////////////////////////////////////////////
//
bool SMTPClient::EndSession()
{
	StringBuffer sBuffer;

	sBuffer = "QUIT\r\n";

	if(WriteData(sBuffer.GetBuffer(), sBuffer.GetBufferLength()) <= 0)
	{
		SetLastError(GetLastSocketErrorString());
		Close();

		return false;
	}

	if(!ReadResponce(221))
	{
		SetLastError("An unexpected QUIT response was received\n");
		Close();

		return false;
	}
	Close();

	return true;
}

/////////////////////////////////////////////////////////////////////////////
//
int SMTPClient::Close()
{
	int iRet = -1;
#if defined(WIN32)
	iRet = closesocket(m_iSocket);
	m_iSocket = -1;
#else
	iRet = close(m_iSocket);
	m_iSocket = -1;
#endif
	return iRet;
}

/////////////////////////////////////////////////////////////////////////////
//	Resets everything to be able to start another message.
void SMTPClient::Clear()
{
	Recipient *pCur  = NULL;
	Attachment *pAtt = NULL;

	if(m_pLastError)
	{
		delete [] m_pLastError;
		m_pLastError = NULL;
	}

	if(m_pMessageBody)
	{
		delete [] m_pMessageBody;
		m_pMessageBody = NULL;
	}

	if(m_pSubject)
	{
		delete [] m_pSubject;
		m_pSubject = NULL;
	}

	if(m_pCharSet)
	{
		delete [] m_pCharSet;
		m_pCharSet = NULL;
	}

	while(m_CCList.Pop(pCur))
	{
		delete pCur;
		pCur = NULL;
	}

	while(m_aAttachmentList.Pop(pAtt))
	{
		delete pAtt;
		pAtt = NULL;
	}
}

/////////////////////////////////////////////////////////////////////////////
//
void SMTPClient::SetSendTo(const char *pName, const char *pMailAddress)
{
	m_To.Set(pName, pMailAddress);
}

/////////////////////////////////////////////////////////////////////////////
//
void SMTPClient::SetFrom(const char *pName, const char *pMailAddress)
{
	m_From.Set(pName, pMailAddress);
}

/////////////////////////////////////////////////////////////////////////////
//
void SMTPClient::SetReplyTo(const char *pName, const char *pMailAddress)
{
	m_ReplyTo.Set(pName, pMailAddress);
}

/////////////////////////////////////////////////////////////////////////////
//
void SMTPClient::SetSubject(const char *pSubject)
{
	if(pSubject)
	{
		if(m_pSubject)
		{
			delete [] m_pSubject;
			m_pSubject = NULL;
		}

		m_pSubject = new char[strlen(pSubject) + 1];
		if(m_pSubject)
		{
			strcpy(m_pSubject, pSubject);
		}
	}
}

/////////////////////////////////////////////////////////////////////////////
//
void SMTPClient::SetCharacterSet(const char *pCharSet)
{
	if(m_pCharSet)
	{
		delete [] m_pCharSet;
		m_pCharSet = NULL;
	}

	if(pCharSet)
	{
		m_pCharSet = new char[strlen(pCharSet) + 1];
		if(m_pCharSet)
		{
			strcpy(m_pCharSet, pCharSet);
		}
	}
}

/////////////////////////////////////////////////////////////////////////////
//
bool SMTPClient::AddAttachment(const char *pFileName, const char *pLocalPath)
{
	int  i64Written  = 0;
	long i64Size     = 0;
	long iSize       = 0;
	char *pBuffer    = NULL;
	char *p64Buffer  = NULL;
	Attachment *pAtt = NULL;
	FILE *fp         = NULL;
	char sSep[5]     ={0};
	StringBuffer sFile;

	if(!pFileName || !pLocalPath)
		return false;

	sFile.Format("%s%s", pLocalPath, pFileName);

	fp = fopen(sFile.GetBuffer(), "rb");
	if(!fp)
		return false;

	fseek(fp, 0L, SEEK_END);
	iSize = ftell(fp);
	fseek(fp, 0L, SEEK_SET);

	if(iSize <= 0)
	{
		fclose(fp);
		fp = NULL;

		return false;
	}

	pBuffer = new char[iSize];
	if(!pBuffer)
	{
		fclose(fp);
		fp = NULL;

		return false;
	}

	fread(pBuffer, 1, iSize, fp);
	fclose(fp);
	fp = NULL;

	i64Size = GetBase64BufferLength(iSize);
	if(i64Size <= 0)
	{
		delete [] pBuffer;
		pBuffer = NULL;

		return false;
	}

	p64Buffer = new char[i64Size];
	if(!p64Buffer)
	{
		delete [] pBuffer;
		pBuffer = NULL;

		return false;
	}

	if(!EncodeBase64(pBuffer, iSize, p64Buffer, i64Size, &i64Written))
	{
		delete [] pBuffer;
		pBuffer = NULL;

		delete [] p64Buffer;
		p64Buffer = NULL;

		return false;
	}

	pAtt = new Attachment;
	if(!pAtt)
	{
		delete [] pBuffer;
		pBuffer = NULL;

		delete [] p64Buffer;
		p64Buffer = NULL;

		return false;
	}

	pAtt->SetFileName(pFileName);
	pAtt->SetData(p64Buffer, i64Written);

	m_aAttachmentList.Add(pAtt);

	delete [] pBuffer;
	pBuffer = NULL;

	return true;
}

/////////////////////////////////////////////////////////////////////////////
//
bool SMTPClient::AddAttachment(char *pBuffer, int iLength, const char *pFileName)
{
	int  i64Written  = 0;
	long i64Size     = 0;
	char *p64Buffer  = NULL;
	Attachment *pAtt = NULL;

	if(!pFileName)
		return false;

	i64Size = GetBase64BufferLength(iLength);

	if(i64Size <= 0)
	{
		delete [] pBuffer;
		pBuffer = NULL;

		return false;
	}

	p64Buffer = new char[i64Size];
	if(!p64Buffer)
	{
		delete [] pBuffer;
		pBuffer = NULL;

		return false;
	}

	if(!EncodeBase64(pBuffer, iLength, p64Buffer, i64Size, &i64Written))
	{
		delete [] pBuffer;
		pBuffer = NULL;

		delete [] p64Buffer;
		p64Buffer = NULL;
	}

	pAtt = new Attachment;
	if(!pAtt)
	{
		delete [] pBuffer;
		pBuffer = NULL;

		delete [] p64Buffer;
		p64Buffer = NULL;
	}

	pAtt->SetFileName(pFileName);
	pAtt->SetData(p64Buffer, i64Written);

	m_aAttachmentList.Add(pAtt);

	delete [] pBuffer;
	pBuffer = NULL;

	return true;
}

/////////////////////////////////////////////////////////////////////////////
//
void SMTPClient::SetMessageBody(const char *pText)
{
	if(pText)
	{
		if(m_pMessageBody)
		{
			delete [] m_pMessageBody;
			m_pMessageBody = NULL;
		}

		m_pMessageBody = new char [strlen(pText) + 1];
		if(m_pMessageBody)
		{
			strcpy(m_pMessageBody, pText);
		}
	}
}

/////////////////////////////////////////////////////////////////////////////
//
void SMTPClient::AddCC(const char *pName, const char *pMailAddress)
{
	if(pMailAddress)
	{
		Recipient *pCC = new Recipient(pName, pMailAddress);
		if(pCC)
		{
			m_CCList.Add(pCC);
		}
	}
}

/////////////////////////////////////////////////////////////////////////////
//
bool SMTPClient::SendMessage()
{
	char sErrorStr[513] = {0};
	Recipient *pCur     = NULL;
	Attachment *pAtt    = NULL;
	int idx             = 0;
	int iDataLength     = 0;
	StringBuffer Cc;
	StringBuffer SendBuffer;
	StringBuffer Buff;

	if(!m_pMessageBody || !m_From.GetAddress())
	{
		SetLastError("ERROR : Either no message body or no sender found.");
		return false;
	}

	//	Send mail command.
	SendBuffer.Format("MAIL FROM:<%s>\r\n", m_From.GetAddress());

	if(send(m_iSocket, SendBuffer.GetBuffer(), SendBuffer.GetBufferLength(), 0) < 0)
	{
		sprintf(sErrorStr, "Failed in call to send MAIL command [%s].", GetLastSocketErrorString());
		SetLastError(sErrorStr);

		return false;
	}

	if(!ReadResponce(250))
	{
		return false;
	}

	//	Send the "To" recipient.
	if(!m_To.GetAddress())
	{
		SetLastError("ERROR : No recipient found.");
		return false;
	}

	if(!SendRCPT(&m_To))
	{
		return false;
	}

	//	Send the "CC" recipients.
	while(m_CCList.Pop(pCur))
	{
		if(!SendRCPT(pCur))
		{
			delete []  pCur;
			pCur = NULL;

			return false;
		}

		if(idx)
			Cc += ",";

		Cc += pCur->GetRegularFormat();

		++idx;

		delete []  pCur;
		pCur = NULL;
	}

	//	Send the DATA command.
	SendBuffer = "DATA\r\n";
	if(send(m_iSocket, SendBuffer.GetBuffer(), SendBuffer.GetBufferLength(), 0) < 0)
	{
		SetLastError(GetLastSocketErrorString());
		return false;
	}

	//	Read DATA command responce.
	if(!ReadResponce(354))
	{
		return false;
	}

	if(!SendHeader(Cc.GetBuffer()))
	{
		return false;
	}

	//	Send MIME body header for attachments if there are any.
	if(m_aAttachmentList.GetSize())
	{
		Buff.Format("%s", "\r\n--#BOUNDARY#\r\nContent-Type: text/plain; charset=%s\r\nContent-Transfer-Encoding: quoted-printable\r\n\r\n",
			        m_pCharSet);

		if(WriteData(Buff.GetBuffer(), Buff.GetBufferLength()) <= 0)
		{
			SetLastError(GetLastSocketErrorString());
			return false;
		}
		Buff.Clear();
	}

	if(!m_pMessageBody)
	{
		return false;
	}

	iDataLength = strlen(m_pMessageBody);

	if(WriteData(m_pMessageBody, iDataLength) <= 0)
	{
		SetLastError(GetLastSocketErrorString());
		return false;
	}

	//	Send attachments.
	if(m_aAttachmentList.SetAtFirst())
	{
		while(m_aAttachmentList.GetCurrent(pAtt))
		{
			//	Send the Mime header for each attachment.
			Buff.Format("\r\n\r\n--#BOUNDARY#\r\nContent-Type: application/octet-stream; name=%s\r\nContent-Transfer-Encoding: base64\r\nContent-Disposition: attachment; filename=%s\r\n\r\n",
						 pAtt->GetFileName(),
						 pAtt->GetFileName());

			if(WriteData(Buff.GetBuffer(), Buff.GetBufferLength()) <= 0)
			{
				SetLastError(GetLastSocketErrorString());
				return false;
			}

			//	Then send the encoded attachment.
			if(WriteData(pAtt->GetDataBuffer(), pAtt->GetDataBufferLength()) <= 0)
			{
				SetLastError(GetLastSocketErrorString());
				return false;
			}
			m_aAttachmentList.Next();
		}
	}

	//	Send the final mime boundary.
	if(m_aAttachmentList.GetSize())
	{
		Buff.Format("%s", "\r\n--#BOUNDARY#--");

		if(WriteData(Buff.GetBuffer(), Buff.GetBufferLength()) <= 0)
		{
			SetLastError(GetLastSocketErrorString());
			return false;
		}
	}

	SendBuffer = "\r\n.\r\n";
	if(WriteData(SendBuffer.GetBuffer(), SendBuffer.GetBufferLength()) <= 0)
	{
		SetLastError(GetLastSocketErrorString());
		return false;
	}

	if(!ReadResponce(250))
	{
		return false;
	}
	return true;
}

/////////////////////////////////////////////////////////////////////////////
//
void SMTPClient::SetLastError(const char *pErrorText)
{
	if(pErrorText)
	{
		if(m_pLastError)
		{
			delete [] m_pLastError;
			m_pLastError = NULL;
		}

		m_pLastError = new char[strlen(pErrorText) + 1];
		if(m_pLastError)
		{
			strcpy(m_pLastError, pErrorText);
		}
	}
}

/////////////////////////////////////////////////////////////////////////////
//
int SMTPClient::GetBase64BufferLength(int iInputBufferSize)
{
  int iOutSize = (iInputBufferSize + 2)/3*4;            //	3:4 conversion ratio.
  iOutSize += strlen(EOL)*iOutSize/BASE64_MAXLINE + 3;  //	Space for newlines and NULL.

  return iOutSize;
}

/////////////////////////////////////////////////////////////////////////////
//
bool SMTPClient::EncodeBase64(const char *pInputBufer, 
							  int iInputBufferLength, 
							  char *pOutputBuffer, 
							  int iOutputBufferSize, 
							  int *iBytesWritten)
{
	if(!pInputBufer || !pOutputBuffer || (iOutputBufferSize <= 0))
		return false;

	if(iOutputBufferSize < GetBase64BufferLength(iInputBufferLength))
		return false;

	//	Set up the parameters prior to the main encoding loop.
	int nInPos   = 0;
	int nOutPos  = 0;
	int nLineLen = 0;
	char* cp     = NULL;

	int nMax     = iInputBufferLength/3;

	//	Get three characters at a time from the input buffer and encode them.
	for(int i=0; i < nMax; ++i) 
	{
		//	Get the next 2 characters.
		int c1 = pInputBufer[nInPos++] & 0xFF;
		int c2 = pInputBufer[nInPos++] & 0xFF;
		int c3 = pInputBufer[nInPos++] & 0xFF;

		//Encode into the 4 6 bit characters
		pOutputBuffer[nOutPos++] = m_pBase64tab[(c1 & 0xFC) >> 2];
		pOutputBuffer[nOutPos++] = m_pBase64tab[((c1 & 0x03) << 4) | ((c2 & 0xF0) >> 4)];
		pOutputBuffer[nOutPos++] = m_pBase64tab[((c2 & 0x0F) << 2) | ((c3 & 0xC0) >> 6)];
		pOutputBuffer[nOutPos++] = m_pBase64tab[c3 & 0x3F];

		nLineLen += 4;

		//	Handle the case where we have gone over the max line boundary.
		if(nLineLen >= BASE64_MAXLINE - 3) 
		{
			char* cp = EOL;
			pOutputBuffer[nOutPos++] = *cp++;

			if(*cp) 
				pOutputBuffer[nOutPos++] = *cp;

			nLineLen = 0;
		}
	}

	//	Encode the remaining one or two characters in the input buffer.
	switch(iInputBufferLength % 3) 
	{
		case 0:
		{
			cp = EOL;
			pOutputBuffer[nOutPos++] = *cp++;
			if (*cp) 
				pOutputBuffer[nOutPos++] = *cp;

			break;
		}
		case 1:
		{
			int c1 = pInputBufer[nInPos] & 0xFF;

			pOutputBuffer[nOutPos++] = m_pBase64tab[(c1 & 0xFC) >> 2];
			pOutputBuffer[nOutPos++] = m_pBase64tab[((c1 & 0x03) << 4)];
			pOutputBuffer[nOutPos++] = '=';
			pOutputBuffer[nOutPos++] = '=';

			cp = EOL;

			pOutputBuffer[nOutPos++] = *cp++;
			if (*cp) 
				pOutputBuffer[nOutPos++] = *cp;

			break;
		}
		case 2:
		{
			int c1 = pInputBufer[nInPos++] & 0xFF;
			int c2 = pInputBufer[nInPos] & 0xFF;

			pOutputBuffer[nOutPos++] = m_pBase64tab[(c1 & 0xFC) >> 2];
			pOutputBuffer[nOutPos++] = m_pBase64tab[((c1 & 0x03) << 4) | ((c2 & 0xF0) >> 4)];
			pOutputBuffer[nOutPos++] = m_pBase64tab[((c2 & 0x0F) << 2)];
			pOutputBuffer[nOutPos++] = '=';

			cp = EOL;

			pOutputBuffer[nOutPos++] = *cp++;
			if (*cp) 
				pOutputBuffer[nOutPos++] = *cp;

			break;
		}
		default: 
		{
			return false;
			break;
		}
	}
	pOutputBuffer[nOutPos] = 0;
	*iBytesWritten = nOutPos;

	return true;
}

/////////////////////////////////////////////////////////////////////////////
//
bool SMTPClient::SendRCPT(Recipient *pRecipient)
{
	char *pBuffer = NULL;
	bool bRet     = false;
	int iStrLen   = 0;

	if(pRecipient && pRecipient->GetAddress())
	{
		char *pBuffer = new char [strlen(pRecipient->GetAddress()) + 1 + 12];
		if(pBuffer)
		{
			sprintf(pBuffer, "RCPT TO:<%s>\r\n", pRecipient->GetAddress());

			iStrLen = strlen(pBuffer);
			if(send(m_iSocket, pBuffer, iStrLen, 0) != -1)
			{
				if(!ReadResponce(250))
				{
					SetLastError("ERROR : An unexpected RCPT response was received");
				}
				else
				{
					bRet = true;
				}
			}
			delete [] pBuffer;
			pBuffer = NULL;
		}
	}
	return bRet;
}

/////////////////////////////////////////////////////////////////////////////
//
bool SMTPClient::ReadResponce(int iExpectedCode)
{
	bool bRet             = false;
	bool bFoundTerminator = false;
	int iRet              = 0;
	int iBytesRead        = 0;
	int iOffset           = 0;
	int iGrowBy           = 256;
	int iCurrentSize      = 256;
	int iElapsedTime      = 0;
	char *pTerminator     = "\r\n";
	int iTermLen          = 0;
	char sReadBuffer[256] = {0};
	char sCode[5]         = {0};
	int iCode             = 0;
	char *pCommandBuffer  = NULL;
	char *pNewBuffer      = NULL;
	int iNbTry            = int(10.0/0.25);
	int iTryCnt           = 0;

	pCommandBuffer = new char[iCurrentSize];
	if(pCommandBuffer)
	{
		while(!bFoundTerminator)
		{
			iRet = Select(0, 250000);
			if(iRet > 0)
			{
				iTryCnt = 0;

				if(FdIsSet() > 0)
				{
					if(Peek() > 0)	//	We have data to read in the socket buffer.
					{
						iBytesRead = recv(m_iSocket, sReadBuffer, 255, 0);
						if((iOffset + iBytesRead) >= iCurrentSize)
						{
							//	Reallocate buffer.
							iCurrentSize += iGrowBy;
							pNewBuffer = new char[iCurrentSize];
							if(pNewBuffer)
							{
								memcpy(pNewBuffer, pCommandBuffer, iOffset);
								delete [] pCommandBuffer;

								pCommandBuffer = pCommandBuffer;
							}
							else
							{
								SetLastError("ERROR : failed to allocate buffer.");
								break;
							}
						}
						memcpy(&pCommandBuffer[iOffset], sReadBuffer, iBytesRead);
						iOffset += iBytesRead;

						pCommandBuffer[iOffset] = 0x00;

						//	We read a complete server responce with a terminator.
						if(strstr(pCommandBuffer, pTerminator))
							bFoundTerminator = true;
					}
					else
					{
						SetLastError(GetLastSocketErrorString());
						break;
					}
				}
				else
				{
					SetLastError(GetLastSocketErrorString());
					break;
				}
			}
			else if(iRet == 0)	//	Select timed out.
			{
				if(iTryCnt >= iNbTry)
				{
					SetLastError("Timeout reading server responce.");
					bRet = false;

					break;
				}
				++iTryCnt;
			}
			else				//	A socket error occured.
			{
				SetLastError(GetLastSocketErrorString());
				break;
			}
		}

		//	Remove the terminator from the responce.
		iTermLen = strlen(pTerminator);
		if(iOffset > iTermLen)
		{
			pCommandBuffer[iOffset - iTermLen] = 0x00;

			sCode[0] = pCommandBuffer[0];
			sCode[1] = pCommandBuffer[1];
			sCode[2] = pCommandBuffer[2];
			sCode[3] = 0x00;

			iCode = atoi(sCode);
		}

		delete [] pCommandBuffer;
		pCommandBuffer = NULL;

		if(iCode == iExpectedCode)
			bRet = true;
	}
	else
	{
		SetLastError("ERROR : failed to allocate buffer.");
	}
	return bRet;
}

/////////////////////////////////////////////////////////////////////////////
//
bool SMTPClient::SendHeader(const char *pCc)
{
	char sTimeStamp[41] = {0};
	StringBuffer Buff;
	StringBuffer tmp;
	StringBuffer Reply;

	if(!GetTimeStamp(sTimeStamp))
	{
		SetLastError("Failed to get time stamp.");
		return false;
	}

	if(pCc)
	{
		Buff.Format("From:	%s\r\nTo: %s\r\nCc: %s\r\nSubject: %s\r\nDate: %s\r\nX-Mailer: %s\r\n",
			        m_From.GetRegularFormat(),
					m_To.GetRegularFormat(),
					pCc,
					m_pSubject,
					sTimeStamp,
					MAILER_VERSION);
	}
	else
	{
		Buff.Format("From: %s\r\nTo: %s\r\nSubject: %s\r\nDate: %s\r\nX-Mailer: %s\r\n",
			        m_From.GetRegularFormat(),
					m_To.GetRegularFormat(),
					m_pSubject,
					sTimeStamp,
					MAILER_VERSION);
	}

	if(m_ReplyTo.GetAddress())
	{
		Reply.Format("Reply-To: %s\r\n", m_ReplyTo.GetAddress());
		Buff += Reply;
	}

	if(m_aAttachmentList.GetSize())
	{
		Buff += "MIME-Version: 1.0\r\nContent-type: multipart/mixed; boundary=\"#BOUNDARY#\"\r\n";
	}
	else
	{
		tmp.Format("MIME-Version: 1.0\r\nContent-type: text/plain; charset=%s\r\n", m_pCharSet);
		Buff += tmp;
	}
	Buff += "\r\n";

	if(WriteData(Buff.GetBuffer(), Buff.GetBufferLength()) <= 0)
	{
		SetLastError(GetLastSocketErrorString());
		return false;
	}
	return true;
}

/////////////////////////////////////////////////////////////////////////////
//
int SMTPClient::Select(int sec, int usec)
{
	struct timeval tv = {0};

	tv.tv_sec  = sec;
	tv.tv_usec = usec;

	FD_ZERO(&m_rset);
	FD_SET(m_iSocket, &m_rset);

	return select(m_iSocket + 1, &m_rset, NULL, NULL, &tv);
}

/////////////////////////////////////////////////////////////////////////////
//
int SMTPClient::FdIsSet()
{
	return FD_ISSET(m_iSocket, &m_rset);
}

/////////////////////////////////////////////////////////////////////////////
//
int SMTPClient::Peek()
{
	char cByte = 0x00;
	return recv(m_iSocket, &cByte, 1, MSG_PEEK);
}

/////////////////////////////////////////////////////////////////////////////
//
int SMTPClient::WriteData(const char *pBuffer, int iLength)
{
	int offset       = 0;
	int bytesWritten = 0;
	int bytesLeft    = 0;

	bytesWritten = send(m_iSocket, pBuffer, iLength, 0);
	if(bytesWritten == -1)
		return (-1);

	offset = bytesWritten;
	while(offset < iLength)
	{
		bytesLeft = (iLength - offset);
		bytesWritten = send(m_iSocket, &pBuffer[offset], bytesLeft, 0);
		if(bytesWritten == -1)
			return (-1);
		offset += bytesWritten;
	}
	return offset;
}

/////////////////////////////////////////////////////////////////////////////
//
bool SMTPClient::GetTimeStamp(char *pDateTime)
{
	struct tm *pTime  = NULL;
	struct tm Time    = {0};			//	Local copy.
	time_t iTime      = 0;
	char TimeZone[11] = {0};
	char sDate[45]    = {0};
	char *pDate       = NULL;
	long iTzBias      = 0;

	if(!pDateTime)
		return false;

	//	Just making sure everything is set for time zone.
#ifdef WIN32
	_tzset();
#else
	tzset();
#endif

	time(&iTime);

	Lock();
	pTime = localtime(&iTime);
	if(!pTime)
		return false;

	memcpy(&Time, pTime, sizeof(struct tm));

	iTzBias = (timezone/60);

	pDate = ctime(&iTime);
	if(!pDate)
		return false;

	strcpy(sDate, pDate);
	sDate[strlen(sDate) - 1] = 0x00;	//	Remove the \r.
	Unlock();

	if(Time.tm_isdst)					//	If daylight saving, remove one hour.
		iTzBias -= 60;

	sprintf(TimeZone, "%+.2d%.2d", -iTzBias/60, iTzBias%60);

	sprintf(pDateTime, "%s %s", sDate, TimeZone);

	return true;
}

/////////////////////////////////////////////////////////////////////////////
//
void SMTPClient::Lock()
{
#if defined(WIN32)
	WaitForSingleObject(m_hMutex,INFINITE);
#else
	pthread_mutex_lock(&m_hMutex);
#endif
}

/////////////////////////////////////////////////////////////////////////////
//
void SMTPClient::Unlock()
{
#if defined(WIN32)
	ReleaseMutex(m_hMutex);
#else
	pthread_mutex_unlock(&m_hMutex);
#endif
}

/////////////////////////////////////////////////////////////////////////////
//	WARNING : This function needs to be tested under Unix at least for
//            compilation. I'm not sure about all the error codes, they
//            may not all be in errno.h.
char *SMTPClient::GetLastSocketErrorString() const
{
#if defined(WIN32)
	switch(WSAGetLastError()) 
	{
		case WSAEINTR:           return "WSAEINTR : A blocking operation was interrupted by a call to WSACancelBlockingCall.";
		case WSAEBADF:           return "WSAEBADF : The file or socket handle supplied is not valid.";
		case WSAEACCES:          return "WSAEACCES : An attempt was made to access a socket in a way forbidden by its access permissions.";
		case WSAEDISCON:         return "WSAEDISCON : Returned by WSARecv or WSARecvFrom to indicate the remote party has initiated a graceful shutdown sequence.";
		case WSAEFAULT:          return "WSAEFAULT : The system detected an invalid pointer address in attempting to use a pointer argument in a call.";
		case WSAEINVAL:          return "WSAEINVAL : An invalid argument was supplied.";
		case WSAEMFILE:          return "WSAEMFILE : Too many open sockets.";
		case WSAEWOULDBLOCK:     return "WSAEWOULDBLOCK : A non-blocking socket operation could not be completed immediately.";
		case WSAEINPROGRESS:     return "WSAEINPROGRESS : A blocking operation is currently executing.";
		case WSAEALREADY:        return "WSAEALREADY : An operation was attempted on a non-blocking socket that already had an operation in progress.";
		case WSAENOTSOCK:        return "WSAENOTSOCK : An operation was attempted on something that is not a socket.";
		case WSAEDESTADDRREQ:    return "WSAEDESTADDRREQ : A required address was omitted from an operation on a socket.";
		case WSAEMSGSIZE:        return "WSAEMSGSIZE : A message sent on a datagram socket was larger than the internal message buffer or some other network limit, or the buffer used to receive a datagram into was smaller than the datagram itself.";
		case WSAEPROTOTYPE:      return "WSAEPROTOTYPE : A protocol was specified in the socket function call that does not support the semantics of the socket type requested.";
		case WSAENOPROTOOPT:     return "WSAENOPROTOOPT : An unknown, invalid, or unsupported option or level was specified in a getsockopt or setsockopt call.";
		case WSAEPROTONOSUPPORT: return "WSAEPROTONOSUPPORT : The requested protocol has not been configured into the system, or no implementation for it exists.";
		case WSAESOCKTNOSUPPORT: return "WSAESOCKTNOSUPPORT : The support for the specified socket type does not exist in this address family.";
		case WSAEOPNOTSUPP:      return "WSAEOPNOTSUPP : The attempted operation is not supported for the type of object referenced.";
		case WSAEPFNOSUPPORT:    return "WSAEPFNOSUPPORT : The protocol family has not been configured into the system or no implementation for it exists.";
		case WSAEAFNOSUPPORT:    return "WSAEAFNOSUPPORT : An address incompatible with the requested protocol was used.";
		case WSAEADDRINUSE:      return "WSAEADDRINUSE : Only one usage of each socket address (protocol/network address/port) is normally permitted.";
		case WSAEADDRNOTAVAIL:   return "WSAEADDRNOTAVAIL : The requested address is not valid in its context.";
		case WSAENETDOWN:        return "WSAENETDOWN : A socket operation encountered a dead network.";
		case WSAENETUNREACH:     return "WSAENETUNREACH : A socket operation was attempted to an unreachable network.";
		case WSAENETRESET:       return "WSAENETRESET : The connection has been broken due to keep-alive activity detecting a failure while the operation was in progress.";
		case WSAECONNABORTED:    return "WSAECONNABORTED : An established connection was aborted by the software in your host machine.";
		case WSAECONNRESET:      return "WSAECONNRESET : An existing connection was forcibly closed by the remote host.";
		case WSAENOBUFS:         return "WSAENOBUFS : An operation on a socket could not be performed because the system lacked sufficient buffer space or because a queue was full.";
		case WSAEISCONN:         return "WSAEISCONN : A connect request was made on an already connected socket.";
		case WSAENOTCONN:        return "WSAENOTCONN : A request to send or receive data was disallowed because the socket is not connected and (when sending on a datagram socket using a sendto call) no";
		case WSAESHUTDOWN:       return "WSAESHUTDOWN : A request to send or receive data was disallowed because the socket had already been shut down in that direction with a previous shutdown call.";
		case WSAETOOMANYREFS:    return "WSAETOOMANYREFS : Too many references to some kernel object.";
		case WSAETIMEDOUT:       return "WSAETIMEDOUT : A connection attempt failed because the connected party did not properly respond after a period of time, or established connection failed because connected host has failed to respond.";
		case WSAECONNREFUSED:    return "WSAECONNREFUSED : No connection could be made because the target machine actively refused it.";
		case WSAELOOP:           return "WSAELOOP : Cannot translate name.";
		case WSAENAMETOOLONG:    return "WSAENAMETOOLONG : Name component or name was too long.";
		case WSAEHOSTDOWN:       return "WSAEHOSTDOWN : A socket operation failed because the destination host was down.";
		case WSASYSNOTREADY:     return "WSASYSNOTREADY : WSAStartup cannot function at this time because the underlying system it uses to provide network services is currently unavailable.";
		case WSAVERNOTSUPPORTED: return "WSAVERNOTSUPPORTED : The Windows Sockets version requested is not supported.";
		case WSANOTINITIALISED:  return "WSANOTINITIALISED : Either the application has not called WSAStartup, or WSAStartup failed.";
		case WSAHOST_NOT_FOUND:  return "WSAHOST_NOT_FOUND : No such host is known.";
		case WSATRY_AGAIN:       return "WSATRY_AGAIN : This is usually a temporary error during hostname resolution and means that the local server did not receive a response from an authoritative server.";
		case WSANO_RECOVERY:     return "WSANO_RECOVERY : A non-recoverable error occurred during a database lookup.";
		case WSANO_DATA:         return "WSANO_DATA : The requested name is valid and was found in the database, but it does not have the correct associated data being resolved for.";

		default:                 return "NO ERROR";
	}
#else
	switch(errno)
	{
		case EINTR:              return "EINTR : A blocking call was interrupted by a signal.";
		case EBADF:              return "EBADF : Bad descriptor.";
		case EACCES:             return "EACCES : Permission denied.";
		case EFAULT:             return "EFAULT : A pointer was passed to a system system call that does not belong to the calling process addressing space.";
		case EINVAL:             return "EINVAL : Invalid argument.";
		case EMFILE:             return "EMFILE : Maximum number of descriptors reached.";
		case EWOULDBLOCK:        return "EWOULDBLOCK : A non-blocking socket operation could not be completed immediately.";
		case EINPROGRESS:        return "EINPROGRESS : A blocking operation is currently executing.";
		case EALREADY:           return "EALREADY : Non-blocking socket already have an operation in progress.";
		case ENOTSOCK:           return "ENOTSOCK : Not a socket.";
		case EDESTADDRREQ:       return "EDESTADDRREQ : Address omitted from an operation on a socket.";
		case EMSGSIZE:           return "EMSGSIZE : Datagram too large.";
		case EPROTOTYPE:         return "EPROTOTYPE : Bad protocol.";
		case ENOPROTOOPT:        return "ENOPROTOOPT : Invalid or unknown option or level.";
		case EAFNOSUPPORT:       return "EAFNOSUPPORT : Address incompatble with protocol.";
		case EADDRINUSE:         return "EADDRINUSE : Address or port already in use.";
		case EADDRNOTAVAIL:      return "EADDRNOTAVAIL : Requested address invalid in the context.";
		case ENETDOWN:           return "ENETDOWN : Network down.";
		case ESHUTDOWN:          return "ESHUTDOWN : A request to send or receive data was disallowed because the socket had already been shut down in that direction with a previous shutdown call.";
		case ETIMEDOUT:          return "ETIMEDOUT : Connecttion attempt timedout.";
		case ECONNREFUSED:       return "ECONNREFUSED : Connection refused.";
		case ENAMETOOLONG:       return "ENAMETOOLONG : A name or address was too long";
		case EHOSTDOWN:          return "EHOSTDOWN : Host is down.";

		default:                 return "NO ERROR";
	}
#endif
}


/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
//
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////
//
SMTPClient::Recipient::Recipient()
{
	m_pName    = NULL;
	m_pAddress = NULL;
	m_pRegForm = NULL;
}

/////////////////////////////////////////////////////////////////////////////
//
SMTPClient::Recipient::Recipient(const char *pName, const char *pAddress)
{
	m_pRegForm = NULL;

	if(pName)
	{
		m_pName = new char [strlen(pName) + 1];
		if(m_pName)
		{
			strcpy(m_pName, pName);
		}
	}

	if(pAddress)
	{
		m_pAddress = new char [strlen(pAddress) + 1];
		if(m_pAddress)
		{
			strcpy(m_pAddress, pAddress);
		}
	}
}

/////////////////////////////////////////////////////////////////////////////
//
SMTPClient::Recipient::~Recipient()
{
	if(m_pName)
	{
		delete [] m_pName;
		m_pName = NULL;
	}

	if(m_pAddress)
	{
		delete [] m_pAddress;
		m_pAddress = NULL;
	}

	if(m_pRegForm)
	{
		delete [] m_pRegForm;
		m_pRegForm = NULL;
	}
}

/////////////////////////////////////////////////////////////////////////////
//
void SMTPClient::Recipient::Set(const char *pName, const char *pAddress)
{
	if(pName)
	{
		if(m_pName)
		{
			delete [] m_pName;
			m_pName = NULL;
		}

		m_pName = new char [strlen(pName) + 1];
		if(m_pName)
		{
			strcpy(m_pName, pName);
		}
	}

	if(pAddress)
	{
		if(m_pAddress)
		{
			delete [] m_pAddress;
			m_pAddress = NULL;
		}

		m_pAddress = new char [strlen(pAddress) + 1];
		if(m_pAddress)
		{
			strcpy(m_pAddress, pAddress);
		}
	}
}

/////////////////////////////////////////////////////////////////////////////
//
char *SMTPClient::Recipient::GetRegularFormat()
{
	char *pName = " ";
	char *pAddr = " ";

	if(m_pName)
		pName = m_pName;

	if(m_pAddress)
		pAddr = m_pAddress;

	if(m_pRegForm)
	{
		delete [] m_pRegForm;
		m_pRegForm = NULL;
	}

	m_pRegForm = new char[strlen(pName) + strlen(pAddr) + 3 + 1];
	if(m_pRegForm)
	{
		sprintf(m_pRegForm, "%s <%s>", pName, pAddr);
	}
	return (m_pRegForm);
}

/////////////////////////////////////////////////////////////////////////////
//
void SMTPClient::Recipient::Clear()
{
	if(m_pName)
	{
		delete [] m_pName;
		m_pName = NULL;
	}

	if(m_pAddress)
	{
		delete [] m_pAddress;
		m_pAddress = NULL;
	}

	if(m_pRegForm)
	{
		delete [] m_pRegForm;
		m_pRegForm = NULL;
	}
}

/////////////////////////////////////////////////////////////////////////////
//
SMTPClient::Attachment::Attachment()
{
}

/////////////////////////////////////////////////////////////////////////////
//
SMTPClient::Attachment::~Attachment()
{
	if(m_pBuffer)
	{
		delete [] m_pBuffer;
		m_pBuffer = NULL;
	}
}
