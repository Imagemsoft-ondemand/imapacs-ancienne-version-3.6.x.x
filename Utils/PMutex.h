/////////////////////////////////////////////////////////////////////////////
//	PMutex.h
/////////////////////////////////////////////////////////////////////////////

#if defined(WIN32)
	#include <Windows.h>
#else
	#include <sys/stat.h>
	#include <sys/types.h>
	#include <pthread.h>
#endif

#if!defined(__PMUTEX_H__)
#define     __PMUTEX_H__

/////////////////////////////////////////////////////////////////////////////
//
namespace util
{

#define	PMUTEX_VERSION	"1.0.0.0"

/////////////////////////////////////////////////////////////////////////////
//
class PMutex
{
public:
	PMutex();
	virtual ~PMutex();

	char *GetVersion() const                        {return PMUTEX_VERSION;}

	bool Create();

	bool Lock();
	bool Unlock();

	bool Destroy();

#if defined(WIN32)
	void SetMutexHandle(HANDLE hMutex)				{m_hMutex = hMutex;}
	HANDLE GetMutexHandle()                         {return m_hMutex;}
#else
	void SetMutexHandle(pthread_mutex_t hMutex)     {m_hMutex = hMutex;}
	pthread_mutex_t GetutexHandle()					{return m_hMutex;}
#endif

private:

#if defined(WIN32)
	HANDLE          m_hMutex;
#else
	pthread_mutex_t m_hMutex;
#endif
};

}	//	End namespace tool.

#endif	//	__PMUTEX_H__
