/////////////////////////////////////////////////////////////////////////////
//	os.h
/////////////////////////////////////////////////////////////////////////////

#if defined(WIN32)
#include <Windows.h>
#include <stdio.h>
#include <direct.h>
#else
#include <sys/stat.h>
#include <sys/types.h>
#include <stdio.h>
//#include <io.h>
#include <unistd.h>
#include <strings.h>
#endif

#include "StringBuffer.h"

#if !defined(__OS_H_INCLUDED__)
#define      __OS_H_INCLUDED__

/////////////////////////////////////////////////////////////////////////////
//
namespace imapacs
{
	/////////////////////////////////////////////////////////////////////////
	//
	class os
	{
	public:

		static int MkDir(const char *pDir, int iMode = 0777);
		static bool FileExists(const char *pFile);
		static bool FileMove(const char *pSource, const char *pDestination);
		static bool CreateFullDirectory(const char *pPath, int iMode = 0777);
		static bool RemoveFile(const char *pFile);
		static void RemoveAccents(char *pSrc);
	};
}

#endif	//	__OS_H_INCLUDED__
