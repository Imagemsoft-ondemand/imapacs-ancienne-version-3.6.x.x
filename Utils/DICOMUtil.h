/////////////////////////////////////////////////////////////////////////////
//	DICOMUtil.h
/////////////////////////////////////////////////////////////////////////////

#include "DicomHeaders.h"
#include "../DataStructures.h"
#include "log.h"
#include "os.h"
#include "Mailer.h"

#if !defined(__DICOMUTIL_H_INCLUDED__)
#define      __DICOMUTIL_H_INCLUDED__

/////////////////////////////////////////////////////////////////////////////
//
namespace imapacs
{
	/////////////////////////////////////////////////////////////////////////
	//
	class DICOMUtil
	{
	public:
		static void ClearDataset(DcmDataset **pDataset)
		{
			if(pDataset && (*pDataset))
			{
				delete (*pDataset);
				(*pDataset) = NULL;
			}
		}

		static void ClearNetwork(T_ASC_Network **pNetwork)
		{
			if(pNetwork && (*pNetwork))
			{
				ASC_dropNetwork(pNetwork);
				(*pNetwork) = NULL;
			}
		}

		static void ClearAssociation(T_ASC_Association **pAssociation)
		{
			if(pAssociation && (*pAssociation))
			{
				ASC_dropAssociation((*pAssociation));
				ASC_destroyAssociation(pAssociation);

				(*pAssociation) = NULL;
			}
		}

		static void ClearDicomImage(DicomImage **pImage)
		{
			if(pImage && (*pImage))
			{
				delete (*pImage);
				(*pImage) = NULL;
			}
		}

		static bool IsImageType(const char *pSOPClassUID);
		static bool IsImageType(DcmDataset *pDataset);

		/////////////////////////////////////////////////////////////////////
		//	To check if a dicom file is valid and can be opened.
		static bool CheckDICOMFile(const char *pFile, Log *pLogger = NULL, Mailer *pMailer = NULL)
		{
			OFCondition cond = EC_Normal;
			DcmFileFormat File;

			if(!pFile)
				return false;

			cond = File.loadFile(pFile);
			if(cond.bad())
			{
				if(pLogger)
				{
					pLogger->LogMsg("Dicom file not valid");
					pLogger->LogMsg("ERROR : %s", cond.text());
				}

				if(pMailer)
				{
					pMailer->AddLine("Dicom file not valid");
					pMailer->AddLine("%s", cond.text());
				}
				return false;
			}
			return true;
		}

		/////////////////////////////////////////////////////////////////////
		//
		static void GetTagFromDataset(DcmItem *pDataset, 
			                          const DcmTagKey &Tag, 
								      TagData *pTagData)
		{
			const char *pValue = NULL;
			OFCondition cond   = EC_Normal;

			cond = pDataset->findAndGetString(Tag, pValue);
			if(cond.good())
			{
				pTagData->bReturn = true;
				if(pValue)
				{
					strcpy(pTagData->Data, pValue);
				}
			}
			else
			{
				pTagData->bReturn = false;
			}
		}

		/////////////////////////////////////////////////////////////////////
		//
		static void GetTagFromDataset(DcmItem *pDataset,
								      const DcmTagKey &Tag,
									  char *pTagValue)
		{
			const char *pValue = NULL;
			OFCondition cond   = EC_Normal;

			cond = pDataset->findAndGetString(Tag, pValue);
			if(cond.good() && pValue)
			{
				strcpy(pTagValue, pValue);
			}
		}

		/////////////////////////////////////////////////////////////////////
		//
		static void UpdateTagInDataset(DcmItem *pDataset,
									   const DcmTagKey &Tag,
									   TagData *pTagData,
									   bool bRemoveAccents = false)
		{
			if(pTagData)
			{
				if(bRemoveAccents)
				{
					os::RemoveAccents(pTagData->Data);
				}
				pDataset->putAndInsertString(Tag, pTagData->Data);
			}
			else
			{
				pDataset->putAndInsertString(Tag, NULL);
			}
		}

		/////////////////////////////////////////////////////////////////////
		//
		static void UpdateTagInDataset(DcmItem *pDataset,
									   const DcmTagKey &Tag,
									   char *pValue,
									   bool bRemoveAccents = false)
		{
			if(pValue)
			{
				if(bRemoveAccents)
				{
					os::RemoveAccents(pValue);
				}
				pDataset->putAndInsertString(Tag, pValue);
			}
			else
			{
				pDataset->putAndInsertString(Tag, NULL);
			}
		}

		static bool isStorageAbstractSyntax(const char* abstractSyntax);

	private:

		static const char* storageUid[];

	};
}

#endif	//	__DICOMUTIL_H_INCLUDED__
