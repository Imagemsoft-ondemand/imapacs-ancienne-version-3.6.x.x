/////////////////////////////////////////////////////////////////////////////
//	PatchModule.h
/////////////////////////////////////////////////////////////////////////////

#include "../PacsConfig.h"
#include "DicomHeaders.h"
#include "log.h"
#include "Mailer.h"
#include "OCIDatabase.h"
#include "Str.h"

#include "DICOMUtil.h" // [Ref:0000577]

#if !defined(__PATCHMODULE_H_INCLUDED__)
#define      __PATCHMODULE_H_INCLUDED__

namespace imapacs
{

	/////////////////////////////////////////////////////////////////////////
	//	Class used to gather all the possible parameters for a patch
	//	method call. This allows parameters to be added if needed without
	//	the need to change the definition of all the patch methods since
	//	methods will just ignore the parameters they don't need.
	/////////////////////////////////////////////////////////////////////////
	class PatchParams
	{
	public:

		PatchParams()
		{
			Reset();
		}

		void Reset()
		{
			sPatchId[0]       = 0x00;
			sSection[0]       = 0x00;
			pCallingAeTitle   = 0;
			pPkgName          = 0;
			pImplementationId = 0;
			pPatientId        = 0;
			pObjectLocation   = 0;
			pStudyDate        = 0;
			pAccessionNumber  = 0;
			pStudyInstanceUID = 0;
			pCfg              = 0;
			pDataset          = 0;
			pDb               = 0;
			pModalityLogger   = 0;
			pMailer           = 0;
			pObjectInfo       = 0;
			pCurrentReq       = 0;
		}

		char             sPatchId[17];
		char             sSection[65];
		const char      *pCallingAeTitle;
		const char      *pPkgName;
		const char      *pImplementationId;
		const char      *pPatientId;
		const char      *pObjectLocation;
		char            *pStudyDate;
		char            *pAccessionNumber;
		char            *pStudyInstanceUID;

		PacsConfig      *pCfg;
		DcmDataset*         pDataset;
		tool::COCIDatabase* pDb;
		Log             *pModalityLogger;
		Mailer          *pMailer;
		ObjectData      *pObjectInfo;

		T_DIMSE_Message *pCurrentReq;
	};

	/////////////////////////////////////////////////////////////////////////
	//	This class is used to gather all the patches made for various 
	//	modalities that need special tweaks. All patches are implemented as
	//	methods of this class. The only purpose of this class is to improve
	//	the readability ofthe code and facilitate the management of the
	//	patches.
	/////////////////////////////////////////////////////////////////////////
	class PatchModule
	{
	public:
		PatchModule()
		{}
		virtual ~PatchModule()
		{}

		/////////////////////////////////////////////////////////////////////
		//	This method will determine which patch to apply (if any). It
		//	needs, at least, pDb, pCallingAeTitle and sPatchId to be
		//	filled since it uses these values to determine the patch to apply.
		//	The method returns true if a patch was applied, false otherwise.
		bool Dispatcher(PatchParams *pParams);

private:

		/////////////////////////////////////////////////////////////////////
		//	00001
		bool SwissRayWorklistPatch(PatchParams *pParams);

		/////////////////////////////////////////////////////////////////////
		//	00002
		bool SwissRayStorePatch(PatchParams *pParams);

		/////////////////////////////////////////////////////////////////////
		//	00003
		bool ImaGatorStudyUIDPatch(PatchParams *pParams);

		/////////////////////////////////////////////////////////////////////
		//	00004
		bool AgfaCR85Patch(PatchParams *pParams);

		/////////////////////////////////////////////////////////////////////
		//	00005
		bool ATL_HDI3000Patch(PatchParams *pParams);

		/////////////////////////////////////////////////////////////////////
		//	00006
		bool DIDILBPatch(PatchParams *pParams);

		/////////////////////////////////////////////////////////////////////
		//	00007
		bool CTJonqRetreivePatch(PatchParams *pParams);

		// [Ref:0000577] - D�but
		/////////////////////////////////////////////////////////////////////
		//	00008
		bool ImaGatorSOPInstanceRequestAdjust(PatchParams *pParams);
		// [Ref:0000577] - Fin

		// [Ref:0000578] - D�but

		/////////////////////////////////////////////////////////////////////
		//	00009
		bool APLIOStorePatch(PatchParams *pParams);
		// [Ref:0000578] - Fin

		//	[Ref:0000636] - D�but
		/////////////////////////////////////////////////////////////////////
		//	00010
		bool PresentationStateLittleEndianHeader(PatchParams *pParams);
		//	[Ref:0000636] - Fin

		//  [Ref:0001811] - D�but
		/////////////////////////////////////////////////////////////////////
		//	00012
		bool HIRISPatch(PatchParams *pParams);
		//  [Ref:0001811] - Fin
	};
}

#endif	//	__PATCHMODULE_H_INCLUDED__
