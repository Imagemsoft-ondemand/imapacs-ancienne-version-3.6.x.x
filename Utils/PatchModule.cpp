/////////////////////////////////////////////////////////////////////////////
//	PatchModule.cpp
/////////////////////////////////////////////////////////////////////////////

#ifdef WIN32
#include "../stdafx.h"
#endif

#include "PatchModule.h"

#ifdef WIN32
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
#endif

namespace imapacs
{

	/////////////////////////////////////////////////////////////////////////////
	//	Dispatch method calls for the appropriate patch.
	bool PatchModule::Dispatcher(PatchParams *pParams)
	{
		bool bRetVal        = false;
		char sApplyPatch[5] = {0};
		int iPatchId        = 0;
		HANDLE hCursor      = 0;
		StringBuffer sql;

		if(pParams->pCallingAeTitle && pParams->pDb && pParams->pModalityLogger && pParams->pMailer && pParams->pCfg)
		{
			hCursor = pParams->pDb->GetCursor();
			if(hCursor)
			{
				sql.Format("SELECT %s.MUST_APPLY_PATCH('%s', '%s') FROM DUAL",
					       pParams->pCfg->GetPkgName(),
					       pParams->sPatchId,
						   pParams->pCallingAeTitle);

				if(pParams->pDb->SQLExec(hCursor, (char *)sql, "%s", sApplyPatch))
				{
					if(sApplyPatch[0] == '1')	
					{
						iPatchId = atoi(pParams->sPatchId);
						switch(iPatchId)
						{
							case 1:	//	Swissray worklist patch.
							{
								pParams->pModalityLogger->LogMsg("Applying patch for Swissray worklist client.");
								pParams->pModalityLogger->LogMsg("Patch id : 00001");

								pParams->pMailer->AddLine("Applying patch for Swissray worklist client.");
								pParams->pMailer->AddLine("Patch id : 00001");

								bRetVal = SwissRayWorklistPatch(pParams);
							}
							break;

							case 2:	//	Swissray store patch (undo the patch 00001).
							{
								pParams->pModalityLogger->LogMsg("Applying patch for Swissray store client.");
								pParams->pModalityLogger->LogMsg("Patch id : 00002");

								pParams->pMailer->AddLine("Applying patch for Swissray store client.");
								pParams->pMailer->AddLine("Patch id : 00002");

								bRetVal = SwissRayStorePatch(pParams);
							}
							break;

							case 3:	//	Imagator Study UID patch.
							{
								pParams->pModalityLogger->LogMsg("Applying patch for Imagator store client.");
								pParams->pModalityLogger->LogMsg("Patch id : 00003");

								pParams->pMailer->AddLine("Applying patch for Imagator store client.");
								pParams->pMailer->AddLine("Patch id : 00003");

								bRetVal = ImaGatorStudyUIDPatch(pParams);
							}
							break;

							case 4:	//	Agfa CR 85 patch.
							{
								pParams->pModalityLogger->LogMsg("Applying patch for Agfa CR85 store client.");
								pParams->pModalityLogger->LogMsg("Patch id : 00004");

								pParams->pMailer->AddLine("Applying patch for Agfa CR85 store client.");
								pParams->pMailer->AddLine("Patch id : 00004");

								bRetVal = AgfaCR85Patch(pParams);
							}
							break;

							case 5:	//	HDI 3000 patch.
							{
								pParams->pModalityLogger->LogMsg("Applying patch for HDI 3000 store client.");
								pParams->pModalityLogger->LogMsg("Patch id : 00005");

								pParams->pMailer->AddLine("Applying patch for HDI 3000 store client.");
								pParams->pMailer->AddLine("Patch id : 00005");

								bRetVal = ATL_HDI3000Patch(pParams);
							}
							break;

							case 6:	//	DIDILB patch.
							{
								pParams->pModalityLogger->LogMsg("Applying patch for DIDILB store client.");
								pParams->pModalityLogger->LogMsg("Patch id : 00006");

								pParams->pMailer->AddLine("Applying patch for DIDILB store client.");
								pParams->pMailer->AddLine("Patch id : 00006");

								bRetVal = DIDILBPatch(pParams);
							}
							break;

							case 7:	//	CTJONQ reformat retrieve patch.
							{
								pParams->pModalityLogger->LogMsg("Applying patch for CTJONQ retrieve client.");
								pParams->pModalityLogger->LogMsg("Patch id : 00007");

								pParams->pMailer->AddLine("Applying patch for CTJONQ retrieve client.");
								pParams->pMailer->AddLine("Patch id : 00007");

								bRetVal = CTJonqRetreivePatch(pParams);
							}
							break;

							case 8:	//	Imagator SOP Class patch.
							{
								pParams->pModalityLogger->LogMsg("Applying patch for Imagator SOPInstance store client.");
								pParams->pModalityLogger->LogMsg("Patch id : 00008");

								pParams->pMailer->AddLine("Applying patch for Imagator SOPInstance store client.");
								pParams->pMailer->AddLine("Patch id : 00008");

								bRetVal = ImaGatorSOPInstanceRequestAdjust(pParams);
							}
							break;

							case 9:	//	APLIO store patch.
							{
								pParams->pModalityLogger->LogMsg("Applying patch for APLIO store client.");
								pParams->pModalityLogger->LogMsg("Patch id : 00009");

								pParams->pMailer->AddLine("Applying patch for APLIO store client.");
								pParams->pMailer->AddLine("Patch id : 00009");

								bRetVal = APLIOStorePatch(pParams);
							}
							break;

							case 10:	//	Presentation states implicit header patch.
							{
								char sSOPClassUID[65] = {0};
								DICOMUtil::GetTagFromDataset(pParams->pDataset, DCM_SOPClassUID, sSOPClassUID);

								if(strcmp(UID_GrayscaleSoftcopyPresentationStateStorage, sSOPClassUID) == 0)
								{
									pParams->pModalityLogger->LogMsg("Applying patch for Presentation states.");
									pParams->pModalityLogger->LogMsg("Patch id : 00010");

									pParams->pMailer->AddLine("Applying patch for Presentation states.");
									pParams->pMailer->AddLine("Patch id : 00010");

									bRetVal = PresentationStateLittleEndianHeader(pParams);
								}
							}

							case 12:
							{
								pParams->pModalityLogger->LogMsg("Applying patch for HIRIS store client.");
								pParams->pModalityLogger->LogMsg("Patch id : 00012");

								pParams->pMailer->AddLine("Applying patch for HIRIS store client.");
								pParams->pMailer->AddLine("Patch id : 00012");

								bRetVal = HIRISPatch(pParams);
							}
							break;
						}
					}
				}
				else
				{
					if(pParams->pDb->GetLastOciErrorCode(hCursor) > 0)
					{
						pParams->pModalityLogger->LogMsg("ERROR : %s", pParams->pDb->GetLastOciErrorText(hCursor));
						pParams->pModalityLogger->LogMsg("SQL   : %s", (char *)sql);
						pParams->pModalityLogger->LogMsg("        Unable to find patch to apply (if any).");

						pParams->pMailer->AddLine("ERROR : %s", pParams->pDb->GetLastOciErrorText(hCursor));
						pParams->pMailer->AddLine("SQL   : %s", (char *)sql);
						pParams->pMailer->AddLine("        Unable to find patch to apply (if any).");
					}
				}
				pParams->pDb->DeleteCursor(hCursor);
				hCursor = 0;
			}
		}
		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////////
	//	Manufacturer	:	Swissray.
	//	Model			:	DDR Multi System.
	//	Description		:	Send the study description as well as the RAMQ
	//						code into the referring physician field because
	//						the DDR GUI displays only this value which is
	//						of no use to the operator.
	//	Patch id		:	00001
	bool PatchModule::SwissRayWorklistPatch(PatchParams *pParams)
	{
		bool bRetVal              = true;
		const char *pDescription  = NULL;
		const char *pRefPhysician = NULL;
		OFCondition cond          = EC_Normal;

		cond = pParams->pDataset->findAndGetString(DCM_RequestedProcedureDescription, pDescription);
		if(cond.good() && pDescription)
		{
			pParams->pDataset->putAndInsertString(DCM_ReferringPhysicianName, pDescription);

			pParams->pModalityLogger->LogMsg("We put the description into the refering physician tag for swissray modality worklist client.");
			pParams->pMailer->AddLine("We put the description into the refering physician tag for swissray modality worklist client.");
		}
		pParams->pModalityLogger->LogMsg("Patch for Swissray DDR Multi System applied.");

		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////////
	//	Manufacturer	:	Swissray.
	//	Model			:	DDR Multi System.
	//	Description		:	Remove the referring physician from the dataset
	//						since it was filled with the requested procedure
	//						description by the worklist patch for reasons
	//						related to SwissRay GUI.
	//	Patch id		:	00002
	bool PatchModule::SwissRayStorePatch(PatchParams *pParams)
	{
		bool bRetVal                 = false;
		OFCondition cond             = EC_Normal;
		char sReferringPhysician[65] = {0};
		const char *pStudyUID        = NULL;
		HANDLE hCursor               = NULL;
		StringBuffer sql;

		hCursor = pParams->pDb->GetCursor();
		if(hCursor)
		{
			cond = pParams->pDataset->findAndGetString(DCM_StudyInstanceUID, pStudyUID);
			if(cond.good() && pStudyUID)
			{
				sql.Format("SELECT STU_REF_PHYSICIAN FROM STUDY WHERE STU_INSTANCE_UID = '%s'", pStudyUID);

				if(pParams->pDb->SQLExec(hCursor, (char *)sql, "%s", sReferringPhysician))
				{
					cond = pParams->pDataset->putAndInsertString(DCM_ReferringPhysicianName, sReferringPhysician);
					if(cond.good())
					{
						bRetVal = true;
					}
					else
					{
						pParams->pModalityLogger->LogMsg("ERRROR : Failed to set referring physician into dataset.");
						pParams->pMailer->AddLine("ERRROR : Failed to set referring physician into dataset.");
					}
				}
				else
				{
					pParams->pModalityLogger->LogMsg("ERROR : %s", pParams->pDb->GetLastOciErrorText(hCursor));
					pParams->pModalityLogger->LogMsg("SQL   : %s", (char *)sql);

					pParams->pMailer->AddLine("ERROR : %s", pParams->pDb->GetLastOciErrorText(hCursor));
					pParams->pMailer->AddLine("SQL   : %s", (char *)sql);
				}
			}
			else
			{
				pParams->pModalityLogger->LogMsg("ERROR : Study instance uid not found in received dcm.");
				pParams->pMailer->AddLine("ERROR : Study instance uid not found in received dcm.");
			}
			pParams->pDb->DeleteCursor(hCursor);
			hCursor = NULL;
		}
		else
		{
			pParams->pModalityLogger->LogMsg("ERROR : Failed to get cursor in PatchModule::SwissRayStorePatch");
			pParams->pMailer->AddLine("ERROR : Failed to get cursor in PatchModule::SwissRayStorePatch");
		}
		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////////
	//	Manufacturer	:	Imagem.
	//	Model			:	ImaGator.
	//	Description		:	Add study date and study time when they are missing.
	//						When a study is cancelled using ImaGator and a
	//						modality's worklist is using it, it is put to
	//						VERIFIED but without STU_DATE and STU_TIME.
	//	Patch id		:	00003
	bool PatchModule::ImaGatorStudyUIDPatch(PatchParams *pParams)
	{
		bool bRetVal   = false;
		HANDLE hCursor = NULL;
		StringBuffer sql;

		if(pParams->pStudyInstanceUID && 
		   pParams->pStudyInstanceUID[0] && 
		   pParams->pPkgName && 
		   pParams->pPkgName[0] && 
		   pParams->pStudyDate)
		{
			if(pParams->pStudyDate[0] == 0x00)
			{
				hCursor = pParams->pDb->GetCursor();
				if(hCursor)
				{
					sql = "SELECT TO_CHAR(SYSDATE,'YYYYMMDD') FROM DUAL";
					if(pParams->pDb->SQLExec(hCursor, (char *)sql, "%s", pParams->pStudyDate))
					{
						sql.Format("CALL %s.UPDATE_STUDY_DATE('%s','%s')", pParams->pPkgName,
																		   pParams->pStudyInstanceUID,
																		   pParams->pStudyDate);
						if(pParams->pDb->SQLExec(hCursor, (char *)sql))
						{
							bRetVal = true;
						}
						else
						{
							pParams->pModalityLogger->LogMsg("ERROR : %s", pParams->pDb->GetLastOciErrorText(hCursor));	
							pParams->pModalityLogger->LogMsg("SQL   :  %s", (char *)sql);

							pParams->pMailer->AddLine("ERROR : %s", pParams->pDb->GetLastOciErrorText(hCursor));
							pParams->pMailer->AddLine("SQL   :  %s", (char *)sql);
						}
					}
					else
					{
						pParams->pModalityLogger->LogMsg("ERROR : %s", pParams->pDb->GetLastOciErrorText(hCursor));
						pParams->pModalityLogger->LogMsg("SQL   :  %s", (char *)sql);

						pParams->pMailer->AddLine("ERROR : %s", pParams->pDb->GetLastOciErrorText(hCursor));
						pParams->pMailer->AddLine("SQL   :  %s", (char *)sql);
					}
					pParams->pDb->DeleteCursor(hCursor);
					hCursor = NULL;
				}
			}
		}
		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////////
	//	Manufacturer	:	Agfa
	//	Model			:	CR 85
	//	Description		:	Doesn't use the study description we send. and put it
	//						into the requested procedure description. We put it
	//						back in the study description.
	//	Patch id		:	00004
	bool PatchModule::AgfaCR85Patch(PatchParams *pParams)
	{
		bool bRetVal       = false;
		const char *pValue = NULL;
		OFCondition cond   = EC_Normal;

		pParams->pModalityLogger->LogMsg("Agfa CR 85 modality");
		pParams->pModalityLogger->LogMsg("Applying patch ...");

		pParams->pMailer->AddLine("Agfa CR 85 modality");
		pParams->pMailer->AddLine("Applying patch ...");

		cond = pParams->pDataset->findAndGetString(DCM_RequestedProcedureDescription, pValue);
		if(cond.good() && pValue)
		{
			pParams->pDataset->putAndInsertString(DCM_StudyDescription, pValue);
		}
		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////////
	//	Manufacturer	:	Philips
	//	Model			:	ATL HDI 3000.
	//	Description		:	This modality pass us the right accession number
	//						but not the right study instance uid.
	//	Patch id		:	00005
	bool PatchModule::ATL_HDI3000Patch(PatchParams *pParams)
	{
		bool bRetVal                  = false;
		HANDLE hCursor                = NULL;
		char sTmpPatId[65]            = {0};
		char sTmpStudyInstanceUID[65] = {0};
		char sTmpStudyDate[33]        = {0};
		char sTmpStatus[17]           = {0};
		StringBuffer sql;

		//	Is it one of our study ?
//[ref:0000652] -d�but-
//		if(Str::StringHasSubstring(pParams->pStudyInstanceUID, pParams->pImplementationId))
		if(!Str::StringHasSubstring(pParams->pStudyInstanceUID, pParams->pImplementationId))
//[ref:0000652] -fin-
		{
			//	It's not one of our study, replace the study instance uid
			//	by the one found in the db for the given accession number.
			hCursor = pParams->pDb->GetCursor();
			if(hCursor)
			{
				sql.Format("SELECT STU_PAT_ID, STU_INSTANCE_UID, STU_DATE, STU_STATUS " \
						   "FROM STUDY WHERE STU_ACCESSION_NUMBER = '%s'", pParams->pAccessionNumber);
//[ref:0000652] -d�but-
//				if(pParams->pDb->SQLExec(hCursor, "%s%s%s%s", sTmpPatId, sTmpStudyInstanceUID, sTmpStudyDate, sTmpStatus))
				if(pParams->pDb->SQLExec(hCursor, (char *)sql, "%s%s%s%s", sTmpPatId, sTmpStudyInstanceUID, sTmpStudyDate, sTmpStatus))
//[ref:0000652] -fin-
				{
					if(strcmp(sTmpPatId, pParams->pPatientId) == 0)
					{
						//	This is the right patient.
						pParams->pModalityLogger->LogMsg("Study %s changed for %s", pParams->pStudyInstanceUID, sTmpStudyInstanceUID);
						pParams->pMailer->AddLine("Study %s changed for %s", pParams->pStudyInstanceUID, sTmpStudyInstanceUID);

						strcpy(pParams->pStudyInstanceUID, sTmpStudyInstanceUID);

						pParams->pDataset->putAndInsertString(DCM_StudyInstanceUID, pParams->pStudyInstanceUID);

						bRetVal = true;
					}
				}
				else
				{
					if(pParams->pDb->GetLastOciErrorCode(hCursor) == 0)
					{
						pParams->pModalityLogger->LogMsg("WARNING : No such study for accession number : %s", pParams->pAccessionNumber);
						pParams->pMailer->AddLine("WARNING : No such study for accession number : %s", pParams->pAccessionNumber);
					}
					else
					{
						pParams->pModalityLogger->LogMsg("ERROR : %s", pParams->pDb->GetLastOciErrorText(hCursor));
						pParams->pModalityLogger->LogMsg("SQL   :  %s", (char *)sql);

						pParams->pMailer->AddLine("ERROR : %s", pParams->pDb->GetLastOciErrorText(hCursor));
						pParams->pMailer->AddLine("SQL   :  %s", (char *)sql);
					}
				}
				pParams->pDb->DeleteCursor(hCursor);
				hCursor = NULL;
			}
		}
		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////////
	//	Manufacturer	:	Philips
	//	Model			:	DIDILB
	//	Description		:	Opies the code meaning from the Procedure code sequence
	//						into the study description.
	//	Patch id		:	00006
	bool PatchModule::DIDILBPatch(PatchParams *pParams)
	{
		bool bRetVal          = false;
		const char *pValue    = NULL;
		DcmElement *pSequence = NULL;
		DcmItem *pDi		  = NULL;
		int iElemCount        = 0;
		OFCondition cond      = EC_Normal;

		//pDataset->findAndDeleteElement(DcmTag(0x0089,0x0010), OFTrue);
		//pDataset->findAndDeleteElement(DcmTag(0x0089,0x1010), OFTrue);

		cond = pParams->pDataset->findAndGetElement(DCM_ProcedureCodeSequence, pSequence);
		if(cond.good())
		{
			iElemCount = (int)((DcmSequenceOfItems*)pSequence)->card();
			if(iElemCount > 0)
			{
				pDi = ((DcmSequenceOfItems*)pSequence)->getItem(0);
				if(pDi)
				{
					cond = pDi->findAndGetString(DCM_CodeMeaning, pValue);
					if(cond.good())
					{
						pParams->pDataset->putAndInsertString(DCM_StudyDescription, pValue);
					}
				}
			}
		}
		return bRetVal;
	}

	/////////////////////////////////////////////////////////////////////////////
	//	Manufacturer	:	General Electric
	//	Model			:	CT (i don't know the model).
	//	Description		:	Transform CT images that unsigned into images that
	//						are signed. If the image is a reformated image, it
	//						will also set the rescale intercept to -1024.
	//						This is to correct a bug due to DIMPL lousy
	//						compressor as well as GE's picky implementation.
	//	Patch id		:	00007
	bool PatchModule::CTJonqRetreivePatch(PatchParams *pParams)
	{
		bool bRetVal                 = false;
		const char *pModality        = NULL;
		const char *pImageType       = NULL;
		const char *pValue           = NULL;
		short iRescale               = 0x0000;
		unsigned short iPixelRep     = 0x0000;
		unsigned short iBits         = 0x0000;
		unsigned short iWidth        = 0x0000;
		unsigned short iHeight       = 0x0000;
		unsigned short iBitAllocated = 0x0000;
		int iSize                    = 0;
		const unsigned short *pUS    = NULL;	//	Unsigned pixel data.
		short *pS                    = NULL;	//	Signed pixel data.
		E_TransferSyntax original    = EXS_Unknown;
		char c[2]                    = {0};
		int idx                      = 0;
		int tmp                      = 0;
		unsigned short pixelswp      = 0x0000;
		short result                 = 0x0000;

		pParams->pDataset->findAndGetString(DCM_Modality, pModality);
		pParams->pDataset->findAndGetString(DCM_ImageType, pImageType);
		pParams->pDataset->findAndGetUint16(DCM_PixelRepresentation, iPixelRep);

		if(pModality && (pModality[0] == 'C') && (pModality[1] == 'T') && (iPixelRep == 0))
		{
			//	Remove Smalest and Largest pixel value tags (DIMPL).
			pParams->pDataset->findAndDeleteElement(DCM_SmallestImagePixelValue, OFTrue);
			pParams->pDataset->findAndDeleteElement(DCM_LargestImagePixelValue, OFTrue);

			//	Get image parmeters.
			pParams->pDataset->findAndGetUint16(DCM_Rows, iHeight);
			pParams->pDataset->findAndGetUint16(DCM_Columns, iWidth);
			pParams->pDataset->findAndGetUint16(DCM_BitsAllocated, iBitAllocated);

			pParams->pDataset->findAndGetString(DCM_RescaleIntercept, pValue);
			if(pValue)
			{
				iRescale = atoi(pValue);
			}

			iSize = (int)(iWidth * iHeight);

			//	Udate pixel rep so it is signed now.
			pParams->pDataset->putAndInsertUint16(DCM_PixelRepresentation, 0x0001);

			pParams->pDataset->findAndGetUint16Array(DCM_PixelData, pUS);

			//	Convert unsigned pixels into signed pixels.
			pS = new short[iSize];
			if(pS)
			{
#ifdef WIN32
				while(idx < iSize)
				{
					memcpy(c,&pUS[idx],2);

					pixelswp = c[1]*256+(c[0]& 0xFF);

					memcpy(&result,&pixelswp,2);
					tmp = result + iRescale;
					pS[idx] = (short)tmp;

					memcpy(c,&pS[idx],2);
					pS[idx]=c[1]*256+(c[0]& 0xFF);

					++idx;
				}
#else
				//	We suppose that, if we are not on Windows, we are on Solaris
				//	with SPARC processor (in our case). That means we have to
				//	change the byte order when computing the signed value.
				while(idx < iSize)
				{
					memcpy(c,&pUS[idx],2);

					pixelswp = c[0]*256+(c[1]& 0xFF);

					memcpy(&result,&pixelswp,2);
					tmp = result + iRescale;
					pS[idx] = (short)tmp;

					memcpy(c,&pS[idx],2);
					pS[idx]=c[0]*256+(c[1]& 0xFF);

					++idx;
				}
#endif
				//	Update pixel data.
				pParams->pDataset->putAndInsertUint16Array(DCM_PixelData,(unsigned short *)pS, iSize);

				//	If the image is refoemated, set rescale
				//	intercept to -1024.
				if(pImageType && strstr(pImageType, "REFORMATTED"))
				{
					pValue = "-1024";
				}
				else
				{
					pValue = "0";
				}
				pParams->pDataset->putAndInsertString(DCM_RescaleIntercept, pValue);

				delete [] pS;
				pS = NULL;
			}
		}
		return bRetVal;
	}

	// [Ref:0000577] - D�but

	/////////////////////////////////////////////////////////////////////
	//	Description	:	Copies the SOP class UID into the request because
	//					it wasn't there.
	//	Patch id	:	00008
	bool PatchModule::ImaGatorSOPInstanceRequestAdjust(PatchParams *pParams)
	{
		strcpy(pParams->pCurrentReq->msg.CStoreRQ.AffectedSOPClassUID, pParams->pObjectInfo->sSopClassUID);

		return true;
	}
	// [Ref:0000577] - Fin

	// [Ref:0000578] - D�but

	/////////////////////////////////////////////////////////////////////
	//	Description	:	Copies the scheduled procedure step into the
	//					study description.
	//	Patch id	:	00009
	bool PatchModule::APLIOStorePatch(PatchParams *pParams)
	{
		bool bRetval          = false;
		OFCondition cond	  = EC_Normal;
		char tagValue[257]	  = {0};
		DcmElement *pElement  = NULL;
		DcmElement *pSequence = NULL;
		DcmItem    *pDi		  = NULL;

		//	On copie le Scheduled Procdure Steps description dans le study desc
		cond = pParams->pDataset->findAndGetElement(DCM_RequestAttributesSequence  , pSequence);
		if(cond.good())
		{
			if(((DcmSequenceOfItems*)pSequence)->card() > 0)
			{
				pDi = ((DcmSequenceOfItems*)pSequence)->getItem(0);
				if(pDi)
				{
					DICOMUtil::GetTagFromDataset(pDi, DCM_ScheduledProcedureStepDescription,(char *)&tagValue);
					if(tagValue)
					{
						DICOMUtil::UpdateTagInDataset(pParams->pDataset, DCM_StudyDescription, tagValue);
					}
				}
			}
		}
		bRetval=true;

		return bRetval;
	}
	// [Ref:0000578] - Fin

	//	[Ref:0000636] - D�but
	/////////////////////////////////////////////////////////////////////
	//	Description	:	Saves the presentation states with a meta info
	//					header that is little-endian implicit (which
	//					is not legal) so that ImaView can open them.
	//	Patch id	:	00010
	bool PatchModule::PresentationStateLittleEndianHeader(PatchParams *pParams)
	{
		bool bRetVal          = false;
		OFCondition cond      = EC_Normal;

		cond = pParams->pDataset->saveFile(pParams->pObjectLocation, EXS_LittleEndianImplicit);
		if(cond.good())
		{
			bRetVal = true;
		}
		return bRetVal;
	}
	//	[Ref:0000636] - Fin

	// [Ref:0001811] - D�but
	/////////////////////////////////////////////////////////////////////////////
	//	Manufacturer	:	DIGITEC
	//	Model			:	HIRIS SN-310.
	//	Description		:	Cete modalit� ne passe pas d'accession number.
	//	Patch id		:	00012
	bool PatchModule::HIRISPatch(PatchParams *pParams)
	{
		bool bRetVal                  = false;
		HANDLE hCursor                = NULL;
		char sTmpPatId[65]            = {0};
		char sTmpAccessionNumber[65]  = {0};
		StringBuffer sql;

		// Si l'accession number n'est pas remplis
		if (pParams->pAccessionNumber || strlen(pParams->pAccessionNumber)==0)
		{
			hCursor = pParams->pDb->GetCursor();
			if(hCursor)
			{
				sql.Format("SELECT STU_PAT_ID, STU_ACCESSION_NUMBER " \
						   "FROM STUDY WHERE STU_INSTANCE_UID = '%s'", pParams->pStudyInstanceUID);

				if(pParams->pDb->SQLExec(hCursor, (char *)sql, "%s%s", sTmpPatId, sTmpAccessionNumber))
				{
					if(strcmp(sTmpPatId, pParams->pPatientId) == 0)
					{
						//	This is the right patient.
						pParams->pModalityLogger->LogMsg("Accession %s added for study %s", sTmpAccessionNumber, pParams->pStudyInstanceUID);
						pParams->pMailer->AddLine("Accession %s added for study %s", sTmpAccessionNumber, pParams->pStudyInstanceUID);

						strcpy(pParams->pAccessionNumber, sTmpAccessionNumber);

						pParams->pDataset->putAndInsertString(DCM_AccessionNumber, pParams->pAccessionNumber);

						bRetVal = true;
					}
				}
				else
				{
					if(pParams->pDb->GetLastOciErrorCode(hCursor) == 0)
					{
						pParams->pModalityLogger->LogMsg("WARNING : No such study for study instance UID : %s", pParams->pStudyInstanceUID);
						pParams->pMailer->AddLine("WARNING : No such study for study instance UID : %s", pParams->pStudyInstanceUID);
					}
					else
					{
						pParams->pModalityLogger->LogMsg("ERROR : %s", pParams->pDb->GetLastOciErrorText(hCursor));
						pParams->pModalityLogger->LogMsg("SQL   :  %s", (char *)sql);

						pParams->pMailer->AddLine("ERROR : %s", pParams->pDb->GetLastOciErrorText(hCursor));
						pParams->pMailer->AddLine("SQL   :  %s", (char *)sql);
					}
				}
				pParams->pDb->DeleteCursor(hCursor);
				hCursor = NULL;
			}
		}
		// [Ref:0001811] - Fin

		return bRetVal;
	}
}
