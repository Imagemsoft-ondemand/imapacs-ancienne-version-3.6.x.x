/////////////////////////////////////////////////////////////////////////////
//	PThreadBase.cpp
/////////////////////////////////////////////////////////////////////////////

#include "PThreadBase.h"

/////////////////////////////////////////////////////////////////////////////
//
namespace util
{

/////////////////////////////////////////////////////////////////////////////
//
PThreadBase::PThreadBase()
{
	m_bIsRunning   = false;

#if defined(WIN32)
	m_hTreadHandle = -1;
#else
	m_hTreadHandle = 0;
#endif
}

/////////////////////////////////////////////////////////////////////////////
//
PThreadBase::~PThreadBase()
{
}

/////////////////////////////////////////////////////////////////////////////
//
bool PThreadBase::Start()
{
	bool bRet = true;

#if defined(WIN32)
	if(!m_bIsRunning)
	{
		m_bIsRunning   = true;
		m_hTreadHandle = _beginthread(ThreadProc,0,this);
		if(m_hTreadHandle == -1)
		{
			m_bIsRunning = false;
			bRet         = false;
		}
		else
		{
		}
	}
#else
	if(!m_bIsRunning)
	{
		m_bIsRunning   = true;
		if(pthread_create(&m_hTreadHandle,NULL,ThreadProc,this))
		{
			m_bIsRunning = false;
			bRet         = false;
		}
	}
#endif
	return bRet;
}

/////////////////////////////////////////////////////////////////////////////
//
bool PThreadBase::Stop(bool bWait)
{
	bool bRet = true;
	if(m_bIsRunning)
	{
		m_bIsRunning = false;

		if(bWait)
		{
#if defined(WIN32)
			if(WaitForSingleObject((HANDLE)m_hTreadHandle,INFINITE) == WAIT_FAILED)
			{
				bRet  = false;
			}
#else
			if(pthread_join(m_hTreadHandle,NULL))
			{
				bRet = false;
			}
#endif
		}
	}
	else
	{
		bRet = false;
	}
	return bRet;
}

/////////////////////////////////////////////////////////////////////////////
//	Used to wait for a thread to terminate. When using this function to wait
//	for the thread to end, make sure to use bWait = false when calling Stop.
bool PThreadBase::WaitForThread()
{
	bool bRet = true;

#if defined(WIN32)
	if(WaitForSingleObject((HANDLE)m_hTreadHandle,INFINITE) == WAIT_FAILED)
	{
		bRet  = false;
	}
#else
	if(pthread_join(m_hTreadHandle,NULL))
	{
		bRet = false;
	}
#endif
	return bRet;
}


/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
//	Thread proc.
#if defined(WIN32)
void ThreadProc(void *pParam)
#else
void *ThreadProc(void *pParam)
#endif
{
	PThreadBase *ptr = (PThreadBase *)pParam;
	if(ptr)
	{
		ptr->Run();
	}

#if !defined(WIN32)	//	For POSIX thread API.
	return (NULL);
#endif
}

}	//	End namespace util.
