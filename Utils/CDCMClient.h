/////////////////////////////////////////////////////////////////////////////
//	CDCMClient.h
//
//	Desc. :	This is a basic C-STORE client used to send dicom objects
//			requested during a C-MOVE request.
/////////////////////////////////////////////////////////////////////////////

#include "Mailer.h"
#include "DicomHeaders.h"
#include "../DICOMCompressor.h"
#include "../PacsConfig.h"
#include "DICOMUtil.h"
#include "StringBuffer.h"
#include "PacsCommon.h"
#include "OCIDatabase.h"

#include "StringBuffer.h"
#include "../Queue.h"
#include "log.h"

#if !defined(__CSTORECLIENT_H_INCLUDED__)
#define      __CSTORECLIENT_H_INCLUDED__

/////////////////////////////////////////////////////////////////////////////
//
namespace imapacs
{
	/////////////////////////////////////////////////////////////////////////
	//
	enum STORE_STATUS
	{
		SS_SUCCESS = 0,
		SS_WARNING = 1,
		SS_ERROR   = 2
	};

	/////////////////////////////////////////////////////////////////////////
	//
	class CDCMClient
	{
	public:
		CDCMClient();
		virtual ~CDCMClient();

		void SetRemoteIP(const char *pIp)                      {if(pIp) strcpy(m_sRemoteIP, pIp);}
		void SetRemotePort(unsigned short iPort)               {m_iRemotePort = iPort;}

		void SetCallingAE(const char *pCallingAE)              {if(pCallingAE) strcpy(m_sCallingAeTitle, pCallingAE);}
		void SetCalledAE(const char *pCalledAE)                {if(pCalledAE) strcpy(m_sCalledAeTitle, pCalledAE);}

		void SetConfig(PacsConfig *pCfg);

		char *GetLastErrorText()                               {return m_sLastErrorText.GetBuffer();}

		T_ASC_Association *GetAssociation()                    {return m_pAssociation;}

		/////////////////////////////////////////////////////////////////////
		//	Should always pass and already established and
		//	negociated association to this function.
		void SetAssociation(T_ASC_Association *pAssociation)   {m_pAssociation = pAssociation;}

		bool CreateAssociation(bool bStorage = true);
		STORE_STATUS StoreObject(const char *pFileName, const char *pCallingAE = NULL);
		STORE_STATUS StoreObject(DcmDataset *pDataset);

		bool RequestAssociation();
		void CloseAssociation();

		E_TransferSyntax GetTrnSynFromStr(const char *pTrnSyn);

	private:

		bool AddAllPresentationContexts(T_ASC_Parameters *pParams, bool bStorage);

		char               m_sRemoteIP[33];
		unsigned short     m_iRemotePort;

		char               m_sCallingAeTitle[17];
		char               m_sCalledAeTitle[17];

		Log               *m_pLogger;
		Log				  *m_pLoggerModality;
		Mailer			  *m_pMailer;

		StringBuffer       m_sLastErrorText;

		DICOMCompressor    m_Compressor;

		PacsConfig        *m_pCfg;

		/////////////////////////////////////////////////////////////////////
		//	dcmtk objects.
		T_ASC_Network     *m_pNetwork;
		T_ASC_Association *m_pAssociation;
		T_ASC_Parameters  *m_pParameters;

		E_TransferSyntax   m_PreferedNetOutputTrnSyn;
	};
}

#endif	//	__CSTORECLIENT_H_INCLUDED__
