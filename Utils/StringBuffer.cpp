/////////////////////////////////////////////////////////////////////////////
//	StringBuffer.cpp
/////////////////////////////////////////////////////////////////////////////

#ifdef WIN32
#include "../stdafx.h"
#endif

#include <stdio.h>
#include <string.h>
#include <stdarg.h>

#include "StringBuffer.h"

#ifdef WIN32
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
#endif

/////////////////////////////////////////////////////////////////////////////
//
StringBuffer::StringBuffer()
{
	m_iBufferLength = 0;
	m_pBuffer       = NULL;
}

/////////////////////////////////////////////////////////////////////////////
//
StringBuffer::StringBuffer(const char *pStr)
{
	if(pStr)
	{
		m_iBufferLength = strlen(pStr);
		if(m_iBufferLength > 0)
		{
			m_pBuffer = new char[m_iBufferLength + 1];
			if(m_pBuffer)
			{
				strcpy(m_pBuffer, pStr);
			}
			else
			{
				m_iBufferLength = 0;
			}
		}
		else
		{
			m_iBufferLength = 0;
			m_pBuffer       = NULL;
		}
	}
}

/////////////////////////////////////////////////////////////////////////////
//
StringBuffer::StringBuffer(const StringBuffer& sStr)
{
	m_iBufferLength = sStr.GetBufferLength();
	if(m_iBufferLength > 0)
	{
		m_pBuffer = new char[m_iBufferLength + 1];
		if(m_pBuffer)
		{
			strcpy(m_pBuffer, sStr.m_pBuffer);
		}
		else
		{
			m_iBufferLength = 0;
		}
	}
	else
	{
		m_iBufferLength = 0;
		m_pBuffer       = NULL;
	}
}

/////////////////////////////////////////////////////////////////////////////
//
StringBuffer::~StringBuffer()
{
	if(m_pBuffer)
	{
		delete [] m_pBuffer;
		m_pBuffer = NULL;
	}
}

/////////////////////////////////////////////////////////////////////////////
//
void StringBuffer::SetBuffer(const char *pBuffer)
{
	if(pBuffer)
	{
		if(m_pBuffer)
		{
			delete [] m_pBuffer;
			m_pBuffer = NULL;
		}

		m_iBufferLength = strlen(pBuffer);

		m_pBuffer = new char[m_iBufferLength + 1];
		if(m_pBuffer)
		{
			strcpy(m_pBuffer, pBuffer);
		}
	}
}

/////////////////////////////////////////////////////////////////////////////
//
void StringBuffer::Clear()
{
	if(m_pBuffer)
	{
		delete [] m_pBuffer;
		m_pBuffer = NULL;
	}
	m_iBufferLength = 0;
}

/////////////////////////////////////////////////////////////////////////////
//	Returns the number of characters in the string if successful, -1 if an
//	error occured.
int StringBuffer::Format(const char *pFormat, ...)
{
	int iLen               = 0;
	int iFormatLen         = 0;
	int iRetVal            = -1;
	char sNumericValue[41] = {0};	//	To determine the size of numerical values.
	const char *pStr       = NULL;
	va_list	Varlist;

	if(pFormat)
	{
		iLen = iFormatLen = strlen(pFormat);
		va_start(Varlist, pFormat);

		for(int i = 0; i < iFormatLen; ++i)
		{
			if(pFormat[i] == '%')
			{
				++i;
				switch(pFormat[i])
				{
					case 's':
					{
						pStr = va_arg(Varlist, char*);
						if(pStr)
						{
							iLen = (iLen + strlen(pStr)) + 1;
						}
					}
					break;

					case 'd':
					{
						sprintf(sNumericValue, "%d", va_arg(Varlist, int));
						iLen = (iLen + strlen(sNumericValue)) + 1;
					}
					break;

					case 'f':
					{
						sprintf(sNumericValue, "%f", va_arg(Varlist, double));
						iLen = (iLen + strlen(sNumericValue)) + 1;
					}
					break;

					case 'x':
					{
						sprintf(sNumericValue, "%x", va_arg(Varlist, int));
						iLen = (iLen + strlen(sNumericValue)) + 1;
					}
					break;
				}
			}
		}
		va_end(Varlist);

		if(m_pBuffer)
		{
			delete [] m_pBuffer;
			m_pBuffer = NULL;
		}
		m_iBufferLength = 0;

		m_pBuffer = new char[iLen + 1];
		if(m_pBuffer)
		{
			va_start(Varlist, pFormat);
			iRetVal = vsprintf(m_pBuffer, pFormat, Varlist);
			va_end(Varlist);

			//	The real buffer size is a bit more then this
			//	but m_iBufferLength is the real size of the
			//	string.
			m_iBufferLength = strlen(m_pBuffer);
		}
	}
	return iRetVal;
}

/////////////////////////////////////////////////////////////////////////////
//	Returns the number of replaced characters, -1 if and error occured.
int StringBuffer::ReplaceChar(const int cToReplace, const int cToReplaceBy)
{
	int iSrcLen     = 0;
	int iReplaceCnt = 0;

	if(!m_pBuffer)
		return -1;

	iSrcLen = strlen(m_pBuffer);
	for(int i = 0; i < iSrcLen; ++i)
	{
		if(m_pBuffer[i] == cToReplace)
		{
			m_pBuffer[i] = cToReplaceBy;
			++iReplaceCnt;
		}
	}
	return iReplaceCnt;
}

/////////////////////////////////////////////////////////////////////////////
//	Same as above but using and external string for convenience.
int StringBuffer::ReplaceChar(char *pStr, const int cToReplace, const int cToReplaceBy)
{
	int iSrcLen     = 0;
	int iReplaceCnt = 0;

	if(!pStr)
		return (-1);

	iSrcLen = strlen(pStr);
	for(int i = 0; i < iSrcLen; ++i)
	{
		if(pStr[i] == cToReplace)
		{
			pStr[i] = cToReplaceBy;
			++iReplaceCnt;
		}
	}
	return iReplaceCnt;
}

/////////////////////////////////////////////////////////////////////////////
//	Removes all instances of the specified character.
void StringBuffer::RemoveCharacter(char *pString, const char cChar)
{
	for(int i = 0; (pString[i] != 0x00); ++i)
	{
		if(pString[i] == cChar)
		{
			strcpy(&pString[i], &pString[i + 1]);
			--i;	//	In case where we have more then one cChar in a row.
		}
	}
}

/////////////////////////////////////////////////////////////////////////////
//	Removes all instances of the specified character.
void StringBuffer::RemoveCharacter(const char cChar)
{
	RemoveCharacter(m_pBuffer, cChar);
	m_iBufferLength = strlen(m_pBuffer);
}

/////////////////////////////////////////////////////////////////////////////
//
StringBuffer &StringBuffer::operator=(const char *pChunk)
{
	if(!pChunk)
		return (*this);

	m_iBufferLength = strlen(pChunk);

	if(m_pBuffer)
	{
		delete [] m_pBuffer;
		m_pBuffer = NULL;
	}

	m_pBuffer = new char[m_iBufferLength + 1];
	if(m_pBuffer)
	{
		strcpy(m_pBuffer, pChunk);
	}
	return (*this);
}

/////////////////////////////////////////////////////////////////////////////
//
StringBuffer &StringBuffer::operator=(const StringBuffer &Str)
{
	char *pTmp = NULL;

	if(this == &Str)
		return (*this);

	if(!Str.m_pBuffer)
		return (*this);

	m_iBufferLength = strlen(Str.m_pBuffer);

	if(m_pBuffer)
	{
		delete [] m_pBuffer;
		m_pBuffer = NULL;
	}

	m_pBuffer = new char[m_iBufferLength + 1];
	if(m_pBuffer)
	{
		strcpy(m_pBuffer, Str.m_pBuffer);
	}
	return (*this);
}

/////////////////////////////////////////////////////////////////////////////
//
StringBuffer &StringBuffer::operator+=(const char *pChunk)
{
	char *pNewBuffer = NULL;

	if(!pChunk)
		return (*this);

	if(m_pBuffer)
	{
		m_iBufferLength = (strlen(m_pBuffer) + strlen(pChunk));

		pNewBuffer = new char[m_iBufferLength + 1];
		if(pNewBuffer)
		{
			strcpy(pNewBuffer, m_pBuffer);

			delete [] m_pBuffer;
			m_pBuffer = NULL;

			strcat(pNewBuffer, pChunk);

			m_pBuffer = pNewBuffer;
		}
	}
	else
	{
		m_iBufferLength = strlen(pChunk);
		m_pBuffer = new char[m_iBufferLength + 1];
		if(m_pBuffer)
		{
			strcpy(m_pBuffer, pChunk);
		}
	}
	return (*this);
}

/////////////////////////////////////////////////////////////////////////////
//
StringBuffer &StringBuffer::operator+=(const StringBuffer &Str)
{
	char *pNewBuffer = NULL;

	if(this == &Str)
		return (*this);

	if(!Str.m_pBuffer)
		return (*this);

	if(m_pBuffer)
	{
		m_iBufferLength = (strlen(m_pBuffer) + Str.GetBufferLength());
		pNewBuffer = new char[m_iBufferLength + 1];
		if(pNewBuffer)
		{
			strcpy(pNewBuffer, m_pBuffer);

			delete [] m_pBuffer;
			m_pBuffer = NULL;

			strcat(pNewBuffer, Str.m_pBuffer);

			m_pBuffer = pNewBuffer;
		}
	}
	else
	{
		m_iBufferLength = Str.GetBufferLength();
		m_pBuffer = new char[m_iBufferLength + 1];
		if(m_pBuffer)
		{
			strcpy(m_pBuffer, Str.m_pBuffer);
		}
	}
	return (*this);
}
