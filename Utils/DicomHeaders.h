/////////////////////////////////////////////////////////////////////////////
//	DicomHeaders.h
/////////////////////////////////////////////////////////////////////////////

#if !defined(__DICOMHEADERS_H_INCLUDED__)
#define __DICOMHEADERS_H_INCLUDED__

#if defined(WIN32)
#include <winsock.h>
#else
#define HAVE_CONFIG_H //	This is necessary when compiling under Unix so the
#endif                //	compiler will use the right configuration file.

/////////////////////////////////////////////////////////////////////////////
//	dcmtk headers.
#include "dcmtk/config/osconfig.h"

#include "dcmtk/ofstd/ofstream.h"
#include "dcmtk/ofstd/ofstring.h"
#include "dcmtk/dcmnet/dimse.h"
#include "dcmtk/dcmnet/diutil.h"
#include "dcmtk/dcmdata/dcdatset.h"
#include "dcmtk/dcmdata/dcmetinf.h"
#include "dcmtk/dcmdata/dcfilefo.h"
//#include "dcmtk/dcmdata/dcdebug.h"
#include "dcmtk/dcmdata/dcuid.h"
#include "dcmtk/dcmdata/dcdict.h"
#include "dcmtk/dcmdata/dcdeftag.h"
#include "dcmtk/dcmdata/cmdlnarg.h"
#include "dcmtk/dcmimgle/dcmimage.h"
//#include "dcmtk/ofstd/ofconapp.h"
#include "dcmtk/dcmdata/dcuid.h"    // For dcmtk version name.
#include "dcmtk/dcmnet/dicom.h"     // For DICOM_APPLICATION_REQUESTOR.
#include "dcmtk/dcmdata/dcostrmz.h" // For dcmZlibCompressionLevel.
#include "dcmtk/dcmnet/dcasccfg.h"  // For class DcmAssociationConfiguration.
#include "dcmtk/dcmnet/dcasccff.h"  // For class DcmAssociationConfigurationFile.
#include "dcmtk/dcmdata/dcddirif.h" // For DicomDirInterface class.
#include "dcmtk/dcmjpeg/djdecode.h"
#include "dcmtk/dcmjpeg/djencode.h"
#include "dcmtk/dcmjpeg/djrplol.h"
#include "dcmtk/dcmjpeg/djrploss.h"

// TEST
//#include "dcmtk/dcmdata/dcostrmf.h"
// FIN TEST

#endif //	__DICOMHEADERS_H_INCLUDED__
