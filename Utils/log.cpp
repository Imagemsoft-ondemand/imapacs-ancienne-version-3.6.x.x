/////////////////////////////////////////////////////////////////////////////
//	log.cpp
/////////////////////////////////////////////////////////////////////////////

#ifdef WIN32
#include "../stdafx.h"
#endif

#include "log.h"

#include <time.h>
#include <string.h>
#include <vector>

/*
#if defined(WIN32)

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#endif
*/

#ifdef WIN32
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
#endif

using namespace std;

/////////////////////////////////////////////////////////////////////////////
//  Construction / destruction.
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//
Log :: Log()
{
	m_pLogBuffer      = NULL;
	m_pLogFd          = NULL;
	m_pPrefix         = NULL;
	m_sLogFilePath[0] = 0x00;
}

/////////////////////////////////////////////////////////////////////////////
//
Log :: ~Log()
{
	if(m_pPrefix)
	{
		delete [] m_pPrefix;
		m_pPrefix = NULL;
	}
}

/////////////////////////////////////////////////////////////////////////////
// Sets the path of the log file.
#if defined(WIN32)

void Log :: SetLogFilePath(const char *logfilepath, HANDLE *hMutex)
{
	if(logfilepath != NULL)
	{
		strcpy(m_sLogFilePath,logfilepath);
	}
	else
	{
		m_sLogFilePath[0] = 0x00;
	}
}

#else

void Log :: SetLogFilePath(const char *logfilepath)
{
	if(logfilepath != NULL)
	{
		strcpy(m_sLogFilePath,logfilepath);
	}
	else
	{
		m_sLogFilePath[0] = 0x00;
	}
}

#endif

/////////////////////////////////////////////////////////////////////////////
//
void Log :: SetPrefix(const char *pPrefix)
{
	if(pPrefix)
	{
		if(m_pPrefix)
		{
			delete [] m_pPrefix;
			m_pPrefix = NULL;
		}

		m_pPrefix = new char[strlen(pPrefix) + 1];
		if(m_pPrefix)
		{
			strcpy(m_pPrefix, pPrefix);
		}
	}
}

/////////////////////////////////////////////////////////////////////////////
// Write a formatted message to the log file. This file is located
// in the directory specified in a call to SetLogFilePath. If no
// path was specified, the current directory is used. 
// The log file as the name : YYYYMMDD.log. Each entry in the log 
// begins with the current time written with the following 
// format : <HH:MM:SS>.
// If a prefix has been specified, the name of the file will be
// of the form PREFIXYYYYMMDD.log.
// The format parameter works like printf with the following
// valid format specifiers : %s, %d, %f, %x.
// If the file could not be opened, return false. Return true
// otherwise.
bool Log ::LogMsg(const char *format,...)
{
	bool retval                    = true;
	char *pLogFile                 = NULL;
	char date[15]                  = {0};
	time_t timer                   = 0;

	struct tm *curtime             = NULL;

	struct tm Safetime             = {0};

	int len                        = 0;
	int bufsize                    = 0;
	int count                      = 0;
	int varsize                    = 0;
	int i                          = 0;
	int j                          = 0;
	param *ptrtmp                  = NULL;
	vector <param *> paramlst;
	vector <param *>::iterator pos;

	va_list	varlist;

	time(&timer);

#if defined(WIN32)
	curtime = localtime(&timer);
	memcpy(&Safetime, curtime, sizeof(struct tm));	//	Safe copy.
#else
	localtime_r(&timer, &Safetime);
#endif


	sprintf(date,"%04d%02d%02d",Safetime.tm_year + 1900,
                                Safetime.tm_mon + 1,
                                Safetime.tm_mday);

	len = strlen(format);
	va_start(varlist,format);

	for(i=0; i < len; ++i)
	{
		if(format[i] == '%')
		{
			++i;
			switch(format[i])
			{
				case 's':   // string.
				case 'S':
					ptrtmp = new param;
					ptrtmp->pstr = va_arg(varlist,char *);
					ptrtmp->size = strlen(ptrtmp->pstr);
					varsize += (ptrtmp->size);
					ptrtmp->isStr = true;
					paramlst.push_back(ptrtmp);
					++count;
				break;

				case 'd':   // integer.
				case 'D':
					ptrtmp = new param;
					sprintf(ptrtmp->nvalue,"%d",va_arg(varlist,int));
					ptrtmp->size = (strlen(ptrtmp->nvalue));
					varsize += (ptrtmp->size);
					ptrtmp->isStr = false;
					paramlst.push_back(ptrtmp);
					++count;
				break;

				case 'f':   // float.
				case 'F':
					ptrtmp = new param;
					sprintf(ptrtmp->nvalue,"%f",va_arg(varlist,double));
					ptrtmp->size = (strlen(ptrtmp->nvalue));
					varsize += (ptrtmp->size);
					ptrtmp->isStr = false;
					paramlst.push_back(ptrtmp);
					++count;
				break;

				case 'x':   // exadecimal.
					ptrtmp = new param;
					sprintf(ptrtmp->nvalue,"%x",va_arg(varlist,int));
					ptrtmp->size = (strlen(ptrtmp->nvalue));
					varsize += (ptrtmp->size);
					ptrtmp->isStr = false;
					paramlst.push_back(ptrtmp);
					++count;
				break;
				case 'X':
					ptrtmp = new param;
					sprintf(ptrtmp->nvalue,"%X",va_arg(varlist,int));
					ptrtmp->size = (strlen(ptrtmp->nvalue));
					varsize += (ptrtmp->size);
					ptrtmp->isStr = false;
					paramlst.push_back(ptrtmp);
					++count;
				break;
			}
		}
	}
	va_end(varlist);

#if defined(WIN32)
	if(m_pPrefix)
	{
		pLogFile = new char[_MAX_PATH + 3 + strlen(date) + 3 + strlen(m_pPrefix) + 8];
		if(pLogFile)
		{
			if(m_sLogFilePath[strlen(m_sLogFilePath) - 1] == '\\')
				sprintf(pLogFile,"%s%s%s.log", m_sLogFilePath, m_pPrefix, date);
			else
				sprintf(pLogFile,"%s\\%s%s.log", m_sLogFilePath, m_pPrefix, date);
		}
	}
	else
	{
		pLogFile = new char[_MAX_PATH + 3 + strlen(date) + 8];
		if(pLogFile)
		{
			if(m_sLogFilePath[strlen(m_sLogFilePath) - 1] == '\\')
				sprintf(pLogFile,"%s%s.log", m_sLogFilePath, date);
			else
				sprintf(pLogFile,"%s\\%s.log", m_sLogFilePath, date);
		}
	}
#else
	if(m_pPrefix)
	{
		pLogFile = new char[_MAX_PATH + 2 + strlen(date) + 2 + strlen(m_pPrefix) + 8];
		if(pLogFile)
		{
			if(m_sLogFilePath[strlen(m_sLogFilePath) - 1] == '/')
			{
				sprintf(pLogFile,"%s%s%s.log", m_sLogFilePath, m_pPrefix, date);
			}
			else
			{
				sprintf(pLogFile,"%s/%s%s.log", m_sLogFilePath, m_pPrefix, date);
			}
		}
	}
	else
	{
		pLogFile = new char[_MAX_PATH + 2 + strlen(date) + 8];
		if(pLogFile)
		{
			sprintf(pLogFile,"%s/%s.log", m_sLogFilePath, date);
		}
	}
#endif

	if(pLogFile)
	{
		m_pLogFd = fopen(pLogFile, "a");

		if(m_pLogFd != NULL)
		{
			bufsize = (25 + (len - (2*count)) + varsize + 1);
			m_pLogBuffer = new char[bufsize];
			m_pLogBuffer[0] = 0x00;
			pos = pos = paramlst.begin();

			for(i = 0;i < len;++i)
			{
				if(format[i] == '%')
				{
					if((*pos)->isStr)
						strcat(&m_pLogBuffer[j],(*pos)->pstr);
					else
						strcat(&m_pLogBuffer[j],(*pos)->nvalue);
					++i;
					j += ((*pos)->size);
					++pos;
				}
				else
				{
					m_pLogBuffer[j] = format[i];
					++j;
					m_pLogBuffer[j] = 0x00;
				}
			}

			// Append to the file.
#if defined(WIN32)
			fprintf(m_pLogFd,"< %02d:%02d:%02d > %s\n",Safetime.tm_hour,
													   Safetime.tm_min,
													   Safetime.tm_sec,
													   m_pLogBuffer);
#else
			fprintf(m_pLogFd,"< %02d:%02d:%02d > %s\n",Safetime.tm_hour,
													   Safetime.tm_min,
													   Safetime.tm_sec,
													   m_pLogBuffer);
#endif

			fclose(m_pLogFd);
			m_pLogFd = NULL;

			if(m_pLogBuffer != NULL)
			{
				delete [] m_pLogBuffer;
				m_pLogBuffer = NULL;
			}
		}
		else
		{
			retval = false;
		}

		delete [] pLogFile;
		pLogFile = NULL;
	}

	pos = paramlst.begin();
	while(pos != paramlst.end())
	{

		if((*pos) != NULL)
		{
			delete (*pos);
			(*pos) = NULL;
		}
		++pos;
	}
	paramlst.clear();

	return retval;
}
