/////////////////////////////////////////////////////////////////////////////
//	DBGTool.h
/////////////////////////////////////////////////////////////////////////////

#ifndef WIN32
#include <time.h>
#endif

#include "log.h"
#include "PacsCommon.h"

#if !defined(__DGBTOOL_H_INCLUDED__)
#define      __DGBTOOL_H_INCLUDED__

/////////////////////////////////////////////////////////////////////////////
//
class DBGTool
{
public:
#ifdef WIN32	
	DBGTool():m_iStartTime(0),
		      m_iEndTime(0),
		      m_bHasPerformanceTimer(false),
			  m_bMultiCore(false)
	{
#ifdef WIN32
		SYSTEM_INFO sysinfo;
		GetSystemInfo(&sysinfo);
		m_bMultiCore = (sysinfo.dwNumberOfProcessors > 1);

		m_bHasPerformanceTimer = QueryPerformanceFrequency(&m_Frequency)? true : false;
#endif
	}
#else
	DBGTool()
	{
	}
#endif
	virtual ~DBGTool()
	{}

	/////////////////////////////////////////////////////////////////////////
	//	Debug output methods.
	/////////////////////////////////////////////////////////////////////////

	static char *MakeFormattedString(const char *pFormat, ...);

	static void OutputLineToFile(const char *pFile,
		                         const char *pFormat, ...);

	static void OutputLine(const char *pFormat, ...);

	/////////////////////////////////////////////////////////////////////////
	//	Timer methods.
	/////////////////////////////////////////////////////////////////////////

	void StartTimer();
	void StopTimer();

	unsigned int GetIntervalInMili() const;

	/////////////////////////////////////////////////////////////////////////
	//	Profiling methods.
	/////////////////////////////////////////////////////////////////////////

	void StartProfiling(Log *pLogger, 
		                const char *pDisplayString)
	{
		pLogger->LogMsg(BEGIN_TRANSACTION_LINE);
		pLogger->LogMsg(pDisplayString);

		StartTimer();
	}

	void EndProfiling(Log *pLogger,
		              const char *pDataToDump,
					  const char *pDisplayString)
	{
		StopTimer();
		float fTime = 0.0f;

		unsigned int iTime = GetIntervalInMili();

		if(pDataToDump)
		{
			pLogger->LogMsg("%s", pDataToDump);
		}

		if(iTime > 999)
		{
			fTime = ((float)iTime/1000.0f);
			pLogger->LogMsg("Time   :   %f seconds", fTime);
		}
		else
		{
			pLogger->LogMsg("Time   :   %d miliseconds", iTime);
		}
		pLogger->LogMsg(pDisplayString);
		pLogger->LogMsg(END_TRANSACTION_LINE);
	}

private:
	
#ifndef WIN32
	int64_t TimeSpecDiff(struct timespec *pTimeA, struct timespec *pTimeB);
#endif

	unsigned int  m_iStartTime;
	unsigned int  m_iEndTime;
	
#ifdef WIN32

	LARGE_INTEGER m_Frequency;
	DWORD_PTR     m_ThreadAffinityMask;

	bool          m_bHasPerformanceTimer;
	bool          m_bMultiCore;
#endif
};

#endif	//	__DGBTOOL_H_INCLUDED__
