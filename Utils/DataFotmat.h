/////////////////////////////////////////////////////////////////////////////
//	DataFotmat.h
/////////////////////////////////////////////////////////////////////////////

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include "PacsCommon.h"
#include "StringBuffer.h"

#if !defined(__DATAFORMAT_H_INCLUDED__)
#define      __DATAFORMAT_H_INCLUDED__

/////////////////////////////////////////////////////////////////////////////
//	All methods used to format data for database or other purpose should
//	go here.
namespace imapacs
{
	class DataFormat
	{
	public:

		/////////////////////////////////////////////////////////////////////
		//
		static void ConvertDateTime(char *datetime, const char *date, const char *time)
		{
			strcpy(datetime, "TO_DATE('");
			strcat(datetime, date);

			if (strlen(time)>0)
				strncat(datetime, time, 6);

			strcat(datetime, "','YYYYMMDD");

			if (strlen(time)>0)
				strcat(datetime, "HH24MISS");

			strcat(datetime, "')");
		}

		/////////////////////////////////////////////////////////////////////
		//
		static void GetStorageIdFromMFS(const char *pMfs, char *pStorageId)
		{
			int i = 0;

			if(pMfs && pStorageId)
			{
				//	Find position of the last /.
				for(i = (strlen((char *)pMfs) - 1); i >= 0; --i)
				{
					if(pMfs[i] == '/')
					{
						++i;
						break;
					}
				}
			}
			strcpy(pStorageId, &pMfs[i]);
		}

		/////////////////////////////////////////////////////////////////////
		//
		static void ComposeWhereClause(const char *pFieldName,
									   const char *pFieldValue,
									   StringBuffer *pWhere)
		{
			StringBuffer Tmp;
			char pValue[129] = {0};

			if(pFieldValue && pFieldValue[0])
			{
				strcpy(pValue, pFieldValue);

				if(pWhere->GetBufferLength())
				{
					(*pWhere) += " AND ";
				}
				else
				{
					(*pWhere) = " WHERE ";
				}
				(*pWhere) += pFieldName;

				if(strstr(pFieldValue, "*"))
				{
					Tmp.ReplaceChar(pValue, '*', PERCENT_SIGN);
					//(*pWhere) += " LIKE '"; // [Ref:0000722]
					(*pWhere) += " LIKE "; // [Ref:0000722]
				}
				else
				{
					//(*pWhere) += " = '"; // [Ref:0000722]
					(*pWhere) += " = "; // [Ref:0000722]
				}

				(*pWhere) += "UPPER('"; // [Ref:0000722]
				(*pWhere) += pValue;
				//(*pWhere) += ")"; // [Ref:0000722]
				(*pWhere) += "')"; // [Ref:0000722]
			}
		}

		/////////////////////////////////////////////////////////////////////
		//
		static void ComposeWhereClauseWithLike(const char *pFieldName,
			                                   const char *pFieldValue,
											   StringBuffer *pWhere)
		{
			char pValue[256] = {0};

			if(pFieldValue && pFieldValue[0])
			{
				strcpy(pValue, pFieldValue);

				if(pWhere->GetBufferLength())
				{
					(*pWhere) += " AND ";
				}
				else
				{
					(*pWhere) = " WHERE ";
				}
				(*pWhere) += pFieldName;

				(*pWhere) += " LIKE UPPER('%";

				StringBuffer::RemoveCharacter(pValue, '*');

				(*pWhere) += pValue;
				(*pWhere) += "%')";
			}
		}

		/////////////////////////////////////////////////////////////////////
		//
		static void SOPClass2Type(const char *pSOPClassUID, char *pType, char *pDesc)
		{
			if(pSOPClassUID && pType)
			{
				if(strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.2") == 0)
				{
					strcpy(pDesc, "CTImageStorage");
					strcpy(pType, "IMAGE");
				}
				else if(strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.1") == 0)
				{
					strcpy(pDesc, "EnhancedXAImageStorage");
					strcpy(pType, "IMAGE");
				}
				else if(strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.1.1") == 0)
				{
					strcpy(pDesc, "DigitalXRayImageStorageForPresentation");
					strcpy(pType, "IMAGE");
				}
				else if(strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.1.1.1") == 0)
				{
					strcpy(pDesc, "DigitalXRayImageStorageForProcessing");
					strcpy(pType, "IMAGE");
				}
				else if(strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.1.2") == 0)
				{
					strcpy(pDesc, "DigitalMammographyXRayImageStorageForPresentation");
					strcpy(pType, "IMAGE");
				}
				else if (strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.1.2.1") == 0)
				{
					strcpy(pDesc, "DigitalMammographyXRayImageStorageForProcessing");
					strcpy(pType, "IMAGE");
				}
				else if(strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.1.3") == 0)
				{
					strcpy(pDesc, "DigitalIntraOralXRayImageStorageForPresentation");
					strcpy(pType, "IMAGE");
				}
				else if(strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.1.3.1") == 0)
				{
					strcpy(pDesc, "DigitalIntraOralXRayImageStorageForProcessing");
					strcpy(pType, "IMAGE");
				}
				else if(strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.2.1") == 0)
				{
					strcpy(pDesc, "EnhancedCTImageStorage");
					strcpy(pType, "IMAGE");
				}
				else if(strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.3.1") == 0)
				{
					strcpy(pDesc, "UltrasoundMultiframeImageStorage");
					strcpy(pType, "IMAGE");
				}
				else if(strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.4") == 0)
				{
					strcpy(pDesc, "MRImageStorage");
					strcpy(pType, "IMAGE");
				}
				else if(strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.4.1") == 0)
				{
					strcpy(pDesc, "EnhancedMRImageStorage");
					strcpy(pType, "IMAGE");
				}
				else if(strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.4.2") == 0)
				{
					strcpy(pDesc, "MRSpectroscopyStorage");
					strcpy(pType, "IMAGE");
				}
				else if(strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.6.1") == 0)
				{
					strcpy(pDesc, "UltrasoundImageStorage");
					strcpy(pType, "IMAGE");
				}
				else if(strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.7") == 0)
				{
					strcpy(pDesc, "SecondaryCaptureImageStorage");
					strcpy(pType, "IMAGE");
				}
				else if(strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.7.1") == 0)
				{
					strcpy(pDesc, "MultiframeSingleBitSecondaryCaptureImageStorage");
					strcpy(pType, "IMAGE");
				}
				else if(strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.7.2") == 0)
				{
					strcpy(pDesc, "MultiframeGrayscaleByteSecondaryCaptureImageStorage");
					strcpy(pType, "IMAGE");
				}
				else if(strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.7.3") == 0)
				{
					strcpy(pDesc, "MultiframeGrayscaleWordSecondaryCaptureImageStorage");
					strcpy(pType, "IMAGE");
				}
				else if(strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.7.4") == 0)
				{
					strcpy(pDesc, "MultiframeTrueColorSecondaryCaptureImageStorage");
					strcpy(pType, "IMAGE");
				}
				else if(strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.7.4") == 0)
				{
					strcpy(pDesc, "MultiframeTrueColorSecondaryCaptureImageStorage");
					strcpy(pType, "IMAGE");
				}
				else if(strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.12.1") == 0)
				{
					strcpy(pDesc, "EnhancedXAImageStorage");
					strcpy(pType, "IMAGE");
				}
				else if(strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.12.2") == 0)
				{
					strcpy(pDesc, "EnhancedXRFImageStorage");
					strcpy(pType, "IMAGE");
				}
				else if(strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.20") == 0)
				{
					strcpy(pDesc, "NuclearMedicineImageStorage");
					strcpy(pType, "IMAGE");
				}
				else if(strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.77.1.1") == 0)
				{
					strcpy(pDesc, "VLEndoscopicImageStorage");
					strcpy(pType, "IMAGE");
				}
				else if(strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.77.1.1.1") == 0)
				{
					strcpy(pDesc, "VideoEndoscopicImageStorage");
					strcpy(pType, "IMAGE");
				}
				else if(strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.77.1.2") == 0)
				{
					strcpy(pDesc, "VLMicroscopicImageStorage");
					strcpy(pType, "IMAGE");
				}
				else if(strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.77.1.2.1") == 0)
				{
					strcpy(pDesc, "VideoMicroscopicImageStorage");
					strcpy(pType, "IMAGE");
				}
				else if(strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.77.1.3") == 0)
				{
					strcpy(pDesc, "VLSlideCoordinatesMicroscopicImageStorage");
					strcpy(pType, "IMAGE");
				}
				else if(strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.77.1.4") == 0)
				{
					strcpy(pDesc, "VLPhotographicImageStorage");
					strcpy(pType, "IMAGE");
				}
				else if(strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.77.1.4.1") == 0)
				{
					strcpy(pDesc, "VideoPhotographicImageStorage");
					strcpy(pType, "IMAGE");
				}
				else if(strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.77.1.5.1") == 0)
				{
					strcpy(pDesc, "OphthalmicPhotography8BitImageStorage");
					strcpy(pType, "IMAGE");
				}
				else if(strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.77.1.5.2") == 0)
				{
					strcpy(pDesc, "OphthalmicPhotography16BitImageStorage");
					strcpy(pType, "IMAGE");
				}
				else if(strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.481.1") == 0)
				{
					strcpy(pDesc, "RTImageStorage");
					strcpy(pType, "IMAGE");
				}
				else if(strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.9.4.1") == 0)
				{
					strcpy(pDesc, "BasicVoiceAudioWaveformStorage");
					strcpy(pType, "AUDIO");
				}
				else if(strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.88.11") == 0)
				{
					strcpy(pDesc, "BasicTextSR");
					strcpy(pType, "SR_UNVERIFIED");
				}
				else if(strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.88.22") == 0)
				{
					strcpy(pDesc, "EnhancedSR");
					strcpy(pType, "SR_UNVERIFIED");
				}
				else if(strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.88.33") == 0)
				{
					strcpy(pDesc, "ComprehensiveSR");
					strcpy(pType, "SR_UNVERIFIED");
				}
				else if(strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.88.50") == 0)
				{
					strcpy(pDesc, "MammographyCADSR");
					strcpy(pType, "SR_UNVERIFIED");
				}
				else if(strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.88.65") == 0)
				{
					strcpy(pDesc, "ChestCADSR");
					strcpy(pType, "SR_UNVERIFIED");
				}
				else if(strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.11.1") == 0)
				{
					strcpy(pDesc, "GrayscaleSoftcopyPresentationStateStorage");
					strcpy(pType, "PRESENTATION_STATE");
				}
				else if(strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.88.59") == 0)
				{
					strcpy(pDesc, "KeyObjectSelectionDocument");
					strcpy(pType, "KEY_OBJECT");
				}
				else if(strcmp(pSOPClassUID,"1.2.840.10008.5.1.4.1.1.13.1.3") == 0)
				{
					strcpy(pDesc, "Breast Tomosynthesis Image Storage");
					strcpy(pType, "IMAGE");
				}
			}
		}

		/////////////////////////////////////////////////////////////////////
		//	Parses the string from the DICOM tag and extracts the x and y
		//	scaling factor into pXscaleFactor and pYscaleFactor.
		static void ExtractScalingFactors(float *pXscaleFactor,
			                              float *pYscaleFactor,
										  const char *pScalingFactors)
		{
			char sTmpFactor[65] = {0};
			int iLen            = strlen(pScalingFactors);
			int i               = 0;
			int j               = 0;

			for(i = 0; i < iLen; ++i)
			{
				if(pScalingFactors[i] != '/')
				{
					sTmpFactor[j] = pScalingFactors[i];
					++j;
				}
				else
				{
					++i;	//	Skip the /
					break;
				}
			}
			sTmpFactor[j] = 0x00;
			j = 0;

			(*pXscaleFactor) = (float)atof(sTmpFactor);

			for(;i < iLen; ++i)
			{
				sTmpFactor[j] = pScalingFactors[i];
				++j;
			}
			sTmpFactor[j] = 0x00;

			(*pYscaleFactor) = (float)atof(sTmpFactor);
		}

		/////////////////////////////////////////////////////////////////////
		//	Removes the matricule in the physician's name if any.
		static void RemoveMatriculeFromPhysicianName(char *pPhysicianName)
		{
			if(pPhysicianName && pPhysicianName[0])
			{
				for(int i = strlen(pPhysicianName); i > 0; --i)
				{
					if(pPhysicianName[i] == '#')
					{
						pPhysicianName[i] = 0x00;
						break;
					}
				}
			}
		}

		/////////////////////////////////////////////////////////////////////
		//	Copy source into string pDestination uo to the first instance
		//	of the character token encontered in stringe pSource. pSource
		//	is also null-terminated after the first token.
		static void CopyAndTrimToFirstModality(char *pDestination, char *pSource, char cToken)
		{
			//if((pDestination && pDestination[0]) && (pSource && pSource[0]) && (cToken != 0x00)) // [Ref:0000566]
			if((pDestination) && (pSource && pSource[0]) && (cToken != 0x00)) // [Ref:0000566]
			{
				int iLen = strlen(pSource);
				for(int i = 0; i < iLen; ++i)
				{
					//if(pSource[0] == cToken) // [Ref:0000566]
					if(pSource[i] == cToken) // [Ref:0000566]
					{
						pSource[i] = 0x00;
						break;
					}
				}
				strcpy(pDestination, pSource);
			}
		}

		/////////////////////////////////////////////////////////////////////
		//
		static void MakeHL7CompleteName(char *pDestination, const char *pSource, int iMaxCount)
		{
			StringBuffer str;
			if(pDestination && (pSource && pSource[0]))
			{
				if(strstr(pSource, "^"))
				{
					str.Format("%s^^^^^^", pSource);
					strncpy(pDestination, (char *)str, (iMaxCount - 1));
					pDestination[iMaxCount] = 0x00;
				}
				else
				{
					str.Format("%s^^^^^^^", pSource);
					strncpy(pDestination, (char *)str, (iMaxCount - 1));
					pDestination[iMaxCount] = 0x00;
				}
			}
		}

		/////////////////////////////////////////////////////////////////////
		//
		static void MakeQueryDates(const char *pDateInterval, 
								   const char *pTimeInterval, 
								   StringBuffer *pWhere,
								   const char *pStartDateColumnName,
								   const char *pStartTimeColumnName)
		{
			StringBuffer DateInterval;
			StringBuffer TimeInterval;
			char sInterval[65]  = {0};
			char sStartDate[65] = {0};
			char sEndDate[65]   = {0};
			char sStartTime[65] = {0};
			char sEndTime[65]   = {0};
			int i               = 0;
			int iLen            = 0;

			/////////////////////////////////////////////////////////////////////////
			//	Process the date interval.
			if(pDateInterval && pDateInterval[0])
			{
				iLen = strlen(pDateInterval);
				strcpy(sInterval, pDateInterval);

				if(strstr(sInterval, "-"))					//	There is an interval.
				{
					if(sInterval[0] != '-')					//	We have a start date.
					{
						if(sInterval[iLen - 1] == '-')		//	We only have a start date.
						{
							sInterval[iLen - 1] = 0x00;
							DateInterval.Format(" AND %s >= '%s'", pStartDateColumnName, sInterval);
							(*pWhere) += DateInterval.GetBuffer();
						}
						else
						{
							//	We have an interval, extract start date and end date.
							for(i = 0; i < iLen; ++i)
							{
								if(sInterval[i] == '-')
								{
									//	We have the position of the -.
									break;
								}
							}
							strncpy(sStartDate, sInterval, i);
							strcpy(sEndDate, &sInterval[i + 1]);

							if(strcmp(sStartDate, sEndDate) != 0)
							{
//[ref:0001325]-d�but
//								DateInterval.Format(" AND %s >= '%s' AND %s < '%s'", 
								DateInterval.Format(" AND %s >= '%s' AND %s <= '%s'", 
													pStartDateColumnName,
													sStartDate,
													pStartDateColumnName,
													sEndDate);
//[ref:0001325]-fin

								(*pWhere) += DateInterval.GetBuffer();
							}
							else
							{
								//	[ref ???]
								DateInterval.Format(" AND %s = '%s'", 
													pStartDateColumnName,
													sStartDate);

								(*pWhere) += DateInterval.GetBuffer();
								//	[ref ???]
							}
						}
					}
					else
					{
						//	We only have an end date.
						DateInterval.Format(" AND %s <= '%s'", pStartDateColumnName, &sInterval[1]);
						(*pWhere) += DateInterval.GetBuffer();
					}
				}
				else
				{
					//	We only have a date.
					DateInterval.Format(" AND %s = '%s'", pStartDateColumnName, sInterval);
					(*pWhere) += DateInterval.GetBuffer();
				}
			}

			/////////////////////////////////////////////////////////////////////////
			//	Process the time interval.
			if(pTimeInterval && pTimeInterval[0])
			{
				iLen = strlen(pTimeInterval);
				strcpy(sInterval, pTimeInterval);

				if(strstr(sInterval, "-"))					//	There is an interval.
				{
					if(sInterval[0] != '-')					//	We have a start date.
					{
						if(sInterval[iLen - 1] == '-')		//	We only have a start time.
						{
							sInterval[iLen - 1] = 0x00;
							TimeInterval.Format(" AND TO_NUMBER(SUBSTR(%s,0,6)) >= %s", pStartTimeColumnName, sInterval);
							(*pWhere) += TimeInterval.GetBuffer();
						}
						else
						{
							//	We have an interval, extract start time and end time.
							for(i = 0; i < iLen; ++i)
							{
								if(sInterval[i] == '-')
								{
									//	We have the position of the -.
									break;
								}
							}
							strncpy(sStartTime, sInterval, i);
							strcpy(sEndTime, &sInterval[i + 1]);
//[ref:0001325]-d�but
//							TimeInterval.Format(" AND TO_NUMBER(SUBSTR(%s,0,6)) >= %s AND TO_NUMBER(SUBSTR(%s,0,6)) < %s",
							TimeInterval.Format(" AND TO_NUMBER(SUBSTR(%s,0,6)) >= %s AND TO_NUMBER(SUBSTR(%s,0,6)) <= %s",
												pStartTimeColumnName,
												sStartTime,
												pStartTimeColumnName,
												sEndTime);
//[ref:0001325]-fin
							(*pWhere) += TimeInterval.GetBuffer();
						}
					}
					else
					{
						//	We only have an end time.
						TimeInterval.Format(" AND TO_NUMBER(SUBSTR(%s,0,6)) <= %s", pStartTimeColumnName, &sInterval[1]);
						(*pWhere) += TimeInterval.GetBuffer();
					}
				}
				else
				{
					//	We only have a time.
					TimeInterval.Format(" AND TO_NUMBER(SUBSTR(%s,0,6)) = %s", pStartTimeColumnName, sInterval);
					(*pWhere) += TimeInterval.GetBuffer();
				}
			}
		}
	};
}

#endif	//	__DATAFORMAT_H_INCLUDED__
