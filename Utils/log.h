/////////////////////////////////////////////////////////////////////////////
//  File    :   log.h
/////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

#if defined(WIN32)

#include <Windows.h>

#else

#include <sys/stat.h>
#include <sys/types.h>
#include <pthread.h>

#endif

#if !defined(_MAX_PATH)
#define _MAX_PATH 256
#endif

#if !defined(__LOG_H__)
#define      __LOG_H__

#define	LOG_VERSION	"1.0.0.0"

/////////////////////////////////////////////////////////////////////////////
//
class Log
{
public:
	Log();
	~Log();

	char *GetVersion() const                                {return LOG_VERSION;}

	struct param
	{
		bool isStr;
		int size;
		char nvalue[51];
		char *pstr;
	};

#if defined(WIN32)
	void SetLogFilePath(const char *logfilepath, HANDLE *hMutex = NULL);
#else
	void SetLogFilePath(const char *logfilepath);
#endif

	void SetPrefix(const char *pPrefix = NULL);

	bool LogMsg(const char *format,...);

private:

	char *m_pLogBuffer;
	char *m_pPrefix;
	FILE *m_pLogFd;
	char m_sLogFilePath[_MAX_PATH + 1];
};

#endif
