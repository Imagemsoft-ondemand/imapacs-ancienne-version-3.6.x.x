// Mailer.cpp: implementation of the Mailer class.
//
//////////////////////////////////////////////////////////////////////

#ifdef WIN32
#include "../stdafx.h"
#endif

#include "Mailer.h"
#include <string.h>

#include <stdarg.h>

#ifdef WIN32
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Mailer::Mailer()
{
	memset(m_mailFrom,0,sizeof(m_mailFrom));
	memset(m_mailTo,0,sizeof(m_mailTo));
	memset(m_serverIP,0,sizeof(m_serverIP));
	m_serverPort=0;

	memset(m_Subject,0,sizeof(m_Subject));
}

Mailer::~Mailer()
{

}

void Mailer::SetMailFrom(const char *mailAdress)
{
	strcpy(m_mailFrom, mailAdress);
	mailClient.SetFrom("ImaPacs", m_mailFrom);
}

void Mailer::SetMailTo(const char *mailAdress)
{
	strcpy(m_mailTo, mailAdress);
	mailClient.SetSendTo("support", m_mailTo);
}

void Mailer::SetMailServer(const char *serverIP)
{
	strcpy(m_serverIP, serverIP);
}

void Mailer::SetMailPort(unsigned int mailPort)
{
	m_serverPort = mailPort;
}

void Mailer::SetSubject(const char *subject)
{
	strcpy(m_Subject, subject);
}

bool Mailer::SendMail(char *subject, char *message)
{
	bool retval=false;
	if (mailClient.Connect(m_serverIP, m_serverPort))
	{
		if (subject)
			mailClient.SetSubject(subject);
		else
			mailClient.SetSubject(m_Subject);

		if (message)
			mailClient.SetMessageBody(message);
		else
		{
			mailClient.SetMessageBody(m_Body.GetBuffer());
			m_Body.Clear();
		}

		if (mailClient.SendMessage())
			retval=true;
		mailClient.EndSession();
	}
	
	return retval;
}

void Mailer::AddLine(char *line, ...)
{
	char tmp[5000]={0};;
	
	va_list	Varlist;
	va_start(Varlist, line);
	vsprintf(tmp, line, Varlist);

	m_Body += tmp;
	m_Body += "\r\n";
}

void Mailer::ClearMail()
{
	m_Body.Clear();
}

char *Mailer::GetLastError()
{
	return mailClient.GetLastError();
}
