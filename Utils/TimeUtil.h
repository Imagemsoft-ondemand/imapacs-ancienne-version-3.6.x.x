/////////////////////////////////////////////////////////////////////////////
//	TimeUtil.h
/////////////////////////////////////////////////////////////////////////////

#include <time.h>
#include <stdio.h>

#if !defined(__IMAPACS_TIMEUTIL_H_INCLUDED__)
#define      __IMAPACS_TIMEUTIL_H_INCLUDED__

/////////////////////////////////////////////////////////////////////////////
//
class Time
{
public:

	/////////////////////////////////////////////////////////////////////////
	//	Sets pTime with the current date
	//	and the format YYYYMMDD.
	//	NOTE : Under windows, this method is not
	//	       thread safe.
	static void GetDate(char *pTime)
	{
		time_t timer        = 0;
#ifdef WIN32
		struct tm *pCurTime = 0;

		time(&timer);

		pCurTime = localtime(&timer);
		if(pCurTime)
		{
			sprintf(pTime, "%04d%02d%02d", pCurTime->tm_year + 1900,
   				                           pCurTime->tm_mon + 1,
										   pCurTime->tm_mday);
		}
#else
		struct tm CurTime = {0};
		localtime_r(&timer, &CurTime);

		sprintf(pTime, "%04d%02d%02d", CurTime.tm_year + 1900,
			                           CurTime.tm_mon + 1,
									   CurTime.tm_mday);
#endif
	}

	/////////////////////////////////////////////////////////////////////////
	//	Sets pTime with the current time
	//	and the format HH24MMSS.
	//	NOTE : Under windows, this method is not
	//	       thread safe.
	static void GetTimeOfDay(char *pTime)
	{
		time_t timer        = 0;
#ifdef WIN32
		struct tm *pCurTime = 0;

		time(&timer);

		pCurTime = localtime(&timer);
		if(pCurTime)
		{
			sprintf(pTime, "%02d%02d%02d", pCurTime->tm_hour,
   				                           pCurTime->tm_min,
										   pCurTime->tm_sec);
		}
#else
		struct tm CurTime = {0};
		localtime_r(&timer, &CurTime);

		sprintf(pTime, "%02d%02d%02d", CurTime.tm_hour,
			                           CurTime.tm_min,
									   CurTime.tm_sec);
#endif
	}

	/////////////////////////////////////////////////////////////////////////
	//	Sets pTime with the current date
	//	and time using the format
	//	YYYYMMDDHH24MMSS.
	//	NOTE : Under windows, this method is not
	//	       thread safe.
	static void GetDateTime(char *pTime)
	{
		time_t timer        = 0;
#ifdef WIN32
		struct tm *pCurTime = 0;

		time(&timer);

		pCurTime = localtime(&timer);
		if(pCurTime)
		{
			sprintf(pTime, "%04d%02d%02d%02d%02d%02d", pCurTime->tm_year + 1900,
   													   pCurTime->tm_mon + 1,
													   pCurTime->tm_mday,
													   pCurTime->tm_hour,
													   pCurTime->tm_min,
													   pCurTime->tm_sec);
		}
#else
		struct tm CurTime = {0};
		localtime_r(&timer, &CurTime);

		sprintf(pTime, "%04d%02d%02d%02d%02d%02d", CurTime.tm_year + 1900,
												   CurTime.tm_mon + 1,
												   CurTime.tm_mday,
												   CurTime.tm_hour,
												   CurTime.tm_min,
												   CurTime.tm_sec);
#endif
	}
};

#endif	//	__IMAPACS_TIMEUTIL_H_INCLUDED__
