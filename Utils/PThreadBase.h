/////////////////////////////////////////////////////////////////////////////
//	PThreadBase.h
/////////////////////////////////////////////////////////////////////////////

#if !defined(__PTHREADBASE_H__)
#define      __PTHREADBASE_H__

#define	PTHREAD_VERSION	"1.0.0.0"

#if defined(WIN32)

#define _WINSOCKAPI_		//	Stops windows.h including winsock.h

	#include <Windows.h>
	#include <process.h>

/////////////////////////////////////////////////////////////////////////////
//
namespace util
{
	void ThreadProc(void *pParam);
}
#else
	#include <stdio.h>
	#include <stdlib.h>
	#include <time.h>
	#include <pthread.h>

/////////////////////////////////////////////////////////////////////////////
//
namespace util
{
	void *ThreadProc(void *pParam);
}
#endif

/////////////////////////////////////////////////////////////////////////////
//
namespace util
{

/////////////////////////////////////////////////////////////////////////////
//
class PThreadBase
{
public:
	PThreadBase();
	virtual ~PThreadBase();

	char *GetVersion() const                               {return PTHREAD_VERSION;}

	bool Start();
	bool Stop(bool bWait = true);

	bool WaitForThread();

	bool IsRunning() const {return m_bIsRunning;}
	
	virtual int Run() = 0;

protected:

	bool            m_bIsRunning;

#if defined(WIN32)
	unsigned int    m_hTreadHandle;
#else
	pthread_t       m_hTreadHandle;
#endif

};

}	//	End namespace tool.

#endif	//	__PTHREADBASE_H__
