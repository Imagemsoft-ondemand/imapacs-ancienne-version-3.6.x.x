/////////////////////////////////////////////////////////////////////////////
//	PMutex.cpp
/////////////////////////////////////////////////////////////////////////////

#include "PMutex.h"

/////////////////////////////////////////////////////////////////////////////
//
namespace util
{

/////////////////////////////////////////////////////////////////////////////
//
PMutex::PMutex()
{
#if defined(WIN32)
	m_hMutex = NULL;
#endif
}

/////////////////////////////////////////////////////////////////////////////
//
PMutex::~PMutex()
{
}

/////////////////////////////////////////////////////////////////////////////
//
bool PMutex::Create()
{
	bool bRet = true;
#if defined(WIN32)
	m_hMutex = CreateMutex(NULL,FALSE,NULL);
	if(!m_hMutex)
		bRet = false;
#else
	if(pthread_mutex_init(&m_hMutex,NULL))
		bRet = false;
#endif
	return bRet;
}

/////////////////////////////////////////////////////////////////////////////
//
bool PMutex::Lock()
{
	bool bRet = true;
#if defined(WIN32)
	if(WaitForSingleObject(m_hMutex,INFINITE) == WAIT_FAILED)
		bRet = false;
#else
	if(pthread_mutex_lock(&m_hMutex))
		bRet = false;
#endif
	return bRet;
}

/////////////////////////////////////////////////////////////////////////////
//
bool PMutex::Unlock()
{
	bool bRet = true;
#if defined(WIN32)
	if(!ReleaseMutex(m_hMutex))
		bRet = false;
#else
	if(pthread_mutex_unlock(&m_hMutex))
		bRet = false;
#endif
	return bRet;
}

/////////////////////////////////////////////////////////////////////////////
//
bool PMutex::Destroy()
{
	bool bRet = true;
#if defined(WIN32)
	if(m_hMutex)
	{
		if(!CloseHandle(m_hMutex))
		{
			bRet = false;
		}
		else
		{
			m_hMutex = NULL;
		}
	}
#else
	if(pthread_mutex_destroy(&m_hMutex))
	{
		bRet = false;
	}
#endif
	return bRet;
}

}	//	End namespace util.
