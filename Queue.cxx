///////////////////////////////////////////////////////////////////
//   File   :   Queue.cxx
//   Desc.  :   Implementation of the class Queue.
//   Author :   Laval Bolduc
//   Date   :   2005/02/21
//   Modif  :   2005/04/21
///////////////////////////////////////////////////////////////////

template <class DTYPE>
Queue<DTYPE>::Queue()
{
	m_iSize  = 0;
	m_pFirst = NULL;
	m_pLast  = NULL;
	m_pCur   = NULL;
}

template <class DTYPE>
Queue<DTYPE>::~Queue()
{
	Container<DTYPE> *pPos = NULL;
	while(m_pFirst)
	{
		pPos = m_pFirst;
		m_pFirst = m_pFirst->m_pNext;
		delete pPos;
	}
	pPos = NULL;
	m_iSize = 0;
	m_pLast = m_pCur = NULL;
}

///////////////////////////////////////////////////////////////////
// Add an element at the end of the list.
template <class DTYPE>
bool Queue<DTYPE>::Add(DTYPE &element)
{
	// Record current last element.
	Container<DTYPE> *tmp = m_pLast;

	// Chain on new element at the end of the list.
	if((m_pLast = new Container<DTYPE>) == NULL)
	{
		m_pLast = tmp;	// Restore element.
		return false;
	}

	m_pLast->m_Content = element;

	// Set new element's backward pointer to point to former
	// end-of-list element
	m_pLast->m_pPrev = tmp;

	// Set former end-of-list's next pointer to point to new element.
	if(tmp)
		tmp->m_pNext = m_pLast;
	else
		m_pFirst = m_pLast;		// First element.

	m_pCur = NULL;

	++m_iSize;
	return true;
}

///////////////////////////////////////////////////////////////////
// Add an element at the top of the list.
template <class DTYPE>
bool Queue<DTYPE>::Push(DTYPE &element)
{
	// Record current first element.
	Container<DTYPE> *tmp = m_pFirst;

	if((m_pFirst = new Container<DTYPE>) == NULL)
	{
		m_pFirst = tmp;	// Restore element.
		return false;
	}

	m_pFirst->m_Content = element;

	m_pFirst->m_pNext = tmp;

	if(tmp)
		tmp->m_pPrev = m_pFirst;
	else
		m_pLast = m_pFirst;

	m_pCur = NULL;

	++m_iSize;
	return true;
}

///////////////////////////////////////////////////////////////////
// Get the element at the top of the list. The element is
// removed from the list.
template <class DTYPE>
bool Queue<DTYPE>::Pop(DTYPE &element)
{
	// Record current first element.
	Container<DTYPE> *tmp = m_pFirst;

	if(m_pFirst == NULL)
		return false;

	element = m_pFirst->m_Content;

	m_pFirst = m_pFirst->m_pNext;

	if(m_pFirst)
		m_pFirst->m_pPrev = NULL;
	else
		m_pLast = NULL;
	
	--m_iSize;

	delete tmp;
	tmp = NULL;

	m_pCur = NULL;

	return true;
}

template <class DTYPE>
bool Queue<DTYPE>::GetCurrent(DTYPE &element)
{
	if(m_pCur == NULL)
		return false;

	element = m_pCur->m_Content;
	return true;
}

template <class DTYPE>
bool Queue<DTYPE>::Next()
{
	if(m_pCur)
	{
		m_pCur = m_pCur->m_pNext;
		if(m_pCur)
			return true;
	}
	return false;
}

///////////////////////////////////////////////////////////////////
// Get the first (top) element of the list. The element remains in
// the list.
template <class DTYPE>
bool Queue<DTYPE>::GetFirst(DTYPE &element)
{
	if(m_pFirst == NULL)
		return false;

	m_pCur = m_pFirst;
	element = m_pCur->m_Content;
	return true;
}

///////////////////////////////////////////////////////////////////
// Get the elementlocated after the one returned by the last call
// to GetFirst or GetNext. The element remains in the list.
template <class DTYPE>
bool Queue<DTYPE>::GetNext(DTYPE &element)
{
	if(m_pCur == NULL)
		return false;

	if((m_pCur = m_pCur->m_pNext) == NULL)
		return false;

	element = m_pCur->m_Content;
	return true;
}

///////////////////////////////////////////////////////////////////
// Removes the element accessed by the last call to GetFirst or
// GetNext. If there is no elment in the list, it returns false.
// NOTE : Once it has been called, the current position is lost
// and GetFirst and GetNext must be called again before using
// RemoveCurrent.
template <class DTYPE>
bool Queue<DTYPE>::RemoveCurrent()
{
	if(m_pCur == NULL)
		return false;

	if(m_pCur->m_pPrev)
		m_pCur->m_pPrev->m_pNext = m_pCur->m_pNext;
	else
		m_pFirst = m_pCur->m_pNext;

	if(m_pCur->m_pNext)
		m_pCur->m_pNext->m_pPrev = m_pCur->m_pPrev;
	else
		m_pLast = m_pCur->m_pPrev;

	delete m_pCur;
	m_pCur = NULL;

	--m_iSize;
	return true;
}

///////////////////////////////////////////////////////////////////
// Reset the list to an empty one.
template <class DTYPE>
void Queue<DTYPE>::Clear()
{
	Container<DTYPE> *pPos = NULL;
	while(m_pFirst)
	{
		pPos = m_pFirst;
		m_pFirst = m_pFirst->m_pNext;
		delete pPos;
	}
	pPos = NULL;
	m_pLast = m_pCur = NULL;
	m_iSize = 0;
}

///////////////////////////////////////////////////////////////////
//
template <class DTYPE>
bool Queue<DTYPE>::SetCurrentPosition(Position <DTYPE> &pos)
{
	if(!m_pCur)
		return false;

	if(!m_iSize)
		return false;

	if(!pos.m_pPosition)
		return false;

	m_pCur = pos.m_pPosition;
	return true;
}

///////////////////////////////////////////////////////////////////
//
template <class DTYPE>
bool Queue<DTYPE>::GetCurrentPosition(Position <DTYPE> &pos)
{
	if(!m_pCur)
	{
		pos.m_pPosition = NULL;
		return false;
	}
	pos.m_pPosition = m_pCur;

	return true;
}
