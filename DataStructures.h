/////////////////////////////////////////////////////////////////////////////
//	DataStructures.h
/////////////////////////////////////////////////////////////////////////////

#include "Queue.h"
#include <string.h>

#if !defined(__DATASTRUCTURES_H_INCLUDED__)
#define      __DATASTRUCTURES_H_INCLUDED__

namespace imapacs
{
	/////////////////////////////////////////////////////////////////////////////
	//
	enum BIND_LEVEL
	{
		BL_PATIENT = 0,
		BL_STUDY,
		BL_SERIES,
		BL_OBJECT
	};

	/////////////////////////////////////////////////////////////////////////////
	//	Container class to hold a SOP class/instance paire.
	class SOPClassInstancePaire
	{
	public:

		SOPClassInstancePaire(const char *pSOPClass, const char *pSOPInstance)
		{
			if(pSOPClass)
			{
				strncpy(sSOPClassUID, pSOPClass, 64);
				sSOPClassUID[64] = 0x00;
			}
			else
			{
				sSOPClassUID[0] = 0x00;
			}

			if(pSOPInstance)
			{
				strncpy(sSOPInstanceUID, pSOPInstance, 64);
				sSOPInstanceUID[64] = 0x00;
			}
			else
			{
				sSOPInstanceUID[0] = 0x00;
			}
		}

		char sSOPClassUID[65];
		char sSOPInstanceUID[65];
	};

	/////////////////////////////////////////////////////////////////////////////
	//	Data for N-ACTION storage commitment transaction.
	class StorageCommitmentActionReq
	{
	public:
		StorageCommitmentActionReq()
		{
			sTransactionUID[0] = 0x00;
			sCalledAeTitle[0]  = 0x00;
		}

		~StorageCommitmentActionReq()
		{
			SOPClassInstancePaire *pCur = 0;

			while(m_aObjectCheckList.Pop(pCur))
			{
				delete pCur;
			}
			pCur = 0;
		}

		char sTransactionUID[65];
		char sCalledAeTitle[17];
		Queue<SOPClassInstancePaire *> m_aObjectCheckList;
	};

	/////////////////////////////////////////////////////////////////////////////
	//	Data for N-ACTION transaction used to update GP SPS status.
	class UpdateSPSData
	{
	public:
		UpdateSPSData()
		{
			sTransactionUID[0]            = 0x00;
			sSPSStatus[0]                 = 0x00;
			sHumanPerformerCode[0]        = 0x00;
			sHumanPerformerCodeScheme[0]  = 0x00;
			sHumanPerformerCodeMeaning[0] = 0x00;
			sPerformerOrganization[0]     = 0x00;
			sPerformerName[0]             = 0x00;
		}
		virtual ~UpdateSPSData()
		{
		}

		char sTransactionUID[65];				//	0x0008, 0x1195
		char sSPSStatus[17];					//	0x0040, 0x4001
		char sHumanPerformerCode[17];			//	0x0008, 0x0100 in seq. 0x0040, 0x4009 in seq. 0x0040, 0x4035.
		char sHumanPerformerCodeScheme[17];		//	0x0008, 0x0102 in seq. 0x0040, 0x4009 in seq. 0x0040, 0x4035.
		char sHumanPerformerCodeMeaning[65];	//	0x0008, 0x0104 in seq. 0x0040, 0x4009 in seq. 0x0040, 0x4035.
		char sPerformerOrganization[65];		//	0040,4036 in seq. 0x0040, 0x4035.
		char sPerformerName[65];				//	0x0040, 0x4037 in seq. 0x0040, 0x4035.
	};

	class CreatePPSData
	{
	public:
		CreatePPSData()
		{
			sPatientName[0]           = 0x00;
			sPatientId[0]             = 0x00;
			sPatientBirthDate[0]      = 0x00;
			sPatientSex[0]            = 0x00;

			sWorkitemCode[0]          = 0x00;

			sPPSInstanceUID[0]        = 0x00;
			sPPSId[0]                 = 0x00;
			sPPSStartDate[0]          = 0x00;
			sPPSStartTime[0]          = 0x00;
			sPPSEndDate[0]            = 0x00;
			sPPSEndTime[0]            = 0x00;
			sPPSStatus[0]             = 0x00;
			sRefSPSSOPClassUID[0]     = 0x00;
			sRefSPSSOPInstanceUID[0]  = 0x00;
			sTransactionUID[0]        = 0x00;
			sPerformerId[0]           = 0x00;
			sPerformerName[0]         = 0x00;
			sPerformerOrganization[0] = 0x00;
			sPerformedStationName[0]  = 0x00;
		}
		virtual ~CreatePPSData()
		{}

		char sPatientName[65];
		char sPatientId[65];
		char sPatientBirthDate[33];
		char sPatientSex[9];

		char sWorkitemCode[17];

		char sPPSInstanceUID[65];
		char sPPSId[17];
		char sPPSStartDate[33];
		char sPPSStartTime[33];
		char sPPSEndDate[33];
		char sPPSEndTime[33];
		char sPPSStatus[17];
		char sRefSPSSOPClassUID[65];
		char sRefSPSSOPInstanceUID[65];
		char sTransactionUID[65];
		char sPerformerId[17];
		char sPerformerName[65];
		char sPerformerOrganization[65];
		char sPerformedStationName[17];
	};

	/////////////////////////////////////////////////////////////////////////////
	//	Data for patient informations.
	typedef struct _PatientData
	{
		char sPatientId[65];				//	0x0010, 0x0020
		char sPatientName[81];				//	0x0010, 0x0010
		char sPatientSex[11];				//	0x0010, 0x0040
		char sPatientBirthDate[33];			//	0x0010, 0x0030
		char sPatientBirthTime[33];			//	0x0010, 0x0032
		char sPatientOtherId[65];			//	0x0010, 0x1000

	} PatientData;

	/////////////////////////////////////////////////////////////////////////////
	//	Data for study informations.
	typedef struct _StudyData
	{
		char sStudyInstanceUID[65];			//	0x0020, 0x000d
		char sStudyDesc[65];				//	0x0008, 0x1030
		char sStudyId[17];					//	0x0020, 0x0010
		char sStudyRefPhysicianName[65];	//	0x0008, 0x0090
		char sPhysicianReadingStudy[65];	//	0x0008, 0x1060
		char sStorageId[65];
		char sStudyDate[33];				//	0x0008, 0x0020
		char sStudyTime[33];				//	0x0008, 0x0030
		char sAccessionNumber[17];			//	0x0008, 0x0050
		char sPatientAge[11];				//	0x0010, 0x1010
		char sPatientSize[11];				//	0x0010, 0x1020
		char sPatientWeight[11];			//	0x0010, 0x1030
		char sInstitutionName[65];			//  0x0008, 0x0080
	//	char sModalitiesInStudy[65];		//	0x0008, 0x0061
		char sSendingAeTitle[17];			//	AE Title of the application
											//	sending and image.

	} StudyData;

	/////////////////////////////////////////////////////////////////////////////
	//	Data for object as well as some relevant series informations.
	typedef struct _ObjectData
	{
		char sSopClassUID[65];				//	0x0008, 0x0016
		char sSerieInstanceUID[65];			//	0x0020, 0x000e
		char sSOPInstanceUID[65];			//	0x0008, 0x0018
		char sSerieNumber[21];				//	0x0020, 0x0011
		char sModality[11];					//	0x0008, 0x0060
		char sInstanceNumber[21];			//	0x0020, 0x0013
		char sSerieDate[33];				//	0x0008, 0x0021
		char sSerieTime[33];				//	0x0008, 0x0031
		char sSerieDescription[65];			//	0x0008, 0x103e
		char sViewPos[17];					//	0x0018, 0x5101
		char sLaterality[17];				//	0x0020, 0x0060
		char sBodyPart[17];					//	0x0018, 0x0015
		char sObjectType[21];
		char sObjectDesc[65];
		char sStationName[65];				//	0x0008, 0x1010
		char sAeTitle[17];

	} ObjectData;

	/////////////////////////////////////////////////////////////////////////////
	//	C-FIND query root.
	#define	PATIENT_ROOT			0
	#define	STUDY_ROOT				1
	#define SERIES_ROOT				2
	#define IMAGE_ROOT				3
	#define PATIENT_STUDY_ROOT		4
	#define QRY_WORKLIST			5

	/////////////////////////////////////////////////////////////////////////////
	//	C-FIND query level.
	#define	PATIENT_LEVEL			6
	#define	STUDY_LEVEL				7
	#define SERIES_LEVEL			8
	#define IMAGE_LEVEL				9

	/////////////////////////////////////////////////////////////////////////////
	//
	#define PATIENT_LEVEL_STRING    "PATIENT"
	#define STUDY_LEVEL_STRING      "STUDY"
	#define SERIE_LEVEL_STRING      "SERIES"
	#define IMAGE_LEVEL_STRING      "IMAGE"

	/////////////////////////////////////////////////////////////////////////////
	//
	class TagData
	{
	public:
		TagData()
		{
			Data[0] = 0x00;
			bReturn = false;
		}

		char Data[129];
		bool bReturn;
	};

	/////////////////////////////////////////////////////////////////////////////
	//
	class FindData
	{
	public:
		FindData():bHasScaleFactors(false),
			       fXscaleFactor(-1.0f),
			       fYscaleFactor(-1.0f)
		{
			sQLevel[0]         = 0x00;
		}

		virtual ~FindData()
		{
		}

		char sQLevel[17];

		/////////////////////////////////////////////////////////////////////////
		//	Patient fields.
		TagData PatId;
		TagData PatOtherId;
		TagData PatName;
		TagData PatFirstName;
		TagData PatLastName;
		TagData PatOtherName;
		TagData PatBirthDate;
		TagData PatBirthTime;
		TagData PatSex;
		TagData PatAge;
		TagData PatSize;
		TagData PatWeight;
		TagData PatMotherName;

		/////////////////////////////////////////////////////////////////////////
		//	Study fields.
		TagData StudyInstanceUID;
		TagData StudyAccessionNumber;
		TagData StudyId;
		TagData StudyAdmDiag;
		TagData StudyReadPhysicianName;
		TagData StudyRefPhysicianName;
		TagData StudyDate;
		TagData StudyTime;
		TagData StudyDesc;
		TagData StudyModalitiesInStudy;		//	0x0008, 0x0061
		TagData StudyOtherNumber; // [Ref:0000722]

		/////////////////////////////////////////////////////////////////////////
		//	Series fields.
		TagData SeriesInstanceUID;
		TagData SeriesNumber;
		TagData SeriesDesc;
		TagData SeriesBodyPart;
		TagData SeriesLaterality;
		TagData SeriesDate;
		TagData SeriesTime;
		TagData SeriesModality;
		TagData SeriesViewPosition;		//	0x0018, 0x5101

		/////////////////////////////////////////////////////////////////////////
		//	Object fields.
		TagData ObjectInstanceUID;
		TagData ObjectSopClasUID;
		TagData ObjectInstanceNumber;
		TagData ObjectAeTitle;
		TagData InstanceNumber;
		TagData ObjectAcquisitionDate;	//	0x0008, 0x0022
		TagData ObjectAcquisitionTime;	//	0x0008, 0x0032

		/////////////////////////////////////////////////////////////////////////
		//	Private tags.
		bool    bHasScaleFactors;
		float   fXscaleFactor;
		float   fYscaleFactor;
	};

	/////////////////////////////////////////////////////////////////////////////
	//
	class Reference
	{
	public:
		Reference(const char *pSOPClassUID = NULL, const char *pSOPInstanceUID = NULL)
		{
			if(pSOPClassUID)
			{
				strcpy(SOPClassUID, pSOPClassUID);
			}
			else
			{
				SOPClassUID[0] = 0x00;
			}

			if(pSOPInstanceUID)
			{
				strcpy(SOPInstanceUID, pSOPInstanceUID);
			}
			else
			{
				SOPInstanceUID[0] = 0x00;
			}
		}

		virtual ~Reference() {}

		char SOPClassUID[65];
		char SOPInstanceUID[65];
	};

	/////////////////////////////////////////////////////////////////////////////
	//
	typedef struct _MoveResult
	{
		char QueryLevel[17];
		char StudyInstanceUID[65];
		char AccessionNumber[17];
		char SeriesNumber[17];
		char InstanceNumber[17];
		char SOPClassUID[65];
		char SOPInstanceUID[65];
	} MoveResult;

	/////////////////////////////////////////////////////////////////////////////
	//
	typedef struct _WLData
	{
		char PatientName[65];
		char PatientFirstName[65];
		char PatientLastName[65];
		char PatientId[17];
		char PatientBirthDate[33];
		char PatientBirthTime[33];
		char PatientSex[4];
		char ScheduledStationAeTitle[17];
		char ScheduledProcedureStepStartDate[33];
		char ScheduledProcedureStepStartTime[33];
		char ScheduledProcedureStepEndDate[33];
		char ScheduledProcedureStepEndTime[33];
		char Modality[11];
		char ScheduledPerformingPhysicianName[65];
		char ScheduledProcedureStepDescription[65];
		char ScheduledProcedureStepLocation[17];
		char PreMedication[65];
		char ScheduledProcedureStepId[17];
		char RequestedContrastAgent[65];
		char ScheduledProcedureStepStatus[17];
		char RequestedProcedureId[17];
		char RequestedProcedureCode[65];
		char RequestedProcedureDescription[65];
		char RequestedProcedureLaterality[65];
		char RequestedProcedureInternalCode[65];
		char StudyInstanceUID[65];
		char AccesionNumber[17];
		char RequestingPhysician[65];
		char ReferringPhysicianName[65];
		char VisitUID[65];
		char sSpsModality[17];

	} WLData;

	/////////////////////////////////////////////////////////////////////////////
	//
	class GPWLData
	{
	public:
		GPWLData()
		{
		}
		virtual ~GPWLData() 
		{
			TagData *pCur = NULL;
			while(m_sReferencedStudies.Pop(pCur))
			{
				delete pCur;
				pCur = NULL;
			}
		}

		TagData m_SOPClassUID;						//	0x0008, 0x0016
		TagData m_SOPInstanceUID;					//	0x0008, 0x0018
		TagData m_GPProcedureStepStatus;			//	0x0040, 0x4001
		TagData m_GPScheduledProcStepPriority;		//	0x0040, 0x4003
		TagData m_InputAvailabilityFlag;			//	0x0040, 0x4020
		TagData m_ScheduledProcStepId;				//	0x0040, 0x0009
		TagData m_SchProcStepStartDate;				//	0x0040, 0x4005

		//	Patient informations.
		TagData m_PatientName;						//	0x0010, 0x0010
		TagData m_PatientId;						//	0x0010, 0x0020
		TagData m_PatientBirthDate;					//	0x0010, 0x0030
		TagData m_PatientSex;						//	0x0010, 0x0040
		TagData m_PatMother;						//	0x0010, 0x0060	//[Ref:0000722]
		TagData m_PatOID;							//	0x0010, 0x1000  //[Ref:0000722]
		TagData m_PatFirstName;
		TagData m_PatLastName;

		//	Study informations.
		TagData m_StudyInstanceUID;					//	0x0020, 0x000D and the one in seq. 0x0040, 0xA370
		TagData m_ReqProcDescription;				//	0x0032, 0x1060 in seq. 0x0040, 0xA370
		TagData m_ReqProcCodeValue;					//	0x0008, 0x0100 in seq. 0x032, 0x1064
		TagData m_ReqProcCodeScheme;				//	0x0008, 0x0102 in seq. 0x032, 0x1064
		TagData m_ReqProcCodeMeaning;				//	0x0008, 0x0104 in seq. 0x032, 0x1064
		TagData m_AccessionNumber;					//	0x0008, 0x0050 in seq. 0x0040, 0xA370
		TagData m_ModalitiesInStudy;				//	0x0008, 0x0061 in seq. 0x0040, 0xA370
		TagData m_StudyDate;						//	0x0008, 0020
		TagData m_StudyTime;						//	0x0008, 0030
		TagData m_StudyReferingPhysician;			//	0x0008, 0x0090	//[Ref:0000722]

		Queue <TagData *> m_sReferencedStudies;		//	0x0008, 0x1110 in seq. 0x0040, 0xA370

		//	Workitem code.
		TagData m_WorkItemCodeValue;				//	0x0008, 0x0100 in seq. 0x0040, 0x4018
		TagData m_WorkItemCodeScheme;				//	0x0008, 0x0102 in seq. 0x0040, 0x4018
		TagData m_WorkItemCodeMeaning;				//	0x0008, 0x0104 in seq. 0x0040, 0x4018

		//	Scheduled performer.
		TagData m_SchPerformerCodeValue;			//	0x0008, 0x0100 in seq. 0x0040, 0x4009
		TagData m_SchPerformerCodeScheme;			//	0x0008, 0x0102 in seq. 0x0040, 0x4009
		TagData m_SchPerformerCodeMeaning;			//	0x0008, 0x0104 in seq. 0x0040, 0x4009

		//	Scheduled station name.
		TagData m_SchStationNameCodeValue;			//	0x0008, 0x0100 in seq. 0x0040, 0x4025
		TagData m_SchStationNameCodeScheme;			//	0x0008, 0x0102 in seq. 0x0040, 0x4025
		TagData m_SchStationNameCodeMeaning;		//	0x0008, 0x0104 in seq. 0x0040, 0x4025

		// Actual Performer [Ref:0002233]
		TagData m_ActualPerformerCodeValue;			//	0x0008, 0x0104 in seq. 0x0040, 0x4035
		TagData m_ActualPerformerCodeMeaning;		//	0x0008, 0x0104 in seq. 0x0040, 0x4035
	};

	/////////////////////////////////////////////////////////////////////////
	//	Represent and element of the scheduled step attribute sequence.
	class ScheduledStepAttribute
	{
	public:
		ScheduledStepAttribute()
		{
			sAccessionNumber[0]      = 0x00;
			sStudyInstanceUID[0]     = 0x00;
			sReqProcDescription[0]   = 0x00;
			sScheProcStepDesc[0]     = 0x00;
			sSPSID[0]                = 0x00;
			sRequestedProcedureID[0] = 0x00;
		}

		ScheduledStepAttribute(const ScheduledStepAttribute &other)
		{
			sAccessionNumber[0]      = 0x00;
			sStudyInstanceUID[0]     = 0x00;
			sReqProcDescription[0]   = 0x00;
			sScheProcStepDesc[0]     = 0x00;
			sSPSID[0]                = 0x00;
			sRequestedProcedureID[0] = 0x00;

			strcpy(sAccessionNumber, other.sAccessionNumber);
			strcpy(sStudyInstanceUID, other.sStudyInstanceUID);
			strcpy(sReqProcDescription, other.sReqProcDescription);
			strcpy(sSPSID, other.sSPSID);
			strcpy(sRequestedProcedureID, other.sRequestedProcedureID);
		}

		~ScheduledStepAttribute()
		{}

		char sAccessionNumber[17];		//	0x0008, 0x0050
		char sStudyInstanceUID[65];		//	0x0020, 0x000d
		char sReqProcDescription[65];	//	0x0032, 0x1060
		char sScheProcStepDesc[65];		//	0x0040, 0x0007
		char sSPSID[17];				//	0x0040, 0x0009
		char sRequestedProcedureID[17];	//	0x0040, 0x1001
	};

	/////////////////////////////////////////////////////////////////////////
	//	Represents an element of the performed series sequence.
	class PerformedSerie
	{
	public:
		PerformedSerie()
		{
			sOperatorName[0]     = 0x00;
			sProtocolName[0]     = 0x00;
			sSerieInstanceUID[0] = 0x00;
			sSerieDescription[0] = 0x0;
		}

		PerformedSerie(const PerformedSerie &other)
		{
			strcpy(sOperatorName, other.sOperatorName);
			strcpy(sProtocolName, other.sProtocolName);
			strcpy(sSerieInstanceUID, other.sSerieInstanceUID);
			strcpy(sSerieDescription, other.sSerieDescription);
		}

		~PerformedSerie()
		{}

		char sOperatorName[65];					//	0x0008, 0x1070
		char sProtocolName[65];					//	0x0018, 0x1030
		char sSerieInstanceUID[65];				//	0x0020, 0x000E
		char sSerieDescription[65];				//	0x0008, 0x103E
	};

	/////////////////////////////////////////////////////////////////////////
	//	Modality performed procedure step data.
	class MPPSData
	{
	public:
		MPPSData()
		{
			sTransactionUID[0]          = 0x00;
			sPatientName[0]             = 0x00;
			sPatientId[0]               = 0x00;
			sPerformedStationAeTitle[0] = 0x00;
			sPerformedStationName[0]    = 0x00;
			sPerformerLocation[0]       = 0x00;
			sPPSStartDate[0]            = 0x00;
			sPPSStartTime[0]            = 0x00;
			sPPSEndDate[0]              = 0x00;
			sPPSEndTime[0]              = 0x00;
			sPPSStatus[0]               = 0x00;
			sPPSId[0]                   = 0x00;
			sPPSDescription[0]          = 0x00;
			sPPSTypeDescription[0]      = 0x00;
			sPPSComment[0]              = 0x00;
			sModality[0]                = 0x00;
			sStudyId[0]                 = 0x00;
		}
		virtual ~MPPSData()
		{
			ScheduledStepAttribute *pElement = NULL;
			while(maScheStepSttribSeq.Pop(pElement))
			{
				delete pElement;
			}
			pElement = NULL;
		}

		char sTransactionUID[65];
		char sPatientName[65];					//	0x0010, 0x0010
		char sPatientId[65];					//	0x0010, 0x0020
		char sPerformedStationAeTitle[17];		//	0x0040, 0x0241
		char sPerformedStationName[17];			//	0x0040, 0x0242
		char sPerformerLocation[17];			//	0x0040, 0x0243
		char sPPSStartDate[33];					//	0x0040, 0x0244
		char sPPSStartTime[33];					//	0x0040, 0x0245
		char sPPSEndDate[33];					//	0x0040, 0x0250
		char sPPSEndTime[33];					//	0x0040, 0x0251
		char sPPSStatus[17];					//	0x0040, 0x0252
		char sPPSId[17];						//	0x0040, 0x0253
		char sPPSDescription[65];				//	0x0040, 0x0254
		char sPPSTypeDescription[65];			//	0x0040, 0x0255
		char sPPSComment[1025];					//	0x0040, 0x0280

		char sModality[17];						//	0x0008, 0x0060
		char sStudyId[17];						//	0x0020, 0x0010

		void AddPerScheStepAttr(const ScheduledStepAttribute &pSSAttrib)
		{
			ScheduledStepAttribute *pElement = new ScheduledStepAttribute(pSSAttrib);
			if(pElement)
			{
				maScheStepSttribSeq.Add(pElement);
			}
		}
		Queue <ScheduledStepAttribute *> maScheStepSttribSeq;	//	0x0040, 0x0270
	};

	/////////////////////////////////////////////////////////////////////////
	//
	class PresentationContext
	{
	public:
		PresentationContext()
		{}
		~PresentationContext()
		{
			char *pCur = NULL;
			while(aTransferSyntaxes.Pop(pCur))
			{
				delete pCur;
			}
			pCur = NULL;
		}

		char           sSOPClassUID[65];	//	Abstract syntax.
		Queue <char *> aTransferSyntaxes;
	};
}
#endif	//	__DATASTRUCTURES_H_INCLUDED__
