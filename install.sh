#!/bin/sh
#Run like this: sudo bash ./install.sh
#If an instance of the PACS server is already running, it
#should be stopped first.

###################################################################################################
#   Creates the configuration file from use's inputs.
#
create_configuration_file()
{
    echo -e "\nConfiration\n\n---------------------------------------"



    echo -n "Enter database host (ip address) : "

    read dbhost

    echo -n "Enter database port : "

    read dbport

    echo -n "Enter database source : "

    read dbsource

    echo -n "Enter database user : "

    read dbuser

    echo -n "Enter database password : "

    read dbpassword

    echo -n "Enter server's ae title : "

    read serveraetitle

    echo -n "Enter pacs listening port : "

    read port

    echo -n "Enter pacs transient listening port : "

    read transientport

    echo -e "dbhost=$dbhost" > $1
    echo -e "dbport=$dbport" >> $1
    echo -e "dbsource=$dbsource" >> $1
    echo -e "dbuser=$dbuser" >> $1
    echo -e "dbpassword=$dbpassword" >> $1
    echo -e "dbpackagename=PKG_PACS" >> $1
    echo -e "serveraetitle=$serveraetitle" >> $1
    echo -e "logpath=./log" >> $1
    echo -e "tmppath=./tmpimg" >> $1
    echo -e "port=$port" >> $1
    echo -e "porttransient=$transientport" >> $1
}

###################################################################################################
#   Installs the server assuming the directories are created.
#
install_pacs()
{
    cp ./dicom.dic $1
    cp ./Imapacs $1
    cp ./imapacs.service /etc/systemd/system/
    cp ./imapacs_transient.service /etc/systemd/system/
    cp ./uninstall.sh $1
    cp stop.sh $1
    cp -Rf ./Oracle $1

    chmod -R 755 $1
    chown -R imagem /opt/imagem

    #   Give the application the permission to bind ports bellow 1024.
    #
    setcap 'cap_net_bind_service=+ep' $1/Imapacs

    #   Add the Oracle instant client directory to the dynamic loader
    #   so it will be found by the OS at application startup.
    #
    echo "$1/Oracle" > /etc/ld.so.conf.d/imapacs.conf

    ldconfig

    #   Reload systemd configuration.
    #
    systemctl daemon-reload

    #systemctl enable imapacs.service
    #systemctl enable imapacs_transient.service
}

###################################################################################################
#
clear

echo -e "\n----------------------------------------"
echo -e "Imapacs installation ..."
echo -e "----------------------------------------\n"

INSTALL_DIR="/opt/imagem/imapacs"
CONFIGURATION_FILE="$INSTALL_DIR/Imapacs.cfg"

if [ -d $INSTALL_DIR ]
then
    if [ -f $CONFIGURATION_FILE ]
    then
        echo -n "Keep existsing configuration file ? (y/n) "

        read resp;

        if [ $resp = "n" ]
        then
            rm -f $CONFIGURATION_FILE

            install_pacs $INSTALL_DIR

            create_configuration_file $CONFIGURATION_FILE
        else
            install_pacs $INSTALL_DIR
        fi
    else
        install_pacs $INSTALL_DIR

        create_configuration_file $CONFIGURATION_FILE
    fi
else
    mkdir -p $INSTALL_DIR
    echo -e "Creating install directory $INSTALL_DIR.\n"

    echo -e "Creating log directory $INSTALL_DIR.\n"
    mkdir -p $INSTALL_DIR/log

    install_pacs $INSTALL_DIR

    create_configuration_file $CONFIGURATION_FILE
fi

echo -e "\n\n"

echo -n "Do you want to start PACS server ? (y/n) "
read resp

if [ $resp = "y" ]
then
    systemctl start imapacs.service
fi

echo -n "Do you want to start PACS transient server ? (y/n) "
read resp

echo -e "\n\n"

if [ $resp = "y" ]
then
    systemctl start imapacs_transient.service
fi
