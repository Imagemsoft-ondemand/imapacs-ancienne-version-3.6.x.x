/////////////////////////////////////////////////////////////////////////////
//	File		:	PacsServerWindowDlg.cpp
//	Description	:	Implementation.
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "PacsServerWindow.h"
#include "PacsServerWindowDlg.h"
#include "../Utils/PacsCommon.h"
#include "AboutDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

/////////////////////////////////////////////////////////////////////////////
//
CPacsServerWindowDlg::CPacsServerWindowDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPacsServerWindowDlg::IDD, pParent),
	  m_bVisible(false), m_iPort(0)
	  , m_bTransient(FALSE)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

/////////////////////////////////////////////////////////////////////////////
//
void CPacsServerWindowDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

/////////////////////////////////////////////////////////////////////////////
//
BEGIN_MESSAGE_MAP(CPacsServerWindowDlg, CDialog)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDOK, &CPacsServerWindowDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CPacsServerWindowDlg::OnBnClickedCancel)
	ON_WM_WINDOWPOSCHANGING()
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
//	CPacsServerWindowDlg message handlers
BOOL CPacsServerWindowDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	BOOL bRetVal = FALSE;

	//	Set the icon for this dialog. The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// Change our window style so it won't show in the taskbar.
	ModifyStyleEx(WS_EX_APPWINDOW,0);

	//Create tray entry.
	if(m_Tray.Create(NULL,
		             WM_ICON_NOTIFY,
				     _T("Imapacs"),
				     m_hIcon,
				     IDR_TRAY_MENU,
				     FALSE,
				     NULL,
				     NULL,
				     0,
				     0))
	{
		m_Tray.SetTargetWnd(FromHandle(m_hWnd));
		m_Tray.SetMenuDefaultItem(3, TRUE);

		if(LoadConfig())
		{
			UpdateInterface();

			if(!m_Server.Start())
			{
				AfxMessageBox("Erreur : impossible de d�marrer le serveur.");

				m_bVisible = true;
				ShowWindow(SW_SHOW);
			}
		}
		else
		{
			m_bVisible = true;
			ShowWindow(SW_SHOW);
		}
		bRetVal = TRUE;
	}
	return bRetVal;  // return TRUE  unless you set the focus to a control
}

/////////////////////////////////////////////////////////////////////////////
//	If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.
void CPacsServerWindowDlg::OnPaint()
{
	if(IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

/////////////////////////////////////////////////////////////////////////////
//	The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CPacsServerWindowDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

/////////////////////////////////////////////////////////////////////////////
//
void CPacsServerWindowDlg::OnWindowPosChanging(WINDOWPOS* lpwndpos)
{
	CDialog::OnWindowPosChanging(lpwndpos);

	// Dialog will not be visible at startup.
	if(!m_bVisible)
		lpwndpos->flags &= ~SWP_SHOWWINDOW;
}

/////////////////////////////////////////////////////////////////////////////
//
BOOL CPacsServerWindowDlg::OnCommand(WPARAM wParam, LPARAM lParam)
{
	switch(wParam)
	{
		case ID_START:
		{
			//	Start the server.
			if(LoadConfig())
			{
				if(!m_Server.Start())
				{
					ShowWindow(SW_SHOW);
				}
			}
			else
			{
				ShowWindow(SW_SHOW);
			}
		}
		break;

		case ID_STOP:
		{
			//	Stop the server and wait.
			m_Server.Stop();
		}
		break;

		case ID_CFG:
		{
			//	Show the dialog for configuration.
			m_bVisible = true;
			ShowWindow(SW_SHOW);
		}
		break;

		case ID_ABOUT:
		{
			//	Make an about dialog.
			CString sAbout;
			sAbout.Format("\n%s\n%s\n%s\n%s", PRODUCT_FAMILY, APP_NAME, IMAPACS_VERSION, ABOUT_MESSAGE);

			CAboutDlg dlg(NULL, sAbout);

			dlg.DoModal();
		}
		break;

		case ID_QUIT:
		{
			//	Stop the server and wait.
			m_Server.Stop();
			::PostMessage(m_hWnd,WM_QUIT,0,0);
		}
		break;
	}
	return CDialog::OnCommand(wParam, lParam);
}

/////////////////////////////////////////////////////////////////////////////
//
void CPacsServerWindowDlg::OnBnClickedOk()
{
	UpdateVariables();

	if(SaveConfig())
	{
		m_Server.Stop();
		if(m_Server.Start())
		{
			ShowWindow(SW_HIDE);
		}
		else
		{
			ShowWindow(SW_SHOW);
		}
	}
	else
	{
		AfxMessageBox("Erreur : impossible de sauvegarder la configuration.");
	}
}

/////////////////////////////////////////////////////////////////////////////
//
void CPacsServerWindowDlg::OnBnClickedCancel()
{
	m_bVisible = false;
	ShowWindow(SW_HIDE);
}

/////////////////////////////////////////////////////////////////////////////
//
bool CPacsServerWindowDlg::LoadConfig()
{
	bool bRetVal    = false;
	char sLine[513] = {0};
	char *pTmp      = 0;

	FILE *fp = fopen("./Imapacs.cfg", "r");
	if(fp)
	{
		while(fgets(sLine, 512, fp))
		{
			pTmp = strpbrk(sLine, "=");
			if(pTmp)
			{
				pTmp[strlen(pTmp) - 1] = 0x00;
				if(strncmp(sLine, "dbsource", 8) == 0)
				{
					m_DbSource = &pTmp[1];
				}
				else if(strncmp(sLine, "dbuser", 6) == 0)
				{
					m_DbUser = &pTmp[1];
				}
				else if(strncmp(sLine, "dbpassword", 10) == 0)
				{
					m_DbPassword = &pTmp[1];
				}
				else if(strncmp(sLine, "serveraetitle", 13) == 0)
				{
					m_SrvAeTitle = &pTmp[1];
				}
				else if(strncmp(sLine, "dbpackagename", 13) == 0)
				{
					m_DbScheme = &pTmp[1];
				}
				else if(strncmp(sLine, "logpath", 7) == 0)
				{
					m_LogPath = &pTmp[1];
				}
				else if(strncmp(sLine, "tmppath", 7) == 0)
				{
					m_TmpPath = &pTmp[1];
				}
				else if(strncmp(sLine, "port", 4) == 0)
				{
					m_iPort = (unsigned short)atoi(&pTmp[1]);
				}
				else if(strncmp(sLine, "istransient", 11) == 0)
				{
					if(atoi(&pTmp[1]) == 1)
						m_bTransient = TRUE;
					else
						m_bTransient = FALSE;
				}
			}
		}
		fclose(fp);
		fp = 0;

		bRetVal = true;
	}
	return bRetVal;
}

/////////////////////////////////////////////////////////////////////////////
//
bool CPacsServerWindowDlg::SaveConfig()
{
	bool bRetVal = false;

	FILE *fp = fopen("./Imapacs.cfg", "w");
	if(fp)
	{
		fprintf(fp, "dbsource=%s\n", m_DbSource);
		fprintf(fp, "dbuser=%s\n", m_DbUser);
		fprintf(fp, "dbpassword=%s\n", m_DbPassword);
		fprintf(fp, "dbpackagename=%s\n", m_DbScheme);
		fprintf(fp, "serveraetitle=%s\n", m_SrvAeTitle);
		fprintf(fp, "logpath=%s\n", m_LogPath);
		fprintf(fp, "tmppath=%s\n", m_TmpPath);
		fprintf(fp, "port=%d\n", m_iPort);
		fprintf(fp, "istransient=%d\n", m_bTransient);

		fclose(fp);
		fp = 0;

		bRetVal = true;
	}
	return bRetVal;
}

/////////////////////////////////////////////////////////////////////////////
//	Sets the gui's items with the content of the variables.
void CPacsServerWindowDlg::UpdateInterface()
{
	char sPort[17] = {0};

	SetDlgItemText(IDC_DB_SOURCE, m_DbSource);
	SetDlgItemText(IDC_DB_USER, m_DbUser);
	SetDlgItemText(IDC_DB_PASSWORD, m_DbPassword);
	SetDlgItemText(IDC_DB_SCHEME, m_DbScheme);
	SetDlgItemText(IDC_SRV_AE_TITLE, m_SrvAeTitle);
	SetDlgItemText(IDC_SRV_LOG_PATH, m_LogPath);
	SetDlgItemText(IDC_SRV_LOG_TMP_PATH, m_TmpPath);

	sprintf(sPort, "%d", m_iPort);
	SetDlgItemText(IDC_SRV_LOG_PORT, sPort);

	((CButton*)GetDlgItem(IDC_CHECK_IS_TRANSIENT))->SetCheck(m_bTransient);
}

/////////////////////////////////////////////////////////////////////////////
//
void CPacsServerWindowDlg::UpdateVariables()
{
	char sValue[256] = {0};

	GetDlgItemText(IDC_DB_SOURCE, sValue, 255);
	m_DbSource = sValue;

	GetDlgItemText(IDC_DB_USER, sValue, 255);
	m_DbUser = sValue;

	GetDlgItemText(IDC_DB_PASSWORD, sValue, 255);
	m_DbPassword = sValue;

	GetDlgItemText(IDC_DB_SCHEME, sValue, 255);
	m_DbScheme = sValue;

	GetDlgItemText(IDC_SRV_AE_TITLE, sValue, 255);
	m_SrvAeTitle = sValue;

	GetDlgItemText(IDC_SRV_LOG_PATH, sValue, 255);
	m_LogPath = sValue;

	GetDlgItemText(IDC_SRV_LOG_TMP_PATH, sValue, 255);
	m_TmpPath = sValue;

	GetDlgItemText(IDC_SRV_LOG_PORT, sValue, 255);
	m_iPort = (unsigned short)atoi(sValue);

	m_bTransient = ((CButton*)GetDlgItem(IDC_CHECK_IS_TRANSIENT))->GetCheck();
}
