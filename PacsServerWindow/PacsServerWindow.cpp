/////////////////////////////////////////////////////////////////////////////
//	File		:	PacsServerWindow.cpp
//	Description	:	Implementation.
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "PacsServerWindow.h"
#include "PacsServerWindowDlg.h"
#include "../DICOMSession.h"

#include <Windows.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

BEGIN_MESSAGE_MAP(CPacsServerWindowApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
//	CPacsServerWindowApp construction
CPacsServerWindowApp::CPacsServerWindowApp()
{
}

/////////////////////////////////////////////////////////////////////////////
//	The one and only CPacsServerWindowApp object
CPacsServerWindowApp theApp;

/////////////////////////////////////////////////////////////////////////////
//	CPacsServerWindowApp initialization
BOOL CPacsServerWindowApp::InitInstance()
{
	CWinApp::InitInstance();

	WSAData ws = {0};

	//	Startup network layer.
	if(WSAStartup(0x0202, &ws))
	{
		AfxMessageBox("Windows socket initialization failed.");
		return FALSE;
	}

	imapacs::DICOMSession::InitializeCodecs();

	/////////////////////////////////////////////////////////////////////////
	//	Standard initialization
	//	If you are not using these features and wish to reduce the size
	//	of your final executable, you should remove from the following
	//	the specific initialization routines you do not need
	//	Change the registry key under which our settings are stored
	//	TODO: You should modify this string to be something appropriate
	//	such as the name of your company or organization
	SetRegistryKey(_T("Imapacs"));

	CPacsServerWindowDlg dlg;

	m_pMainWnd        = &dlg;
	INT_PTR nResponse = dlg.DoModal();

	if(nResponse == IDOK)
	{
	}
	else if(nResponse == IDCANCEL)
	{
	}

	WSACleanup();	//	Shutdown network layer.

	imapacs::DICOMSession::ClearCodecs();

	//	Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}
