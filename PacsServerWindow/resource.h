//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by PacsServerWindow.rc
//
#define IDD_PACSSERVERWINDOW_DIALOG     102
#define IDR_MAINFRAME                   128
#define IDR_TRAY_MENU                   129
#define IDB_LOGO_ONE                    130
#define IDB_LOGO_TWO                    131
#define IDD_ABOUT                       132
#define IDC_DB_SOURCE                   1000
#define IDC_STATIC_GRP_DB               1001
#define IDC_STATIC_DB_SRC               1002
#define IDC_DB_USER                     1003
#define IDC_STATIC_DB_USR               1004
#define IDC_DB_PASSWORD                 1005
#define IDC_STATIC_DB_PWD               1006
#define IDC_STATIC_GRP_DICOM            1007
#define IDC_STATIC_LOGO_LEFT            1008
#define IDC_STATIC_LOGO_RIGHT           1009
#define IDC_STATIC_ABOUT_LOGO_ONE       1010
#define IDC_STATIC_ABOUT_LOGO_TWO       1010
#define IDC_STATIC_ABOUT_TEXT           1011
#define IDB_CHECK_IS_TRANSIENT          1013
#define IDC_CHECK_IS_TRANSIENT          1013
#define IDC_SRV_LOG_TMP_PATH            1014
#define IDC_SRV_LOG_PORT                1016
#define IDC_STATIC_SRV_PORT             1017
#define IDC_DB_SCHEME                   1018
#define IDC_STATIC_DB_SCHEME            1019
#define IDC_SRV_AE_TITLE                1020
#define IDC_STATIC_SRV_AE               1021
#define IDC_SRV_LOG_PATH                1022
#define IDC_STATIC_SRV_LOGPATH          1023
#define IDC_STATIC_SRV_TMP              1024
#define ID_TRAY_D32771                  32771
#define ID_TRAY_ARR32772                32772
#define ID_TRAY_CONFIGURATION           32773
#define ID_TRAY_32774                   32774
#define ID_TRAY_QUITTER                 32775
#define ID_STOP                         32776
#define ID_START                        32777
#define ID_CFG                          32778
#define ID_ABOUT                        32779
#define ID_QUIT                         32780

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        133
#define _APS_NEXT_COMMAND_VALUE         32781
#define _APS_NEXT_CONTROL_VALUE         1014
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
