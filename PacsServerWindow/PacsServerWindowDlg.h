/////////////////////////////////////////////////////////////////////////////
//	File		:	PacsServerWindowDlg.h
//	Description	:	Main dialog.
/////////////////////////////////////////////////////////////////////////////

#define WM_ICON_NOTIFY	WM_APP + 10

#include "SystemTray.h"
#include "../Server/PacsServer.h"

#pragma once

/////////////////////////////////////////////////////////////////////////////
//	CPacsServerWindowDlg dialog
class CPacsServerWindowDlg : public CDialog
{
// Construction
public:
	CPacsServerWindowDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_PACSSERVERWINDOW_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


	CSystemTray       m_Tray;
	bool              m_bVisible;

private:

	bool LoadConfig();
	bool SaveConfig();
	void UpdateInterface();
	void UpdateVariables();

	imapacs::PacsServer m_Server;

	CString             m_DbSource;
	CString             m_DbUser;
	CString             m_DbPassword;
	CString             m_DbScheme;

	CString             m_SrvAeTitle;
	CString             m_LogPath;
	CString             m_TmpPath;
	unsigned short      m_iPort;

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnWindowPosChanging(WINDOWPOS* lpwndpos);

protected:
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
public:
	BOOL m_bTransient;
};
