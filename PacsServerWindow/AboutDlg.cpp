/////////////////////////////////////////////////////////////////////////////
//	File		:	AboutDlg.cpp
//	Description	:	Implementation.
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "PacsServerWindow.h"
#include "AboutDlg.h"


// CAboutDlg dialog

IMPLEMENT_DYNAMIC(CAboutDlg, CDialog)

/////////////////////////////////////////////////////////////////////////////
//
CAboutDlg::CAboutDlg(CWnd* pParent,
					 const char *pAboutText)
	: CDialog(CAboutDlg::IDD, pParent)
{
	if(pAboutText)
	{
		m_AboutText = pAboutText;
	}
}

/////////////////////////////////////////////////////////////////////////////
//
CAboutDlg::~CAboutDlg()
{
}

/////////////////////////////////////////////////////////////////////////////
//
void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
//
BOOL CAboutDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  Add extra initialization here
	SetDlgItemText(IDC_STATIC_ABOUT_TEXT, m_AboutText);

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}
