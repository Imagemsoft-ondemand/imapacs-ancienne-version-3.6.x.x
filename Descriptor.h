/////////////////////////////////////////////////////////////////////////////
//	Descriptor.h
//
//	Desc.	:	This file and the corresponding .cpp are used only used
//				under windows. We need a global variable to hold the
//				socket descriptor in that case.
/////////////////////////////////////////////////////////////////////////////

#if defined(_WINDOWS)
#include <winsock.h>
#endif

#if !defined(__DESCRIPTOR_H_INCLUDED__)
#define      __DESCRIPTOR_H_INCLUDED__

/////////////////////////////////////////////////////////////////////////////
//
#if defined(_WINDOWS)
typedef SOCKET SOCKET_DESCRIPTOR;
#else
typedef int SOCKET_DESCRIPTOR;
#endif

#endif	//	__DESCRIPTOR_H_INCLUDED__
