///////////////////////////////////////////////////////////////////
//   File   :   Queue.h
///////////////////////////////////////////////////////////////////

#include <stdio.h>	// For NULL.

#if !defined(__QUEUE_H__)
#define      __QUEUE_H__

#define	QUEUE_VERSION	"1.0.0.0"

///////////////////////////////////////////////////////////////////
// Conatiner object.
template <class DTYPE>
class Container
{
public:
	Container()          {m_pNext = NULL;m_pPrev = NULL;}
	virtual ~Container() {}
	DTYPE             m_Content;
	Container<DTYPE> *m_pNext;
	Container<DTYPE> *m_pPrev;
};

///////////////////////////////////////////////////////////////////
//	Used to keep or set position in the queue.
template <class DTYPE>
class Position
{
public:
	Position()           {m_pPosition = NULL;};
	virtual ~Position()  {}

	Container<DTYPE> *m_pPosition;
};

///////////////////////////////////////////////////////////////////
// Simply linked list of container objects with access methods.
template <class DTYPE>
class Queue
{
public:
	Queue();
	virtual ~Queue();

	char *GetVersion() const                               {return QUEUE_VERSION;}

	bool SetAtFirst() {m_pCur = m_pFirst;if(m_pCur == NULL) return false;return true;};
	bool Add(DTYPE &element);
	bool Push(DTYPE &element);
	bool Pop(DTYPE &element);
	bool GetCurrent(DTYPE &element);
	bool Next();
	bool GetFirst(DTYPE &element);
	bool GetNext(DTYPE &element);
	bool RemoveCurrent();
	void Clear();
	int GetSize()                                          {return m_iSize;}

	bool SetCurrentPosition(Position <DTYPE> &pos);
	bool GetCurrentPosition(Position <DTYPE> &pos);

private:
	int               m_iSize;
	Container<DTYPE> *m_pFirst;
	Container<DTYPE> *m_pLast;
	Container<DTYPE> *m_pCur;
};

#include "Queue.cxx"

#endif	//	__QUEUE_H__
