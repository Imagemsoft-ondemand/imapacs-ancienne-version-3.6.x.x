﻿# Pour développer
## Dépendances
Oracle instant client 12
dcmtk 3.6.1

## Instant client
Télécharger : 
* instantclient-basic-linux.x64-12.2.0.1.0.zip
* instantclient-sdk-linux.x64-12.2.0.1.0.zip
* instantclient-sqlplus-linux.x64-12.2.0.1.0.zip

Extraires les zip avec la commande 
* unzip <Nom du fichier>

## DCMTK
* Télécharger le package dcmtk361 à partir de artifactory (private-internal-repository:dcmtk361/dcmtk361/dcmtk361/dcmtk361-dcmtk361.zip )

## Comilation
* make
* make installer

NOTE : Il est important d'ajuster les chemins dans le fichier makefile pour l'inclusion de dcmtk et du SDK de OCI selon leur emplacement.

Le processus génère un ficher pacs_x.y.z.w_installer.zip

## Pour installer
* unzip pacs_x.y.z.w_installer.zip
* sudo installer/install.sh

