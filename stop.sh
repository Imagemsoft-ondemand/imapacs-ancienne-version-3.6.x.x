#!/bin/sh
# Script used to stop the PACS server.

###################################################################################################
#
is_process_running()
{
    local running=0

    ps -o pid= -p $1

    if [ $? -eq 0 ]
    then
        running=1
    fi

    return $running
}

###################################################################################################
#
PID_FILE=""
INSTALL_DIR="/opt/imagem/imapacs"
PID_FILE_NORMAL=$INSTALL_DIR/Imapacs.pid
PID_FILE_TRANSIENT=$INSTALL_DIR/Imapacs_Transient.pid


if [ -d $INSTALL_DIR ]
then
    if [ -f $PID_FILE ]
    then

        if [ $1 = "t" ]
        then
            PID_FILE=$PID_FILE_TRANSIENT;
        else
            PID_FILE=$PID_FILE_NORMAL;
        fi

        PROCESS_PID="$(cat $PID_FILE)"

        is_running="false"

        is_process_running $PROCESS_PID
        
        if [ $? -eq 1 ]
        then

            #   Try to stop the server gracefully.
            #
            kill -15 $PROCESS_PID

            tm=15

            while [ $tm != 0 ]
            do
                is_process_running $PROCESS_PID

                if [ $? -eq 0 ]
                then
                    break;
                fi

                sleep 1

                tm=(tm-1)
            done

            is_process_running $PROCESS_PID

            if [ $tm -eq 1 ]
            then
                #   Kill the server process
                #
                kill -9 $PROCESS_PID
            fi
        fi
    fi
fi
